package com.tescobank.mobile.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;

/**
 * Broadcast receiver for network status changes
 */
public class NetworkStatusChangeReceiver extends BroadcastReceiver {
	
	private static final String TAG = NetworkStatusChecker.class.getSimpleName();
	
	/** The network status checker */
	private NetworkStatusChecker networkStatusChecker;

	/** Whether the network was available the last time it was checked */
	private boolean networkWasAvailable;
	
	/**
	 * Constructor
	 * 
	 * @param context the context
	 */
	public NetworkStatusChangeReceiver(Context context) {
		super();
		
		this.networkStatusChecker = new NetworkStatusChecker();
		this.networkWasAvailable = networkStatusChecker.isAvailable(context);
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Network Status Changed");
		}
		
		boolean networkIsAvailable = networkStatusChecker.isAvailable(context);
		if (isConnectionRegained(networkIsAvailable)) {
			handleConnectionRegained(context);
		} else if (isConnectionLost(networkIsAvailable)) {
			handleConnectionLost(context);
		} else {
			handleConnectionUnchanged();
		}
		
		// Note whether the network is available
		networkWasAvailable = networkIsAvailable;
	}
	
	private boolean isConnectionRegained(boolean networkIsAvailable) {
		return networkIsAvailable && !networkWasAvailable;
	}
	
	private boolean isConnectionLost(boolean networkIsAvailable) {
		return !networkIsAvailable && networkWasAvailable;
	}
	
	private void handleConnectionRegained(Context context) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Notifiying Listeners of Connection Regained");
		}
		
		// Inform listeners that the connection has been regained
		Intent intentToBroadcast = new Intent(NetworkEvent.CONNECTION_REGAINED);
		context.sendBroadcast(intentToBroadcast);
	}
	
	private void handleConnectionLost(Context context) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Notifiying Listeners of Connection Lost");
		}
		
		// Inform listeners that the connection has been lost
		Intent intentToBroadcast = new Intent(NetworkEvent.CONNECTION_LOST);
		context.sendBroadcast(intentToBroadcast);
	}
	
	private void handleConnectionUnchanged() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Ignoring Network Status Change as Not Significant");
		}
	}
}
