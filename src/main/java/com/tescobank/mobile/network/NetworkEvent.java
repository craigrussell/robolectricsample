package com.tescobank.mobile.network;

/**
 * Supported network events
 */
public interface NetworkEvent {
	
	/** Indicates that the network connection has just been lost */
	String CONNECTION_LOST = "connection-lost";
	
	/** Indicates that the network connection has just been regained */
	String CONNECTION_REGAINED = "connection-regained";
}
