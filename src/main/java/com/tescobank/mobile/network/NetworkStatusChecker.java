package com.tescobank.mobile.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;

/**
 * Used to check the status of the network
 */
public class NetworkStatusChecker {
	
	private static final String TAG = NetworkStatusChecker.class.getSimpleName();
	
	/**
	 * Constructor
	 */
	public NetworkStatusChecker() {
		super();
	}
	
	/**
	 * Returns true if the network is available
	 * 
	 * @param context the context
	 * 
	 * @return true if the network is available
	 */
	public boolean isAvailable(Context context) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Checking Network Status");
		}
		
		ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		boolean available = (networkInfo != null) && networkInfo.isConnected();
		
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Network Is " + (available ? "" : "Not ") + "Available");
		}
		
		return available;
	}
}
