package com.tescobank.mobile.encode;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;

public final class HMACSHA256Encoder {
	
	private static final String TAG = HMACSHA256Encoder.class.getSimpleName();
	
	private static final int OFFSET = 0x100;
	private static final int MASK = 0xff;
	private static final int BASE_16 = 16;
	
	private HMACSHA256Encoder() {
		super();
	}

	/**
	 * @param item
	 * @param salt
	 * @return hmac sha256 encoded hexadecimal String
	 */
	public static String encode(String item, String salt) {
		String hashedItem = null;
		try {
			Mac mac = Mac.getInstance("HmacSHA256");
			SecretKeySpec secretKey = new SecretKeySpec(salt.getBytes(),"HmacSHA256");
			mac.init(secretKey);
			byte[] hashedBytes = mac.doFinal(item.getBytes("UTF-8"));
			hashedItem = bytesToHex(hashedBytes);
		} catch (Exception e) {
			if (BuildConfig.DEBUG) {
				Log.e(TAG, "Could not create hash for item", e);
			}
		}
		return hashedItem;
	}

	private static String bytesToHex(byte[] bytes) {
		StringBuffer hex = new StringBuffer();
		for (byte b : bytes) {
			hex.append(Integer.toString((b & MASK) + OFFSET, BASE_16).substring(1));
		}
		return hex.toString();
	}
}
