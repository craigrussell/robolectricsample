package com.tescobank.mobile.device;

import java.util.List;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

public final class DeviceChecker {
	
	private DeviceChecker() {
		super();
	}
	
	public static boolean isEmulator(Context context) {
		
		SensorManager manager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE); 
		List<Sensor> sensors = manager.getSensorList(Sensor.TYPE_ALL);
		
		if(sensors.isEmpty()) {
			return true;
		}
		return false;
	}
}
