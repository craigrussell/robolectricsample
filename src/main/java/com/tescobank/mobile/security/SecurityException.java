package com.tescobank.mobile.security;

/**
 * Security provider exception
 */
public class SecurityException extends Exception {
	
	/** Serial version ID */
	private static final long serialVersionUID = -101965589584119674L;

	/**
	 * Constructor
	 * 
	 * @param cause the cause
	 */
	public SecurityException(Throwable cause) {
		super(cause);
	}
}
