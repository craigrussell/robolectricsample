package com.tescobank.mobile.security;

/**
 * Listener for OTP generation events
 */
public interface OTPGenerationListener {
	
	/**
	 * Notifies the listener that the specified OTP has been generated
	 * 
	 * @param otp the OTP; null if generation failed
	 */
	void otpGenerated(String otp);
}
