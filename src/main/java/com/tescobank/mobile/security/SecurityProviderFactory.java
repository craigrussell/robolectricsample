package com.tescobank.mobile.security;

import android.content.Context;

import com.tescobank.mobile.persist.Persister;

/**
 * Factory for security providers
 */
public interface SecurityProviderFactory {
	
	SecurityProvider getSecurityProvider(Context context, Persister persister, String provUrl);
	
	class DefaultSecurityProviderFactory implements SecurityProviderFactory {

		/** The single security provider instance */
		private static SecurityProvider securityProvider;

		/**
		 * Returns a security provider
		 * 
		 * @param context
		 *            the context
		 * @param persister
		 *            the persister
		 * 
		 * @return a security provider
		 */
		public synchronized SecurityProvider getSecurityProvider(Context context, Persister persister, String provUrl) {

			// Implemented as a singleton as Arcot only appears to support a single instance on a device

			if (securityProvider == null) {
				securityProvider = new ArcotSecurityProvider(context, persister, provUrl);
			}

			return securityProvider;
		}
		
	}
	
}
