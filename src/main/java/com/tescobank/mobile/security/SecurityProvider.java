package com.tescobank.mobile.security;

/**
 * A provider of security-related services
 */
public interface SecurityProvider {
	
	/**
	 * Creates an account
	 * 
	 * @param xml the user information that is required to create an account
	 * 
	 * @throws SecurityException
	 */
	void createAccount(String xml) throws SecurityException;
	
	/**
	 * Generates an OTP, notifying the specified listener of the OTP generated
	 * 
	 * @param seed the seed
	 * @param the listener to be notified
	 */
	void generateOTP(String seed, OTPGenerationListener listener);
	
	/**
	 * Cancels any outstanding OTP generation
	 */
	void cancelOTPGeneration();
	
	/**
	 * Notes that the last OTP generated was used successfully
	 */
	void lastOTPWasSuccessful();
	
	/**
	 * Changes PIN
	 * 
	 * @param oldPin the old PIN
	 * @param newPin the new PIN
	 * 
	 * @throws SecurityException
	 */
	void changePin(String oldPin, String newPin) throws SecurityException;
	
	/**
	 * Delete all accounts
	 * 
	 * @throws SecurityException
	 */
	void deleteAllAccounts() throws SecurityException;
}
