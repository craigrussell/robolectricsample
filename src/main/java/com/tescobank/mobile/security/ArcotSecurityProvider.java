package com.tescobank.mobile.security;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.arcot.aotp.lib.Account;
import com.arcot.aotp.lib.OTP;
import com.arcot.aotp.lib.OTPException;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.persist.Persister;

/**
 * Arcot-specific security provider
 */
public class ArcotSecurityProvider implements SecurityProvider {

	private static final String TAG = ArcotSecurityProvider.class.getSimpleName();
	private static final String START_UTC_TAG = "<utc>";
	private static final String END_UTC_TAG = "</utc>";
	private static final int UTC_START_TAG_OFFSET = 5;

	/**
	 * This is static so that it gets a handle to ui thread. The handleMessage method runs on the ui thread.
	 */
	private static final Handler uiThreadHandler = new Handler() {
		public void handleMessage(Message msg) {
			OTPResult result = OTPResult.class.cast(msg.obj);
			result.listener.otpGenerated(result.otp);
		}
	};

	/** The interval between generation of successfully-used OTPs (in milliseconds) */
	private static final long OTP_GENERATION_INTERVAL = 30000;

	/** Signifies no OTP generation time */
	private static final long NO_OTP_GENERATION_TIME = 0;

	/** The persister */
	private Persister persister;

	/** The underlying Arcot OTP instance */
	private OTP otp;

	/** The URL of the security server that issued the OTP for the user */
	private String provUrl;

	/** The date/time (in milliseconds) that the last OTP was generated */
	private Long lastOTPGeneratedAt;

	private Future<?> otpGenerationTask;

	/**
	 * Constructor
	 * 
	 * @param context
	 *            the context
	 */
	public ArcotSecurityProvider(Context context, Persister persister, String provUrl) {
		super();

		this.persister = persister;
		this.otp = new OTP(context);
		this.provUrl = provUrl;
	}

	@Override
	public void createAccount(String xml) throws SecurityException {
		try {

			deleteAllAccounts();
			Account account = otp.provisionAccount(xml, provUrl, null, null);
			int startIndex = xml.indexOf(START_UTC_TAG) + UTC_START_TAG_OFFSET;

			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Getting xml = " + xml);
				Log.d(TAG, "Getting start Index = " + startIndex);
			}

			int endIndex = xml.lastIndexOf(END_UTC_TAG);

			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Getting end Index = " + endIndex);
				Log.d(TAG, "Epoche = " + xml.substring(startIndex, endIndex));
			}
			otp.resync(account, xml.substring(startIndex, endIndex));

		} catch (OTPException e) {
			throw new SecurityException(e);
		}
	}

	@Override
	public void generateOTP(String seed, OTPGenerationListener listener) {

		long delay = 0;
		long lastSuccessfulOTPGeneratedAt = persister.getLastSuccessfulOTPGeneratedAt();

		if (lastSuccessfulOTPGeneratedAt != NO_OTP_GENERATION_TIME) {
			delay = Math.max(0, OTP_GENERATION_INTERVAL - (System.currentTimeMillis() - lastSuccessfulOTPGeneratedAt));
		}

		otpGenerationTask = Executors.newSingleThreadScheduledExecutor().schedule(new OTPGenerationTask(seed, listener), delay, TimeUnit.MILLISECONDS);
	}

	@Override
	public void cancelOTPGeneration() {

		// Avoid the need to synchronize by obtaining a local handle to the task
		Future<?> taskToCancel = otpGenerationTask;
		if (taskToCancel != null) {
			taskToCancel.cancel(true);
		}
	}

	@Override
	public void lastOTPWasSuccessful() {
		if (lastOTPGeneratedAt != null) {
			persister.persistLastSuccessfulOTPGeneratedAt(lastOTPGeneratedAt);
		}
	}

	@Override
	public void changePin(String oldPin, String newPin) throws SecurityException {
		try {

			String accountId = getExistingAccountId();
			otp.resetPin(accountId, oldPin, newPin);

		} catch (OTPException e) {
			throw new SecurityException(e);
		}
	}

	@Override
	public void deleteAllAccounts() throws SecurityException {
		try {

			Account[] accounts = otp.getAllAccounts();
			if (accounts != null) {
				for (Account account : accounts) {
					otp.deleteAccount(account.getId());
				}
			}

		} catch (OTPException e) {
			throw new SecurityException(e);
		}
	}

	/**
	 * Closes any underlying connections
	 * 
	 * Provided to support automated testing; not exposed by the SecurityProvider interface
	 */
	public void close() {
		otp.close();
	}

	/**
	 * Returns the ID of the existing account
	 * 
	 * @return the account ID
	 * 
	 * @throws OTPException
	 */
	private String getExistingAccountId() throws OTPException {

		// Assume that there is exactly one account (devices cannot be shared)
		Account[] accounts = otp.getAllAccounts();
		String accountId = ((accounts == null) || (accounts.length == 0)) ? null : accounts[0].getId();

		return accountId;
	}

	/**
	 * Inner class used to generate an OTP after a specified delay
	 */
	private class OTPGenerationTask implements Runnable {

		private String seed;
		private OTPGenerationListener listener;
		private String otpGenerated;

		public OTPGenerationTask(String seed, OTPGenerationListener listener) {
			super();

			this.seed = seed;
			this.listener = listener;
		}

		@Override
		public void run() {

			try {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "Generating OTP");
				}

				String accountId = getExistingAccountId();
				otpGenerated = otp.generateOTP(accountId, seed, null);
				lastOTPGeneratedAt = System.currentTimeMillis();
				otpGenerationTask = null;

			} catch (OTPException e) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "Failed to generate OTP", e);
				}
			}

			Message message = Message.obtain(uiThreadHandler);
			message.obj = new OTPResult(listener, otpGenerated);
			message.sendToTarget();
		}
	}

	private static class OTPResult {
		private final String otp;
		private final OTPGenerationListener listener;

		public OTPResult(OTPGenerationListener listener, String otp) {
			this.otp = otp;
			this.listener = listener;
		}
	}

}
