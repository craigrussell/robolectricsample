package com.tescobank.mobile.format;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;

/**
 * Data formatter
 */
public class DataFormatter {

	private static final int SORT_CODE_PART2_END = 4;

	private static final int SORT_CODE_PART1_END = 2;

	private static final String TAG = DataFormatter.class.getSimpleName();
	
	private static final String EMPTY_STRING = "";
	
	private static final String PERCENTAGE_SYMBOL = "%";

	private static final SimpleDateFormat INPUT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);

	private static final SimpleDateFormat OUTPUT_DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy", Locale.UK);

	private static final SimpleDateFormat OUTPUT_FULL_DATE_FORMAT = new SimpleDateFormat("dd MMMM yyyy", Locale.UK);

	private static final SimpleDateFormat OUTPUT_DATE_FORMAT_DD_MM = new SimpleDateFormat("dd MMM", Locale.UK);
	
	private static final SimpleDateFormat OUTPUT_DATE_FORMAT_YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);

	private static final SimpleDateFormat OUTPUT_DATE_FORMAT_DAY_FULL_MONTH_AND_YEAR = new SimpleDateFormat("dd MMMM yyyy", Locale.UK);
	
	private static final SimpleDateFormat OUTPUT_DATE_FORMAT_FULL_MONTH_AND_YEAR = new SimpleDateFormat("MMMM yyyy", Locale.UK);
	
	private static final SimpleDateFormat OUTPUT_DATE_FORMAT_INAUTH = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ", Locale.UK);
	
	private static final SimpleDateFormat INPUT_DATE_FORMAT_INAUTH = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ", Locale.UK);

	public String formatCurrency(Object amount) {
		NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.UK);
		if (amount == null) {
			return nf.format(0);
		}

		return nf.format(amount);
	}
	
	public String formatCurrencyNoDecimalPlaces(Object amount) {
		NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.UK);
		nf.setMaximumFractionDigits(0);
		
		if (amount == null) {
			return nf.format(0);
		}

		return nf.format(amount);
	}

	public String formatPercentage(String amount) {
		return getDoubleFromString(amount) + PERCENTAGE_SYMBOL;
	}
	
	public String formatPercentageRoundedToTwoDecimalPlaces(String amount) {
		return formatRoundedToTwoDecimalPlaces(amount) + PERCENTAGE_SYMBOL;
	}

	public String formatRoundedToTwoDecimalPlaces(String amount) {	
		return String.format(Locale.UK,"%.2f", getDoubleFromString(amount));
	}
	
	private double getDoubleFromString(String amount) {
		if (amount == null) {
			return 0;
		} else {
			return Double.valueOf(amount);
		}
	}

	public String formatDate(String date) {
		return formatDate(date, OUTPUT_DATE_FORMAT);
	}

	public String formatDateToFullDate(Date date) {
		return OUTPUT_FULL_DATE_FORMAT.format(date);
	}
	
	public String formatDateToDDMM(String date) {
		return formatDate(date, OUTPUT_DATE_FORMAT_DD_MM);
	}
	
	public String formatDateToYYYYMMDD(Date date) {
		return formatDate(date, OUTPUT_DATE_FORMAT_YYYY_MM_DD);
	}

	private String formatDate(String dateString, SimpleDateFormat outputFormat) {

		if (dateString == null) {
			return dateString;
		}

		Date date = parseDate(dateString);
		if (date == null) {
			return dateString;
		}

		return outputFormat.format(date);
	}
	
	private String formatDate(Date date, SimpleDateFormat outputFormat) {
		
		if (date == null) {
			return EMPTY_STRING;
		}
		
		return outputFormat.format(date);
	}
	
	public Calendar parseCalendarDate(String dateStr) {
		Date date = parseDate(dateStr, INPUT_DATE_FORMAT);
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(date);
		return calDate;
	}
	
	public Date parseDate(String date) {
		return parseDate(date, INPUT_DATE_FORMAT);
	}
	
	private Date parseDate(String date, SimpleDateFormat format) {
		
		if (date == null) {
			return null;
		}
		
		try {
			return format.parse(date);
		} catch (ParseException e) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Error parsing date using format: " + format);
			}
		}
		return null;
	}

	public String formatSortCode(String sortCode) {
		try {
			return String.format("%s-%s-%s", sortCode.substring(0, SORT_CODE_PART1_END), sortCode.substring(SORT_CODE_PART1_END, SORT_CODE_PART2_END), sortCode.substring(SORT_CODE_PART2_END));
		} catch (IndexOutOfBoundsException ie) {
			return sortCode;
		}
	}

	public String formatDateAsDayFullMonthAndYear(Date date) {
		return OUTPUT_DATE_FORMAT_DAY_FULL_MONTH_AND_YEAR.format(date);
	}
	
	public String formatDateAsFullMonthAndYear(Date date) {
		return OUTPUT_DATE_FORMAT_FULL_MONTH_AND_YEAR.format(date);
	}
	
	public Date parseInAuthDate(String date) {
		return parseDate(date, INPUT_DATE_FORMAT_INAUTH);
	}
	
	public String formatInAuthDate(Date date) {
		return OUTPUT_DATE_FORMAT_INAUTH.format(date);
	}
	
	public String sortCodeAndAccoutNumberJoiner(String sortCode, String accountNumber) {		
		return formatSortCode(sortCode) + " " + accountNumber;
	}
	
	public static String formatCardNumber(String cardNumber) {
		if (null == cardNumber || cardNumber.length() < 4) {
			return "";
		}
		String last4 = cardNumber.substring(cardNumber.length() - 4);
		return "**** **** **** " + last4;
	}
	
}
