package com.tescobank.mobile.rest.processor.volley;

import com.tescobank.mobile.rest.processor.RestConfig;
import com.tescobank.mobile.rest.processor.RestRequestProcessor;
import com.tescobank.mobile.rest.processor.RestRequestProcessorFactory;

public class VolleyRestRequestProcessorFactory<A> implements RestRequestProcessorFactory {

	@Override
	public <E> RestRequestProcessor<E> createRestRequestProcessor(RestConfig<E> config) {
		return new VolleyRestRequestProcessor<E, A>(config);
	}

}
