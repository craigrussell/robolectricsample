package com.tescobank.mobile.rest.processor;

import android.content.Context;

import com.tescobank.mobile.json.JsonParser;
import com.tescobank.mobile.rest.request.RestErrorResponseBuilder;
import com.tescobank.mobile.rest.request.RestRequestHeadersProvider;
import com.tescobank.mobile.rest.request.RestResponseHeadersHandler;

/**
 * Configuration for Rest processing
 * 
 * @param <E>
 *            the object type of the error response
 */
public class RestConfig<E> {
	/** The context */
	private Context context;
	/** The JSON parser */
	private JsonParser jsonParser;
	/** The default base URL */
	private String defaultBaseUrl;
	/** The request headers provider */
	private RestRequestHeadersProvider requestHeadersProvider;
	/** The response headers handler */
	private RestResponseHeadersHandler responseHeadersHandler;

	/** The error response builder */
	private RestErrorResponseBuilder<E> errorResponseBuilder;

	private int connectionTimeOutValue;

	/**
	 * Constructor
	 */
	public RestConfig() {
		super();
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public JsonParser getJsonParser() {
		return jsonParser;
	}

	public void setJsonParser(JsonParser jsonParser) {
		this.jsonParser = jsonParser;
	}

	public String getDefaultBaseUrl() {
		return defaultBaseUrl;
	}

	public void setConnectionTimeOut(int connectionTimeOut) {
		this.connectionTimeOutValue = connectionTimeOut;
	}

	public int getConnectionTimeOut() {
		return connectionTimeOutValue;
	}

	public void setDefaultBaseUrl(String defaultBaseUrl) {
		this.defaultBaseUrl = defaultBaseUrl;
	}

	public RestRequestHeadersProvider getRequestHeadersProvider() {
		return requestHeadersProvider;
	}

	public void setRequestHeadersProvider(RestRequestHeadersProvider requestHeadersProvider) {
		this.requestHeadersProvider = requestHeadersProvider;
	}

	public RestResponseHeadersHandler getResponseHeadersHandler() {
		return responseHeadersHandler;
	}

	public void setResponseHeadersHandler(RestResponseHeadersHandler responseHeadersHandler) {
		this.responseHeadersHandler = responseHeadersHandler;
	}

	public RestErrorResponseBuilder<E> getErrorResponseBuilder() {
		return errorResponseBuilder;
	}

	public void setErrorResponseBuilder(RestErrorResponseBuilder<E> errorResponseBuilder) {
		this.errorResponseBuilder = errorResponseBuilder;
	}
}
