package com.tescobank.mobile.rest.processor;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.rest.request.RestRequest;
import com.tescobank.mobile.rest.request.RestRequestCompletionListener;

public class BatchRestRequestProcessor implements RestRequestCompletionListener<ErrorResponseWrapper> {
	
	private static final String TAG = BatchRestRequestProcessor.class.getSimpleName();

	private RestRequestProcessor<ErrorResponseWrapper> processor;
	private BatchRestRequestCompletionListener listener;
	private List<BatchRequest> batchRequests;
	private boolean notifiedListener;
	
	public BatchRestRequestProcessor(RestRequestProcessor<ErrorResponseWrapper> processor, BatchRestRequestCompletionListener listener) {
		batchRequests = new ArrayList<BatchRequest>();
		this.processor = processor;
		this.listener = listener;
	}

	/**
	 * @param request, a request to process. This will be processed when execute is called in the order it was added.
	 * @param parallelise, whether a request can be processed at the same time as others.
	 * 
	 * If a request is parallel and a subsequent request is the same, they will be processed together. As 
	 * will the next if it too is parallelisable. Non-parallelisable requests wait until in-flight requests
     * have finished before being executed and all other requests in turn wait for them.
	 */
	public void addRequest(RestRequest<?, ErrorResponseWrapper> request, boolean parallelise) {
		request.setCompletionListener(this);
		batchRequests.add(new BatchRequest(request, parallelise));
	}

	public void resetBatchRequestProcessor() {
		batchRequests.clear();
		notifiedListener = false;
	}
	/**
	 * Execute request batch. 
	 */
	public void execute() {
		executeNextRequests();
	}

	/**
	 * Executes as many parallel requests as possible
	 */
	private void executeNextRequests() {
		for (BatchRequest batchRequest : batchRequests) {
			if (batchRequest.getStatus() == RequestStatus.NOT_STARTED) {
				if (!areRequestsInFlight() || batchRequest.shouldParallelise()) {
					processRequest(batchRequest);
				}
				if (!batchRequest.shouldParallelise()) {
					break;
				}
			}
		}
	}

	private void processRequest(BatchRequest batchRequest) {
		batchRequest.setStatus(RequestStatus.IN_PROGRESS);
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting request " + batchRequest.getRequest().getClass().getSimpleName());
		}
		processor.processRequest(batchRequest.getRequest());
	}
	
	private BatchRequest getBatchRequest(RestRequest<?, ErrorResponseWrapper> request) {
		for (BatchRequest batchRequestWrapper : batchRequests) {
			if (batchRequestWrapper.getRequest().equals(request)) {
				return batchRequestWrapper;
			}
		}
		return null;
	}

	public boolean areRequestsInFlight() {
		for (BatchRequest batchRequest : batchRequests) {
			if (batchRequest.isInProgress()) {
				return true;
			}
		}
		return false;
	}

	public boolean isComplete() {
		for (BatchRequest batchRequest : batchRequests) {
			if (!batchRequest.isComplete()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void notifyRequestSuceeded(RestRequest<?, ErrorResponseWrapper> request) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Request Suceeded " + request.getClass().getSimpleName());
		}
		BatchRequest batchRequest = getBatchRequest(request);
		if(batchRequest != null) {
			batchRequest.setStatus(RequestStatus.COMPLETE);
		}
		
		if(isComplete()) {
			notifyListenerOfSuccess();
		} else {
			executeNextRequests();
		}
	}
	
	@Override
	public void notifyRequestFailed(RestRequest<?, ErrorResponseWrapper> request, ErrorResponseWrapper errorResponse) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Request Failed " + request.getClass().getSimpleName());
		}
		BatchRequest batchRequest = getBatchRequest(request);
		if(batchRequest != null) {
			batchRequest.setStatus(RequestStatus.COMPLETE);
		}
		processor.cancelAllRequests();
		notifyListenerOfError(errorResponse);
	}
	
	public synchronized void notifyListenerOfSuccess() {
		if(listener != null && !getNotifierListener()) {
			notifiedListener = true;
			listener.onBatchRequestsSuceeded();		
		}
	}

	public synchronized void notifyListenerOfError(ErrorResponseWrapper errorResponse) {
		if(listener != null && !getNotifierListener()) {
			notifiedListener = true;
			listener.onBatchRequestsFailed(errorResponse);
		}
	}
	
	public boolean getNotifierListener() {
		return notifiedListener;
	}
	
	private enum RequestStatus {
		NOT_STARTED,
		IN_PROGRESS,
		COMPLETE
	}
	
	private static class BatchRequest {
		
		private RestRequest<?, ErrorResponseWrapper> request;
		
		private boolean parallelise;
		
		private RequestStatus status;
		
		public BatchRequest(RestRequest<?, ErrorResponseWrapper> request, boolean parallelise) {
			this.request = request;
			this.parallelise = parallelise; 
			this.status = RequestStatus.NOT_STARTED;
		}
		
		public boolean shouldParallelise() {
			return parallelise;
		}

		public RequestStatus getStatus() {
			return status;
		}
		
		public void setStatus(RequestStatus status) {
			this.status = status;
		}

		public boolean isComplete() {
			return RequestStatus.COMPLETE.equals(status);
		}

		public boolean isInProgress() {
			return RequestStatus.IN_PROGRESS.equals(status);
		}

		public RestRequest<?, ErrorResponseWrapper> getRequest() {
			return request;
		}
	}
}