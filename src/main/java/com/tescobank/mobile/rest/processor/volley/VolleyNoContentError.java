package com.tescobank.mobile.rest.processor.volley;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

/**
 * Volley error indicating that a response contained no content
 */
public class VolleyNoContentError extends VolleyError {

	private static final long serialVersionUID = 5958447186180935579L;
	
	public VolleyNoContentError(NetworkResponse response) {
		super(response);
	}
}
