package com.tescobank.mobile.rest.processor;

/**
 * Factory for Rest request processors
 */
public interface RestRequestProcessorFactory {

	/**
	 * Creates and returns a Rest request processor
	 * 
	 * @param config
	 *            the Rest configuration
	 * 
	 * @return a Rest request processor
	 */
	<E> RestRequestProcessor<E> createRestRequestProcessor(RestConfig<E> config);
}
