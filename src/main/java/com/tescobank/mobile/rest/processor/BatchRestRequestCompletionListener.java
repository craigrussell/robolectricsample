package com.tescobank.mobile.rest.processor;

import com.tescobank.mobile.api.error.ErrorResponseWrapper;

public interface BatchRestRequestCompletionListener {

	void onBatchRequestsSuceeded();

	void onBatchRequestsFailed(ErrorResponseWrapper errorResponse);
}
