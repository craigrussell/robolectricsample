package com.tescobank.mobile.rest.processor.volley;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.tescobank.mobile.BuildConfig;

/**
 * Volley-specific retry policy
 */
public class VolleyRetryPolicy extends DefaultRetryPolicy {

	
	private static final String TAG = VolleyRetryPolicy.class.getSimpleName();
	
	private boolean shouldRetry;
	
	public VolleyRetryPolicy(int connectionTimeoutMS, boolean shouldRetry) {
		super(connectionTimeoutMS, shouldRetry ? DEFAULT_MAX_RETRIES : 0, DEFAULT_BACKOFF_MULT);
		this.shouldRetry = shouldRetry;
	}

	@Override
	public void retry(VolleyError error) throws VolleyError {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "retry: " + getCurrentRetryCount() + " " + error);
		}
		super.retry(error);
	}
	

	public boolean shouldRetry() {
		return shouldRetry;
	}

}
