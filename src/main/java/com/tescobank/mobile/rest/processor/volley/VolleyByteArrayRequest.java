package com.tescobank.mobile.rest.processor.volley;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.tescobank.mobile.json.JsonParseException;
import com.tescobank.mobile.rest.processor.RestConfig;
import com.tescobank.mobile.rest.request.RestByteArrayResponse;
import com.tescobank.mobile.rest.request.RestResponseHeadersHandler;

/**
 * Volley request where the response is a byte array
 * 
 * @param <E>
 *            the error response type
 */
public class VolleyByteArrayRequest<E> extends Request<Object> {
	private Response.Listener<Object> listener;
	private RestConfig<E> config;
	private String acceptType;
	private Map<String, String> requestHeaders;
	private RestResponseHeadersHandler responseHeadersHandler;
	private boolean responseHeaderHandlingRequired;
	private Class<E> errorResponseObjectClass;

	public VolleyByteArrayRequest(int method, String url,  String acceptType, Listener<Object> listener, ErrorListener errorListener, RestConfig<E> config, Class<E> errorResponseObjectClass, boolean responseHeaderHandlingRequired, boolean shouldRetry) {
		super(method, url, errorListener);

		setRetryPolicy(new VolleyRetryPolicy(config.getConnectionTimeOut(), shouldRetry));
		this.listener = listener;
		this.config = config;
		this.acceptType = acceptType;
		this.requestHeaders = config.getRequestHeadersProvider().getHeaders();
		this.responseHeadersHandler = config.getResponseHeadersHandler();
		this.responseHeaderHandlingRequired = responseHeaderHandlingRequired;
		this.errorResponseObjectClass = errorResponseObjectClass;
	}

	@Override
	protected Response<Object> parseNetworkResponse(NetworkResponse response) {
		byte[] data = response.data;
		try {
			handleResponseHeaders(response);
			return handleAsErrorJson(response, data);
		} catch (UnsupportedEncodingException e) {
			return Response.error(new ParseError(e));
		} catch (JsonParseException e) {
			return handleAsObjectByteArray(response, data);
		}
	}

	private void handleResponseHeaders(NetworkResponse response) {
		if (response != null && responseHeaderHandlingRequired) {
			responseHeadersHandler.handleHeaders(response.headers);
		}
	}

	private Response<Object> handleAsErrorJson(NetworkResponse response, byte[] data) throws UnsupportedEncodingException, JsonParseException {
		if ((data == null) || (data.length == 0)) {
			return Response.error(new VolleyNoContentError(response));
		}
		String json = new String(data, HttpHeaderParser.parseCharset(response.headers));
		Object errorObject = config.getJsonParser().parseObject(json, errorResponseObjectClass);
		return Response.success(errorObject, HttpHeaderParser.parseCacheHeaders(response));
	}

	private Response<Object> handleAsObjectByteArray(NetworkResponse response, byte[] data) {
		Object byteArrayHolder = new RestByteArrayResponse(data);
		return Response.success(byteArrayHolder, HttpHeaderParser.parseCacheHeaders(response));
	}

	@Override
	protected VolleyError parseNetworkError(VolleyError volleyError) {
		handleResponseHeaders(volleyError.networkResponse);
		return super.parseNetworkError(volleyError);
	}
	
	@Override
	protected void deliverResponse(Object response) {
		listener.onResponse(response);
	}

	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		Map<String, String> headers = (requestHeaders == null) ? new HashMap<String, String>() : requestHeaders;
		if (null != acceptType) {
			headers.put("Accept", acceptType);
		}
		return headers;
	}
}
