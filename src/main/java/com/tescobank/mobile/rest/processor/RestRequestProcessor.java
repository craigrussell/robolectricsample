package com.tescobank.mobile.rest.processor;

import android.app.Activity;

import com.tescobank.mobile.rest.request.RestRequest;

/**
 * Rest request processor
 * 
 * @param <E> the object type of the error response
 */
public interface RestRequestProcessor<E> {
	
	/**
	 * Processes the specified Rest request
	 * 
	 * @param restRequest the Rest request
	 */
	void processRequest(RestRequest<?, E> restRequest);
	
	/**
	 * Processes the specified Rest request
	 * 
	 * @param restRequest the Rest request
	 * @param requesterType the requester type
	 */
	void processRequest(RestRequest<?, E> restRequest, Class<? extends Activity> requesterType);
	
	/**
	 * Cancels all Rest requests being handled by this processor
	 */
	void cancelAllRequests();
	
	/**
	 * Returns the class of the last request made by the specified requester type
	 * 
	 * @param requesterType the requester type
	 * 
	 * @return the class of the last request made by that requester type
	 */
	Class<?> getLastRequestTypeForRequesterType(Class<? extends Activity> requesterType);
	
	/**
	 * Clears the mapping from requester type to last request type for that request type
	 * 
	 * @param requesterType the requester type
	 */
	void clearLastRequestTypeForRequesterType(Class<? extends Activity> requesterType);
}
