package com.tescobank.mobile.rest.processor.volley;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.json.JsonParseException;
import com.tescobank.mobile.rest.processor.RestConfig;
import com.tescobank.mobile.rest.request.RestResponseHeadersHandler;

/**
 * Volley request where the response is a JSON string
 * 
 * @param <T>
 *            the response type
 * @param <E>
 *            the error response type
 */
public class VolleyJsonStringRequest<T, E> extends JsonRequest<Object> {
	private final static String TAG = VolleyJsonStringRequest.class.getSimpleName();

	private Map<String, String> requestHeaders;
	private RestResponseHeadersHandler responseHeadersHandler;
	private boolean responseHeaderHandlingRequired;
	private String bodyContentType;
	private Class<?> responseObjectClass;
	private RestConfig<?> config;
	private Class<?> errorResponseObjectClass;
	private String acceptType;

	public VolleyJsonStringRequest(int method, String url, String requestBody, String bodyContentType, String acceptType, Listener<Object> listener, ErrorListener errorListener, RestConfig<E> config, Class<T> responseObjectClass, Class<T> errorResponseObjectClass,
			boolean responseHeaderHandlingRequired, boolean shouldRetry) {
		super(method, url, requestBody, listener, errorListener);

		setRetryPolicy(new VolleyRetryPolicy(config.getConnectionTimeOut(), shouldRetry));
		this.config = config;
		this.requestHeaders = config.getRequestHeadersProvider().getHeaders();
		this.responseHeadersHandler = config.getResponseHeadersHandler();
		this.responseHeaderHandlingRequired = responseHeaderHandlingRequired;
		this.bodyContentType = bodyContentType;
		this.acceptType = acceptType;
		this.responseObjectClass = responseObjectClass;
		this.errorResponseObjectClass = errorResponseObjectClass;
	}

	@Override
	protected Response<Object> parseNetworkResponse(NetworkResponse response) {
		String json = null;
		try {
			handleResponseHeaders(response);
			json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
			return handleAsObjectJson(response, json);
		} catch (UnsupportedEncodingException e) {
			return Response.error(new ParseError(response));
		} catch (JsonParseException e) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, e.toString());
			}
			return handleAsErrorJson(response, json);
		}
	}
	
	@Override
	protected VolleyError parseNetworkError(VolleyError volleyError) {
		handleResponseHeaders(volleyError.networkResponse);
		return super.parseNetworkError(volleyError);
	}

	private void handleResponseHeaders(NetworkResponse response) {
		if (response != null && responseHeaderHandlingRequired) {
			responseHeadersHandler.handleHeaders(response.headers);
		}
	}

	private Response<Object> handleAsObjectJson(NetworkResponse response, String json) throws JsonParseException {
		if ((json == null) || json.trim().isEmpty()) {
			return Response.error(new VolleyNoContentError(response));
		}
		Object parsedJsonObject = config.getJsonParser().parseObject(json, responseObjectClass);
		return Response.success(parsedJsonObject, HttpHeaderParser.parseCacheHeaders(response));
	}

	private Response<Object> handleAsErrorJson(NetworkResponse response, String json) {
		try {
			Object errorObject = config.getJsonParser().parseObject(json, errorResponseObjectClass);
			return Response.success(errorObject, HttpHeaderParser.parseCacheHeaders(response));
		} catch (JsonParseException e1) {
			return Response.error(new ParseError(response));
		}
	}

	@Override
	public String getBodyContentType() {
		return bodyContentType + "; charset=" + getParamsEncoding();
	}

	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		Map<String, String> headers = (requestHeaders == null) ? new HashMap<String, String>() : requestHeaders;
		if (null != acceptType) {
			headers.put("Accept", acceptType);
		}
		return headers;
	}

}
