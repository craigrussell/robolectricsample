package com.tescobank.mobile.rest.processor;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

import com.tescobank.mobile.rest.RestEncodable;

/**
 * Rest request body builder
 */
public final class RestRequestBodyBuilder implements RestEncodable {
	
	private RestRequestBodyBuilder() {
		super();
	}

	/**
	 * Returns the request body to be used for the specified Rest request
	 * 
	 * @param restRequest the restRequest
	 * 
	 * @return the request body
	 */
	public static String buildRequestBody(Map<String, Object> bodyParams) {
		
		if (bodyParams == null) {
			return null;
		}
		
		StringBuilder encodedParams = new StringBuilder();
		
		for (Iterator<Map.Entry<String, Object>> iterator = bodyParams.entrySet().iterator(); iterator.hasNext();) {
			
			Map.Entry<String, Object> entry = iterator.next();
			
			addParam(encodedParams, entry);
			
			addAmpersand(encodedParams, iterator);
		}
		
		return encodedParams.toString();
	}
	
	private static void addParam(StringBuilder encodedParams, Map.Entry<String, Object> entry) {
		try {
			
			encodedParams.append(URLEncoder.encode(entry.getKey(), ENCODING));
			encodedParams.append("=");
	
			if (entry.getValue() != null) {
				encodedParams.append(URLEncoder.encode(String.valueOf(entry.getValue()), ENCODING));
			}
			
		} catch (UnsupportedEncodingException e) {
			throw new RestException("Encoding not supported: " + ENCODING, e);
		}
	}
	
	private static void addAmpersand(StringBuilder encodedParams, Iterator<Map.Entry<String, Object>> iterator) {
		if (iterator.hasNext()) {
			encodedParams.append("&");
		}
	}
	
	private static class RestException extends RuntimeException {

		private static final long serialVersionUID = -6292141299509003527L;
		
		public RestException(String message, Throwable throwable) {
			super(message, throwable);
		}
	}
}
