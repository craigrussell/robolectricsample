package com.tescobank.mobile.rest.processor.volley;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.android.volley.Request;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Mapping from {@link RestHttpMethod} to {@link Request.Method}
 */
public class VolleyRestHttpMethodMapper {
	
	/** Mapping from {@link RestHttpMethod} to {@link Request.Method} */
	private static final Map<RestHttpMethod, Integer> HTTP_METHODS;
	
	static {
		
		Map<RestHttpMethod, Integer> httpMethods = new HashMap<RestHttpMethod, Integer>();
		httpMethods.put(RestHttpMethod.GET, Request.Method.GET);
		httpMethods.put(RestHttpMethod.PUT, Request.Method.PUT);
		httpMethods.put(RestHttpMethod.POST, Request.Method.POST);
		httpMethods.put(RestHttpMethod.DELETE, Request.Method.DELETE);
		
		HTTP_METHODS = Collections.unmodifiableMap(httpMethods);
	}
	
	/**
	 * Constructor
	 */
	public VolleyRestHttpMethodMapper() {
		super();
	}
	
	/**
	 * Returns the {@link Request.Method} for the specified {@link RestHttpMethod}
	 * 
	 * @param restHttpMethod the {@link RestHttpMethod}
	 * 
	 * @return the corresponding {@link Request.Method}
	 */
	public int toVolleyHttpMethod(RestHttpMethod restHttpMethod) {
		return HTTP_METHODS.get(restHttpMethod);
	}
}
