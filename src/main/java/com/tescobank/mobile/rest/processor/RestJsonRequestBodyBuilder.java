package com.tescobank.mobile.rest.processor;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tescobank.mobile.rest.RestEncodable;

/**
 * Rest request body builder for JSON body requests
 */
public final class RestJsonRequestBodyBuilder implements RestEncodable {
	
	private RestJsonRequestBodyBuilder() {
		super();
	}

	public static <T> String buildJsonRequestBody(List<T> jsonList, Class <?> concreteClass, Class<?> mixInClass) {
		if ((jsonList == null) || jsonList.isEmpty()) {
			return null;
		}
		
		return buildJsonRequestBodyForObjectOrList(jsonList, concreteClass, mixInClass);
	}
	
	public static <T> String buildJsonRequestBody(T jsonObject, Class <?> concreteClass, Class<?> mixInClass) {
		return buildJsonRequestBodyForObjectOrList(jsonObject, concreteClass, mixInClass);
	}
	
	private static <T> String buildJsonRequestBodyForObjectOrList(T jsonObject, Class <?> concreteClass, Class<?> mixInClass) {

		if (jsonObject == null) {
			return null;
		}
		
		final OutputStream out = new ByteArrayOutputStream();
		final ObjectMapper mapper = new ObjectMapper();
		try {
			if(mixInClass != null) {
				mapper.addMixInAnnotations(concreteClass, mixInClass);
			}	
			mapper.writeValue(out, jsonObject);
			return new String(((ByteArrayOutputStream) out).toByteArray());
		} catch (Exception e) {
			throw new RestException("Failed to build JSON request", e);
		} finally {
			closeStream(out);
		}
	}
	
	private static void closeStream(Closeable stream) {
		try {
			if (stream != null) {
				stream.close();
			}
		} catch (IOException ioe) {
			throw new RestException("Failed to close JSON request Output Buffer Stream", ioe);
		}
	}
	
	private static class RestException extends RuntimeException {
		
		private static final long serialVersionUID = 3100640965512321118L;
		
		public RestException(String message, Throwable throwable) {
			super(message, throwable);
		}
	}
}
