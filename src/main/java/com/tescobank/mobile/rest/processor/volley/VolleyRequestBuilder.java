package com.tescobank.mobile.rest.processor.volley;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.rest.processor.RestConfig;
import com.tescobank.mobile.rest.processor.RestRequestUrlBuilder;
import com.tescobank.mobile.rest.request.RestByteArrayRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;
import com.tescobank.mobile.rest.request.RestRequest;

/**
 * Volley request builder
 * 
 * @param <E>
 *            the object type of the error response
 * @param <A>
 */
public class VolleyRequestBuilder<E, A> {

	private static final String TAG = VolleyRequestBuilder.class.getSimpleName();
	/** The Rest configuration */
	private RestConfig<E> config;

	/** The mapper from {@link RestHttpMethod} to {@link Request.Method} */
	private VolleyRestHttpMethodMapper volleyRestHttpMethodMapper;

	/** The Rest request URL builder */
	private RestRequestUrlBuilder restRequestUrlBuilder;

	/**
	 * Constructor
	 * 
	 * @param config
	 *            the Rest configuration
	 */
	public VolleyRequestBuilder(RestConfig<E> config) {
		super();
		this.config = config;
		volleyRestHttpMethodMapper = new VolleyRestHttpMethodMapper();
		restRequestUrlBuilder = new RestRequestUrlBuilder(config.getDefaultBaseUrl());
	}

	/**
	 * Creates and returns a Volley request for the specified Rest request
	 * 
	 * @param restRequest
	 *            the restRequest
	 * 
	 * @return the Volley request
	 */
	public <T> Request<Object> buildVolleyRequest(RestRequest<T, E> restRequest) {

		int method = volleyRestHttpMethodMapper.toVolleyHttpMethod(restRequest.getHttpMethod());
		String url = restRequestUrlBuilder.buildRequestUrl(restRequest);
		String requestBody = restRequest.getBody();
		Listener<Object> listener = createListener(restRequest, method, url, requestBody);
		ErrorListener errorListener = createErrorListener(restRequest, method, url, requestBody);
		return createVolleyRequest(restRequest, method, url, requestBody, listener, errorListener);
	}

	private <T> Listener<Object> createListener(final RestRequest<T, E> restRequest, final int method, final String url, final String requestBody) {
		return new Listener<Object>() {

			@Override
			public void onResponse(Object json) {

				// XXX For helping debug ui tests
				if (BuildConfig.DEBUG) {
					try {
						Log.d(TAG, "onResponse: " + method + ":" + url + ":" + requestBody + ", response=" + new ObjectMapper().writeValueAsString(json));
					} catch (JsonProcessingException e) {
						Log.e(TAG, e.getMessage());
					}
				}
				handleResponse(restRequest, json);
			}
		};
	}
	
	private <T> ErrorListener createErrorListener(final RestRequest<T, E> restRequest, final int method, final String url, final String requestBody) {
		return new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "onErrorResponse: " + method + ":" + url + ":" + requestBody + ", HTTP status=" + getHttpStatusCode(volleyError) + ", Connection timeout=" + isTimeoutError(volleyError));
				}
				handleVolleyErrorResponse(restRequest, volleyError);
			}
		};
	}

	@SuppressWarnings("unchecked")
	private <T> void handleResponse(RestRequest<T, E> restRequest, Object json) {
		if (isErrorResponse(json)) {
			handleErrorResponse(restRequest, json, 0);
		} else {
			restRequest.onResponse((T) json);
		}
	}

	@SuppressWarnings("unchecked")
	private <T> void handleErrorResponse(RestRequest<T, E> restRequest, Object json, int statusCode) {
		try {
			restRequest.onErrorResponse((E) json);
		} catch (Exception e) {
			restRequest.onErrorResponse(config.getErrorResponseBuilder().buildErrorResponse(statusCode));
		}
	}
	
	private <T> void handleVolleyErrorResponse(RestRequest<T, E> restRequest, VolleyError volleyError) {
		int status = getHttpStatusCode(volleyError);
		try {
			String responseBody = new String(volleyError.networkResponse.data, HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));
			Object json = config.getJsonParser().parseObject(responseBody, config.getErrorResponseBuilder().errorResponseClass());
			handleErrorResponse(restRequest, json, status);
		} catch (Exception e) {
			restRequest.onErrorResponse(config.getErrorResponseBuilder().buildErrorResponse(status));
		} 
	}

	private boolean isErrorResponse(Object json) {
		Class<E> errorClass = config.getErrorResponseBuilder().errorResponseClass();
		return errorClass.isInstance(json);
	}

	private int getHttpStatusCode(VolleyError error) {
		return ((error != null) && (error.networkResponse != null)) ? error.networkResponse.statusCode : 0;
	}

	private boolean isTimeoutError(VolleyError error) {
		return error instanceof TimeoutError;
	}

	@SuppressWarnings("unchecked")
	private <T> Request<Object> createVolleyRequest(RestRequest<T, E> restRequest, int method, String url, String requestBody, Listener<Object> listener, ErrorListener errorListener) {
		if (restRequest instanceof RestByteArrayRequest) {
			return new VolleyByteArrayRequest<E>(method, url, restRequest.getAcceptedContentType(), listener, errorListener, config, (Class<E>) restRequest.getErrorResponseObjectClass(), restRequest.isResponseHeaderHandlingRequired(), restRequest.shouldRetry());
		}
		return new VolleyJsonStringRequest<T, E>(method, url, requestBody, restRequest.getBodyContentType(), restRequest.getAcceptedContentType(), listener, errorListener, config, restRequest.getResponseObjectClass(), (Class<T>) restRequest.getErrorResponseObjectClass(), restRequest.isResponseHeaderHandlingRequired(),
					restRequest.shouldRetry());
	}
}
