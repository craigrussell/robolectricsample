package com.tescobank.mobile.rest.processor.volley;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.HurlStack;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.trustmanager.TescoSSLSocketFactory;

public class TescoHurlStack extends HurlStack {

    private static final String TAG = TescoHurlStack.class.getName();
    private final static String CONNECTION_PROTOCOL ="https";
 
    @Override
    protected HttpURLConnection createConnection(URL url) throws IOException {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "createConnection(" + url + ")");
        }
        HttpURLConnection urlConnection = super.createConnection(url);
        if (CONNECTION_PROTOCOL.equals(url.getProtocol())) {
            ((HttpsURLConnection) urlConnection).setSSLSocketFactory(new TescoSSLSocketFactory().getTescoSSLSocketFactory());
        }
        return urlConnection;
    }

	@Override
	protected void setConnectionParametersForRequest(HttpURLConnection connection, Request<?> request) throws IOException, AuthFailureError {
		try {
			VolleyRetryPolicy policy = VolleyRetryPolicy.class.cast(request.getRetryPolicy());
			if (!policy.shouldRetry()) {
				protectConnectionFromDuplicateRequests(connection);
			}
		} catch (ClassCastException e) {
			// do nothing
		} finally {
			super.setConnectionParametersForRequest(connection, request);
		}
	}

	private void protectConnectionFromDuplicateRequests(HttpURLConnection connection) {
		// chunkingStreamingMode stops potential duplicate requests on slow keep alive connections.
		// Another alternative would be to mark the connection as Connection: close however this causes vpn issues on android <4.2.1
		connection.setChunkedStreamingMode(0);
	}
}