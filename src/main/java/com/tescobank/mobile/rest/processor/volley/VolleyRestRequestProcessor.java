package com.tescobank.mobile.rest.processor.volley;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.NoCache;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.network.NetworkStatusChecker;
import com.tescobank.mobile.rest.processor.RestConfig;
import com.tescobank.mobile.rest.processor.RestRequestProcessor;
import com.tescobank.mobile.rest.request.RestRequest;

/**
 * Volley-specific Rest request processor implementation
 * 
 * @param <E>
 *            the object type of the error response
 * @param <A>
 */
public class VolleyRestRequestProcessor<E, A> implements RestRequestProcessor<E> {

	private static final String TAG = VolleyRestRequestProcessor.class.getSimpleName();

	private RequestQueue requestQueue;

	private VolleyRequestBuilder<E, A> volleyRequestBuilder;

	private NetworkStatusChecker networkStatusChecker;

	private RestConfig<E> config;

	private Map<Class<? extends Activity>, Class<?>> lastRequestTypeForRequesterType;

	/**
	 * Constructor
	 * 
	 * @param config
	 *            the Rest configuration
	 */
	public VolleyRestRequestProcessor(RestConfig<E> config) {
		super();
		this.config = config;

		requestQueue = new RequestQueue(new NoCache(), new BasicNetwork(new TescoHurlStack()));
		requestQueue.start();

		volleyRequestBuilder = new VolleyRequestBuilder<E, A>(config);
		networkStatusChecker = new NetworkStatusChecker();
		lastRequestTypeForRequesterType = new HashMap<Class<? extends Activity>, Class<?>>();
	}

	@Override
	public void processRequest(RestRequest<?, E> restRequest) {
		processRequest(restRequest, null);
	}

	@Override
	public void processRequest(RestRequest<?, E> restRequest, Class<? extends Activity> requesterType) {

		if (requesterType != null) {
			lastRequestTypeForRequesterType.put(requesterType, restRequest.getClass());
		}

		// Don't add to the queue if we don't have network.
		if (networkStatusChecker.isAvailable(config.getContext())) {

			// Associate the request with this request processor instance to support request cancellation
			Request<Object> volleyRequest = volleyRequestBuilder.buildVolleyRequest(restRequest);
			volleyRequest.setTag(this);
			requestQueue.add(volleyRequest);

		} else {

			if (BuildConfig.DEBUG) {
				Log.d(TAG, "No network connection when adding request to queue");
			}
			restRequest.onErrorResponse(config.getErrorResponseBuilder().buildErrorResponse(InAppError.NoInternetConnection));
		}
	}

	@Override
	public void cancelAllRequests() {
		requestQueue.cancelAll(this);
	}

	@Override
	public Class<?> getLastRequestTypeForRequesterType(Class<? extends Activity> requesterType) {
		return lastRequestTypeForRequesterType.get(requesterType);
	}

	@Override
	public void clearLastRequestTypeForRequesterType(Class<? extends Activity> requesterType) {
		lastRequestTypeForRequesterType.remove(requesterType);
	}
}
