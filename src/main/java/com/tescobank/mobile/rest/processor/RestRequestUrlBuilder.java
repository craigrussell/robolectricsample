package com.tescobank.mobile.rest.processor;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import com.tescobank.mobile.rest.request.RestRequest;

/**
 * Rest request URL builder
 */
public class RestRequestUrlBuilder {
	
	/** Empty string */
	private static final String EMPTY_STRING = "";
	
	/** The default base URL */
	private String defaultBaseUrl;

	/**
	 * Constructor
	 * 
	 * @param defaultBaseUrl the default base URL
	 */
	public RestRequestUrlBuilder(String defaultBaseUrl) {
		super();
		
		this.defaultBaseUrl = defaultBaseUrl;
	}
	
	/**
	 * Returns the request URL to be used for the specified Rest request
	 * 
	 * @param restRequest the restRequest
	 * 
	 * @return the URL
	 */
	public String buildRequestUrl(RestRequest<?, ?> restRequest) {
		
		StringBuilder requestUrl = new StringBuilder();
		
		appendBaseUrl(requestUrl, restRequest);
		appendResourceIdentifier(requestUrl, restRequest);
		
		return requestUrl.toString();
	}
	
	private void appendBaseUrl(StringBuilder requestUrl, RestRequest<?, ?> restRequest) {
		String requestSpecificBaseUrl = restRequest.getBaseUrl();
		if (requestSpecificBaseUrl != null) {
			requestUrl.append(requestSpecificBaseUrl);
		} else if (defaultBaseUrl != null) {
			requestUrl.append(defaultBaseUrl);
		}
	}
	
	private void appendResourceIdentifier(StringBuilder requestUrl, RestRequest<?, ?> restRequest) {
		String resourceIdentifier = getResourceIdentifier(restRequest.getResourceIdentifier(), restRequest.getUrlParams());
		if (resourceIdentifier != null) {
			requestUrl.append(resourceIdentifier);
		}
	}
	
	/**
	 * Returns the specified parameterized resource identifier with replaceable parameters replaced by the specified parameter values
	 * 
	 * For example given "/:a/:b/:a" and {a => "AAA", b => "BBB"} should return "/AAA/BBB/AAA"
	 * 
	 * @param parameterizedResourceIdentifier the parameterized resource identifier
	 * @param replaceableParams the replaceable parameters
	 * 
	 * @return the resulting resource identifier
	 */
	private String getResourceIdentifier(String parameterizedResourceIdentifier, Map<String, Object> replaceableParams) {
		
		String resourceIdentifier = parameterizedResourceIdentifier;
		
		if (isStringEmpty(resourceIdentifier) || isMapEmpty(replaceableParams)) {
			return resourceIdentifier;
		}
		
		StringBuilder stringBuilder = new StringBuilder(parameterizedResourceIdentifier);
		for (String paramName : replaceableParams.keySet()) {
			addParam(replaceableParams, stringBuilder, paramName);
		}
		
		return stringBuilder.toString();
	}
	
	private boolean isMapEmpty(Map<String, Object> replaceableParams) {
		return (replaceableParams == null) || replaceableParams.isEmpty();
	}
	
	private boolean isStringEmpty(String string) {
		return (string == null) || string.isEmpty();
	}
	
	private void addParam(Map<String, Object> replaceableParams, StringBuilder stringBuilder, String paramName) {
		Object paramValue = replaceableParams.get(paramName);
		String paramValueAsString = (paramValue == null) ? EMPTY_STRING : paramValue.toString();
		
		try {
			replaceAll(stringBuilder, paramName, URLEncoder.encode(paramValueAsString, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// UTF-8 is supported, so this shouldn't happen
			throw new RuntimeException(e);
		}
	}
	
	private void replaceAll(StringBuilder stringBuilder, String str, String replacement) {
		
		if (isStringBuilderEmpty(stringBuilder) || isStringEmpty(str)) {
			return;
		}
		
		int strIndex = stringBuilder.indexOf(str);
		int strLength = str.length();
		
		while (strIndex >= 0) {
			stringBuilder.replace(strIndex, strIndex + strLength, replacement);
			strIndex = stringBuilder.indexOf(str, strIndex + 1);
		}
	}
	
	private boolean isStringBuilderEmpty(StringBuilder stringBuilder) {
		return (stringBuilder == null) || (stringBuilder.length() <= 0);
	}
}
