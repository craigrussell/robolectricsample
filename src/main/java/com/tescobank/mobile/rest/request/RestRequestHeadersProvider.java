package com.tescobank.mobile.rest.request;

import java.util.Map;

/**
 * Provides the Rest request headers to be used
 */
public interface RestRequestHeadersProvider {

	/**
	 * @return the header names / values for the request
	 */
	Map<String, String> getHeaders();

	/**
	 * Resets headers
	 */
	void resetAuthorization();
}
