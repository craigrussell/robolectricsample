package com.tescobank.mobile.rest.request;

import java.util.Arrays;

/**
 * Rest response composed of a byte array
 */
public class RestByteArrayResponse {
	
	private byte[] bytes;
	
	public RestByteArrayResponse(byte[] bytes) {
		super();
		
		this.bytes = (bytes == null) ? null : Arrays.copyOf(bytes, bytes.length);
	}

	public byte[] getBytes() {
		return bytes;
	}
}
