package com.tescobank.mobile.rest.request;


/**
 * Abstract base class for Rest request returning a {@link RestByteArrayResponse}
 * 
 * @param E the error response type
 */
public abstract class RestByteArrayRequest<E> extends RestObjectRequest<RestByteArrayResponse, E> {
	
	public RestByteArrayRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.GET;
	}
	
	@Override
	public Class<RestByteArrayResponse> getResponseObjectClass() {
		return RestByteArrayResponse.class;
	}
	
	@Override
	public String getBody() {
		return null;
	}

	@Override
	public String getBodyContentType() {
		return null;
	}
	
	@Override
	public void setCompletionListener(RestRequestCompletionListener<E> listener) {
		// do nothing
	}
}
