package com.tescobank.mobile.rest.request;

import java.util.Map;

/**
 * Handles Rest response headers received
 */
public interface RestResponseHeadersHandler {
	
	/**
	 * @param headers the header names / values from the response
	 */
	void handleHeaders(Map<String, String> headers);
}
