package com.tescobank.mobile.rest.request;

import java.util.HashMap;
import java.util.Map;

import com.tescobank.mobile.rest.RestEncodable;

/**
 * Abstract base class for single-object Rest requests
 * 
 * @param <T>
 *            the object type of the response
 * @param <E>
 *            the object type of the error response
 */
public abstract class RestObjectRequest<T, E> implements RestRequest<T, E>, RestEncodable {

	@Override
	public String getBaseUrl() {
		return null;
	}

	@Override
	public Map<String, Object> getUrlParams() {
		return new HashMap<String, Object>();
	}

	@Override
	public String getAcceptedContentType() {
		return null;
	}

	@Override
	public boolean isResponseHeaderHandlingRequired() {
		return true;
	}

	/**
	 * The default is to retry.
	 */
	@Override
	public boolean shouldRetry() {
		return true;
	}

}
