package com.tescobank.mobile.rest.request;

import com.tescobank.mobile.api.error.ErrorResponseWrapper;

/**
 * Abstract base class for API request returning a {@link RestByteArrayResponse}
 */
public abstract class ByteArrayApiRequest extends RestByteArrayRequest<ErrorResponseWrapper> {
	
	public ByteArrayApiRequest() {
		super();
	}
	
	@Override
	public Class<ErrorResponseWrapper> getErrorResponseObjectClass() {
		return ErrorResponseWrapper.class;
	}
}
