package com.tescobank.mobile.rest.request;

/**
 * Supported Rest HTTP methods
 */
public enum RestHttpMethod {
	GET,
	PUT,
	POST,
	DELETE
}
