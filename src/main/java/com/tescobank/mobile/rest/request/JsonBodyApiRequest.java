package com.tescobank.mobile.rest.request;

import com.tescobank.mobile.json.JsonWriter;
import com.tescobank.mobile.json.JsonWriterFactory;

public abstract class JsonBodyApiRequest<REQ, RESPONSE> extends ApiRequest<RESPONSE> {

	private REQ bodyObject;

	public JsonBodyApiRequest(REQ bodyObject) {
		this.bodyObject = bodyObject;
	}

	@Override
	public final String getBody() {
		JsonWriter writer = JsonWriterFactory.createJsonWriter();
		return writer.writeValueAsString(bodyObject);
	}

	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}
}
