package com.tescobank.mobile.rest.request;

import java.util.Map;


/**
 * A Rest request
 * 
 * @param <T> the object type of the response
 * @param <E> the object type of the error response
 */
public interface RestRequest<T, E> {
	
	/**
	 * @return the Rest HTTP method for the request
	 */
	RestHttpMethod getHttpMethod();
	
	/**
	 * @return the base URL for the request
	 */
	String getBaseUrl();
	
	/**
	 * @return the resource identifier for the request
	 */
	String getResourceIdentifier();
	
	/**
	 * @return the parameter names / values for the request URL (names should include any prefix required, e.g. ":")
	 */
	Map<String, Object> getUrlParams();

	
	/**
	 * @return the response object class for the request
	 */
	Class<T> getResponseObjectClass();
	
	/**
	 * @return the error response object class for the request
	 */
	Class<E> getErrorResponseObjectClass();
	
	/**
	 * Called when a response is received
	 * 
	 * @param response the response
	 */
	void onResponse(T response);
	
	/**
	 * Called when an error response is received
	 * 
	 * @param errorResponse the errorResponse
	 */
	void onErrorResponse(E errorResponse);
	
	/**
	 * @return the parameter names / values for the request body
	 */
	String getBody();
	
	/**
	 * @return a json media type for the body content passed in the <code>Content-Type</code> header, e.g. "application/vnd.tescobank.payment-v1+json"
	 */
	String getBodyContentType();

	/**
	 * @return a json media type expected from the server passed in the <code>Accept</code> header, e.g. "application/vnd.tescobank.payment-v1+json"
	 */
	String getAcceptedContentType();

	/**
	 * @return whether response headers should be handled
	 */
	boolean isResponseHeaderHandlingRequired();
	
	/**
	 * @return whether the response should retry
	 */
	boolean shouldRetry();

	/**
	 * Sets the request completion listener which is notified when a request completes
	 * successfully or with an error. Optional, request may choose to do nothing.
	 */
	void setCompletionListener(RestRequestCompletionListener<E> listener);	
}
