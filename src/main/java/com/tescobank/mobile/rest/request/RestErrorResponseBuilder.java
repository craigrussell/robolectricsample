package com.tescobank.mobile.rest.request;

import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;

/**
 * Rest error response builder
 * 
 * @param <T> the object type of the error response
 */
public interface RestErrorResponseBuilder<T> {
	
	/**
	 * Builds and returns an error response for the specified HTTP status code, if any
	 * 
	 * @param httpStatusCode the HTTP status code, or 0 if there was no response
	 * 
	 * @return the error response
	 */
	T buildErrorResponse(int httpStatusCode);
	
	/**
	 * Builds and returns an error response for the specified in-app error
	 * 
	 * @param inAppError the in-app error
	 * 
	 * @return the error response
	 */
	T buildErrorResponse(InAppError inAppError);
	
	/**
	 * @return the class of the error response
	 */
	Class<T> errorResponseClass();
}
