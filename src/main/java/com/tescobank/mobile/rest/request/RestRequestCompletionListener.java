package com.tescobank.mobile.rest.request;


public interface RestRequestCompletionListener<E> {

	void notifyRequestSuceeded(RestRequest<?, E> request);

	void notifyRequestFailed(RestRequest<?, E> request, E errorResponse);
}
