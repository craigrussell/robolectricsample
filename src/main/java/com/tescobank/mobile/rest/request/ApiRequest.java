package com.tescobank.mobile.rest.request;

import java.util.HashMap;
import java.util.Map;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.rest.processor.RestRequestBodyBuilder;

/**
 * Abstract base class for API requests
 */
public abstract class ApiRequest<T> extends RestObjectRequest<T, ErrorResponseWrapper> {

	private static final String TAG = ApiRequest.class.getSimpleName();
	
	private RestRequestCompletionListener<ErrorResponseWrapper> listener;

	@Override
	public Class<ErrorResponseWrapper> getErrorResponseObjectClass() {
		return ErrorResponseWrapper.class;
	}

	/**
	 * @return the parameter names / values for the request body
	 */
	public Map<String, Object> getBodyParams() {
		return new HashMap<String, Object>();
	}

	@Override
	public String getBody() {
		return RestRequestBodyBuilder.buildRequestBody(getBodyParams());
	}

	@Override
	public String getBodyContentType() {
		return "application/x-www-form-urlencoded";
	}
	
	@Override
	public void setCompletionListener(RestRequestCompletionListener<ErrorResponseWrapper> listener) {
		this.listener = listener;
	}

	@Override
	public void onResponse(T response) {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Response received " + this.getClass().getSimpleName());
		}

		if (listener != null) {
			listener.notifyRequestSuceeded(this);
		}
	}

	@Override
	public void onErrorResponse(ErrorResponseWrapper errorResponse) {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Response error received " + this.getClass().getSimpleName());
		}

		if (listener != null) {
			listener.notifyRequestFailed(this, errorResponse);
		}
	}
}
