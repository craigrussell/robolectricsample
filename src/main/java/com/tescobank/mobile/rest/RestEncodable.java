package com.tescobank.mobile.rest;

/**
 * Character encoding for components of a Rest request
 */
public interface RestEncodable {
	
	/** The character encoding */
	String ENCODING = "UTF-8";
}
