package com.tescobank.mobile.encrypt;

public class CouldNotGenerateSecretKeyException extends Exception {

	private static final long serialVersionUID = -3248697287109229087L;
	
	public CouldNotGenerateSecretKeyException() {
		super();
	}
	
	public CouldNotGenerateSecretKeyException(Throwable throwable) {
		super(throwable);
	}
}
