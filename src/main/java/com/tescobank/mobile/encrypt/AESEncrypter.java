package com.tescobank.mobile.encrypt;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import android.util.Base64;
import android.util.Log;

public class AESEncrypter implements Encrypter {

	private static final String TAG = AESEncrypter.class.getSimpleName();
	private static final String ENCRYPTION_ALGORITHM = "AES/ECB/PKCS5Padding";

	private SecretKey key;

	public AESEncrypter(SecretKey key) {
		this.key = key;
	}

	@Override
	public String encryptString(String value) {

		String encoded = null;

		if (value != null) {
			try {
				Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
				cipher.init(Cipher.ENCRYPT_MODE, key);
				byte[] valueBytes = value.getBytes();
				byte[] encryptedBytes = cipher.doFinal(valueBytes);
				encoded = Base64.encodeToString(encryptedBytes, Base64.DEFAULT);
			} catch (Exception e) {
				Log.e(TAG, "Failed to encrypt string", e);
			}
		}
		return encoded;
	}

	@Override
	public String decryptString(String value) {

		String decrypted = null;
		if (value != null) {
			try {
				Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
				cipher.init(Cipher.DECRYPT_MODE, key);
				byte[] valueBytes = value.getBytes();
				byte[] decodedBytes = Base64.decode(valueBytes,Base64.DEFAULT);
				byte[] decryptedBytes = cipher.doFinal(decodedBytes);
				decrypted = new String(decryptedBytes);
			} catch (Exception e) {
				Log.e(TAG, "Failed to decrypt string", e);
			}
		}
		return decrypted;
	}
}
