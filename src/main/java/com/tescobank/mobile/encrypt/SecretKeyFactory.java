package com.tescobank.mobile.encrypt;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import android.content.Context;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;

public final class SecretKeyFactory {

	private static final String TAG = SecretKeyFactory.class.getSimpleName();
	private static final String ALGORITHM = "AES";
	private static final int OUTPUT_KEY_LENGTH = 256;
	
	private SecretKeyFactory() {
		super();
	}

	public static void clearSecretKey(Context context, String name) {
		SecretKeyProcessor tescoSecretKeyProcessor = SecretKeyProcessorFactory.getTescoBankSecretKeyProcessor(context, name);
		tescoSecretKeyProcessor.clear();
	}

	/**
	 * Generates a secret key Reference http://android-developers.blogspot.co.uk/ 2013/02/using-cryptography-to-store-credentials.html
	 * 
	 * @return unique key
	 * @throws CouldNotGenerateSecretKeyException
	 */
	public static SecretKey createSecretKey(Context context, String name) throws CouldNotGenerateSecretKeyException {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting createSecretKey");
		}

		SecureRandom secureRandom = new SecureRandom();
		KeyGenerator generator;
		SecretKey key;
		SecretKeyProcessor tescoSecretKeyProcessor = SecretKeyProcessorFactory.getTescoBankSecretKeyProcessor(context, name);

		// Read the Key from Disk and only if no key found is a key generated
		// and written to disk
		key = tescoSecretKeyProcessor.readKeyFromDisk();

		if (key == null) {
			try {
				generator = KeyGenerator.getInstance(ALGORITHM);
				generator.init(OUTPUT_KEY_LENGTH, secureRandom);
				key = generator.generateKey();
				tescoSecretKeyProcessor.writeKeyToDisk(key);
			} catch (NoSuchAlgorithmException e) {
				if (BuildConfig.DEBUG) {
					Log.e(TAG, "Error encountered when generating AES code", e);
				}
				throw new CouldNotGenerateSecretKeyException(e);
			}
		}

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Finished createSecretKey");
		}
		return key;
	}

}
