package com.tescobank.mobile.encrypt;

import android.content.Context;

public final class SecretKeyProcessorFactory {
	
	private SecretKeyProcessorFactory() {
		super();
	}

	/**
	 * Creates and returns an SecretKeyProcessor
	 * 
	 * @return an SecretKeyProcessor
	 */
	public static SecretKeyProcessor getTescoBankSecretKeyProcessor(Context context,String name) {
		return new TescoSecretKeyProcessor(context,name);
	}
}
