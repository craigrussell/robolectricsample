package com.tescobank.mobile.encrypt;

/**
 * Encrypts / decrypts data
 */
public interface Encrypter {
	
	/**
	 * Encrypts the specified string value
	 * 
	 * @param value the string value to encrypt
	 * 
	 * @return the result of the encryption
	 */
	String encryptString(String value);
	
	/**
	 * Decrypts the specified string value
	 * 
	 * @param value the string value to decrypt
	 * 
	 * @return the result of the decryption
	 */
	String decryptString(String value);
}
