package com.tescobank.mobile.encrypt;

import javax.crypto.SecretKey;


public interface SecretKeyProcessor {
	
	SecretKey readKeyFromDisk() throws CouldNotGenerateSecretKeyException;
	
	void writeKeyToDisk(SecretKey key) throws CouldNotGenerateSecretKeyException;
	
	void clear();
	
}
