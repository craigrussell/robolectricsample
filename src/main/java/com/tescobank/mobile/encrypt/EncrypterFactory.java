package com.tescobank.mobile.encrypt;

import javax.crypto.SecretKey;

/**
 * Factory for encrypters
 */
public final class EncrypterFactory {
	
	/**
	 * Private constructor
	 */
	private EncrypterFactory() {
		super();
	}
	
	/**
	 * Creates and returns an encrypter
	 * 
	 * @return an encrypter
	 * @throws EncrypterFaiedException 
	 */
	public static Encrypter createEncrypter(SecretKey key) {
		return new AESEncrypter(key);
	}
}
