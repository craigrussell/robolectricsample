package com.tescobank.mobile.encrypt;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import android.content.Context;
import android.provider.Settings.Secure;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.device.DeviceChecker;

public class TescoSecretKeyProcessor implements SecretKeyProcessor {

	private static final String TAG = TescoSecretKeyProcessor.class.getSimpleName();
	private static final String ALGORITHM = "AES";
	private Context context;
	private String name;

	public TescoSecretKeyProcessor(Context context, String name) {
		this.context = context;
		this.name = name;
	}

	public SecretKey readKeyFromDisk() throws CouldNotGenerateSecretKeyException {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting readKeyFromDisk");
		}

		File file = context.getFileStreamPath(name);
		int size = (int) file.length();

		// if the file doesnt exist or contains no data return
		if (noFile(file, size)) {
			return null;
		}

		SecretKey key = null;
		BufferedInputStream bis = null;

		try {

			bis = new BufferedInputStream(context.openFileInput(name));
			long length = bis.available();
			
			// Create the byte array to hold the data
			byte[] keyData = new byte[(int) length];

			// Read in the bytes
			int offset = 0;
			int numRead = 0;

			while (offset < keyData.length && (numRead = bis.read(keyData, offset, keyData.length - offset)) >= 0) {
				offset += numRead;
			}

			// Ensure all the bytes have been read in
			checkAllRead(offset, keyData);

			byte[] unObfuscatedKeyData = unObfuscateKey(keyData);
			key = new SecretKeySpec(unObfuscatedKeyData, ALGORITHM);
			
		} catch (Exception e) {
			// Do nothing, key may not exist for a new user
			throw new CouldNotGenerateSecretKeyException(e);
		} finally {
			closeStream(bis);
		}

		return key;
	}
	
	private boolean noFile(File file, int size) {
		return !file.exists() || (size == 0);
	}
	
	private void checkAllRead(int offset, byte[] keyData) throws CouldNotGenerateSecretKeyException {
		if (offset < keyData.length) {
			throw new CouldNotGenerateSecretKeyException();
		}
	}

	public void writeKeyToDisk(SecretKey key) throws CouldNotGenerateSecretKeyException {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting writeKeyToDisk");
		}

		FileOutputStream fos = null;
		try {
			fos = context.openFileOutput(name, Context.MODE_PRIVATE);
			byte[] keyData = key.getEncoded();
			byte[] obfuscatedKeyData = obfuscateKey(keyData);
			fos.write(obfuscatedKeyData);
		} catch (Exception e) {
			throw new CouldNotGenerateSecretKeyException(e);
		} finally {
			closeStream(fos);
		}
	}

	@Override
	public void clear() {
		context.deleteFile(name);
	}

	private byte[] unObfuscateKey(byte[] key) throws CouldNotGenerateSecretKeyException {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting unObfuscateKey");
		}

		byte[] paddedKeyOffset = generateOffsetKey(key.length);
		byte[] unObfuscatedKey = new byte[key.length];
		for (int i = 0; i < key.length; i++) {
			unObfuscatedKey[i] = (byte) (paddedKeyOffset[i] ^ key[i]);
		}

		return unObfuscatedKey;
	}

	private byte[] generateOffsetKey(int length) throws CouldNotGenerateSecretKeyException {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting generateOffsetKey");
		}

		String androidID = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
		// If in debug and we are using an emulator the androidID will be null
		// therefore create an android ID

		if (BuildConfig.DEBUG && androidID == null && DeviceChecker.isEmulator(context)) {
			androidID = UUID.randomUUID().toString();
		}
		if (androidID == null) {
			throw new CouldNotGenerateSecretKeyException();
		}

		byte[] uniqueIdData = androidID.getBytes();
		byte[] paddedUniqueIdData = new byte[length];

		for (int i = 0, pos = 0; i < paddedUniqueIdData.length; i++) {
			if (pos >= uniqueIdData.length) {
				pos = 0;
			}
			paddedUniqueIdData[i] = uniqueIdData[pos++];
		}

		return paddedUniqueIdData;
	}

	private byte[] obfuscateKey(byte[] key) throws CouldNotGenerateSecretKeyException {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting obfuscateKey");
		}

		byte[] paddedKeyOffset = generateOffsetKey(key.length);
		byte[] obfuscatedKey = new byte[key.length];
		for (int i = 0; i < key.length; i++) {
			obfuscatedKey[i] = (byte) (key[i] ^ paddedKeyOffset[i]);
		}

		return obfuscatedKey;
	}

	private void closeStream(Closeable stream) {
		try {
			if (stream != null) {
				stream.close();
			}
		} catch (IOException e) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Failed to close stream", e);
			}
		}
	}
}
