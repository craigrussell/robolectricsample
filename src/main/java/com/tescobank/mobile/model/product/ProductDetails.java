package com.tescobank.mobile.model.product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProductDetails extends Product  {

	private static final long serialVersionUID = -5714155248034193593L;
	
	private static final String SUBSCRIPTION_SUBSCRIBED = "subscribed";
	private static final String SUBSCRIPTION_AVAILABLE = "available";
	private static final String SUBSCRIPTION_LIMIT = "limit";
	public static final String PAYEMENT_RULE_CAN_BE_SOURCE = "canBeSource";
	public static final String PAYEMENT_RULE_CAN_BE_DESTINATION = "canBeDestination";

	private String paymentDue;
	private String nextPaymentDate;
	private String regularPayment;
	private String finalPaymentDate;
	private BigDecimal finalPaymentAmount;
	private String interestRate;
	private String accountId;
	private String amountSubscribed;
	private String amountAvailable;
	private String taxYearStart;
	private String taxYearEnd;
	private BigDecimal amountRemaining;
	private BigDecimal nextPaymentAmount;
	private BigDecimal availableBalance;
	private BigDecimal overdraftLimit;
	private String cardNumber;
	private BigDecimal balance;

	private BigDecimal availableCredit;
	private BigDecimal creditLimit;
	
	private BigDecimal minimumPayment;
	private List<String> cycleDates;
	private BigDecimal statementBalance;
	private String accountStatusCode;
	private Map<String,Boolean> paymentRules;
	private boolean limitedAccess;
	private Map<String,BigDecimal> subscription;
	private BigDecimal amountDue;
	private BigDecimal amountOfPaymentsGoingOut;
	
	/** Funding accounts (derived property) */
	private List<FundingAccount> fundingAccounts = new ArrayList<FundingAccount>();

	/**
	 * @return the subscription amount subscribed
	 */
	public BigDecimal getSubscriptionAmountSubscribed() {
		return (subscription == null) ? null : subscription.get(SUBSCRIPTION_SUBSCRIBED);
	}
	
	/**
	 * @return the subscription amount available
	 */
	public BigDecimal getSubscriptionAmountAvailable() {
		return (subscription == null) ? null : subscription.get(SUBSCRIPTION_AVAILABLE);
	}
	
	/**
	 * @return the subscription limit
	 */
	public BigDecimal getSubscriptionLimit() {
		return (subscription == null) ? null : subscription.get(SUBSCRIPTION_LIMIT);
	}
	
	/**
	 * @return true of the product can be the source of a transfer
	 */
	public boolean getCanBeSource() {
		return (paymentRules != null && Boolean.TRUE.equals(paymentRules.get(PAYEMENT_RULE_CAN_BE_SOURCE)));
	}
	
	/**
	 * @return true of the product can be the destination of a transfer
	 */
	public boolean getCanBeDestination() {
		return (paymentRules != null && Boolean.TRUE.equals(paymentRules.get(PAYEMENT_RULE_CAN_BE_DESTINATION)));
	}

	/**
	 * @return the finalPaymentAmount
	 */
	public BigDecimal getFinalPaymentAmount() {
		return finalPaymentAmount;
	}

	/**
	 * @param finalPaymentAmount the finalPaymentAmount to set
	 */
	public void setFinalPaymentAmount(BigDecimal finalPaymentAmount) {
		this.finalPaymentAmount = finalPaymentAmount;
	}

	/**
	 * @return the taxYearStart
	 */
	public String getTaxYearStart() {
		return taxYearStart;
	}

	/**
	 * @param taxYearStart the taxYearStart to set
	 */
	public void setTaxYearStart(String taxYearStart) {
		this.taxYearStart = taxYearStart;
	}

	public BigDecimal getAmountRemaining() {
		return amountRemaining;
	}

	public void setAmountRemaining(BigDecimal amountRemaining) {
		this.amountRemaining = amountRemaining;
	}

	public BigDecimal getNextPaymentAmount() {
		return nextPaymentAmount;
	}

	public void setNextPaymentAmount(BigDecimal nextPaymentAmount) {
		this.nextPaymentAmount = nextPaymentAmount;
	}

	public String getNextPaymentDate() {
		return nextPaymentDate;
	}

	public void setNextPaymentDate(String nextPaymentDate) {
		this.nextPaymentDate = nextPaymentDate;
	}

	public String getRegularPayment() {
		return regularPayment;
	}

	public void setRegularPayment(String regularPayment) {
		this.regularPayment = regularPayment;
	}

	public String getFinalPaymentDate() {
		return finalPaymentDate;
	}

	public void setFinalPaymentDate(String finalPaymentDate) {
		this.finalPaymentDate = finalPaymentDate;
	}

	public String getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(String interestRate) {
		this.interestRate = interestRate;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance; 
	}

	public BigDecimal getOverdraftLimit() {
		return overdraftLimit;
	}

	public void setOverdraftLimit(BigDecimal overdraftLimit) {
		this.overdraftLimit = overdraftLimit;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public Map<String, BigDecimal> getSubscription() {
		return subscription;
	}

	public void setSubscription(Map<String, BigDecimal> subscription) {
		this.subscription = subscription;
	}

	public String getAmountSubscribed() {
		return amountSubscribed;
	}

	public void setAmountSubscribed(String amountSubscribed) {
		this.amountSubscribed = amountSubscribed;
	}

	public String getAmountAvailable() {
		return amountAvailable;
	}

	public void setAmountAvailable(String amountAvailable) {
		this.amountAvailable = amountAvailable;
	}

	public String getTaxYearEnd() {
		return taxYearEnd;
	}

	public void setTaxYearEnd(String taxYearEnd) {
		this.taxYearEnd = taxYearEnd;
	}

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the availableCredit
	 */
	public BigDecimal getAvailableCredit() {
		return availableCredit;
	}

	/**
	 * @param availableCredit the availableCredit to set
	 */
	public void setAvailableCredit(BigDecimal availableCredit) {
		this.availableCredit = availableCredit;
	}

	/**
	 * @return the creditLimit
	 */
	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	/**
	 * @param creditLimit the creditLimit to set
	 */
	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	/**
	 * @return the paymentDue
	 */
	public String getPaymentDue() {
		return paymentDue;
	}

	/**
	 * @param paymentDue the paymentDue to set
	 */
	public void setPaymentDue(String paymentDue) {
		this.paymentDue = paymentDue;
	}

	/**
	 * @return the minimumPayment
	 */
	public BigDecimal getMinimumPayment() {
		return minimumPayment;
	}

	/**
	 * @param minimumPayment the minimumPayment to set
	 */
	public void setMinimumPayment(BigDecimal minimumPayment) {
        this.minimumPayment = minimumPayment;
	}

	/**
	 * @return the cycleDates
	 */
	public List<String> getCycleDates() {
		return cycleDates;
	}

	/**
	 * @param cycleDates the cycleDates to set
	 */
	public void setCycleDates(List<String> cycleDates) {
		this.cycleDates = cycleDates;
	}

	/**
	 * @return the statementBalance
	 */
	public BigDecimal getStatementBalance() {
		return statementBalance;
	}

	/**
	 * @param statementBalance the statementBalance to set
	 */
	public void setStatementBalance(BigDecimal statementBalance) {
		this.statementBalance = statementBalance;
	}

	/**
	 * @return the accountStatusCode
	 */
	public String getAccountStatusCode() {
		return accountStatusCode;
	}

	/**
	 * @param accountStatusCode the accountStatusCode to set
	 */
	public void setAccountStatusCode(String accountStatusCode) {
		this.accountStatusCode = accountStatusCode;
	}

	/**
	 * @return the paymentRules
	 */
	public Map<String, Boolean> getPaymentRules() {
		return paymentRules;
	}

	/**
	 * @param paymentRules the paymentRules to set
	 */
	public void setPaymentRules(Map<String, Boolean> paymentRules) {
		this.paymentRules = paymentRules;
	}

	/**
	 * @return the limitedAccess
	 */
	public boolean isLimitedAccess() {
		return limitedAccess;
	}

	/**
	 * @param limitedAccess the limitedAccess to set
	 */
	public void setLimitedAccess(boolean limitedAccess) {
		this.limitedAccess = limitedAccess;
	}
	
	/**
	 * @return funding accounts
	 */
	public List<FundingAccount> getFundingAccounts() {
		return fundingAccounts;
	}
	
	/**
	 * @param fundingAccounts funding accounts
	 */
	public void setFundingAccounts(List<FundingAccount> fundingAccounts) {
		this.fundingAccounts = (fundingAccounts == null) ? new ArrayList<FundingAccount>() : fundingAccounts;
	}

	/**
	 * @param amountDue amount due
	 */
	public BigDecimal getAmountDue() {
		return amountDue;
	}

	/**
	 * @param amountDue amount due
	 */
	public void setAmountDue(BigDecimal amountDue) {
		this.amountDue = amountDue;
	}

	/**
	 * @param amountOfPaymentsGoingOut amount of payments going out
	 */
	public BigDecimal getAmountOfPaymentsGoingOut() {
		return amountOfPaymentsGoingOut;
	}

	/**
	 * @param amountOfPaymentsGoingOut amount of payments going out
	 */
	public void setAmountOfPaymentsGoingOut(BigDecimal amountOfPaymentsGoingOut) {
		this.amountOfPaymentsGoingOut = amountOfPaymentsGoingOut;
	}

}
