package com.tescobank.mobile.model.product;

import java.io.Serializable;

import com.tescobank.mobile.format.DataFormatter;

/**
 * A funding account
 */
public class FundingAccount implements Serializable{
	
	private static final long serialVersionUID = 6552686754351954824L;

	private static final String SPACE = " ";
	
	private String name;
	
	private String accountNumber;
	
	private String sortCode;
	
	private transient DataFormatter dataFormatter;
	
	public FundingAccount() {
		super();
		
		dataFormatter = new DataFormatter();
	}
	
	/**
	 * @return the account identifier (derived), e.g. "12-34-56 12345678"
	 */
	public String getAccountIdentifier() {
		return dataFormatter.formatSortCode(sortCode) + SPACE + accountNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getSortCode() {
		return sortCode;
	}

	public void setSortCode(String sortCode) {
		this.sortCode = sortCode;
	}
}
