package com.tescobank.mobile.model.product;

/**
 * Supported product types
 */
public enum ProductType {
	
	PCA_PRODUCT("Personal Current Account", false),
	CC_PRODUCT("Credit Card", false),
	IAS_PRODUCT("Instant Access Savings", false),
	IS_PRODUCT("Internet Savings", false),
	IAI_PRODUCT("Instant Access ISA", true),
	JISA_PRODUCT("Junior ISA", true),
	FISA_PRODUCT("Fixed ISAs", true),		
	FJISA_PRODUCT("Fixed Junior ISAs", true),
	FRS_PRODUCT("Fixed Rate Saver", false),
	CCP_PRODUCT("Clubcard Plus", false),
	LOAN_PRODUCT("Loan", false);
	
	private String label;
	
	private boolean isISA;
	
	ProductType(String label, boolean isISA) {
		this.label = label;
		this.isISA = isISA;
	}
	
	public String getLabel() {
		return label;
	}
	
	public boolean isISA() {
		return isISA;
	}
	
	public static ProductType typeForLabel(String label) {
		
		for (ProductType type : ProductType.values()) {
			if(type.getLabel().equalsIgnoreCase(label)) {
				return type;
			}
		}
		return null;	
	}
}
