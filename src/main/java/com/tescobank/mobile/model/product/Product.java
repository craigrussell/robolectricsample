package com.tescobank.mobile.model.product;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Product implements Serializable {
	
	private static final long serialVersionUID = -7552614616445418374L;
	
	public static final String SAVINGS_CATEGORY = "savings";
	public static final String CREDITCARD_CATEGORY = "creditCard";
	public static final String PCA_CATEGORY = "personalCurrentAccount";
	public static final String LOAN_CATEGORY = "loan";

	private String productCategory;
	private String productType;
	private String productId;
    private String productName;
    private String productCode;
    private String accountNumber;
    private String sortCode;
    private String customerId;
    private boolean dirty;
    
	private String displayProductId;
	private String productTokenId;
	private String outageStatus;
    
    /**
     * @return true if the product is a savings product
     */
    public boolean isSavings() {
    	return SAVINGS_CATEGORY.equals(productCategory);
    }

    /**
     * @return true if the product is a credit card product
     */
    public boolean isCreditCard() {
    	return CREDITCARD_CATEGORY.equals(productCategory);
    }
    
    /**
     * @return true if the product is a personal current account product
     */
    public boolean isPersonalCurrentAccount() {
    	return PCA_CATEGORY.equals(productCategory);
    }
    
    /**
     * @return true if the product is a loan product
     */
    public boolean isLoan() {
    	return LOAN_CATEGORY.equals(productCategory);
    }
    
    /**
     * @return true if the product is a loan product
     */
    public boolean isBankingProduct() {
    	if(isLoan() || isSavings() || isPersonalCurrentAccount()) {
    		return true;
    	}
    	return false;
    }
    
	/**
	 * @return the productCategory
	 */
	public String getProductCategory() {
		return productCategory;
	}
	/**
	 * @param productCategory the productCategory to set
	 */
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}
	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}
	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}
	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	/**
	 * @return the accountNumber
	 */
    @JsonProperty(required=false)
	public String getAccountNumber() {
		return accountNumber;
	}
	
	/**
	 * @param accountNumber the accountNumber to set
	 */
    @JsonProperty(required=false)
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	/**
	 * @return the sortCode
	 */
    @JsonProperty(required=false)
	public String getSortCode() {
		return sortCode;
	}
	
	/**
	 * @param sortCode the sortCode to set
	 */
    @JsonProperty(required=false)
	public void setSortCode(String sortCode) {
		this.sortCode = sortCode;
	}
	
	/**
	 * @return the customerId
	 */
    @JsonProperty(required=false)
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId the customerId to set
	 */
    @JsonProperty(required=false)
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
    
    @Override
	public boolean equals(Object o) {
    	if (o instanceof Product) {
    		Product oProduct = (Product)o;
    		return productId.equals(oProduct.getProductId());
    	} else {
    		return false;
    	}
	}

	@Override
	public int hashCode() {
		return productId.hashCode();
	}

	public boolean isDirty() {
		return dirty;
	}

	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

    @JsonProperty(required=false)
	public String getDisplayProductId() {
		return displayProductId;
	}

    @JsonProperty(required=false)
	public void setDisplayProductId(String displayProductId) {
		this.displayProductId = displayProductId;
	}

	public String getProductTokenId() {
		return productTokenId;
	}

    @JsonProperty(required=false)
	public void setProductTokenId(String productTokenId) {
		this.productTokenId = productTokenId;
	}

    @JsonProperty(required=false)
	public String getOutageStatus() {
		return outageStatus;
	}

    @JsonProperty(required=false)
	public void setOutageStatus(String outageStatus) {
		this.outageStatus = outageStatus;
	}
}
