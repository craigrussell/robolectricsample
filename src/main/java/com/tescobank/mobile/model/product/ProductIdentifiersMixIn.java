package com.tescobank.mobile.model.product;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class ProductIdentifiersMixIn {	
	
	@JsonIgnore
	abstract String getProductName();

	@JsonIgnore
	abstract String getProductCode();

	@JsonIgnore
	abstract String getSortCode();
	
	@JsonIgnore
	abstract String getAccountNumber();
	
	@JsonIgnore
	abstract boolean isSavings();
	
	@JsonIgnore
	abstract boolean isCreditCard();
	
	@JsonIgnore
	abstract boolean isPersonalCurrentAccount();
	
	@JsonIgnore
	abstract boolean isLoan();
	
	@JsonIgnore
	abstract boolean couldHaveLinkedAccounts();
	
	@JsonIgnore
	abstract List<FundingAccount> getFundingAccounts();
	
	@JsonIgnore
	abstract boolean isDirty();
	
	@JsonIgnore
	abstract String getPaymentDue();
	
	@JsonIgnore
	abstract String getNextPaymentDate();
	
	@JsonIgnore
	abstract String getRegularPayment();

	@JsonIgnore
	abstract String getFinalPaymentDate();

	@JsonIgnore
	abstract String getInterestRate();

	@JsonIgnore
	abstract String getAccountId();
	
	@JsonIgnore
	abstract String getAmountSubscribed();
	
	@JsonIgnore
	abstract String getAmountAvailable();
	
	@JsonIgnore
	abstract String getTaxYearStart();

	@JsonIgnore
	abstract String getTaxYearEnd();

	@JsonIgnore
	abstract String getAmountRemaining();

	@JsonIgnore
	abstract String getNextPaymentAmount();

	@JsonIgnore
	abstract String getAavailableBalance();

	@JsonIgnore
	abstract String getOverdraftLimit();
	
	@JsonIgnore
	abstract String getFinalPaymentAmount();
	
	@JsonIgnore
	abstract String getAvailableBalance();
	
	@JsonIgnore
	abstract String getCardNumber();
	
	@JsonIgnore
	abstract String getBalance();
	
	@JsonIgnore
	abstract String getAvailableCredit();
	
	@JsonIgnore
	abstract String getCreditLimit();

	@JsonIgnore
	abstract String getMinimumPayment();

	@JsonIgnore
	abstract String getCycleDates();

	@JsonIgnore
	abstract String getStatementBalance();

	@JsonIgnore
	abstract String getAccountStatusCode();

	@JsonIgnore
	abstract String getPaymentRules();
	
	@JsonIgnore
	abstract String isLimitedAccess();

	@JsonIgnore
	abstract String getSubscription();
	
	@JsonIgnore
	abstract String getSubscriptionAmountSubscribed();

	@JsonIgnore
	abstract String getSubscriptionAmountAvailable();
	
	@JsonIgnore
	abstract String getSubscriptionLimit();
	
	@JsonIgnore
	abstract String getCanBeSource();

	@JsonIgnore
	abstract String getCanBeDestination();
	
	@JsonIgnore
	abstract boolean isBankingProduct();
	
	@JsonIgnore
	abstract String getDisplayProductId();
	
	@JsonIgnore
	abstract String getProductTokenId();
	
	@JsonIgnore
	abstract String getOutageStatus();
}
