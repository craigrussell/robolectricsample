package com.tescobank.mobile.model.movemoney;

import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.MakePayment;

import com.tescobank.mobile.api.payment.GetTransacteesForUserResponse.Transactee;
import com.tescobank.mobile.model.product.ProductDetails;

/**
 * A payment
 */
public class Payment extends MoneyMovement<ProductDetails, Transactee> {

	private static final long serialVersionUID = 5751935871423871285L;

	private String reference;
	
	public Payment(ProductDetails source, Transactee destination) {
		super(source, destination);
	}

	@Override
	public String getProcessCompleteAnalyticsEventName() {
		return MakePayment.getName();
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}
}
