package com.tescobank.mobile.model.movemoney;

import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.MoveMoneyIn;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.tescobank.mobile.api.payment.RetrieveFundingAccountsResponse.Account;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;

/**
 * A saving
 */
public class Saving extends MoneyMovement<Account, ProductDetails> {
	
	private static final long serialVersionUID = -7837373952542392669L;
	private int isaWarningPeriod;
	
	public Saving(Account source, ProductDetails destination) {
		super(source, destination);
		this.isaWarningPeriod = -1;
	}

	public void setISAWarningPeriod(int isaWarningPeriod) {
		this.isaWarningPeriod = isaWarningPeriod;
	}
	
	@Override
	public String getProcessCompleteAnalyticsEventName() {
		return MoveMoneyIn.getName();
	}
	
	public boolean isISADestination() {
		return isISA(getDestination());
	}

	public boolean isNextTaxYear() {
		return isNextTaxYear(getDestination());
	}
	
	public boolean isWithinISAYearEndWarningPeriod() {
		
		if(containsISAWarningPeriod()) {

			GregorianCalendar paymentDate = new GregorianCalendar();
			paymentDate.setTime(getDate());
			
			return paymentDate.compareTo(getISAYearEndWarningDate()) >= 0;
		}
		return false;
	}
	
	public Calendar getISAYearEndWarningDate() {
		if(containsISAWarningPeriod()) {
			GregorianCalendar yearEnd = new GregorianCalendar();
			yearEnd.setTime(new DataFormatter().parseDate(getDestination().getTaxYearEnd()));
			
			GregorianCalendar limitDate = (GregorianCalendar) yearEnd.clone();
			limitDate.add(Calendar.DAY_OF_MONTH, isaWarningPeriod*-1);
			return limitDate;
		}
		return null;
	}
	
	public boolean isBreachingLimits(boolean nextTaxYear, BigDecimal nextTaxYearSubscriptionLimit) {
		return isBreachingLimits(getDestination(), nextTaxYear, nextTaxYearSubscriptionLimit);
	}
	
	private boolean containsISAWarningPeriod() {
		return isISADestination() && isaWarningPeriod >= 0;
	}
}
