package com.tescobank.mobile.model.movemoney;

import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.MakeTransfer;

import java.math.BigDecimal;

import com.tescobank.mobile.model.product.ProductDetails;

/**
 * A transfer
 */
public class Transfer extends MoneyMovement<ProductDetails, ProductDetails> {

	private static final long serialVersionUID = -7009927303954957912L;
	
	public Transfer(ProductDetails source, ProductDetails destination) {
		super(source, destination);
	}

	@Override
	public String getProcessCompleteAnalyticsEventName() {
		return MakeTransfer.getName();
	}

	public boolean isISADestination() {
		return isISA(getDestination());
	}

	public boolean isNextTaxYear() {
		return isNextTaxYear(getDestination());
	}
	
	public boolean isBreachingLimits(boolean nextTaxYear, BigDecimal nextTaxYearSubscriptionLimit) {
		return isBreachingLimits(getDestination(), nextTaxYear, nextTaxYearSubscriptionLimit);
	}
}
