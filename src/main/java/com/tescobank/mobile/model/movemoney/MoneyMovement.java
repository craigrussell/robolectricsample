package com.tescobank.mobile.model.movemoney;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.model.product.ProductType;

/**
 * Abstract base class for money movements
 * 
 * @param T
 *            destination type
 */
public abstract class MoneyMovement<S, D> implements Serializable {

	private static final long serialVersionUID = 7063706562626673004L;

	private S source;

	private D destination;

	private BigDecimal amount;

	private Date date;

	/**
	 * Constructor
	 */
	public MoneyMovement(S source, D destination) {
		super();
		this.source = source;
		this.destination = destination;
	}

	/**
	 * @return the process complete analytics events name
	 */
	public abstract String getProcessCompleteAnalyticsEventName();

	/**
	 * @return true if the money movement date is today
	 */
	public boolean isMoneyMovementDateToday() {
		Calendar dateCelendar = Calendar.getInstance();
		dateCelendar.setTime(date);
		Calendar nowCalendar = Calendar.getInstance();
		return (dateCelendar.get(Calendar.YEAR) == nowCalendar.get(Calendar.YEAR)) && (dateCelendar.get(Calendar.DAY_OF_YEAR) == nowCalendar.get(Calendar.DAY_OF_YEAR));
	}
	
	protected boolean isISA(ProductDetails product) {
		return ProductType.typeForLabel(product.getProductType()).isISA();
	}
	
	protected boolean isNextTaxYear(ProductDetails product) {
		if (product.getTaxYearEnd() == null) {
			return false;
		}

		return getDate().after(new DataFormatter().parseDate(product.getTaxYearEnd()));
	}
	
	protected boolean isBreachingLimits(ProductDetails product, boolean nextTaxYear, BigDecimal nextTaxYearSubscriptionLimit) {
		return isBreachingNextTaxYearLimit(nextTaxYear, nextTaxYearSubscriptionLimit) || isBreachingThisTaxYearLimit(product, nextTaxYear);
	}
	
	private boolean isBreachingNextTaxYearLimit(boolean nextTaxYear, BigDecimal nextTaxYearSubscriptionLimit) {
		return nextTaxYear && (nextTaxYearSubscriptionLimit != null) && (amount.doubleValue() > nextTaxYearSubscriptionLimit.doubleValue());
	}
	
	private boolean isBreachingThisTaxYearLimit(ProductDetails product, boolean nextTaxYear) {
		return !nextTaxYear && (product.getSubscriptionAmountAvailable() != null) && (amount.doubleValue() > product.getSubscriptionAmountAvailable().doubleValue());
	}

	public S getSource() {
		return source;
	}

	public D getDestination() {
		return destination;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
