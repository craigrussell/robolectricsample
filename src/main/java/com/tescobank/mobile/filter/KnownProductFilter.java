package com.tescobank.mobile.filter;

import java.util.ArrayList;
import java.util.List;

import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.model.product.ProductType;

public class KnownProductFilter<P extends Product> {

	private List<P> allProducts;
	private List<P> knownProducts;
	
	public KnownProductFilter(List<P> products) {
		this.allProducts = products;
		this.knownProducts = new ArrayList<P>();
		filterKnownProducts();
	}
	
	private void filterKnownProducts() {
		for(P product : allProducts) {
			if(isProductKnown(product)) {
				knownProducts.add(product);
			}
		}
	}
	
	private boolean isProductKnown(P product) {
		if(!isProductTypeKnown(product)) {
			return false;
		}
		return true;
	}
		
	private boolean isProductTypeKnown(P product) {
		return ProductType.typeForLabel(product.getProductType()) != null;
	}
	
	public List<P> getKnownProducts() {
		return knownProducts;
	}
}
