package com.tescobank.mobile.properties;

import java.io.InputStream;
import java.util.Properties;

import android.content.Context;
import android.content.res.AssetManager;

public class TescoApplicationPropertiesReader {
	
	private Context context;
	private Properties properties;
	private String fileName;
	
	public static enum ApplicationPropertyKeys {

		API_CONECTION_TIMEOUT_MS("apiConnectionTimeoutMS"),
		ARCOT_PROV_URL("arcotProvUrl"),
		PDF_API_BASE("pdfUrlBase"),
		THREE_D_SECURE_URL("threeDSecureUrl"),
		BASE_API_URL("baseAPIUrl"),
		BASE_DOCUMENT_URL("baseDocumentUrl"),
		CONTACTUS_URL("contactUsUrl"),
		PRE_LOGIN_FAQ_URL("preLoginFaqUrl"),
		POST_LOGIN_FAQ_URL("postLoginFaqUrl"),
		TERMS_AND_CONDITIONS_URL("termsAndConditionsUrl"),
		TERMS_AND_CONDITIONS_DOWNLOAD_URL("termsAndConditionsDownloadUrl"),
		LOCATION_UPDATE_MIN_TIME_MILLIS("location_update_min_time_millis"),
		LOCATION_UPDATE_DISTANCE_METERS_MILLIS("location_update_min_distance_meters"),
		USER_TIMEOUT_ATTEMPTS("user_timeout_attempts"),
		INAUTH_URL("in_auth_url"),
		INAUTH_ACCOUNT_ID("in_auth_account_id"),
		INAUTH_FILE_DOWNLOAD_DELAY("in_auth_file_download_delay"),
		TEST_DEPLOYMENT("test_deployment");

		private String name;

		ApplicationPropertyKeys(String name) {
			this.name = name;
		}

		public String getName() {
			if (name != null) {
				return name;
			} else {
				return this.name();
			}
		}
	};
	
	public TescoApplicationPropertiesReader(Context context, String fileName) throws Exception {
		this.context = context;
		this.properties = new Properties();
		this.fileName = fileName;
		loadApplicationPropertiesFile();
	}

	public Context getContext() {
		return context;
	}
	
	public Properties getProperties() {
		return properties;
	}
	
	public AssetManager getAssetManager() {
		return getContext().getAssets();
	}
	
	public InputStream getFile() throws Exception{
		return getAssetManager().open(fileName);
	}
	
	public void loadApplicationPropertiesFile() throws Exception{
		getProperties().load(getFile());		
	}
	
	public String getApplicationPropertiesValue(ApplicationPropertyKeys applicationPropertyKey) {	
		return getProperties().getProperty(applicationPropertyKey.getName());
	}

}
