package com.tescobank.mobile.persist;

import java.util.Set;

/**
 * Persists / retrieves data to / from the local device
 */
public interface Persister {

	/**
	 * Clears persisted data
	 */
	void clear();

	/**
	 * @return the persisted device ID, or null
	 */
	String getDeviceId();

	/**
	 * @return the persisted CSSO ID, or null
	 */
	String getCssoId();

	/**
	 * @return the persisted TB ID, or null
	 */
	String getTbId();

	/**
	 * @return true if using password rather than passcode
	 */
	boolean getIsPassword();

	/**
	 * @return the date/time (in milliseconds) that the last successfully-used OTP was generated
	 */
	long getLastSuccessfulOTPGeneratedAt();
	
	/**
	 * @return the number of times that an OTP SMS has been requested
	 */
	int getSmsRequestCount();
	
	
	/**
	 * @return the last date the user attempted to launch the app
	 */
	String getLastAccessedDate();

	/**
	 * @return the selected location preference, or null if none has been set
	 */
	UseLocationPreference getUseLocationPreference();

	/**
	 * @param deviceId
	 *            the device ID to persist
	 * @param cssoId
	 *            the CSSO ID
	 * @param tbId
	 *            the TB ID
	 * @param isPassword
	 *            true if using password rather than passcode
	 */
	void persistSensitiveData(String deviceId, String cssoId, String tbId, boolean isPassword);
	
	/**
	 * @param count
	 * 			number of times that SMS has been requested
	 */
	void persistSmsRequestCount(int count);

	/**
	 * @param isPassword
	 *            true if using password rather than passcode
	 */
	void persistIsPassword(boolean isPassword);

	/**
	 * @param locationPreference
	 *            the location preference or null if the user has not chosen yet.
	 */
	void persistUseLocation(UseLocationPreference locationPreference);

	/**
	 * @param lastSuccessfulOTPGeneratedAt
	 *            the date/time (in milliseconds) that the last successfully-used OTP was generated
	 */
	void persistLastSuccessfulOTPGeneratedAt(long lastSuccessfulOTPGeneratedAt);

	/**
	 * <ul>
	 * <li>{@link #YES} - use location services if available</li>
	 * <li>{@link #NO} -  never use location services</li>
	 * <li>A null value for this preference means no preference has been set yet. This is not really a valid state beyond the Welcome screen, but just in case, assume {@link #NO}.</li>
	 */
	static enum UseLocationPreference {
		YES, NO
	}

	/** 
	 * @param productId the product type as a {@link String}
	 * @return the sort order as a {@link String}
	 */
	String getTransactionSortOrderForProductId(String productId);

	/** 
	 * @param productId the product type as a {@link String}
	 * @return the sort order as a {@link String}
	 */
	String getRegularPaymentSortOrderForProductId(String productId);

	/**
	 * @param sortOrder the sort order as a {@link String}
	 * @param productId the product type as a {@link String}
	 */
	void persistTransactionSortOrderForProductId(String sortOrder, String productId);

	/**
	 * @param sortOrder the sort order as a {@link String}
	 * @param productId the product type as a {@link String}
	 */
	void persistRegularPaymentSortOrderForProductId(String sortOrder, String productId);
	
	/**
	 * @param time that first OTP SMS was requested
	 */
	void persistInitialSmsRequestTime(long time);
	
	/**
	 * @return time that first OTP SMS was requested
	 */
	long getInitialSmsRequestTime();
	
	/**
	 * @param date in DD-MM-YYYY format {@link String}
	 */
	void persistLastAccessedDate(String date);
	
	/**
	 * @return the InAuth sigfile update date
	 */
	String getInAuthSigfileUpdatedAt();
	
	/**
	 * @param date the InAuth sigfile update date
	 */
	void persistInAuthSigfileUpdatedAt(String date);
	
	/**
	 * @return the InAuth mwfile update date
	 */
	String getInAuthMwfileUpdatedAt();
	
	/**
	 * @param date the InAuth mwfile update date
	 */
	void persistInAuthMwfileUpdatedAt(String date);
	
	/**
	 * @return the last known 'rooted' value
	 */
	boolean getLastKnownRooted();
	
	/**
	 * @param rooted the last known 'rooted' value
	 */
	void persistLastKnownRooted(boolean rooted);

	void persistSubsequentLoginCount(int newCount);
	
	int getSubsequentLoginCount();
	
	/**
	 * @param whether the customer has enabled balance peek
	 */
	void persistIsBalancePeekEnabled(boolean enabled);

	/**
	 * @param the customer's balance peek enablement value
	 */
	boolean isBalancePeekEnabled();
	
	boolean hasBalancePeekEverBeenEnabled();

	void persistBalancePeekHasBeenEnabled();
	
	boolean hasSeenBalancePeekTutorial();
	
	void persistHasSeenBalancePeekTutorial();

	/**
	 * @param persists the string set of product identifiers for balance peek
	 */
	void persistBalancePeekProductIdentifiers(Set<String> productIdentifiers);

	/**
	 * @param the string set of product identifiers for balance peek
	 */
	Set<String> getBalancePeekProductIdentifiers();

	/**
	 * @param removes the current string set of product identifiers for Balance peek
	 */
	void removeBalancePeekProductIdentifiers();
}
