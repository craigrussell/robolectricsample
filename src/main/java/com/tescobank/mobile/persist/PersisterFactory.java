package com.tescobank.mobile.persist;

import android.content.Context;

import com.tescobank.mobile.encrypt.Encrypter;
import com.tescobank.mobile.persist.prefs.SharedPreferencesPersister;

/**
 * Factory for persisters
 */
public final class PersisterFactory {
	
	/**
	 * Private constructor
	 */
	private PersisterFactory() {
		super();
	}
	
	/**
	 * Creates and returns a persister
	 * 
	 * @param context the context
	 * @param encrypter the encrypter
	 * 
	 * @return a persister
	 */
	public static Persister createPersister(Context context, Encrypter encrypter) {
		return new SharedPreferencesPersister(context, encrypter);
	}
}
