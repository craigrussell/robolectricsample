package com.tescobank.mobile.persist.prefs;

import java.util.Set;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.tescobank.mobile.encode.HMACSHA256Encoder;
import com.tescobank.mobile.encrypt.Encrypter;
import com.tescobank.mobile.persist.Persister;

/**
 * Shared preferences-specific persister implementation
 */
public class SharedPreferencesPersister implements Persister {

	/** Preferences area name */
	private static final String PREF_AREA_NAME = "tescoBankPrefs";

	/** Preferences key name for device ID */
	private static final String PREF_KEY_DEVICE_ID = "deviceId";

	/** Preferences key name for CSSO ID */
	private static final String PREF_KEY_CSSO_ID = "cssoId";

	/** Preferences key name for TB ID */
	private static final String PREF_KEY_TB_ID = "tbId";

	/** Preferences key name for 'is password' */
	private static final String PREF_KEY_IS_PASSWORD = "isPassword";

	/** Preferences key name for 'last successful OTP generated at' */
	private static final String PREF_KEY_OTP_GENERATED_AT = "lastSuccessfulOTPGeneratedAt";

	/** Preferences key name for use location. */
	private static final String PREF_KEY_USE_LOCATION = "useLocation";

	/** Preferences key prefix for product type sort orders. */
	private static final String PREF_KEY_PRODUCT_TYPE_SORT_ORDER_TRANSACTIONS_PREFIX = "productTypeTransactionsSortOrder";
	
	/** Preferences key prefix for product type sort orders. */
	private static final String PREF_KEY_PRODUCT_TYPE_SORT_ORDER_REGULAR_PAYMENTS_PREFIX = "productTypeRegularPaymentsSortOrder";
	
	/** Preferences key prefix for number of times OTP SMS has been requested. */
	private static final String PREF_KEY_SMS_REQUEST_COUNT = "smsButtonCount";
	
	/** Preferences key prefix for the time that the first OTP SMS was requested */
	private static final String PREF_KEY_FIRST_SMS_REQUEST_TIME = "firstSmsRequestTime";

	private static final String PREF_KEY_LAST_ACCESSED_APP_DATE = "lastAccessedAppDate";
	
	/** Preferences key name for the InAuth sigfile update date */
	private static final String PREF_KEY_INAUTH_SIGFILE_UPDATED_AT = "inAuthSigfileUpdatedAt";
	
	/** Preferences key name for the InAuth mwfile update date */
	private static final String PREF_KEY_INAUTH_MWFILE_UPDATED_AT = "inAuthMwfileUpdatedAt";
	
	/** Preferences key name for the last known 'rooted' value */
	private static final String PREF_KEY_LAST_KNOWN_ROOTED = "lastKnownRooted";
	
	private static final String PREF_KEY_SUBSEQUENT_LOGIN_COUNT = "subsequentLoginCount";

	private static final String PREF_KEY_BALANCE_PEEK_ENABLED = "balancePeekEnabled";
	
	private static final String PREF_KEY_BALANCE_PEEK_EVER_ENABLED = "balancePeekEverEnabled";
	
	private static final String PREF_KEY_BALANCE_PEEK_TUTORIAL_SEEN = "balancePeekTutorialSeen";
	
	private static final String PREF_KEY_BALANCE_PEEK_PRODUCT_IDENTIFIERS = "balancePeekProductIdentifers";


	/** The context */
	private Context context;

	/** The encrypter */
	private Encrypter encrypter;

	/**
	 * Constructor
	 * 
	 * @param context
	 *            the context
	 * @param encrypter
	 *            the encrypter
	 */
	public SharedPreferencesPersister(Context context, Encrypter encrypter) {
		super();

		this.context = context;
		this.encrypter = encrypter;
	}

	@Override
	public void clear() {
		Editor editor = getPrivateSharedPreferences().edit();
		editor.clear();
		editor.commit();
	}

	@Override
	public String getDeviceId() {
		return getEncryptedStringFromPrivateSharedPreferences(PREF_KEY_DEVICE_ID);
	}

	@Override
	public String getCssoId() {
		return getEncryptedStringFromPrivateSharedPreferences(PREF_KEY_CSSO_ID);
	}

	@Override
	public String getTbId() {
		return getEncryptedStringFromPrivateSharedPreferences(PREF_KEY_TB_ID);
	}

	@Override
	public boolean getIsPassword() {
		return getBooleanFromPrivateSharedPreferences(PREF_KEY_IS_PASSWORD, true);
	}

	@Override
	public long getLastSuccessfulOTPGeneratedAt() {
		return getLongFromPrivateSharedPreferences(PREF_KEY_OTP_GENERATED_AT, 0);
	}

	@Override
	public String getTransactionSortOrderForProductId(String productType) {
		return getStringFromPrivateSharedPreferences(PREF_KEY_PRODUCT_TYPE_SORT_ORDER_TRANSACTIONS_PREFIX + HMACSHA256Encoder.encode(productType,getCssoId()));
	}

	@Override
	public String getRegularPaymentSortOrderForProductId(String productType) {
		return getStringFromPrivateSharedPreferences(PREF_KEY_PRODUCT_TYPE_SORT_ORDER_REGULAR_PAYMENTS_PREFIX + HMACSHA256Encoder.encode(productType,getCssoId()));
	}
	
	@Override
	public String getLastAccessedDate() {
		return getStringFromPrivateSharedPreferences(PREF_KEY_LAST_ACCESSED_APP_DATE);
	}

	@Override
	public void persistTransactionSortOrderForProductId(String sortOrder, String productType) {
		Editor editor = getPrivateSharedPreferences().edit();
		editor.putString(PREF_KEY_PRODUCT_TYPE_SORT_ORDER_TRANSACTIONS_PREFIX + HMACSHA256Encoder.encode(productType,getCssoId()), sortOrder);
		editor.commit();
	}

	@Override
	public void persistRegularPaymentSortOrderForProductId(String sortOrder, String productType) {
		Editor editor = getPrivateSharedPreferences().edit();
		editor.putString(PREF_KEY_PRODUCT_TYPE_SORT_ORDER_REGULAR_PAYMENTS_PREFIX + HMACSHA256Encoder.encode(productType,getCssoId()), sortOrder);
		editor.commit();
	}
	
	@Override
	public synchronized void persistSensitiveData(String deviceId, String cssoId, String tbId, boolean isPassword) {
		persistDeviceId(deviceId);
		persistCssoId(cssoId);
		persistTbId(tbId);
		persistIsPassword(isPassword);
	}
	
	private void persistDeviceId(String deviceId) {
		Editor editor = getPrivateSharedPreferences().edit();
		editor.putString(PREF_KEY_DEVICE_ID, encrypter.encryptString(deviceId)).commit();
	}
	
	private void persistCssoId(String cssoId) {
		Editor editor = getPrivateSharedPreferences().edit();
		editor.putString(PREF_KEY_CSSO_ID, encrypter.encryptString(cssoId)).commit();
	}
	
	private void persistTbId(String tbId) {
		Editor editor = getPrivateSharedPreferences().edit();
		editor.putString(PREF_KEY_TB_ID, encrypter.encryptString(tbId)).commit();
	}

	@Override
	public void persistIsPassword(boolean isPassword) {
		Editor editor = getPrivateSharedPreferences().edit();
		editor.putBoolean(PREF_KEY_IS_PASSWORD, isPassword).commit();
	}

	@Override
	public void persistLastSuccessfulOTPGeneratedAt(long lastSuccessfulOTPGeneratedAt) {
		Editor editor = getPrivateSharedPreferences().edit();
		editor.putLong(PREF_KEY_OTP_GENERATED_AT, lastSuccessfulOTPGeneratedAt);
		editor.commit();
	}

	@Override
	public UseLocationPreference getUseLocationPreference() {
		String pref = getStringFromPrivateSharedPreferences(PREF_KEY_USE_LOCATION);
		if (null == pref) {
			return null;
		}
		return UseLocationPreference.valueOf(pref);
	}

	@Override
	public void persistUseLocation(UseLocationPreference locationPreference) {
		String pref = null;
		if (null != locationPreference) {
			pref = locationPreference.toString();
		}

		Editor editor = getPrivateSharedPreferences().edit();
		editor.putString(PREF_KEY_USE_LOCATION, pref);
		editor.commit();
	}

	private String getEncryptedStringFromPrivateSharedPreferences(String key) {
		return encrypter.decryptString(getPrivateSharedPreferences().getString(key, null));
	}

	private String getStringFromPrivateSharedPreferences(String key) {
		return getPrivateSharedPreferences().getString(key, null);
	}

	private Boolean getBooleanFromPrivateSharedPreferences(String key, boolean defaultValue) {
		return getPrivateSharedPreferences().getBoolean(key, defaultValue);
	}

	private long getLongFromPrivateSharedPreferences(String key, long defaultValue) {
		return getPrivateSharedPreferences().getLong(key, defaultValue);
	}

	private SharedPreferences getPrivateSharedPreferences() {
		return context.getSharedPreferences(PREF_AREA_NAME, Context.MODE_PRIVATE);
	}

	@Override
	public void persistSmsRequestCount(int count) {
		Editor editor = getPrivateSharedPreferences().edit();
		editor.putInt(PREF_KEY_SMS_REQUEST_COUNT, count);
		editor.commit();
	}

	@Override
	public int getSmsRequestCount() {
		return getPrivateSharedPreferences().getInt(PREF_KEY_SMS_REQUEST_COUNT, 0);
	}

	@Override
	public void persistInitialSmsRequestTime(long time) {
		Editor editor = getPrivateSharedPreferences().edit();
		editor.putLong(PREF_KEY_FIRST_SMS_REQUEST_TIME, time);
		editor.commit();
	}

	@Override
	public long getInitialSmsRequestTime() {
		return getPrivateSharedPreferences().getLong(PREF_KEY_FIRST_SMS_REQUEST_TIME, 0);
	}
	
	@Override
	public void persistLastAccessedDate(String date) {
		Editor editor = getPrivateSharedPreferences().edit();
		editor.putString(PREF_KEY_LAST_ACCESSED_APP_DATE, date).commit();
	}
	
	@Override
	public String getInAuthSigfileUpdatedAt() {
		return getPrivateSharedPreferences().getString(PREF_KEY_INAUTH_SIGFILE_UPDATED_AT, null);
	}

	@Override
	public void persistInAuthSigfileUpdatedAt(String date) {
		getPrivateSharedPreferences().edit().putString(PREF_KEY_INAUTH_SIGFILE_UPDATED_AT, date).commit();
	}
	
	@Override
	public String getInAuthMwfileUpdatedAt() {
		return getPrivateSharedPreferences().getString(PREF_KEY_INAUTH_MWFILE_UPDATED_AT, null);
	}

	@Override
	public void persistInAuthMwfileUpdatedAt(String date) {
		getPrivateSharedPreferences().edit().putString(PREF_KEY_INAUTH_MWFILE_UPDATED_AT, date).commit();
	}

	@Override
	public boolean getLastKnownRooted() {
		return getPrivateSharedPreferences().getBoolean(PREF_KEY_LAST_KNOWN_ROOTED, false);
	}

	@Override
	public void persistLastKnownRooted(boolean rooted) {
		getPrivateSharedPreferences().edit().putBoolean(PREF_KEY_LAST_KNOWN_ROOTED, rooted).commit();
	}

	@Override
	public void persistSubsequentLoginCount(int newCount) {
		getPrivateSharedPreferences().edit().putInt(HMACSHA256Encoder.encode(PREF_KEY_SUBSEQUENT_LOGIN_COUNT, getCssoId()), newCount).commit();
	}
	
	public int getSubsequentLoginCount() {
		return getPrivateSharedPreferences().getInt(HMACSHA256Encoder.encode(PREF_KEY_SUBSEQUENT_LOGIN_COUNT, getCssoId()), 0);
	}
	
	@Override
	public void persistIsBalancePeekEnabled(boolean enabled) {
		Editor editor = getPrivateSharedPreferences().edit();
		editor.putBoolean(HMACSHA256Encoder.encode(PREF_KEY_BALANCE_PEEK_ENABLED, getCssoId()), enabled).commit();
	}

	@Override
	public boolean isBalancePeekEnabled() {
		return getPrivateSharedPreferences().getBoolean(HMACSHA256Encoder.encode(PREF_KEY_BALANCE_PEEK_ENABLED, getCssoId()), false);
	}	
	
	@Override
	public boolean hasBalancePeekEverBeenEnabled() {
		return getPrivateSharedPreferences().getBoolean(HMACSHA256Encoder.encode(PREF_KEY_BALANCE_PEEK_EVER_ENABLED, getCssoId()), false);
	}

	@Override
	public void persistBalancePeekHasBeenEnabled() {
		getPrivateSharedPreferences().edit().putBoolean(HMACSHA256Encoder.encode(PREF_KEY_BALANCE_PEEK_EVER_ENABLED, getCssoId()), true).commit();
	}
	
	@Override
	public void persistHasSeenBalancePeekTutorial() {
		getPrivateSharedPreferences().edit().putBoolean(HMACSHA256Encoder.encode(PREF_KEY_BALANCE_PEEK_TUTORIAL_SEEN, getCssoId()), true).commit();
	}
	
	@Override
	public boolean hasSeenBalancePeekTutorial() {
		return getPrivateSharedPreferences().getBoolean(HMACSHA256Encoder.encode(PREF_KEY_BALANCE_PEEK_TUTORIAL_SEEN, getCssoId()), false);
	}
	
	@Override
	public void persistBalancePeekProductIdentifiers(Set<String> productIdentifiers) {
		Editor editor = getPrivateSharedPreferences().edit();
		editor.putStringSet(PREF_KEY_BALANCE_PEEK_PRODUCT_IDENTIFIERS,  productIdentifiers).commit();
	}
	
	@Override
	public Set<String> getBalancePeekProductIdentifiers() {	
		return getPrivateSharedPreferences().getStringSet(PREF_KEY_BALANCE_PEEK_PRODUCT_IDENTIFIERS, null);
	}
	
	@Override
	public void removeBalancePeekProductIdentifiers() {
		Editor editor = getPrivateSharedPreferences().edit();
		editor.remove(PREF_KEY_BALANCE_PEEK_PRODUCT_IDENTIFIERS).commit();
	}
}
