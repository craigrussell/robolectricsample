package com.tescobank.mobile.download;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.Executors;

import javax.net.ssl.HttpsURLConnection;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.debug.keystoreadapter.ssl.DebugSSLHostnameValidator;
import com.tescobank.mobile.network.NetworkStatusChecker;
import com.tescobank.mobile.trustmanager.TescoSSLSocketFactory;
import com.tescobank.mobile.ui.TescoActivity;

public abstract class DownloadHelper<T> {

	private static final String TAG = DownloadHelper.class.getSimpleName();

	private TescoActivity context;
	private String expectedMediaType;
	private NetworkStatusChecker networkStatusChecker;
	private ErrorResponseWrapperBuilder errorBuilder;

	public DownloadHelper(TescoActivity context, String expectedMediaType) {
		this.context = context;
		this.expectedMediaType = expectedMediaType;
		this.networkStatusChecker = new NetworkStatusChecker();
		this.errorBuilder = new ErrorResponseWrapperBuilder(context);
	}

	public abstract void onResponse(T result);

	public abstract void onErrorResponse(ErrorResponseWrapper errorResponseWrapper);

	public abstract T convertInputStreamToType(InputStream inputStream);

	public void execute(final String url) {

		Executors.newSingleThreadExecutor().execute(new Runnable() {
			@Override
			public void run() {
				T result = doInBackground(url);
				handleResult(result);
			}
		});
	}

	private void handleResult(final T result) {
		context.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				onPostExecute(result);
			}
		});
	}

	private T doInBackground(String url) {

		if (!networkStatusChecker.isAvailable(context)) {
			return null;
		}

		return performDownload(url);
	}

	private T performDownload(String downloadUrl) {

		InputStream inputStream = null;
		try {
			URLConnection urlCon = DownloadHelper.getURLConnection(context,downloadUrl);
			if (!expectedMediaType.equals(urlCon.getContentType())) {
				return null;
			}
			inputStream = urlCon.getInputStream();
			return convertInputStreamToType(inputStream);

		} catch (Exception e) {
			handleException(downloadUrl, e);
		} finally {
			closeStream(inputStream);
		}

		return null;
	}

	public static URLConnection getURLConnection(TescoActivity context, String downloadUrl) throws IOException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		URL url = new URL(downloadUrl);
		URLConnection urlCon = url.openConnection();
		if (context.isTestDeployment()) {
			urlCon = DebugSSLHostnameValidator.ignoreHostCertsForDebug(urlCon);
		} else {
			HttpsURLConnection.class.cast(urlCon).setSSLSocketFactory(new TescoSSLSocketFactory().getTescoSSLSocketFactory());
		}
		return urlCon;
	}

	private void handleException(String downloadUrl, Exception e) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Error processing Url - " + downloadUrl, e);
		}
	}
	
	private void closeStream(Closeable inputStream) {
		try {
			if (inputStream != null) {
				inputStream.close();
			}
		} catch (IOException e) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Failed to close input stream", e);
			}
		}
	}

	private void onPostExecute(T result) {
		if (result == null) {
			if (!networkStatusChecker.isAvailable(context)) {
				onErrorResponse(errorBuilder.buildErrorResponse(InAppError.NoInternetConnection));
			} else {
				onErrorResponse(errorBuilder.buildErrorResponse(InAppError.HttpStatus200));
			}
		} else {
			onResponse(result);
		}
	}
}
