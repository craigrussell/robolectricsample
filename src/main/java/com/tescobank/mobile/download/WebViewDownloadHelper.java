package com.tescobank.mobile.download;

import java.io.InputStream;
import java.net.URLConnection;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.ui.TescoActivity;

public class WebViewDownloadHelper {

	private static final String TAG = WebViewDownloadHelper.class.getSimpleName();
	private TescoActivity context;

	public WebViewDownloadHelper(TescoActivity context) {
		this.context = context;
	}

	public InputStream performDownload(String downloadUrl) {
		try {
			URLConnection urlCon = DownloadHelper.getURLConnection(context, downloadUrl);
			return urlCon.getInputStream();
		} catch (Exception e) {
			handleException(downloadUrl, e);
		}
		return null;
	}

	private void handleException(String downloadUrl, Exception e) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Error processing Url - " + downloadUrl, e);
		}
	}
}