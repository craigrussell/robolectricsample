package com.tescobank.mobile.analytics;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Rounds amounts up to the nearest 10, 100, 1000, etc
 */
public class AmountRounder {
	
	private static final int ZERO = 0;
	private static final int ONE = 1;
	private static final int TEN = 10;

	/**
	 * @param amount the amount to be rounded
	 * 
	 * @return the result
	 */
	public long round(BigDecimal amount) {
		
		BigDecimal wholeValue = amount.setScale(ZERO, RoundingMode.HALF_DOWN);
		long longValue = wholeValue.longValue();
		int digits = Long.toString(longValue).length();
		int multiplier = (int)Math.pow(TEN, Math.max(digits - ONE, ONE));
		return (longValue/multiplier + ONE) * multiplier;
	}
}
