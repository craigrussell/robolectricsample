package com.tescobank.mobile.analytics;

public enum AnalyticsGroup {
	Alerts,
	ATMFinder,
	Calendar,
	Carousel,
	ChangeCredentials,
	CreditCardPayments,
	Errors,
	FAQs,
	Help,
	Launch,
	Login1stTime,
	LoginMessaging,
	LoginSubsequent,
	Payments,
	Result,
	Settings,
	SMS,
	Tutorial,
	ViewDocument,
	ViewTransactions
}