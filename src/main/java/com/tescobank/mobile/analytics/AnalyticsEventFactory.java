package com.tescobank.mobile.analytics;

import static com.tescobank.mobile.analytics.AnalyticsEventFactory.DocumentStatus.Exported;
import static com.tescobank.mobile.analytics.AnalyticsEventFactory.DocumentStatus.Viewed;
import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.AddDebitCard;
import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.BalancePeekSettingsUpdated;
import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.ChangePasscode;
import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.ChangePassword;
import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.DeleteDebitCard;
import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.FirstTimeLogin;
import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.ForgotPasscode;
import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.ForgotPassword;
import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.SwitchToPasscode;
import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.SwitchToPassword;
import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.UserChangedPasswordOnline;
import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedResultType.Failure;
import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedResultType.Success;
import static com.tescobank.mobile.analytics.AnalyticsGroup.Carousel;
import static com.tescobank.mobile.analytics.AnalyticsGroup.CreditCardPayments;
import static com.tescobank.mobile.analytics.AnalyticsGroup.Result;
import static com.tescobank.mobile.analytics.AnalyticsGroup.Settings;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.tescobank.mobile.api.content.RetrieveContentResponse.Document;
import com.tescobank.mobile.api.error.ErrorResponseWrapper.ErrorResponse;
import com.tescobank.mobile.api.payment.RetrievePaymentMethodsResponse.PaymentMethod;
import com.tescobank.mobile.api.storefinder.Store;
import com.tescobank.mobile.model.movemoney.MoneyMovement;
import com.tescobank.mobile.model.movemoney.Payment;
import com.tescobank.mobile.model.movemoney.Saving;
import com.tescobank.mobile.model.movemoney.Transfer;
import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.paycreditcard.CreditCardPayment;

public class AnalyticsEventFactory {	


	private static final int TRANSACTIONS_INITIAL_PAGE_NUMBER = 1;

	private static final String EVENT_1ST_TIME_WELCOME = "1stTimeLoginWelcome";
	private static final String EVENT_1ST_TIME_USERNAME = "1stTimeLoginEnterUserName";
	private static final String EVENT_1ST_TIME_PAM = "1stTimeLoginPAM";
	private static final String EVENT_ENTER_SECURITY_NUMBER = "EnterSecurityNumber";
	private static final String EVENT_ENTER_SMS_OTA = "EnterSMSOTA";
	private static final String EVENT_WELCOME_SETUP_PASSCODE = "WelcomeSetupPasscode";
	private static final String EVENT_SUBSEQUENT_LOGIN = "SubsequentLogin";
	private static final String EVENT_CHANGE_PASSCODE_ENTER_OLD = "ChangePasscodeEnterOld";
	private static final String EVENT_ALERT_DISPLAYED = "AlertDisplayed";
	private static final String EVENT_VIEW_TUTORIAL_SCREEN = "ViewTutorialScreen";
	private static final String EVENT_ERROR = "Error";
	private static final String EVENT_PASSIVE_ALERT_DISPLAYED = "PassiveAlertDisplayed";
	private static final String EVENT_VIEW_TSCS_FROM_SETTINGS_LINK_CLICKED = "ViewTs&CsFromSettings-LinkClicked";
	private static final String EVENT_VIEW_TSCS_FROM_SETTINGS = "ViewTs&CsFromSettings";
	private static final String EVENT_FAQS = "FAQs";
	private static final String EVENT_FAQS_LINK_CLICKED = "FAQs-LinkClicked";
	private static final String EVENT_VIEW_CONTACT_US = "ViewContactUs";
	private static final String EVENT_VIEW_CONTACT_US_LINK_CLICKED = "ViewContactUs-LinkClicked";
	private static final String EVENT_VIEW_DOCUMENT = "ViewDocument";
	private static final String EVENT_VIEW_DOCUMENT_LINK_CLICKED = "ViewDocument-LinkClicked";
	private static final String EVENT_CAROUSEL_COMPLETED_LOGIN = "Carousel-CompletedLogin";
	private static final String EVENT_CAROUSEL_ACCOUNT_VIEWED = "Carousel-AccountViewed";
	private static final String EVENT_CAROUSEL_VIEW_ATM_FINDER = "Carousel-ViewATMFinder";
	private static final String EVENT_CAROUSEL_ACCOUNT_DETAILS_VIEWED = "CarouselAccountDetailsViewed";
	private static final String EVENT_VIEW_TRANSACTION_DETAIL = "ViewTransactionDetail";
	private static final String EVENT_VIEW_TRANSACTION_LIST = "ViewTransactionList";
	private static final String EVENT_VIEW_TRANSACTION_LIST_SEARCH = "ViewTransactionList-Search";
	private static final String EVENT_VIEW_TRANSACTION_LIST_SORTING = "ViewTransactionList-Sorting";
	private static final String EVENT_VIEW_SETTINGS = "ViewSettings";
	private static final String EVENT_SELECT_ACTION_SHEET_ITEM = "SelectActionSheetItem";
	private static final String EVENT_BALANCE_PEEK_SETTINGS = "BalancePeekSettings";
	private static final String EVENT_BALANCE_PEEK_SETTINGS_CONFIRM = "ConfirmBalancePeekSettings";
	private static final String EVENT_BALANCE_PEEK_VIEW = "BalancePeekView";
	private static final String EVENT_ADD_DEBIT_CARD = "AddDebitCard";
	private static final String EVENT_CONFIRM_ADD_DEBIT_CARD = "ConfirmAddDebitCard";
	private static final String EVENT_CONFIRM_DELETE_DEBIT_CARD = "ConfirmDeleteDebitCard";
	private static final String EVENT_SET_OR_UPDATE_PASSWORD = "SetOrUpdatePassword";
	private static final String EVENT_SET_OR_UPDATE_PASSCODE = "SetOrUpdatePasscode";
	private static final String EVENT_SEASONAL_EFFECT_STARTED = "SeasonalEffect-Started";
	private static final String EVENT_SEASONAL_EFFECT_STOPPED_AUTOMATICALLY = "SeasonalEffect-StoppedAutomatically";
	private static final String EVENT_SEASONAL_EFFECT_STOPPED_MANUALLY = "SeasonalEffect-StoppedManually";
	private static final String EVENT_VIEW_STATEMENT_ARCHIVE = "ViewStatementArchive";
	private static final String EVENT_LIST_VIEW = "ListView";
	private static final String EVENT_VIEW_REGULAR_PAYMENTS = "ViewRegularPayments";
	private static final String EVENT_PROCESS_COMPLETED = "ProcessCompleted";
	private static final String EVENT_PAY_CREDIT_CARD_ENTER_CVV = "PayCreditCardEnterCVV";
	private static final String EVENT_SELECT_DEBIT_CARD = "SelectDebitCard";
	private static final String EVENT_PAY_CREDIT_CARD = "PayCreditCard";
	private static final String EVENT_CONFIRM_CREDIT_CARD_PAYMENT = "ConfirmCreditCardPayment";
	private static final String EVENT_PAY_CREDIT_CARD3D_SECURE = "PayCreditCard3DSecure";
	private static final String EVENT_MAKE_PAYMENT_TRANSFER = "MakePayment-Transfer";
	private static final String EVENT_PAYMENTS_WHEEL = "PaymentsWheel";
	private static final String EVENT_MAKE_PAYMENT_PAYMENT = "MakePayment-Payment";
	private static final String EVENT_VIEW_CALENDAR_MONTH = "ViewCalendarMonth";
	private static final String EVENT_ATM_FINDER_MAP_VIEW = "ATMFinderMapView";
	private static final String EVENT_ATM_FINDER_DETAIL_VIEW = "ATMFinderDetailView";
	private static final String EVENT_ATM_FINDER_LIST_VIEW = "ATMFinderListView";

	private static final String KEY_HASH_VALUE = "HashValue";
	private static final String KEY_LOGIN_STATUS = "LoginStatus";
	private static final String KEY_ACCESSIBILITY_VOICE_OVER_RUNNING = "AccessibilityVoiceOverRunning";
	private static final String KEY_PROCESS_COMPLETED_NAME = "ProcessCompletedName";
	private static final String KEY_PROCESS_COMPLETED_RESULT_TYPE = "ProcessCompletedResultType";
	private static final String KEY_PROCESS_COMPLETED_VALUE = "ProcessCompletedValue";
	private static final String KEY_PROCESS_COMPLETED_VALUE_ROUNDED = "ProcessCompletedValue-rounded";
	private static final String KEY_PRODUCT_TYPE = "ProductType";
	private static final String KEY_PRODUCT_CODE = "ProductCode";
	private static final String KEY_PRODUCT_NAME = "ProductName";
	private static final String KEY_PAYMENTS_WHEEL_AMOUNT_ROUNDED = "PaymentsWheelAmount-Rounded";
	private static final String KEY_PAYMENTS_WHEEL_AMOUNT = "PaymentsWheelAmount";
	private static final String KEY_CREDIT_CARD_PAYMENT_SELECTED = "CreditCardPaymentSelected";
	private static final String KEY_CREDIT_CARD_AMOUNT_PAYED_ROUNDED = "CreditCardAmountPayed-Rounded";
	private static final String KEY_CREDIT_CARD_AMOUNT_PAYED = "CreditCardAmountPayed";
	private static final String KEY_TRANSFER_SCHEDULED = "TransferScheduled";
	private static final String KEY_SEARCH_TERM = "SearchTerm";
	private static final String KEY_SORT_OPTION = "SortOption";
	private static final String KEY_VIEW_STATEMENT_CYCLE = "ViewStatementCycle";
	private static final String KEY_VIEWED_STATEMENT_ARCHIVE_FROM = "ViewedFrom";
	private static final String KEY_HTTP_STATUS_CODE = "HttpStatusCode";
	private static final String KEY_ERROR_CODE = "ErrorCode";
	private static final String KEY_PASSIVE_ALERT_NAME = "PassiveAlertName";
	private static final String KEY_ACTION_SHEET_ITEM = "ActionSheetItem";
	private static final String KEY_ALERT_MESSAGE = "AlertMessage";
	private static final String KEY_ALERT_NAME = "AlertName";
	private static final String KEY_SCREEN_NAME = "ScreenName";
	private static final String KEY_TOGGLE_METHOD = "ToggleMethod";
	private static final String KEY_TOGGLE_TO = "ToggleTo";
	private static final String KEY_DOCUMENT_STATUS = "DocumentStatus";
	private static final String KEY_DOC_URL = "DocURL";
	private static final String KEY_PDF_URL = "PdfURL";
	private static final String KEY_URL = "URL";
	private static final String KEY_VIEW_FAQS = "ViewFAQs";
	private static final String KEY_WAS_PNP = "WasPNP";
	private static final String KEY_CONFIRM_TRANSFER_AMOUNT_ROUNDED = "ConfirmTransferAmount-Rounded";
	private static final String KEY_CONFIRM_TRANSFER_AMOUNT = "ConfirmTransferAmount";
	private static final String KEY_PAYMENT_SCHEDULED = "PaymentScheduled";
	private static final String KEY_CONFIRM_PAYMENT_AMOUNT_ROUNDED = "ConfirmPaymentAmount-Rounded";
	private static final String KEY_CONFIRM_PAYMENT_AMOUNT = "ConfirmPaymentAmount";
	private static final String KEY_BALANCE_PEEK_TOGGLE = "BalancePeekToggle";
	private static final String KEY_BALANCE_PEEK_ACCOUNTS_ENABLED = "BalancePeekAccountsEnabled";
	private static final String KEY_BALANCE_PEEK_ACCOUNT_TO_SHOW = "AccountToShow";

	private static final String VALUE_POST_LOGIN = "PostLogin";
	private static final String VALUE_PRE_LOGIN = "PreLogin";
	private static final String VALUE_ENABLED = "Enabled";
	private static final String VALUE_DISABLED = "Disabled";
	
	private AmountRounder amountRounder;

	private String hashedTbId;

	public static enum LoginStatus {

		FirstTimeLogin("1stTimeLogin"), SubsequentLoginPassword, SubsequentLoginPasscode, ForgotPassword, ForgotPasscode, ChangePassword, ChangePasscode, SwitchToPasscode, SwitchToPassword, UserChangedPasswordOnline;

		private String name;

		LoginStatus() {
		}

		LoginStatus(String name) {
			this.name = name;
		}

		public String getName() {
			if (name != null) {
				return name;
			} else {
				return this.name();
			}
		}
	};

	public static enum DocumentStatus {
		Viewed, Exported, Accepted
	}

	public static enum ProcessCompletedResultType {
		Success, Failure, Unknown;
	}

	public static enum ProcessCompletedName {

		MakePayment, MakeTransfer, MoveMoneyIn, PayCreditCard, BalancePeekSettingsUpdated("BalancePeekSettings-Updated"), AddDebitCard, DeleteDebitCard, ChangePasscode, ChangePassword, FirstTimeLogin("1stTimeLogin"), ForgotPassword, ForgotPasscode, SwitchToPasscode, SwitchToPassword, UserChangedPasswordOnline;

		private String name;

		ProcessCompletedName() {
		}

		ProcessCompletedName(String name) {
			this.name = name;
		}

		public String getName() {
			if (name != null) {
				return name;
			} else {
				return this.name();
			}
		}

	}

	private static enum PaymentSchedule {
		Immediate, Future
	}

	public AnalyticsEventFactory() {
		this.amountRounder = new AmountRounder();
	}

	public void setHashedTbId(String hashedTbId) {
		this.hashedTbId = hashedTbId;
	}

	public AnalyticsEvent eventForWelcome() {
		return new AnalyticsEvent(AnalyticsGroup.Login1stTime, EVENT_1ST_TIME_WELCOME);
	}

	public AnalyticsEvent eventForRegistrationEnterUsername() {
		return new AnalyticsEvent(AnalyticsGroup.Login1stTime, EVENT_1ST_TIME_USERNAME);
	}

	public AnalyticsEvent eventForRegistrationPAM() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Login1stTime, EVENT_1ST_TIME_PAM);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForRegistrationEnterPin() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_ENTER_SECURITY_NUMBER);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.FirstTimeLogin.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForUserForgottenPasswordPin() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_ENTER_SECURITY_NUMBER);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.ForgotPassword.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForUserChangePasswordPin() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_ENTER_SECURITY_NUMBER);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.ChangePassword.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForUserSwitchToPasswordPin() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_ENTER_SECURITY_NUMBER);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.SwitchToPassword.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForRegistrationEnterPassword() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_SET_OR_UPDATE_PASSWORD);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.FirstTimeLogin.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForUserForgottenPassword() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_SET_OR_UPDATE_PASSWORD);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.ForgotPassword.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForUserForgottenPasscode() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_SET_OR_UPDATE_PASSCODE);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.ForgotPasscode.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForUserChangePasscodeExistingPasscode() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_CHANGE_PASSCODE_ENTER_OLD);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForUserChangePasscodeCreatePasscode() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_SET_OR_UPDATE_PASSCODE);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.ChangePasscode.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForUserChangePasswordCreatePassword() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_SET_OR_UPDATE_PASSWORD);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.ChangePassword.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForUserSwitchToPasscodePassword() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_SET_OR_UPDATE_PASSWORD);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.SwitchToPasscode.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForUserSwitchToPasscodeCreatePasscode() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_SET_OR_UPDATE_PASSCODE);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.SwitchToPasscode.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForUserSwitchToPasswordCreatePassword() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_SET_OR_UPDATE_PASSWORD);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.SwitchToPassword.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForLoginPasswordChangedOnlinePinEntered() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_ENTER_SECURITY_NUMBER);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.UserChangedPasswordOnline.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForLoginPasswordChangedOnlineSMSEntered() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.SMS, EVENT_ENTER_SMS_OTA);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.UserChangedPasswordOnline.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForLoginPasswordChangedOnlinePasswordEntered() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_SET_OR_UPDATE_PASSWORD);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.UserChangedPasswordOnline.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForLoginPasswordChangedOnlineCreatePasscode() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_SET_OR_UPDATE_PASSCODE);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.UserChangedPasswordOnline.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForFirstTimeLoginCompletedSuccessfully() {
		return eventForProcessCompleted(FirstTimeLogin, Success);
	}

	public AnalyticsEvent eventForUserSwitchToPasscodeCreatePasscodeCompletedSuccessfully() {
		return eventForProcessCompleted(SwitchToPasscode, Success);
	}

	public AnalyticsEvent eventForUserSwitchToPasswordCreatePasswordCompletedSuccessfully() {
		return eventForProcessCompleted(SwitchToPassword, Success);
	}

	public AnalyticsEvent eventForUserForgottenPasscodeCreatePasscodeCompletedSuccessfully() {
		return eventForProcessCompleted(ForgotPasscode, Success);
	}

	public AnalyticsEvent eventForUserForgottenPasswordCreatePasswordCompletedSuccessfully() {
		return eventForProcessCompleted(ForgotPassword, Success);
	}

	public AnalyticsEvent eventForUserChangePasscodeCreatePasscodeCompletedSuccessfully() {
		return eventForProcessCompleted(ChangePasscode, Success);
	}

	public AnalyticsEvent eventForUserChangePasswordCreatePasswordCompletedSuccessfully() {
		return eventForProcessCompleted(ChangePassword, Success);
	}

	public AnalyticsEvent eventForUserPasswordChangedOnlineCreatePasscodeCompletedSuccessfully() {
		return eventForProcessCompleted(UserChangedPasswordOnline, Success);
	}

	public AnalyticsEvent eventForRegistrationSMS() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.SMS, EVENT_ENTER_SMS_OTA);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.FirstTimeLogin.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForUserForgottenPasswordSMS() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.SMS, EVENT_ENTER_SMS_OTA);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.ForgotPassword.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForUserForgottenPasscodePassword() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_SET_OR_UPDATE_PASSWORD);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.ForgotPasscode.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForRegistrationComplete() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_WELCOME_SETUP_PASSCODE);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.FirstTimeLogin.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForRegistrationSetupPasscode() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ChangeCredentials, EVENT_SET_OR_UPDATE_PASSCODE);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.FirstTimeLogin.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForLoginWithPassword() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.LoginSubsequent, EVENT_SUBSEQUENT_LOGIN);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.SubsequentLoginPassword.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForLoginWithPasscode() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.LoginSubsequent, EVENT_SUBSEQUENT_LOGIN);
		event.addData(KEY_LOGIN_STATUS, LoginStatus.SubsequentLoginPasscode.getName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForCarouselCompletedLogin(List<? extends Product> products, boolean talkBack) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Carousel, EVENT_CAROUSEL_COMPLETED_LOGIN);
		addProductTypesToEvent(event, products);
		event.addData(KEY_ACCESSIBILITY_VOICE_OVER_RUNNING, Boolean.toString(talkBack));
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForCarouselProductShown(Product product, boolean isDetail) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Carousel, isDetail ? EVENT_CAROUSEL_ACCOUNT_DETAILS_VIEWED : EVENT_CAROUSEL_ACCOUNT_VIEWED);
		addProductDetailsToEvent(event, product);
		addHashedTbIDToEvent(event);
		return event;
	}
	
	public AnalyticsEvent eventForSeasonalEffectStarted() {
		return new AnalyticsEvent(Carousel, EVENT_SEASONAL_EFFECT_STARTED);
	}

	public AnalyticsEvent eventForSeasonalEffectStoppedAutomatically() {
		return new AnalyticsEvent(Carousel, EVENT_SEASONAL_EFFECT_STOPPED_AUTOMATICALLY);
	}

	public AnalyticsEvent eventForSeasonalEffectStoppedManually() {
		return new AnalyticsEvent(Carousel, EVENT_SEASONAL_EFFECT_STOPPED_MANUALLY);
	}

	public AnalyticsEvent eventForViewTransactions(Product product, int pageNumber) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ViewTransactions, EVENT_VIEW_TRANSACTION_LIST);
		addProductDetailsToEvent(event, product);
		addHashedTbIDToEvent(event);
		if (pageNumber > TRANSACTIONS_INITIAL_PAGE_NUMBER) {
			event.addData("ViewTransactionLazyLoadCount", String.valueOf(pageNumber));
		}
		return event;
	}

	public AnalyticsEvent eventForViewTransactionDetails(Product product) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ViewTransactions, EVENT_VIEW_TRANSACTION_DETAIL);
		addProductDetailsToEvent(event, product);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForViewTransactionsListSort(String sort, ProductDetails product) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ViewTransactions, EVENT_VIEW_TRANSACTION_LIST_SORTING);
		event.addData(KEY_SORT_OPTION, sort);
		addProductDetailsToEvent(event, product);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForViewTransactionsListSearch(String text, ProductDetails product) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ViewTransactions, EVENT_VIEW_TRANSACTION_LIST_SEARCH);
		event.addData(KEY_SEARCH_TERM, text);
		addProductDetailsToEvent(event, product);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForViewStatement(Product product, int cycleNumber, boolean exported) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ViewDocument, "ViewStatement" + (exported ? "-Exported" : ""));
		event.addData(KEY_VIEW_STATEMENT_CYCLE, String.valueOf(cycleNumber));
		event.addData(KEY_DOCUMENT_STATUS, Viewed.name());
		addProductDetailsToEvent(event, product);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForViewATM() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Carousel, EVENT_CAROUSEL_VIEW_ATM_FINDER);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForPassiveError(ErrorResponse error) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Alerts, EVENT_PASSIVE_ALERT_DISPLAYED);
		event.addData(KEY_PASSIVE_ALERT_NAME, error.getDisplay().getMessage());
		event.addData(KEY_ERROR_CODE, error.getErrorCode());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForScreenErrorCommunicatingWithNetwork(ErrorResponse error) {
		AnalyticsEvent event = eventForScreenError(error);
		event.addData(KEY_HTTP_STATUS_CODE, Integer.toString(error.getHttpStatusCode()));
		return event;
	}

	public AnalyticsEvent eventForScreenError(ErrorResponse error) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Errors, EVENT_ERROR);
		event.addData(KEY_ERROR_CODE, error.getErrorCode());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForDocument(Document document, boolean registered, boolean usesPassword, DocumentStatus status) {

		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.LoginMessaging, EVENT_VIEW_DOCUMENT);
		LoginStatus staus = documentLoginStatus(registered, usesPassword);
		event.addData(KEY_LOGIN_STATUS, staus.getName());
		if (document.getPdfURL() != null) {
			event.addData(KEY_PDF_URL, document.getPdfURL());
		}
		event.addData(KEY_DOC_URL, document.getHtmlURL());
		event.addData(KEY_DOCUMENT_STATUS, status.name());

		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForClickedUrlInDocument(String url) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.LoginMessaging, EVENT_VIEW_DOCUMENT_LINK_CLICKED);
		event.addData(KEY_URL, url);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForViewContactUs() {
		return eventForViewSettingsItem(EVENT_VIEW_CONTACT_US);
	}

	public AnalyticsEvent eventForViewContactUsLinkClicked(String url) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Settings, EVENT_VIEW_CONTACT_US_LINK_CLICKED);
		event.getData().put(KEY_URL, url);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForViewFaq(boolean authenticated) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.FAQs, EVENT_FAQS);
		event.addData(KEY_VIEW_FAQS, authenticated ? VALUE_POST_LOGIN : VALUE_PRE_LOGIN);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForViewFaqLinkClicked(boolean authenticated, String url) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.FAQs, EVENT_FAQS_LINK_CLICKED);
		event.addData(KEY_VIEW_FAQS, authenticated ? VALUE_POST_LOGIN : VALUE_PRE_LOGIN);
		event.addData(KEY_URL, url);
		addHashedTbIDToEvent(event);
		return event;
	}
	
	public AnalyticsEvent eventForViewTermsAndConditions() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Settings, EVENT_VIEW_TSCS_FROM_SETTINGS);
		event.addData(KEY_DOCUMENT_STATUS, Viewed.name());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForViewTermsAndConditionsLinkClicked(String url) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Settings, EVENT_VIEW_TSCS_FROM_SETTINGS_LINK_CLICKED);
		event.addData(KEY_URL, url);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForExportTerms() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Settings, "ViewTs&CsFromSettings-Exported");
		event.addData(KEY_DOCUMENT_STATUS, Exported.name());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForATMFinderList() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ATMFinder, EVENT_ATM_FINDER_LIST_VIEW);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForATMDetail(Store store) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ATMFinder, EVENT_ATM_FINDER_DETAIL_VIEW);
		event.addData("branchID", store.getStoreMetadata().getBranchID());
		event.addData("branchName", store.getStoreMetadata().getBranchName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForATMFinderMap() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ATMFinder, EVENT_ATM_FINDER_MAP_VIEW);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForViewTransactionCalendar(String monthYearViewed) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Calendar, EVENT_VIEW_CALENDAR_MONTH);
		event.addData("MonthViewed", monthYearViewed);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForMakePayment(Payment payment) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Payments, EVENT_MAKE_PAYMENT_PAYMENT);
		event.addData(KEY_CONFIRM_PAYMENT_AMOUNT, formatAmount(payment.getAmount()));
		event.addData(KEY_CONFIRM_PAYMENT_AMOUNT_ROUNDED, String.valueOf(amountRounder.round(payment.getAmount())));
		event.addData(KEY_PAYMENT_SCHEDULED, payment.isMoneyMovementDateToday() ? PaymentSchedule.Immediate.name() : PaymentSchedule.Future.name());
		addProductDetailsToEvent(event, payment.getSource());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForMakeTransfer(Transfer transfer, boolean isPayNoPayTransfer) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Payments, EVENT_MAKE_PAYMENT_TRANSFER);
		event.addData(KEY_CONFIRM_TRANSFER_AMOUNT, formatAmount(transfer.getAmount()));
		event.addData(KEY_CONFIRM_TRANSFER_AMOUNT_ROUNDED, String.valueOf(amountRounder.round(transfer.getAmount())));
		event.addData(KEY_TRANSFER_SCHEDULED, transfer.isMoneyMovementDateToday() ? PaymentSchedule.Immediate.name() : PaymentSchedule.Future.name());
		addProductDetailsToEvent(event, transfer.getSource());
		event.addData(KEY_WAS_PNP, Boolean.toString(isPayNoPayTransfer).toUpperCase(Locale.UK));
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForMakeSaving(Saving saving) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Payments, EVENT_PAYMENTS_WHEEL);
		addProductDetailsToEvent(event, saving.getDestination());
		event.addData(KEY_PAYMENTS_WHEEL_AMOUNT, formatAmount(saving.getAmount()));
		event.addData(KEY_PAYMENTS_WHEEL_AMOUNT_ROUNDED, String.valueOf(amountRounder.round(saving.getAmount())));
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForMakePaymentCompletedSuccessfully(Payment payment) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Result, EVENT_PROCESS_COMPLETED);
		addProductDetailsToEvent(event, payment.getSource());
		event.addData(KEY_PROCESS_COMPLETED_VALUE, formatAmount(payment.getAmount()));
		event.addData(KEY_PROCESS_COMPLETED_VALUE_ROUNDED, String.valueOf(amountRounder.round(payment.getAmount())));
		event.addData(KEY_PROCESS_COMPLETED_RESULT_TYPE, ProcessCompletedResultType.Success.name());
		event.addData(KEY_PROCESS_COMPLETED_NAME, payment.getProcessCompleteAnalyticsEventName());
		event.addData(KEY_PAYMENT_SCHEDULED, payment.isMoneyMovementDateToday() ? PaymentSchedule.Immediate.name() : PaymentSchedule.Future.name());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForMakeTransferCompletedSuccessfully(Transfer transfer) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Result, EVENT_PROCESS_COMPLETED);
		addProductDetailsToEvent(event, transfer.getSource());
		event.addData(KEY_PROCESS_COMPLETED_VALUE, formatAmount(transfer.getAmount()));
		event.addData(KEY_PROCESS_COMPLETED_VALUE_ROUNDED, String.valueOf(amountRounder.round(transfer.getAmount())));
		event.addData(KEY_PROCESS_COMPLETED_RESULT_TYPE, ProcessCompletedResultType.Success.name());
		event.addData(KEY_PROCESS_COMPLETED_NAME, transfer.getProcessCompleteAnalyticsEventName());
		event.addData(KEY_TRANSFER_SCHEDULED, transfer.isMoneyMovementDateToday() ? PaymentSchedule.Immediate.name() : PaymentSchedule.Future.name());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForMoveMoneyCompletedUnsuccessfully(MoneyMovement<ProductDetails, ?> moneyMovement) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Result, EVENT_PROCESS_COMPLETED);
		addProductDetailsToEvent(event, moneyMovement.getSource());
		event.addData(KEY_PROCESS_COMPLETED_VALUE, formatAmount(moneyMovement.getAmount()));
		event.addData(KEY_PROCESS_COMPLETED_VALUE_ROUNDED, String.valueOf(amountRounder.round(moneyMovement.getAmount())));
		event.addData(KEY_PROCESS_COMPLETED_RESULT_TYPE, ProcessCompletedResultType.Failure.name());
		event.addData(KEY_PROCESS_COMPLETED_NAME, moneyMovement.getProcessCompleteAnalyticsEventName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForMakeSavingCompletedSuccessfully(Saving saving) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Result, EVENT_PROCESS_COMPLETED);
		addProductDetailsToEvent(event, saving.getDestination());
		event.addData(KEY_PROCESS_COMPLETED_VALUE, formatAmount(saving.getAmount()));
		event.addData(KEY_PROCESS_COMPLETED_VALUE_ROUNDED, String.valueOf(amountRounder.round(saving.getAmount())));
		event.addData(KEY_PROCESS_COMPLETED_RESULT_TYPE, ProcessCompletedResultType.Success.name());
		event.addData(KEY_PROCESS_COMPLETED_NAME, saving.getProcessCompleteAnalyticsEventName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForMakeSavingCompletedUnsuccessfully(Saving saving) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Result, EVENT_PROCESS_COMPLETED);
		addProductDetailsToEvent(event, saving.getDestination());
		event.addData(KEY_PROCESS_COMPLETED_VALUE, formatAmount(saving.getAmount()));
		event.addData(KEY_PROCESS_COMPLETED_VALUE_ROUNDED, String.valueOf(amountRounder.round(saving.getAmount())));
		event.addData(KEY_PROCESS_COMPLETED_RESULT_TYPE, ProcessCompletedResultType.Failure.name());
		event.addData(KEY_PROCESS_COMPLETED_NAME, saving.getProcessCompleteAnalyticsEventName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForThreeDSecure() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.CreditCardPayments, EVENT_PAY_CREDIT_CARD3D_SECURE);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForPayCreditCard(ProductDetails creditCardProductDetails) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.CreditCardPayments, EVENT_PAY_CREDIT_CARD);
		addProductDetailsToEvent(event, creditCardProductDetails);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForPayCreditCardSelectDebitCard() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.CreditCardPayments, EVENT_SELECT_DEBIT_CARD);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForPayCreditCardEnterCvv() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.CreditCardPayments, EVENT_PAY_CREDIT_CARD_ENTER_CVV);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForPayCreditCardConfirmation(MoneyMovement<PaymentMethod, ProductDetails> payment, String creditCardPaymentType) {

		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.CreditCardPayments, EVENT_CONFIRM_CREDIT_CARD_PAYMENT);
		addProductDetailsToEvent(event, payment.getDestination());
		event.addData(KEY_CREDIT_CARD_AMOUNT_PAYED, formatAmount(payment.getAmount()));
		event.addData(KEY_CREDIT_CARD_AMOUNT_PAYED_ROUNDED, String.valueOf(amountRounder.round(payment.getAmount())));
		event.addData(KEY_CREDIT_CARD_PAYMENT_SELECTED, creditCardPaymentType);

		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForPayCreditCardCompletedSuccessfully(CreditCardPayment payment) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Result, EVENT_PROCESS_COMPLETED);
		addProductDetailsToEvent(event, payment.getDestination());
		event.addData(KEY_PROCESS_COMPLETED_VALUE, formatAmount(payment.getAmount()));
		event.addData(KEY_PROCESS_COMPLETED_VALUE_ROUNDED, String.valueOf(amountRounder.round(payment.getAmount())));
		event.addData(KEY_PROCESS_COMPLETED_RESULT_TYPE, ProcessCompletedResultType.Success.name());
		event.addData(KEY_PROCESS_COMPLETED_NAME, payment.getProcessCompleteAnalyticsEventName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForPayCreditCardCompletedUnsuccessfully(CreditCardPayment payment) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Result, EVENT_PROCESS_COMPLETED);
		addProductDetailsToEvent(event, payment.getDestination());
		event.addData(KEY_PROCESS_COMPLETED_VALUE, formatAmount(payment.getAmount()));
		event.addData(KEY_PROCESS_COMPLETED_VALUE_ROUNDED, String.valueOf(amountRounder.round(payment.getAmount())));
		event.addData(KEY_PROCESS_COMPLETED_RESULT_TYPE, ProcessCompletedResultType.Failure.name());
		event.addData(KEY_PROCESS_COMPLETED_NAME, payment.getProcessCompleteAnalyticsEventName());
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForAddDebitCard() {
		AnalyticsEvent event = new AnalyticsEvent(CreditCardPayments, EVENT_ADD_DEBIT_CARD);
		addHashedTbIDToEvent(event);
		return event;
	}
	
	public AnalyticsEvent eventForAddDebitCardConfirmation() {
		AnalyticsEvent event = new AnalyticsEvent(CreditCardPayments, EVENT_CONFIRM_ADD_DEBIT_CARD);
		addHashedTbIDToEvent(event);
		return event;
	}
	
	public AnalyticsEvent eventForAddDebitCardCompletedSuccessfully(Product product) {
		AnalyticsEvent event = new AnalyticsEvent(Result, EVENT_PROCESS_COMPLETED);
		event.addData(KEY_PROCESS_COMPLETED_RESULT_TYPE, Success.name());
		event.addData(KEY_PROCESS_COMPLETED_NAME, AddDebitCard.getName());
		addProductDetailsToEvent(event, product);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForDeleteDebitCardConfirmation() {
		AnalyticsEvent event = new AnalyticsEvent(CreditCardPayments, EVENT_CONFIRM_DELETE_DEBIT_CARD);
		addHashedTbIDToEvent(event);
		return event;
	}
	
	public AnalyticsEvent eventForDeleteDebitCardCompletedSuccessfully(Product product) {
		AnalyticsEvent event = new AnalyticsEvent(Result, EVENT_PROCESS_COMPLETED);
		event.addData(KEY_PROCESS_COMPLETED_RESULT_TYPE, Success.name());
		event.addData(KEY_PROCESS_COMPLETED_NAME, DeleteDebitCard.getName());
		addProductDetailsToEvent(event, product);
		addHashedTbIDToEvent(event);
		return event;
	}
		
	public AnalyticsEvent eventForViewRegularPayments(ProductDetails product) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Payments, EVENT_VIEW_REGULAR_PAYMENTS);
		addProductDetailsToEvent(event, product);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForViewStatementArchive(Product product, String activityName) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.ViewDocument, EVENT_VIEW_STATEMENT_ARCHIVE);
		event.addData(KEY_VIEWED_STATEMENT_ARCHIVE_FROM, "ViewedFrom(" + activityName + ")");
		addProductDetailsToEvent(event, product);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForViewTutorialScreen(String screenName) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Tutorial, EVENT_VIEW_TUTORIAL_SCREEN);
		event.addData(KEY_SCREEN_NAME, screenName);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForAlertDisplayed(String name, String message) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Alerts, EVENT_ALERT_DISPLAYED);
		event.addData(KEY_ALERT_NAME, name == null ? "" : name);
		event.addData(KEY_ALERT_MESSAGE, message == null ? "" : message);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForViewSettings() {
		return eventForViewSettingsItem(EVENT_VIEW_SETTINGS);
	}
	
	public AnalyticsEvent eventForViewBalancePeekSetup() {
		return eventForViewSettingsItem(EVENT_BALANCE_PEEK_SETTINGS);
	}
	
	public AnalyticsEvent eventForViewBalancePeek(int numOfAccounts) {
		AnalyticsEvent event = new AnalyticsEvent(Carousel, EVENT_BALANCE_PEEK_VIEW);
		event.addData(KEY_BALANCE_PEEK_ACCOUNT_TO_SHOW, String.valueOf(numOfAccounts));
		addHashedTbIDToEvent(event);
		return event;
	}
	
	public AnalyticsEvent eventForBalancePeekEnabledSubmitted(List<String> products) {
		AnalyticsEvent event = eventForViewSettingsItem(EVENT_BALANCE_PEEK_SETTINGS_CONFIRM);
		event.addData(KEY_BALANCE_PEEK_TOGGLE, VALUE_ENABLED);
		event.addData(KEY_BALANCE_PEEK_ACCOUNTS_ENABLED, createCsvStringFromList(products));
		return event;
	}
	
	public AnalyticsEvent eventForBalancePeekDisabledSubmitted() {
		AnalyticsEvent event = eventForViewSettingsItem(EVENT_BALANCE_PEEK_SETTINGS_CONFIRM);
		event.addData(KEY_BALANCE_PEEK_TOGGLE, VALUE_DISABLED);
		return event;
	}

	public AnalyticsEvent eventForBalancePeekSubmitPreferencesProcessCompleted(boolean success) {
		return eventForProcessCompleted(BalancePeekSettingsUpdated, success ? Success : Failure);
	}

	public AnalyticsEvent eventForMenuItemSelected(String title) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Carousel, EVENT_SELECT_ACTION_SHEET_ITEM);
		event.addData(KEY_ACTION_SHEET_ITEM, title);
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForToggleToListView() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Carousel, EVENT_LIST_VIEW);
		event.addData(KEY_TOGGLE_TO, "List");
		event.addData(KEY_TOGGLE_METHOD, "Button");
		addHashedTbIDToEvent(event);
		return event;
	}

	public AnalyticsEvent eventForToggleToCarousel() {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Carousel, EVENT_LIST_VIEW);
		event.addData(KEY_TOGGLE_TO, "Carousel");
		event.addData(KEY_TOGGLE_METHOD, "Button");
		addHashedTbIDToEvent(event);
		return event;
	}
	
	private AnalyticsEvent eventForProcessCompleted(ProcessCompletedName name, ProcessCompletedResultType resultType) {
		AnalyticsEvent event = new AnalyticsEvent(AnalyticsGroup.Result, EVENT_PROCESS_COMPLETED);
		event.addData(KEY_PROCESS_COMPLETED_RESULT_TYPE, resultType.name());
		event.addData(KEY_PROCESS_COMPLETED_NAME, name.getName());
		addHashedTbIDToEvent(event);
		return event;
	}
	
	private AnalyticsEvent eventForViewSettingsItem(String eventName) {
		AnalyticsEvent event = new AnalyticsEvent(Settings, eventName);
		addHashedTbIDToEvent(event);
		return event;		
	}

	private String formatAmount(BigDecimal amount) {
		BigDecimal rounded = amount.setScale(2, BigDecimal.ROUND_DOWN);
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		df.setGroupingUsed(false);
		return df.format(rounded);
	}

	private LoginStatus documentLoginStatus(boolean registered, boolean usesPassword) {

		LoginStatus status = LoginStatus.FirstTimeLogin;
		if (registered) {
			if (usesPassword) {
				status = LoginStatus.SubsequentLoginPassword;
			} else {
				status = LoginStatus.SubsequentLoginPasscode;
			}
		}
		return status;
	}

	private void addHashedTbIDToEvent(AnalyticsEvent event) {
		if (hashedTbId != null) {
			event.addData(KEY_HASH_VALUE, hashedTbId);
		}
	}

	private void addProductTypesToEvent(AnalyticsEvent event, List<? extends Product> produtcs) {
		Map<String, String> productTypes = createProductTypeMap(produtcs);
		for (String productType : productTypes.keySet()) {
			event.addData(productType, productTypes.get(productType));
		}
	}

	private void addProductDetailsToEvent(AnalyticsEvent event, Product product) {
		event.addData(KEY_PRODUCT_TYPE, product.getProductType());
		event.addData(KEY_PRODUCT_CODE, product.getProductCode());
		event.addData(KEY_PRODUCT_NAME, product.getProductName());
	}

	private Map<String, String> createProductTypeMap(List<? extends Product> produtcs) {
		Map<String, String> productTypes = new HashMap<String, String>();
		for (Product product : produtcs) {
			String current = productTypes.get(product.getProductType());
			String updated = addItemToCsvString(current, product.getProductCode());
			productTypes.put(product.getProductType(), updated);
		}
		return productTypes;
	}
	
	private String createCsvStringFromList(List<String> list) {
		String csv = "";
		for(String s : list) {
			csv = addItemToCsvString(csv, s);
		}
		return csv;
	}
	
	private String addItemToCsvString(String csvString, String itemToAdd) {
		if(csvString == null || csvString.isEmpty()) {
			return itemToAdd;
		}
		return csvString + "," + itemToAdd;
	}
}
