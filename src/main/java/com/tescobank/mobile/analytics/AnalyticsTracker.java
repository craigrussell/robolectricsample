package com.tescobank.mobile.analytics;

import java.util.List;

import android.content.Context;

public interface AnalyticsTracker {
	
	void setEnabledTrackingGroups(List<AnalyticsGroup> groups);
	
	void startTracking(Context context);

	/**
	 * @param event The analytics event to track
	 * @return whether the event was passed to the analytics provider. A false
	 *         result indicates that the analytics group is not currently
	 *         enabled.
	 */
	boolean trackEvent(AnalyticsEvent event);

	void stopTracking();
	
	boolean trackEventGroupsNotYetAvailable(AnalyticsEvent event);
}
