package com.tescobank.mobile.analytics;

import java.util.List;

import android.content.Context;

import com.tescobank.mobile.analytics.AnalyticsEventFactory.DocumentStatus;
import com.tescobank.mobile.api.content.RetrieveContentResponse.Document;
import com.tescobank.mobile.api.error.ErrorResponseWrapper.ErrorResponse;
import com.tescobank.mobile.api.payment.RetrievePaymentMethodsResponse.PaymentMethod;
import com.tescobank.mobile.api.storefinder.Store;
import com.tescobank.mobile.model.movemoney.MoneyMovement;
import com.tescobank.mobile.model.movemoney.Payment;
import com.tescobank.mobile.model.movemoney.Saving;
import com.tescobank.mobile.model.movemoney.Transfer;
import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.paycreditcard.CreditCardPayment;

/**
 * Convenience class to create Tesco Analytics Events and simultaneously track them
 */
public class TescoAnalyticsTracker implements AnalyticsTracker {

	private AnalyticsTracker tracker;
	private AnalyticsEventFactory eventFactory;

	public TescoAnalyticsTracker(AnalyticsTracker tracker, AnalyticsEventFactory eventFactory) {
		this.tracker = tracker;
		this.eventFactory = eventFactory;
	}

	@Override
	public void setEnabledTrackingGroups(List<AnalyticsGroup> groups) {
		tracker.setEnabledTrackingGroups(groups);
	}

	@Override
	public void startTracking(Context context) {
		tracker.startTracking(context);
	}

	@Override
	public boolean trackEvent(AnalyticsEvent event) {
		return tracker.trackEvent(event);
	}
	
	@Override
	public boolean trackEventGroupsNotYetAvailable(AnalyticsEvent event) {
		return tracker.trackEventGroupsNotYetAvailable(event);
	}

	@Override
	public void stopTracking() {
		tracker.stopTracking();
	}

	public void setHashedTbId(String hashedTbId) {
		eventFactory.setHashedTbId(hashedTbId);
	}
	
	public boolean trackUserChangePasscodeExistingPasscode() {
		return trackEvent(eventFactory.eventForUserChangePasscodeExistingPasscode());
	}

	public boolean trackUserFirstTimeLoginCompletedSuccessfully() {
		return trackEvent(eventFactory.eventForFirstTimeLoginCompletedSuccessfully());
	}

	public boolean trackUserSwitchToPasscodeCreatePasscodeCompletedSuccessfully() {
		return trackEvent(eventFactory.eventForUserSwitchToPasscodeCreatePasscodeCompletedSuccessfully());
	}

	public boolean trackUserSwitchToPasswordCreatePasswordCompletedSuccessfully() {
		return trackEvent(eventFactory.eventForUserSwitchToPasswordCreatePasswordCompletedSuccessfully());
	}

	public boolean trackUserChangePasscodeCreatePasscodeCompletedSuccessfully() {
		return trackEvent(eventFactory.eventForUserChangePasscodeCreatePasscodeCompletedSuccessfully());
	}

	public boolean trackUserChangePasswordCreatePasswordCompletedSuccessfully() {
		return trackEvent(eventFactory.eventForUserChangePasswordCreatePasswordCompletedSuccessfully());
	}

	public boolean trackCarouselCompletedLogin(List<? extends Product> products, boolean accessibilityEnabled) {
		return trackEvent(eventFactory.eventForCarouselCompletedLogin(products, accessibilityEnabled));
	}

	public boolean trackCarouselProductShown(Product product, boolean isDetail) {
		return trackEvent(eventFactory.eventForCarouselProductShown(product, isDetail));
	}
	
	public boolean trackSeasonalEffectStarted() {
		return trackEvent(eventFactory.eventForSeasonalEffectStarted());
	}

	public boolean trackSeasonalEffectStoppedManually() {
		return trackEvent(eventFactory.eventForSeasonalEffectStoppedManually());
	}

	public boolean trackSeasonalEffectStoppedAutomatically() {
		return trackEvent(eventFactory.eventForSeasonalEffectStoppedAutomatically());
	}
	
	public boolean trackViewTransactions(Product product, int pageNumber) {
		return trackEvent(eventFactory.eventForViewTransactions(product, pageNumber));
	}

	public boolean trackViewTransactionDetails(Product product) {
		return trackEvent(eventFactory.eventForViewTransactionDetails(product));
	}

	public boolean trackViewATM() {
		return trackEvent(eventFactory.eventForViewATM());
	}

	public boolean trackPassiveError(ErrorResponse error) {
		return trackEvent(eventFactory.eventForPassiveError(error));
	}

	public boolean trackScreenErrorCommunicatingWithNetwork(ErrorResponse error) {
		return trackEvent(eventFactory.eventForScreenErrorCommunicatingWithNetwork(error));
	}
	
	public boolean trackScreenError(ErrorResponse error) {
		return trackEvent(eventFactory.eventForScreenError(error));
	}

	public void trackScreenErrorMalwareFound(ErrorResponse error) {
		trackEventGroupsNotYetAvailable(eventFactory.eventForScreenError(error));
	}
	
	public boolean trackDocument(Document document, boolean registered, boolean usesPassword, DocumentStatus status) {
		return trackEvent(eventFactory.eventForDocument(document, registered, usesPassword, status));
	}

	public boolean trackClickedUrlInDocument(String url) {
		return trackEvent(eventFactory.eventForClickedUrlInDocument(url));
	}

	public boolean trackViewContactUs() {
		return trackEvent(eventFactory.eventForViewContactUs());
	}

	public boolean trackViewContactUsLinkClicked(String url) {
		return trackEvent(eventFactory.eventForViewContactUsLinkClicked(url));
	}

	public boolean trackViewFaq(boolean authenticated) {
		return trackEvent(eventFactory.eventForViewFaq(authenticated));
	}

	public boolean trackViewFaqLinkClicked(boolean authenticated, String url) {
		return trackEvent(eventFactory.eventForViewFaqLinkClicked(authenticated, url));
	}

	public boolean trackViewTermsAndConditions() {
		return trackEvent(eventFactory.eventForViewTermsAndConditions());
	}

	public boolean trackViewTermsAndConditionsLinkClicked(String url) {
		return trackEvent(eventFactory.eventForViewTermsAndConditionsLinkClicked(url));
	}

	public boolean trackExportTerms() {
		return trackEvent(eventFactory.eventForExportTerms());
	}

	public boolean trackATMFinderList() {
		return trackEvent(eventFactory.eventForATMFinderList());
	}

	public boolean trackATMDetail(Store store) {
		return trackEvent(eventFactory.eventForATMDetail(store));
	}

	public boolean trackATMFinderMap() {
		return trackEvent(eventFactory.eventForATMFinderMap());
	}

	public boolean trackViewTransactionsListSorting(String sort, ProductDetails product) {
		return trackEvent(eventFactory.eventForViewTransactionsListSort(sort, product));
	}

	public boolean trackViewTransactionsListSearch(ProductDetails product, String text) {
		return trackEvent(eventFactory.eventForViewTransactionsListSearch(text, product));
	}

	public boolean trackTransactionCalendar(String monthYearViewed) {
		return trackEvent(eventFactory.eventForViewTransactionCalendar(monthYearViewed));
	}

	public boolean trackViewStatement(Product product, int cycleNumber, boolean exported) {
		return trackEvent(eventFactory.eventForViewStatement(product, cycleNumber, exported));
	}

	public boolean trackMakePayment(Payment payment) {
		return trackEvent(eventFactory.eventForMakePayment(payment));
	}

	public boolean trackMakeTransfer(Transfer transfer, boolean isPayNoPayTransfer) {
		return trackEvent(eventFactory.eventForMakeTransfer(transfer, isPayNoPayTransfer));
	}

	public boolean trackMakeSaving(Saving saving) {
		return trackEvent(eventFactory.eventForMakeSaving(saving));
	}

	public boolean trackMakePaymentCompletedSuccessfully(Payment payment) {
		return trackEvent(eventFactory.eventForMakePaymentCompletedSuccessfully(payment));
	}

	public boolean trackMakeTransferCompletedSuccessfully(Transfer transfer) {
		return trackEvent(eventFactory.eventForMakeTransferCompletedSuccessfully(transfer));
	}

	public boolean trackMoveMoneyCompletedUnsuccessfully(MoneyMovement<ProductDetails, ?> moneyMovement) {
		return trackEvent(eventFactory.eventForMoveMoneyCompletedUnsuccessfully(moneyMovement));
	}

	public boolean trackMakeSavingCompletedSuccessfully(Saving saving) {
		return trackEvent(eventFactory.eventForMakeSavingCompletedSuccessfully(saving));
	}

	public boolean trackMakeSavingCompletedUnsuccessfully(Saving saving) {
		return trackEvent(eventFactory.eventForMakeSavingCompletedUnsuccessfully(saving));
	}

	public boolean trackThreeDSecure() {
		return trackEvent(eventFactory.eventForThreeDSecure());
	}

	public boolean trackPayCreditCard(ProductDetails creditCardProductDetails) {
		return trackEvent(eventFactory.eventForPayCreditCard(creditCardProductDetails));
	}
	
	public boolean trackPayCreditCardSelectDebitCard() {
		return trackEvent(eventFactory.eventForPayCreditCardSelectDebitCard());
	}

	public boolean trackPayCreditCardEnterCvv() {
		return trackEvent(eventFactory.eventForPayCreditCardEnterCvv());
	}

	public boolean trackPayCreditCardConfirmation(MoneyMovement<PaymentMethod, ProductDetails> payment, String creditCardPaymentType) {
		return trackEvent(eventFactory.eventForPayCreditCardConfirmation(payment, creditCardPaymentType));
	}
	
	public boolean trackPayCreditCardCompletedSuccessfully(CreditCardPayment payment) {
		return trackEvent(eventFactory.eventForPayCreditCardCompletedSuccessfully(payment));
	}
	
	public boolean trackPayCreditCardCompletedUnsuccessfully(CreditCardPayment payment) {
		return trackEvent(eventFactory.eventForPayCreditCardCompletedUnsuccessfully(payment));
	}
	
	public boolean trackAddDebitCard() {
		return trackEvent(eventFactory.eventForAddDebitCard());
	}

	public boolean trackAddDebitCardConfirmation() {
		return trackEvent(eventFactory.eventForAddDebitCardConfirmation());
	}
	
	public boolean trackAddDebitCardCompletedSuccessfully(Product product) {
		return trackEvent(eventFactory.eventForAddDebitCardCompletedSuccessfully(product));
	}

	public boolean trackDeleteDebitCardConfirmation() {
		return trackEvent(eventFactory.eventForDeleteDebitCardConfirmation());
	}
	
	public boolean trackDeleteDebitCardCompletedSuccessfully(Product product) {
		return trackEvent(eventFactory.eventForDeleteDebitCardCompletedSuccessfully(product));
	}
	
	public boolean trackViewRegularPayments(ProductDetails productDetails) {
		return trackEvent(eventFactory.eventForViewRegularPayments(productDetails));
	}

	public boolean trackViewSatementArchive(ProductDetails productDetails, String activityLaunchedFrom) {
		return trackEvent(eventFactory.eventForViewStatementArchive(productDetails, activityLaunchedFrom));
	}

	public boolean trackViewTutorialScreen(String screenName) {
		return trackEvent(eventFactory.eventForViewTutorialScreen(screenName));
	}

	public boolean trackAlertDisplayed(String name, String message) {
		return trackEvent(eventFactory.eventForAlertDisplayed(name, message));
	}

	public boolean trackViewSettings() {
		return trackEvent(eventFactory.eventForViewSettings());
	}
	
	public boolean trackViewBalancePeekSetup() {
		return trackEvent(eventFactory.eventForViewBalancePeekSetup());
	}	
	
	public boolean trackViewBalancePeek(int numOfAccounts) {
		return trackEvent(eventFactory.eventForViewBalancePeek(numOfAccounts));
	}
	
	public boolean trackBalancePeekEnabledSubmitted(List<String> names) {
		return trackEvent(eventFactory.eventForBalancePeekEnabledSubmitted(names));
	}

	public boolean trackBalancePeekDisabledSubmitted() {
		return trackEvent(eventFactory.eventForBalancePeekDisabledSubmitted());
	}
	
	public boolean trackBalancePeekSubmitPreferencesProcessCompleted(boolean success) {
		return trackEvent(eventFactory.eventForBalancePeekSubmitPreferencesProcessCompleted(success));
	}

	public boolean trackMenuItemSelected(String title) {
		return trackEvent(eventFactory.eventForMenuItemSelected(title));
	}

	public boolean trackToggleToListView() {
		return trackEvent(eventFactory.eventForToggleToListView());
	}

	public boolean trackToggleToCarousel() {
		return trackEvent(eventFactory.eventForToggleToCarousel());
	}
}
