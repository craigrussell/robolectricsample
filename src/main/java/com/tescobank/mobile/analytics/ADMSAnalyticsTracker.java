package com.tescobank.mobile.analytics;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.adobe.mobile.Analytics;
import com.adobe.mobile.Config;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.application.TescoLocationProvider;

class ADMSAnalyticsTracker implements AnalyticsTracker {

	private static final String TAG = ADMSAnalyticsTracker.class.getName();
	
	private List<AnalyticsGroup> enabledGroups;
	
	private final TescoLocationProvider locationProvider;

	public ADMSAnalyticsTracker(TescoLocationProvider locationProvider) {
		this.locationProvider = locationProvider;		
		enabledGroups = new LinkedList<AnalyticsGroup>();
	}

	@Override
	public void setEnabledTrackingGroups(List<AnalyticsGroup> groups) {
		this.enabledGroups = groups;
	}

	@Override
	public void startTracking(Context context) {
		Config.setContext(context);
		Config.collectLifecycleData();
	}

	@Override
	public boolean trackEvent(AnalyticsEvent event) {
		boolean enabled = enabledGroups.contains(event.getGroup());
		if (!enabled) {
			return false;
		}

		trackEventState(event);
		return true;
	}
	
	@Override
	public boolean trackEventGroupsNotYetAvailable(AnalyticsEvent event) {
		if(enabledGroups == null || enabledGroups.isEmpty()) {
			trackEventState(event);
			return true;
		}
		return false;
	}

	private void trackEventState(AnalyticsEvent event) {
		addLocationToEvent(event);
		Hashtable<String, Object> contextData = new Hashtable<String, Object>(event.getData());
		contextData.put("group", event.getGroup().toString());
		Analytics.trackState(event.getEventName(), contextData);
	}

	@Override
	public void stopTracking() {
		Config.pauseCollectingLifecycleData();
	}
	
	private AnalyticsEvent addLocationToEvent(AnalyticsEvent event) {
		Location location = locationProvider.getLocation();
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Location " + location);
		}
		if (null != location && locationProvider.isUserEnabled()) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Location " + location.getLatitude() + " " + location.getLongitude());
			}
			event.addData("Location", location.getLatitude() + " " + location.getLongitude());
		}
		return event;
	}
}
