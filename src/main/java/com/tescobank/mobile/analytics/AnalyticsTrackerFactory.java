package com.tescobank.mobile.analytics;

import com.tescobank.mobile.application.TescoLocationProvider;

public final class AnalyticsTrackerFactory {

	private AnalyticsTrackerFactory() {
		super();
	}

	public static AnalyticsTracker createAnalyticsTracker(TescoLocationProvider locationProvider) {
		return new ADMSAnalyticsTracker(locationProvider);
	}
}
