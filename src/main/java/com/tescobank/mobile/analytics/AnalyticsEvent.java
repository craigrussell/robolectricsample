package com.tescobank.mobile.analytics;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class AnalyticsEvent implements Serializable {

	private static final long serialVersionUID = -537411062951242546L;

	private AnalyticsGroup group;
	private String eventName;
	private Map<String, String> data;

	public AnalyticsEvent(AnalyticsGroup group, String eventName) {
		this.group = group;
		this.eventName = eventName;
		this.data = new HashMap<String, String>();
	}

	public void addData(String name, String value) {
		data.put(name, value);
	}

	/**
	 * @return the group
	 */
	public AnalyticsGroup getGroup() {
		return group;
	}

	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @return the data
	 */
	public Map<String, String> getData() {
		return data;
	}
}
