package com.tescobank.mobile.helper;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.payment.RetrieveFundingAccountsResponse.Account;
import com.tescobank.mobile.api.payment.RetrieveFundingAccountsResponse.Account.Transactee.BankAccount;
import com.tescobank.mobile.model.product.FundingAccount;
import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.sort.FundingAccountComparator;

public final class ProductHelper {

	private static final String TAG = ProductHelper.class.getSimpleName();
	
	private ProductHelper() {
		super();
	}

	public static List<ProductDetails> updateProductDetailsWithSummaryData(List<Product> products, List<ProductDetails> productDetails) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting updateProductDetailsWithSummaryData");
		}

		if (haveProductsAndDetails(products, productDetails)) {
			return handleProductsAndDetails(products, productDetails);
		} else {
			return handleNoProductsOrDetails(productDetails);
		}
	}
	
	private static boolean haveProductsAndDetails(List<Product> products, List<ProductDetails> productDetails) {
		return (products != null) && !products.isEmpty() && (productDetails != null) && !productDetails.isEmpty();
	}
	
	private static List<ProductDetails> handleProductsAndDetails(List<Product> products, List<ProductDetails> productDetails) {
		Map<String, Product> productMap = new HashMap<String, Product>();
		
		for (Product product : products) {
			productMap.put(product.getProductId(), product);
		}
		
		return setProductDetailsFromProduct(productMap, productDetails);
	}
	
	private static List<ProductDetails> handleNoProductsOrDetails(List<ProductDetails> productDetails) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Empty Product or Product Detail");
		}
		return productDetails;
	}

	private static List<ProductDetails> setProductDetailsFromProduct(Map<String, Product> productMap, List<ProductDetails> productDetails) {

		Product currentProduct = null;

		for (Product prodDetails : productDetails) {
			currentProduct = productMap.get(prodDetails.getProductId());
			if (currentProduct != null) {
				prodDetails.setAccountNumber(currentProduct.getAccountNumber());
				prodDetails.setSortCode(currentProduct.getSortCode());
				prodDetails.setCustomerId(currentProduct.getCustomerId());

			} else {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "Cannot find matching productDetail in product list. Product Details ID is " + prodDetails.getProductId());
				}
			}
		}

		return productDetails;

	}

	public static void updateProductDetailsWithFundingAccounts(List<ProductDetails> productDetails, Account[] accounts) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting updateProductDetailsWithFundingAccounts");
		}
		
		Map<String, ProductDetails> products = getProducts(productDetails);
		
		addFundingAccountsToProducts(products, accounts);
		
		sortFundingAccounts(productDetails);
	}
	
	private static Map<String, ProductDetails> getProducts(List<ProductDetails> productDetails) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Getting products");
		}
		
		Map<String, ProductDetails> products = new HashMap<String, ProductDetails>();
		
		for (ProductDetails product : productDetails) {
			
			String accountNumber = product.getAccountNumber();
			
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Found product with account number = " + accountNumber);
			}
			
			products.put(accountNumber, product);
		}
		
		return products;
	}
	
	private static void addFundingAccountsToProducts(Map<String, ProductDetails> products, Account[] accounts) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating funding accounts to products");
		}
		
		for (Account account : accounts) {
			
			ProductDetails product = products.get(account.getDestinationAccountNumber());
			if (product != null) {
				handleFundingAccount(account, product);
			} else {
				handleNoFundingAccount(account);
			}
		}
	}
	
	private static void handleFundingAccount(Account account, ProductDetails product) {
		FundingAccount fundingAccount = createFundingAccount(account.getTransactee().getBankAccount());
		product.getFundingAccounts().add(fundingAccount);
		
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Added funding account with account number = " + fundingAccount.getAccountNumber() + " to product with account nunber = " + product.getAccountNumber());
		}
	}
	
	private static void handleNoFundingAccount(Account account) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Funding account with destination account number = " + account.getDestinationAccountNumber()  + " not associated with any products");
		}
	}
	
	private static FundingAccount createFundingAccount(BankAccount bankAccount) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating funding account");
		}
		
		String accountNumber = bankAccount.getAccountNumber();
		
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Found funding account with account number = " + accountNumber);
		}
		
		String name = bankAccount.getDisplayName();
		
		FundingAccount fundingAccount = new FundingAccount();
		fundingAccount.setName(name);
		fundingAccount.setAccountNumber(accountNumber);
		fundingAccount.setSortCode(bankAccount.getSortCode());
		
		return fundingAccount;
	}
	
	private static void sortFundingAccounts(List<ProductDetails> productDetails) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Sorting funding accounts");
		}
		
		FundingAccountComparator comparator = new FundingAccountComparator();
		for (ProductDetails product : productDetails) {
			
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Sorting funding accounts for product with account number = " + product.getAccountNumber());
			}
			
			Collections.sort(product.getFundingAccounts(), comparator);
		}
	}
}
