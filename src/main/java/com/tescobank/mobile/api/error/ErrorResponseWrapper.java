package com.tescobank.mobile.api.error;

import java.io.Serializable;

import com.tescobank.mobile.api.Display;

/**
 * 'Error' response wrapper
 */
public class ErrorResponseWrapper implements Serializable {

	private static final long serialVersionUID = -8254841670649178425L;
	private ErrorResponse errorResponse;

	public ErrorResponseWrapper() {
		super();
	}

	public ErrorResponse getErrorResponse() {
		return errorResponse;
	}

	public void setErrorResponse(ErrorResponse errorResponse) {
		this.errorResponse = errorResponse;
	}

	/**
	 * ErrorResponse inner class
	 */
	public static class ErrorResponse implements Serializable {

		private static final long serialVersionUID = 6181557548299061382L;

		private static final Display NULL_DISPLAY = new Display();

		public static final String ERROR_TYPE_FATAL = "fatalError";

		public static final String ERROR_TYPE_PASSIVE = "passiveError";

		public static final String ERROR_TYPE_RETRY = "retryError";
		
		private static final String ERROR_CODE_PASSWORD_CHANGED_ONLINE = "TVI-01";
		
		private static final String ERROR_CODE_CHANNEL_LIMITS_BREACHED = "BCL-01";
		
		private static final String ERROR_CODE_INSUFFICIENT_FUNDS = "IFE-01";
		
		private static final String ERROR_CODE_GENERIC_DECLINED_SAVE_NOW = "EAF-01";
		
		private static final String ERROR_CODE_GENERIC_DECLINED_PAYMENTS_AND_TRANSFERS = "ETP-01";

		private static final String ERROR_CODE_GENERIC_DECLINED_CARDS = "EPM-01";

		private static final String ERROR_CODE_INITIAL_CONNECTIVITY = "NCE-01";
		
		private static final String ERROR_CODE_CONNECTIVITY_LOST = "INAPP-01";

		private static final String ERROR_CODE_SERVER_NOT_RESPONDING = "SNR-01";
		
		private static final String ERROR_CODE_AUTH_FAILURE = "AUT-01";
		
		private static final String ERROR_CODE_MALWARE_DETECTED = "MAL-01";
		
		private static final String ERROR_CODE_GENERIC = "GENERIC";
		
		private String errorCode;
		
		private int httpStatusCode;

		private String errorType;

		private Display display;
		
		/** client-side property indicating that this is an information message rather than an error */
		private boolean isInformationMessage;

		public ErrorResponse() {
			super();
		}

		public boolean isErrorTypeFatal() {
			return ERROR_TYPE_FATAL.equals(errorType);
		}

		public boolean isErrorTypePassive() {
			return ERROR_TYPE_PASSIVE.equals(errorType);
		}

		public boolean isErrorTypeRetry() {
			return ERROR_TYPE_RETRY.equals(errorType);
		}
		
		public boolean isAuthFailure() {
			return ERROR_CODE_AUTH_FAILURE.equals(errorCode);
		}
		
		public boolean isErrorPasswordChangedOnline() {
			return ERROR_CODE_PASSWORD_CHANGED_ONLINE.equals(errorCode);
		}
		
		public boolean isErrorChannelLimitsBreached() {
			return ERROR_CODE_CHANNEL_LIMITS_BREACHED.equals(errorCode);
		}
		
		public boolean isErrorInsufficientFunds() {
			return ERROR_CODE_INSUFFICIENT_FUNDS.equals(errorCode);
		}
		
		public boolean isErrorSaveNowGenericDeclined() {
			return ERROR_CODE_GENERIC_DECLINED_SAVE_NOW.equals(errorCode);		
		}
		
		public boolean isErrorPaymentTransferGenericDeclined() {
			return ERROR_CODE_GENERIC_DECLINED_PAYMENTS_AND_TRANSFERS.equals(errorCode);
		}

		public boolean isErrorCardsGenericDeclined() {
			return ERROR_CODE_GENERIC_DECLINED_CARDS.equals(errorCode);
		}

		public boolean isDeclined() {
			return isErrorChannelLimitsBreached() || isErrorInsufficientFunds() || isErrorPaymentTransferGenericDeclined() || isErrorSaveNowGenericDeclined() ||isErrorCardsGenericDeclined();
		}
	
		public boolean isErrorNoInternetConnectionAtLogin() {
			return ERROR_CODE_INITIAL_CONNECTIVITY.equals(errorCode);
		}
		
		public boolean isErrorInternetConnectionLost() {
			return ERROR_CODE_CONNECTIVITY_LOST.equals(errorCode);
		}

		/**
		 * Internet connectivity seems ok however did not receive a correct status from the server
		 */
		public boolean isErrorServerResponseStatus() {
			return ERROR_CODE_SERVER_NOT_RESPONDING.equals(errorCode);
		}
		
		/**
		 * Internet connectivity seems ok however did not receive correct content
		 */
		public boolean isErrorServerResponseContent() {
			return ERROR_CODE_GENERIC.equals(errorCode);
		}
		
		/**
		 * Internet connectivity seems ok however did not receive any/correct response from server
		 */
		public boolean isErrorCommunicatingWithServer() {
			return isErrorServerResponseStatus() || isErrorServerResponseContent();
		}
		
		/**
		 * Malware detected on a customer's device
		 */
		public boolean isErrorMalwareDetected() {
			return ERROR_CODE_MALWARE_DETECTED.equals(errorCode);
		}
		
		public String getErrorCode() {
			return errorCode;
		}

		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}

		public String getErrorType() {
			return errorType;
		}

		public void setErrorType(String errorType) {
			this.errorType = errorType;
		}

		/**
		 * Return the display information from the server or an empty display object if the server has not provided one.
		 * 
		 * @return never null
		 */
		public Display getDisplay() {
			return display != null ? display : NULL_DISPLAY;
		}

		public void setDisplay(Display display) {
			this.display = display;
		}
		
		public boolean getIsInformationMessage() {
			return isInformationMessage;
		}

		public void setIsInformationMessage(boolean isInformationMessage) {
			this.isInformationMessage = isInformationMessage;
		}
		
		public int getHttpStatusCode() {
			return httpStatusCode;
		}

		public void setHttpStatusCode(int httpStatusCode) {
			this.httpStatusCode = httpStatusCode;
		}
		
		public boolean hasHttpStatusCode() {
			return httpStatusCode != 0;
		}
	}
}
