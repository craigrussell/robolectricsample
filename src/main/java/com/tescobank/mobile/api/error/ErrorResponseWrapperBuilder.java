package com.tescobank.mobile.api.error;

import org.apache.http.HttpStatus;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.Display;
import com.tescobank.mobile.api.Display.CustomerService;
import com.tescobank.mobile.api.error.ErrorResponseWrapper.ErrorResponse;
import com.tescobank.mobile.rest.request.RestErrorResponseBuilder;

/**
 * Error response wrapper builder
 */
public class ErrorResponseWrapperBuilder implements RestErrorResponseBuilder<ErrorResponseWrapper> {
	
	/** The context */
	private Context context;
	
	/** Supported in-app errors */
	public static enum InAppError {
		
		HttpStatus403(R.string.error_code_403, R.string.error_display_title_403, R.string.error_display_message_403, ErrorResponse.ERROR_TYPE_FATAL),
		HttpStatus200(R.string.error_code_generic, R.string.error_display_title_generic, R.string.error_display_message_generic, ErrorResponse.ERROR_TYPE_RETRY),
		HttpStatusNon200(R.string.error_code_non_200, R.string.error_display_title_non_200, R.string.error_display_message_non_200, ErrorResponse.ERROR_TYPE_RETRY),
		NoInternetConnectionAtLogin(R.string.error_code_no_initial_connectivity, R.string.error_display_title_no_initial_connectivity, R.string.error_display_message_no_initial_connectivity, ErrorResponse.ERROR_TYPE_RETRY),
		NoInternetConnection(R.string.error_code_no_connectivity, R.string.error_display_title_no_connectivity, R.string.error_display_message_no_connectivity, ErrorResponse.ERROR_TYPE_RETRY),
		InternetConnectionLost(R.string.error_code_connectivity_lost, R.string.error_display_title_connectivity_lost, R.string.error_display_message_connectivity_lost, ErrorResponse.ERROR_TYPE_PASSIVE),
		InternetConnectionRegained(R.string.error_code_connectivity_regained, R.string.error_display_title_connectivity_regained, R.string.error_display_message_connectivity_regained, ErrorResponse.ERROR_TYPE_PASSIVE, true),
		BlankUsername(R.string.error_code_blank_username, R.string.error_display_title_blank_username, R.string.error_display_message_blank_username, ErrorResponse.ERROR_TYPE_PASSIVE),
		BlankPassword(R.string.error_code_blank_password, R.string.error_display_title_blank_password, R.string.error_display_message_blank_password, ErrorResponse.ERROR_TYPE_PASSIVE),
		IncompletePasscode(R.string.error_code_blank_passcode, R.string.error_display_title_blank_passcode, R.string.error_display_message_blank_passcode, ErrorResponse.ERROR_TYPE_PASSIVE),
		IncompletePIN(R.string.error_code_blank_pin, R.string.error_display_title_blank_pin, R.string.error_display_message_blank_pin, ErrorResponse.ERROR_TYPE_PASSIVE),
		IncompleteSMS(R.string.error_code_blank_sms, R.string.error_display_title_blank_sms, R.string.error_display_message_blank_sms, ErrorResponse.ERROR_TYPE_PASSIVE),
		PasscodeMismatch(R.string.error_code_passcode_match, R.string.error_display_title_passcode_match, R.string.error_display_message_passcode_match, ErrorResponse.ERROR_TYPE_PASSIVE),
		PasswordMismatch(R.string.error_code_password_match, R.string.error_display_title_password_match, R.string.error_display_message_password_match, ErrorResponse.ERROR_TYPE_PASSIVE),
		PasscodeValidation(R.string.error_code_passcode_validity, R.string.error_display_title_passcode_validity, R.string.error_display_message_passcode_validity, ErrorResponse.ERROR_TYPE_PASSIVE),
		PasswordValidation(R.string.error_code_password_validity, R.string.error_display_title_password_validity, R.string.error_display_message_password_validity, ErrorResponse.ERROR_TYPE_PASSIVE),
		FormValidation(R.string.error_code_invalid_form, R.string.error_display_title_invalid_form, R.string.error_display_title_invalid_form, ErrorResponse.ERROR_TYPE_PASSIVE),
		ExpiryDateValidation(R.string.error_code_debit_expiry_in_past, R.string.error_display_title_debit_expiry_in_past, R.string.error_display_title_debit_expiry_in_past, ErrorResponse.ERROR_TYPE_PASSIVE),
		CreateAccount(R.string.error_code_arcot_account_creation_failed, R.string.error_display_title_arcot_account_creation_failed, R.string.error_display_message_arcot_account_creation_failed, ErrorResponse.ERROR_TYPE_FATAL),
		CreateAccountChangePassword(R.string.error_code_arcot_account_creation_failed_change_password, R.string.error_display_title_arcot_account_creation_failed_change_password, R.string.error_display_message_arcot_account_creation_failed_change_password, ErrorResponse.ERROR_TYPE_FATAL),
		GenerateOTP(R.string.error_code_arcot_otp_generation_failed, R.string.error_display_title_arcot_otp_generation_failed, R.string.error_display_message_arcot_otp_generation_failed, ErrorResponse.ERROR_TYPE_FATAL),
		ChangePin(R.string.error_code_arcot_set_pin_failed, R.string.error_display_title_arcot_set_pin_failed, R.string.error_display_message_arcot_set_pin_failed, ErrorResponse.ERROR_TYPE_FATAL), 
		LocationSearchNoResults(R.string.error_code_location_search_failed, R.string.error_display_title_location_search_failed, R.string.error_display_message_location_search_failed, ErrorResponse.ERROR_TYPE_PASSIVE),
		NoMoreTransactions(R.string.error_code_no_more_transactions, R.string.error_display_title_no_more_transactions, R.string.error_display_message_no_more_transactions, ErrorResponse.ERROR_TYPE_PASSIVE),
		ThreeDSecureNotLoaded(R.string.error_code_3d_secure_did_not_load, R.string.error_display_title_3d_secure_did_not_load, R.string.error_display_message_3d_secure_did_not_load, ErrorResponse.ERROR_TYPE_RETRY),
		NoDebitCards(R.string.error_code_no_debit_cards, R.string.error_display_title_no_debit_cards, R.string.error_display_message_no_debit_cards, ErrorResponse.ERROR_TYPE_PASSIVE), 
		NoActiveCards(R.string.error_code_no_active_cards, R.string.error_display_title_no_active_cards, R.string.error_display_message_no_active_cards, ErrorResponse.ERROR_TYPE_PASSIVE), 
		MoneyMovementDateBeforeToday(R.string.error_code_movemoney_date_before_today, R.string.error_display_title_movemoney_date_before_today, R.string.error_display_message_movemoney_date_before_today, ErrorResponse.ERROR_TYPE_PASSIVE),
		MoneyMovementDateBeforeTomorrow(R.string.error_code_movemoney_date_before_tomorrow, R.string.error_display_title_movemoney_date_before_tomorrow, R.string.error_display_message_movemoney_date_before_tomorrow, ErrorResponse.ERROR_TYPE_PASSIVE),
		MoneyMovementDateAfterOneYear(R.string.error_code_movemoney_date_after_one_year, R.string.error_display_title_movemoney_date_after_one_year, R.string.error_display_message_movemoney_date_after_one_year, ErrorResponse.ERROR_TYPE_PASSIVE),
		CannotPayZero(R.string.error_code_cannot_pay_zero, R.string.error_display_title_cannot_pay_zero, R.string.error_display_message_cannot_pay_zero, ErrorResponse.ERROR_TYPE_PASSIVE),
		NoFundingAccounts(R.string.error_code_no_funding_accounts, R.string.error_display_title_no_funding_accounts, R.string.error_display_message_no_funding_accounts, ErrorResponse.ERROR_TYPE_PASSIVE), 
		UnableToWriteFile(R.string.error_code_unable_to_write_file, R.string.error_display_title_unable_to_write_file, R.string.error_display_message_unable_to_write_file, ErrorResponse.ERROR_TYPE_PASSIVE),
		CriticalMalwareDetected(R.string.error_code_critical_malware_detected, R.string.error_display_title_critical_malware_detected, R.string.error_display_message_critical_malware_detected, ErrorResponse.ERROR_TYPE_FATAL);
		
		private int code;
		private int title;
		private int message;
		private String type;
		private boolean isInformationMessage;
		
		private InAppError(int code, int title, int message, String type) {
			this(code, title, message, type, false);
		};
		
		private InAppError(int code, int title, int message, String type, boolean isInformationMessage) {
			this.code = code;
			this.title = title;
			this.message = message;
			this.type = type;
			this.isInformationMessage = isInformationMessage;
		};
		
		public int getCode() {
			return code;
		}
		
		public int getTitle() {
			return title;
		}
		
		public int getMessage() {
			return message;
		}
		
		public String getType() {
			return type;
		}
		
		public boolean getIsInformationMessage() {
			return isInformationMessage;
		}
	};

	/**
	 * Constructor
	 * 
	 * @param Context context;
	 */
	public ErrorResponseWrapperBuilder(Context context) {
		super();

		this.context = context;
	}
	
	@Override
	public ErrorResponseWrapper buildErrorResponse(int httpStatusCode) {
		ErrorResponseWrapper error = null;
		switch (httpStatusCode) {
			case HttpStatus.SC_FORBIDDEN:
				error = buildErrorResponse(InAppError.HttpStatus403);
				break;
			case HttpStatus.SC_OK:
				error = buildErrorResponse(InAppError.HttpStatus200);
				break;
			default:
				error = buildErrorResponse(InAppError.HttpStatusNon200);
		}
		error.getErrorResponse().setHttpStatusCode(httpStatusCode);
		return error;
	}
	
	@Override
	public ErrorResponseWrapper buildErrorResponse(InAppError inAppError) {
		return buildErrorResponse(context.getString(inAppError.getCode()), context.getString(inAppError.getTitle()), context.getString(inAppError.getMessage()), inAppError.getType(), inAppError.isInformationMessage);
	}
	
	private ErrorResponseWrapper buildErrorResponse(String errorCode, String displayTitle, String displayMessage, String errorType, boolean isInformationMessage) {
		
		ErrorResponseWrapper errorResponseWrapper = new ErrorResponseWrapper();

		errorResponseWrapper.setErrorResponse(new ErrorResponse());
		
		errorResponseWrapper.getErrorResponse().setErrorCode(errorCode);
		
		errorResponseWrapper.getErrorResponse().setDisplay(new Display());
		errorResponseWrapper.getErrorResponse().getDisplay().setCustomerService(new CustomerService());
		
		errorResponseWrapper.getErrorResponse().getDisplay().setTitle(displayTitle);
		errorResponseWrapper.getErrorResponse().getDisplay().setMessage(displayMessage);
		
		errorResponseWrapper.getErrorResponse().getDisplay().getCustomerService().setMessage(context.getString(R.string.error_display_customerservice_message));
		errorResponseWrapper.getErrorResponse().getDisplay().getCustomerService().setPhoneNumber(context.getString(R.string.error_display_customerservice_phonenumber));
		errorResponseWrapper.getErrorResponse().getDisplay().getCustomerService().setUrl(context.getString(R.string.error_display_customerservice_url));
		
		errorResponseWrapper.getErrorResponse().setErrorType(errorType);
		errorResponseWrapper.getErrorResponse().setIsInformationMessage(isInformationMessage);

		return errorResponseWrapper;
	}

	@Override
	public Class<ErrorResponseWrapper> errorResponseClass() {
		return ErrorResponseWrapper.class;
	}
}
