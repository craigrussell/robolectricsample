package com.tescobank.mobile.api.user;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'get user IDs' requests
 */
public abstract class GetUserIDsRequest extends ApiRequest<GetUserIDsResponse> {
	
	private String uID;
	
	public GetUserIDsRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.GET;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/users/ids/:uID";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		
		urlParams.put(":uID", uID);
		
		return urlParams;
	}
	
	@Override
	public Class<GetUserIDsResponse> getResponseObjectClass() {
		return GetUserIDsResponse.class;
	}

	public String getUID() {
		return uID;
	}

	public void setUID(String uID) {
		this.uID = uID;
	}
}
