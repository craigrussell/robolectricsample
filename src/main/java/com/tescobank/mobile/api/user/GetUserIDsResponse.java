package com.tescobank.mobile.api.user;

/**
 * 'Get user IDs' response
 */
public class GetUserIDsResponse {
	
	private String cssoId;
	
	private String tbId;

	public GetUserIDsResponse() {
		super();
	}
	
	public String getCssoId() {
		return cssoId;
	}

	public void setCssoId(String cssoID) {
		this.cssoId = cssoID;
	}

	public String getTbId() {
		return tbId;
	}

	public void setTbId(String tbID) {
		this.tbId = tbID;
	}
}
