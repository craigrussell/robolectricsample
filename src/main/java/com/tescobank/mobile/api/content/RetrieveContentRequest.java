package com.tescobank.mobile.api.content;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'retrieve content' requests
 */
public abstract class RetrieveContentRequest extends ApiRequest<RetrieveContentResponse> {
	
	private String tbId;
	
	private Journey journeyID;
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.GET;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/content/request/:tbId/:journeyID";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		
		urlParams.put(":tbId", (tbId == null) ? "null" : tbId);
		urlParams.put(":journeyID", (journeyID == null) ? null : journeyID.toString());
		
		return urlParams;
	}
	
	@Override
	public Class<RetrieveContentResponse> getResponseObjectClass() {
		return RetrieveContentResponse.class;
	}

	public String getTbId() {
		return tbId;
	}

	public void setTbId(String tbId) {
		this.tbId = tbId;
	}

	public Journey getJourneyID() {
		return journeyID;
	}

	public void setJourneyID(Journey journeyID) {
		this.journeyID = journeyID;
	}
	
	/**
	 * Supported journeys
	 */
	public static enum Journey {
		
		TERMS_FIRST_TIME_LOGIN("termsFirstTimeLogin"),
		TERMS_LOGIN("termsLogin");
		
		private String journey;
		
		private Journey(String journey) {
			this.journey = journey;
		}
		
		@Override
		public String toString() {
			return journey;
		}
	}
}
