package com.tescobank.mobile.api.content;

import java.io.Serializable;
import java.util.List;

/**
 * 'Retrieve content' response
 */
public class RetrieveContentResponse implements Serializable {

	private static final long serialVersionUID = 5404648571438363644L;

	private List<Document> documents;

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	/**
	 * Documents inner class
	 */
	public static class Document implements Serializable {

		private static final long serialVersionUID = -1368242487439956438L;

		private static final String THEME_CHEVRON = "chevron";

		private static final String THEME_CREME = "creme";

		private String docID;

		private Boolean mandatory;

		private String htmlURL;

		private String pdfURL;

		private String theme;

		private Integer version;
		
		/** Whether the document has been acknowledged */
		private boolean acknowledged;

		public Document() {
			super();
		}

		public boolean isThemeChevron() {
			return THEME_CHEVRON.equals(theme);
		}

		public boolean isThemeCreme() {
			return THEME_CREME.equals(theme);
		}

		public String getDocID() {
			return docID;
		}

		public void setDocID(String docID) {
			this.docID = docID;
		}

		public Boolean isMandatory() {
			return mandatory;
		}

		public void setMandatory(Boolean mandatory) {
			this.mandatory = mandatory;
		}

		public String getHtmlURL() {
			return htmlURL;
		}

		public void setHtmlURL(String htmlURL) {
			this.htmlURL = htmlURL;
		}

		public String getPdfURL() {
			return pdfURL;
		}

		public void setPdfURL(String pdfURL) {
			this.pdfURL = pdfURL;
		}

		public String getTheme() {
			return theme;
		}

		public void setTheme(String theme) {
			this.theme = theme;
		}

		public Integer getVersion() {
			return version;
		}

		public void setVersion(Integer version) {
			this.version = version;
		}
		
		public boolean isAcknowledged() {
			return acknowledged;
		}

		public void setAcknowledged(boolean acknowledged) {
			this.acknowledged = acknowledged;
		}
	}
}
