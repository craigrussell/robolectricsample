package com.tescobank.mobile.api.content;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'acknowledge content' requests
 */
public abstract class AcknowledgeContentRequest extends ApiRequest<AcknowledgeContentResponse> {
	
	private String contentID;
	
	private Integer contentversion;
	
	private String tbId;
	
	public AcknowledgeContentRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.GET;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/content/response/:contentID/:contentversion/:tbId";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		
		urlParams.put(":contentID", contentID);
		urlParams.put(":contentversion", contentversion);
		urlParams.put(":tbId", tbId);
		
		return urlParams;
	}
	
	@Override
	public Class<AcknowledgeContentResponse> getResponseObjectClass() {
		return AcknowledgeContentResponse.class;
	}

	public String getContentID() {
		return contentID;
	}

	public void setContentID(String contentID) {
		this.contentID = contentID;
	}

	public Integer getContentversion() {
		return contentversion;
	}

	public void setContentversion(Integer contentversion) {
		this.contentversion = contentversion;
	}

	public String getTbId() {
		return tbId;
	}

	public void setTbId(String tbId) {
		this.tbId = tbId;
	}
}
