package com.tescobank.mobile.api.content;

/**
 * 'Acknowledge content' response
 */
public class AcknowledgeContentResponse {
	
	private Boolean contentAccepted;
	
	public AcknowledgeContentResponse() {
		super();
	}

	public Boolean getContentAccepted() {
		return contentAccepted;
	}

	public void setContentAccepted(Boolean contentAccepted) {
		this.contentAccepted = contentAccepted;
	}
}
