package com.tescobank.mobile.api.deviceregistration;

/**
 * 'Check device' response
 */
public class CheckDeviceResponse {

	private String deviceID;

	public CheckDeviceResponse() {
		super();
	}
	
	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
}
