package com.tescobank.mobile.api.deviceregistration;

import java.util.Map;

import com.tescobank.mobile.application.DeviceMetrics;
import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'check device' requests
 */
public abstract class CheckDeviceRequest extends ApiRequest<CheckDeviceResponse> {
	
	private String cssoID;
	private DeviceMetrics deviceMetrics;
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}

	@Override
	public String getResourceIdentifier() {
		return "/checkDevice";
	}

	@Override
	public Map<String, Object> getBodyParams() {
		Map<String, Object> bodyParams = super.getBodyParams();
		
		if (cssoID != null) {
			bodyParams.put("cssoID", cssoID);
		}
		if(deviceMetrics != null) {
			DeviceMetricsRequestBodyMapper mapper = new DeviceMetricsRequestBodyMapper(deviceMetrics);
			bodyParams.putAll(mapper.toBodyMap());
		}
		return bodyParams;
	}

	@Override
	public Class<CheckDeviceResponse> getResponseObjectClass() {
		return CheckDeviceResponse.class;
	}

	public String getCssoID() {
		return cssoID;
	}

	public void setCssoID(String cssoID) {
		this.cssoID = cssoID;
	}
	
	public void setDeviceMetrics(DeviceMetrics deviceMetrics) {
		this.deviceMetrics = deviceMetrics;
	}
	
	public DeviceMetrics getDeviceMetrics() {
		return deviceMetrics;
	}
}
