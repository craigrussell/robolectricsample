package com.tescobank.mobile.api.deviceregistration;

import java.util.Map;

import com.tescobank.mobile.application.DeviceMetrics;
import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'register device' requests
 */
public abstract class RegisterDeviceRequest extends ApiRequest<RegisterDeviceResponse> {
	
	private String cssoID;

	private DeviceMetrics deviceMetrics;
	
	public RegisterDeviceRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}

	@Override
	public String getResourceIdentifier() {
		return "/registered/:cssoID/:deviceID";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		urlParams.put(":cssoID", cssoID);
		if(deviceMetrics != null) {
			urlParams.put(":deviceID", deviceMetrics.getDeviceId());
		}
		return urlParams;
	}
	
	@Override
	public Map<String, Object> getBodyParams() {
		Map<String, Object> bodyParams = super.getBodyParams();
		if(deviceMetrics != null) {
			DeviceMetricsRequestBodyMapper mapper = new DeviceMetricsRequestBodyMapper(deviceMetrics);
			bodyParams.putAll(mapper.toBodyMap());
		}
		return bodyParams;
	}

	@Override
	public Class<RegisterDeviceResponse> getResponseObjectClass() {
		return RegisterDeviceResponse.class;
	}
	
	public String getCssoID() {
		return cssoID;
	}

	public void setCssoID(String cssoID) {
		this.cssoID = cssoID;
	}

	public void setDeviceMetrics(DeviceMetrics deviceMetrics) {
		this.deviceMetrics = deviceMetrics;
	}
	
	public DeviceMetrics getDeviceMetrics() {
		return deviceMetrics;
	}
}
