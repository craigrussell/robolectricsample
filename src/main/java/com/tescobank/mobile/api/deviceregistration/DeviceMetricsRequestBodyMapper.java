package com.tescobank.mobile.api.deviceregistration;

import java.util.HashMap;
import java.util.Map;

import com.tescobank.mobile.application.DeviceMetrics;

public class DeviceMetricsRequestBodyMapper {
	
	private DeviceMetrics metrics;
	
	public DeviceMetricsRequestBodyMapper(DeviceMetrics metrics) {
		this.metrics = metrics;
	}
		
	public Map<String, Object> toBodyMap() {
		Map<String,Object> map = new HashMap<String,Object>();
		if (metrics.getDeviceId() != null) {
			map.put("deviceId", metrics.getDeviceId());
		}
		map.put("deviceModel", metrics.getDeviceModel());
		map.put("deviceOsVersion", metrics.getDeviceOsVersion());
		map.put("deviceOsName", metrics.getDeviceOsName());
		map.put("clientAppName", metrics.getClientAppName());
		map.put("clientAppVersion", metrics.getClientAppVersion());
		map.put("language", metrics.getLanguage());
		map.put("timezone", metrics.getTimezone());
		map.put("fullHeight", metrics.getFullHeight());
		map.put("avlHeight", metrics.getAvlHeight());
		map.put("fullWidth", metrics.getFullWidth());
		map.put("avlWidth", metrics.getAvlWidth());
		map.put("mac", metrics.getMac());
		map.put("externalIP", metrics.getExternalIP());
		map.put("internalIP", metrics.getInternalIP());
		map.put("deviceIsJailbroken", metrics.getDeviceIsJailbroken());
		return map;
	}
}
