package com.tescobank.mobile.api.deviceregistration;

/**
 * 'Register device' response
 */
public class RegisterDeviceResponse {
	
	private Boolean registrationComplete;
	
	public RegisterDeviceResponse() {
		super();
	}

	public Boolean getRegistrationComplete() {
		return registrationComplete;
	}

	public void setRegistrationComplete(Boolean registrationComplete) {
		this.registrationComplete = registrationComplete;
	}
}
