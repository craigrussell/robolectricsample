package com.tescobank.mobile.api.inauth;

/**
 * 'Retrieve InAuth file status' response
 */
public class RetrieveInAuthFileStatusResponse {
	
	private static final String STATUS_OUTDATED = "outdated";
	
	private String status;
	
	private String updated_at;

	private String download_url;

	public RetrieveInAuthFileStatusResponse() {
		super();
	}
	
	public boolean isOutdated() {
		return STATUS_OUTDATED.equals(status);
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	
	public String getDownload_url() {
		return download_url;
	}

	public void setDownload_url(String download_url) {
		this.download_url = download_url;
	}
}
