package com.tescobank.mobile.api.inauth;

import static com.tescobank.mobile.rest.request.RestHttpMethod.GET;

import com.tescobank.mobile.rest.request.ByteArrayApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'retrieve InAuth file' requests
 */
public abstract class RetrieveInAuthFileRequest extends ByteArrayApiRequest {
	
	private String url;

	public RetrieveInAuthFileRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return GET;
	}
	
	@Override
	public String getBaseUrl() {
		return url;
	}
	
	@Override
	public String getResourceIdentifier() {
		return null;
	}
	
	@Override
	public boolean isResponseHeaderHandlingRequired() {
		return false;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
