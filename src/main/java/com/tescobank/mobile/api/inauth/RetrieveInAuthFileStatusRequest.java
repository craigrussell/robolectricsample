package com.tescobank.mobile.api.inauth;

import static com.tescobank.mobile.rest.request.RestHttpMethod.GET;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'retrieve InAuth file status' requests
 */
public abstract class RetrieveInAuthFileStatusRequest extends ApiRequest<RetrieveInAuthFileStatusResponse> {
	
	private String baseUrl;
	
	private String accountGuid;
	
	private InAuthFile inAuthFile;
	
	private String fileVersion;

	public RetrieveInAuthFileStatusRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return GET;
	}
	
	@Override
	public String getBaseUrl() {
		return baseUrl;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/sigfile/status/android?account_guid=:accountGuid&type=:sigType&version=:fileVersion";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		
		urlParams.put(":accountGuid", accountGuid);
		urlParams.put(":sigType", inAuthFile.getSigType());
		urlParams.put(":fileVersion", getFileVersion());
		
		return urlParams;
	}
	
	@Override
	public boolean isResponseHeaderHandlingRequired() {
		return false;
	}
	
	@Override
	public Class<RetrieveInAuthFileStatusResponse> getResponseObjectClass() {
		return RetrieveInAuthFileStatusResponse.class;
	}
	
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
	public String getAccountGuid() {
		return accountGuid;
	}

	public void setAccountGuid(String accountGuid) {
		this.accountGuid = accountGuid;
	}
	
	public InAuthFile getInAuthFile() {
		return inAuthFile;
	}

	public void setInAuthFile(InAuthFile inAuthFile) {
		this.inAuthFile = inAuthFile;
	}
	
	public String getFileVersion() {
		return fileVersion;
	}

	public void setFileVersion(String fileVersion) {
		this.fileVersion = fileVersion;
	}
	
	/**
	 * Supported InAuth files
	 */
	public static enum InAuthFile {
		
		SIGFILE("sigfile", "root"),
		MWFILE("mwfile", "malware");
		
		private String filename;
		private String sigType;
		
		private InAuthFile(String filename, String sigType) {
			this.filename = filename;
			this.sigType = sigType;
		}
		
		public String getFilename() {
			return filename;
		}
		
		public String getSigType() {
			return sigType;
		}
	}
}
