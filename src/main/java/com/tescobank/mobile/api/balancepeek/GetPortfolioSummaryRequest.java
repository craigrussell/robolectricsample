package com.tescobank.mobile.api.balancepeek;

import static com.tescobank.mobile.rest.request.RestHttpMethod.GET;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

public class GetPortfolioSummaryRequest extends ApiRequest<GetPortfolioSummaryResponse>{

	private final static String RESOURCE_IDENTIFIER = "/portfolio/summary";
	private final static String JSON_MIME_TYPE = "application/json";
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return GET;
	}

	@Override
	public String getResourceIdentifier() {
		return RESOURCE_IDENTIFIER;
	}

	@Override
	public Class<GetPortfolioSummaryResponse> getResponseObjectClass() {
		return GetPortfolioSummaryResponse.class;
	}
	
	@Override
	public String getAcceptedContentType() {
		return JSON_MIME_TYPE;
	}
}
