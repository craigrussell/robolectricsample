package com.tescobank.mobile.api.balancepeek;

import java.util.Map;

import com.tescobank.mobile.api.success.SuccessResponseWrapper;
import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

public class CheckDeviceNonInteractiveEvalRiskRequest extends ApiRequest<SuccessResponseWrapper>{
	
	private final static String RESOURCE_IDENTIFIER = "/checkdevice/noninteractive/user/:cssoId";
	private final static String CSSO_URL_PARM = ":cssoId";
	private final static String JSON_MIME_TYPE = "application/json";
	private String cssoId;

	public CheckDeviceNonInteractiveEvalRiskRequest() {
	}

	public String getCssoId() {
		return cssoId;
	}

	public void setCssoId(String cssoId) {
		this.cssoId = cssoId;
	}

	@Override
	public String getResourceIdentifier() {
		return RESOURCE_IDENTIFIER;
	}

	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> map = super.getUrlParams();
		map.put(CSSO_URL_PARM, cssoId);
		return map;
	}

	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}

	@Override
	public String getAcceptedContentType() {
		return JSON_MIME_TYPE;
	}

	@Override
	public String getBodyContentType() {
		return JSON_MIME_TYPE;
	}
	
	@Override
	public Class<SuccessResponseWrapper> getResponseObjectClass() {
		return SuccessResponseWrapper.class;
	}


}
