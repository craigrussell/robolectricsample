package com.tescobank.mobile.api.balancepeek;

import com.tescobank.mobile.api.success.SuccessResponseWrapper;
import com.tescobank.mobile.rest.request.JsonBodyApiRequest;

public class ChangeBalancePeekEnabledAccountsRequest extends JsonBodyApiRequest<ChangeBalancePeekEnabledAccountsRequestBody, SuccessResponseWrapper> {

	private static final String JSON_MIME_TYPE = "application/json";
	private static final String RESOURCE_IDENTIFIER = "/registered/product/noninteractive";
	
	public ChangeBalancePeekEnabledAccountsRequest(ChangeBalancePeekEnabledAccountsRequestBody bodyObject) {
		super(bodyObject);
	}

	@Override
	public String getResourceIdentifier() {
		return RESOURCE_IDENTIFIER;
	}

	@Override
	public String getBodyContentType() {
		return JSON_MIME_TYPE;
	}

	@Override
	public String getAcceptedContentType() {
		return JSON_MIME_TYPE;
	}

	@Override
	public Class<SuccessResponseWrapper> getResponseObjectClass() {
		return SuccessResponseWrapper.class;
	}

}
