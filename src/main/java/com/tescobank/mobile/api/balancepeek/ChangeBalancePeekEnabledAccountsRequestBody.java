package com.tescobank.mobile.api.balancepeek;

import java.util.ArrayList;
import java.util.List;

public class ChangeBalancePeekEnabledAccountsRequestBody {

	private final List<String> nonInteractiveProductTokenIds;

	public ChangeBalancePeekEnabledAccountsRequestBody() {
		nonInteractiveProductTokenIds = new ArrayList<String>();
	}
	
	public List<String> getNonInteractiveProductTokenIds() {
		return nonInteractiveProductTokenIds;
	}

	public void addTokenId(String tokenId) {
		if(!nonInteractiveProductTokenIds.contains(tokenId)) {
			nonInteractiveProductTokenIds.add(tokenId);
		}
	}
}
