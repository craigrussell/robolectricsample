package com.tescobank.mobile.api.balancepeek;

import java.util.List;

import com.tescobank.mobile.model.product.ProductDetails;

public class GetPortfolioSummaryResponse {
	private List<ProductDetails> products;

	public List<ProductDetails> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDetails> products) {
		this.products = products;
	}
}
