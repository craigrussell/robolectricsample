package com.tescobank.mobile.api.portfolio;

import java.util.Map;

import com.tescobank.mobile.api.success.SuccessResponseWrapper;
import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

public class DeleteCardPaymentMethodRequest extends ApiRequest<SuccessResponseWrapper> {

	private String productTokenID;
	private String paymentMethodTokenID;

	public DeleteCardPaymentMethodRequest() {
	}

	public String getProductTokenID() {
		return productTokenID;
	}

	public void setProductTokenID(String productTokenID) {
		this.productTokenID = productTokenID;
	}
	
	public String getPaymentMethodTokenID() {
		return paymentMethodTokenID;
	}

	public void setPaymentMethodTokenID(String paymentMethodTokenID) {
		this.paymentMethodTokenID = paymentMethodTokenID;
	}

	@Override
	public String getResourceIdentifier() {
		return "/portfolio/product/creditcard/:targetProductTokenID/transmission/inward/paymentmethods/debitcard/:paymentMethodTokenID";
	}

	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> map = super.getUrlParams();
		map.put(":targetProductTokenID", productTokenID);
		map.put(":paymentMethodTokenID", paymentMethodTokenID);
		return map;
	}

	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.DELETE;
	}

	@Override
	public String getAcceptedContentType() {
		return "application/json";
	}

	@Override
	public String getBodyContentType() {
		return null;
	}
	
	@Override
	public Class<SuccessResponseWrapper> getResponseObjectClass() {
		return SuccessResponseWrapper.class;
	}
}
