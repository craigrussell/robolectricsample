package com.tescobank.mobile.api.portfolio;

import java.util.Map;

import com.tescobank.mobile.api.success.SuccessResponseWrapper;
import com.tescobank.mobile.rest.request.JsonBodyApiRequest;

public class AddCardPaymentMethodRequest extends JsonBodyApiRequest<AddCardPaymentMethodRequestBody, SuccessResponseWrapper> {

	private String productTokenID;

	public AddCardPaymentMethodRequest(AddCardPaymentMethodRequestBody bodyObject) {
		super(bodyObject);
	}

	public String getProductTokenID() {
		return productTokenID;
	}

	/**
	 * This is the ID of the product. At some point in the future, this will become a transient id. However, this API should remain unchanged though the source of the id might change.
	 * 
	 * @param productTokenID
	 *            the product id or product token id
	 */
	public void setProductTokenID(String productTokenID) {
		this.productTokenID = productTokenID;
	}

	@Override
	public String getResourceIdentifier() {
		return "/portfolio/product/creditcard/:targetProductTokenID/transmission/inward/paymentmethods/debitcard/";
	}

	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> map = super.getUrlParams();
		map.put(":targetProductTokenID", productTokenID);
		return map;
	}

	@Override
	public Class<SuccessResponseWrapper> getResponseObjectClass() {
		return SuccessResponseWrapper.class;
	}

	@Override
	public String getAcceptedContentType() {
		return "application/json";
	}

	@Override
	public String getBodyContentType() {
		return "application/json";
	}

}
