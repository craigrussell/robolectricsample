package com.tescobank.mobile.api.portfolio;

public class AddCardPaymentMethodRequestBody {

	private CardPaymentMethod cardPaymentMethod;

	public CardPaymentMethod getCardPaymentMethod() {
		return cardPaymentMethod;
	}

	public void setCardPaymentMethod(CardPaymentMethod cardPaymentMethod) {
		this.cardPaymentMethod = cardPaymentMethod;
	}

	public static class CardPaymentMethod {

		private String cardNickName;

		private String cardPan;

		private Integer expiresEndMonth;

		private Integer expiresEndYear;

		private String addressLineOne;

		private String postCode;

		public String getCardNickName() {
			return cardNickName;
		}

		public void setCardNickName(String cardNickName) {
			this.cardNickName = cardNickName;
		}

		public String getCardPan() {
			return cardPan;
		}

		public void setCardPan(String cardPan) {
			this.cardPan = cardPan;
		}

		public Integer getExpiresEndMonth() {
			return expiresEndMonth;
		}

		public void setExpiresEndMonth(Integer validEndMonth) {
			this.expiresEndMonth = validEndMonth;
		}

		public Integer getExpiresEndYear() {
			return expiresEndYear;
		}

		public void setExpiresEndYear(Integer expiresEndYear) {
			this.expiresEndYear = expiresEndYear;
		}

		public String getAddressLineOne() {
			return addressLineOne;
		}

		public void setAddressLineOne(String addressLineOne) {
			this.addressLineOne = addressLineOne;
		}

		public String getPostCode() {
			return postCode;
		}

		public void setPostCode(String postCode) {
			this.postCode = postCode;
		}

	}

}
