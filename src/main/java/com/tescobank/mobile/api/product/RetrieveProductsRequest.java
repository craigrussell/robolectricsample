package com.tescobank.mobile.api.product;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

public abstract class RetrieveProductsRequest extends ApiRequest<RetrieveProductsResponse>{
	
	private String tbID;
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.GET;
	}

	@Override
	public String getResourceIdentifier() {
		return "/products/:tbID";
	}

	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();		
		urlParams.put(":tbID", tbID);
		return urlParams;
	}
	
	@Override
	public Class<RetrieveProductsResponse> getResponseObjectClass() {
		return RetrieveProductsResponse.class;
	}
	
	public void setTbID(String tbID) {
		this.tbID = tbID;		
	}

	public Object getTbID() {
		return tbID;
	}
}
