package com.tescobank.mobile.api.product;

import java.util.Map;

import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.model.product.ProductIdentifiersMixIn;
import com.tescobank.mobile.rest.processor.RestJsonRequestBodyBuilder;
import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'retrieve paged transactions' requests
 */
public abstract class RetrievePagedTransactionsRequest extends ApiRequest<RetrievePagedTransactionsResponse> {

	public static final String FIRST_PAGE = "1";
	
	private String tbID;

	private String nextPageRef;

	private Product product;

	public RetrievePagedTransactionsRequest() {
		super();
	}

	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}

	@Override
	public String getResourceIdentifier() {
		return "/products/:tbID/transactions/page/:nextPageRef";
	}

	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();

		urlParams.put(":tbID", tbID);
		urlParams.put(":nextPageRef", nextPageRef);

		return urlParams;
	}

	@Override
	public Map<String, Object> getBodyParams() {
		Map<String, Object> bodyParams = super.getBodyParams();

		String json = RestJsonRequestBodyBuilder.buildJsonRequestBody(product, Product.class, ProductIdentifiersMixIn.class);
		bodyParams.put("product", json);

		return bodyParams;
	}

	@Override
	public Class<RetrievePagedTransactionsResponse> getResponseObjectClass() {
		return RetrievePagedTransactionsResponse.class;
	}

	public String getTbID() {
		return tbID;
	}

	public void setTbID(String tbID) {
		this.tbID = tbID;
	}

	public void setNextPageRef(String nextPageRef) {
		this.nextPageRef = nextPageRef;
	}

	public String getNextPageRef() {
		return nextPageRef;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
}
