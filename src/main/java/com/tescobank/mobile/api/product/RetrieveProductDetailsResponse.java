package com.tescobank.mobile.api.product;

import java.io.Serializable;
import java.util.List;

import com.tescobank.mobile.model.product.ProductDetails;

public class RetrieveProductDetailsResponse implements Serializable {

	private static final long serialVersionUID = 4980008364033958867L;
	private List<ProductDetails> products;

	public List<ProductDetails> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDetails> products) {
		this.products = products;
	}

}
