package com.tescobank.mobile.api.product;

import java.util.Date;
import java.util.Map;

import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.rest.request.ByteArrayApiRequest;

/**
 * Abstract base class for 'retrieve statement' requests
 */
public abstract class RetrieveStatementRequest extends ByteArrayApiRequest {
	
	private DataFormatter dataFormatter;
	
	private boolean isCreditCard;
	
	private String tbId;

	private String productCategory;

	private String productId;

	private String cif;

	private Date date;

	public RetrieveStatementRequest(boolean isCreditCard) {
		super();
		
		this.dataFormatter = new DataFormatter();
		this.isCreditCard = isCreditCard;
	}
	
	@Override
	public String getResourceIdentifier() {
		return isCreditCard ? "/products/:tbId/:productCategory/:productId/statements/:date" : "/products/:tbId/:productCategory/:productId/:cif/statements/:date";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();

		urlParams.put(":tbId", tbId);
		urlParams.put(":productCategory", productCategory);
		urlParams.put(":productId", productId);
		urlParams.put(":cif", cif);
		urlParams.put(":date", dataFormatter.formatDateToYYYYMMDD(date));

		return urlParams;
	}
	
	public String getTbId() {
		return tbId;
	}

	public void setTbId(String tbId) {
		this.tbId = tbId;
	}
	
	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
