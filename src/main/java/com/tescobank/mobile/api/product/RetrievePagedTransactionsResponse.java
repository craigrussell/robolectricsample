package com.tescobank.mobile.api.product;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;

/**
 * 'Retrieve paged transactions' response
 */
public class RetrievePagedTransactionsResponse {

	private List<Transaction> transactions;
	private String nextPageRef;

	public RetrievePagedTransactionsResponse() {
		super();
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	public String getNextPageRef() {
		return nextPageRef;
	}

	public void setNextPageRef(String nextPageRef) {
		this.nextPageRef = nextPageRef;
	}

	/**
	 * Transaction inner class
	 */
	public static class Transaction implements Serializable {

		private static final long serialVersionUID = 3839802200224638441L;

		public static final String CATEGORY_CREDIT = "Credit";
		public static final String CATEGORY_DEBIT = "Debit";

		private static final String TAG = RetrievePagedTransactionsResponse.Transaction.class.getSimpleName();

		private String transactionId;

		private BigDecimal amount;

		private String description;

		private String merchantLocation;

		private String merchantName;

		private String occurred;

		private String type;

		private String proccessed;

		private String category;

		/** Client-side property supporting sorting by date-time */
		private Integer occurredIndex;

		/** Client-side property supporting grouping / sorting by date */
		private Date occurredDate;

		/** Client-side property supporting filtering by amount */
		private String amountString;

		public Transaction() {
			super();
		}

		/**
		 * @return whether the transaction is a credit (rather than debit)
		 */
		public boolean isCredit() {
			return CATEGORY_CREDIT.equalsIgnoreCase(category);
		}

		/**
		 * @param filterText
		 *            the filter text
		 * 
		 * @return whether the transaction matches the specified filter text
		 */
		public boolean matchesFilter(String filterText) {

			if ((filterText == null) || filterText.isEmpty()) {
				return true;
			}

			return matchesValues(filterText, merchantName) || matchesValues(filterText, description) || matchesValues(filterText, amountString);
		}

		private boolean matchesValues(String filterText, String value) {
			return (value != null) && value.toLowerCase(Locale.UK).contains(filterText.toLowerCase(Locale.UK));
		}

		public String getTransactionId() {
			return transactionId;
		}

		public void setTransactionId(String transactionId) {
			this.transactionId = transactionId;
		}

		public BigDecimal getAmount() {
			return amount;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getMerchantLocation() {
			return merchantLocation;
		}

		public void setMerchantLocation(String merchantLocation) {
			this.merchantLocation = merchantLocation;
		}

		public String getMerchantName() {
			return merchantName;
		}

		public void setMerchantName(String merchantName) {
			this.merchantName = merchantName;
		}

		public String getOccurred() {
			return occurred;
		}

		public void setOccurred(String occurred) {
			this.occurred = occurred;
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
				Date date = sdf.parse(occurred);
				setOccurredDate(date);
			} catch (ParseException e) {
				if (BuildConfig.DEBUG) {
					Log.e(TAG, "Date was not parsed", e);
				}
			}
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getProccessed() {
			return proccessed;
		}

		public void setProccessed(String proccessed) {
			this.proccessed = proccessed;
		}

		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}

		public Integer getOccurredIndex() {
			return occurredIndex;
		}

		public void setOccurredIndex(Integer occurredIndex) {
			this.occurredIndex = occurredIndex;
		}

		public Date getOccurredDate() {
			return occurredDate;
		}

		public void setOccurredDate(Date occurredDate) {
			this.occurredDate = occurredDate;
		}

		public String getAmountString() {
			return amountString;
		}

		public void setAmountString(String amountString) {
			this.amountString = amountString;
		}

		// PAA-706
		private static final Set<String> DESCRIPTION_PHRASES_TO_STRIP = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList("card payment ", "card payment, ", "personal card payment, ")));

		/**
		 * PAA-706 - get a condensed description which strips the phrases in {@link #DESCRIPTION_PHRASES_TO_STRIP} from the start of the description.
		 */
		public String getCondensedDescription() {
			if (description == null) {
				return null;
			}

			String descriptionToTest = description.toLowerCase();
			for (String phrase : DESCRIPTION_PHRASES_TO_STRIP) {
				if (descriptionToTest.startsWith(phrase)) {
					return description.substring(phrase.length());
				}
			}

			return description;
		}
	}
}
