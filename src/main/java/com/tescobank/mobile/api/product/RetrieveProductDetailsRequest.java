package com.tescobank.mobile.api.product;

import java.util.List;
import java.util.Map;

import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.model.product.ProductIdentifiersMixIn;
import com.tescobank.mobile.rest.processor.RestJsonRequestBodyBuilder;
import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'Retrieve Product Details' requests
 */
public abstract class RetrieveProductDetailsRequest extends ApiRequest<RetrieveProductDetailsResponse> {

	private String tbID;

	private List<Product> products;

	public RetrieveProductDetailsRequest() {
		super();
	}

	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}

	@Override
	public String getResourceIdentifier() {
		return "/products/:tbID";
	}

	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();

		urlParams.put(":tbID", tbID);

		return urlParams;
	}
	
	@Override
	public Map<String, Object> getBodyParams() {
		String json = RestJsonRequestBodyBuilder.buildJsonRequestBody(products, Product.class, ProductIdentifiersMixIn.class);
		Map<String, Object> bodyParams = super.getBodyParams();
		bodyParams.put("products",json);
		return bodyParams;
	}

	@Override
	public Class<RetrieveProductDetailsResponse> getResponseObjectClass() {
		return RetrieveProductDetailsResponse.class;
	}

	public String getTbID() {
		return tbID;
	}

	public void setTbID(String tbID) {
		this.tbID = tbID;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProductIdentifiers(List<Product> products) {
		this.products = products;

	}

}
