package com.tescobank.mobile.api.product;

import java.util.List;

import com.tescobank.mobile.model.product.Product;

public class RetrieveProductsResponse {

	private List<Product> products;
	
	public List<Product> getProducts() {
		return products;
	}

}
