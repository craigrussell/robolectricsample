package com.tescobank.mobile.api.configuration;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'settings' requests
 */
public abstract class SettingsRequest extends ApiRequest<SettingsResponse> {
	
	private String deviceID;
	
	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/configuration/settings";
	}
	
	@Override
	public Map<String, Object> getBodyParams() {
		Map<String, Object> bodyParams = super.getBodyParams();
		
		// Optional
		if (deviceID != null) {
			bodyParams.put("deviceID", deviceID);
		}

		return bodyParams;
	}
	
	@Override
	public Class<SettingsResponse> getResponseObjectClass() {
		return SettingsResponse.class;
	}
}
