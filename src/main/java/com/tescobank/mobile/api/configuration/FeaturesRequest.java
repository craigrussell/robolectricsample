package com.tescobank.mobile.api.configuration;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'feature' requests
 */
public abstract class FeaturesRequest extends ApiRequest<FeaturesResponse> {
	
	public FeaturesRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.GET;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/configuration/features";
	}
	
	@Override
	public Class<FeaturesResponse> getResponseObjectClass() {
		return FeaturesResponse.class;
	}
}
