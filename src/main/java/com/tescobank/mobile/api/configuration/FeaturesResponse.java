package com.tescobank.mobile.api.configuration;

import java.io.Serializable;
import java.util.Map;

/**
 * 'Features' response
 */
public class FeaturesResponse implements Serializable {
	
	private static final long serialVersionUID = 2208669866092675074L;

	public static final String PAY_CC_BILL_WITH_DEBIT_CARD = "payCCBillWithDebitCard";
	public static final String SAVE_NOW = "saveNow";
	public static final String MANAGE_CREDIT_CARD_PAYMENTS = "manageCCPaymentMethods";
	public static final String ATM_STORE_FINDER = "atmStoreFinder";

	public static final String EDIT_PAYMENT_REFERENCE = "editPaymentReference";
	public static final String FUTURE_PAYMENTS = "futurePayments";
	public static final String IMMEDIATE_PAYMENTS = "immediatePayments";

	public static final String VIEW_CURRENT_ACCOUNT_STATEMENT = "viewCurrentAccountStatement";
	public static final String VIEW_DOCUMENTS = "viewDocuments";
	public static final String VIEW_STATEMENT = "viewStatement";

	public static final String CHANGE_PASSWORD = "changePassword";
	public static final String SET_PASSCODE = "setPasscode";
	public static final String UNLINK_CUSTOMER_AND_DEVICE = "unlinkCustomerAndDevice";
	public static final String RESET_FORGOTTEN_PASSWORD = "resetForgottenPassword";
	public static final String NON_INTERACTIVE_PRODUCTS = "nonInteractiveProducts";


	/** Features can come and go so represent as a map */
	private Map<String, Boolean> features;

	public FeaturesResponse() {
		super();
	}

	public Map<String, Boolean> getFeatures() {
		return features;
	}

	public void setFeatures(Map<String, Boolean> features) {
		this.features = features;
	}

	public boolean isImmediatePaymentsEnabled() {
		return isFeatureEnabled(IMMEDIATE_PAYMENTS);
	}

	public boolean isFuturePaymentsEnabled() {
		return isFeatureEnabled(FUTURE_PAYMENTS);
	}

	public boolean isEditPaymentReferenceEnabled() {
		return isFeatureEnabled(EDIT_PAYMENT_REFERENCE);
	}

	public boolean isViewStatement() {
		return isFeatureEnabled(VIEW_STATEMENT);
	}

	public boolean isViewCurrentAccountStatement() {
		return isFeatureEnabled(VIEW_CURRENT_ACCOUNT_STATEMENT);
	}

	public boolean isAtmStoreFinderEnabled() {
		return isFeatureEnabled(ATM_STORE_FINDER);
	}

	public boolean isViewDocumentsEnabled() {
		return isFeatureEnabled(VIEW_DOCUMENTS);
	}

	public boolean isPayCCBillWithDebitCardEnabled() {
		return isFeatureEnabled(PAY_CC_BILL_WITH_DEBIT_CARD);
	}

	public boolean isSaveNowEnabled() {
		return isFeatureEnabled(SAVE_NOW);
	}
	
	public boolean isManageCreditCardPaymensEnabled() {
		return isFeatureEnabled(MANAGE_CREDIT_CARD_PAYMENTS);
	}

	public boolean isChangedPasswordEnabled() {
		return isFeatureEnabled(CHANGE_PASSWORD);
	}

	public boolean isSetPasscodeEnabled() {
		return isFeatureEnabled(SET_PASSCODE);
	}

	public boolean isUnlinkCustomerAndDeviceEnabled() {
		return isFeatureEnabled(UNLINK_CUSTOMER_AND_DEVICE);
	}

	public boolean isResetForgottenPasswordEnabled() {
		return isFeatureEnabled(RESET_FORGOTTEN_PASSWORD);
	}
	
	public boolean isNonInteractiveProductsEnabled() {
		Boolean feature = (features == null) ? null : features.get(NON_INTERACTIVE_PRODUCTS);
		return (feature == null) ? false : feature;
	}

	private boolean isFeatureEnabled(String featureName) {

		//In most cases the feature toggle is used to turn features off so default to on if the feature isn't specified
		Boolean feature = (features == null) ? null : features.get(featureName);
		return (feature == null) ? true : feature;
	}

}
