package com.tescobank.mobile.api.configuration;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.analytics.AnalyticsGroup;

/**
 * 'Settings' response
 */
public class SettingsResponse implements Serializable {

	private static final long serialVersionUID = 2128110965679254477L;

	private static final String TAG = SettingsResponse.class.getSimpleName();

	private static final int SECONDS_PER_MINUTE = 60;

	private static final String ENABLED_ANALYTIC_GROUPS_SETTING_NAME = "enabledAnalyticGroups";
	private static final String APP_TIMEOUT_SETTING_NAME = "appTimeOut";
	private static final String SMS_TIMEOUT_SETTING_NAME = "smsTimeOut";
	private static final String LEAVING_APP_TIMEOUT_SETTING_NAME = "answerCallTimeOut";
	private static final String NEXT_TAX_YEAR_SUBSCRIPTION_LIMIT_SETTING_NAME = "nextTaxYearSubscriptionLimit";
	private static final String DAYS_BEFORE_WARNING_ABOUT_PAYMENTS_INTO_ISA_SETTING_NAME = "daysBeforeWarningAboutPaymentsIntoISAs";
	private static final String NEXT_TAX_YEAR_JUNIOR_SUBSCRIPTION_LIMIT_SETTING_NAME = "nextTaxYearJuniorSubscriptionLimit";
	private static final String LANDING_PAGE_URL = "landingPageURL";

	private static final String SEASONAL_LOGO_IMAGE_PATH = "seasonalLogoImagePath";
	private static final String SEASONAL_EFFECT_IMAGE_PATHS = "seasonalEffectImagePaths";
	private static final String SEASONAL_EFFECT_IMAGE_PATHS_NONE = "none";

	public static final int DEFAULT_APP_TIMEOUT_SECONDS = 2 * SECONDS_PER_MINUTE;
	public static final int DEFAULT_SMS_TIMEOUT_SECONDS = 5 * SECONDS_PER_MINUTE;
	public static final int DEFAULT_LEAVING_APP_TIMEOUT_SECONDS = 10;

	private Map<String, String> configuration;

	public SettingsResponse() {
		configuration = new HashMap<String, String>();
	}

	public Map<String, String> getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Map<String, String> configuration) {
		this.configuration = configuration;
	}

	public int getApplicationTimeout() {
		return getIntValue(APP_TIMEOUT_SETTING_NAME, DEFAULT_APP_TIMEOUT_SECONDS);
	}

	public int getSmsTimeOut() {
		return getIntValue(SMS_TIMEOUT_SETTING_NAME, DEFAULT_SMS_TIMEOUT_SECONDS);
	}

	public int getLeavingApplicationTimeout() {
		return getIntValue(LEAVING_APP_TIMEOUT_SETTING_NAME, DEFAULT_LEAVING_APP_TIMEOUT_SECONDS);
	}

	public BigDecimal getNextTaxYearSubscriptionLimit() {
		try {
			return new BigDecimal(configuration.get(NEXT_TAX_YEAR_SUBSCRIPTION_LIMIT_SETTING_NAME));
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public BigDecimal getNextTaxYearJuniorSubscriptionLimit() {
		try {
			return new BigDecimal(configuration.get(NEXT_TAX_YEAR_JUNIOR_SUBSCRIPTION_LIMIT_SETTING_NAME));
		} catch (NumberFormatException e) {
			return null;
		}
	}

	/**
	 * @return number of days before tax year end within which a warning about
	 *         payments into ISAs should be issued. -1 if no warning should be
	 *         issued.
	 */
	public int getDaysBeforeWarningAboutPaymentsIntoIsas() {
		return getIntValue(DAYS_BEFORE_WARNING_ABOUT_PAYMENTS_INTO_ISA_SETTING_NAME, -1);
	}

	public String getLandingPageURL() {
		return getConfiguration().get(LANDING_PAGE_URL);
	}

	public List<AnalyticsGroup> getAnalyticsGroups() {
		String groupsStr = configuration.get(ENABLED_ANALYTIC_GROUPS_SETTING_NAME);
		if (groupsStr != null) {
			String[] groupsArr = groupsStr.split(",");
			return getAnalyticGroups(groupsArr);
		}
		return new LinkedList<AnalyticsGroup>();
	}

	public String getSeasonalLogoPath() {
		if (configuration == null) {
			return null;
		}

		String value = configuration.get(SEASONAL_LOGO_IMAGE_PATH);
		if (value == null || SEASONAL_EFFECT_IMAGE_PATHS_NONE.equals(value)) {
			return null;
		}
		return value;
	}

	public List<String> getSeasonalImages() {
		String groupsStr = configuration.get(SEASONAL_EFFECT_IMAGE_PATHS);
		if (null == groupsStr || SEASONAL_EFFECT_IMAGE_PATHS_NONE.equals(groupsStr)) {
			return null;
		}
		return Arrays.asList(groupsStr.split(","));
	}

	private int getIntValue(String settingName, int defaultValue) {
		String stringValue = getStringValue(settingName);
		if (stringValue != null) {
			return parseStringValue(stringValue, defaultValue);
		} else {
			return defaultValue;
		}
	}

	private String getStringValue(String settingName) {
		return (configuration == null) ? null : configuration.get(settingName);
	}

	private int parseStringValue(String stringValue, int defaultValue) {
		try {
			return Integer.valueOf(stringValue);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	private List<AnalyticsGroup> getAnalyticGroups(String[] groups) {
		List<AnalyticsGroup> groupsList = new LinkedList<AnalyticsGroup>();
		for (String groupStr : groups) {
			try {
				AnalyticsGroup group = AnalyticsGroup.valueOf(groupStr);
				groupsList.add(group);
			} catch (IllegalArgumentException e) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "Unknown analytic group " + groupStr + " received from server");
				}
			}
		}
		return groupsList;
	}

}
