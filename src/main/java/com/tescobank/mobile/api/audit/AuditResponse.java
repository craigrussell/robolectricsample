package com.tescobank.mobile.api.audit;

/**
 * 'Audit' response
 */
public class AuditResponse {
	
	private Boolean successful;
	
	public AuditResponse() {
		super();
	}

	public Boolean getSuccessful() {
		return successful;
	}

	public void setSuccessful(Boolean successful) {
		this.successful = successful;
	}
}
