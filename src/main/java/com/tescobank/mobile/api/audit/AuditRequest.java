package com.tescobank.mobile.api.audit;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'audit' requests
 */
public abstract class AuditRequest extends ApiRequest<AuditResponse> {
	
	private String tbID;
	
	private String deviceID;
	
	private OperationName operationName;
	
	private Reason reason;
	
	public AuditRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/audit/:tbID/:deviceID";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		
		urlParams.put(":tbID", tbID);
		urlParams.put(":deviceID", deviceID);
		
		return urlParams;
	}
	
	@Override
	public Map<String, Object> getBodyParams() {
		Map<String, Object> bodyParams = super.getBodyParams();
		
		bodyParams.put("operationName", operationName.toString());
		
		if (reason != null) {
			bodyParams.put("reason", reason.toString());
		}
		
		return bodyParams;
	}
	
	@Override
	public Class<AuditResponse> getResponseObjectClass() {
		return AuditResponse.class;
	}

	public String getTbID() {
		return tbID;
	}

	public void setTbID(String tbID) {
		this.tbID = tbID;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public OperationName getOperationName() {
		return operationName;
	}

	public void setOperationName(OperationName operationName) {
		this.operationName = operationName;
	}

	public Reason getReason() {
		return reason;
	}

	public void setReason(Reason reason) {
		this.reason = reason;
	}
	
	/**
	 * Supported operation names
	 */
	public static enum OperationName {
		
		SUBSEQUENT_LOGIN_PASSCODE("SubsequentLoginPasscode"),
		SUBSEQUENT_LOGIN_PASSWORD("SubsequentLoginPassword"),
		SET_CREDENTIAL("SetCredential");
		
		private String operationName;
		
		private OperationName(String operationName) {
			this.operationName = operationName;
		}
		
		@Override
		public String toString() {
			return operationName;
		}
	}
	
	/**
	 * Supported reasons
	 */
	public static enum Reason {
		
		FIRST_TIME_LOGIN("firstTimeLogin"),
		FORGOTTEN("forgotten"),
		SWITCHED_TO_PASSCODE("switchedToPasscode"),
		SWITCHED_TO_PASSWORD("switchedToPassword "),
		CHANGE("change"),
		USER_CHANGED_PASSWORD_ONLINE("userChangedPasswordOnline");
		
		private String reason;
		
		private Reason(String reason) {
			this.reason = reason;
		}
		
		@Override
		public String toString() {
			return reason;
		}
	}
}
