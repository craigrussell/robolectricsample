package com.tescobank.mobile.api.audit;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;

/**
 * Default 'audit' request that doesn't care about success or failure
 */
public class DefaultAuditRequest extends AuditRequest {

	private static final String TAG = DefaultAuditRequest.class.getSimpleName();
	
	@Override
	public void onResponse(AuditResponse response) {
		if (BuildConfig.DEBUG) {
			if (Boolean.TRUE.equals(response.getSuccessful())) {
				Log.d(TAG, "Audit Request Succeeded");
			} else {
				Log.d(TAG, "Audit Request Failed: Success = False");
			}
		}
		// Do nothing as we don't care whether this request succeeds or fails
	}

	@Override
	public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Audit Request Failed: " + errorResponseWrapper.getErrorResponse().getErrorCode());
		}
		// Do nothing as we don't care whether this request succeeds or fails
	}

}
