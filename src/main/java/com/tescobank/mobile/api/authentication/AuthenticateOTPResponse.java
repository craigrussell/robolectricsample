package com.tescobank.mobile.api.authentication;

/**
 * 'Authenticate OTP' response
 */
public class AuthenticateOTPResponse {
	
	private Boolean authenticated;
	
	public AuthenticateOTPResponse() {
		super();
	}
	
	public Boolean getAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(Boolean authenticated) {
		this.authenticated = authenticated;
	}
}
