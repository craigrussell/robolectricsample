package com.tescobank.mobile.api.authentication;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'fetch OTP token' requests
 */
public abstract class FetchOTPTokenRequest extends ApiRequest<FetchOTPTokenResponse> {
	
	private String cssoId;

	private String deviceId;
	
	public FetchOTPTokenRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/login/cssoToken/:cssoID";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		
		urlParams.put(":cssoID", cssoId);
		
		return urlParams;
	}
	
	@Override
	public Map<String, Object> getBodyParams() {
		Map<String, Object> bodyParams =  super.getBodyParams();
		
		bodyParams.put("deviceId", deviceId);
		
		return bodyParams;
	}
	
	@Override
	public Class<FetchOTPTokenResponse> getResponseObjectClass() {
		return FetchOTPTokenResponse.class;
	}
	
	public String getCssoId() {
		return cssoId;
	}

	public void setCssoId(String cssoId) {
		this.cssoId = cssoId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
}
