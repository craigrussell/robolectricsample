package com.tescobank.mobile.api.authentication;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'get pin challenge' requests
 */
public abstract class GetPinChallengeRequest extends ApiRequest<GetPinChallengeResponse> {
	
	private String cssoId;
	
	public GetPinChallengeRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.GET;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/login/challenge/:cssoID";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		
		urlParams.put(":cssoID", cssoId);
		
		return urlParams;
	}
	
	@Override
	public Class<GetPinChallengeResponse> getResponseObjectClass() {
		return GetPinChallengeResponse.class;
	}
	
	public String getCssoId() {
		return cssoId;
	}

	public void setCssoId(String cssoId) {
		this.cssoId = cssoId;
	}
}
