package com.tescobank.mobile.api.authentication;

/**
 * 'Fetch OTP token' response
 */
public class FetchOTPTokenResponse {
	
	private String encryptedToken;
	
	public FetchOTPTokenResponse() {
		super();
	}

	public String getEncryptedToken() {
		return encryptedToken;
	}

	public void setEncryptedToken(String encryptedToken) {
		this.encryptedToken = encryptedToken;
	}
}
