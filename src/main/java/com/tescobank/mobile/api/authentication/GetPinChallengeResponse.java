package com.tescobank.mobile.api.authentication;

import java.io.Serializable;
import java.util.Arrays;

/**
 * 'Get pin challenge' response
 */
public class GetPinChallengeResponse implements Serializable{
	
	private static final long serialVersionUID = -8476314927226830382L;

	private String challengeID;
	
	private Integer[] pinDigits;

	public GetPinChallengeResponse() {
		super();
	}

	public String getChallengeID() {
		return challengeID;
	}

	public void setChallengeID(String challengeID) {
		this.challengeID = challengeID;
	}
	
	public Integer[] getPinDigits() {
		return pinDigits;
	}

	public void setPinDigits(Integer[] pinDigits) {
		this.pinDigits = (pinDigits == null) ? null : Arrays.copyOf(pinDigits, pinDigits.length);
	}
}
