package com.tescobank.mobile.api.authentication;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'get SMS challenge' requests
 */
public abstract class GetSMSChallengeRequest extends ApiRequest<GetSMSChallengeResponse> {
	
	private String tbID;
	
	private String cssoID;
	
	public GetSMSChallengeRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.GET;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/sms/:cssoID/:tbID";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		
		urlParams.put(":cssoID", getCssoID());
		urlParams.put(":tbID", getTbID());
		
		return urlParams;
	}
	
	@Override
	public Class<GetSMSChallengeResponse> getResponseObjectClass() {
		return GetSMSChallengeResponse.class;
	}

	public String getTbID() {
		return tbID;
	}

	public void setTbID(String tbID) {
		this.tbID = tbID;
	}

	public String getCssoID() {
		return cssoID;
	}

	public void setCssoID(String cssoID) {
		this.cssoID = cssoID;
	}
}
