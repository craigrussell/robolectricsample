package com.tescobank.mobile.api.authentication;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'authenticate OTP' requests
 */
public abstract class AuthenticateOTPRequest extends ApiRequest<AuthenticateOTPResponse> {
	
	private String tbId;
	
	private String otp;
	
	private String deviceId;
	
	public AuthenticateOTPRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}

	@Override
	public String getResourceIdentifier() {
		return "/login/authenticate/:tbId";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		
		urlParams.put(":tbId", tbId);
		
		return urlParams;
	}
	
	@Override
	public Map<String, Object> getBodyParams() {
		Map<String, Object> bodyParams = super.getBodyParams();
		
		bodyParams.put("otp", otp);
		bodyParams.put("deviceId", deviceId);
		
		return bodyParams;
	}

	@Override
	public Class<AuthenticateOTPResponse> getResponseObjectClass() {
		return AuthenticateOTPResponse.class;
	}

	public String getTbId() {
		return tbId;
	}

	public void setTbId(String tbId) {
		this.tbId = tbId;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
}
