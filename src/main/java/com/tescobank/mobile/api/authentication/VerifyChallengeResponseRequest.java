package com.tescobank.mobile.api.authentication;

import java.util.Arrays;
import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'verify challenge response' requests
 */
public abstract class VerifyChallengeResponseRequest extends ApiRequest<VerifyChallengeResponseResponse> {
	
	private String cssoId;
	
	private String challengeId;
	
	private Integer[] pinDigits;
	
	private String deviceId;
	
	private Boolean verifyTokenVersion;

	public VerifyChallengeResponseRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/login/challenge/response/:cssoID/:challengeID";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		
		urlParams.put(":cssoID", cssoId);
		urlParams.put(":challengeID", challengeId);
		
		return urlParams;
	}
	
	@Override
	public Map<String, Object> getBodyParams() {
		Map<String, Object> bodyParams = super.getBodyParams();
		
		bodyParams.put("pinDigits", getPinDigitsAsString());
		bodyParams.put("deviceId", deviceId);
		bodyParams.put("verifyTokenVersion", verifyTokenVersion);

		return bodyParams;
	}
	
	@Override
	public Class<VerifyChallengeResponseResponse> getResponseObjectClass() {
		return VerifyChallengeResponseResponse.class;
	}

	public String getCssoId() {
		return cssoId;
	}

	public void setCssoId(String cssoId) {
		this.cssoId = cssoId;
	}

	public String getChallengeId() {
		return challengeId;
	}

	public void setChallengeId(String challengeId) {
		this.challengeId = challengeId;
	}

	public Integer[] getPinDigits() {
		return pinDigits;
	}

	public void setPinDigits(Integer[] pinDigits) {
		this.pinDigits = (pinDigits == null) ? null : Arrays.copyOf(pinDigits, pinDigits.length);
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	public Boolean getVerifyTokenVersion() {
		return verifyTokenVersion;
	}

	public void setVerifyTokenVersion(Boolean verifyTokenVersion) {
		this.verifyTokenVersion = verifyTokenVersion;
	}
	
	private String getPinDigitsAsString() {
		StringBuilder stringBuilder = new StringBuilder();
		
		if (pinDigits != null) {
			for (Integer pinDigit : pinDigits) {
				stringBuilder.append(pinDigit);
			}
		}
		
		return stringBuilder.toString();
	}
}
