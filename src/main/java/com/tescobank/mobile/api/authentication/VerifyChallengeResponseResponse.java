package com.tescobank.mobile.api.authentication;

import java.io.Serializable;

/**
 * 'Verify challenge response' response
 */
public class VerifyChallengeResponseResponse implements Serializable {
	
	private static final long serialVersionUID = -4701607722921614044L;
	
	private String encryptedToken;
	
	public VerifyChallengeResponseResponse() {
		super();
	}

	public String getEncryptedToken() {
		return encryptedToken;
	}

	public void setEncryptedToken(String encryptedToken) {
		this.encryptedToken = encryptedToken;
	}
}
