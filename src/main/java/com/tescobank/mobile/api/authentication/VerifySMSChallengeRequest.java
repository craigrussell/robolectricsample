package com.tescobank.mobile.api.authentication;

import java.util.Arrays;
import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'verify SMSC challenge' requests
 */
public abstract class VerifySMSChallengeRequest extends ApiRequest<VerifySMSChallengeResponse> {
	
	private String cssoID;
	
	private Integer[] smsDigits;

	public VerifySMSChallengeRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/sms/response/:cssoID";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		
		urlParams.put(":cssoID", getCssoID());
		return urlParams;
	}
	
	@Override
	public Map<String, Object> getBodyParams() {
		Map<String, Object> bodyParams = super.getBodyParams();
	
		bodyParams.put("smsDigits", getSmsDigitsAsString());

		return bodyParams;
	}
	
	@Override
	public Class<VerifySMSChallengeResponse> getResponseObjectClass() {
		return VerifySMSChallengeResponse.class;
	}

	public String getCssoID() {
		return cssoID;
	}

	public void setCssoID(String cssoID) {
		this.cssoID = cssoID;
	}
	
	public Integer[] getSmsDigits() {
		return smsDigits;
	}

	public void setSmsDigits(Integer[] smsDigits) {
		this.smsDigits = (smsDigits == null) ? null : Arrays.copyOf(smsDigits, smsDigits.length);
	}
	
	private String getSmsDigitsAsString() {
		StringBuilder stringBuilder = new StringBuilder();
		
		if (smsDigits != null) {
			for (Integer smsDigit : smsDigits) {
				stringBuilder.append(smsDigit);
			}
		}
		
		return stringBuilder.toString();
	}
}
