package com.tescobank.mobile.api.authentication;

/**
 * 
 * 'Verify SMS' response
 */
public class VerifySMSChallengeResponse {
	
	private Boolean smsValid;

	public VerifySMSChallengeResponse() {
		super();
	}

	public Boolean getSmsValid() {
		return smsValid;
	}

	public void setSmsIssued(Boolean smsValid) {
		this.smsValid = smsValid;
	}
}
