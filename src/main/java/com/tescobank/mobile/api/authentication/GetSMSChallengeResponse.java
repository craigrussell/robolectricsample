package com.tescobank.mobile.api.authentication;

import java.io.Serializable;

/**
 * 'Get SML challenge' response
 */
public class GetSMSChallengeResponse implements Serializable {
	
	private static final long serialVersionUID = -1405566681713218777L;
	
	private Boolean smsIssued;
	
	private String mobileNumber;

	public GetSMSChallengeResponse() {
		super();
	}

	public Boolean getSmsIssued() {
		return smsIssued;
	}

	public void setSmsIssued(Boolean smsIssued) {
		this.smsIssued = smsIssued;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
}
