package com.tescobank.mobile.api.credential;

import java.util.Map;

import com.tescobank.mobile.api.deviceregistration.DeviceMetricsRequestBodyMapper;
import com.tescobank.mobile.application.DeviceMetrics;
import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'set credential' requests
 */
public abstract class SetCredentialRequest extends ApiRequest<SetCredentialResponse> {

	private String cssoID;

	private Credential credential;

	private String credentialValue;

	private DeviceMetrics deviceMetrics;


	public SetCredentialRequest() {
		super();
	}

	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}

	@Override
	public String getResourceIdentifier() {
		return "/credential/:cssoID/:credential";
	}

	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();

		urlParams.put(":cssoID", cssoID);
		urlParams.put(":credential", credential);

		return urlParams;
	}

	@Override
	public Map<String, Object> getBodyParams() {
		Map<String, Object> bodyParams = super.getBodyParams();
		bodyParams.put("credentialValue", (credentialValue == null) ? null : credentialValue);
		if(deviceMetrics != null) {
			DeviceMetricsRequestBodyMapper mapper = new DeviceMetricsRequestBodyMapper(deviceMetrics);
			bodyParams.putAll(mapper.toBodyMap());
		}
		return bodyParams;
	}

	@Override
	public Class<SetCredentialResponse> getResponseObjectClass() {
		return SetCredentialResponse.class;
	}

	public String getCssoID() {
		return cssoID;
	}

	public void setCssoID(String cssoID) {
		this.cssoID = cssoID;
	}

	public Credential getCredential() {
		return credential;
	}

	public void setCredential(Credential credential) {
		this.credential = credential;
	}

	public String getCredentialValue() {
		return credentialValue;
	}

	public void setCredentialValue(String credentialValue) {
		this.credentialValue = credentialValue;
	}
	
	public void setDeviceMetrics(DeviceMetrics deviceMetrics) {
		this.deviceMetrics = deviceMetrics;
	}
	
	public DeviceMetrics getDeviceMetrics() {
		return deviceMetrics;
	}
	
	/**
	 * Supported credentials
	 */
	public static enum Credential {

		PASSWORD("password"), PASSCODE("passcode");

		private String credential;

		private Credential(String credential) {
			this.credential = credential;
		}

		@Override
		public String toString() {
			return credential;
		}
	}
}
