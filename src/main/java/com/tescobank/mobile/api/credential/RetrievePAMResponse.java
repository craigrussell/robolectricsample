package com.tescobank.mobile.api.credential;

import java.io.Serializable;

/**
 * 'Retrieve PAM' response
 */
public class RetrievePAMResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String pamPhrase;
	
	private String pamUrl;
	
	public RetrievePAMResponse() {
		super();
	}

	public String getPamPhrase() {
		return pamPhrase;
	}

	public void setPamPhrase(String pamPhrase) {
		this.pamPhrase = pamPhrase;
	}

	public String getPamUrl() {
		return pamUrl;
	}

	public void setPamUrl(String pamUrl) {
		this.pamUrl = pamUrl;
	}
}
