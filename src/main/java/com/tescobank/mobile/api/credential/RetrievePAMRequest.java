package com.tescobank.mobile.api.credential;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'retrieve PAM' requests
 */
public abstract class RetrievePAMRequest extends ApiRequest<RetrievePAMResponse> {
	
	private String tbID;

	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.GET;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/credential/pam/:tbID";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		
		urlParams.put(":tbID", tbID);
		
		return urlParams;
	}
	
	@Override
	public Class<RetrievePAMResponse> getResponseObjectClass() {
		return RetrievePAMResponse.class;
	}
	
	public String getTbID() {
		return tbID;
	}

	public void setTbID(String tbID) {
		this.tbID = tbID;
	}
}
