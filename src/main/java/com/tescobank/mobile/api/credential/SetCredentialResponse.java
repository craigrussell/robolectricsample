package com.tescobank.mobile.api.credential;

/**
 * 'Set credential' response
 */
public class SetCredentialResponse {
	
	private Boolean successful;
	
	public SetCredentialResponse() {
		super();
	}

	public Boolean getSuccessful() {
		return successful;
	}

	public void setSuccessful(Boolean successful) {
		this.successful = successful;
	}
}
