package com.tescobank.mobile.api.payment;

import java.io.Serializable;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class RetrievePaymentMethodsResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5284663227164131766L;
	private List<PaymentMethod> paymentMethods;

	public List<PaymentMethod> getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public static class PaymentMethod implements Serializable {

		private static final long serialVersionUID = -4168181928582687831L;

		private String cardNumber;
		private String paymentMethodId;
		private String cardType;
		private String cardStartYear;
		private String cardStartMonth;
		private String cardExpiryYear;
		private String cardExpiryMonth;
		private Integer issueNumber;
		private String cardName;

		public String getCardNumber() {
			return cardNumber;
		}

		public void setCardNumber(String cardNumber) {
			this.cardNumber = cardNumber;
		}

		public String getPaymentMethodId() {
			return paymentMethodId;
		}

		public void setPaymentMethodId(String paymentMethodId) {
			this.paymentMethodId = paymentMethodId;
		}

		public String getCardType() {
			return cardType;
		}

		public void setCardType(String cardType) {
			this.cardType = cardType;
		}

		public String getCardStartYear() {
			return cardStartYear;
		}

		public void setCardStartYear(String cardStartYear) {
			this.cardStartYear = cardStartYear;
		}

		public String getCardStartMonth() {
			return cardStartMonth;
		}

		public void setCardStartMonth(String cardStartMonth) {
			this.cardStartMonth = cardStartMonth;
		}

		public String getCardExpiryYear() {
			return cardExpiryYear;
		}

		public void setCardExpiryYear(String cardExpiryYear) {
			this.cardExpiryYear = cardExpiryYear;
		}

		public String getCardExpiryMonth() {
			return cardExpiryMonth;
		}

		public void setCardExpiryMonth(String cardExpiryMonth) {
			this.cardExpiryMonth = cardExpiryMonth;
		}

		public Integer getIssueNumber() {
			return issueNumber;
		}

		public void setIssueNumber(Integer issueNumber) {
			this.issueNumber = issueNumber;
		}

		public String getCardName() {
			return cardName;
		}

		public void setCardName(String cardName) {
			this.cardName = cardName;
		}

		public Date toCardStartDate() {
			return toDate(getCardStartMonth(), getCardStartYear());
		}

		public Date toCardExpiryDate() {
			return toDate(getCardExpiryMonth(), getCardExpiryYear());
		}

		private Date toDate(String month, String year) {
			if (null == month || null == year) {
				return null;
			}

			Calendar cal = new GregorianCalendar(0, 0, 0, 0, 0, 0);
			cal.set(Calendar.MONTH, Integer.parseInt(month));
			cal.set(Calendar.YEAR, Integer.parseInt(year));
			return cal.getTime();
		}

		public boolean hasExpired() {
			return toCardExpiryDate().before(new Date());
		}

	}

}
