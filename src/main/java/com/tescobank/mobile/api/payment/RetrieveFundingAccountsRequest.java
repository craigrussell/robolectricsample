package com.tescobank.mobile.api.payment;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'retrieve funding accounts' requests
 */
public abstract class RetrieveFundingAccountsRequest extends ApiRequest<RetrieveFundingAccountsResponse> {
	
	private String tbId;
	
	public RetrieveFundingAccountsRequest() {
		super();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.GET;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/products/:tbId/payments/funding";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		
		urlParams.put(":tbId", tbId);
		
		return urlParams;
	}
	
	@Override
	public Class<RetrieveFundingAccountsResponse> getResponseObjectClass() {
		return RetrieveFundingAccountsResponse.class;
	}

	public String getTbId() {
		return tbId;
	}

	public void setTbId(String tbId) {
		this.tbId = tbId;
	}
}
