package com.tescobank.mobile.api.payment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 'Retrieve funding accounts' response
 */
public class RetrieveFundingAccountsResponse implements Serializable {

	private static final long serialVersionUID = 8969209283884452034L;
	private Account[] accounts;

	public RetrieveFundingAccountsResponse() {
		super();
	}

	public Account[] getAccounts() {
		return accounts;
	}

	public void setAccounts(Account[] accounts) {
		this.accounts = (accounts == null) ? null : Arrays.copyOf(accounts, accounts.length);
	}

	/**
	 * Account inner class
	 */
	public static class Account implements Serializable {

		private static final long serialVersionUID = 3230197201327285577L;

		private String id;

		private Transactee transactee;

		private String destinationAccountNumber;

		private String destinationProductCategory;

		private BigDecimal currentAmount;

		private String fundingType;

		private String referenceId;

		private Boolean directDebitCreated;

		private String description;

		public Account() {
			super();
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public Transactee getTransactee() {
			return transactee;
		}

		public void setTransactee(Transactee transactee) {
			this.transactee = transactee;
		}

		public String getDestinationAccountNumber() {
			return destinationAccountNumber;
		}

		public void setDestinationAccountNumber(String destinationAccountNumber) {
			this.destinationAccountNumber = destinationAccountNumber;
		}

		public String getDestinationProductCategory() {
			return destinationProductCategory;
		}

		public void setDestinationProductCategory(String destinationProductCategory) {
			this.destinationProductCategory = destinationProductCategory;
		}

		public BigDecimal getCurrentAmount() {
			return currentAmount;
		}

		public void setCurrentAmount(BigDecimal currentAmount) {
			this.currentAmount = currentAmount;
		}

		public String getFundingType() {
			return fundingType;
		}

		public void setFundingType(String fundingType) {
			this.fundingType = fundingType;
		}

		public String getReferenceId() {
			return referenceId;
		}

		public void setReferenceId(String referenceId) {
			this.referenceId = referenceId;
		}

		public Boolean getDirectDebitCreated() {
			return directDebitCreated;
		}

		public void setDirectDebitCreated(Boolean directDebitCreated) {
			this.directDebitCreated = directDebitCreated;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		/**
		 * Transactee inner class
		 */
		public static class Transactee implements Serializable {

			private static final long serialVersionUID = -3410044872428969673L;

			private String transacteeName;

			private BankAccount bankAccount;

			public Transactee() {
				super();
			}

			public String getTransacteeName() {
				return transacteeName;
			}

			public void setTransacteeName(String transacteeName) {
				this.transacteeName = transacteeName;
			}

			public BankAccount getBankAccount() {
				return bankAccount;
			}

			public void setBankAccount(BankAccount bankAccount) {
				this.bankAccount = bankAccount;
			}

			/**
			 * BankAccount inner class
			 */
			public static class BankAccount implements Serializable {

				private static final long serialVersionUID = 5560483986362501994L;

				private String accountNumber;

				private String sortCode;

				private String accountName;

				private String accountNickName;

				public BankAccount() {
					super();
				}

				public String getAccountNumber() {
					return accountNumber;
				}

				public void setAccountNumber(String accountNumber) {
					this.accountNumber = accountNumber;
				}

				public String getSortCode() {
					return sortCode;
				}

				public void setSortCode(String sortCode) {
					this.sortCode = sortCode;
				}

				public String getAccountName() {
					return accountName;
				}

				public void setAccountName(String accountName) {
					this.accountName = accountName;
				}

				public String getAccountNickName() {
					return accountNickName;
				}

				public void setAccountNickName(String accountNickName) {
					this.accountNickName = accountNickName;
				}

				public String getDisplayName() {
					return !isBlank(getAccountNickName()) ? getAccountNickName() : getAccountName();
				}
				
				private boolean isBlank(String s) {
					return null == s || s.trim().isEmpty();
				}
			}
		}
	}
}
