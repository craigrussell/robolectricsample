package com.tescobank.mobile.api.payment;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

public abstract class RetrievePaymentMethodsRequest extends ApiRequest<RetrievePaymentMethodsResponse> {

	private String productId;

	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.GET;
	}

	@Override
	public String getResourceIdentifier() {
		return "/products/:productId/payment/methods";
	}

	@Override
	public Class<RetrievePaymentMethodsResponse> getResponseObjectClass() {
		return RetrievePaymentMethodsResponse.class;
	}

	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		urlParams.put(":productId", productId);
		return urlParams;
	}
	
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

}
