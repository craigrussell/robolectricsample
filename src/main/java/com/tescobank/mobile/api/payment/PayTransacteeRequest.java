package com.tescobank.mobile.api.payment;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'pay transactee' requests
 */
public abstract class PayTransacteeRequest extends ApiRequest<PayTransacteeResponse> {

	private DataFormatter dataFormatter;

	private TransacteeCategory transacteeCategory;

	private String transacteeId;

	private BigDecimal amount;

	private String reference;

	private Date date;

	private String fromProductId;

	private String accountDetails;

	public PayTransacteeRequest() {
		super();

		dataFormatter = new DataFormatter();
	}

	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}

	@Override
	public String getResourceIdentifier() {
		return "/products/:transacteeCategory/payments/transactee/request";
	}
	
	@Override
	public boolean shouldRetry() {
		return false;
	}

	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();

		urlParams.put(":transacteeCategory", (transacteeCategory == null) ? null : transacteeCategory.toString());

		return urlParams;
	}

	@Override
	public Map<String, Object> getBodyParams() {
		Map<String, Object> bodyParams = super.getBodyParams();

		bodyParams.put("transacteeId", transacteeId);
		bodyParams.put("amount", amount.toPlainString());
		bodyParams.put("date", dataFormatter.formatDateToYYYYMMDD(date));
		bodyParams.put("fromProductId", fromProductId);
		bodyParams.put("accountDetails", accountDetails);

		if (reference != null) {
			bodyParams.put("reference", reference);
		}

		return bodyParams;
	}

	@Override
	public Class<PayTransacteeResponse> getResponseObjectClass() {
		return PayTransacteeResponse.class;
	}

	public TransacteeCategory getTransacteeCategory() {
		return transacteeCategory;
	}

	public void setTransacteeCategory(TransacteeCategory transacteeCategory) {
		this.transacteeCategory = transacteeCategory;
	}

	public String getTransacteeId() {
		return transacteeId;
	}

	public void setTransacteeId(String transacteeId) {
		this.transacteeId = transacteeId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getFromProductId() {
		return fromProductId;
	}

	public void setFromProductId(String fromProductId) {
		this.fromProductId = fromProductId;
	}

	public String getAccountDetails() {
		return accountDetails;
	}

	public void setAccountDetails(String accountDetails) {
		this.accountDetails = accountDetails;
	}

	/**
	 * Supported transactee categories
	 */
	public static enum TransacteeCategory {

		PAYEE("PAYEE"), INTERNAL("INTERNAL"), LINKED("LINKED");

		private String transacteeCategory;

		private TransacteeCategory(String transacteeCategory) {
			this.transacteeCategory = transacteeCategory;
		}

		@Override
		public String toString() {
			return transacteeCategory;
		}
	}
}
