package com.tescobank.mobile.api.payment;

import java.io.Serializable;
import java.util.List;

/**
 * 'Get transactees for user' response
 */
public class GetTransacteesForUserResponse {

	private List<Transactee> transactee;
	
	public GetTransacteesForUserResponse() {
		super();
	}
	
	public List<Transactee> getTransactee() {
		return transactee;
	}

	public void setTransactee(List<Transactee> transactee) {
		this.transactee = transactee;
	}
	
	/**
	 * Transactee inner class
	 */
	public static class Transactee implements Serializable {
		
		private static final long serialVersionUID = 7279041199879869436L;

		private static final String CATEGORY_LINKED = "LINKED";

		private String transacteeId;
		
		private String transacteeName;
		
		private String transacteeReference;
		
		private String transacteeNickName;
		
		private String transacteeCategory;
		
		private BankAccount bankAccount;
		
		/** The selected transactee reference */
		private String selectedTransacteeReference;

		public Transactee() {
			super();
		}
		
		public boolean isLinked() {
			return CATEGORY_LINKED.equals(transacteeCategory);
		}

		public String getTransacteeId() {
			return transacteeId;
		}

		public void setTransacteeId(String transacteeId) {
			this.transacteeId = transacteeId;
		}

		public String getTransacteeName() {
			return transacteeName;
		}

		public void setTransacteeName(String transacteeName) {
			this.transacteeName = transacteeName;
		}

		public String getTransacteeReference() {
			return transacteeReference;
		}

		public void setTransacteeReference(String transacteeReference) {
			this.transacteeReference = transacteeReference;
		}

		public String getTransacteeNickName() {
			return transacteeNickName;
		}

		public void setTransacteeNickName(String transacteeNickName) {
			this.transacteeNickName = transacteeNickName;
		}

		public String getTransacteeCategory() {
			return transacteeCategory;
		}

		public void setTransacteeCategory(String transacteeCategory) {
			this.transacteeCategory = transacteeCategory;
		}

		public BankAccount getBankAccount() {
			return bankAccount;
		}

		public void setBankAccount(BankAccount bankAccount) {
			this.bankAccount = bankAccount;
		}
		
		public String getSelectedTransacteeReference() {
			return selectedTransacteeReference;
		}

		public void setSelectedTransacteeReference(String selectedTransacteeReference) {
			this.selectedTransacteeReference = selectedTransacteeReference;
		}
		
		@Override
		public boolean equals(Object o) {
			if (o instanceof Transactee) {
				Transactee oTransactee = (Transactee)o;
	    		return transacteeId.equals(oTransactee.getTransacteeId());
	    	} else {
	    		return false;
	    	}
		}

		@Override
		public int hashCode() {
			return transacteeId.hashCode();
		}
		
		/**
		 * BankAccount inner class
		 */
		public static class BankAccount implements Serializable {
			
			private static final long serialVersionUID = 3862409276348399067L;

			private String accountNumber;
			
			private String sortCode;
			
			public BankAccount() {
				super();
			}
			
			public String getAccountNumber() {
				return accountNumber;
			}

			public void setAccountNumber(String accountNumber) {
				this.accountNumber = accountNumber;
			}

			public String getSortCode() {
				return sortCode;
			}

			public void setSortCode(String sortCode) {
				this.sortCode = sortCode;
			}
		}
	}
}
