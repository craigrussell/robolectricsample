package com.tescobank.mobile.api.payment;

import java.io.Serializable;


public class ThreeDSecureEnrollmentStatusResponse implements Serializable{

	private static final long serialVersionUID = 2230237749802011176L;
	
	private boolean enrollmentStatus;
	private String url;
	private String paymentAuthenticationRequest;

	public boolean getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(boolean enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPaymentAuthenticationRequest() {
		return paymentAuthenticationRequest;
	}

	public void setPaymentAuthenticationRequest(String paymentAuthenticationRequest) {
		this.paymentAuthenticationRequest = paymentAuthenticationRequest;
	}
}
