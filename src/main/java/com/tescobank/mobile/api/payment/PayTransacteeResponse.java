package com.tescobank.mobile.api.payment;

/**
 * 'Pay transactee' response
 */
public class PayTransacteeResponse {
	
	private String confirmationNumber;
	
	public PayTransacteeResponse() {
		super();
	}

	public String getConfirmationNumber() {
		return confirmationNumber;
	}

	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
}
