package com.tescobank.mobile.api.payment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RetrieveRegularPaymentsResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<RegularPayment> regularPayments;

	public List<RegularPayment> getRegularPayments() {
		return regularPayments;
	}

	public void setRegularPayments(List<RegularPayment> regularPayments) {
		this.regularPayments = regularPayments;
	}

	public static class RegularPayment {

		private String directDebitStatus;
		private String originatorName;
		private String originatorId;
		private String reference;
		private BigDecimal lastPaymentAmount;
		private String lastPaymentDate;
		private String nextPaymentDate;
		private String id;
		private Frequency frequency;
		private String accountType;
		private Transactee transactee;
		private BigDecimal currentAmount;
		private String productCategory;

		public String getOriginatorId() {
			return originatorId;
		}

		public void setOriginatorId(String originatorId) {
			this.originatorId = originatorId;
		}

		public String getLastPaymentDate() {
			return lastPaymentDate;
		}

		public void setLastPaymentDate(String lastPaymentDate) {
			this.lastPaymentDate = lastPaymentDate;
		}

		public String getNextPaymentDate() {
			return nextPaymentDate;
		}

		public void setNextPaymentDate(String nextPaymentDate) {
			this.nextPaymentDate = nextPaymentDate;
		}

		public String getDirectDebitStatus() {
			return directDebitStatus;
		}

		public void setDirectDebitStatus(String directDebitStatus) {
			this.directDebitStatus = directDebitStatus;
		}

		public String getOriginatorName() {
			return originatorName;
		}

		public void setOriginatorName(String originatorName) {
			this.originatorName = originatorName;
		}

		public String getReference() {
			return reference;
		}

		public void setReference(String reference) {
			this.reference = reference;
		}

		public BigDecimal getLastPaymentAmount() {
			return lastPaymentAmount;
		}

		public void setLastPaymentAmount(BigDecimal lastPaymentAmount) {
			this.lastPaymentAmount = lastPaymentAmount;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public Frequency getFrequency() {
			return frequency;
		}

		public void setFrequency(Frequency frequency) {
			this.frequency = frequency;
		}

		public String getAccountType() {
			return accountType;
		}

		public void setAccountType(String accountType) {
			this.accountType = accountType;
		}

		public Transactee getTransactee() {
			return transactee;
		}

		public void setTransactee(Transactee transactee) {
			this.transactee = transactee;
		}

		public BigDecimal getCurrentAmount() {
			return currentAmount;
		}

		public void setCurrentAmount(BigDecimal currentAmount) {
			this.currentAmount = currentAmount;
		}

		public String getProductCategory() {
			return productCategory;
		}

		public void setProductCategory(String productCategory) {
			this.productCategory = productCategory;
		}

		public static class Frequency {

			private String statusCode;
			private String finalPaymentDate;
			private String initialPaymentDate;
			private String frequencyType;

			/**
			 * TODO clarify if this needs to exist
			 * 
			 * Appears to be returned by "factory" broker, but not SIT1!
			 */
			@Deprecated
			public String getFrequencyType() {
				return frequencyType;
			}

			@Deprecated
			public void setFrequencyType(String frequenceType) {
				this.frequencyType = frequenceType;
			}

			public String getStatusCode() {
				return statusCode;
			}

			public void setStatusCode(String statusCode) {
				this.statusCode = statusCode;
			}

			public String getFinalPaymentDate() {
				return finalPaymentDate;
			}

			public void setFinalPaymentDate(String finalPaymentDate) {
				this.finalPaymentDate = finalPaymentDate;
			}

			public String getInitialPaymentDate() {
				return initialPaymentDate;
			}

			public void setInitialPaymentDate(String initialPaymentDate) {
				this.initialPaymentDate = initialPaymentDate;
			}

		}

		public static class Transactee {

			private BankAccount bankAccount;

			private String transacteeName;

			private String transacteeNickName;

			public BankAccount getBankAccount() {
				return bankAccount;
			}

			public void setBankAccount(BankAccount bankAccount) {
				this.bankAccount = bankAccount;
			}

			public String getTransacteeName() {
				return transacteeName;
			}

			public void setTransacteeName(String transacteeName) {
				this.transacteeName = transacteeName;
			}

			public String getTransacteeNickName() {
				return transacteeNickName;
			}

			public void setTransacteeNickName(String transacteeNickname) {
				this.transacteeNickName = transacteeNickname;
			}

			public static class BankAccount {

				private String accountNumber;

				private String sortCode;

				public String getAccountNumber() {
					return accountNumber;
				}

				public void setAccountNumber(String accountNumber) {
					this.accountNumber = accountNumber;
				}

				public String getSortCode() {
					return sortCode;
				}

				public void setSortCode(String sortCode) {
					this.sortCode = sortCode;
				}

			}

		}

	}
}