package com.tescobank.mobile.api.payment;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

public abstract class MakeCreditCardPaymentRequest extends ApiRequest<MakeCreditCardPaymentResponse> {
	
	private DataFormatter dataFormatter;

	private String productId;
	private String paymentMethodId;
	private BigDecimal amount;
	private Date dated;
	private String secondaryAccountID;
	private String cvv;
	private String paRes;
	
	public MakeCreditCardPaymentRequest() {
		super();
		dataFormatter = new DataFormatter();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}
	@Override
	public String getResourceIdentifier() {
		return "/products/:productId/payments/request";
	}
	
	@Override
	public boolean shouldRetry() {
		return false;
	}
	
	@Override
	public Class<MakeCreditCardPaymentResponse> getResponseObjectClass() {
		return MakeCreditCardPaymentResponse.class;
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		urlParams.put(":productId", productId);
		return urlParams;
	}
	
	@Override
	public Map<String, Object> getBodyParams() {
		Map<String, Object> bodyParams = super.getBodyParams();
		bodyParams.put("paymentMethodId", paymentMethodId);
		bodyParams.put("amount", amount.toPlainString());
		bodyParams.put("dated", dataFormatter.formatDateToYYYYMMDD(dated));
		bodyParams.put("secondaryAccountID", secondaryAccountID);
		bodyParams.put("CVV", cvv);
		bodyParams.put("paRes", paRes);
		return bodyParams;	
	}
	
	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(String paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getDated() {
		return dated;
	}

	public void setDated(Date dated) {
		this.dated = dated;
	}

	public String getSecondaryAccountID() {
		return secondaryAccountID;
	}

	public void setSecondaryAccountID(String secondaryAccountID) {
		this.secondaryAccountID = secondaryAccountID;
	}

	public String getCvv() {
		return cvv;
	}
	
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getPaRes() {
		return paRes;
	}
	
	public void setPaRes(String paRes) {
		this.paRes = paRes;
	}	
}
