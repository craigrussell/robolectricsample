package com.tescobank.mobile.api.payment;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'make payment to Tesco account' requests
 */
public abstract class MakePaymentToTescoAccountRequest extends ApiRequest<MakePaymentToTescoAccountResponse> {
	
	private DataFormatter dataFormatter;
	
	private String tbId;
	
	private String productCategory;
	
	private String accountNumber;
	
	private String sortCode;
	
	private String destinationAccountNumber;
	
	private BigDecimal amount;
	
	private Date processingDate;
	
	private Date validFromDate;
	
	private FundingType fundingType;
	
	private Frequency frequency;
	
	private Boolean createFundingDirectDebit;
	
	private String transacteeId;

	public MakePaymentToTescoAccountRequest() {
		super();
		
		dataFormatter = new DataFormatter();
	}
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}
	
	@Override
	public String getResourceIdentifier() {
		return "/products/:tbId/:productCategory/payments/funding/add";
	}
	
	@Override
	public boolean shouldRetry() {
		return false;
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		
		urlParams.put(":tbId", tbId);
		urlParams.put(":productCategory", productCategory);
		
		return urlParams;
	}
	
	@Override
	public Map<String, Object> getBodyParams() {
		Map<String, Object> bodyParams = super.getBodyParams();
		
		bodyParams.put("accountNumber", accountNumber);
		bodyParams.put("sortCode", sortCode);
		bodyParams.put("destinationAccountNumber", destinationAccountNumber);
		bodyParams.put("amount", amount.toPlainString());
		bodyParams.put("processingDate", dataFormatter.formatDateToYYYYMMDD(processingDate));
		bodyParams.put("validFromDate", dataFormatter.formatDateToYYYYMMDD(validFromDate));
		bodyParams.put("fundingType", (fundingType == null) ? null : fundingType.toString());
		bodyParams.put("frequency", (frequency == null) ? null : frequency.toString());
		bodyParams.put("createFundingDirectDebit", String.valueOf(Boolean.TRUE.equals(createFundingDirectDebit)));
		bodyParams.put("transacteeId", transacteeId);
		
		return bodyParams;
	}
	
	@Override
	public Class<MakePaymentToTescoAccountResponse> getResponseObjectClass() {
		return MakePaymentToTescoAccountResponse.class;
	}
	
	public String getTbId() {
		return tbId;
	}

	public void setTbId(String tbId) {
		this.tbId = tbId;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getSortCode() {
		return sortCode;
	}

	public void setSortCode(String sortCode) {
		this.sortCode = sortCode;
	}

	public String getDestinationAccountNumber() {
		return destinationAccountNumber;
	}

	public void setDestinationAccountNumber(String destinationAccountNumber) {
		this.destinationAccountNumber = destinationAccountNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getProcessingDate() {
		return processingDate;
	}

	public void setProcessingDate(Date processingDate) {
		this.processingDate = processingDate;
	}

	public Date getValidFromDate() {
		return validFromDate;
	}

	public void setValidFromDate(Date validFromDate) {
		this.validFromDate = validFromDate;
	}

	public FundingType getFundingType() {
		return fundingType;
	}

	public void setFundingType(FundingType fundingType) {
		this.fundingType = fundingType;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	public Boolean getCreateFundingDirectDebit() {
		return createFundingDirectDebit;
	}

	public void setCreateFundingDirectDebit(Boolean createFundingDirectDebit) {
		this.createFundingDirectDebit = createFundingDirectDebit;
	}
	
	public String getTransacteeId() {
		return transacteeId;
	}

	public void setTransacteeId(String transacteeId) {
		this.transacteeId = transacteeId;
	}

	/**
	 * Supported funding types
	 */
	public static enum FundingType {
		ONE_TIME("oneTime"),
		RECURRENT("recurrent");
		
		private String fundingType;
		
		private FundingType(String fundingType) {
			this.fundingType = fundingType;
		}
		
		@Override
		public String toString() {
			return fundingType;
		}
	}
	
	/**
	 * Supported frequencies
	 */
	public static enum Frequency {
		DAILY("Daily"),
		TWICE_MONTHLY("TwiceMonthly"),
		WEEKLY("Weekly"),
		MONTHLY("Monthly"),
		BI_MONTHLY("BiMonthly"),
		WI_WEEKLY("BiWeekly"),
		QUARTLERLY("Quarterly"),
		SEMI_ANNUALLY("SemiAnnually"),
		ANNUAL("Annual"),
		FOUR_WEEKLY("FourWeeks"),
		NO_FREQUENCY("NoFrequency");
		
		private String frequency;
		
		private Frequency(String frequency) {
			this.frequency = frequency;
		}
		
		@Override
		public String toString() {
			return frequency;
		}
	}
}
