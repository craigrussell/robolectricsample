package com.tescobank.mobile.api.payment;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

public abstract class RetrieveRegularPaymentsRequest extends ApiRequest<RetrieveRegularPaymentsResponse> {

	private String tbID;
	private String productCategory;
	private String productID;
	private RegularPaymentType regularPaymentType;

	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.GET;
	}

	@Override
	public String getResourceIdentifier() {
		return "/products/:tbID/payments/regular/:productCategory/:productID/:regularPaymentType";
	}

	@Override
	public Class<RetrieveRegularPaymentsResponse> getResponseObjectClass() {
		return RetrieveRegularPaymentsResponse.class;
	}

	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		urlParams.put(":tbID", tbID);
		urlParams.put(":productCategory", productCategory);
		urlParams.put(":productID", productID);
		urlParams.put(":regularPaymentType", regularPaymentType.toString());
		return urlParams;
	}

	public String getTbID() {
		return tbID;
	}

	public void setTbID(String tbID) {
		this.tbID = tbID;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getProductID() {
		return productID;
	}

	public void setProductID(String productID) {
		this.productID = productID;
	}

	public RegularPaymentType getRegularPaymentType() {
		return regularPaymentType;
	}

	public void setRegularPaymentType(RegularPaymentType regularPaymentType) {
		this.regularPaymentType = regularPaymentType;
	}

	public static enum RegularPaymentType {

		directDebit, standingOrder, futureDatedPayment

	}

}
