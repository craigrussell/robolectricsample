package com.tescobank.mobile.api.payment;

/**
 * 'Make payment to Tesco account' response
 */
public class MakePaymentToTescoAccountResponse {
	
	private String result;

	public MakePaymentToTescoAccountResponse() {
		super();
	}
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
