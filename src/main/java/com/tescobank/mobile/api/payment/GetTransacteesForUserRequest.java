package com.tescobank.mobile.api.payment;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

/**
 * Abstract base class for 'get transactees for user' requests
 */
public abstract class GetTransacteesForUserRequest extends ApiRequest<GetTransacteesForUserResponse>{
	
	private String tbID;
	
	public GetTransacteesForUserRequest() {
		super();
	}

	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.GET;
	}

	@Override
	public String getResourceIdentifier() {
		return "/products/:tbID/payments/transactee";
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();

		urlParams.put(":tbID", tbID);

		return urlParams;
	}

	@Override
	public Class<GetTransacteesForUserResponse> getResponseObjectClass() {
		return GetTransacteesForUserResponse.class;
	}


	public String getTbID() {
		return tbID;
	}

	public void setTbID(String tbID) {
		this.tbID = tbID;
	}

}
