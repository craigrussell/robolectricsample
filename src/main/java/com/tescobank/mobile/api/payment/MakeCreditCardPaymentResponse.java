package com.tescobank.mobile.api.payment;

public class MakeCreditCardPaymentResponse  {
	
	private String confirmationNumber;

	public String getConfirmationNumber() {
		return confirmationNumber;
	}

	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
}
