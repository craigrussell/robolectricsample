package com.tescobank.mobile.api.payment;

import java.math.BigDecimal;
import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

public abstract class ThreeDSecureEnrollmentStatusRequest extends ApiRequest<ThreeDSecureEnrollmentStatusResponse> {
	
	private String productId;
	private BigDecimal amount;
	private String paymentMethodId;
	
	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.POST;
	}
	@Override
	public String getResourceIdentifier() {
		return "/products/:productId/payment/methods/enrollmentStatus";
	}
	
	@Override
	public Class<ThreeDSecureEnrollmentStatusResponse> getResponseObjectClass() {
		return ThreeDSecureEnrollmentStatusResponse.class;
	}
	
	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();
		urlParams.put(":productId", productId);
		return urlParams;
	}
	
	@Override
	public Map<String, Object> getBodyParams() {
		Map<String, Object> bodyParams = super.getBodyParams();
		bodyParams.put("amount", amount.toPlainString());
		bodyParams.put("paymentMethodId", paymentMethodId);
		return bodyParams;	
	}
	
	public String getPaymentMethodId() {
		return paymentMethodId;
	}
	
	public void setPaymentMethodId(String paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
