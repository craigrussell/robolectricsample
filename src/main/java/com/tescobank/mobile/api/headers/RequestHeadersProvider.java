package com.tescobank.mobile.api.headers;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.application.DeviceInfo;
import com.tescobank.mobile.application.DeviceMetrics;
import com.tescobank.mobile.application.TescoDeviceMetrics;
import com.tescobank.mobile.rest.request.RestRequestHeadersProvider;

/**
 * Request headers provider
 */
public class RequestHeadersProvider implements RestRequestHeadersProvider, Serializable {

	public static final String HEADER_DEVICE_ID = "X-DeviceID";
	public static final String HEADER_AUTHORIZATION = "Authorization";
	public static final String HEADER_AUTHORIZATION_CREDENTIALS = "credentials";
	public static final String HEADER_CREDENTIAL = "X-Credential";
	public static final String HEADER_OS_NAME = "X-OSName";
	public static final String HEADER_DEVICE_TYPE = "X-DeviceType";
	public static final String HEADER_OS_VERSION = "X-OSVersion";
	public static final String HEADER_JAILBROKEN = "X-Jailbroken";
	public static final String HEADER_CLIENT_APP_VERSION = "X-ClientAppVersion";
	public static final String HEADER_LANGUAGE = "X-Language";
	public static final String HEADER_TIMEZONE = "X-Timezone";
	public static final String HEADER_FULL_HEIGHT = "X-FullHeight";
	public static final String HEADER_AVL_HEIGHT = "X-AvlHeight";
	public static final String HEADER_FULL_WIDTH = "X-FullWidth";
	public static final String HEADER_AVL_WIDTH = "X-AvlWidth";
	public static final String HEADER_MAC = "X-Mac";
	public static final String HEADER_INTERNAL_IP = "X-InternalIP";
	public static final String HEADER_LONGITUDE = "X-Current-Location-Longitude";
	public static final String HEADER_LATITUDE = "X-Current-Location-Latitude";
	
	private static final long serialVersionUID = -8618631858642555077L;
	private static final String TAG = RequestHeadersProvider.class.getSimpleName();

	private DeviceMetrics deviceMetrics;

	private Credential credential;

	private String authorization;
	
	private TescoLocation location;

	public RequestHeadersProvider(DeviceInfo deviceInfo) {
		super();
		this.deviceMetrics = new TescoDeviceMetrics(null,deviceInfo);
	}

	@Override
	public Map<String, String> getHeaders() {
		Map<String, String> headers = new HashMap<String, String>();

		addAuthorizatonHeaders(headers);
		addCredentialHeader(headers);
		addDeviceIdHeader(headers);
		addDeviceMetricsHeaders(headers);
		addCurrentLocationHeaders(headers);

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "getHeaders() : " + headers);
		}

		return headers;
	}

	@Override
	public void resetAuthorization() {
		authorization = null;
	}
	
	private void addAuthorizatonHeaders(Map<String, String> headers) {
		if (authorization != null) {
			headers.put(HEADER_AUTHORIZATION_CREDENTIALS, "auth-scheme #auth-param");
			headers.put(HEADER_AUTHORIZATION, authorization);
		}
	}
	
	private void addCredentialHeader(Map<String, String> headers) {
		if (credential != null) {
			headers.put(HEADER_CREDENTIAL, credential.toString());
		}
	}
	
	private void addDeviceIdHeader(Map<String, String> headers) {
		if (deviceMetrics.getDeviceId() != null) {
			headers.put(HEADER_DEVICE_ID, deviceMetrics.getDeviceId());
		}
	}
	
	private void addDeviceMetricsHeaders(Map<String, String> headers) {
		Boolean isJailbroken = deviceMetrics.getDeviceIsJailbroken();
		headers.put(HEADER_OS_NAME, deviceMetrics.getDeviceOsName());
		headers.put(HEADER_OS_VERSION, deviceMetrics.getDeviceOsVersion());
		headers.put(HEADER_DEVICE_TYPE, deviceMetrics.getDeviceModel());
		headers.put(HEADER_JAILBROKEN, (isJailbroken != null) && isJailbroken ? "Y" : "N");
		headers.put(HEADER_CLIENT_APP_VERSION, deviceMetrics.getClientAppVersion());
		headers.put(HEADER_LANGUAGE, deviceMetrics.getLanguage());
		headers.put(HEADER_TIMEZONE, deviceMetrics.getTimezone());
		headers.put(HEADER_FULL_HEIGHT, deviceMetrics.getFullHeight().toString());
		headers.put(HEADER_AVL_HEIGHT, deviceMetrics.getAvlHeight().toString());
		headers.put(HEADER_FULL_WIDTH, deviceMetrics.getFullWidth().toString());
		headers.put(HEADER_AVL_WIDTH, deviceMetrics.getAvlWidth().toString());
		headers.put(HEADER_MAC, deviceMetrics.getMac());
		headers.put(HEADER_INTERNAL_IP, deviceMetrics.getInternalIP());
	}

	private void addCurrentLocationHeaders(Map<String, String> headers) {
		// leave setting location related headers to last possible moment
		if (location != null) {
			headers.put(HEADER_LATITUDE, String.valueOf(location.getLatitude()));
			headers.put(HEADER_LONGITUDE, String.valueOf(location.getLongitude()));
		}
	}
	
	public void setDeviceId(String deviceId) {
		deviceMetrics.setDeviceId(deviceId);
	}

	public void setCredential(Credential credential) {
		this.credential = credential;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
	
	public void setLocation(TescoLocation location) {
		this.location = location;
	}

	/**
	 * Supported credentials
	 */
	public static enum Credential {

		PASSWORD("MobWord"), PASSCODE("MobCode");

		private String credential;

		private Credential(String credential) {
			this.credential = credential;
		}

		@Override
		public String toString() {
			return credential;
		}
	}
}
