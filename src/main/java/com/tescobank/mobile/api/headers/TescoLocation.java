package com.tescobank.mobile.api.headers;

import java.io.Serializable;

public class TescoLocation implements Serializable {
	
	private static final long serialVersionUID = -3870575378843496921L;

	private double longitude;

	private double latitude;

	public TescoLocation(double latitude,double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public double getLatitude() {
		return latitude;
	}
}
