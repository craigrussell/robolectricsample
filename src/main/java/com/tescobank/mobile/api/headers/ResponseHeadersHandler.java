package com.tescobank.mobile.api.headers;

import java.util.Map;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.rest.request.RestResponseHeadersHandler;

/**
 * Response headers handler
 */
public class ResponseHeadersHandler implements RestResponseHeadersHandler {
	
	private static final String TAG = ResponseHeadersHandler.class.getSimpleName();
	
	private static final String AUTHORIZATION_RESPONSE_HEADER_NAME = "X-TB-API-TOKEN";
	
	private static final String AUTHORIZATION_REQUEST_HEADER_PREFIX = "TB-API-TOKEN ";
	
	private RequestHeadersProvider requestHeadersProvider;
	
	public ResponseHeadersHandler(RequestHeadersProvider requestHeadersProvider) {
		super();
		
		this.requestHeadersProvider = requestHeadersProvider;
	}
	
	@Override
	public void handleHeaders(Map<String, String> headers) {
		
		// Obtain the 'Authorization' response header
		String authorizationResponseHeader = headers.get(AUTHORIZATION_RESPONSE_HEADER_NAME);
		
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "'Authorization' Response Header = " + authorizationResponseHeader);
		}
		
		if (authorizationResponseHeader != null) {
			
			// Create an equivalent 'Authorization' request header
			String authorizationRequestHeader = AUTHORIZATION_REQUEST_HEADER_PREFIX + authorizationResponseHeader;
			
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Setting 'Authorization' Request Header: " + authorizationRequestHeader);
			}
			
			// Update the request headers provider
			requestHeadersProvider.setAuthorization(authorizationRequestHeader);
			
		} else {
			
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Leaving 'Authorization' Request Header Unchanged as No 'Authorization' Response Header Received");
			}
		}
	}
}
