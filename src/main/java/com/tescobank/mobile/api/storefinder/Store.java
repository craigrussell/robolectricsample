package com.tescobank.mobile.api.storefinder;

import java.io.Serializable;
import java.util.List;

public class Store implements Serializable{

	private static final long serialVersionUID = -8284985659790860007L;

	private StoreMetadata storeMetadata;

	private OpeningTimes openingTimes;

	private List<String> facilitiesAvailable;

	public StoreMetadata getStoreMetadata() {
		return storeMetadata;
	}

	public void setStoreMetadata(StoreMetadata storeMetadata) {
		this.storeMetadata = storeMetadata;
	}

	public OpeningTimes getOpeningTimes() {
		return openingTimes;
	}

	public void setOpeningTimes(OpeningTimes openingTimes) {
		this.openingTimes = openingTimes;
	}

	public List<String> getFacilitiesAvailable() {
		return facilitiesAvailable;
	}

	public void setFacilitiesAvailable(List<String> facilitiesAvailable) {
		this.facilitiesAvailable = facilitiesAvailable;
	}

}
