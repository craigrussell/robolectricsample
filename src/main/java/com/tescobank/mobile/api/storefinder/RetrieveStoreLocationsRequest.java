package com.tescobank.mobile.api.storefinder;

import java.util.Map;

import com.tescobank.mobile.rest.request.ApiRequest;
import com.tescobank.mobile.rest.request.RestHttpMethod;

public abstract class RetrieveStoreLocationsRequest extends ApiRequest<RetrieveStoreLocationsResponse> {

	private double latitude;

	private double longitude;

	private int quantity;

	private String filter;

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	@Override
	public RestHttpMethod getHttpMethod() {
		return RestHttpMethod.GET;
	}

	@Override
	public String getResourceIdentifier() {
		return "/atm/:latitude/:longitude/:quantity/:filter";
	}

	@Override
	public Class<RetrieveStoreLocationsResponse> getResponseObjectClass() {
		return RetrieveStoreLocationsResponse.class;
	}

	@Override
	public Map<String, Object> getUrlParams() {
		Map<String, Object> urlParams = super.getUrlParams();		
		urlParams.put(":latitude", latitude);
		urlParams.put(":longitude", longitude);
		urlParams.put(":quantity", quantity);
		urlParams.put(":filter", filter);
		return urlParams;
	}
	
}
