package com.tescobank.mobile.api.storefinder;

import java.util.List;

public class RetrieveStoreLocationsResponse {

	private List<Store> stores;

	public List<Store> getStores() {
		return stores;
	}

	public void setStores(List<Store> stores) {
		this.stores = stores;
	}
	
}
