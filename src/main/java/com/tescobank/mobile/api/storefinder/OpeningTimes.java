package com.tescobank.mobile.api.storefinder;

import java.io.Serializable;

public class OpeningTimes implements Serializable{

	private static final long serialVersionUID = -4429275461796795684L;
	private String monOpen;
	private String monClose;
	private String tueOpen;
	private String tueClose;
	private String wedOpen;
	private String wedClose;
	private String thuOpen;
	private String thuClose;
	private String friOpen;
	private String friClose;
	private String satOpen;
	private String satClose;
	private String sunOpen;
	private String sunClose;

	public String getMonOpen() {
		return monOpen;
	}

	public void setMonOpen(String monOpen) {
		this.monOpen = monOpen;
	}

	public String getMonClose() {
		return monClose;
	}

	public void setMonClose(String monClose) {
		this.monClose = monClose;
	}

	public String getTueOpen() {
		return tueOpen;
	}

	public void setTueOpen(String tueOpen) {
		this.tueOpen = tueOpen;
	}

	public String getTueClose() {
		return tueClose;
	}

	public void setTueClose(String tueClose) {
		this.tueClose = tueClose;
	}

	public String getWedOpen() {
		return wedOpen;
	}

	public void setWedOpen(String wedOpen) {
		this.wedOpen = wedOpen;
	}

	public String getWedClose() {
		return wedClose;
	}

	public void setWedClose(String wedClose) {
		this.wedClose = wedClose;
	}

	public String getThuOpen() {
		return thuOpen;
	}

	public void setThuOpen(String thuOpen) {
		this.thuOpen = thuOpen;
	}

	public String getThuClose() {
		return thuClose;
	}

	public void setThuClose(String thuClose) {
		this.thuClose = thuClose;
	}

	public String getFriOpen() {
		return friOpen;
	}

	public void setFriOpen(String friOpen) {
		this.friOpen = friOpen;
	}

	public String getFriClose() {
		return friClose;
	}

	public void setFriClose(String friClose) {
		this.friClose = friClose;
	}

	public String getSatOpen() {
		return satOpen;
	}

	public void setSatOpen(String satOpen) {
		this.satOpen = satOpen;
	}

	public String getSatClose() {
		return satClose;
	}

	public void setSatClose(String satClose) {
		this.satClose = satClose;
	}

	public String getSunOpen() {
		return sunOpen;
	}

	public void setSunOpen(String sunOpen) {
		this.sunOpen = sunOpen;
	}

	public String getSunClose() {
		return sunClose;
	}

	public void setSunClose(String sunClose) {
		this.sunClose = sunClose;
	}

}
