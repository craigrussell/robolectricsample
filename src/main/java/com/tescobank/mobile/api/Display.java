package com.tescobank.mobile.api;

import java.io.Serializable;

public class Display implements Serializable {

	private static final long serialVersionUID = -3546715030733556804L;

	private String title;
	private String message;
	private CustomerService customerService;

	public Display() {
		super();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public static class CustomerService implements Serializable {

		private static final long serialVersionUID = 8313617447935584876L;

		private String message;
		private String phoneNumber;
		private String url;

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getPhoneNumber() {
			return phoneNumber;
		}

		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}
	}
}