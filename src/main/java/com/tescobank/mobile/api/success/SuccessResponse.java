package com.tescobank.mobile.api.success;

import com.tescobank.mobile.api.Display;

public class SuccessResponse {

	private String successCode;

	private String successType;

	private Display display;

	public String getSuccessCode() {
		return successCode;
	}

	public void setSuccessCode(String successCode) {
		this.successCode = successCode;
	}

	public String getSuccessType() {
		return successType;
	}

	public void setSuccessType(String successType) {
		this.successType = successType;
	}

	public Display getDisplay() {
		return display;
	}

	public void setDisplay(Display display) {
		this.display = display;
	}
}
