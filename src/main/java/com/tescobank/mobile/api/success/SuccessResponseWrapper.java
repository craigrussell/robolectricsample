package com.tescobank.mobile.api.success;

public class SuccessResponseWrapper {

	private SuccessResponse successResponse;
	private ResourceData resourceData;

	public SuccessResponse getSuccessResponse() {
		return successResponse;
	}

	public void setSuccessResponse(SuccessResponse successResponse) {
		this.successResponse = successResponse;
	}

	public ResourceData getResourceData() {
		return resourceData;
	}

	public void setResourceData(ResourceData resourceData) {
		this.resourceData = resourceData;
	}

	public static class ResourceData {

		private String tokenID;

		public String getTokenID() {
			return tokenID;
		}

		public void setTokenID(String tokenID) {
			this.tokenID = tokenID;
		}
	}
}
