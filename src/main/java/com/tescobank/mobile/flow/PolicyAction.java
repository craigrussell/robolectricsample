package com.tescobank.mobile.flow;

import java.io.Serializable;

import com.tescobank.mobile.ui.TescoActivity;

public interface PolicyAction extends Serializable {

	void setListener(PolicyActionListener listener);
	
	void execute(TescoActivity activity);

}
