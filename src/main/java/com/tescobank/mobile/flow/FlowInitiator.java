package com.tescobank.mobile.flow;

/**
 * Prepares a flow and starts it
 */
public interface FlowInitiator {
	
	/**
	 * Executes the task and starts the flow
	 */
	void execute();
}
