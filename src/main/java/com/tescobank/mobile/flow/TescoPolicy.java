package com.tescobank.mobile.flow;

import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createDefaultAnimationPolicyAction;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createDefaultCancellationAction;
import android.app.Activity;

import com.tescobank.mobile.analytics.AnalyticsEvent;
import com.tescobank.mobile.flow.Flow.StackBehaviour;
import com.tescobank.mobile.ui.ActionBarConfiguration;

public class TescoPolicy implements Policy {

	private static final long serialVersionUID = 7886184762129352363L;
	
	private Class<? extends Activity> activityClass;
	private ActionBarConfiguration actionBarConfig;
	private PolicyAction completion;
	private PolicyAction cancellation;
	private PolicyErrorAction error;
	private PolicyAction animation;
	private AnalyticsEvent activityDisplayedAnalytic;
	private StackBehaviour stackBehaviour;
	
	public TescoPolicy(Class<? extends Activity> activityClass) {
		this.activityClass = activityClass;
		this.stackBehaviour = StackBehaviour.FINISH;
		this.cancellation = createDefaultCancellationAction();
		this.animation = createDefaultAnimationPolicyAction();
	}	

	@Override
	public Class<? extends Activity> getActivityClass() {
		return activityClass;
	}
	
	@Override
	public ActionBarConfiguration getActionBarConfiguration() {
		return actionBarConfig;
	}

	@Override
	public void setActionBarConfig(ActionBarConfiguration actionBarConfig) {
		this.actionBarConfig = actionBarConfig;
	}

	@Override
	public boolean hasCompletion() {
		return completion != null;
	}
	
	@Override
	public PolicyAction getCompletion() {
		return completion;
	}
	
	@Override
	public void setCompletion(PolicyAction completion) {
		this.completion = completion;
	}
	
	@Override
	public boolean hasCancellation() {
		return cancellation != null;
	}
	
	@Override
	public PolicyAction getCancellation() {
		return cancellation;
	}
	
	@Override
	public void setCancellation(PolicyAction cancellation) {
		this.cancellation = cancellation;
	}
	
	@Override
	public boolean hasError() {
		return error != null;
	}
	
	@Override
	public PolicyErrorAction getError() {
		return error;
	}
	
	@Override
	public void setError(PolicyErrorAction error) {
		this.error = error;
	}
	
	@Override
	public StackBehaviour getStackBehaviour() {
		return stackBehaviour;
	}
	
	@Override
	public void setStackBehaviour(StackBehaviour stackBehaviour) {
		this.stackBehaviour = stackBehaviour;
	}
	
	@Override
	public boolean hasDisplayAnalytic() {
		return activityDisplayedAnalytic != null;
	}
	
	@Override
	public void setDisplayAnalytic(AnalyticsEvent activityDisplayedAnalytic) {
		this.activityDisplayedAnalytic = activityDisplayedAnalytic;
	}
	
	@Override
	public AnalyticsEvent getDisplayAnalytic() {
		return activityDisplayedAnalytic;
	}
	
	@Override
	public boolean hasAnimation() {
		return animation != null;
	}

	@Override
	public PolicyAction getAnimation() {
		return animation;
	}

	@Override
	public void setAnimation(PolicyAction animation) {
		this.animation = animation;	
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public static class Builder {
		
		private TescoPolicy policy;
		
		public Builder(Class<? extends Activity> activityClass) {
			policy = new TescoPolicy(activityClass);
		}
		
		public Builder setActionBarConfig(ActionBarConfiguration actionBarConfig) {
			policy.setActionBarConfig(actionBarConfig);
			return this;
		}
		
		public Builder setCancellation(PolicyAction cancellation) {
			policy.setCancellation(cancellation);
			return this;
		}
		
		public Builder setCompletion(PolicyAction completion) {
			policy.setCompletion(completion);
			return this;
		}
		
		public Builder setError(PolicyErrorAction error) {
			policy.setError(error);
			return this;
		}
		
		public Builder setAnimation(PolicyAction animation) {
			policy.setAnimation(animation);
			return this;
		}
		
		public Builder setStackBehaviour(StackBehaviour stackBehaviour) {
			policy.setStackBehaviour(stackBehaviour);
			return this;
		}
		
		public Builder setActivityDisplayedAnalytic(AnalyticsEvent activityDisplayedAnalytic) {
			policy.setDisplayAnalytic(activityDisplayedAnalytic);
			return this;
		}
		
		public TescoPolicy create() {
			return policy;
		}
	}
}
