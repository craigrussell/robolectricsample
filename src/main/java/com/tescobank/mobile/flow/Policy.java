package com.tescobank.mobile.flow;

import java.io.Serializable;

import android.app.Activity;

import com.tescobank.mobile.analytics.AnalyticsEvent;
import com.tescobank.mobile.flow.Flow.StackBehaviour;
import com.tescobank.mobile.ui.ActionBarConfiguration;

public interface Policy extends Serializable, Cloneable {

	Class<? extends Activity> getActivityClass();
	
	ActionBarConfiguration getActionBarConfiguration();
	
	void setActionBarConfig(ActionBarConfiguration actionBarConfig);
		
	boolean hasCompletion();

	PolicyAction getCompletion();

	void setCompletion(PolicyAction completion);
	
	boolean hasCancellation();

	PolicyAction getCancellation();

	void setCancellation(PolicyAction cancellation);
	
	StackBehaviour getStackBehaviour();

	void setStackBehaviour(StackBehaviour stackBehaviour);

	boolean hasError();

	PolicyErrorAction getError();

	void setError(PolicyErrorAction error);
	
	boolean hasAnimation();

	PolicyAction getAnimation();

	void setAnimation(PolicyAction animation);

	void setDisplayAnalytic(AnalyticsEvent displayAnalytic);

	AnalyticsEvent getDisplayAnalytic();

	boolean hasDisplayAnalytic();
	
	Object clone() throws CloneNotSupportedException;
}
