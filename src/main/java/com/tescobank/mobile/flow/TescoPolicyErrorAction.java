package com.tescobank.mobile.flow;

import java.io.Serializable;

import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.ui.TescoActivity;

public abstract class TescoPolicyErrorAction implements PolicyErrorAction, Serializable{

	private static final long serialVersionUID = -7252416986923113935L;

	private transient PolicyErrorActionListener errorListener;

	@Override
	public void setListener(PolicyErrorActionListener errorListener) {
		this.errorListener = errorListener;
	}
	
	@Override
	public void executeError(TescoActivity activity, ErrorResponseWrapper errorResponseWrapper) {
		onErrorActionCompleted();
	}
	
	protected void onErrorActionCompleted() {
		if(errorListener != null) {
			errorListener.onErrorActionComplete(this);
		}
	}
}
