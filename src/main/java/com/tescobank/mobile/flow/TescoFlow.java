package com.tescobank.mobile.flow;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;

import com.tescobank.mobile.analytics.AnalyticsEvent;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.TescoActivity;

public class TescoFlow implements Flow, PolicyActionListener, PolicyErrorActionListener, Cloneable {

	private int titleId;
	private PolicyInventory inventory;
	private Policy current;
	private Bundle dataBundle;
	private transient TescoActivity currentActivity;
	private StackBehaviour initialStackBehaviour;

	public TescoFlow(Bundle dataBundle, PolicyInventory inventory) {
		this.inventory = inventory;
		this.dataBundle = dataBundle;
		this.initialStackBehaviour = StackBehaviour.FINISH;
	}

	public TescoFlow(Parcel inParcel) {
		this.titleId = inParcel.readInt();
		this.initialStackBehaviour = StackBehaviour.class.cast(inParcel.readSerializable());
		this.inventory = PolicyInventory.class.cast(inParcel.readSerializable());
		this.current = Policy.class.cast(inParcel.readSerializable());
		this.dataBundle = inParcel.readBundle();
	}

	@Override
	public int getTitleId() {
		return titleId;
	}

	public void setTitleId(int titleId) {
		this.titleId = titleId;
	}

	@Override
	public void setInitialStackBehaviour(StackBehaviour stackBehaviour) {
		this.initialStackBehaviour = stackBehaviour;
	}
	
	public Bundle getDataBundle() {
		return dataBundle;
	}
	
	@Override
	public PolicyInventory getPolicyInventory() {
		return inventory;
	}

	@Override
	public void addPolicy(Policy policy) {
		inventory.addPolicy(policy);
	}

	@Override
	public void addPolices(List<Policy> policies) {
		inventory.addPolices(policies);	
	}
	
	public void setCurrentPolicy(Policy current) {
		this.current = current;
	}

	@Override
	public void begin(TescoActivity activity) {
		this.currentActivity = activity;
		startNextPolicy(activity, initialStackBehaviour);
	}
	
	private void startNextPolicy(TescoActivity activity, StackBehaviour behaviour) {
		TescoFlow nextPolicyFlow = this.safelyCloneForNextActivity();
		nextPolicyFlow.startNextActivity(activity);
		updateStack(activity, behaviour);
	}	
	
	private void startNextActivity(TescoActivity activity) {
		if (inventory.hasNext()) {
			current = inventory.nextPolicy();
			Intent intent = new Intent(activity, current.getActivityClass());
			intent.putExtra(Flow.class.getName(), this);
			activity.startActivity(intent);
		}
	}
	
	private void updateStack(TescoActivity activity, StackBehaviour behaviour) {		
		switch (behaviour) {
		case CLEAR_ALL:
			ActivityCompat.finishAffinity(activity);
			return;
		case FINISH:
			activity.finish();
			return;
		case KEEP_ALIVE:
			return;
		}
	}
	
	@Override
	public void onPolicyComplete(TescoActivity activity) {
		this.currentActivity = activity;
		if(current != null && current.hasCompletion()) {
			PolicyAction action = current.getCompletion();
			action.setListener(this);
			action.execute(activity);
		} else {
			startNextPolicy(activity, current.getStackBehaviour());	
		}
	}

	@Override
	public void onPolicyError(TescoActivity activity, ErrorResponseWrapper errorResponseWrapper) {
		if(current != null && current.hasError()) {
			PolicyErrorAction action = current.getError();
			action.executeError(activity, errorResponseWrapper);
		}
	}
	
	@Override
	public void onPolicyCancelled(TescoActivity activity) {
		if(current != null && current.hasCancellation()) {
			PolicyAction action = current.getCancellation();
			action.execute(activity);
		}
	}
	
	@Override
	public void updatePolicyCancellation(PolicyAction policyAction) {
		current.setCancellation(policyAction);
	}
	
	@Override
	public PolicyAction getCurrentCancellationAction() {
		return current.getCancellation();
	}
	
	@Override
	public void onActionComplete(PolicyAction action) {
		if(action.equals(current.getCompletion())) {
			startNextPolicy(currentActivity, current.getStackBehaviour());
		}		
	}
	
	@Override
	public void onErrorActionComplete(PolicyErrorAction action) {
		if(action.equals(current.getCompletion())) {
			startNextPolicy(currentActivity, current.getStackBehaviour());
		}		
	}
	
	@Override
	public void applyAnimation(TescoActivity activity) {
		if(current != null && current.hasAnimation()) {
			PolicyAction action = current.getAnimation();
			action.execute(activity);
		}
	}
	
	@Override
	public ActionBarConfiguration getActionBarConfig() {
		return (current != null) ? current.getActionBarConfiguration() : null;
	}
	
	@Override
	public AnalyticsEvent getDisplayAnalytic() {
		return (current != null && current.hasDisplayAnalytic()) ? current.getDisplayAnalytic() : null;
	}
	
	private TescoFlow safelyCloneForNextActivity() {
		TescoFlow clone = null;
		try {
			clone = cloneForNextActivity();
		} catch (CloneNotSupportedException e) {
			// should never happen as all implement Cloneable
		}
		return clone;
	}

	/**
	 * Clones the tesco flow that will be made available to the next activity.
	 * This is necessary so as to maintain the current activity's flow state
	 * 
	 * This is a medium rather than a deep clone for efficiency. Deeper cloning
	 * can be implemented if and as required.
	 */
	private TescoFlow cloneForNextActivity() throws CloneNotSupportedException {
		TescoFlow clone = TescoFlow.class.cast(super.clone());
		clone.inventory = PolicyInventory.class.cast(inventory.clone());
		if(current != null) {
			clone.current = Policy.class.cast(current.clone());
		}
		return clone;
	}
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(titleId);
		dest.writeSerializable(initialStackBehaviour);
		dest.writeSerializable(inventory);
		dest.writeSerializable(current);
		dest.writeBundle(dataBundle);
	}

	public static final Parcelable.Creator<TescoFlow> CREATOR = new Parcelable.Creator<TescoFlow>() {
		public TescoFlow createFromParcel(Parcel in) {
			return new TescoFlow(in);
		}

		@Override
		public TescoFlow[] newArray(int size) {
			return new TescoFlow[size];
		}
	};
}
