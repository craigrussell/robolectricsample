package com.tescobank.mobile.flow;

import java.util.List;

public class TescoPolicyInventory implements PolicyInventory {

	private static final long serialVersionUID = 5248534353901809678L;
	
	private List<Policy> policies;
	private int next = 0;

	public TescoPolicyInventory(List<Policy> policies) {
		this.policies = policies;
	}

	@Override
	public void addPolicy(Policy policy) {
		policies.add(policies.size(), policy);
	}

	@Override
	public void addPolices(List<Policy> newPolicies) {
		policies.addAll(next, newPolicies);	
	}
	
	@Override
	public Policy nextPolicy() {
		if (hasNext()) {
			return policies.get(next++);
		}
		return null;
	}
	
	@Override
	public boolean hasNext() {
		return next < policies.size();
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
