package com.tescobank.mobile.flow;

import com.tescobank.mobile.ui.TescoActivity;

public class TescoFlowInitiator implements FlowInitiator {

	private TescoActivity activity;
	private Flow flow;
	
	public TescoFlowInitiator(TescoActivity activity, Flow flow) {
		this.flow = flow;
		this.activity = activity;
	}
		
	@Override
	public void execute() {
		flow.begin(activity);
	}	
}
