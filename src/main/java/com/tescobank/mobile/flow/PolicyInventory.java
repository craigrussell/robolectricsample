package com.tescobank.mobile.flow;

import java.io.Serializable;
import java.util.List;

public interface PolicyInventory extends Serializable, Cloneable {

	void addPolicy(Policy policy);

	void addPolices(List<Policy> newPolicies);
	
	boolean hasNext();
		
	Policy nextPolicy();

	Object clone() throws CloneNotSupportedException;
}
