package com.tescobank.mobile.flow;

import java.io.Serializable;

import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.ui.TescoActivity;

public interface PolicyErrorAction extends Serializable {

	void setListener(PolicyErrorActionListener listener);

	void executeError(TescoActivity activity, ErrorResponseWrapper errorResponseWrapper);
}
