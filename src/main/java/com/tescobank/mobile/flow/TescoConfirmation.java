package com.tescobank.mobile.flow;

import java.io.Serializable;

public class TescoConfirmation implements Serializable {
	
	private static final long serialVersionUID = -1958389682163056951L;
	private int title;
	private int message;

	public TescoConfirmation(int title, int message) {
		this.title = title;
		this.message = message;
	}

	public int getTitleId() {
		return title;
	}

	public int getMessageId() {
		return message;
	}
}
