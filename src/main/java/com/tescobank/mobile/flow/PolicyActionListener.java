package com.tescobank.mobile.flow;

public interface PolicyActionListener {
	void onActionComplete(PolicyAction action);
}
