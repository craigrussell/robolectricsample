package com.tescobank.mobile.flow;

import java.util.List;

import android.os.Bundle;
import android.os.Parcelable;

import com.tescobank.mobile.analytics.AnalyticsEvent;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.TescoActivity;

public interface Flow extends Parcelable {
	
	public enum StackBehaviour {
		CLEAR_ALL,
		FINISH,
		KEEP_ALIVE;
	}
	
	/**
	 * A flow may have a display title which is presented to the user
	 * @return the string resource id of the title, or 0 if this flow does not have a title
	 */
	int getTitleId();
			
	/**
	 * Sets the initial stack behaviour. This is the stack action that will be executed when the flow begins.
	 * For example the existing stack may be cleared when the flow begins.
	 * @param stackBehaviour
	 */
	void setInitialStackBehaviour(StackBehaviour stackBehaviour);
	
	Bundle getDataBundle();

	PolicyInventory getPolicyInventory();
	
	void addPolicy(Policy policy);

	void addPolices(List<Policy> newPolicies);
	
	/**
	 * Begins the flow
	 * @param activity the activity from which to trigger the flow
	 */
	void begin(TescoActivity activity);

	void onPolicyComplete(TescoActivity activity);
	
	void onPolicyCancelled(TescoActivity activity);

	void onPolicyError(TescoActivity activity, ErrorResponseWrapper errorResponseWrapper);
	
	PolicyAction getCurrentCancellationAction();

	void updatePolicyCancellation(PolicyAction policyAction);

	void applyAnimation(TescoActivity activity);

	/**
	 * @return the action bar configuration for the current policy.
	 */
	ActionBarConfiguration getActionBarConfig();

	/**
	 * @return the display analytic for the current policy. This is the analytic which should 
	 * be fired when an activity is first displayed.
	 */
	AnalyticsEvent getDisplayAnalytic();
}
