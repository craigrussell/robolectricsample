package com.tescobank.mobile.flow;


/**
 * Prepares a flow and seeks user confirmation to start it
 */
public interface FlowConfirmInitiator extends FlowInitiator {

	void showDialog();

	void dismissDialog();

	boolean isDialogShowing();
}
