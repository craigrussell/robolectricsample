package com.tescobank.mobile.flow;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoAlertDialog;

public class TescoFlowConfirmInitiator implements FlowConfirmInitiator {

	private TescoActivity activity;
	private AlertDialog alertDialog;
	private TescoConfirmation confirmation;
	private FlowInitiator flowInititator;
	
	private static final String TAG = TescoFlowConfirmInitiator.class.getName();
	
	public TescoFlowConfirmInitiator(TescoActivity activity, TescoConfirmation confirmation, FlowInitiator flowInitiator) {
		this.activity = activity;
		this.confirmation = confirmation;
		this.flowInititator = flowInitiator;
	}
	
	@Override
	public void showDialog() {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Showing confirmation");
		}

		TescoAlertDialog.Builder dialog = new TescoAlertDialog.Builder(activity);
		dialog.setTitle(confirmation.getTitleId());
		dialog.setMessage(confirmation.getMessageId());
		dialog.setNegativeButton(R.string.dialog_confirm_cancel, null);
		dialog.setPositiveButton(R.string.dialog_confirm_ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				execute();
			}
		});
		alertDialog = dialog.show();
	}
	
	@Override
	public void execute() {
		flowInititator.execute();
	}
	
	@Override
	public void dismissDialog() {
		if(alertDialog != null) {
			alertDialog.dismiss();
		}	
	}

	@Override
	public boolean isDialogShowing() {
		if(alertDialog != null) {
			return alertDialog.isShowing();
		}
		return false;
	}
}
