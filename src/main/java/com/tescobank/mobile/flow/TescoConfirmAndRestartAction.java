package com.tescobank.mobile.flow;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoAlertDialog;

public class TescoConfirmAndRestartAction extends TescoPolicyAction {

	private static final long serialVersionUID = -1633717900769853837L;

	private transient TescoActivity activity;
	private TescoConfirmation confirmation;

	public TescoConfirmAndRestartAction(TescoConfirmation confirmation) {
		this.confirmation = confirmation;
	}
	
	@Override
	public void execute(final TescoActivity activity) {
		this.activity = activity;
		showConfirmationDialog(activity);
	}

	private void showConfirmationDialog(final TescoActivity activity) {
		TescoAlertDialog.Builder dialog = new TescoAlertDialog.Builder(activity);
		dialog.setTitle(confirmation.getTitleId());
		dialog.setMessage(confirmation.getMessageId());
		dialog.setNegativeButton(R.string.dialog_confirm_cancel, null);
		dialog.setPositiveButton(R.string.dialog_confirm_ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				onActionCompleted();
			}
		});
		dialog.show();
	}
	
	@Override
	protected void onActionCompleted() {
		activity.logOut();
	}
}
