package com.tescobank.mobile.flow;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v4.app.ActivityCompat;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoAlertDialog;

public class TescoConfirmAndExitResetApplicationAction extends TescoPolicyAction {

	private static final long serialVersionUID = -1633717900769853837L;
	
	private TescoConfirmation confirmation;

	public TescoConfirmAndExitResetApplicationAction(TescoConfirmation confirmation) {
		this.confirmation = confirmation;
	}
	
	@Override
	public void execute(final TescoActivity activity) {
		showConfirmationDialog(activity);
	}

	private void showConfirmationDialog(final TescoActivity activity) {
		TescoAlertDialog.Builder dialog = new TescoAlertDialog.Builder(activity);
		dialog.setTitle(confirmation.getTitleId());
		dialog.setMessage(confirmation.getMessageId());
		dialog.setNegativeButton(R.string.dialog_confirm_cancel, null);
		dialog.setPositiveButton(R.string.dialog_confirm_ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				completeExecution(activity);
			}
		});
		dialog.show();
	}
	
	@SuppressLint("NewApi")
	private void completeExecution(TescoActivity activity) {
		activity.getApplicationState().getRestRequestProcessor().cancelAllRequests();
		activity.getApplicationState().resetApplication();
		ActivityCompat.finishAffinity(activity);
		onActionCompleted();
	}	
}
