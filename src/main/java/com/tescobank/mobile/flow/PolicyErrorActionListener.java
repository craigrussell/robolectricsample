package com.tescobank.mobile.flow;

public interface PolicyErrorActionListener {
	void onErrorActionComplete(PolicyErrorAction errorAction);
}
