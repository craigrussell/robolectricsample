package com.tescobank.mobile.flow;

import java.io.Serializable;

import com.tescobank.mobile.ui.TescoActivity;

public abstract class TescoPolicyAction implements PolicyAction, Serializable{

	private static final long serialVersionUID = -7252416986923113935L;

	private transient PolicyActionListener listener;

	@Override
	public void setListener(PolicyActionListener listener) {
		this.listener = listener;
	}

	@Override
	public void execute(TescoActivity activity) {
		onActionCompleted();
	}
	
	protected void onActionCompleted() {
		if(listener != null) {
			listener.onActionComplete(this);
		}
	}
}
