package com.tescobank.mobile.ui.wheel;

import android.view.MotionEvent;
import android.view.View;

/**
 * Calculates angles
 */
public class AngleCalculator {
	
	private static final int DEGREES_360 = 360;
	private static final int DEGREES_90 = 90;

	/**
	 * Constructor
	 */
	public AngleCalculator() {
		super();
	}
	
	/**
	 * Returns the angle from the center of the specified view to the location of the specified motion event within that view
	 * 
	 * @param view the view 
	 * @param event the motion event
	 * 
	 * @return the angle
	 */
	public int calculateAngle(View view, MotionEvent event) {
		
    	int fromCentreX = (int)event.getX() - (view.getWidth() / 2);
    	int fromCentreY = -((int)event.getY() - (view.getHeight() / 2));
    	
    	if ((fromCentreX == 0) && (fromCentreY == 0)) {
    		fromCentreY = 1;
    	}
    	
    	int angle = DEGREES_90 - (int)Math.toDegrees(Math.atan2(fromCentreY, fromCentreX));
    	if (angle < 0) {
    		angle = DEGREES_360 + angle;
    	}
    	
    	return angle;
	}
}
