package com.tescobank.mobile.ui.wheel;

import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

/**
 * Renders a rotated image view
 */
public class RotatedImageViewRenderer {

	/** The image view */
	private ImageView imageView;

	private float centerX;
	private float centerY;

	/**
	 * Constructor
	 * 
	 * @param imageView
	 *            the image view
	 */
	public RotatedImageViewRenderer(ImageView imageView) {
		super();

		this.imageView = imageView;

		centerX = BitmapDrawable.class.cast(imageView.getDrawable()).getBitmap().getWidth() / 2;
		centerY = BitmapDrawable.class.cast(imageView.getDrawable()).getBitmap().getHeight() / 2;
	}

	/**
	 * Renders the image view with the specified rotation angle
	 * 
	 * @param rotationAngle
	 *            the rotation angle
	 */
	public void renderRotatedImage(int rotationAngle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(rotationAngle, centerX, centerY);
		imageView.setImageMatrix(matrix);
	}
}
