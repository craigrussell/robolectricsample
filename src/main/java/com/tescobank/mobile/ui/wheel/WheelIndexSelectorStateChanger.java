package com.tescobank.mobile.ui.wheel;

import android.animation.IntEvaluator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.TargetApi;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.tescobank.mobile.BuildConfig;

/**
 * Changes the state of a wheel-based index selector
 */
public class WheelIndexSelectorStateChanger {

	private static final int DEGREES_360 = 360;
	private static final int DEGREES_180 = 180;
	private static final int ANIMATION_DURATION_THRESHOLD_DEGREES = 45;
	private static final int ANIMATION_DURATION_THRESHOLD_MS = 250;
	private static final String TAG = WheelIndexSelectorStateChanger.class.getSimpleName();

	/** The selector's state */
	private WheelIndexSelectorState state;

	/** The wheel angle change listener */
	private WheelAngleChangeListener wheelAngleChangeListener;

	/** The angle calculator */
	private AngleCalculator angleCalculator;

	/** The angle animator */
	private ValueAnimator angleAnimator;

	/**
	 * Constructor
	 * 
	 * @param state
	 *            the selector's state
	 * @param wheelAngleChangeListener
	 *            the wheel angle change listener
	 */
	public WheelIndexSelectorStateChanger(WheelIndexSelectorState state, WheelAngleChangeListener wheelAngleChangeListener) {
		super();

		this.state = state;
		this.wheelAngleChangeListener = wheelAngleChangeListener;
		this.angleCalculator = new AngleCalculator();
	}

	/**
	 * Handles the wheel being dragged round by the specified number of degrees
	 * 
	 * @param angleChange
	 *            the number of degrees
	 */
	public void handleDrag(int angleChange) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling drag");
		}
		state.setCurrentAngle(state.getCurrentAngle() + angleChange);
		wheelAngleChangeListener.angleChanged(state.getCurrentAngle());
	}

	/**
	 * Handles the wheel being clicked by selecting the clicked segment
	 * 
	 * @param view
	 *            the view that triggered the motion event
	 * @param event
	 *            the motion event
	 * 
	 * @return true if the click resulted in different index being selected
	 */
	public boolean handleClick(View view, MotionEvent event) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling click");
		}
		int clickAngle = angleCalculator.calculateAngle(view, event);
		int requiredAngle = state.getCurrentAngle() - clickAngle;
		int selectedSegement = getSelectedSegment(requiredAngle);
		int adjustedRequiredAngle = getCentreAngle(selectedSegement);

		rotateWheelTo(adjustedRequiredAngle);

		return setSelectedIndex(selectedSegement);
	}

	/**
	 * Handles the wheel being released by rotating to the centre of the selected segment
	 * 
	 * @return true if the release resulted in different index being selected
	 */
	public boolean handleRelease() {
		boolean selectedIndexChanged = setSelectedIndex();
		rotateWheelToCentreOfSelectedIndex();
		return selectedIndexChanged;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void cancelAnimation() {
		if (angleAnimator != null) {
			angleAnimator.cancel();
		}
	}

	private int getSelectedSegment(int angle) {
		return getCorrectedAngle(angle + (state.getDegreesPerIndex() / 2)) / state.getDegreesPerIndex();
	}

	private int getCentreAngle(int selectedSegment) {
		return selectedSegment * state.getDegreesPerIndex();
	}

	private int getCorrectedAngle(int angle) {
		if (angle < 0) {
			return DEGREES_360 + angle;
		} else if (angle >= DEGREES_360) {
			return angle - DEGREES_360;
		} else {
			return angle;
		}
	}

	private void setCurrentAngle(int angle) {
		state.setCurrentAngle(getCorrectedAngle(angle));
	}

	private boolean setSelectedIndex() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "setSelectedIndex() - currentAngle = " + state.getCurrentAngle());
		}
		return setSelectedIndex(getSelectedSegment(state.getCurrentAngle()));
	}

	private boolean setSelectedIndex(int selectedSegment) {
		state.setSelectedSegment(selectedSegment);
		int oldSelectedIndex = state.getSelectedIndex();
		state.setSelectedIndex((selectedSegment == 0) ? 0 : state.getIndexCount() - selectedSegment);
		return state.getSelectedIndex() != oldSelectedIndex;
	}

	private void rotateWheelToCentreOfSelectedIndex() {
		rotateWheelTo(getCentreAngle(state.getSelectedSegment()));
	}

	private void rotateWheelTo(int newAngle) {

		if (newAngle == state.getCurrentAngle()) {
			return;
		}
		
		state.setTargetAngle(newAngle);
		
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
			setCurrentAngle(newAngle);
			wheelAngleChangeListener.angleChanged(state.getCurrentAngle());
		} else {
			animateWheelToAngle(newAngle);
		}
		
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void animateWheelToAngle(int newAngle) {

		final int initialAngle = state.getCurrentAngle();
		int angleChange = newAngle - state.getCurrentAngle();

		if (angleChange > DEGREES_180) {
			angleChange = angleChange - DEGREES_360;
		} else if (angleChange < -DEGREES_180) {
			angleChange = angleChange + DEGREES_360;
		}

		angleAnimator = ValueAnimator.ofObject(new IntEvaluator(), 0, angleChange);
		angleAnimator.setDuration(getAnimationDuration(angleChange));
		angleAnimator.addUpdateListener(new AnimatorUpdateListener() {

			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				setCurrentAngle(initialAngle + (Integer) animation.getAnimatedValue());
				wheelAngleChangeListener.angleChanged(state.getCurrentAngle());
			}
		});

		angleAnimator.start();
	}

	private int getAnimationDuration(int angle) {
		return ((Math.abs(angle) / ANIMATION_DURATION_THRESHOLD_DEGREES) + 1) * ANIMATION_DURATION_THRESHOLD_MS;
	}
}
