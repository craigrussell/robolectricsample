package com.tescobank.mobile.ui.wheel;

/**
 * Listener for wheel angle changes
 */
public interface WheelAngleChangeListener {
	
	/**
	 * Called when the wheel angle changes
	 * 
	 * @param newAngle the wheel's new angle
	 */
	void angleChanged(int newAngle);
}
