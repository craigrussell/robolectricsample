package com.tescobank.mobile.ui.wheel;

import android.os.Bundle;
import android.widget.ImageView;

/**
 * Wheel-based index selector
 */
public class WheelIndexSelector {
	
	/** The selector's state */
	protected WheelIndexSelectorState state;
	
	/** The rotated image view renderer */
	private RotatedImageViewRenderer rotatedImageViewRenderer;
	
	/** The selector's on-touch listener */
	private WheelIndexSelectorOnTouchListener onTouchListener;

	/**
	 * Constructor
	 * 
	 * @param indexCount the number of indices
	 * @param wheelImageView the wheel image view
	 */
	public WheelIndexSelector(int indexCount, ImageView wheelImageView, float screenDensity) {
		super();
		
		this.state = new WheelIndexSelectorState(indexCount);
		this.rotatedImageViewRenderer = new RotatedImageViewRenderer(wheelImageView);
		this.onTouchListener = new WheelIndexSelectorOnTouchListener(state, createWheelAngleChangeListener(), screenDensity);
		
		wheelImageView.setOnTouchListener(onTouchListener);
	}
	
	/**
	 * @param wheelIndexSelectionListener the wheel index selection listener
	 */
	public void setWheelIndexSelectionListener(WheelIndexSelectionListener wheelIndexSelectionListener) {
		onTouchListener.setWheelIndexSelectionListener(wheelIndexSelectionListener);
	}
	
	/**
	 * @return the selected index
	 */
	public int getSelectedIndex() {
		return state.getSelectedIndex();
	}

	public void saveInstanceState(Bundle state) {
		this.state.saveInstanceState(state);
	}

	public void restoreInstanceState(Bundle state) {
		this.state.restoreInstanceState(state);
		rotatedImageViewRenderer.renderRotatedImage(this.state.getCurrentAngle());
	}

	private WheelAngleChangeListener createWheelAngleChangeListener() {
		return new WheelAngleChangeListener() {

			@Override
			public void angleChanged(int newAngle) {
				rotatedImageViewRenderer.renderRotatedImage(newAngle);
			}
		};
	}
}
