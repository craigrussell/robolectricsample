package com.tescobank.mobile.ui.wheel;

/**
 * Listener for wheel index selections
 */
public interface WheelIndexSelectionListener {
	
	/**
	 * Called when an index is selected
	 * 
	 * @param selectedIndex the selected index
	 */
	void indexSelected(int selectedIndex);
}
