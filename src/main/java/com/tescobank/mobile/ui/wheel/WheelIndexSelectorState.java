package com.tescobank.mobile.ui.wheel;

import android.os.Bundle;

/**
 * The state of a wheel-based index selector
 */
public class WheelIndexSelectorState implements Cloneable {

	private static final int DEGREES = 360;

	/** The number of indices */
	private final int indexCount;

	/** The number of degrees per index */
	private final int degreesPerIndex;

	/** The wheel's current angle */
	private int currentAngle;

	/** The selected segment */
	private int selectedSegment;

	/** The selected index */
	private int selectedIndex;

	private int targetAngle;

	/**
	 * Constructor
	 * 
	 * @param indexCount
	 *            the number of indices
	 */
	public WheelIndexSelectorState(int indexCount) {
		super();

		this.indexCount = indexCount;
		this.degreesPerIndex = DEGREES / indexCount;
		this.currentAngle = 0;
		this.selectedSegment = 0;
		this.selectedIndex = 0;
	}

	/**
	 * @return the number of indices
	 */
	public int getIndexCount() {
		return indexCount;
	}

	/**
	 * @return the number of degrees per index
	 */
	public int getDegreesPerIndex() {
		return degreesPerIndex;
	}

	/**
	 * @return the wheel's current angle
	 */
	public int getCurrentAngle() {
		return currentAngle;
	}

	/**
	 * @param currentAngle
	 *            the current angle to set
	 */
	public void setCurrentAngle(int currentAngle) {
		this.currentAngle = currentAngle;
	}

	/**
	 * @return the selected segment
	 */
	public int getSelectedSegment() {
		return selectedSegment;
	}

	/**
	 * @param selectedSegment
	 *            the selected segment to set
	 */
	public void setSelectedSegment(int selectedSegment) {
		this.selectedSegment = selectedSegment;
	}

	/**
	 * @return the selected index
	 */
	public int getSelectedIndex() {
		return selectedIndex;
	}

	/**
	 * @param selectedIndex
	 *            the selected index to set
	 */
	public void setSelectedIndex(int selectedIndex) {
		this.selectedIndex = selectedIndex;
	}

	public void saveInstanceState(Bundle state) {
		state.putInt("currentAngle", targetAngle);
		state.putInt("selectedSegment", selectedSegment);
		state.putInt("selectedIndex", selectedIndex);
	}

	public void restoreInstanceState(Bundle state) {
		this.currentAngle = state.getInt("currentAngle");
		this.selectedSegment = state.getInt("selectedSegment");
		this.selectedIndex = state.getInt("selectedIndex");
	}

	public WheelIndexSelectorState copy() {
		try {
			return WheelIndexSelectorState.class.cast(this.clone());
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	public void setTargetAngle(int newAngle) {
		this.targetAngle = newAngle;
	}

	public int getTargetAngle() {
		return targetAngle;
	}

}
