package com.tescobank.mobile.ui.wheel;

import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_MOVE;
import static android.view.MotionEvent.ACTION_UP;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.tescobank.mobile.BuildConfig;

/**
 * On-touch listener for a wheel-based index selector
 */
public class WheelIndexSelectorOnTouchListener implements OnTouchListener {
	
	private static final int CLICK_THRESHOLD_DISTANCE_DPI = 10;

	private static final int CLICK_THRESHOLD_DURATION_MS = 150;

	private static final String TAG = WheelIndexSelectorOnTouchListener.class.getSimpleName();
	
	/** The selector's state */
	private WheelIndexSelectorState state;
	
	/** The selector's state changer */
	private WheelIndexSelectorStateChanger stateChanger;
	
	/** The angle calculator */
	private AngleCalculator angleCalculator;
	
	/** The time at which the ACTION_DOWN event occurred */
	private long touchStartTime;
	
	/** The wheel's angle */
	private int oldAngle;
	
	/** The wheel index selection listener */
	private WheelIndexSelectionListener wheelIndexSelectionListener;

	private float actionDownX;

	private float actionDownY;
	
	private float screenDensity;
	
	/**
	 * Constructor
	 * 
	 * @param state the selector's state
	 * @param wheelAngleChangeListener the wheel angle change listener
	 */
	public WheelIndexSelectorOnTouchListener(WheelIndexSelectorState state, WheelAngleChangeListener wheelAngleChangeListener, float screenDensity) {
		super();
		
		this.state = state;
		this.stateChanger = new WheelIndexSelectorStateChanger(state, wheelAngleChangeListener);
		this.angleCalculator = new AngleCalculator();
		this.screenDensity = screenDensity;
	}
	
	/**
	 * @param wheelIndexSelectionListener the wheel index selection listener
	 */
	public void setWheelIndexSelectionListener(WheelIndexSelectionListener wheelIndexSelectionListener) {
		this.wheelIndexSelectionListener = wheelIndexSelectionListener;
	}
	
	@Override
	public boolean onTouch(View view, MotionEvent event) {
		
		stateChanger.cancelAnimation();
		
		switch (event.getAction()) {
		
			case ACTION_DOWN:
				handleActionDown(view, event);
				break;
				
            case ACTION_MOVE:
            	handleActionMove(view, event);
                break;
                
            case ACTION_UP:
            	handleActionUp(view, event);
            	break;
                
            default:
                break;
        }
		
        return true;
	}
	
	private void handleActionDown(View view, MotionEvent event) {
		touchStartTime = System.currentTimeMillis();
		actionDownX = event.getX();
		actionDownY = event.getY();
		oldAngle = angleCalculator.calculateAngle(view, event);
	}
	
	private void handleActionMove(View view, MotionEvent event) {
		
		int newAngle = angleCalculator.calculateAngle(view, event);
    	int angleChange = newAngle - oldAngle;
    	oldAngle = newAngle;
    	
    	if(BuildConfig.DEBUG) {
    		Log.d(TAG, "handleActionMove - angleChange = " + angleChange + ", newAngle = " + newAngle);
    	}
    	stateChanger.handleDrag(angleChange);
	}
	
	private void handleActionUp(View view, MotionEvent event) {
		
		boolean selectedIndexChanged = false;
    	
    	if (actionUpNowMeansClick(event)) {
    		selectedIndexChanged = stateChanger.handleClick(view, event);
    	} else {
    		selectedIndexChanged = stateChanger.handleRelease();
    	}
    	
    	if (selectedIndexChanged) {
    		notifyWheelIndexSelectionListener();
    	}
	}
	
	private boolean actionUpNowMeansClick(MotionEvent actionUpEvent) {
		if(BuildConfig.DEBUG) {
			Log.d(TAG, "Length of touch event = " + (System.currentTimeMillis() - touchStartTime) + "ms");
		}
		float changeInX = actionDownX - actionUpEvent.getX();
		float changeInY = actionDownY - actionUpEvent.getY();
		float dragDistance = (float) Math.sqrt(Math.pow(changeInX, 2) + Math.pow(changeInY, 2));
		
		if(BuildConfig.DEBUG) {
			Log.d(TAG, "Length of drag = " + dragDistance + "pixels");
		}

		return (dragDistance > CLICK_THRESHOLD_DISTANCE_DPI*screenDensity) || ((System.currentTimeMillis() - touchStartTime) > CLICK_THRESHOLD_DURATION_MS) ? false : true;
	}
	
	private void notifyWheelIndexSelectionListener() {
		if (wheelIndexSelectionListener == null) {
			return;
		}
		wheelIndexSelectionListener.indexSelected(state.getSelectedIndex());
	}
}
