package com.tescobank.mobile.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import com.tescobank.mobile.R;

public final class TypefaceAdapter {
	
	private TypefaceAdapter() {
		super();
	}
	
	public static void setTypeface(TypefaceDuckType duck, Context context, AttributeSet attrs) {

			TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TescoTypeface, 0, 0);
			String typeFaceName = a.getString(R.styleable.TescoTypeface_customTypeface);
			if (typeFaceName != null) {
				duck.setTypeface(TypefaceCache.get(context, typeFaceName));
			}
			a.recycle();
	}
}
