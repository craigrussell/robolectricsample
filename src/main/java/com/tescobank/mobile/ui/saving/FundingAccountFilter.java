package com.tescobank.mobile.ui.saving;

import java.util.LinkedList;
import java.util.List;

import com.tescobank.mobile.api.payment.RetrieveFundingAccountsResponse.Account;

public class FundingAccountFilter {

	private List<Account> fundingAccounts;

	public FundingAccountFilter(List<Account> fundingAccounts) {
		this.fundingAccounts = fundingAccounts;
	}
	
	public List<Account> getSavingsFundingAccount(String destinationAccountNumber) {
		List<Account> savingsFunding = new LinkedList<Account>();
		for (Account account : fundingAccounts) {
			if(account.getDirectDebitCreated() && destinationAccountNumber.equals(account.getDestinationAccountNumber())) {
				savingsFunding.add(account);
			}
		}
		return savingsFunding;
	}
}
