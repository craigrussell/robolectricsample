package com.tescobank.mobile.ui.saving.amount.other;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import java.math.BigDecimal;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.AmountFilter;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoEditText;

/**
 * Other amount selector
 */
public class OtherAmountSelector {

	private static final String SELECTED_AMOUNT = OtherAmountSelector.class.getSimpleName() + ".selectedAmount";
	private static final String OTHER_AMOUNT_INPUT = OtherAmountSelector.class.getSimpleName() + ".otherAmountInput";

	/** The other amount container */
	private View otherAmountContainer;

	/** The other amount input */
	private TescoEditText otherAmountInput;

	/** The selected amount */
	private BigDecimal selectedAmount;

	/** The other amount selection listener */
	private OtherAmountSelectionListener otherAmountSelectionListener;

	/** The activity */
	private TescoActivity activity;

	/**
	 * Constructor
	 * 
	 * @param activity
	 *            the activity
	 */
	public OtherAmountSelector(TescoActivity activity) {
		super();

		this.otherAmountContainer = activity.findViewById(R.id.saving_wheel_other_amount_container);
		this.otherAmountInput = TescoEditText.class.cast(otherAmountContainer.findViewById(R.id.saving_wheel_other_amount_input));

		this.otherAmountInput.addTextChangedListener(createTextChangedListener());
		this.otherAmountInput.setFilters(new InputFilter[] { new AmountFilter() });
		this.activity = activity;
	}

	/**
	 * @param otherAmountSelectionListener
	 *            the other amount selection listener
	 */
	public void setOtherAmountSelectionListener(OtherAmountSelectionListener otherAmountSelectionListener) {
		this.otherAmountSelectionListener = otherAmountSelectionListener;
	}

	public void show() {
		this.otherAmountInput = TescoEditText.class.cast(otherAmountContainer.findViewById(R.id.saving_wheel_other_amount_input));
		otherAmountContainer.setVisibility(VISIBLE);
		otherAmountInput.requestFocus();
		activity.showKeyboard();
		otherAmountSelectionListener.otherAmountActivated();
	}

	public void hide() {
		otherAmountContainer.setVisibility(GONE);
		activity.hideKeyboard();
		otherAmountSelectionListener.otherAmountDismissed();
	}

	public BigDecimal getSelectedAmount() {
		return selectedAmount;
	}

	private TextWatcher createTextChangedListener() {
		return new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				setSelectedAmount();
				notifyOtherAmountSelectionListener();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
		};
	}

	private void setSelectedAmount() {
		try {
			if (otherAmountInput.length() > 0) {
				otherAmountInput.setPrefix(activity.getResources().getString(R.string.movemoney_amount_selector_pound_symbol));
				otherAmountInput.setTextSuffixColour(activity.getResources().getColor(R.color.blue_tesco));
				selectedAmount = new BigDecimal(otherAmountInput.getText().toString());
			} else {
				otherAmountInput.setPrefix("");
				selectedAmount = null;
			}

		} catch (NumberFormatException e) {
			selectedAmount = null;
		}
	}

	private void notifyOtherAmountSelectionListener() {
		if (otherAmountSelectionListener == null) {
			return;
		}
		otherAmountSelectionListener.otherAmountSelected(selectedAmount);
	}

	public void restoreInstanceState(Bundle state) {
		otherAmountInput.setText(state.getCharSequence(OTHER_AMOUNT_INPUT));
		selectedAmount = BigDecimal.class.cast(state.getSerializable(SELECTED_AMOUNT));
	}

	public void saveInstanceState(Bundle state) {
		state.putCharSequence(OTHER_AMOUNT_INPUT, otherAmountInput.getText().toString());
		state.putSerializable(SELECTED_AMOUNT, selectedAmount);
	}

}
