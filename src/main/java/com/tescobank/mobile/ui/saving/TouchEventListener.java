package com.tescobank.mobile.ui.saving;

public interface TouchEventListener {
	void onTouchActionUp(int tag);
	void onTouchActionDown(int tag);
}
