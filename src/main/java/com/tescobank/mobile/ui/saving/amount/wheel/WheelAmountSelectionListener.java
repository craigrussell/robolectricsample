package com.tescobank.mobile.ui.saving.amount.wheel;

import java.math.BigDecimal;

/**
 * Listener for wheel-based amount selections
 */
public interface WheelAmountSelectionListener {
	
	/**
	 * Called when a specific amount is selected
	 * 
	 * @param selectedAmount the specific amount selected
	 */
	void amountSelected(BigDecimal selectedAmount);
	
	/**
	 * Called when the 'other amount' option is selected
	 */
	void otherAmountSelected();
}
