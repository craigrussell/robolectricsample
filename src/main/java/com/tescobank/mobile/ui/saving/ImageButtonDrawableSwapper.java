package com.tescobank.mobile.ui.saving;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

/**
 * 
 * Responsible for swapping images associated to state
 *
 */
public class ImageButtonDrawableSwapper extends ImageTouchEventListener{
	

	private Drawable disabledDrawable;
	private Drawable enabledDrawable;
	private ImageView imageView;

	public ImageButtonDrawableSwapper(TouchEventListener imageButtonListener, ImageView imageView, Drawable disabledDrawable, Drawable enabledDrawable) {
		super(imageButtonListener, imageView);
		this.imageView = imageView;
		this.enabledDrawable = enabledDrawable;
		this.disabledDrawable = disabledDrawable;	
	}
	
	public void setEnabledImageDrawable() {
		if(enabledDrawable == null) {
			return;
		}
		
		imageView.setImageDrawable(enabledDrawable);
	}
	
	public void setDisabledImageDrawable() {
		if(disabledDrawable == null) {
			return;
		}
		
		imageView.setImageDrawable(disabledDrawable);
	}
	
	
}
