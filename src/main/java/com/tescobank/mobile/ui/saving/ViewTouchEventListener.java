package com.tescobank.mobile.ui.saving;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

/**
 * 
 * Responsible for handing touch events for views. On Action UP callsback to the
 * observer in question
 * 
 */
public class ViewTouchEventListener implements OnTouchListener {

	private View view;
	private TouchEventListener touchEventListener;

	public ViewTouchEventListener(TouchEventListener touchEventListener, View view) {
		this.view = view;
		this.touchEventListener = touchEventListener;
	}

	public boolean onTouch(View v, MotionEvent event) {

		switch (event.getAction()) {
		case MotionEvent.ACTION_UP:
			touchEventListener.onTouchActionUp((Integer) view.getTag());
			break;
		default:
			break;
		}
		return true;
	}
}
