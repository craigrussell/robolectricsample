package com.tescobank.mobile.ui.saving;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;

public class BitmapTransparentPixelHelper {

	private Bitmap bitmap;
	private Matrix matrix;

	/**
	 * 
	 * @param bitmap the bitmap (drawable bitmap) to be used
	 * @param matrix the matrix of the image to be used
	 */
	public BitmapTransparentPixelHelper(Bitmap bitmap, Matrix matrix) {
		this.bitmap = bitmap;
		this.matrix = matrix;
	}

	/**
	 * Responsible for determining whether or not the selected pixel is
	 * transparent or not
	 * 
	 * @param Bitmap the bitmap (drawable bitmap) to be used
	 * @param ImageView the image to be used
	 * @param float the coordinate of the X motion event
	 * @param float the coordinate of the Y motion event
	 * @return boolean
	 */
	public boolean isTransparentPixelAt(float xEventCoordinate, float yEventCoordinate) {
		float[] eventXY = new float[] { xEventCoordinate, yEventCoordinate };
		eventXY = createInvertMatrixPointRange(eventXY);
		BitmapTransparentPixelEventXYCoordinateChecker xYCoordinateChecker =  new BitmapTransparentPixelEventXYCoordinateChecker((int) eventXY[0], (int) eventXY[1], bitmap);

		return Color.alpha(bitmap.getPixel(xYCoordinateChecker.getXEventCoordinate(), xYCoordinateChecker.getYEventCoordinate())) == 0;
	}

	/**
	 * Create an Inverted Matrix for the given image and map the points of the x
	 * and y floats and return the updated values
	 * 
	 * @param imageView
	 * @param xEventCoordinate
	 * @param yEventCoordinate
	 * @return
	 */
	private float[] createInvertMatrixPointRange(float[] eventXY) {

		Matrix invertMatrix = new Matrix();
		matrix.invert(invertMatrix);
		invertMatrix.mapPoints(eventXY);
		
		return eventXY;
	}
}
