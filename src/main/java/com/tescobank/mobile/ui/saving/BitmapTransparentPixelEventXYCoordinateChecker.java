package com.tescobank.mobile.ui.saving;

import android.graphics.Bitmap;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;

public class BitmapTransparentPixelEventXYCoordinateChecker {

	private static final String TAG = BitmapTransparentPixelEventXYCoordinateChecker.class.getSimpleName();
	
	private int xCoordinate;
	private int yCoordinate;
	private Bitmap bitmap;


	public BitmapTransparentPixelEventXYCoordinateChecker(int xCoordinate, int yCoordinate, Bitmap bitmap) {
		this.xCoordinate = xCoordinate;
		this.yCoordinate = yCoordinate;
		this.bitmap = bitmap;
	}

	/**
	 * Returns the x coordinate - may be updated if
	 * xEventCoordinate  < 0 
	 * xEventCoordinate > bitmap width -1
	 * 
	 * @param bitmap
	 * @param xEventCoordinate
	 * @return int
	 */
	public int getXEventCoordinate() {
		if (xCoordinate < 0) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "X Cordinate set to zero " + xCoordinate);
			}
			xCoordinate = 0;
		} else if (xCoordinate > bitmap.getWidth() - 1) {
			xCoordinate = bitmap.getWidth() - 1;
		}
		
		return xCoordinate;
	}

	/**
	 * Returns the x coordinate - may be updated
	 * yEventCoordinate  < 0 
	 * yEventCoordinate > bitmap height -1
	 * 
	 * @param bitmap
	 * @param yEventCoordinate
	 * @return int
	 */
	public int getYEventCoordinate() {
		if (yCoordinate < 0) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Y Cordinate set to zero " + yCoordinate);
			}
			yCoordinate = 0;
		} else if (yCoordinate > bitmap.getHeight() - 1) {
			yCoordinate = bitmap.getHeight() - 1;
		}

		return yCoordinate;
	}
}
