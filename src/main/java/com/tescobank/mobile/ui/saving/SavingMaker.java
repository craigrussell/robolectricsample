package com.tescobank.mobile.ui.saving;

import static com.tescobank.mobile.api.payment.MakePaymentToTescoAccountRequest.Frequency.NO_FREQUENCY;
import static com.tescobank.mobile.api.payment.MakePaymentToTescoAccountRequest.FundingType.ONE_TIME;
import static java.lang.Boolean.FALSE;

import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.payment.GetTransacteesForUserResponse.Transactee;
import com.tescobank.mobile.api.payment.MakePaymentToTescoAccountRequest;
import com.tescobank.mobile.api.payment.MakePaymentToTescoAccountResponse;
import com.tescobank.mobile.api.payment.RetrieveFundingAccountsResponse.Account.Transactee.BankAccount;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.movemoney.Saving;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.ProgressRenderer;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoAlertDialog;
import com.tescobank.mobile.ui.TescoTextView;
import com.tescobank.mobile.ui.movemoney.MoneyMovementListener;

/**
 * Makes a saving
 */
public class SavingMaker {

	private enum State {
		CONFIRM_PROCEED_WITH_SAVE_NOW, CONFIRM_PROCEED_WITHIN_ISA_TAX_YEAR_WARNING_PERIOD, SHOW_BREACHING_LIMIT,
	};

	private static final String TAG = SavingMaker.class.getSimpleName();
	
	private static final String STATE_SAVING = SavingMaker.class.getSimpleName() + ".saving";
	private static final String STATE = SavingMaker.class.getSimpleName() + ".state";

	private static final String EMPTY_STRING = "";

	private TescoActivity activity;

	private ProgressRenderer progressRenderer;

	private MoneyMovementListener<Saving> moneyMovementListener;

	private boolean savingInProgress;

	private AlertDialog dialog;

	private Saving saving;

	private State state;

	/**
	 * Constructor
	 * 
	 * @param activity
	 *            the activity
	 * @param progressRenderer
	 *            the progress renderer
	 * @param moneyMovementListener
	 *            the money movement listener
	 */
	public SavingMaker(TescoActivity activity, ProgressRenderer progressRenderer, MoneyMovementListener<Saving> moneyMovementListener) {
		super();

		this.activity = activity;
		this.progressRenderer = progressRenderer;
		this.moneyMovementListener = moneyMovementListener;
	}

	/**
	 * @return whether a saving is in progress
	 */
	public boolean isSavingInProgress() {
		return savingInProgress;
	}

	/**
	 * Makes a saving
	 * 
	 * @param saving
	 *            the saving
	 */
	public void makeSaving(Saving saving) {
		this.saving = saving;
		confirmProceedWithSaveNow();
	}

	private void confirmProceedWithSaveNow() {
		this.state = State.CONFIRM_PROCEED_WITH_SAVE_NOW;

		int title = R.string.saving_save_now_proceed_title;
		int ok = R.string.saving_warning_confirm;
		int cancel = R.string.saving_warning_cancel;

		String selectedAmount = new DataFormatter().formatCurrency(saving.getAmount());
		TescoTextView accountName = (TescoTextView) activity.findViewById(R.id.saving_funding_account_name);
		String message = activity.getString(R.string.saving_save_now_proceed_message, selectedAmount, accountName.getText());

		OnClickListener listener = new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == Dialog.BUTTON_POSITIVE) {
					if (saving.isISADestination()) {
						checkForISALimitAndTaxYearThresholdsBeforeMakingSaving(saving);
					} else {
						executeMakeSavingRequest();
					}
				} else {
					reset();
				}
			}
		};
		AlertDialog.Builder builder = new TescoAlertDialog.Builder(activity).setTitle(title).setMessage(Html.fromHtml(message)).setPositiveButton(ok, listener).setNegativeButton(cancel, listener);

		dialog = builder.show();
		TextView messageView = (TextView) dialog.findViewById(android.R.id.message);
		messageView.setText(Html.fromHtml(message));
		messageView.setGravity(Gravity.CENTER);
		messageView.setTextSize(16);
	}

	private void checkForISALimitAndTaxYearThresholdsBeforeMakingSaving(Saving saving) {
		if (saving.isBreachingLimits(saving.isNextTaxYear(), activity.getApplicationState().getNextTaxYearSubscriptionLimit(saving.getDestination()))) {
			showBreachingLimit();
		} else if (saving.isWithinISAYearEndWarningPeriod()) {
			confirmProceedWithinISATaxYearWarningPeriod();
		} else {
			executeMakeSavingRequest();
		}
	}

	private void showBreachingLimit() {
		state = State.SHOW_BREACHING_LIMIT;
		dialog = new TescoAlertDialog.Builder(activity).setTitle(R.string.saving_breaching_alert_title).setMessage(R.string.saving_breaching_alert_message).setPositiveButton(android.R.string.ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				reset();
			}
		}).create();
		dialog.show();
	}

	private void reset() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "reset()");
		}
		saving = null;
		state = null;
		dialog = null;
	}

	private void confirmProceedWithinISATaxYearWarningPeriod() {
		state = State.CONFIRM_PROCEED_WITHIN_ISA_TAX_YEAR_WARNING_PERIOD;

		int title = R.string.saving_isa_next_tax_year_warning_title;
		int ok = R.string.saving_warning_confirm;
		int cancel = R.string.saving_warning_cancel;

		String paymentLimitDate = new DataFormatter().formatDateAsDayFullMonthAndYear(saving.getISAYearEndWarningDate().getTime());
		String message = activity.getString(R.string.saving_isa_next_tax_year_warning_message, paymentLimitDate);

		OnClickListener listener = new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == Dialog.BUTTON_POSITIVE) {
					executeMakeSavingRequest();
				} else {
					reset();
				}
			}
		};

		dialog = new TescoAlertDialog.Builder(activity).setTitle(title).setMessage(message).setPositiveButton(ok, listener).setNegativeButton(cancel, listener).show();
	}

	private void executeMakeSavingRequest() {
		savingInProgress = true;
		progressRenderer.showInProgressIndicator();
		activity.getApplicationState().getRestRequestProcessor().processRequest(createRequest(saving));
		reset();
	}

	private MakePaymentToTescoAccountRequest createRequest(final Saving saving) {

		MakePaymentToTescoAccountRequest request = new MakePaymentToTescoAccountRequest() {

			@Override
			public void onResponse(MakePaymentToTescoAccountResponse response) {
				savingInProgress = false;
				progressRenderer.hideInProgressIndicator();
				moneyMovementListener.moneyMovementSuccessful(saving);
			}

			@Override
			public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
				savingInProgress = false;
				progressRenderer.hideInProgressIndicator();
				if(errorResponseWrapper.getErrorResponse().isDeclined()) {
					moneyMovementListener.moneyMovementDeclined(saving, errorResponseWrapper);
				} else {
					activity.handleErrorResponse(errorResponseWrapper);
				}
			}
		};

		BankAccount source = saving.getSource().getTransactee().getBankAccount();
		ProductDetails destination = saving.getDestination();
		Date date = saving.getDate();

		request.setTbId(activity.getApplicationState().getTbId());
		request.setProductCategory(destination.getProductCategory());
		request.setAccountNumber(source.getAccountNumber());
		request.setSortCode(source.getSortCode());
		request.setDestinationAccountNumber(getDestinationAccountNumber(destination));
		request.setAmount(saving.getAmount());
		request.setProcessingDate(date);
		request.setValidFromDate(date);
		request.setFundingType(ONE_TIME);
		request.setFrequency(NO_FREQUENCY);
		request.setCreateFundingDirectDebit(FALSE);
		request.setTransacteeId(getTransacteeId(source));

		return request;
	}

	private String getDestinationAccountNumber(ProductDetails destination) {
		return destination.getSortCode() + destination.getAccountNumber();
	}

	private String getTransacteeId(BankAccount source) {

		List<Transactee> transactees = activity.getApplicationState().getTransactees();

		for (Transactee transactee : transactees) {
			if (transactee.isLinked() && transacteePropertiesMatch(source, transactee)) {
				return transactee.getTransacteeId();
			}
		}

		for (Transactee transactee : transactees) {
			if (transactee.isLinked() && accountPropertiesMatch(source, transactee)) {
				return transactee.getTransacteeId();
			}
		}

		return null;
	}

	private boolean transacteePropertiesMatch(BankAccount source, Transactee transactee) {

		String sourceAccountName = source.getAccountName();
		String sourceAccountNickName = source.getAccountNickName();

		String transacteeAccountName = transactee.getTransacteeName();
		String transacteeAccountNickName = transactee.getTransacteeNickName();

		return accountPropertiesMatch(source, transactee) && stringsMatch(sourceAccountName, transacteeAccountName) && stringsMatch(sourceAccountNickName, transacteeAccountNickName);
	}

	private boolean accountPropertiesMatch(BankAccount source, Transactee transactee) {

		String sourceSortCode = source.getSortCode();
		String sourceAccountNumber = source.getAccountNumber();

		String transacteeSortCode = transactee.getBankAccount().getSortCode();
		String transacteeAccountNumber = transactee.getBankAccount().getAccountNumber();

		return stringsMatch(sourceSortCode, transacteeSortCode) && stringsMatch(sourceAccountNumber, transacteeAccountNumber);
	}

	private boolean stringsMatch(String string1, String string2) {
		return getNonNullString(string1).equalsIgnoreCase(getNonNullString(string2));
	}

	private String getNonNullString(String string) {
		return (string == null) ? EMPTY_STRING : string;
	}

	public void saveInstanceState(Bundle outState) {
		if (null != dialog) {
			outState.putSerializable(STATE_SAVING, saving);
			outState.putSerializable(STATE, state);
			dialog.dismiss();
		}
	}

	public void restoreInstanceState(Bundle savedInstanceState) {
		saving = Saving.class.cast(savedInstanceState.getSerializable(STATE_SAVING));
		state = State.class.cast(savedInstanceState.getSerializable(STATE));
		if (null != saving) {
			switch (state) {
			case CONFIRM_PROCEED_WITH_SAVE_NOW:
				confirmProceedWithSaveNow();
				break;

			case CONFIRM_PROCEED_WITHIN_ISA_TAX_YEAR_WARNING_PERIOD:
				confirmProceedWithinISATaxYearWarningPeriod();
				break;

			case SHOW_BREACHING_LIMIT:
				showBreachingLimit();
				break;
			}
		}
	}

}
