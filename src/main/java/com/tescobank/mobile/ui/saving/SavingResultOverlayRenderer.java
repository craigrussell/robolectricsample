package com.tescobank.mobile.ui.saving;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.model.movemoney.Saving;
import com.tescobank.mobile.ui.overlay.ResultOverlayRenderer;

/**
 * Renders result overlays required by the saving activity
 */
public class SavingResultOverlayRenderer extends ResultOverlayRenderer {
	
	/**
	 * Constructor
	 * 
	 * @param activity the activity
	 */
	public SavingResultOverlayRenderer(Activity activity) {
		super(activity);
	}
	
	/**
	 * Renders a saving success overlay
	 * 
	 * @param saving the saving
	 * @param onDismissListener the listener called when the overlay is dismissed
	 */
	public void renderSavingSuccessOverlay(Saving saving, OnDismissListener onDismissListener) {
		View overlay = getActivity().getLayoutInflater().inflate(R.layout.movemoney_result_overlay, null);
		
		TextView resultMessageStart = (TextView) overlay.findViewById(R.id.result_message_start);
		resultMessageStart.setText(R.string.savings_result_success_message_start);
		
		TextView resultAmount = (TextView) overlay.findViewById(R.id.result_amount);
		resultAmount.setText(getDataFormatter().formatCurrency(saving.getAmount()));
		
		TextView resultMessageEnd = (TextView) overlay.findViewById(R.id.result_message_end);
		resultMessageEnd.setText(R.string.savings_result_success_message_end);
		
		renderOverlay(overlay, false, onDismissListener);
	}
	
	/**
	 * Renders a saving declined overlay
	 * 
	 * @param saving the saving
	 * @param onDismissListener the listener called when the overlay is dismissed
	 */
	public void renderSavingDeclinedOverlay(Saving saving, OnDismissListener onDismissListener) {
		
		View overlay = getActivity().getLayoutInflater().inflate(R.layout.movemoney_result_overlay, null);
		
		ImageView resultIcon = (ImageView) overlay.findViewById(R.id.result_icon);
		resultIcon.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_fail));
		
		TextView resultTitle = (TextView) overlay.findViewById(R.id.result_title);
		resultTitle.setText(R.string.savings_result_decline_title);
		
		TextView resultMessageStart = (TextView) overlay.findViewById(R.id.result_message_start);
		resultMessageStart.setText(R.string.savings_result_decline_message_start);
		
		TextView resultAmount = (TextView) overlay.findViewById(R.id.result_amount);
		resultAmount.setText(getDataFormatter().formatCurrency(saving.getAmount()));
		
		TextView resultMessageEnd = (TextView) overlay.findViewById(R.id.result_message_end);
		resultMessageEnd.setText(R.string.savings_result_decline_message_end);
		
		TextView resultMessageFooter = (TextView) overlay.findViewById(R.id.result_footer);
		resultMessageFooter.setText(R.string.savings_result_decline_message_footer);
		resultMessageFooter.setVisibility(View.VISIBLE);
		
		renderOverlay(overlay, false, onDismissListener);
	}
}
