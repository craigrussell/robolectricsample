package com.tescobank.mobile.ui.saving;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

/**
 * Responsible for handing touch events for images. Also checks to see is pixel
 * in question is transparent. Depending result of the check will determine the
 * result returned. Also on the Action UP event callsback to the observer in
 * question
 * 
 */
public class ImageTouchEventListener implements OnTouchListener {

	private ImageView imageView;
	private TouchEventListener imageTouchEventListener;
	private BitmapTransparentPixelHelper bitmapTransparentPixelHelper;

	public ImageTouchEventListener(TouchEventListener imageTouchEventListener, ImageView imageView) {

		this.imageView = imageView;
		this.imageTouchEventListener = imageTouchEventListener;
		Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
		this.bitmapTransparentPixelHelper = new BitmapTransparentPixelHelper(bitmap, this.imageView.getImageMatrix());
	}

	public boolean onTouch(View v, MotionEvent event) {
		boolean transparentPixelSelected = bitmapTransparentPixelHelper.isTransparentPixelAt(event.getX(), event.getY());

		switch (event.getAction()) {
		case MotionEvent.ACTION_UP:
			imageTouchEventListener.onTouchActionUp((Integer) imageView.getTag());
			break;
		default:
			break;

		}
		return !transparentPixelSelected;
	}
}
