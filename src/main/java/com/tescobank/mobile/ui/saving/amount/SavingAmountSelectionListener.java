package com.tescobank.mobile.ui.saving.amount;

import java.math.BigDecimal;

/**
 * Listener for amount selections for saving
 */
public interface SavingAmountSelectionListener {
	
	/**
	 * Called when an amount is selected
	 * 
	 * @param selectedAmount the amount selected
	 */
	void amountSelected(BigDecimal selectedAmount);
	
	void otherAmountActivated();
	
	void otherAmountDismissed();
}
