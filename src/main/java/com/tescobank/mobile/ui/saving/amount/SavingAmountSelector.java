package com.tescobank.mobile.ui.saving.amount;

import java.math.BigDecimal;

import android.os.Bundle;

import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.saving.amount.other.OtherAmountSelectionListener;
import com.tescobank.mobile.ui.saving.amount.other.OtherAmountSelector;
import com.tescobank.mobile.ui.saving.amount.wheel.WheelAmountSelectionListener;
import com.tescobank.mobile.ui.saving.amount.wheel.WheelAmountSelector;

/**
 * Amount selector for saving
 */
public class SavingAmountSelector {

	private static final String STATE_SELECTED_AMOUNT = SavingAmountSelector.class.getSimpleName() + ".selectedAmount";

	/** The other amount selector */
	private OtherAmountSelector otherAmountSelector;

	/** The selected amount */
	private BigDecimal selectedAmount;

	/** The saving amount selection listener */
	private SavingAmountSelectionListener savingAmountSelectionListener;

	private WheelAmountSelector wheelAmountSelector;

	/**
	 * Constructor
	 * 
	 * @param activity
	 *            the activity
	 */
	public SavingAmountSelector(TescoActivity activity) {
		super();

		wheelAmountSelector = new WheelAmountSelector(activity);

		this.otherAmountSelector = new OtherAmountSelector(activity);
		this.selectedAmount = wheelAmountSelector.getSelectedAmount();

		wheelAmountSelector.setWheelAmountSelectionListener(createWheelAmountSelectionListener());
		this.otherAmountSelector.setOtherAmountSelectionListener(createOtherAmountSelectionListener());
	}

	/**
	 * @param savingAmountSelectionListener
	 *            the saving amount selection listener
	 */
	public void setSavingAmountSelectionListener(SavingAmountSelectionListener savingAmountSelectionListener) {
		this.savingAmountSelectionListener = savingAmountSelectionListener;
	}

	/**
	 * @return the selected amount
	 */
	public BigDecimal getSelectedAmount() {
		return selectedAmount;
	}

	private WheelAmountSelectionListener createWheelAmountSelectionListener() {
		return new WheelAmountSelectionListener() {

			@Override
			public void amountSelected(BigDecimal selectedAmount) {
				otherAmountSelector.hide();
				SavingAmountSelector.this.selectedAmount = selectedAmount;
				notifySavingAmountSelectionListener();
			}

			@Override
			public void otherAmountSelected() {
				otherAmountSelector.show();
				SavingAmountSelector.this.selectedAmount = otherAmountSelector.getSelectedAmount();
				notifySavingAmountSelectionListener();
			}
		};
	}

	private OtherAmountSelectionListener createOtherAmountSelectionListener() {
		return new OtherAmountSelectionListener() {

			@Override
			public void otherAmountSelected(BigDecimal selectedAmount) {
				SavingAmountSelector.this.selectedAmount = selectedAmount;
				notifySavingAmountSelectionListener();
			}

			@Override
			public void otherAmountActivated() {
				savingAmountSelectionListener.otherAmountActivated();
			}

			@Override
			public void otherAmountDismissed() {
				savingAmountSelectionListener.otherAmountDismissed();
			}
		};
	}

	private void notifySavingAmountSelectionListener() {
		if (savingAmountSelectionListener == null) {
			return;
		}
		savingAmountSelectionListener.amountSelected(selectedAmount);
	}

	public void restoreInstanceState(Bundle state) {
		otherAmountSelector.restoreInstanceState(state);
		wheelAmountSelector.restoreInstanceState(state);
		selectedAmount = BigDecimal.class.cast(state.getSerializable(STATE_SELECTED_AMOUNT));
		if (wheelAmountSelector.isOtherSelected()) {
			otherAmountSelector.show();
		}
	}

	public void saveInstanceState(Bundle state) {
		state.putSerializable(STATE_SELECTED_AMOUNT, selectedAmount);
		otherAmountSelector.saveInstanceState(state);
		wheelAmountSelector.saveInstanceState(state);
	}
}
