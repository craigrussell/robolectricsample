package com.tescobank.mobile.ui.saving.amount.other;

import java.math.BigDecimal;

/**
 * Listener for other amount selections
 */
public interface OtherAmountSelectionListener {
	
	/**
	 * Called when an other amount is selected
	 * 
	 * @param selectedAmount the other amount selected
	 */
	void otherAmountSelected(BigDecimal selectedAmount);
		
	void otherAmountActivated();
	
	void otherAmountDismissed();
}
