package com.tescobank.mobile.ui.saving;

public interface TouchEventHelperListener {
	void onTouchActionUp(int tag);
	void onTouchActionDown(int tag);
}
