package com.tescobank.mobile.ui.saving;

import static com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError.NoFundingAccounts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow.OnDismissListener;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.payment.RetrieveFundingAccountsResponse.Account;
import com.tescobank.mobile.api.payment.RetrieveFundingAccountsResponse.Account.Transactee.BankAccount;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.movemoney.Saving;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.ActivityRequestCode;
import com.tescobank.mobile.ui.ActivityResultCode;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.ProgressRenderer;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoTextView;
import com.tescobank.mobile.ui.errors.ErrorHandler;
import com.tescobank.mobile.ui.movemoney.MoneyMovementListener;
import com.tescobank.mobile.ui.prodheader.ProductHeaderViewFactory;
import com.tescobank.mobile.ui.saving.amount.SavingAmountSelectionListener;
import com.tescobank.mobile.ui.saving.amount.SavingAmountSelector;
import com.tescobank.mobile.ui.tutorial.TooltipId;
import com.tescobank.mobile.ui.tutorial.groups.SavingsWheelTooltipGroup;

/**
 * Activity for saving
 */
public class SavingActivity extends TescoActivity implements ProgressRenderer, ErrorHandler, MoneyMovementListener<Saving>, TouchEventListener {

	public static final String TAG = SavingActivity.class.getSimpleName();

	public static final String DESTINATION_PRODUCT_EXTRA = "destinationProduct";
	public static final String FUNDING_ACCOUNTS_EXTRA = "fundingAccounts";
	public static final String SELECTED_FUNDING_ACCOUNT_INDEX_EXTRA = "selectedFundingAccountIndex";
	private static final String STATE_SELECTED_FUNDING_ACCOUNT_INDEX = SavingActivity.class.getSimpleName() + "." + SELECTED_FUNDING_ACCOUNT_INDEX_EXTRA;

	private static final int NO_ACCOUNTS_MESSAGE_DELAY = 1000;

	private static final String STATE_SUCCESSFUL_SAVING = SavingActivity.class.getSimpleName() + ".successfulSaving";
	private static final String STATE_DECLINED_SAVING = SavingActivity.class.getSimpleName() + ".declinedSaving";

	private DataFormatter df;

	private ProductDetails destinationProduct;
	private List<Account> fundingAccounts;
	private int selectedFundingAccountIndex;

	private View fundingAccountContainer;
	private TescoTextView fundingAccountNameTextView;
	private TescoTextView fundingAccountSortTextView;
	private TescoTextView fundingAccountNumberTextView;
	private ImageView fundingAccountSelectionChevron;
	private InProgressIndicator inProgressIndicator;
	private View footer;
	private TescoTextView footerText;

	private SavingAmountSelector amountSelector;
	private SavingMaker savingMaker;
	private SavingResultOverlayRenderer resultOverlayRenderer;
	private ImageButtonDrawableSwapper savingsImageButtonHelper;
	private boolean noFundingAccountsMessageDisplayed;

	private Saving successfulMoneyMovement;

	private Saving declinedMoneyMovement;

	private Bundle savedInstanceState;

	private boolean finishAfterOverlay;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		df = new DataFormatter();

		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		setContentView(R.layout.activity_saving);
		destinationProduct = (ProductDetails) getIntent().getExtras().getSerializable(DESTINATION_PRODUCT_EXTRA);
		fundingAccountSelectionChevron = (ImageView) findViewById(R.id.savings_funding_account_selection_chevron);
		footer = findViewById(R.id.savings_payment_message_container);
		footerText = (TescoTextView) findViewById(R.id.informationText);
		inProgressIndicator = new InProgressIndicator(this, false);

		savingMaker = new SavingMaker(this, this, this);
		resultOverlayRenderer = new SavingResultOverlayRenderer(this);

		configureHeader();
		configureFundingAccount();
		configureAmountSelector();
		configureSaveButton();
		
		getTooltipManager().setGroup(new SavingsWheelTooltipGroup(fundingAccounts.size()));

	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// override to prevent default behaviour (android will automatically repopulate fields)
		this.savedInstanceState = savedInstanceState;
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		if (hasFocus) {
			if (savedInstanceState != null) {
				applySavedState();
			}
		}

	}

	protected void applySavedState() {
		selectedFundingAccountIndex = savedInstanceState.getInt(STATE_SELECTED_FUNDING_ACCOUNT_INDEX);
		if (!fundingAccounts.isEmpty()) {
			configureFundingAccountTextForSelection();
		}

		amountSelector.restoreInstanceState(savedInstanceState);
		setSaveButtonState();

		savingMaker.restoreInstanceState(savedInstanceState);

		declinedMoneyMovement = Saving.class.cast(savedInstanceState.getSerializable(STATE_DECLINED_SAVING));
		successfulMoneyMovement = Saving.class.cast(savedInstanceState.getSerializable(STATE_SUCCESSFUL_SAVING));

		if (null != declinedMoneyMovement) {
			showDeclinedOverlay(declinedMoneyMovement);
		} else if (null != successfulMoneyMovement) {
			showSuccessOverlay(successfulMoneyMovement);
		}
		savedInstanceState = null;
	}

	@Override
	protected void onPause() {
		super.onPause();
		getTooltipManager().hide();
		cancelTasks();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt(STATE_SELECTED_FUNDING_ACCOUNT_INDEX, selectedFundingAccountIndex);
		amountSelector.saveInstanceState(outState);
		savingMaker.saveInstanceState(outState);

		outState.putSerializable(STATE_SUCCESSFUL_SAVING, successfulMoneyMovement);
		outState.putSerializable(STATE_DECLINED_SAVING, declinedMoneyMovement);

		finishAfterOverlay = false;
		resultOverlayRenderer.dismissOverlay();
	}

	@Override
	protected void onResume() {
		super.onResume();
		displayNoFundingAccountsPassiveMessage();
		getTooltipManager().show();
	}

	private void cancelTasks() {
		resultOverlayRenderer.cancelTasks();
	}

	@Override
	public void onBackPressed() {

		if (savingMaker.isSavingInProgress()) {
			return;
		}

		if (resultOverlayRenderer.dismissOverlay()) {
			return;
		}

		super.onBackPressed();
	}

	@Override
	public void showInProgressIndicator() {
		inProgressIndicator.showInProgressIndicator();
		savingsImageButtonHelper.setDisabledImageDrawable();
	}

	@Override
	public void hideInProgressIndicator() {
		inProgressIndicator.hideInProgressIndicator();
		savingsImageButtonHelper.setEnabledImageDrawable();
	}

	@Override
	public void handleError(ErrorResponseWrapper errorResponseWrapper) {
		handleErrorResponse(errorResponseWrapper);
	}

	@Override
	public void moneyMovementSuccessful(Saving saving) {
		successfulMoneyMovement = saving;
		getApplicationState().getAnalyticsTracker().trackMakeSavingCompletedSuccessfully(saving);
		showSuccessOverlay(saving);
	}

	@Override
	public void moneyMovementDeclined(Saving saving, ErrorResponseWrapper errorResponseWrapper) {
		declinedMoneyMovement = saving;
		getApplicationState().getAnalyticsTracker().trackMakeSavingCompletedUnsuccessfully(saving);
		showDeclinedOverlay(saving);
	}

	private void showSuccessOverlay(Saving saving) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "showing success overlay");
		}

		finishAfterOverlay = true;
		resultOverlayRenderer.renderSavingSuccessOverlay(saving, createSuccessOverlayDismissListener());
	}

	private void showDeclinedOverlay(Saving saving) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "showing declined overlay");
		}

		finishAfterOverlay = true;
		resultOverlayRenderer.renderSavingDeclinedOverlay(saving, createSuccessOverlayDismissListener());
	}

	private OnDismissListener createSuccessOverlayDismissListener() {
		return new OnDismissListener() {

			@Override
			public void onDismiss() {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "overlay dismissed, finishing: " + finishAfterOverlay);
				}

				if (finishAfterOverlay) {
					finish();
				}
			}
		};
	}

	private void configureHeader() {
		ViewGroup headerContainer = (ViewGroup) findViewById(R.id.saving_header);
		ProductHeaderViewFactory productHeaderViewFactory = new ProductHeaderViewFactory(destinationProduct, this, headerContainer);
		productHeaderViewFactory.createHeaderView();
	}

	private void configureFundingAccount() {
		fundingAccountContainer = findViewById(R.id.savings_funding_account_container);
		fundingAccountNameTextView = (TescoTextView) findViewById(R.id.saving_funding_account_name);
		fundingAccountSortTextView = (TescoTextView) findViewById(R.id.saving_funding_account_sort_value);
		fundingAccountNumberTextView = (TescoTextView) findViewById(R.id.saving_funding_account_num_value);

		List<Account> allFundingAccounts = Arrays.asList(getApplicationState().getFundingAccounts());
		FundingAccountFilter filter = new FundingAccountFilter(allFundingAccounts);
		fundingAccounts = filter.getSavingsFundingAccount(destinationProduct.getAccountNumber());

		if (fundingAccounts.size() == 0) {
			onNoFundingAccount();
		} else if (fundingAccounts.size() == 1) {
			onSingleFundingAccount();
		} else {
			onMultipleFundingAccounts();
		}
	}

	private void onNoFundingAccount() {
		configureNoLinkedAccountsMessage();
		fundingAccountContainer.setEnabled(false);
	}

	private void onSingleFundingAccount() {
		fundingAccountContainer.setEnabled(false);
		configureFundingAccountTextForSelection();
		configurePaymentDelayMessage();
	}

	private void onMultipleFundingAccounts() {
		selectedFundingAccountIndex = 0;
		fundingAccountContainer.setEnabled(true);
		fundingAccountSelectionChevron.setVisibility(ImageView.VISIBLE);
		configureFundingAccountTextForSelection();
		configurePaymentDelayMessage();
	}

	private void configureFundingAccountTextForSelection() {
		Account selectedFundingAccount = fundingAccounts.get(selectedFundingAccountIndex);
		BankAccount bankAccount = selectedFundingAccount.getTransactee().getBankAccount();
		String name = bankAccount.getDisplayName();
		fundingAccountNameTextView.setText(name);
		fundingAccountSortTextView.setText(df.formatSortCode(bankAccount.getSortCode()));
		fundingAccountNumberTextView.setText(bankAccount.getAccountNumber());
		setSaveButtonState();
	}

	public void onSelectFundingAccountButtonPressed(View view) {
		ArrayList<Account> fundingAccountsArrayList = new ArrayList<Account>(fundingAccounts);
		Intent intent = new Intent(this, SelectFundingAccountActivity.class);
		intent.putExtra(DESTINATION_PRODUCT_EXTRA, destinationProduct);
		intent.putExtra(FUNDING_ACCOUNTS_EXTRA, fundingAccountsArrayList);
		intent.putExtra(SELECTED_FUNDING_ACCOUNT_INDEX_EXTRA, selectedFundingAccountIndex);
		startActivityForResult(intent, ActivityRequestCode.REQUEST_SAVINGS_FUNDING_SOURCE);
		getTooltipManager().dismiss(TooltipId.SavingsWheelFirstUse.ordinal());
		getTooltipManager().dismiss(TooltipId.SavingsWheelChangeAccount.ordinal());
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ActivityRequestCode.REQUEST_SAVINGS_FUNDING_SOURCE) {
			onFundingSourceActivityResult(resultCode, data);
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	protected void onFundingSourceActivityResult(int resultCode, Intent data) {
		if (resultCode == ActivityResultCode.RESULT_SAVINGS_FUNDING_SELECTED) {
			selectedFundingAccountIndex = data.getIntExtra(SELECTED_FUNDING_ACCOUNT_INDEX_EXTRA, selectedFundingAccountIndex);
			configureFundingAccountTextForSelection();
		}
	}

	private void configureAmountSelector() {
		amountSelector = new SavingAmountSelector(this);
		amountSelector.setSavingAmountSelectionListener(new SavingAmountSelectionListener() {

			@Override
			public void amountSelected(BigDecimal selectedAmount) {
				setSaveButtonState();
				getTooltipManager().dismiss(TooltipId.SavingsWheelFirstUse.ordinal());
			}

			@Override
			public void otherAmountActivated() {
				footer.setVisibility(View.GONE);
			}

			@Override
			public void otherAmountDismissed() {
				footer.setVisibility(View.VISIBLE);
			}
		});
	}

	private void configureSaveButton() {
		ImageView saveImage = (ImageView) findViewById(R.id.saving_save_image);
		saveImage.setTag(R.id.saving_save_image);
		savingsImageButtonHelper = new ImageButtonDrawableSwapper(this, saveImage, getResources().getDrawable(R.drawable.savingswheel_savedisabledbtn), getResources().getDrawable(R.drawable.savingswheel_saveenabledbtn));
		saveImage.setOnTouchListener(savingsImageButtonHelper);
		setSaveButtonState();
	}

	private void setSaveButtonState() {

		if (savingsImageButtonHelper == null) {
			return;
		}

		if (canSave()) {
			savingsImageButtonHelper.setEnabledImageDrawable();
		} else {
			savingsImageButtonHelper.setDisabledImageDrawable();
		}
	}

	private boolean canSave() {
		BigDecimal selectedAmount = (amountSelector == null) ? null : amountSelector.getSelectedAmount();
		int fundingAccountsCount = fundingAccounts.size();
		return (selectedAmount != null) && (selectedAmount.compareTo(BigDecimal.ZERO) > 0) && (fundingAccountsCount > 0) && (selectedFundingAccountIndex >= 0) && (selectedFundingAccountIndex < fundingAccountsCount);
	}

	private void onSaveButtonPressed() {
		Saving saving = new Saving(fundingAccounts.get(selectedFundingAccountIndex), destinationProduct);
		saving.setISAWarningPeriod(getApplicationState().getSettings().getDaysBeforeWarningAboutPaymentsIntoIsas());
		saving.setDate(Calendar.getInstance().getTime());
		submitSaving(saving);
		getTooltipManager().dismiss(TooltipId.SavingsWheelFirstUse.ordinal());
		getTooltipManager().hide();
	}

	private void submitSaving(Saving saving) {
		saving.setAmount(amountSelector.getSelectedAmount());
		saving.setDate(new Date());

		getApplicationState().getAnalyticsTracker().trackMakeSaving(saving);
		savingMaker.makeSaving(saving);
		hideKeyboard();
	}

	private void configurePaymentDelayMessage() {
		footerText.setText(R.string.savings_payment_processing_time_message);
	}

	private void configureNoLinkedAccountsMessage() {
		footerText.setText(R.string.savings_no_funding_accounts_message);
	}

	private void displayNoFundingAccountsPassiveMessage() {
		if (fundingAccounts.isEmpty() && !noFundingAccountsMessageDisplayed) {

			noFundingAccountsMessageDisplayed = true;

			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					try {
						handleError(getErrorBuilder().buildErrorResponse(NoFundingAccounts));
					} catch (Exception e) {
						// Ignore
					}
				}

			}, NO_ACCOUNTS_MESSAGE_DELAY);
		}
	}

	@Override
	public void onTouchActionUp(int tag) {
		if ((tag == R.id.saving_save_image) && canSave()) {
			onSaveButtonPressed();
		}
	}

	@Override
	public void onTouchActionDown(int tag) {
		// Nothing as yet
	}
}
