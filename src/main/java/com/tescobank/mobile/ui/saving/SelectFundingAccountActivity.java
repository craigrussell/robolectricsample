package com.tescobank.mobile.ui.saving;

import static com.tescobank.mobile.ui.saving.SavingActivity.DESTINATION_PRODUCT_EXTRA;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.payment.RetrieveFundingAccountsResponse.Account;
import com.tescobank.mobile.api.payment.RetrieveFundingAccountsResponse.Account.Transactee.BankAccount;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.ActivityResultCode;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.prodheader.ProductHeaderViewFactory;

public class SelectFundingAccountActivity extends TescoActivity {

	private DataFormatter df;
	
	private ProductDetails destinationProduct;
	private List<Account> fundingAccounts;
	private int selectedFundingAccountIndex;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		setContentView(R.layout.activity_select_funding_account);
		destinationProduct = (ProductDetails) getIntent().getExtras().getSerializable(DESTINATION_PRODUCT_EXTRA);
		fundingAccounts = SelectFundingAccountActivity.accountsFromIntent(getIntent());
		selectedFundingAccountIndex = getIntent().getIntExtra(SavingActivity.SELECTED_FUNDING_ACCOUNT_INDEX_EXTRA,0);
		
		df = new DataFormatter();
		configureHeader();
		configureList();
	}
	
	private void configureHeader() {
		ViewGroup headerContainer = (ViewGroup) findViewById(R.id.product_header);
		ProductHeaderViewFactory productHeaderViewFactory = new ProductHeaderViewFactory(destinationProduct, this, headerContainer);
		productHeaderViewFactory.createHeaderView();
	}
	
	private void configureList() {
		ListView listView = (ListView) findViewById(R.id.list);
		Account[] fundingAccountsArray = fundingAccounts.toArray(new Account[fundingAccounts.size()]);

		View footer = getLayoutInflater().inflate(R.layout.list_item_message_footer, null, false);
		TextView footerText = (TextView) footer.findViewById(R.id.informationText);
		footerText.setText(R.string.select_funding_account_manage_accounts);
		listView.addFooterView(footer,null,false);

		listView.setAdapter(new ArrayAdapter<Account>(this, 0, fundingAccountsArray) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				return getFundingAccountView(position, convertView);
			}
		});
		
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				onItemSelected(view,position);
			}
		});
	}
	
	private View getFundingAccountView(int position, View convertView) {
	
		View view = (convertView != null) ? convertView : getLayoutInflater().inflate(R.layout.list_item_funding_account, null);

		Account fundingAccount = fundingAccounts.get(position);
		BankAccount bankAccount = fundingAccount.getTransactee().getBankAccount() ;
		String name = bankAccount.getDisplayName();
		String number = df.formatSortCode(bankAccount.getSortCode()) + " " + bankAccount.getAccountNumber();		
		
		TextView nameView = (TextView) view.findViewById(R.id.product_name);
		TextView numberView = (TextView) view.findViewById(R.id.product_number);
		nameView.setText(name);
		numberView.setText(number);

		return view;
	}
	
	private void onItemSelected(View selectedView,int position) {
		selectedFundingAccountIndex = position;
		Intent data = new Intent();
		data.putExtra(SavingActivity.SELECTED_FUNDING_ACCOUNT_INDEX_EXTRA,selectedFundingAccountIndex);
		finishWithResult(ActivityResultCode.RESULT_SAVINGS_FUNDING_SELECTED,data);
	}

	@SuppressWarnings("unchecked")
	public static List<Account> accountsFromIntent(Intent intent) {
		return (ArrayList<Account>) intent.getSerializableExtra(SavingActivity.FUNDING_ACCOUNTS_EXTRA);
	}
}