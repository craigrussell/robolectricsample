package com.tescobank.mobile.ui.saving.amount.wheel;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import android.view.View;
import android.widget.ImageView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.wheel.WheelIndexSelectionListener;
import com.tescobank.mobile.ui.wheel.WheelIndexSelector;

/**
 * Wheel-based amount selector
 */
public class WheelAmountSelector extends WheelIndexSelector {

	/** Available amounts */
	private static final List<BigDecimal> AVAILABLE_AMOUNTS = Arrays.asList(new BigDecimal[] { BigDecimal.valueOf(50), BigDecimal.valueOf(100), BigDecimal.valueOf(250), BigDecimal.valueOf(500), BigDecimal.valueOf(1500), null, BigDecimal.valueOf(10), BigDecimal.valueOf(20) });

	/** The wheel amount selection listener */
	private WheelAmountSelectionListener wheelAmountSelectionListener;

	/** The content description generator */
	private WheelAmountSelectorContentDescriptionGenerator contentDescriptionGenerator;

	/** The wheel view */
	private View wheelView;

	/**
	 * Constructor
	 * 
	 * @param activity
	 *            the activity
	 */
	public WheelAmountSelector(TescoActivity activity) {
		super(AVAILABLE_AMOUNTS.size(), (ImageView) activity.findViewById(R.id.saving_wheel_image), activity.getResources().getDisplayMetrics().density);

		contentDescriptionGenerator = new WheelAmountSelectorContentDescriptionGenerator(activity, AVAILABLE_AMOUNTS);
		wheelView = activity.findViewById(R.id.saving_wheel_image);

		attachWheelIndexSelectionListener();
		updateContentDescription();
	}

	/**
	 * @param wheelAmountSelectionListener
	 *            the wheel amount selection listener
	 */
	public void setWheelAmountSelectionListener(WheelAmountSelectionListener wheelAmountSelectionListener) {
		this.wheelAmountSelectionListener = wheelAmountSelectionListener;
	}

	/**
	 * @return the selected amount
	 */
	public BigDecimal getSelectedAmount() {
		return AVAILABLE_AMOUNTS.get(getSelectedIndex());
	}

	private void attachWheelIndexSelectionListener() {
		setWheelIndexSelectionListener(new WheelIndexSelectionListener() {

			@Override
			public void indexSelected(int selectedIndex) {
				updateContentDescription();
				notifyWheelAmountSelectionListener();
			}
		});
	}

	private void notifyWheelAmountSelectionListener() {
		if (wheelAmountSelectionListener == null) {
			return;
		}

		BigDecimal selectedAmount = getSelectedAmount();
		if (selectedAmount == null) {
			wheelAmountSelectionListener.otherAmountSelected();
		} else {
			wheelAmountSelectionListener.amountSelected(selectedAmount);
		}
	}

	private void updateContentDescription() {
		wheelView.setContentDescription(contentDescriptionGenerator.generateContentDescription(getSelectedIndex()));
	}

	public boolean isOtherSelected() {
		return getSelectedIndex() == AVAILABLE_AMOUNTS.indexOf(null);
	}

}
