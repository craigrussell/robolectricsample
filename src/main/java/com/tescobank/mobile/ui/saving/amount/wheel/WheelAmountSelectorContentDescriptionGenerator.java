package com.tescobank.mobile.ui.saving.amount.wheel;

import java.math.BigDecimal;
import java.util.List;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;

import android.content.Context;

/**
 * Generates content descriptions for a wheel-based amount selector
 */
public class WheelAmountSelectorContentDescriptionGenerator {
	
	private Context context;
	
	private List<BigDecimal> amounts;
	
	private DataFormatter dataFormatter;
	
	/**
	 * Constructor
	 * 
	 * @param context the context
	 * @param amounts the amounts
	 */
	public WheelAmountSelectorContentDescriptionGenerator(Context context, List<BigDecimal> amounts) {
		super();
		
		this.context = context;
		this.amounts = amounts;
		this.dataFormatter = new DataFormatter();
	}
	
	/**
	 * Generates a content description for the specified selected index
	 * 
	 * @param selectedIndex the selected index
	 * 
	 * @return the content description
	 */
	public String generateContentDescription(int selectedIndex) {
		return context.getResources().getString(R.string.savings_wheel_description, getSelectedAmount(selectedIndex), getClockwiseAmount(selectedIndex), getAnticlockwiseAmount(selectedIndex));
	}
	
	private String getSelectedAmount(int selectedIndex) {
		return getFormattedAmount(amounts.get(selectedIndex));
	}
	
	private String getClockwiseAmount(int selectedIndex) {
		return getFormattedAmount(amounts.get((selectedIndex == 0) ? (amounts.size() - 1) : (selectedIndex - 1)));
	}
	
	private String getAnticlockwiseAmount(int selectedIndex) {
		return getFormattedAmount(amounts.get((selectedIndex == (amounts.size() - 1)) ? 0 : (selectedIndex + 1)));
	}
	
	private String getFormattedAmount(BigDecimal amount) {
		return (amount == null) ? context.getResources().getString(R.string.savings_other_amount_description) : dataFormatter.formatCurrency(amount);
	}
}
