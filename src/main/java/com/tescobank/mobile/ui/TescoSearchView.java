package com.tescobank.mobile.ui;

import android.content.Context;
import android.util.AttributeSet;

import com.actionbarsherlock.widget.SearchView;

public class TescoSearchView extends SearchView {

	public TescoSearchView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TescoSearchView(Context context) {
		super(context);
	}
	
}
