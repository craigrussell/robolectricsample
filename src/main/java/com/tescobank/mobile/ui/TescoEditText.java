package com.tescobank.mobile.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import android.widget.EditText;

import com.tescobank.mobile.BuildConfig;

public class TescoEditText extends EditText implements TypefaceDuckType {

	public static final String TAG = TescoEditText.class.getSimpleName();

	private OnDeleteListener deleteListener;

	private TextPaint textPaint = new TextPaint();
	private float fontSize;
	private TagDrawable leftSuffix;

	private String suffix = "";

	private Rect editTextBounds = new Rect();
	private int editTextBaseLineBounds;;

	public TescoEditText(Context context) {
		super(context);
	}

	public TescoEditText(Context context, AttributeSet attrs) {
		super(context, attrs);

		leftSuffix = new TagDrawable();
		
		fontSize = getTextSize();

		textPaint.setColor(getCurrentHintTextColor());
		textPaint.setTextSize(fontSize);
		textPaint.setTextAlign(Paint.Align.LEFT);

		setCompoundDrawables(leftSuffix, null, null, null);
		setTypeface(context, attrs);
	}

	public TescoEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setTypeface(context, attrs);
	}

	public final void setTypeface(Context context, AttributeSet attrs) {
		if (!isInEditMode()) {
			TypefaceAdapter.setTypeface(this, context, attrs);
		}
	}

	@Override
	public void setTypeface(Typeface typeface) {
		super.setTypeface(typeface);
		if (textPaint != null) {
			textPaint.setTypeface(typeface);
		}

		postInvalidate();
	}

	public void setOnDeleteListener(OnDeleteListener deleteListener) {
		this.deleteListener = deleteListener;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (KeyEvent.KEYCODE_DEL == keyCode) {
			if (fireOnDeleteEvent()) {
				return true;
			}
		}

		return super.onKeyUp(keyCode, event);
	}

	@Override
	public void onEditorAction(int actionCode) {
		if (getContext() instanceof TescoActivity) {
			TescoActivity activity = (TescoActivity) getContext();
			if (activity.isUserInteractionBlocked()) {
				return;
			}
		}
		super.onEditorAction(actionCode);
	}

	@Override
	public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onCreateInputConnection: " + outAttrs);
		}
		InputConnection inputConnection = super.onCreateInputConnection(outAttrs);
		return (inputConnection == null) ? null : new DeleteKeyInputConnection(inputConnection, true);
	}

	private boolean fireOnDeleteEvent() {
		if (null != deleteListener) {
			deleteListener.onDeletePressed(TescoEditText.this);
			return true;
		}

		return false;
	}

	public void setPrefix(String s) {
		leftSuffix.setText(s);
		setCompoundDrawables(leftSuffix, null, null, null);
	}

	public void setSuffix(String s) {
		suffix = s;
		setCompoundDrawables(leftSuffix, null, null, null);
	}

	public void setTextSuffixColour(int colourId) {
		textPaint.setColor(colourId);
	}

	@Override
	public void onDraw(Canvas c) {
		editTextBaseLineBounds = getLineBounds(0, editTextBounds);

		super.onDraw(c);

		int xSuffix = (int) textPaint.measureText(leftSuffix.text + getText().toString()) + getPaddingLeft();
		c.drawText(suffix, xSuffix, editTextBounds.bottom, textPaint);

	}

	public class TagDrawable extends Drawable {

		public String text = "";

		public void setText(String s) {
			text = s;
			setBounds(0, 0, getIntrinsicWidth(), getIntrinsicHeight());
			invalidateSelf();
		}

		@Override
		public void draw(Canvas canvas) {
			
			int fieldWidth = getWidth();
			int textWidth;
			
			if(getText().toString().isEmpty() && getHint() != null) {
				textWidth = (int)textPaint.measureText(getHint().toString());
			} else {
				textWidth = (int)textPaint.measureText(getText().toString());
			}
			
			int xoffset = (fieldWidth / 2) - (textWidth / 2) - getIntrinsicWidth() - getPaddingLeft();
			canvas.drawText(text, xoffset, editTextBaseLineBounds + canvas.getClipBounds().top, textPaint);
		}

		@Override
		public void setAlpha(int i) {
		}

		@Override
		public void setColorFilter(ColorFilter colorFilter) {
		}

		@Override
		public int getOpacity() {
			return 1;
		}

		@Override
		public int getIntrinsicHeight() {
			return (int) fontSize;
		}

		@Override
		public int getIntrinsicWidth() {
			return (int) textPaint.measureText(text);
		}
	}

	private class DeleteKeyInputConnection extends InputConnectionWrapper {

		public DeleteKeyInputConnection(InputConnection target, boolean mutable) {
			super(target, mutable);
		}

		@Override
		public boolean sendKeyEvent(KeyEvent event) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "sendKeyEvent: " + event);
			}

			// This is required for 2.3.3
			if (event.getAction() == KeyEvent.ACTION_UP && event.getKeyCode() == KeyEvent.KEYCODE_DEL) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "delete key pressed!");
				}
				fireOnDeleteEvent();
			}

			return super.sendKeyEvent(event);
		}

		@Override
		public boolean deleteSurroundingText(int beforeLength, int afterLength) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "deleteSurroundingText: " + beforeLength + ", " + afterLength);
			}
			fireOnDeleteEvent();
			return super.deleteSurroundingText(beforeLength, afterLength);
		}

	}

	public interface OnDeleteListener {
		void onDeletePressed(TescoEditText editText);
	}

}