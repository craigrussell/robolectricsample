package com.tescobank.mobile.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class TescoButton extends Button implements TypefaceDuckType {

	public TescoButton(Context context){
		super(context);
	}
	
	public TescoButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		setTypeface(context, attrs);
	}

	public TescoButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setTypeface(context, attrs);
	}

	public final void setTypeface(Context context, AttributeSet attrs) {
		if (!isInEditMode()) {
			TypefaceAdapter.setTypeface(this, context, attrs);
		}
	}
}