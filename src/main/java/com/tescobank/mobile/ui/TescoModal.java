package com.tescobank.mobile.ui;

import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;

public class TescoModal extends Dialog {

	private static final String TAG = TescoModal.class.getSimpleName();
	protected String headingText;
	protected String messageText;
	protected View additionalView;
	
	public TescoModal(Context context) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.transparent_background));
		setContentView(R.layout.tesco_modal);
	}

	@Override
	public void show() {
		setCanceledOnTouchOutside(true);
		configureHeader();
		configureMessage();
		super.show();
	}
	
	public void setHeadingText(String headingText) {
		this.headingText = headingText;
	}
	
	public void setMessageText(String message) {
		this.messageText = message;
	}
	
	private void configureHeader() {
		if(BuildConfig.DEBUG) {
			Log.d(TAG, "Configuring header");
		}
		TextView heading = (TextView) findViewById(R.id.heading);
		heading.setText(headingText);
		configureClosure();
	}
	
	private void configureMessage() {
		if(BuildConfig.DEBUG) {
			Log.d(TAG, "Configuring message");
		}
		TextView message = (TextView) findViewById(R.id.message);
		message.setText(Html.fromHtml(messageText));
	}
	
	private void configureClosure() {
		findViewById(R.id.close_image).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}
	
}
