package com.tescobank.mobile.ui.errors;

import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_BACK_FROM_RETRY_PRESSED;
import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_BACK_ON_PASSWORD_CHANGED_ONLINE;
import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_RETRY_PRESSED;
import static com.tescobank.mobile.ui.auth.AuthFlowFactory.createPasswordChangedOnlineFlow;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapper.ErrorResponse;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.auth.PinRequestingFlowInitiator;

/**
 * Activity for displaying an error screen
 */
public class ErrorActivity extends TescoActivity {

	public static final String BACK_FROM_RETRY_ERROR_SHOULD_RETRY_EXTRA = "backOnRetryErrorShouldRetry";
	
	private ErrorResponse error;
	
	private boolean backFromRetryErrorShouldRetry;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_error);
		
		error = (ErrorResponse) getIntent().getSerializableExtra(ErrorResponse.class.getName());
		backFromRetryErrorShouldRetry = getIntent().getBooleanExtra(BACK_FROM_RETRY_ERROR_SHOULD_RETRY_EXTRA, true);
		
		configureActionBar();
		configureTitle();
		configureMessage();
		configureCustomerService();
		configureButton();
	}
	
	private void configureTitle() {
		TextView textTitle = (TextView) findViewById(R.id.errorMessageTitle);
		textTitle.setText(error.getDisplay().getTitle());
	}
	
	private void configureMessage() {
		TextView textMessage = (TextView) findViewById(R.id.errorMessageText);
		textMessage.setText(error.getDisplay().getMessage());
	}
	
	private void configureCustomerService() {
		if ((error.getDisplay().getCustomerService() != null) && !isBlank(error.getDisplay().getCustomerService().getMessage())) {
			configureCustomerServiceMessage();
			configureCustomerServiceContact();
		} else {
			findViewById(R.id.errorCustomerServicesLayout).setVisibility(View.INVISIBLE);
		}
	}
	
	private void configureCustomerServiceMessage() {
		TextView textCustomerServiceMessage = (TextView) findViewById(R.id.errorCustomerServicesText);
		Spanned errorText = Html.fromHtml(error.getDisplay().getCustomerService().getMessage().replaceAll("(\r\n|\n)", "<br />"));
		textCustomerServiceMessage.setText(errorText);
	}
	
	private void configureCustomerServiceContact() {
		ImageView imageCustomerServices = (ImageView) findViewById(R.id.errorCustomerServiceImage);
		if (!isBlank(error.getDisplay().getCustomerService().getPhoneNumber())) {
			TextView textCustomerServicePhoneNumber = (TextView) findViewById(R.id.errorCustomerServicesPhoneNumber);
			textCustomerServicePhoneNumber.setText(error.getDisplay().getCustomerService().getPhoneNumber());
			imageCustomerServices.setImageResource(R.drawable.error_phone);
		} else if (!isBlank(error.getDisplay().getCustomerService().getUrl())) {
			imageCustomerServices.setImageResource(R.drawable.error_url);
		} else {
			findViewById(R.id.errorCustomerServicesLayout).setVisibility(View.INVISIBLE);
		}
	}
	
	private void configureButton() {
		Button errorButton = (Button) findViewById(R.id.errorButton);
		if (error.isErrorPasswordChangedOnline()) {
			errorButton.setText(R.string.error_button_continue);
		} else if (error.isErrorTypeRetry()) {
			errorButton.setText(R.string.error_button_retry);
		} else {
			errorButton.setText(R.string.error_button_fatal);
		}
	}
	
	@Override
	public void onBackPressed() {
		if(error.isErrorPasswordChangedOnline()) {
			finishWithResult(RESULT_BACK_ON_PASSWORD_CHANGED_ONLINE);
		} else if (error.isErrorNoInternetConnectionAtLogin() || !backFromRetryErrorShouldRetry) {
			finishWithResult(RESULT_BACK_FROM_RETRY_PRESSED);
		} else{
			onButtonPressed(null);
		}
	}
	
	private void configureActionBar() {
		ActionBar actionBar = getSupportActionBar();
		ActionBarConfiguration configuration = ActionBarConfiguration.class.cast(getIntent().getSerializableExtra(ActionBarConfiguration.class.getName()));
		if(actionBar != null && configuration != null) {
			configuration.apply(this, actionBar);
		}
	}

	@Override
	protected void onStart() {
		trackAnalytic();
		super.onStart();
	}
		
	private void trackAnalytic() {
		if(error.isErrorServerResponseStatus()) {
			getApplicationState().getAnalyticsTracker().trackScreenErrorCommunicatingWithNetwork(error);
			return;
		} 

		if(error.isErrorMalwareDetected()) {
			getApplicationState().getAnalyticsTracker().trackScreenErrorMalwareFound(error);
			return;
		}
		
		getApplicationState().getAnalyticsTracker().trackScreenError(error);
	}
	
	@Override
	protected void checkNetworkStatus() {
		// Do nothing
	}
	
	public void onButtonPressed(View view) {
		if (error.isErrorPasswordChangedOnline()) {
			handlePasswordChangedOnline();
			return;
		} else if (error.isErrorTypeRetry()) {
			handleRetry();
			return;
		} else {
			handleFatal();
			return;
		}
	}
	
	@SuppressLint("NewApi")
	private void handlePasswordChangedOnline() {
		PinRequestingFlowInitiator passwordChangedOnlineHelper = new PinRequestingFlowInitiator(this, createPasswordChangedOnlineFlow());
		passwordChangedOnlineHelper.execute();
		return;
	}
	
	private void handleRetry() {
		finishWithResult(RESULT_RETRY_PRESSED);
	}
	
	private void handleFatal() {
		ActivityCompat.finishAffinity(this);
	}

	public void onContactPressed(View view) {

		if (!isBlank(error.getDisplay().getCustomerService().getPhoneNumber())) {
			performPhoneCall(error.getDisplay().getCustomerService().getPhoneNumber());
		} else if (!isBlank(error.getDisplay().getCustomerService().getUrl())) {
			performOpenWebPage(error.getDisplay().getCustomerService().getUrl());
		}
	}

	private void performOpenWebPage(String url) {
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	private void performPhoneCall(String phoneNumber) {
		Intent intent = new Intent(Intent.ACTION_DIAL);
		intent.setData(Uri.parse("tel:" + phoneNumber));
		startActivity(intent);
	}

	private boolean isBlank(String s) {
		return null == s || s.trim().isEmpty();
	}

}
