package com.tescobank.mobile.ui.errors;

import com.tescobank.mobile.api.error.ErrorResponseWrapper;

/**
 * An error handler
 */
public interface ErrorHandler {
	
	/**
	 * Handles the specified error
	 * 
	 * @param errorResponseWrapper the error
	 */
	void handleError(ErrorResponseWrapper errorResponseWrapper);
}
