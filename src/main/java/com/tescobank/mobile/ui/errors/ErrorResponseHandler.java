package com.tescobank.mobile.ui.errors;

import com.tescobank.mobile.api.error.ErrorResponseWrapper;

public interface ErrorResponseHandler {

	/**
	 * @param wrapper
	 *            the error
	 * @return true if the activity should retry NOW, false otherwise
	 */
	boolean handleErrorResponse(ErrorResponseWrapper wrapper);

	void hidePassiveError();

}
