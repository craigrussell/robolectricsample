package com.tescobank.mobile.ui.errors;

import static com.tescobank.mobile.ui.ActionBarConfiguration.getStyle;
import static com.tescobank.mobile.ui.ActivityRequestCode.REQUEST_FATAL_ERROR;
import static com.tescobank.mobile.ui.ActivityRequestCode.REQUEST_RETRY_ERROR;
import static com.tescobank.mobile.ui.errors.ErrorActivity.BACK_FROM_RETRY_ERROR_SHOULD_RETRY_EXTRA;
import android.content.Context;
import android.content.Intent;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.Display;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapper.ErrorResponse;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.ActionBarConfiguration.Type;
import com.tescobank.mobile.ui.TescoActivity;

public class DefaultErrorResponseHandler implements ErrorResponseHandler {

	private final TescoActivity activity;
	private final PassiveErrorHelper passiveError;

	public DefaultErrorResponseHandler(TescoActivity tescoActivity) {
		this.activity = tescoActivity;
		passiveError = new PassiveErrorHelper(tescoActivity);
	}

	@Override
	public boolean handleErrorResponse(ErrorResponseWrapper wrapper) {
		
		ErrorResponse error = wrapper.getErrorResponse();
		
		new ErrorPopulator(activity, error.getDisplay()).ensurePopulated();
		
		if (error.isErrorTypePassive()) {
			passiveError.show(error);
			return retryNow(error);
		}
		
		ActionBarConfiguration configuration = new ActionBarConfiguration(getStyle(activity.getSupportActionBar()),Type.HOME);
		Intent errorIntent = new Intent(activity, ErrorActivity.class);
		errorIntent.putExtra(ErrorResponse.class.getName(), error);
		errorIntent.putExtra(ActionBarConfiguration.class.getName(),configuration);

		if (error.isErrorTypeRetry()) {
			errorIntent.putExtra(BACK_FROM_RETRY_ERROR_SHOULD_RETRY_EXTRA, activity.backFromRetryErrorShouldRetry());
			activity.startActivityForResult(errorIntent, REQUEST_RETRY_ERROR);
		} else {
			activity.startActivityForResult(errorIntent, REQUEST_FATAL_ERROR);
		}

		return false;
	}

	@Override
	public void hidePassiveError() {
		passiveError.hide();
	}
	
	private boolean retryNow(ErrorResponse error) {
		return !error.getIsInformationMessage() && !error.isErrorInternetConnectionLost();
	}
	
	private static class ErrorPopulator {
		
		private Context context;
		private Display display;
		
		public ErrorPopulator(Context context, Display display) {
			super();
			
			this.context = context;
			this.display = display;
		}
		
		public void ensurePopulated() {
			if (requiresPopulation()) {
				populateWithDefaults();
			}
		}
		
		private boolean requiresPopulation() {
			return isTitleEmpty() && isMessageEmpty();
		}
		
		private boolean isTitleEmpty() {
			return isEmpty(display.getTitle());
		}
		
		private boolean isMessageEmpty() {
			return isEmpty(display.getMessage());
		}
		
		private boolean isEmpty(String string) {
			return (string == null) || string.trim().isEmpty();
		}
		
		private void populateWithDefaults() {
			populateWithDefaultTitle();
			populateWithDefaultMessage();
		}
		
		private void populateWithDefaultTitle() {
			display.setTitle(context.getResources().getString(R.string.error_display_title_default));
		}
		
		private void populateWithDefaultMessage() {
			display.setMessage(context.getResources().getString(R.string.error_display_message_default));
		}
	}
}
