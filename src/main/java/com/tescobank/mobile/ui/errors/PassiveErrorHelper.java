package com.tescobank.mobile.ui.errors;

import com.tescobank.mobile.api.error.ErrorResponseWrapper.ErrorResponse;
import com.tescobank.mobile.ui.TescoActivity;

public class PassiveErrorHelper {

	private PassiveError currentError;
	private final TescoActivity activity;

	public PassiveErrorHelper(TescoActivity activity) {
		this.activity = activity;
	}

	public void show(ErrorResponse error) {
		
		if(currentError != null) {
			currentError.hide();
		}
		currentError = new PassiveError(activity, error);
		currentError.show();
	}
	
	public void hide() {
		if(currentError != null) {
			currentError.hide();
		}
		currentError = null;
	}
}
