package com.tescobank.mobile.ui.errors;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.Display;
import com.tescobank.mobile.api.error.ErrorResponseWrapper.ErrorResponse;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoPopupWindow;

public class PassiveError {

	private static final int PASSIVE_ERROR_TIMEOUT = 5 * 1000;
	private static final int ERROR_LAYOUT_ID = R.layout.passive_error;
	private static final int MESSAGE_LAYOUT_ID = R.layout.passive_message;
	
	private final TescoActivity activity;
	private final ErrorResponse error;
	private TescoPopupWindow errorWindow;
	private View errorContentView;
	private ScheduledFuture<?> hideSlowlyFuture;
	
	public PassiveError(TescoActivity activity, ErrorResponse error) {
		super();
		
		this.activity = activity;
		this.error = error;
		this.errorContentView = activity.getLayoutInflater().inflate(error.getIsInformationMessage() ? MESSAGE_LAYOUT_ID : ERROR_LAYOUT_ID, null);
		this.errorWindow = new TescoPopupWindow(activity, errorContentView);
		
		this.errorWindow.setWindowLayoutMode(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	public void show() {
		trackAnalytic();

		TextView text = (TextView) errorContentView.findViewById(R.id.passive_error_text);
		text.setText(getText(error.getDisplay()));
		errorWindow.setAnimationStyle(R.style.fadeInOutAnimation);
		errorWindow.setClippingEnabled(false);
		errorWindow.showAtLocation(Gravity.TOP, 0, 0);
		
		ImageView cancelButton = (ImageView) errorContentView.findViewById(R.id.cancel_button);
		cancelButton.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				hide();
				return true;
			}
		});
		hideSlowly();
	}
	
	private String getText(Display display) {
		return isEmpty(display.getTitle()) ? display.getMessage() : display.getTitle();
	}
	
	private boolean isEmpty(String string) {
		return (string == null) || string.trim().isEmpty();
	}

	public void hide() {
		if (hideSlowlyFuture != null) {
			hideSlowlyFuture.cancel(true);
		}
		
		if (errorWindow.isShowing()) {
			errorWindow.dismiss();
		}
	}

	private void trackAnalytic() {
		if (!error.getIsInformationMessage()) {
			activity.getApplicationState().getAnalyticsTracker().trackPassiveError(error);
		}
	}

	private void hideSlowly() {
		hideSlowlyFuture = Executors.newSingleThreadScheduledExecutor().schedule(new Runnable() {
			@Override
			public void run() {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						hide();
					}
				});
			}
		}, PASSIVE_ERROR_TIMEOUT, TimeUnit.MILLISECONDS);
	}
}
