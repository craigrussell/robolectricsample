package com.tescobank.mobile.ui;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.view.Surface;

public class DetailedScreenOrientation {
	
	private int simpleOrientation;
	private int rotation;
	
	/**
	 * @param simpleOrientation, either PORTRAIT or LANDSCAPE as specified in android.content.res.Configuration
	 * @param rotation, as specified in android.view.Surface
	 */
	public DetailedScreenOrientation(int simpleOrientation, int rotation) {
		this.simpleOrientation = simpleOrientation;
		this.rotation = rotation;
	}
	
	/**
	 * @return the current orientation defined by one of the following values (see ActivityInfo):
	 *   SCREEN_ORIENTATION_PORTRAIT
	 *   SCREEN_ORIENTATION_REVERSE_PORTRAIT
	 *   SCREEN_ORIENTATION_LANDSCAPE
	 *   SCREEN_ORIENTATION_REVERSE_LANDSCAPE
	 */
	public int getFullOrientation() {
		if(simpleOrientation == Configuration.ORIENTATION_LANDSCAPE) {
			return landscapeOrientationForRotation();
		}
		return portraitOrientationForRotation();
	}
	
	private int landscapeOrientationForRotation() {
		switch (rotation) {
		case Surface.ROTATION_0:
		case Surface.ROTATION_90:
			return ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
		case Surface.ROTATION_180:
		case Surface.ROTATION_270:
			return ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
		default:
			return ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
		}
	}
	
	private int portraitOrientationForRotation() {
		switch (rotation) {
		case Surface.ROTATION_0:
		case Surface.ROTATION_90:
			return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
		case Surface.ROTATION_180:
		case Surface.ROTATION_270:
			return ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
		default:
			return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
		}
	}
}
