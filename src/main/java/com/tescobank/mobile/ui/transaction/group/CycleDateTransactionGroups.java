package com.tescobank.mobile.ui.transaction.group;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;

/**
 * Collection of transaction groups with the potential for a group per cycle date (depending on what transactions have been added)
 */
public class CycleDateTransactionGroups extends TransactionGroups {
	
	private List<Date> cycleDates;
	private List<TransactionGroup> potentialTransactionGroups;
	
	public CycleDateTransactionGroups(Context context, List<String> cycleDates) {
		super(context);
		
		buildCycleDates((cycleDates == null) ? new ArrayList<String>() : cycleDates);
		buildPotentialTransactionGroups();
		addInitialTransactionGroup();
	}
	
	private void buildCycleDates(List<String> cycleDateStrings) {
		
		cycleDates = new ArrayList<Date>();
		
		for (String cycleDateString : cycleDateStrings) {
			cycleDates.add(getDataFormatter().parseDate(cycleDateString));
		}
	}
	
	private void buildPotentialTransactionGroups() {
		
		potentialTransactionGroups = new ArrayList<TransactionGroup>();
		
		addInitialPotentialTransactionGroup();
		addPotentialTransactionGroupsForCycleDates();
		addFinalPotentialTransactionGroup();
	}
	
	private void addInitialPotentialTransactionGroup() {
		
		TransactionGroup transactionGroup = new TransactionGroup();
		transactionGroup.setName(getContext().getResources().getString(R.string.transaction_group_header_initial_cc));
		
		if (!cycleDates.isEmpty()) {
			Date cycleDate = cycleDates.get(0);
			transactionGroup.setEarliestDateInclusive(cycleDate);
		}
		
		potentialTransactionGroups.add(transactionGroup);
	}
	
	private void addPotentialTransactionGroupsForCycleDates() {
		
		for (int i = 0; i < cycleDates.size() - 1; i++) {
			
			Date cycleDate = cycleDates.get(i);
			
			TransactionGroup transactionGroup = new TransactionGroup();
			transactionGroup.setEarliestDateInclusive(cycleDates.get(i + 1));
			transactionGroup.setLatestDateExclusive(cycleDate);
			transactionGroup.setName(getTransactionGroupName(transactionGroup));
			potentialTransactionGroups.add(transactionGroup);
		}
	}
	
	private void addFinalPotentialTransactionGroup() {
		
		if (!cycleDates.isEmpty()) {
			
			Date cycleDate = cycleDates.get(cycleDates.size() - 1);
			
			TransactionGroup transactionGroup = new TransactionGroup();
			transactionGroup.setLatestDateExclusive(cycleDate);
			transactionGroup.setName(getTransactionGroupName(transactionGroup));
			potentialTransactionGroups.add(transactionGroup);
		}
	}
	
	private String getTransactionGroupName(TransactionGroup group) {
		return getDataFormatter().formatDateAsDayFullMonthAndYear(group.getLatestDateExclusive());
	}
	
	private void addInitialTransactionGroup() {
		addTransactionGroup(potentialTransactionGroups.get(0));
		potentialTransactionGroups.removeAll(getTransactionGroups());
	}
	
	@Override
	protected void addNewGroupsAndTransaction(Transaction transaction) {
		
		for (TransactionGroup newTransactionGroup : potentialTransactionGroups) {
			
			addTransactionGroup(newTransactionGroup);
			
			if (newTransactionGroup.matchesDate(transaction.getOccurredDate())) {
				newTransactionGroup.addTransaction(transaction);
				break;
			}
		}
		
		potentialTransactionGroups.removeAll(getTransactionGroups());
	}
}
