package com.tescobank.mobile.ui.transaction;

import static android.content.Intent.FLAG_ACTIVITY_REORDER_TO_FRONT;
import static android.widget.ExpandableListView.getPackedPositionForChild;
import static com.tescobank.mobile.api.product.RetrievePagedTransactionsRequest.FIRST_PAGE;
import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_BACK_FROM_RETRY_PRESSED;
import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_CAROUSEL_LOADED;
import static com.tescobank.mobile.ui.statement.ViewStatementListActivity.LAUNCHED_FROM_STATEMENT_ARCHIVE_LIST;
import static com.tescobank.mobile.ui.transaction.TransactionRetriever.TRANSACTION_RETRIEVAL_EXTRA;
import static com.tescobank.mobile.ui.transactioncalendar.CalendarActivity.LAUNCHED_FROM_TRANSACTION_CALENDAR;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.configuration.FeaturesResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.SearchViewHelperFactory;
import com.tescobank.mobile.ui.SearchViewHelperFactory.SearchViewHelper;
import com.tescobank.mobile.ui.SearchViewHelperFactory.SubmitQueryCallback;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.calendar.CalendarMenuDayDrawer;
import com.tescobank.mobile.ui.carousel.CarouselActivity;
import com.tescobank.mobile.ui.prodheader.ProductHeaderViewFactory;
import com.tescobank.mobile.ui.statement.ViewStatementListActivity;
import com.tescobank.mobile.ui.transaction.TransactionRetriever.TransactionRetrivedResult;
import com.tescobank.mobile.ui.transaction.TransactionsListAdapter.SortOrder;
import com.tescobank.mobile.ui.transaction.group.ExpansionStateRestorer;
import com.tescobank.mobile.ui.transactioncalendar.CalendarActivity;
import com.tescobank.mobile.ui.tutorial.TooltipId;
import com.tescobank.mobile.ui.tutorial.groups.TransactionsListTooltipGroup;

/**
 * Activity to render transactions
 */
public class TransactionsActivity extends TescoActivity implements DataChunkRetriever, SubmitQueryCallback {

	private static final String TAG = TransactionsActivity.class.getSimpleName();

	public static final String TRANSACTIONS_PRODUCT_EXTRA = "product";
	public static final String STATEMENT_PRODUCT_EXTRA = "statementProduct";
	public static final String TRANSACTION_ACTIVITY = "Transactions";

	private static final int HUNDRED_PERCENT = 100;
	private static final ScheduledExecutorService SCHEDULER = Executors.newSingleThreadScheduledExecutor();

	private ProductDetails product;
	private boolean autoRetrievalEnabled = true;
	private boolean moreTransactionsAvailable = true;
	private ExpandableListView transactionsListView;
	private TransactionsListAdapter transactionsListAdapter;
	private ExpansionStateRestorer expansionStateRestorer;
	private InProgressIndicator inProgressIndicator;
	private TextView sortOrderTextView;
	private TextView transactionsMore;
	private SearchViewHelper searchViewHelper;
	private View progressView;

	private boolean scrolledToTheEnd = false;
	private OverscrollView overScrollView;
	private double maxDistance;
	private MenuItem transCalItem;
	private MenuItem statementItem;
	private ScheduledFuture<?> scheduledAnalytics;
	private TransactionDetail transactionDetail;
	private TransactionRetriever transactionRetriever;
	private BroadcastReceiver transactionsBroadcastReceiver;
	private boolean launchedFromTransactionCalendar;
	private boolean firstVisitComplete;
	private FeaturesResponse features;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating Transactions Activity");
		}
		super.onCreate(savedInstanceState);

		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		setContentView(R.layout.activity_transactions);

		features = getApplicationState().getFeatures();
		product = (ProductDetails) getIntent().getExtras().getSerializable(TRANSACTIONS_PRODUCT_EXTRA);
		inProgressIndicator = new InProgressIndicator(this, false);
		progressView = findViewById(R.id.progressbutton);
		sortOrderTextView = (TextView) findViewById(R.id.transaction_sort_order_header);
		overScrollView = (OverscrollView) findViewById(R.id.overscrollview);
		transactionsMore = (TextView) findViewById(R.id.transactions_more);
		maxDistance = getResources().getDisplayMetrics().density * HUNDRED_PERCENT;
		transactionRetriever = (TransactionRetriever) getIntent().getExtras().getSerializable(TRANSACTION_RETRIEVAL_EXTRA);
		launchedFromTransactionCalendar = getIntent().getBooleanExtra(LAUNCHED_FROM_TRANSACTION_CALENDAR, false);
		configureHeaderView();
		configureListView();
		configureDisablementOfDeviceMenuKey();
		createTransactionReceiver();
		applyInitialSort();
		getTooltipManager().setGroup(new TransactionsListTooltipGroup(getApplicationContext(), false, hasCalendar(), areStatementsEnabled()));
	}

	private void createTransactionReceiver() {
		if (!isTransactionRetrieverCreated()) {
			transactionRetriever = new TransactionRetriever(product);
		}
	}

	private boolean isTransactionRetrieverCreated() {
		return transactionRetriever == null ? false : true;
	}

	private boolean hasTransactionsToShow() {
		return !FIRST_PAGE.equals(transactionRetriever.getNextPageRef());
	}

	private void configureDisablementOfDeviceMenuKey() {
		try {
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
			if (menuKeyField != null) {
				menuKeyField.setAccessible(true);
				menuKeyField.setBoolean(config, false);
			}
		} catch (Exception ex) {
			// Ignore
		}
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		getTooltipManager().hide();
		getTooltipManager().show();		
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		launchedFromTransactionCalendar = intent.getBooleanExtra(LAUNCHED_FROM_TRANSACTION_CALENDAR, false);
		if (launchedFromTransactionCalendar) {
			transactionRetriever = (TransactionRetriever) intent.getExtras().getSerializable(TRANSACTION_RETRIEVAL_EXTRA);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		registerTransactionsBroadcastRecevier();

		if (launchedFromTransactionCalendar) {
			rebuildTransactionsListWhenLaunchedFromTransactionCalendar();
		} else if (!firstVisitComplete) {
			firstVisitComplete = true;
			retrieveNextPageOfTransactions();
		}

		getTooltipManager().show();
	}

	private void rebuildTransactionsListWhenLaunchedFromTransactionCalendar() {
		transactionsListAdapter.clear();
		transactionsListAdapter.notifyDataSetChanged();
		transactionsListAdapter.addTransactions(transactionRetriever.getTransactions());
		expansionStateRestorer.restoreState();

		if (!transactionsListAdapter.isEmpty()) {
			getTooltipManager().setGroup(new TransactionsListTooltipGroup(getApplicationContext(), true, hasCalendar(), areStatementsEnabled()));
		}
	}

	@Override
	protected void onPause() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Pausing");
		}

		unregisterTransactionsBroadcastRecevier();

		if (null != transactionDetail) {
			transactionDetail.dismiss();
			transactionDetail = null;
		}
		super.onPause();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.menu_item_transaction_sort:
			getTooltipManager().dismiss(TooltipId.TransactionSort.ordinal());
			break;

		case R.id.menu_item_transaction_sort_alphabetically:
			applySort(TransactionsListAdapter.SortOrder.AtoZ, true);
			getTooltipManager().dismiss(TooltipId.TransactionSort.ordinal());
			break;

		case R.id.menu_item_transaction_sort_amount:
			applySort(TransactionsListAdapter.SortOrder.Amount, true);
			getTooltipManager().dismiss(TooltipId.TransactionSort.ordinal());
			break;

		case R.id.menu_item_transaction_sort_date:
			applySort(TransactionsListAdapter.SortOrder.Date, true);
			getTooltipManager().dismiss(TooltipId.TransactionSort.ordinal());
			break;

		case R.id.menu_item_transaction_sort_inout:
			applySort(TransactionsListAdapter.SortOrder.InOut, true);
			getTooltipManager().dismiss(TooltipId.TransactionSort.ordinal());
			break;

		case R.id.menu_item_transaction_statement:
			initialiseStatementActivity();
			getTooltipManager().dismiss(TooltipId.TransactionStatementToggle.ordinal());
			break;

		case R.id.menu_item_transaction_calendar:
			initialiseTransactionCalendarActivity();
			getTooltipManager().dismiss(TooltipId.TransactionCalendarToggle.ordinal());
			break;

		default:
			Intent carousel = new Intent(this, CarouselActivity.class);
			carousel.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(carousel);
			finishWithResult(RESULT_CAROUSEL_LOADED);
			break;
		}

		return false;
	}

	private void initialiseStatementActivity() {
		Intent statementListIntent = new Intent(this, ViewStatementListActivity.class);
		statementListIntent.setFlags(FLAG_ACTIVITY_REORDER_TO_FRONT);
		statementListIntent.putExtra(STATEMENT_PRODUCT_EXTRA, product);
		statementListIntent.putExtra(LAUNCHED_FROM_STATEMENT_ARCHIVE_LIST, TRANSACTION_ACTIVITY);
		startActivity(statementListIntent);
	}

	private void initialiseTransactionCalendarActivity() {
		Intent calendarIntent = new Intent(this, CalendarActivity.class);
		calendarIntent.setFlags(FLAG_ACTIVITY_REORDER_TO_FRONT);
		calendarIntent.putExtra(TRANSACTIONS_PRODUCT_EXTRA, product);
		calendarIntent.putExtra(TransactionRetriever.TRANSACTION_RETRIEVAL_EXTRA, transactionRetriever);
		startActivity(calendarIntent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onCreateOptionsMenu");
		}

		getSupportMenuInflater().inflate(R.menu.transactions, menu);

		searchViewHelper = SearchViewHelperFactory.create((SearchView) menu.findItem(R.id.menu_item_search).getActionView());
		searchViewHelper.setSubmitQueryCallback(this);

		transCalItem = menu.findItem(R.id.menu_item_transaction_calendar);
		statementItem = menu.findItem(R.id.menu_item_transaction_statement);
		if (transCalItem != null && statementItem != null) {
			initialiseProductSpecificActionBarIcons();
			drawTodaysDayOnCalendarMenuIcon();
		}
		return true;
	}

	private void initialiseProductSpecificActionBarIcons() {
		statementItem.setVisible(areStatementsEnabled());
		statementItem.setEnabled(areStatementsEnabled());
		transCalItem.setVisible(hasCalendar());
		transCalItem.setEnabled(hasCalendar());
	}
	
	private void drawTodaysDayOnCalendarMenuIcon() {
		int textSize = getResources().getInteger(R.integer.actionbar_calendar_day_text_size);
		int yOffset = getResources().getInteger(R.integer.actionbar_calendar_day_y_offset);
		CalendarMenuDayDrawer drawer = new CalendarMenuDayDrawer(textSize, yOffset);
		drawer.drawTodaysDay(getResources(), transCalItem);
	}
	
	private boolean hasCalendar() {
		return product.isPersonalCurrentAccount();
	}
	
	private boolean areStatementsEnabled() {
		return (product.isPersonalCurrentAccount() && features.isViewCurrentAccountStatement()) ||
			   (product.isCreditCard() && features.isViewStatement());
	}

	@Override
	public void nextChunk() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Next Chunk");
		}

		retrieveNextPageOfTransactions();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_BACK_FROM_RETRY_PRESSED) {
			if (hasTransactionsToShow()) {
				return;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onRetryPressed() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Retry");
		}

		retrieveNextPageOfTransactions();
	}

	@Override
	public void onBackPressed() {
		if (searchViewHelper.handleBackPressed()) {
			return;
		}
		super.onBackPressed();
	}

	@Override
	public boolean backFromRetryErrorShouldRetry() {
		return false;
	}

	@Override
	public void textSubmitted(String search) {
		// Do nothing
	}

	@Override
	public void textChanged(final String text) {

		getTooltipManager().dismiss(TooltipId.TransactionSearch.ordinal());

		autoRetrievalEnabled = (text == null) || text.isEmpty();
		transactionsListAdapter.filterBy(text);
		expansionStateRestorer.expandAll();
		expansionStateRestorer.selectFirst();

		if (null != scheduledAnalytics) {
			scheduledAnalytics.cancel(false);
			scheduledAnalytics = null;
		}

		if (!autoRetrievalEnabled) {
			scheduledAnalytics = SCHEDULER.schedule(new Runnable() {
				@Override
				public void run() {
					getApplicationState().getAnalyticsTracker().trackViewTransactionsListSearch(product, text);
				}
			}, 1, TimeUnit.SECONDS);
		}

	}

	private void applyInitialSort() {
		SortOrder initial = getApplicationState().getTransactionSortOrderForProduct(product);
		if (null == initial) {
			initial = SortOrder.Date;
		}
		applySort(initial, false);
	}

	private void applySort(SortOrder sort, boolean track) {

		if (track) {
			getApplicationState().getAnalyticsTracker().trackViewTransactionsListSorting(sort.name(), product);
		}

		String sortTypeStr = getString(sort.getDisplayResource());
		sortOrderTextView.setText(getString(R.string.transaction_sort_by_header, sortTypeStr));
		getApplicationState().setTransactionSortOrderForProduct(sort, product);
		transactionsListAdapter.sortBy(sort);
		expansionStateRestorer.expandAll();
		expansionStateRestorer.selectFirst();
	}

	private void configureHeaderView() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Configuring Header View");
		}

		ViewGroup headerContainer = (ViewGroup) findViewById(R.id.transaction_header);
		ProductHeaderViewFactory headerViewFactory = new ProductHeaderViewFactory(product, this, headerContainer);
		headerViewFactory.createHeaderView();
	}

	private void configureListView() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Configuring List View");
		}

		transactionsListView = (ExpandableListView) findViewById(R.id.transactions_list);
		transactionsListAdapter = new TransactionsListAdapter(this, product.getCycleDates(), product.isCreditCard());
		expansionStateRestorer = new ExpansionStateRestorer(transactionsListView, transactionsListAdapter);
		transactionsListView.setAdapter(transactionsListAdapter);

		attachScrollListener();
		attachChildClickListener();
	}

	private void attachScrollListener() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Attaching Over-Scroll Listener");
		}

		transactionsListView.setOnScrollListener(new TransactionsOnScrollListener());
		transactionsListView.setOnTouchListener(new TransactionsOnTouchListener());
	}

	private void attachChildClickListener() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Attaching Child Click Listener");
		}

		transactionsListView.setOnChildClickListener(new TransactionsOnChildClickListener());
	}

	private void retrieveNextPageOfTransactions() {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Retrieve Transactions Request for Page = " + transactionRetriever.getNextPageRef());
		}

		if (!autoRetrievalEnabled) {
			return;
		}

		if (!transactionRetriever.areMoreTransactionsAvailable()) {
			onNoMoreTransactionsRetrieved();
			return;
		}

		inProgressIndicator.showInProgressIndicator();
		progressView.setVisibility(View.VISIBLE);

		trackAnalytic();

		transactionRetriever.retrieveMoreTransactions(this);

	}

	private void registerTransactionsBroadcastRecevier() {
		transactionsBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {

				handleTransactionsReceived(intent);
			}

		};
		LocalBroadcastManager.getInstance(this).registerReceiver(transactionsBroadcastReceiver, new IntentFilter(TransactionRetriever.TRANSACTION_RETRIEVAL_BROADCAST_EVENT));
	}

	private void unregisterTransactionsBroadcastRecevier() {
		if (transactionsBroadcastReceiver != null) {
			LocalBroadcastManager.getInstance(this).unregisterReceiver(transactionsBroadcastReceiver);
		}
	}

	private void trackAnalytic() {
		getApplicationState().getAnalyticsTracker().trackViewTransactions(product, transactionRetriever.getPagesRetrievedCount() + 1);
	}

	private void onNoMoreTransactionsRetrieved() {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling No More Transactions Retrieved");
		}

		if (moreTransactionsAvailable) {
			inProgressIndicator.hideInProgressIndicator();
			progressView.setVisibility(View.GONE);
			unregisterTransactionsBroadcastRecevier();
			moreTransactionsAvailable = false;
			transactionsListAdapter.setMoreTransactionsAvailable(false);
			handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.NoMoreTransactions));
		}

	}

	private void onTransactionsRetrieved(List<Transaction> transactionsRetrieved) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Transactions Retrieved");
		}
		inProgressIndicator.hideInProgressIndicator();
		progressView.setVisibility(View.GONE);
		expansionStateRestorer.captureState();
		transactionsListAdapter.addTransactions(transactionsRetrieved);
		expansionStateRestorer.restoreState();
	}

	private void handleTransactionsReceived(Intent intent) {
		TransactionRetrivedResult status = (TransactionRetrivedResult) intent.getSerializableExtra(TransactionRetriever.TRANSACTION_RETRIEVAL_STATUS_EXTRA);
		status = (status == null ? TransactionRetrivedResult.FAIL : status);

		switch (status) {
		case SUCCESS:
			@SuppressWarnings("unchecked")
			List<Transaction> transactions = (ArrayList<Transaction>) intent.getSerializableExtra(TransactionRetriever.TRANSACTION_RETRIEVAL_NEW_EXTRA);
			onTransactionsRetrieved(transactions);
			getTooltipManager().setGroup(new TransactionsListTooltipGroup(getApplicationContext(), true, hasCalendar(), areStatementsEnabled()));
			getTooltipManager().show();
			break;
		case NO_MORE:
			onNoMoreTransactionsRetrieved();
			break;
		case FAIL:
			ErrorResponseWrapper error = (ErrorResponseWrapper) intent.getSerializableExtra(ErrorResponseWrapper.class.getName());
			if (error != null) {
				handleErrorResponse(error);
			}

			inProgressIndicator.hideInProgressIndicator();
			progressView.setVisibility(View.GONE);
			break;
		}
	}

	private class DialogManager implements OnCancelListener, OnDismissListener {

		@Override
		public void onCancel(DialogInterface dialog) {
			execute();
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			execute();
		}

		private void execute() {
			transactionDetail = null;
		}
	}

	private class TransactionsOnScrollListener implements OnScrollListener {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

			if ((firstVisibleItem + visibleItemCount >= totalItemCount) && (transactionDetail == null)) {
				scrolledToTheEnd = true;
			} else {
				scrolledToTheEnd = false;
			}

		}
	}

	private class TransactionsOnTouchListener implements OnTouchListener {

		private Integer y;

		@Override
		public boolean onTouch(View v, MotionEvent event) {

			Log.d(TAG, "onTouch: " + event);

			if (event.getAction() == MotionEvent.ACTION_MOVE) {
				handleActionMove(event);
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				handleActionUp();
			}

			return false;
		}

		private void handleActionMove(MotionEvent event) {

			if (null == y) {
				y = (int) event.getY();
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "setting y " + y);
				}
				return;
			}

			int distance = (int) (y - event.getY());
			distance = (int) Math.min(distance, maxDistance);

			if (BuildConfig.DEBUG) {
				Log.d(TAG, "setting " + scrolledToTheEnd + " distance: " + distance);
			}

			if (scrolledToTheEnd) {
				handleScrollToTheEnd(distance);
			}
		}

		private void handleScrollToTheEnd(int distance) {

			if (distance >= maxDistance && moreTransactionsAvailable) {
				retrieveMoreTransactions();
			} else {
				renderMoreTransactionsMessage(distance);
			}

			overScrollView.requestLayout();
			overScrollView.invalidate();
		}

		private void retrieveMoreTransactions() {
			scrolledToTheEnd = false;
			progressView.setVisibility(View.VISIBLE);
			retrieveNextPageOfTransactions();
			overScrollView.setHeight(0);
		}

		private void renderMoreTransactionsMessage(int distance) {
			overScrollView.setHeight(distance > 0 ? distance : 0);

			if (moreTransactionsAvailable) {
				transactionsMore.setText(R.string.transaction_more);
			} else {
				transactionsMore.setText(R.string.transaction_message_no_more_transactions);
			}
		}

		private void handleActionUp() {
			y = null;
			overScrollView.setHeight(0);
			overScrollView.requestLayout();
			overScrollView.invalidate();
		}
	}

	private class TransactionsOnChildClickListener implements OnChildClickListener {

		@Override
		public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
			getApplicationState().getAnalyticsTracker().trackViewTransactionDetails(product);
			getTooltipManager().dismiss(TooltipId.TransactionDetail.ordinal());

			DialogManager dialogManager = new DialogManager();

			transactionDetail = new TransactionDetail(parent, transactionsListAdapter, groupPosition, childPosition, TransactionsActivity.this, product, TransactionsActivity.this);
			transactionDetail.setOnCancelListener(dialogManager);
			transactionDetail.setOnDismissListener(dialogManager);
			transactionDetail.show();

			parent.setSelectionFromTop(childPosition, 0);
			parent.setItemChecked(parent.getFlatListPosition(getPackedPositionForChild(groupPosition, childPosition)), true);
			return false;
		}
	}
}
