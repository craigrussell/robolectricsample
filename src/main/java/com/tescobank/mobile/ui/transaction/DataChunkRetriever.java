package com.tescobank.mobile.ui.transaction;

public interface DataChunkRetriever {

	void nextChunk();
	
}
