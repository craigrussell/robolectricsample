package com.tescobank.mobile.ui.transaction.group;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;
import com.tescobank.mobile.format.DataFormatter;

/**
 * Collection of transaction groups
 */
public abstract class TransactionGroups {
	
	private Context context;
	private DataFormatter dataFormatter = new DataFormatter();
	private List<TransactionGroup> transactionGroups;
	
	public TransactionGroups(Context context) {
		super();
		
		this.context = context;
		this.dataFormatter = new DataFormatter();
		this.transactionGroups = new ArrayList<TransactionGroup>();
	}
	
	protected Context getContext() {
		return context;
	}
	
	protected DataFormatter getDataFormatter() {
		return dataFormatter;
	}
	
	protected List<TransactionGroup> getTransactionGroups() {
		return transactionGroups;
	}
	
	protected void addTransactionGroup(TransactionGroup transactionGroup) {
		transactionGroups.add(transactionGroup);
	}
	
	public void addTransactions(List<Transaction> transactions) {
		for (Transaction transaction : transactions) {
			addTransaction(transaction);
		}
	}
	
	private void addTransaction(Transaction transaction) {
		
		TransactionGroup lastGroup = getLastTransactionGroup();
		
		if ((lastGroup != null) && lastGroup.matchesDate(transaction.getOccurredDate())) {
			lastGroup.addTransaction(transaction);
		} else {
			addNewGroupsAndTransaction(transaction);
		}
	}
	
	private TransactionGroup getLastTransactionGroup() {
		return transactionGroups.isEmpty() ? null : transactionGroups.get(transactionGroups.size() - 1);
	}
	
	protected abstract void addNewGroupsAndTransaction(Transaction transaction);
	
	public int size() {
		return transactionGroups.size();
	}
	
	public TransactionGroup get(int location) {
		return transactionGroups.get(location);
	}
}
