package com.tescobank.mobile.ui.transaction;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.tescobank.mobile.ui.transaction.TransactionsListAdapter.SortOrder.Date;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.sort.transaction.TransactionAmountComparator;
import com.tescobank.mobile.sort.transaction.TransactionAtoZComparator;
import com.tescobank.mobile.sort.transaction.TransactionInOutComparator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoTextView;
import com.tescobank.mobile.ui.transaction.group.CalendarMonthTransactionGroups;
import com.tescobank.mobile.ui.transaction.group.CycleDateTransactionGroups;
import com.tescobank.mobile.ui.transaction.group.SingleGroupTransactionGroups;
import com.tescobank.mobile.ui.transaction.group.TransactionGroup;
import com.tescobank.mobile.ui.transaction.group.TransactionGroups;

/**
 * List adapter for transactions
 */
public class TransactionsListAdapter extends BaseExpandableListAdapter {

	private static final String TAG = TransactionsListAdapter.class.getSimpleName();
	
	private static final int MAX_CC = 4;

	/** Supported sort orders. */
	public static enum SortOrder {
		InOut("IN_OUT", R.string.transactions_sort_in_out), Date("DATE", R.string.transactions_sort_date), AtoZ("A_TO_Z", R.string.transactions_sort_a_z), Amount("AMOUNT",
				R.string.transactions_sort_amount);

		/**
		 * The valueOf(string) approach throws an exception if it can't be
		 * found. This will also, gracefully, survive an app upgrade where the
		 * enum constants change.
		 */
		private final String persistenceValue;

		private final int res;

		private SortOrder(String persistenceValue, int res) {
			this.persistenceValue = persistenceValue;
			this.res = res;
		}

		public String persistenceValue() {
			return persistenceValue;
		}

		public static SortOrder fromPersistenceValue(String value) {
			for (SortOrder sortOrder : values()) {
				if (sortOrder.persistenceValue.equals(value)) {
					return sortOrder;
				}
			}
			return null;
		}

		public int getDisplayResource() {
			return res;
		}

	}

	private static final SortOrder DEFAULT_SORT_ORDER = Date;

	private static final String EMPTY_STRING = "";

	private TescoActivity activity;
	
	private List<String> cycleDates;

	private boolean isCreditCard;

	private SortOrder sortOrder;

	private String filterText;

	private List<Transaction> originalTransactions;
	
	private TransactionGroups transactionGroups;

	private boolean moreTransactionsAvailable = true;

	private LayoutInflater inflater;

	private DataFormatter dataFormatter;

	/**
	 * Constructor
	 * 
	 * @param activity
	 *            the activity
	 * @param cycleDates
	 *            the cycle dates
	 * @param isCreditCard
	 *            whether the transactions are for a credit card
	 */
	public TransactionsListAdapter(TescoActivity activity, List<String> cycleDates, boolean isCreditCard) {
		super();
		
		this.activity = activity;
		this.cycleDates = cycleDates;
		this.isCreditCard = isCreditCard;
		this.sortOrder = DEFAULT_SORT_ORDER;
		this.originalTransactions = new ArrayList<Transaction>();
		this.transactionGroups = new SingleGroupTransactionGroups();
		this.inflater = (LayoutInflater) activity.getSystemService(LAYOUT_INFLATER_SERVICE);
		this.dataFormatter = new DataFormatter();

		sortBy(sortOrder);
	}

	/**
	 * Adds the specified transactions
	 * 
	 * @param transactionsToAdd
	 *            the transactions to add
	 */
	public void addTransactions(List<Transaction> transactionsToAdd) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Adding " + transactionsToAdd.size() + " transactions");
		}

		populateAdditionalProperties(transactionsToAdd);
		originalTransactions.addAll(transactionsToAdd);
		
		sortBy(sortOrder);
	}
	
	public void clear() {
		originalTransactions.clear();
	}

	/**
	 * Populates additional properties for the specified transactions
	 * 
	 * @param transactionsToAdd
	 *            the transactions
	 */
	private void populateAdditionalProperties(List<Transaction> transactionsToAdd) {

		int existingAccount = originalTransactions.size();

		for (int i = 0; i < transactionsToAdd.size(); i++) {

			Transaction transaction = transactionsToAdd.get(i);
			transaction.setOccurredIndex(existingAccount + i);
			transaction.setOccurredDate(dataFormatter.parseDate(transaction.getOccurred()));
			transaction.setAmountString((transaction.getAmount() == null) ? EMPTY_STRING : transaction.getAmount().toPlainString());
		}
	}

	/**
	 * Sorts transactions using the specified sort order
	 * 
	 * @param sortOrder
	 *            the sort order
	 */
	public final void sortBy(SortOrder sortOrder) {
		this.sortOrder = sortOrder;

		switch (sortOrder) {
		case InOut:
			sortByInOut();
			break;

		case Date:
			sortByDate();
			break;

		case AtoZ:
			sortByAtoZ();
			break;

		case Amount:
			sortByAmount();
			break;
		}

		notifyDataSetChanged();
	}

	private void sortByInOut() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Sorting by in/out");
		}

		sort(new TransactionInOutComparator());
	}

	private void sortByDate() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Sorting by date");
		}

		if (filterText == null) {

			List<Transaction> filteredTransactions = getFilteredTransaction();
			transactionGroups = isCreditCard ? new CycleDateTransactionGroups(activity, cycleDates) : new CalendarMonthTransactionGroups(activity, new Date());
			addTransactionsToGroups(filteredTransactions);

		} else {

			sort(null);
		}
	}

	private void sortByAtoZ() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Sorting by a-to-z");
		}

		sort(new TransactionAtoZComparator());
	}

	private void sortByAmount() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Sorting by amount");
		}

		sort(new TransactionAmountComparator());
	}

	private void sort(Comparator<Transaction> comparator) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Sorting");
		}

		List<Transaction> filteredTransactions = getFilteredTransaction();

		if (comparator != null) {
			Collections.sort(filteredTransactions, comparator);
		}
		
		transactionGroups = new SingleGroupTransactionGroups();
		
		addTransactionsToGroups(filteredTransactions);
	}
	
	private void addTransactionsToGroups(List<Transaction> filteredTransactions) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Adding transactions to groups");
		}
		
		transactionGroups.addTransactions(filteredTransactions);
	}

	private List<Transaction> getFilteredTransaction() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Getting filtered transactions (filter text = '" + filterText + "')");
		}

		if (filterText == null) {

			return new ArrayList<Transaction>(originalTransactions);

		} else {

			List<Transaction> filteredTransactions = new ArrayList<Transaction>();

			for (Transaction transaction : originalTransactions) {
				if (transaction.matchesFilter(filterText)) {
					filteredTransactions.add(transaction);
				}
			}

			return filteredTransactions;
		}
	}

	/**
	 * Filters transactions using the specified filter text
	 * 
	 * @param filterText
	 *            the filter text
	 */
	public void filterBy(String filterText) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Filtering by '" + filterText + "'");
		}

		this.filterText = (filterText == null || filterText.isEmpty()) ? null : filterText;

		sortBy(sortOrder);
	}

	/**
	 * @param moreTransactionsAvailable
	 *            whether more transactions may be available
	 */
	public void setMoreTransactionsAvailable(boolean moreTransactionsAvailable) {
		this.moreTransactionsAvailable = moreTransactionsAvailable;
		notifyDataSetChanged();
	}

	/**
	 * @param transactionPosition
	 *            specify the position of the current transaction; updated to
	 *            the position of the previous transaction
	 * @param updatePosition
	 *            whether the position will be updated
	 * @return the previous transaction, or null if there is none
	 */
	public Transaction getPreviousTransaction(TransactionPosition transactionPosition, boolean updatePosition) {
		
		int grpPosition = transactionPosition.getGroupPosition();
		int chdPosition = transactionPosition.getChildPosition();
		
		int originalGrpPosition = grpPosition;
		int originalChdPosition = chdPosition;

		if (canMoveBackInGroup(transactionPosition)) {
			chdPosition--;
		} else if (canMoveToPreviousGroup(grpPosition)) {
			grpPosition--;
			chdPosition = getChildrenCount(grpPosition) - 1;
		} else {
			return null;
		}

		transactionPosition.setGroupPosition(grpPosition);
		transactionPosition.setChildPosition(chdPosition);
		
		Transaction transaction = getPreviousTransaction(transactionPosition, updatePosition, grpPosition, chdPosition);
		restoreTransactionPosition(transactionPosition, updatePosition, transaction, originalGrpPosition, originalChdPosition);
		
		return transaction;
	}
	
	private boolean canMoveBackInGroup(TransactionPosition transactionPosition) {
		return transactionPosition.getChildPosition() > 0;
	}
	
	private boolean canMoveToPreviousGroup(int grpPosition) {
		return (grpPosition > 0) && (getChildrenCount(grpPosition - 1) > 0);
	}
	
	private Transaction getPreviousTransaction(TransactionPosition transactionPosition, boolean updatePosition, int grpPosition, int chdPosition) {
		Transaction transaction = (Transaction)getChild(grpPosition, chdPosition);
		if (transaction == null) {
			transaction = getPreviousTransaction(transactionPosition, updatePosition);
		}
		return transaction;
	}
	
	private void restoreTransactionPosition(TransactionPosition transactionPosition, boolean updatePosition, Transaction transaction, int originalGrpPosition, int originalChdPosition) {
		if ((transaction == null) || !updatePosition) {
			transactionPosition.setGroupPosition(originalGrpPosition);
			transactionPosition.setChildPosition(originalChdPosition);
		}
	}

	/**
	 * @param transactionPosition
	 *            specify the position of the current transaction; updated to
	 *            the position of the next transaction
	 * @param updatePosition
	 *            whether the position will be updated
	 * @return the next transaction, or null if there is none
	 */
	public Transaction getNextTransaction(TransactionPosition transactionPosition, boolean updatePosition) {

		int grpPosition = transactionPosition.getGroupPosition();
		int chdPosition = transactionPosition.getChildPosition();
		
		int originalGrpPosition = grpPosition;
		int originalChdPosition = chdPosition;

		if (canMoveForwardInGroup(grpPosition, chdPosition)) {
			chdPosition++;
		} else if (canMoveToNextGroup(grpPosition)) {
			grpPosition++;
			chdPosition = 0;
		} else {
			return null;
		}

		transactionPosition.setGroupPosition(grpPosition);
		transactionPosition.setChildPosition(chdPosition);
		
		Transaction transaction = getNextTransaction(transactionPosition, updatePosition, grpPosition, chdPosition);
		restoreTransactionPosition(transactionPosition, updatePosition, transaction, originalGrpPosition, originalChdPosition);
		
		return transaction;
	}
	
	private boolean canMoveForwardInGroup(int grpPosition, int chdPosition) {
		return chdPosition < (getChildrenCount(grpPosition) - 1);
	}
	
	private boolean canMoveToNextGroup(int grpPosition) {
		return (grpPosition < (getGroupCount() - 1)) && (getChildrenCount(grpPosition + 1) > 0);
	}
	
	private Transaction getNextTransaction(TransactionPosition transactionPosition, boolean updatePosition, int grpPosition, int chdPosition) {
		Transaction transaction = (Transaction)getChild(grpPosition, chdPosition);
		if (transaction == null) {
			transaction = getNextTransaction(transactionPosition, updatePosition);
		}
		return transaction;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return transactionGroups.get(groupPosition).isEmpty() ? null : transactionGroups.get(groupPosition).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

		View theView = convertView;
		Transaction transaction = (Transaction) getChild(groupPosition, childPosition);
		if (transaction == null) {
			theView = getChildViewForNoTransaction(groupPosition, theView);
		} else {
			theView = getChildViewForTransaction(theView, transaction);
		}

		return theView;
	}
	
	private View getChildViewForNoTransaction(int groupPosition, View view) {
		
		View theView = getChildViewForNoTransaction(view);

		int messageId;
		if (!moreTransactionsAvailable) {
			messageId = R.string.transaction_message_no_more_transactions;
		} else if (filterText != null) {
			messageId = R.string.transaction_message_load_more_then_filter;
		} else if (!isCreditCard) {
			messageId = R.string.transaction_message_load_more_banking;
		} else if (groupPosition < MAX_CC) {
			messageId = R.string.transaction_message_load_more_cc;
		} else {
			messageId = R.string.transaction_message_view_statement;
		}

		TescoTextView messageView = (TescoTextView) theView.findViewById(R.id.transaction_message);
		messageView.setText(activity.getResources().getString(messageId));
		
		return theView;
	}
	
	private View getChildViewForNoTransaction(View view) {
		if ((view == null) || (view.findViewById(R.id.transaction_message) == null)) {
			return inflater.inflate(R.layout.activity_transactions_message_row, null);
		} else {
			return view;
		}
	}
	
	private View getChildViewForTransaction(View view, Transaction transaction) {
		
		View theView = getChildViewForTransaction(view);

		ImageView inOutView = (ImageView) theView.findViewById(R.id.transaction_in_out_image);
		inOutView.setImageDrawable(activity.getResources().getDrawable(transaction.isCredit() ? R.drawable.ic_in_white_large : R.drawable.ic_out_white_large));
		inOutView.setContentDescription(activity.getResources().getString(transaction.isCredit() ? R.string.product_transactions_in_description : R.string.product_transactions_out_description));

		TescoTextView descriptionView = (TescoTextView) theView.findViewById(R.id.transaction_description);
		descriptionView.setText(isCreditCard ? transaction.getMerchantName() : transaction.getCondensedDescription());

		TescoTextView dateView = (TescoTextView) theView.findViewById(R.id.transaction_date);
		dateView.setText(dataFormatter.formatDate(transaction.getOccurred()));

		TescoTextView amountView = (TescoTextView) theView.findViewById(R.id.transaction_amount);
		amountView.setText(dataFormatter.formatCurrency(transaction.getAmount()));
		
		return theView;
	}
	
	private View getChildViewForTransaction(View view) {
		if ((view == null) || (view.findViewById(R.id.transaction_in_out_image) == null)) {
			return inflater.inflate(R.layout.activity_transactions_list_row, null);
		} else {
			return view;
		}
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (originalTransactions.isEmpty() && moreTransactionsAvailable) {
			return 0;
		} else if (transactionGroups.get(groupPosition).isEmpty()) {
			return 1;
		} else {
			return transactionGroups.get(groupPosition).size();
		}
	}

	@Override
	public Object getGroup(int groupPosition) {
		return transactionGroups.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return transactionGroups.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		
		View theView = convertView;
		if (isNewGroupViewRequired(theView)) {
			theView = inflater.inflate(R.layout.activity_transactions_group_header, null);
		} else {
			theView.setVisibility(View.VISIBLE);
		}

		theView = configireGroupView(groupPosition, theView);

		return theView;
	}
	
	private boolean isNewGroupViewRequired(View theView) {
		return (theView == null) || "hidden".equals(theView.getTag());
	}
	
	private View configireGroupView(int groupPosition, View theView) {
		
		TextView description = (TextView) theView.findViewById(R.id.transaction_group_description);
		TransactionGroup group = TransactionGroup.class.cast(getGroup(groupPosition));
		description.setText(group == null ? null : group.getName());
		
		if ((filterText != null) || (sortOrder != SortOrder.Date)) {
			
			FrameLayout hidden = new FrameLayout(activity);
			hidden.setTag("hidden");
			hidden.setVisibility(View.GONE);
			return hidden;
		}
		
		return theView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return (getChild(groupPosition, childPosition) != null);
	}
	
	/**
	 * The position of a transaction within a collection of transaction groups
	 */
	public static class TransactionPosition {
		
		private int groupPosition;
		
		private int childPosition;
		
		public TransactionPosition(int groupPosition, int childPosition) {
			super();
			
			this.groupPosition = groupPosition;
			this.childPosition = childPosition;
		}

		public int getGroupPosition() {
			return groupPosition;
		}

		public void setGroupPosition(int groupPosition) {
			this.groupPosition = groupPosition;
		}

		public int getChildPosition() {
			return childPosition;
		}

		public void setChildPosition(int childPosition) {
			this.childPosition = childPosition;
		}
	}
}
