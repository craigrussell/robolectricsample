package com.tescobank.mobile.ui.transaction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.product.RetrievePagedTransactionsRequest;
import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse;
import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;
import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.ui.TescoActivity;

public class TransactionRetriever implements Serializable {

	private static final long serialVersionUID = -5289540758717587200L;

	public enum TransactionRetrivedResult {
		SUCCESS, FAIL, NO_MORE
	}
	
	public static final String TRANSACTION_RETRIEVAL_NEW_EXTRA = "TRANSACTION_RETRIEVAL_NEW_EXTRA";
	public static final String TRANSACTION_RETRIEVAL_BROADCAST_EVENT = "TRANSACTION_RETRIEVAL_BROADCAST_EVENT";
	public static final String TRANSACTION_RETRIEVAL_STATUS_EXTRA = "TRANSACTION_RETRIEVAL_STATUS_EXTRA";
	public static final String TRANSACTION_RETRIEVAL_EXTRA ="TransactionReceiver";


	private static final String TAG = TransactionRetriever.class.getSimpleName();

	private List<Transaction> transactions = new ArrayList<Transaction>();
	private Product product;

	private String nextPageRef = "1";
	
	private boolean moreTransactionsAvailable = true;
	private boolean retrievalInProgress = false;
	private int pagesRetrievedCount;

	public TransactionRetriever(Product product) {
		this.product = product;
	}

	/*
	 * This will provide a copy of the transactions at a current point in time, 
	 * if the user of this method is interested in new transactions they need to listen to the broadcast.
	 */
	public List<Transaction> getTransactions() {
		return new ArrayList<Transaction>(transactions);
	}
	
	public void retrieveMoreTransactions(final TescoActivity activity) {

		if (retrievalInProgress || !moreTransactionsAvailable) {
			return;
		}
		retrievalInProgress = true;
		
		final RetrievePagedTransactionsRequest request = new TransactionsRetrievePagedTransactionsRequest(activity);
		request.setTbID(activity.getApplicationState().getTbId());
		request.setNextPageRef(nextPageRef);
		request.setProduct(product);
		activity.getApplicationState().getRestRequestProcessor().processRequest(request);
	}

	private void onNoMoreTransactionsRetrieved(Context context) {
		Intent intent = new Intent(TRANSACTION_RETRIEVAL_BROADCAST_EVENT);
		intent.putExtra(TRANSACTION_RETRIEVAL_STATUS_EXTRA, TransactionRetrivedResult.NO_MORE);
		LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
	}

	private void onTransactionsRetrieved(Context context, List<Transaction> transactionsRetrieved) {
		ArrayList<Transaction> transactionsArrayList = new ArrayList<Transaction>(transactionsRetrieved);
		
		synchronized(transactions) {	
			List<Transaction> newList = new ArrayList<Transaction>(transactions);
			newList.addAll(transactionsRetrieved);
			transactions = newList;
		}
		
		Intent intent = new Intent(TRANSACTION_RETRIEVAL_BROADCAST_EVENT);
		intent.putExtra(TRANSACTION_RETRIEVAL_STATUS_EXTRA, TransactionRetrivedResult.SUCCESS);
		intent.putExtra(TRANSACTION_RETRIEVAL_NEW_EXTRA, transactionsArrayList);
		LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
	}

	private void onTransactionError(Context context, ErrorResponseWrapper errorResponse) {
		Intent intent = new Intent(TRANSACTION_RETRIEVAL_BROADCAST_EVENT);
		intent.putExtra(TRANSACTION_RETRIEVAL_STATUS_EXTRA, TransactionRetrivedResult.FAIL);
		intent.putExtra(ErrorResponseWrapper.class.getName(), errorResponse);
		LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
	}

	public boolean areMoreTransactionsAvailable() {
		return moreTransactionsAvailable;
	}

	public int count() {
		return transactions.size();
	}


	
	private class TransactionsRetrievePagedTransactionsRequest extends RetrievePagedTransactionsRequest {
		
		private TescoActivity activity;
		
		public TransactionsRetrievePagedTransactionsRequest(TescoActivity activity) {
			super();
			
			this.activity = activity;
		}
		
		@Override
		public void onResponse(RetrievePagedTransactionsResponse response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Retrieve Transactions Request Succeeded");
			}

			retrievalInProgress = false;
			pagesRetrievedCount++;
			nextPageRef = response.getNextPageRef();
			List<Transaction> newTransactions = response.getTransactions();
			
			if (newTransactions != null && !newTransactions.isEmpty()) {
				onTransactionsRetrieved(activity, newTransactions);
			}
			
			if (null == nextPageRef || nextPageRef.trim().isEmpty()) {
				moreTransactionsAvailable = false;
				onNoMoreTransactionsRetrieved(activity);
			}
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponse) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Retrieve Transactions Request Failed: " + errorResponse.getErrorResponse().getErrorCode());
			}
			retrievalInProgress = false;
			onTransactionError(activity, errorResponse);
		}
	}

	public String getNextPageRef() {
		return nextPageRef;
	}

	public int getPagesRetrievedCount() {
		return pagesRetrievedCount;
	}
	
}
