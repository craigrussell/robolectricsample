package com.tescobank.mobile.ui.transaction;

import android.app.Dialog;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.ui.transaction.TransactionsListAdapter.TransactionPosition;

public class TransactionDetail extends Dialog {
	private static final String TAG = TransactionDetail.class.getSimpleName();
	private static final String CREDIT = "Credit";
	private ExpandableListView listView;
	private DataFormatter dataFormatter;
	private TransactionsListAdapter transactionsListAdapter;
	private int childPosition;
	private int groupPosition;
	private Product product;

	private DataSetObserver dataObserver;

	private DataChunkRetriever chunker;

	public TransactionDetail(ExpandableListView listView, TransactionsListAdapter transactionsListAdapter, int groupPosition, int childPosition, Context applicationContext, Product product, DataChunkRetriever chunker) {
		super(applicationContext);
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting TransactionDetail consructor");
		}
		this.listView = listView;
		this.transactionsListAdapter = transactionsListAdapter;
		this.groupPosition = groupPosition;
		this.childPosition = childPosition;
		this.dataFormatter = new DataFormatter();
		this.product = product;
		this.chunker = chunker;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setCanceledOnTouchOutside(true);
		getWindow().setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.transparent_background));

		setContentView(R.layout.dialog_transaction_detail);

		configureClosure();
		configureNextTransaction();
		configurePreviousTransaction();

		populateFromTransactionDetail();
	}

	private void populateFromTransactionDetail() {

		Transaction transaction = getTransaction();
		TextView transactionDescriptionAndName = (TextView) findViewById(R.id.transaction_detail_description);
		if (product.isCreditCard()) {
			transactionDescriptionAndName.setText(transaction.getMerchantName());
		} else {
			transactionDescriptionAndName.setText(transaction.getDescription());
		}

		TextView transactionMerchantLocation = (TextView) findViewById(R.id.transaction_detail_merchant_location);
		transactionMerchantLocation.setText(transaction.getMerchantLocation());

		TextView transactionDate = (TextView) findViewById(R.id.transaction_detail_date);
		transactionDate.setText(dataFormatter.formatDateToDDMM(transaction.getOccurred()));

		TextView transactionCRDR = (TextView) findViewById(R.id.transaction_detail_crdr);
		transactionCRDR.setText(transaction.getCategory());

		ImageView transactionTypeImage = (ImageView) findViewById(R.id.transaction_in_out_image);
		if (transaction.getCategory().equals(CREDIT)) {
			transactionTypeImage.setImageResource(R.drawable.ic_in_white_large);
		} else {
			transactionTypeImage.setImageResource(R.drawable.ic_out_white_large);
		}

		TextView transactionAmount = (TextView) findViewById(R.id.transaction_detail_amount);
		transactionAmount.setText(dataFormatter.formatCurrency(transaction.getAmount()));
		populatePreviousTransactionButton();
		populateNextTransactionButton();

	}

	private Transaction getTransaction() {
		return (Transaction) transactionsListAdapter.getChild(groupPosition, childPosition);

	}

	private void configureClosure() {
		findViewById(R.id.close_image).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

	/**
	 * Configures previous transaction requested
	 */
	private void configurePreviousTransaction() {
		findViewById(R.id.transaction_header).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getPreviousTransactionData(true);
				refresh();
			}
		});
	}

	/**
	 * Configures next Transaction requested
	 */
	private void configureNextTransaction() {
		findViewById(R.id.transaction_footer).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getNextTransactionData(true);
				refresh();
			}
		});
	}

	private void refresh() {
		populateFromTransactionDetail();
		
		listView.expandGroup(groupPosition);
		
		long packedPosition = ExpandableListView.getPackedPositionForChild(groupPosition, childPosition);
		listView.setItemChecked(listView.getFlatListPosition(packedPosition), true);
		listView.setSelectedChild(groupPosition, childPosition, true);
	}
	
	private Transaction getPreviousTransactionData(boolean updatePosition) {
		TransactionPosition transactionPosition = new TransactionPosition(groupPosition, childPosition);
		Transaction transaction = transactionsListAdapter.getPreviousTransaction(transactionPosition, updatePosition);
		groupPosition = transactionPosition.getGroupPosition();
		childPosition = transactionPosition.getChildPosition();
		return transaction;
	}
	
	private Transaction getNextTransactionData(boolean updatePosition) {
		TransactionPosition transactionPosition = new TransactionPosition(groupPosition, childPosition);
		Transaction transaction = transactionsListAdapter.getNextTransaction(transactionPosition, updatePosition);
		groupPosition = transactionPosition.getGroupPosition();
		childPosition = transactionPosition.getChildPosition();
		return transaction;
	}

	private void populatePreviousTransactionButton() {
		Transaction transaction = getPreviousTransactionData(false);

		TextView previousTransactionDetailLabel = (TextView) findViewById(R.id.transaction_up_detail_label);
		ImageView previousTransactionImage = (ImageView) findViewById(R.id.up_image);

		if (transaction == null) {
			previousTransactionDetailLabel.setVisibility(View.INVISIBLE);
			previousTransactionImage.setVisibility(View.INVISIBLE);
		} else {
			previousTransactionDetailLabel.setVisibility(View.VISIBLE);
			previousTransactionImage.setVisibility(View.VISIBLE);
		}
	}

	private void populateNextTransactionButton() {
		Transaction transaction = getNextTransactionData(false);

		TextView nextTransactionDetailLabel = (TextView) findViewById(R.id.transaction_down_detail_label);
		ImageView nextTransactionImage = (ImageView) findViewById(R.id.down_image);

		if (transaction == null) {
			nextTransactionDetailLabel.setVisibility(View.INVISIBLE);
			nextTransactionImage.setVisibility(View.INVISIBLE);

			if (null == dataObserver) {
				
				dataObserver = new DataSetObserver() {
					@Override
					public void onChanged() {
						if (BuildConfig.DEBUG) {
							Log.d(TAG, "DataSetObserver#onChanged");
						}
						refresh();
						transactionsListAdapter.unregisterDataSetObserver(this);
						TransactionDetail.this.dataObserver = null;
					}
				};
				
				transactionsListAdapter.registerDataSetObserver(dataObserver);
				
				chunker.nextChunk();
			}

		} else {
			nextTransactionDetailLabel.setVisibility(View.VISIBLE);
			nextTransactionImage.setVisibility(View.VISIBLE);
		}
	}
}
