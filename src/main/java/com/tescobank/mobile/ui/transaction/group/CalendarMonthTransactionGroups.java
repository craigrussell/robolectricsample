package com.tescobank.mobile.ui.transaction.group;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

import java.util.Calendar;
import java.util.Date;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;

/**
 * Collection of transaction groups with a group per calendar month (depending on what transactions have been added)
 */
public class CalendarMonthTransactionGroups extends TransactionGroups {
	
	private Calendar earliestMonthSoFar;
	
	public CalendarMonthTransactionGroups(Context context, Date today) {
		super(context);
		
		setEarliestMonthSoFar(today);
		addInitialTransactionGroup();
	}
	
	private void setEarliestMonthSoFar(Date today) {
		
		Calendar todayCalendar = Calendar.getInstance();
		todayCalendar.setTime(today);
		
		earliestMonthSoFar = Calendar.getInstance();
		earliestMonthSoFar.setTimeInMillis(0);
		earliestMonthSoFar.set(YEAR, todayCalendar.get(YEAR));
		earliestMonthSoFar.set(MONTH, todayCalendar.get(MONTH));
		earliestMonthSoFar.set(DAY_OF_MONTH, 1);
	}
	
	private void addInitialTransactionGroup() {
		
		TransactionGroup transactionGroup = new TransactionGroup();
		transactionGroup.setEarliestDateInclusive(earliestMonthSoFar.getTime());
		transactionGroup.setName(getContext().getResources().getString(R.string.transaction_group_header_initial_other));
		addTransactionGroup(transactionGroup);
	}
	
	@Override
	protected void addNewGroupsAndTransaction(Transaction transaction) {
		
		TransactionGroup newTransactionGroup;
		boolean added = false;
		
		do {
			
			newTransactionGroup = addNewTransactionGroup();
			
			if (newTransactionGroup.matchesDate(transaction.getOccurredDate())) {
				newTransactionGroup.addTransaction(transaction);
				added = true;
			}
			
		} while (!added);
	}
	
	private TransactionGroup addNewTransactionGroup() {
		
		TransactionGroup newTransactionGroup = new TransactionGroup();
		
		newTransactionGroup.setLatestDateExclusive(earliestMonthSoFar.getTime());
		
		earliestMonthSoFar.add(MONTH, -1);
		newTransactionGroup.setEarliestDateInclusive(earliestMonthSoFar.getTime());
		
		newTransactionGroup.setName(getDataFormatter().formatDateAsFullMonthAndYear(newTransactionGroup.getEarliestDateInclusive()));
		
		addTransactionGroup(newTransactionGroup);
		
		return newTransactionGroup;
	}
}
