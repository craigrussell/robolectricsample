package com.tescobank.mobile.ui.transaction.group;

import java.util.ArrayList;
import java.util.List;

import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

/**
 * Captures and restores the group expansion state of an expandable list view
 */
public class ExpansionStateRestorer {
	
	private ExpandableListView expandableListView;
	private ExpandableListAdapter expandableListAdapter;
	private List<Boolean> groupExpansionStates;
	
	public ExpansionStateRestorer(ExpandableListView expandableListView, ExpandableListAdapter expandableListAdapter) {
		super();
		
		this.expandableListView = expandableListView;
		this.expandableListAdapter = expandableListAdapter;
		this.groupExpansionStates = new ArrayList<Boolean>();
	}
	
	public void captureState() {
		
		int groupCount = expandableListAdapter.getGroupCount();
		
		groupExpansionStates.clear();
		
		for (int i = 0; i < groupCount; i++) {
			groupExpansionStates.add(expandableListView.isGroupExpanded(i));
		}
	}
	
	public void restoreState() {
		
		int groupCount = expandableListAdapter.getGroupCount();
		int restoreCount = Math.min(groupCount, groupExpansionStates.size());
		
		restoreCapturedState(restoreCount);
		expandNewGroups(groupCount, restoreCount);
	}
	
	private void restoreCapturedState(int restoreCount) {
		
		for (int i = 0; i < restoreCount; i++) {
			if (groupExpansionStates.get(i)) {
				expandableListView.expandGroup(i);
			} else {
				expandableListView.collapseGroup(i);
			}
		}
	}
	
	private void expandNewGroups(int groupCount, int restoreCount) {
		for (int i = restoreCount; i < groupCount; i++) {
			expandableListView.expandGroup(i);
		}
	}
	
	public void expandAll() {
		
		int groupCount = expandableListAdapter.getGroupCount();
		
		for (int i = 0; i < groupCount; i++) {
			expandableListView.expandGroup(i);
		}
	}
	
	public void selectFirst() {
		if (expandableListAdapter.getGroupCount() > 0) {
			expandableListView.setSelectedGroup(0);
		}
	}
}
