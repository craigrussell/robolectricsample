package com.tescobank.mobile.ui.transaction;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.tescobank.mobile.BuildConfig;

public class OverscrollView extends View {

	private static final String TAG = OverscrollView.class.getSimpleName();

	private int height;

	public OverscrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public OverscrollView(Context context) {
		super(context);
	}

	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		View view = (View)getParent();
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onMeasure: " + view.getWidth() + " by " + height);
		}
		setMeasuredDimension(view.getWidth(), height);
	}

}
