package com.tescobank.mobile.ui.transaction.group;

import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;

/**
 * Collection of one transaction group where all transactions are added to that group
 */
public class SingleGroupTransactionGroups extends TransactionGroups {
	
	public SingleGroupTransactionGroups() {
		super(null);
		
		getTransactionGroups().add(new TransactionGroup());
	}

	@Override
	protected void addNewGroupsAndTransaction(Transaction transaction) {
		// Do nothing
	}
}
