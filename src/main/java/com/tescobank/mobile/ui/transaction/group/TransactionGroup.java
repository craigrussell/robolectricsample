package com.tescobank.mobile.ui.transaction.group;

import static java.util.Calendar.DAY_OF_YEAR;
import static java.util.Calendar.YEAR;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;

/**
 * Group of transactions
 */
public class TransactionGroup {

	private Date earliestDateInclusive;
	private Date latestDateExclusive;
	private List<Transaction> transactions;
	private String name;
	
	public TransactionGroup() {
		super();
		
		transactions = new ArrayList<Transaction>();
	}
	
	public Date getEarliestDateInclusive() {
		return earliestDateInclusive;
	}
	
	public void setEarliestDateInclusive(Date earliestDateInclusive) {
		this.earliestDateInclusive = earliestDateInclusive;
	}
	
	public Date getLatestDateExclusive() {
		return latestDateExclusive;
	}
	
	public void setLatestDateExclusive(Date latestDateExclusive) {
		this.latestDateExclusive = latestDateExclusive;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void addTransaction(Transaction transaction) {
		transactions.add(transaction);
	}
	
	public boolean matchesDate(Date date) {
		
		if (isDatesNull()) {
			return true;
		}
		
		if (latestDateExclusive == null) {
			return isDateOnOrAfterEarliestDateInclusive(date);
		}
		
		if (earliestDateInclusive == null) {
			return isDateBeforeLatestDateExclusive(date);
		}
		
		return isInDateRange(date);
	}
	
	public int size() {
		return transactions.size();
	}
	
	public boolean isEmpty() {
		return transactions.isEmpty();
	}
	
	public Transaction get(int location) {
		return transactions.get(location);
	}
	
	private boolean isDatesNull() {
		return (earliestDateInclusive == null) && (latestDateExclusive == null);
	}
	
	private boolean isInDateRange(Date date) {
		return isDateOnOrAfterEarliestDateInclusive(date) && isDateBeforeLatestDateExclusive(date);
	}
	
	private boolean isDateOnOrAfterEarliestDateInclusive(Date date) {
		
		Calendar dateCalendar = Calendar.getInstance();
		dateCalendar.setTime(date);
		
		Calendar earliestCalendar = Calendar.getInstance();
		earliestCalendar.setTime(earliestDateInclusive);
		
		int dateYear = dateCalendar.get(YEAR);
		int earliestYear = earliestCalendar.get(YEAR);
		
		if (dateYear > earliestYear) {
			return true;
		} else if (dateYear < earliestYear) {
			return false;
		} else {
			return dateCalendar.get(DAY_OF_YEAR) >= earliestCalendar.get(DAY_OF_YEAR);
		}
	}
	
	private boolean isDateBeforeLatestDateExclusive(Date date) {
		
		Calendar dateCalendar = Calendar.getInstance();
		dateCalendar.setTime(date);
		
		Calendar latestCalendar = Calendar.getInstance();
		latestCalendar.setTime(latestDateExclusive);
		
		int dateYear = dateCalendar.get(YEAR);
		int latestYear = latestCalendar.get(YEAR);
		
		if (dateYear < latestYear) {
			return true;
		} else if (dateYear > latestYear) {
			return false;
		} else {
			return dateCalendar.get(DAY_OF_YEAR) < latestCalendar.get(DAY_OF_YEAR);
		}
	}
}
