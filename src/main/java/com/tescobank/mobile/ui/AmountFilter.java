package com.tescobank.mobile.ui;

import android.text.InputFilter;
import android.text.Spanned;

public class AmountFilter implements InputFilter {

	private static final String ACCEPT = null;
	private static final String REJECT = "";
	private static final double MAX_AMOUNT = 999999.99;

	public AmountFilter() {
		super();
	}

	@Override
	public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

		CharSequence sourcePart = source.subSequence(start, end);
		CharSequence destStart = dest.subSequence(0, dstart);
		CharSequence destEnd = dest.subSequence(dend, dest.length());
		String stringValue = destStart.toString() + sourcePart.toString() + destEnd.toString();
		
		if (stringValue.trim().isEmpty()) {
			return ACCEPT;
		}
		
		if (!isValid(stringValue)) {
			return REJECT;
		}

		return ACCEPT;
	}
	
	private boolean isValid(String stringValue) {
		Double doubleValue = getDoubleValue(stringValue);
		return ((doubleValue != null) && (doubleValue <= MAX_AMOUNT) && !tooManyDecimalPlaces(stringValue));
	}
	
	private Double getDoubleValue(String stringValue) {
		try {
			return Double.valueOf(stringValue);
		} catch (NumberFormatException e) {
			return null;
		}
	}
	
	private boolean tooManyDecimalPlaces(String stringValue) {
		int index = stringValue.indexOf('.');
		return (index > 0) && !stringValue.endsWith(".") && (stringValue.split("\\.")[1].length() > 2);
	}
}
