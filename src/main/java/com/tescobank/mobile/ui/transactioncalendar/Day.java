package com.tescobank.mobile.ui.transactioncalendar;

import java.math.BigDecimal;
import static com.tescobank.mobile.ui.transactioncalendar.CalendarUtilities.calendarAtStartOfDay;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;
import com.tescobank.mobile.format.DataFormatter;

public class Day {

	public enum DayType {
		BLANK, PAST, FUTURE, TODAY
	}

	private DayType type;
	private Calendar day;
	private String label;
	private boolean creditCardDueDate;
	private boolean finalLoanPaymentDate;
	private boolean expanded;
	private List<Transaction> debits;
	private List<Transaction> credits;
	private BigDecimal totalDebits;
	private BigDecimal totalCredits;
	private DataFormatter dataFormatter;

	public Day() {
		this.type = DayType.BLANK;
		this.label = "";
		debits = new ArrayList<Transaction>();
		credits = new ArrayList<Transaction>();
		totalDebits = BigDecimal.ZERO;
		totalCredits = BigDecimal.ZERO;
		dataFormatter = new DataFormatter();
	}

	public void setDate(Calendar date) {
		this.day = calendarAtStartOfDay(date);
		this.type = getType(day);
		this.label = String.valueOf(day.get(Calendar.DAY_OF_MONTH));
	}

	public void setCreditCardDueDate(boolean dueDate) {
		this.creditCardDueDate = dueDate;
	}

	public boolean isCreditCardDueDate() {
		return creditCardDueDate;
	}

	public void setFinalLoanPaymentDate(boolean date) {
		this.finalLoanPaymentDate = date;
	}

	public boolean isFinalLoanPaymentDate() {
		return finalLoanPaymentDate;
	}

	public String getLabel() {
		return label;
	}

	public DayType getType() {
		return type;
	}

	private DayType getType(Calendar date) {
		Calendar now = new GregorianCalendar();
		Calendar today = calendarAtStartOfDay(now);
		if (date.before(today)) {
			return DayType.PAST;
		}
		if (date.after(today)) {
			return DayType.FUTURE;
		}
		return DayType.TODAY;
	}

	public Calendar getDate() {
		return day;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public boolean isExpandable() {
		return isCreditCardDueDate() || isFinalLoanPaymentDate() || !credits.isEmpty() || !debits.isEmpty();
	}

	public void open() {
		this.expanded = true;
	}

	public void close() {
		this.expanded = false;
	}

	public void addTranactions(List<Transaction> transactions) {
		for (Transaction transaction : transactions) {
			if (transaction.getCategory().equals(Transaction.CATEGORY_DEBIT)) {
				debits.add(transaction);
				totalDebits = totalDebits.add(transaction.getAmount());
			} else {
				credits.add(transaction);
				totalCredits = totalCredits.add(transaction.getAmount());
			}
		}
	}

	public BigDecimal getTotalDebits() {
		return totalDebits;
	}

	public BigDecimal getTotalCredits() {
		return totalCredits;
	}

	public String getTotalDebitsFormattedToNearestPound() {
		if (totalDebits.equals(BigDecimal.ZERO)) {
			return "";
		}
		return dataFormatter.formatCurrencyNoDecimalPlaces(totalDebits);
	}

	public String getTotalCreditsFormattedToNearestPound() {
		if (totalCredits.equals(BigDecimal.ZERO)) {
			return "";
		}
		return dataFormatter.formatCurrencyNoDecimalPlaces(totalCredits);
	}

	public boolean hasTransactions() {
		return !credits.isEmpty() || !debits.isEmpty();
	}

	public List<Transaction> getCredits() {
		return credits;
	}

	public List<Transaction> getDebits() {
		return debits;
	}
}
