package com.tescobank.mobile.ui.transactioncalendar;

import java.util.Calendar;
import java.util.GregorianCalendar;

public final class CalendarUtilities {

	private CalendarUtilities() {
	}
	
	public static int getWeeksInMonth(Calendar date) {
		Calendar lastDayInMonth = (Calendar) date.clone();
		lastDayInMonth.setFirstDayOfWeek(Calendar.MONDAY);
		lastDayInMonth.setMinimalDaysInFirstWeek(1);
		lastDayInMonth.set(Calendar.DAY_OF_MONTH, 1);
		lastDayInMonth.add(Calendar.MONTH, 1);
		lastDayInMonth.add(Calendar.DAY_OF_MONTH, -1);
		return lastDayInMonth.get(Calendar.WEEK_OF_MONTH);
	}
	
	public static boolean datesInSameMonth(Calendar first, Calendar second) {
		return first.get(Calendar.MONTH) == second.get(Calendar.MONTH);
	}
	
	public static Calendar calendarAtStartOfMonth(Calendar calendar) {
		return new GregorianCalendar(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),1);
	}
	
	public static Calendar calendarAtStartOfDay(Calendar calendar) {
		return new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
	}
}
