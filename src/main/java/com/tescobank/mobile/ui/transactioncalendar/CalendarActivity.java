package com.tescobank.mobile.ui.transactioncalendar;

import static android.content.Intent.FLAG_ACTIVITY_REORDER_TO_FRONT;
import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_CAROUSEL_LOADED;
import static com.tescobank.mobile.ui.transaction.TransactionRetriever.TRANSACTION_RETRIEVAL_EXTRA;
import static com.tescobank.mobile.ui.transaction.TransactionsActivity.TRANSACTIONS_PRODUCT_EXTRA;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.HideAndShowProgressIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.carousel.CarouselActivity;
import com.tescobank.mobile.ui.transaction.TransactionRetriever;
import com.tescobank.mobile.ui.transaction.TransactionRetriever.TransactionRetrivedResult;
import com.tescobank.mobile.ui.transaction.TransactionsActivity;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class CalendarActivity extends TescoActivity implements OnScrollListener, OnItemClickListener {

	private static final String TAG = CalendarActivity.class.getSimpleName();
	private static final int DISPLAY_MONTHS = 3;

	private DataFormatter formatter;
	private ProductDetails product;
	private TransactionRetriever transactionRetriever;
	private BroadcastReceiver transactionsBroadcastReceiver;

	private Calendar currentVisibleMonth;
	private View headerView;
	private ListView calendarView;

	private HeaderAdapter headerAdapter;
	private CalendarViewModel calendar;
	private CalendarAdapter calendarAdapter;
	private List<Transaction> transactionsBeingProcessed;
	boolean transactionsRetrieved;

	private HideAndShowProgressIndicator hideAndShowProgressIndicator;

	public static final String LAUNCHED_FROM_TRANSACTION_CALENDAR = "launched_from_transaction_calendar";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		if (BuildConfig.DEBUG) {
			Log.v(TAG, "Activity setup began at " + Calendar.getInstance().getTimeInMillis());
		}

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transaction_calendar);
		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		hideAndShowProgressIndicator = new HideAndShowProgressIndicator(this);
		formatter = new DataFormatter();
		product = (ProductDetails) getIntent().getExtras().getSerializable(TRANSACTIONS_PRODUCT_EXTRA);
		restoreSavedState(savedInstanceState);
		initialiseHeader();
		initialiseCalendar();
		startTransactionReceiver();
	}

	private void startTransactionReceiver() {
		super.lockScreenOrientation();
		transactionsBeingProcessed = new ArrayList<Transaction>();
		if(product != null) {
			if(!isTransactionRetrieverCreated()) {
				transactionRetriever = (TransactionRetriever) getIntent().getExtras().getSerializable(TRANSACTION_RETRIEVAL_EXTRA);
			}
			if (!isTransactionRetrieverCreated()) {
				transactionRetriever = new TransactionRetriever(product);
			}
			onTransactionsRetrieved(transactionRetriever.getTransactions());
		}
	}

	private boolean isTransactionRetrieverCreated() {
		return transactionRetriever != null;
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		transactionRetriever = (TransactionRetriever) intent.getExtras().getSerializable(TRANSACTION_RETRIEVAL_EXTRA);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(transactionRetriever != null) {
			outState.putSerializable(TRANSACTION_RETRIEVAL_EXTRA, transactionRetriever);
		}
	}
	
	private void restoreSavedState(Bundle savedInstanceState) {
		if(savedInstanceState == null) {
			return;
		}
		
		if(savedInstanceState.containsKey(TRANSACTION_RETRIEVAL_EXTRA)) {
			transactionRetriever = TransactionRetriever.class.cast(savedInstanceState.getSerializable(TRANSACTION_RETRIEVAL_EXTRA));
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		calendarView.setSelection(calendarAdapter.getPositionForMonth(1));

		if (BuildConfig.DEBUG) {
			Log.v(TAG, "Activity setup complete " + Calendar.getInstance().getTimeInMillis());
		}

		if (transactionRetriever.areMoreTransactionsAvailable()) {
			registerTransactionsBroadcastRecevier();
			transactionRetriever.retrieveMoreTransactions(this);
		}
	}

	@Override
	protected void onPause() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Pausing");
		}

		unregisterTransactionsBroadcastRecevier();
		super.onPause();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onOptionsItemSelected: " + item);
		}
		if (android.R.id.home == item.getItemId()) {
			Intent carousel = new Intent(this, CarouselActivity.class);
			carousel.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(carousel);
			finishWithResult(RESULT_CAROUSEL_LOADED);
		}

		if (item.getItemId() == R.id.menu_item_transactions) {
			getTooltipManager().dismiss(TooltipId.CalendarTransactionsToggle.ordinal());
			startTransactionActivity();
		}
		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		getSupportMenuInflater().inflate(R.menu.transaction_calendar, menu);
		configureTransactionListMenuItem(menu);
		return true;
	}

	@Override
	public void onRetryPressed() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Retry");
		}

		transactionRetriever.retrieveMoreTransactions(this);
	}

	@Override
	public boolean backFromRetryErrorShouldRetry() {
		return false;
	}

	private void configureTransactionListMenuItem(Menu menu) {
		MenuItem transactionListMenuItem = menu.findItem(R.id.menu_item_transactions);
		transactionListMenuItem.setVisible(transactionsRetrieved);
	}

	private void initialiseHeader() {
		headerView = findViewById(R.id.transaction_calendar_header);
		headerAdapter = new HeaderAdapter(this, headerView);
	}

	private void initialiseCalendar() {
		calendarView = (ListView) findViewById(R.id.transaction_calendar);

		Calendar startMonth = new GregorianCalendar();
		startMonth.add(Calendar.MONTH, -1);
		calendar = new CalendarViewModel(startMonth, DISPLAY_MONTHS);
		calendar.addCreditCardDueDates(getCreditCardPaymentDueDates());
		calendar.addFinalLoanPaymentDates(getFinalLoanPaymentDates());
		calendarAdapter = new CalendarAdapter(this, calendar);
		calendarView.setAdapter(calendarAdapter);
		calendarView.setOnItemClickListener(this);
		calendarView.setOnScrollListener(this);
		currentVisibleMonth = CalendarUtilities.calendarAtStartOfMonth(Calendar.getInstance());
		triggerAnalytic();
	}

	public static int getMeasuredRelativeLayoutHeight(View view) {
		if (view.getLayoutParams() == null) {
			view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
		}
		view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		return view.getMeasuredHeight();
	}

	private void updateHeaderData() {
		headerAdapter.setHeader(calendar.getMonthHeaders());
		calendarView.setOnScrollListener(this);
		updateHeaderView();
	}

	private void updateHeaderView() {
		headerAdapter.updateViewForMonth(currentVisibleMonth);
	}
	
	private void triggerAnalytic() {
		SimpleDateFormat formatter = new SimpleDateFormat("MMMM yyyy", Locale.UK);
		getApplicationState().getAnalyticsTracker().trackTransactionCalendar(formatter.format(currentVisibleMonth.getTime()));		
	}

	private List<Calendar> getCreditCardPaymentDueDates() {

		List<Calendar> creditCardPaymentDueDates = new ArrayList<Calendar>();
		List<ProductDetails> productDetails = getApplicationState().getProductDetails();
		if (productDetails != null) {
			for (ProductDetails productprodDetails : productDetails) {
				if (productprodDetails.isCreditCard() && productprodDetails.getPaymentDue() != null) {
					Calendar date = formatter.parseCalendarDate(productprodDetails.getPaymentDue());
					creditCardPaymentDueDates.add(date);
				}
			}
		}
		return creditCardPaymentDueDates;
	}

	private List<Calendar> getFinalLoanPaymentDates() {

		List<Calendar> finalLoanPaymentDates = new ArrayList<Calendar>();
		List<ProductDetails> productDetails = getApplicationState().getProductDetails();
		if (productDetails != null) {
			for (ProductDetails productprodDetails : productDetails) {
				if (productprodDetails.isLoan() && productprodDetails.getFinalPaymentDate() != null) {
					Calendar date = formatter.parseCalendarDate(productprodDetails.getFinalPaymentDate());
					finalLoanPaymentDates.add(date);
				}
			}
		}
		return finalLoanPaymentDates;
	}

	private void startTransactionActivity() {
		Intent transactionActivityIntent = new Intent(this, TransactionsActivity.class);
		transactionActivityIntent.setFlags(FLAG_ACTIVITY_REORDER_TO_FRONT);
		transactionActivityIntent.putExtra(TRANSACTIONS_PRODUCT_EXTRA, product);
		transactionActivityIntent.putExtra(TRANSACTION_RETRIEVAL_EXTRA, transactionRetriever);
		transactionActivityIntent.putExtra(LAUNCHED_FROM_TRANSACTION_CALENDAR, true);
		startActivity(transactionActivityIntent);
	}

	protected TransactionRetriever getTransactionRetriever() {
		return transactionRetriever;
	}

	private void registerTransactionsBroadcastRecevier() {
		hideAndShowProgressIndicator.showInProgressIndicator();
		transactionsBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				handleTransactionsReceived(intent);
			}
		};
		LocalBroadcastManager.getInstance(this).registerReceiver(transactionsBroadcastReceiver, new IntentFilter(TransactionRetriever.TRANSACTION_RETRIEVAL_BROADCAST_EVENT));
	}

	private void onTransactionsRetrieved(List<Transaction> transactions) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Transactions Retrieved");
		}

		for (Transaction transaction : transactions) {
			Date calendarStartDate = calendar.getStartDate().getTime();
			if (calendarStartDate.after(transaction.getOccurredDate())) {
				onFinishedRetrievingTransactions();
				break;
			}
			if (isTransactionOnDifferentDayToLastAdded(transaction)) {
				flushTransactionsBeingProcessed();
			}
			transactionsBeingProcessed.add(transaction);
		}
		retrieveMoreTransactions();
	}

	private void retrieveMoreTransactions() {
		if(transactionRetriever.areMoreTransactionsAvailable()) {
			transactionRetriever.retrieveMoreTransactions(this);
		} else {
			onFinishedRetrievingTransactions();
		}
	}

	private boolean isTransactionOnDifferentDayToLastAdded(Transaction transaction) {
		if (transactionsBeingProcessed.isEmpty()) {
			return false;
		}
		Date dateOfLastTransactionAdded = transactionsBeingProcessed.get(transactionsBeingProcessed.size() - 1).getOccurredDate();
		return !transaction.getOccurredDate().equals(dateOfLastTransactionAdded);
	}

	private void flushTransactionsBeingProcessed() {
		if (!transactionsBeingProcessed.isEmpty()) {
			Date dateOfTransactions = transactionsBeingProcessed.get(0).getOccurredDate();
			addListOfTransactionsToDay(transactionsBeingProcessed, dateOfTransactions);
			transactionsBeingProcessed = new ArrayList<Transaction>();
		}
	}

	private void addListOfTransactionsToDay(List<Transaction> transactions, Date date) {
		Calendar dateOfTransactions = new GregorianCalendar();
		dateOfTransactions.setTime(date);
		calendar.addDaysTransactions(dateOfTransactions, transactions);
		calendarAdapter.notifyDataSetChanged();
	}

	private void onFinishedRetrievingTransactions() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Finished Retrieving transactions");
		}
		flushTransactionsBeingProcessed();
		unregisterTransactionsBroadcastRecevier();
		updateHeaderData();
		calendarAdapter.onFinishedLoadingData();
		transactionsRetrieved = true;
		supportInvalidateOptionsMenu();
		super.unlockScreenOrientation();
	}

	private void handleTransactionsReceived(Intent intent) {
		TransactionRetrivedResult status = (TransactionRetrivedResult) intent.getSerializableExtra(TransactionRetriever.TRANSACTION_RETRIEVAL_STATUS_EXTRA);
		status = (status == null ? TransactionRetrivedResult.FAIL : status);

		switch (status) {
		case SUCCESS:
			@SuppressWarnings("unchecked")
			List<Transaction> transactions = (ArrayList<Transaction>) intent.getSerializableExtra(TransactionRetriever.TRANSACTION_RETRIEVAL_NEW_EXTRA);
			onTransactionsRetrieved(transactions);
			break;
		case NO_MORE:
			onFinishedRetrievingTransactions();
			break;
		case FAIL:
			ErrorResponseWrapper error = (ErrorResponseWrapper) intent.getSerializableExtra(ErrorResponseWrapper.class.getName());
			if (error != null) {
				handleErrorResponse(error);
			}
			break;
		}
	}

	private void unregisterTransactionsBroadcastRecevier() {
		hideAndShowProgressIndicator.hideInProgressIndicator();
		if (transactionsBroadcastReceiver != null) {
			LocalBroadcastManager.getInstance(this).unregisterReceiver(transactionsBroadcastReceiver);
		}
	}
	
	@Override
	public synchronized void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
		calendarAdapter.onItemClick(adapterView, view, position, id);
			calendarView.postDelayed(new Runnable() {
				@Override
				public void run() {
					refreshCalendarScrollPosition();	
				}
			}, 500);
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		calendar.setScrollPosition(firstVisibleItem, visibleItemCount);
	}
	
	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		if (scrollState == SCROLL_STATE_IDLE) {
			onCalendarPositionChanged();
		}
	}

	private void onCalendarPositionChanged() {
		if(!currentVisibleMonth.equals(calendar.getCurrentlyVisibleMonth())) {
			currentVisibleMonth = calendar.getCurrentlyVisibleMonth();
			updateHeaderView();
			triggerAnalytic();
		}
	}
	
	private void refreshCalendarScrollPosition() {
		int firstVisible = calendarView.getFirstVisiblePosition();
		int lastVisible = calendarView.getLastVisiblePosition();
		int count = lastVisible - firstVisible + 1;
		calendar.setScrollPosition(firstVisible,count);
		onCalendarPositionChanged();
	}
	
	@Override
	public void unlockScreenOrientation() {
		//do nothing, we will control locking behaviour
	}
	
	@Override
	public void onBackPressed() {
		if(!calendarAdapter.onBackPressed()) {
			super.onBackPressed();
		}
	}
}