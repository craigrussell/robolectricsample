package com.tescobank.mobile.ui.transactioncalendar;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;

public abstract class TransactionListRow {

	public static enum TransactionListRowType {
		TRANSACTION, TOTAL;

		public static int count() {
			return values().length;
		}
	}

	public abstract TransactionListRowType getType();

	public int getTypeNumber() {
		return getType().ordinal();
	}

	public static class TransactionRow extends TransactionListRow {

		private Transaction transaction;

		public TransactionRow(Transaction transaction) {
			super();
			this.transaction = transaction;
		}

		@Override
		public TransactionListRowType getType() {
			return TransactionListRowType.TRANSACTION;
		}

		public boolean isCredit() {
			return transaction.isCredit();
		}

		public String getDescription() {
			return transaction.getCondensedDescription();
		}

		public String getAmountText() {
			return NumberFormat.getCurrencyInstance(Locale.UK).format(transaction.getAmount());
		}
	}

	public static class TotalRow extends TransactionListRow {

		private String description;
		private BigDecimal amount;
		private boolean subtotal;

		public TotalRow(String description, BigDecimal amount, boolean isSubtotal) {
			super();
			this.description = description;
			this.amount = amount;
			this.subtotal = isSubtotal;
		}

		@Override
		public TransactionListRowType getType() {
			return TransactionListRowType.TOTAL;
		}

		public boolean isCredit() {
			return amount.compareTo(BigDecimal.ZERO) >= 0;
		}

		public boolean isSubtotal() {
			return subtotal;
		}
		
		public String getTotalText() {
			return description;
		}

		public String getAmountText() {
			return NumberFormat.getCurrencyInstance(Locale.UK).format(amount);
		}
	}
}