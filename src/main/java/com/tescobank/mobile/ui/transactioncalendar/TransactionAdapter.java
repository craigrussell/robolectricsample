package com.tescobank.mobile.ui.transactioncalendar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;
import com.tescobank.mobile.ui.transactioncalendar.TransactionListRow.TotalRow;
import com.tescobank.mobile.ui.transactioncalendar.TransactionListRow.TransactionListRowType;
import com.tescobank.mobile.ui.transactioncalendar.TransactionListRow.TransactionRow;

public class TransactionAdapter {

	private Context context;
	private LayoutInflater inflater;
	private List<TransactionListRow> transactionsViewModel;

	public TransactionAdapter(Context ctx, Day day) {
		super();
		this.context = ctx;
		this.inflater = LayoutInflater.from(ctx);
		TransactionsViewModelFactory factory = new TransactionsViewModelFactory(ctx, day);
		transactionsViewModel = factory.get();
	}

	public int getCount() {
		return transactionsViewModel.size();
	}

	public void populateViewContainer(LinearLayout listContaier) {
		listContaier.removeAllViewsInLayout();
		for (int i = 0; i < getCount(); i++) {
			listContaier.addView(getView(i));
		}
	}

	public View getView(int position) {

		TransactionListRow row = transactionsViewModel.get(position);
		if (row.getType() == TransactionListRowType.TRANSACTION) {
			return getTransactionView((TransactionRow) row);
		} else {
			return getTotalView((TotalRow) row);
		}
	}

	private View getTransactionView(TransactionRow row) {
		View view = inflater.inflate(R.layout.transaction_calendar_transaction, null);
		TextView description = (TextView) view.findViewById(R.id.transaction_calendar_transaction_description);
		description.setText(row.getDescription());
		TextView amount = (TextView) view.findViewById(R.id.transaction_calendar_transaction_amount);
		amount.setText(row.getAmountText());
		applyStyleToTransactionRow(view, row);
		return view;
	}

	private void applyStyleToTransactionRow(View view, TransactionRow row) {
		if (row.isCredit()) {
			styleTransactionRow(view, R.drawable.ic_in_white_large, R.string.product_transactions_out_description, R.color.transaction_calendar_transaction_credit_text);
		} else {
			styleTransactionRow(view, R.drawable.ic_out_white_large, R.string.product_transactions_out_description, R.color.transaction_calendar_transaction_debit_text);
		}
	}

	private void styleTransactionRow(View view, int iconId, int iconDescriptionId, int colorId) {
		ImageView icon = (ImageView) view.findViewById(R.id.transaction_calendar_transaction_type_icon);
		icon.setImageDrawable(context.getResources().getDrawable(iconId));
		icon.setContentDescription(context.getString(iconDescriptionId));

		TextView amount = (TextView) view.findViewById(R.id.transaction_calendar_transaction_amount);
		amount.setTextColor(context.getResources().getColor(colorId));
	}

	private View getTotalView(TotalRow row) {

		View view = inflater.inflate(R.layout.transaction_calendar_transaction_total, null);
		View divider = view.findViewById(R.id.transaction_calendar_transaction_total_divider);
		divider.setVisibility(row.isSubtotal() ? View.GONE : View.VISIBLE);

		int descriptionColor = row.isSubtotal() ? R.color.transaction_calendar_transaction_text : R.color.blue_tesco;
		TextView description = (TextView) view.findViewById(R.id.transaction_calendar_transaction_total_description);
		description.setText(row.getTotalText());
		description.setTextColor(context.getResources().getColor(descriptionColor));

		int amountColour = row.isCredit() ? R.color.transaction_calendar_transaction_credit_text : R.color.transaction_calendar_transaction_debit_text;
		TextView amount = (TextView) view.findViewById(R.id.transaction_calendar_transaction_total_amount);
		amount.setTextColor(context.getResources().getColor(amountColour));
		amount.setText(row.getAmountText());

		return view;
	}

	private static class TransactionsViewModelFactory {

		private Context context;
		private Day day;
		private List<TransactionListRow> transactionsViewModel;

		public TransactionsViewModelFactory(Context context, Day day) {
			this.context = context;
			this.day = day;
			transactionsViewModel = new ArrayList<TransactionListRow>();
			addRows();
		}

		public List<TransactionListRow> get() {
			return transactionsViewModel;
		}

		private void addRows() {
			addTransactions(day.getCredits());
			if (showSubtotals()) {
				String description = context.getString(R.string.transaction_calendar_total_income);
				addTotal(description, day.getTotalCredits(), true);
			}
			addTransactions(day.getDebits());
			if (showSubtotals()) {
				String description = context.getString(R.string.transaction_calendar_total_outgoings);
				addTotal(description, day.getTotalDebits(), true);
			}
			String description = context.getString(R.string.transaction_calendar_total);
			addTotal(description, day.getTotalCredits().add(day.getTotalDebits()), false);
		}

		private void addTransactions(List<Transaction> transactions) {
			for (Transaction transaction : transactions) {
				transactionsViewModel.add(new TransactionRow(transaction));
			}
		}

		private void addTotal(String description, BigDecimal amount, boolean isSubtotal) {
			transactionsViewModel.add(new TotalRow(description, amount, isSubtotal));
		}

		private boolean showSubtotals() {
			return (day.getCredits().size() >= 1 && day.getDebits().size() >= 1) && (day.getCredits().size() > 1 || day.getDebits().size() > 1);
		}
	}
}
