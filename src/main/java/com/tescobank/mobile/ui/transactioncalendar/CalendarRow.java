package com.tescobank.mobile.ui.transactioncalendar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.tescobank.mobile.ui.transactioncalendar.Day.DayType;

public abstract class CalendarRow {
	
	private static final int LAST_DAY_IN_WEEK_INDEX = 6;
	
	public abstract CalendarRowType getType();

	public int getTypeNumber() {
		return getType().ordinal();
	}

	public static enum CalendarRowType {
		MONTH_TITLE, WEEK;

		public static int count() {
			return values().length;
		}
	}

	public static enum WeekDay {
		MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;

		public static int count() {
			return values().length;
		}
	}

	public static class MonthTitle extends CalendarRow {

		private String label;
		private int labelPosition;

		public MonthTitle(Calendar date) {
			super();
			this.label = createMonthLabel(date);
			this.labelPosition = getDayOfWeekIndex(date);
		}

		private String createMonthLabel(Calendar date) {
			SimpleDateFormat sdf = new SimpleDateFormat("MMM", Locale.UK);
			String month = sdf.format(date.getTime());
			return month.toUpperCase(Locale.UK);
		}

		public CalendarRowType getType() {
			return CalendarRowType.MONTH_TITLE;
		}

		public String getLabel() {
			return label;
		}

		public int getLabelPosition() {
			return labelPosition;
		}
	}

	public static class Week extends CalendarRow {

		private List<Day> days;

		public Week() {
			super();
			int numberOfDaysInWeek = WeekDay.count();
			days = new ArrayList<Day>(numberOfDaysInWeek);
			for (int i = 0; i < numberOfDaysInWeek; i++) {
				days.add(new Day());
			}
		}

		@Override
		public CalendarRowType getType() {
			return CalendarRowType.WEEK;
		}

		public Day getDayAtIndex(int weekday) {
			return days.get(weekday);
		}
		
		public Day getFirstNonBlankDay() {
			for (Day day : days) {
				if(!day.getType().equals(DayType.BLANK)) {
					return day;
				}
			}
			return null;
		}
	}
	
	public static int getDayOfWeekIndex(Calendar date) {
		return (date.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) ? LAST_DAY_IN_WEEK_INDEX : (date.get(Calendar.DAY_OF_WEEK) - 2);
	}
}
