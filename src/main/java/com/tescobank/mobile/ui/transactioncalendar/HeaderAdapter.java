package com.tescobank.mobile.ui.transactioncalendar;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tescobank.mobile.R;

public class HeaderAdapter {

	private Context context;

	private Map<Calendar, Header> headers;
	private Header header;

	private View headerView;
	private TextView monthText;
	private TextView inLabel;
	private TextView outLabel;
	private TextView differenceLabel;
	private View inContainer;
	private View outContainer;
	private View differenceContainer;
	private int barContainerWidth;
	private boolean expanded;

	public HeaderAdapter(Context ctx, View headerView) {
		super();
		this.context = ctx;
		this.headers = new HashMap<Calendar, Header>();
		this.headerView = headerView;
		initialiseViews();
		attachClickListener();
	}

	public void setHeader(Map<Calendar, Header> headers) {
		this.headers = headers;
	}

	private void initialiseViews() {
		monthText = (TextView) headerView.findViewById(R.id.transaction_calendar_header_month_text);
		inLabel = (TextView) headerView.findViewById(R.id.transaction_calendar_header_in_label);
		outLabel = (TextView) headerView.findViewById(R.id.transaction_calendar_header_out_label);
		differenceLabel = (TextView) headerView.findViewById(R.id.transaction_calendar_header_difference_label);
		inContainer = headerView.findViewById(R.id.transaction_calendar_header_in);
		outContainer = headerView.findViewById(R.id.transaction_calendar_header_out);
		differenceContainer = headerView.findViewById(R.id.transaction_calendar_header_difference);
		applyStyleToAmountView(inContainer, R.drawable.ic_in_white_large, R.string.transaction_calendar_header_in_description, R.color.transaction_calendar_in);
		applyStyleToAmountView(outContainer, R.drawable.ic_out_white_large, R.string.transaction_calendar_header_out_description, R.color.transaction_calendar_out);
		applyStyleToDifferenceAmountView();
	}

	private void attachClickListener() {
		headerView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				onHeaderClicked();
			}
		});
	}

	public void updateViewForMonth(Calendar headerMonth) {
		Header newHeader = headers.get(headerMonth);
		if (newHeader != null && !newHeader.equals(header)) {
			this.header = newHeader;
			monthText.setText(header.getMonthLabel());
			barContainerWidth = inContainer.findViewById(R.id.transaction_calendar_header_amount_bar_container).getWidth();
			if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1) {
				refreshAmountsWithAnimation();
			} else {
				refreshAmounts();
			}
		}
	}

	private void applyStyleToAmountView(View container, int iconId, int contentDescriptionId, int barColourId) {
		ImageView icon = (ImageView) container.findViewById(R.id.transaction_calendar_header_amount_icon);
		icon.setImageDrawable(context.getResources().getDrawable(iconId));
		icon.setContentDescription(context.getResources().getString(contentDescriptionId));

		View bar = container.findViewById(R.id.transaction_calendar_header_amount_bar);
		bar.setBackgroundColor(context.getResources().getColor(barColourId));
	}

	private void applyStyleToDifferenceAmountView() {
		if (header != null && header.isPositiveOrEqualDifference()) {
			applyStyleToAmountView(differenceContainer, R.drawable.ic_diff_green_large, R.string.transaction_calendar_header_diff_description, R.color.transaction_calendar_diff_pos);
		} else {
			applyStyleToAmountView(differenceContainer, R.drawable.ic_diff_large, R.string.transaction_calendar_header_diff_description, R.color.transaction_calendar_diff_neg);
		}
	}

	private void refreshAmounts() {
		refreshAmountView(inContainer, header.getInAmountLabel(), header.getInAmountRatio());
		refreshAmountView(outContainer, header.getOutAmountLabel(), header.getOutAmountRatio());
		refreshAmountView(differenceContainer, header.getDifferenceAmountLabel(), header.getDiffAmountRatio());
		applyStyleToDifferenceAmountView();
	}

	private void refreshAmountsWithAnimation() {
		HeaderChangeAnimator animator = new HeaderChangeAnimator();
		animator.animate();
	}

	private void clearAllAmounts() {
		updateAmountText(inContainer, "");
		updateAmountText(outContainer, "");
		updateAmountText(differenceContainer, "");
	}

	private void refreshAmountView(View container, String amountText, float barPercentage) {
		View bar = container.findViewById(R.id.transaction_calendar_header_amount_bar);
		bar.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, barPercentage));
		updateAmountText(container, amountText);
	}

	private void updateAmountText(View container, String amountText) {
		TextView amount = (TextView) container.findViewById(R.id.transaction_calendar_header_amount_text);
		amount.setText(amountText);
		layoutAmountText(container, amount);
	}

	private void layoutAmountText(View container, TextView amount) {
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) amount.getLayoutParams();
		int amountWidth = getMeasuredWidth(amount);
		int barWidth = getBarWidth(container);

		if (amountWidth >= barWidth) {
			params.setMargins(barWidth, 0, 0, 0);
		} else {
			params.setMargins(barWidth - amountWidth, 0, 0, 0);
		}
	}

	private int getBarWidth(View container) {
		View bar = container.findViewById(R.id.transaction_calendar_header_amount_bar);
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) bar.getLayoutParams();
		return (int) (barContainerWidth * params.weight);
	}

	private int getMeasuredWidth(View view) {
		view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		return view.getMeasuredWidth();
	}

	private synchronized void onHeaderClicked() {
		if (header == null) {
			return;
		}

		if (expanded) {
			closeHeader();
		} else {
			expandHeader();
			refreshAmounts();
		}
	}

	private void expandHeader() {
		expanded = true;
		inLabel.setVisibility(View.VISIBLE);
		outLabel.setVisibility(View.VISIBLE);
		differenceLabel.setVisibility(View.VISIBLE);
		differenceContainer.setVisibility(View.VISIBLE);
	}

	private void closeHeader() {
		expanded = false;
		inLabel.setVisibility(View.GONE);
		outLabel.setVisibility(View.GONE);
		differenceLabel.setVisibility(View.GONE);
		differenceContainer.setVisibility(View.GONE);
	}

	private class HeaderChangeAnimator implements AnimationListener {

		private static final int ANIMATION_DURATION_MS = 500;
		
		private void animate() {
			AnimationSet animations = new AnimationSet(true);
			animations.addAnimation(getAmountChangeAnimation(inContainer, header.getInAmountRatio()));
			animations.addAnimation(getAmountChangeAnimation(outContainer, header.getOutAmountRatio()));
			animations.addAnimation(getAmountChangeAnimation(differenceContainer, header.getDiffAmountRatio()));
			animations.setDuration(ANIMATION_DURATION_MS);
			animations.setAnimationListener(this);
			headerView.startAnimation(animations);
		}

		private Animation getAmountChangeAnimation(final View view, final float barPercentage) {
			View bar = view.findViewById(R.id.transaction_calendar_header_amount_bar);
			LinearLayout.LayoutParams params = (LayoutParams) bar.getLayoutParams();
			float start = params.weight;
			return new WeightChangeAnimation(bar, start, barPercentage);
		}

		@Override
		public void onAnimationStart(Animation animation) {
			clearAllAmounts();
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			refreshAmounts();
		}
	}

	private static class WeightChangeAnimation extends Animation {

		private View view;
		private float start;
		private float difference;

		public WeightChangeAnimation(View view, float start, float finish) {
			super();
			this.view = view;
			this.start = start;
			this.difference = finish - start;
		}

		@Override
		protected void applyTransformation(float interpolatedTime, Transformation t) {
			float weight = start + (difference * interpolatedTime);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, weight);
			view.setLayoutParams(params);
		}
	}
}
