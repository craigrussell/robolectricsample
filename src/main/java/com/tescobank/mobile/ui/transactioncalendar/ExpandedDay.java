package com.tescobank.mobile.ui.transactioncalendar;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class ExpandedDay {

	private Day day;
	private String label;
	
	public ExpandedDay(Day day) {
		this.day = day;
		initialiseLabel();
	}
	private void initialiseLabel() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.UK);
		label  = sdf.format(day.getDate().getTime());
	}

	public String getLabel() {
		return label;
	}
	
	public Day getDay() {
		return day;
	}
}