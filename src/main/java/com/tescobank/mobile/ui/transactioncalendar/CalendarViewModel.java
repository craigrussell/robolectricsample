package com.tescobank.mobile.ui.transactioncalendar;

import static com.tescobank.mobile.ui.transactioncalendar.CalendarUtilities.calendarAtStartOfMonth;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;
import com.tescobank.mobile.ui.transactioncalendar.CalendarRow.MonthTitle;
import com.tescobank.mobile.ui.transactioncalendar.CalendarRow.Week;
import com.tescobank.mobile.ui.transactioncalendar.CalendarRow.WeekDay;

public class CalendarViewModel {

	private static final String TAG = CalendarViewModel.class.getSimpleName();

	private int numberOfMonths;
	private Calendar startDate;
	private Map<Calendar, Day> days;
	private List<CalendarRow> calendarRows;
	private int scrollPosition;
	private int visibleRows;

	public CalendarViewModel(Calendar startMonth, int numberOfMonths) {
		this.numberOfMonths = numberOfMonths;
		startDate = calendarAtStartOfMonth(startMonth);
		CalendarViewModelBuilder builder = new CalendarViewModelBuilder();
		builder.build();
	}

	public void addCreditCardDueDates(List<Calendar> creditCardDueDates) {
		for (Calendar dueDate : creditCardDueDates) {
			Day day = days.get(dueDate);
			if (day != null) {
				day.setCreditCardDueDate(true);
			}
		}
	}

	public void addFinalLoanPaymentDates(List<Calendar> finalLoanPaymentDates) {
		for (Calendar date : finalLoanPaymentDates) {
			Day day = days.get(date);
			if (day != null) {
				day.setFinalLoanPaymentDate(true);
			}
		}
	}

	public int getNumberOfMonths() {
		return numberOfMonths;
	}

	public Calendar getStartDate() {
		return startDate;
	}

	public void addDay(Calendar date, Day day) {
		days.put(date, day);
	}

	public Day getDay(Calendar date) {
		return days.get(date);
	}

	public CalendarRow getRow(int position) {
		return calendarRows.get(position);
	}

	public void addRow(CalendarRow row) {
		calendarRows.add(row);
	}

	public void addRow(int index, CalendarRow row) {
		calendarRows.add(index, row);
	}

	public void removeRow(int position) {
		calendarRows.remove(position);
	}

	public void removeRow(CalendarRow row) {
		calendarRows.remove(row);
	}

	public int positionOf(CalendarRow row) {
		return calendarRows.indexOf(row);
	}

	public int rowCount() {
		return calendarRows.size();
	}

	public void addDaysTransactions(Calendar transactionsDate, List<Transaction> transactions) {
		Day day = getDay(transactionsDate);
		if (day != null) {
			day.addTranactions(transactions);
		}
	}

	public Map<Calendar, Header> getMonthHeaders() {
		Map<Calendar, Header> headers = new HashMap<Calendar, Header>();
		for (Day day : days.values()) {
			Calendar current = day.getDate();
			Calendar month = new GregorianCalendar(current.get(Calendar.YEAR), current.get(Calendar.MONTH), 1);

			Header header = headers.get(month);
			if (header == null) {
				header = new Header(month);
				headers.put(month, header);
			}
			header.addCredit(day.getTotalCredits());
			header.addDebit(day.getTotalDebits());
		}
		return headers;
	}

	public void setScrollPosition(int scrollPosition, int visibleRows) {
		this.scrollPosition = scrollPosition;
		this.visibleRows = visibleRows;
	}

	public Calendar getCurrentlyVisibleMonth() {
		Map<Calendar, Integer> weekCount = new LinkedHashMap<Calendar, Integer>();
		for (int i = scrollPosition; i < scrollPosition + visibleRows; i++) {
			CalendarRow row = calendarRows.get(i);
			if (row instanceof Week) {
				incrementWeekCount(weekCount, (Week) row);
			}
		}
		Calendar currentlyVisible = calculateFirstFullyVisibleMonth(weekCount);
		if(currentlyVisible == null) {
			currentlyVisible = calculateMonthWithHighestWeekCount(weekCount);
		}
		return currentlyVisible;
	}

	
	private Calendar calculateFirstFullyVisibleMonth(Map<Calendar, Integer> weekCount) {
		for (Calendar month : weekCount.keySet()) {
			if(CalendarUtilities.getWeeksInMonth(month) == weekCount.get(month)) {
				return month;
			}
		}
		return null;
	}

	private Calendar calculateMonthWithHighestWeekCount(Map<Calendar, Integer> weekCount) {
		Calendar highest = null;
		for (Calendar month : weekCount.keySet()) {
			if (highest == null || weekCount.get(month) > weekCount.get(highest)) {
				highest = month;
			}
		}
		return highest;
	}

	private void incrementWeekCount(Map<Calendar, Integer> weekCount, Week week) {
		Day day = week.getFirstNonBlankDay();
		if (day != null) {
			Calendar month = calendarAtStartOfMonth(day.getDate());
			if (weekCount.containsKey(month)) {
				int count = weekCount.get(month) + 1;
				weekCount.put(month, count);
			} else {
				weekCount.put(month, 1);
			}
		}
	}

	private class CalendarViewModelBuilder {

		private Calendar currentDay;

		public CalendarViewModelBuilder() {
			currentDay = (Calendar) startDate.clone();
		}
		
		public void build() {
			days = new HashMap<Calendar, Day>();
			calendarRows = new ArrayList<CalendarRow>();
			for (int i = 0; i < getNumberOfMonths(); i++) {
				buildMonth();
			}
			setScrollPosition(0, 2);

			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Calendar built, initial row count is " + rowCount());
			}
		}

		private void buildMonth() {
			CalendarRow monthTitle = new MonthTitle(currentDay);
			addRow(monthTitle);

			int currentWeeksInMonth = CalendarUtilities.getWeeksInMonth(currentDay);
			for (int j = 0; j < currentWeeksInMonth; j++) {
				buildWeek();
			}
		}

		private void buildWeek() {
			Calendar startDay = (Calendar) currentDay.clone();
			Week week = new Week();

			int dayOfWeekIndex = CalendarRow.getDayOfWeekIndex(currentDay);
			for (int i = dayOfWeekIndex; i < WeekDay.count() && CalendarUtilities.datesInSameMonth(startDay, currentDay); i++) {
				Day day = week.getDayAtIndex(i);
				day.setDate(currentDay);
				addDay(day.getDate(), day);
				currentDay.add(Calendar.DAY_OF_MONTH, 1);
			}
			addRow(week);
		}
	}
}
