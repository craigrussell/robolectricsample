package com.tescobank.mobile.ui.transactioncalendar;

import java.util.Calendar;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.transactioncalendar.CalendarRow.CalendarRowType;
import com.tescobank.mobile.ui.transactioncalendar.CalendarRow.MonthTitle;
import com.tescobank.mobile.ui.transactioncalendar.CalendarRow.Week;
import com.tescobank.mobile.ui.transactioncalendar.CalendarRow.WeekDay;

public class CalendarAdapter extends BaseAdapter {

	private Context context;
	private LayoutInflater inflator;

	private CalendarViewModel calendar;
	private View expandedWeekView;
	private Day currentExpandedDay;
	private Day nextExpandedDay;
	
	private boolean canAnimate;
	private boolean expandAnimationInProgress;

	public CalendarAdapter(Context ctx, CalendarViewModel calendar) {
		this.context = ctx;
		this.inflator = LayoutInflater.from(ctx);
		this.calendar = calendar;
	}

	public int getPositionForMonth(int monthsFromStart) {
		Calendar scrollMonth = (Calendar) calendar.getStartDate().clone();

		int position = 0;
		for (int i = 0; i < monthsFromStart; i++) {
			position++;
			position += CalendarUtilities.getWeeksInMonth(scrollMonth);
			scrollMonth.add(Calendar.MONTH, 1);
		}
		return position;
	}

	@Override
	public int getCount() {
		return calendar.rowCount();
	}

	@Override
	public int getViewTypeCount() {
		return CalendarRowType.count();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		CalendarRow row = calendar.getRow(position);
		switch (row.getType()) {
		case MONTH_TITLE:
			return getMonthTitleView(convertView, (MonthTitle) row);
		case WEEK:
			return getWeekView(convertView, (Week) row);
		}
		return convertView;
	}

	private View getMonthTitleView(View convertView, MonthTitle monthTitle) {
		View view = (convertView != null) ? convertView : inflator.inflate(R.layout.transaction_calendar_month_title, null);
		int currentId = context.getResources().getIdentifier("month_name_pos_" + monthTitle.getLabelPosition(), "id", "com.tescobank.mobile");
		for (int i = 0; i < WeekDay.count(); i++) {
			int textViewId = context.getResources().getIdentifier("month_name_pos_" + i, "id", "com.tescobank.mobile");
			View positionContainer = view.findViewById(textViewId);
			TextView textView = (TextView) positionContainer.findViewById(R.id.month_name);
			if (currentId == textViewId) {
				textView.setText(monthTitle.getLabel());
			} else {
				textView.setText("");
			}
		}
		return view;
	}

	private View getWeekView(View convertView, Week week) {
		
		View view = (convertView != null) ? convertView : inflator.inflate(R.layout.transaction_calendar_week, null);
		View expandedView = view.findViewById(R.id.expanded_day);
		expandedView.setVisibility(View.GONE);

		for (int i = 0; i < WeekDay.count(); i++) {
			Day day = week.getDayAtIndex(i);
			int dayViewId = context.getResources().getIdentifier("day_" + i, "id", "com.tescobank.mobile");
			View dayView = view.findViewById(dayViewId);
			configureDayView(dayView, day);
			if (day.isExpanded()) {
				configureExpandedDayView(expandedView, new ExpandedDay(day));
				if(!expandAnimationInProgress) {
					view.findViewById(R.id.expanded_day).setVisibility(View.VISIBLE);
				}
			}
		}
		return view;
	}
	
	private void configureExpandedDayView(View view, ExpandedDay expandedDay) {
		view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		TextView label = (TextView) view.findViewById(R.id.transaction_calendar_date);
		label.setText(expandedDay.getLabel());
		configureExpandedDayViewCreditCardMessage(view, expandedDay);
		configureExpandedDayViewLoanMessage(view, expandedDay);
		configureExpandedViewTransactions(view, expandedDay);
	}

	private void configureExpandedDayViewCreditCardMessage(View view, ExpandedDay expandedDay) {
		View infoContainer = view.findViewById(R.id.transaction_calendar_credit_message_container);
		Day day = expandedDay.getDay();
		if (day.isCreditCardDueDate()) {
			TextView infoText = (TextView) infoContainer.findViewById(R.id.informationText);
			infoText.setText(context.getString(R.string.transaction_calendar_credit_card_due_message));
			infoContainer.setVisibility(View.VISIBLE);
		} else {
			infoContainer.setVisibility(View.GONE);
		}
	}

	private void configureExpandedDayViewLoanMessage(View view, ExpandedDay expandedDay) {
		View infoContainer = view.findViewById(R.id.transaction_calendar_loan_message_container);
		Day day = expandedDay.getDay();
		if (day.isFinalLoanPaymentDate()) {
			TextView infoText = (TextView) infoContainer.findViewById(R.id.informationText);
			infoText.setText(context.getString(R.string.transaction_calendar_final_loan_due_message));
			infoContainer.setVisibility(View.VISIBLE);
		} else {
			infoContainer.setVisibility(View.GONE);
		}
	}

	private void configureExpandedViewTransactions(View view, ExpandedDay expandedDay) {
		Day day = expandedDay.getDay();
		View transactionsContainer = view.findViewById(R.id.transaction_calendar_transactions_container);
		transactionsContainer.setVisibility(View.GONE);
		if (day.hasTransactions()) {
			LinearLayout listView = (LinearLayout) view.findViewById(R.id.transaction_calendar_transactions);
			TransactionAdapter adapter = new TransactionAdapter(context, day);
			adapter.populateViewContainer(listView);
			transactionsContainer.setVisibility(View.VISIBLE);
		}
	}

	private void configureDayView(View dayView, Day day) {
		dayView.setTag(day.getDate());

		TextView dayText = (TextView) dayView.findViewById(R.id.date);
		dayText.setText(day.getLabel());

		ImageView paymentIcon = (ImageView) dayView.findViewById(R.id.info_icon);
		paymentIcon.setVisibility(day.isCreditCardDueDate() || day.isFinalLoanPaymentDate() ? View.VISIBLE : View.GONE);

		setOutgoingsAndIncomingsForDay(day, dayView);
		dayView.setOnTouchListener(new DayTouchHandler());
		styleDayView(dayView, day);
	}

	private void setOutgoingsAndIncomingsForDay(Day day, View dayView) {
		TextView outgoingsTextView = (TextView) dayView.findViewById(R.id.total_out);
		TextView incomingsTextView = (TextView) dayView.findViewById(R.id.total_in);
		outgoingsTextView.setText(day.getTotalDebitsFormattedToNearestPound());
		incomingsTextView.setText(day.getTotalCreditsFormattedToNearestPound());
	}

	@Override
	public int getItemViewType(int position) {
		return calendar.getRow(position).getTypeNumber();
	}

	private void styleDayView(View dayView, Day day) {
		switch (day.getType()) {
		case BLANK:
			styleDayView(dayView, R.color.transaction_calendar_background, R.color.transaction_calendar_past_date_text, false);
			break;
		case PAST:
			styleDayView(dayView, R.color.transaction_calendar_past_day_background, R.color.transaction_calendar_past_date_text, false);
			break;
		case TODAY:
			styleDayView(dayView, R.color.transaction_calendar_current_day_background, R.color.transaction_calendar_current_date_text, true);
			break;
		case FUTURE:
			styleDayView(dayView, R.color.transaction_calendar_background, R.color.transaction_calendar_future_date_text, false);
			break;
		}

		if (day.isExpanded()) {
			dayView.setBackgroundColor(getColourFromResourceId(R.color.transaction_calendar_selected_day_background));
		}
	}

	private void styleDayView(View dayView, int backgroundColor, int dateTextColour, boolean isDateHightlighted) {
		dayView.setBackgroundColor(getColourFromResourceId(backgroundColor));
		TextView dateText = (TextView) dayView.findViewById(R.id.date);
		dateText.setTextColor(getColourFromResourceId(dateTextColour));
		View currentDateHighlight = dayView.findViewById(R.id.current_date_highlight);
		if (isDateHightlighted) {
			currentDateHighlight.setVisibility(View.VISIBLE);
		} else {
			currentDateHighlight.setVisibility(View.GONE);
		}
	}

	private void queueDayForExpansion(View dayView) {
		Day day = calendar.getDay((Calendar) dayView.getTag());
		if (day != null && !day.equals(currentExpandedDay) && day.isExpandable()) {
			nextExpandedDay = day;
		} else {
			nextExpandedDay = null;
		}
	}

	public boolean hasExpandedDay() {
		return currentExpandedDay != null;
	}

	private void updateWeekToShowExpandedDay(View weekView) {
		updateWeekToHideExpandedDay();
		expandedWeekView = weekView;
		currentExpandedDay = nextExpandedDay;
		currentExpandedDay.open();
		showExpandedDayAndAnimateIfSupported();
	}

	public void updateWeekToHideExpandedDay() {
		if (currentExpandedDay != null) {
			currentExpandedDay.close();
			closeExpandedDayAndAnimateIfSupported();
		}
		currentExpandedDay = null;
	}
	
	private void showExpandedDayAndAnimateIfSupported() {
		if(canAnimate) {
			showExpandedDayWithAnimation();
		} else {
			notifyDataSetChanged();
		}
	}
	
	private void closeExpandedDayAndAnimateIfSupported() {
		if(canAnimate && !expandAnimationInProgress) {
			closeExpandedDayWithAnimation();
		} else {
			notifyDataSetChanged();
		}
	}

	private void showExpandedDayWithAnimation() {
		notifyDataSetChanged();
		final View view = expandedWeekView.findViewById(R.id.expanded_day);
		configureExpandedDayView(view, new ExpandedDay(currentExpandedDay));
		view.measure(MeasureSpec.makeMeasureSpec(expandedWeekView.getWidth(), MeasureSpec.AT_MOST),
				     MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));		
		int animationHeight = view.getMeasuredHeight();
		HeightChangeAnimation animation = new HeightChangeAnimation(view, 0, animationHeight);
		view.setAnimation(animation);
		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				view.setVisibility(View.VISIBLE);
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				expandAnimationInProgress = false;
			}
		});
	}

	private void closeExpandedDayWithAnimation() {
		final View view = expandedWeekView.findViewById(R.id.expanded_day);
		HeightChangeAnimation animation = new HeightChangeAnimation(view, view.getHeight(), 0);
		view.setAnimation(animation);
		animation.startNow();
		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {}
			
			@Override
			public void onAnimationRepeat(Animation animation) {}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				notifyDataSetChanged();
			}
		});
	}
	

	private boolean isDayQueuedForExpansion() {
		return nextExpandedDay != null;
	}

	private boolean isWeekView(View view) {
		return view.getId() == R.id.transaction_calendar_week;
	}

	private int getColourFromResourceId(int id) {
		return context.getResources().getColor(id);
	}

	/**
	 * Inform the adapter that the user pressed back
	 * 
	 * @return true if the back event was handled, otherwise false
	 */
	public boolean onBackPressed() {
		if (hasExpandedDay()) {
			updateWeekToHideExpandedDay();
			return true;
		}
		return false;
	}
	
	public void onFinishedLoadingData() {
		enableAnimationsIfSupported();
	}
	
	private void enableAnimationsIfSupported() {
		canAnimate = Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB;
	}

	public synchronized void onDayClicked(View dayView) {
		queueDayForExpansion(dayView);
	}

	public synchronized void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
		ListView listView = (ListView) adapterView;
		if (isWeekView(view) && isDayQueuedForExpansion()) {
			expandAnimationInProgress = canAnimate;
			updateWeekToShowExpandedDay(view);
			listView.setSelection(position);
		} else {
			updateWeekToHideExpandedDay();
		}
		nextExpandedDay = null;
	}

	private class DayTouchHandler implements OnTouchListener {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				onDayClicked(v);
			}
			return false;
		}
	}

	private static class HeightChangeAnimation extends Animation {

		private View view;
		private int start;
		private int difference;

		public HeightChangeAnimation(View view, int start, int finish) {
			super();
			this.view = view;
			this.start = start;
			this.difference = finish - start;
			setDuration(400);
			setFillAfter(true);
		}

		@Override
		protected void applyTransformation(float interpolatedTime, Transformation t) {
			int height = (int) (start + (difference * interpolatedTime));
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height);
			view.setLayoutParams(params);
		}
	}
}
