package com.tescobank.mobile.ui.transactioncalendar;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import com.tescobank.mobile.format.DataFormatter;

public class Header {

	private DataFormatter formatter;

	private BigDecimal inAmount;
	private BigDecimal outAmount;
	private BigDecimal differenceAmount;

	private String monthLabel;
	private String inAmountLabel;
	private String outAmountLabel;
	private String differenceAmountLabel;

	public Header(Calendar month) {
		super();
		this.formatter = new DataFormatter();
		this.monthLabel = new SimpleDateFormat("MMMM yyyy", Locale.UK).format(month.getTime());
		this.inAmount = BigDecimal.ZERO;
		this.outAmount = BigDecimal.ZERO;
		this.differenceAmount = BigDecimal.ZERO;
	}

	private void updateAmountLabels() {
		inAmountLabel = formatter.formatCurrencyNoDecimalPlaces(inAmount);
		outAmountLabel = formatter.formatCurrencyNoDecimalPlaces(outAmount);
		differenceAmountLabel = formatter.formatCurrencyNoDecimalPlaces(differenceAmount);
	}

	public void addCredit(BigDecimal credit) {
		inAmount = inAmount.add(credit.abs());
		differenceAmount = inAmount.subtract(outAmount).abs();
		updateAmountLabels();
	}

	public void addDebit(BigDecimal debit) {
		outAmount = outAmount.add(debit.abs());
		differenceAmount = inAmount.subtract(outAmount).abs();
		updateAmountLabels();
	}

	public String getMonthLabel() {
		return monthLabel;
	}

	public String getInAmountLabel() {
		return inAmountLabel;
	}

	public String getOutAmountLabel() {
		return outAmountLabel;
	}

	public String getDifferenceAmountLabel() {
		return differenceAmountLabel;
	}

	public boolean isPositiveOrEqualDifference() {
		return inAmount.compareTo(outAmount) >= 0;
	}

	public boolean isNegativeOrEqualDifference() {
		return inAmount.compareTo(outAmount) <= 0;
	}

	public float getInAmountRatio() {
		
		if (inAmount.intValue() == 0) {
			return 0.0f;
		}

		if (isPositiveOrEqualDifference()) {
			return 1.0f;
		}

		float inRatio = inAmount.floatValue() / outAmount.floatValue();
		return !Float.isInfinite(inRatio) ? inRatio : 0;
	}

	public float getOutAmountRatio() {
		
		if (outAmount.intValue() == 0) {
			return 0.0f;
		}

		if (isNegativeOrEqualDifference()) {
			return 1.0f;
		}
 		float outRatio = outAmount.floatValue() / inAmount.floatValue();
		return !Float.isInfinite(outRatio) ? outRatio : 0;
	}
	
	public float getDiffAmountRatio() {
		return Math.abs(getInAmountRatio() - getOutAmountRatio());
	}
}
