package com.tescobank.mobile.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;

public class TescoAlertDialog {

	public static class Builder extends AlertDialog.Builder {

		private TescoActivity activity;
		private String title;
		private String message;

		public Builder(TescoActivity activity) {
			super(activity);
			this.activity = activity;
		}

		@Override
		public android.app.AlertDialog.Builder setTitle(CharSequence title) {
			this.title = title.toString();
			return super.setTitle(title);
		}

		@Override
		public android.app.AlertDialog.Builder setTitle(int titleId) {
			this.title = activity.getString(titleId);
			return super.setTitle(titleId);
		}

		@Override
		public android.app.AlertDialog.Builder setMessage(CharSequence message) {
			this.message = message.toString();
			return super.setMessage(message);
		}

		@Override
		public android.app.AlertDialog.Builder setMessage(int messageId) {
			this.message = activity.getString(messageId);
			return super.setMessage(messageId);
		}

		@Override
		public AlertDialog create() {
			AlertDialog dialog = super.create();
			dialog.setOnShowListener(new OnShowListener() {
				@Override
				public void onShow(DialogInterface dialog) {
					activity.getApplicationState().getAnalyticsTracker().trackAlertDisplayed(title, message);
				}
			});
			return dialog;
		}

	}

}
