package com.tescobank.mobile.ui.calendar;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.TypefaceCache;

public final class CalendarMenuDayDrawer {

	private int textSize;
	private int yOffset;

	public CalendarMenuDayDrawer(int textSize, int yOffset) {
		this.textSize = textSize;
		this.yOffset = yOffset;
	}

	/**
	 * Adds today's day number (e.g '15' for the 15th of March) to a menu item. 
	 * Note this method expects the menu item to have a bitmap drawable icon, otherwise no action is taken
	 */
	public void drawTodaysDay(Resources resources, MenuItem menuItem) {
		Drawable drawable = menuItem.getIcon();
		if (drawable instanceof BitmapDrawable) {
			Bitmap icon = BitmapDrawable.class.cast(drawable).getBitmap();
			Bitmap iconWithText = drawTodaysDay(resources, icon);
			menuItem.setIcon(new BitmapDrawable(resources, iconWithText));
		}
	}

	private Bitmap drawTodaysDay(Resources resources, Bitmap icon) {
		Bitmap iconWithText = Bitmap.createBitmap(icon.getWidth(), icon.getHeight(), icon.getConfig());
		Canvas canvas = new Canvas(iconWithText);
		canvas.drawBitmap(icon, 0, 0, new Paint());
		Paint style = generateStyle(resources);
		float xPosition = icon.getWidth() / 2;
		float yPosition = ((icon.getHeight() - style.ascent()) / 2) + yOffset * resources.getDisplayMetrics().density;
		canvas.drawText(getTodaysDay(), xPosition, yPosition, style);
		canvas.save();
		return iconWithText;
	}

	private Paint generateStyle(Resources resources) {
		Paint paint = new Paint();
		paint.setTextSize(textSize * resources.getDisplayMetrics().density);
		paint.setTypeface(TypefaceCache.get(resources.getAssets(), "TescoBd"));
		paint.setAntiAlias(true);
		paint.setStyle(Style.FILL);
		paint.setTextAlign(Align.CENTER);
		paint.setColor(resources.getColor(R.color.white));
		return paint;
	}

	private String getTodaysDay() {
		GregorianCalendar cal = new GregorianCalendar();
		return String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
	}
}
