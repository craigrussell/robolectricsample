package com.tescobank.mobile.ui.regpay;

import com.tescobank.mobile.model.product.Product;

public interface ProductProvider {
	
	Product findByAccountNumber(String accountNumber);

}
