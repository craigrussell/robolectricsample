package com.tescobank.mobile.ui.regpay;

import static com.tescobank.mobile.api.payment.RetrieveRegularPaymentsRequest.RegularPaymentType.standingOrder;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.payment.RetrieveRegularPaymentsRequest;
import com.tescobank.mobile.api.payment.RetrieveRegularPaymentsRequest.RegularPaymentType;
import com.tescobank.mobile.api.payment.RetrieveRegularPaymentsResponse;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.prodheader.ProductHeaderViewFactory;
import com.tescobank.mobile.ui.regpay.RegularPaymentsAdapter.SortOrder;

public class RegularPaymentsActivity extends TescoActivity {

	public static final String REGULAR_PAYMENTS_PRODUCT_DETAILS_EXTRA = "PRODUCT_DETAILS_EXTRA";

	private ProductDetails productDetails;

	private RegularPaymentsAdapter adapter;

	private ErrorResponseWrapper lastError;

	private ProductProvider productProvider;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_regular_payments);

		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		productDetails = (ProductDetails) getIntent().getExtras().getSerializable(REGULAR_PAYMENTS_PRODUCT_DETAILS_EXTRA);
		productProvider = new DefaultProductProvider(getApplicationState());

		configureHeader();
		configureListView();
		applyInitialSort();
		startRequiredAPICalls();
		trackAnalytics();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.regular_payments, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.menu_item_regular_payments_sort_alphabetically:
			applySort(RegularPaymentsAdapter.SortOrder.AtoZ);
			return true;

		case R.id.menu_item_regular_payments_sort_amount:
			applySort(RegularPaymentsAdapter.SortOrder.Amount);
			return true;

		case R.id.menu_item_regular_payments_sort_date:
			applySort(RegularPaymentsAdapter.SortOrder.Date);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void applyInitialSort() {
		SortOrder initial = getApplicationState().getRegularPaymentSortOrderForProduct(productDetails);
		if (initial == null) {
			initial = SortOrder.AtoZ;
		}
		applySort(initial);
	}

	private void applySort(SortOrder sort) {
		getApplicationState().setRegularPaymentSortOrderForProduct(sort, productDetails);
		adapter.sortBy(sort);
	}

	@Override
	protected void onRetryPressed() {
		startRequiredAPICalls();
	}

	@Override
	public boolean backFromRetryErrorShouldRetry() {
		return false;
	}

	private void trackAnalytics() {
		getApplicationState().getAnalyticsTracker().trackViewRegularPayments(productDetails);
	}

	private void startRequiredAPICalls() {
		lastError = null;
		if (adapter.getStandingOrders() == null) {
			startGetRegularPaymentsRequest(standingOrder);
		}
		if (adapter.getDirectDebits() == null) {
			startGetRegularPaymentsRequest(RegularPaymentType.directDebit);
		}
		if (adapter.getOneOffPayments() == null) {
			startGetRegularPaymentsRequest(RegularPaymentType.futureDatedPayment);
		}
	}

	private void startGetRegularPaymentsRequest(final RegularPaymentType type) {
		RetrieveRegularPaymentsRequest request = new RetrieveRegularPaymentsRequest() {
			@Override
			public void onErrorResponse(ErrorResponseWrapper errorResponse) {
				if (null == lastError) {
					synchronized (productDetails) {
						if (null == lastError) {
							getApplicationState().getRestRequestProcessor().cancelAllRequests();
							lastError = errorResponse;
							handleErrorResponse(errorResponse);
						}
					}
				}
			}

			@Override
			public void onResponse(RetrieveRegularPaymentsResponse response) {
				handleResponseForType(response, type);
			}
		};
		request.setProductCategory(productDetails.getProductCategory());
		request.setProductID(productDetails.getProductId());
		request.setRegularPaymentType(type);
		request.setTbID(getApplicationState().getTbId());

		getApplicationState().getRestRequestProcessor().processRequest(request);
	}

	private void handleResponseForType(RetrieveRegularPaymentsResponse response, RegularPaymentType type) {
		switch (type) {
		case standingOrder:
			adapter.setStandingOrders(response.getRegularPayments());
			break;

		case directDebit:
			adapter.setDirectDebits(response.getRegularPayments());
			break;

		case futureDatedPayment:
			adapter.setOneOffPayments(response.getRegularPayments());
			break;
		}
	}

	private void configureListView() {
		ExpandableListView list = ExpandableListView.class.cast(findViewById(R.id.list));
		adapter = new RegularPaymentsAdapter(this, productProvider, new DefaultListItemViewConfigurator(this, productProvider));
		list.setAdapter(adapter);
		list.setGroupIndicator(null);
		list.expandGroup(0);
		list.expandGroup(1);
		list.expandGroup(2);
	}

	private void configureHeader() {
		ViewGroup headerContainer = ViewGroup.class.cast(findViewById(R.id.transfer_header));
		ProductHeaderViewFactory productHeaderViewFactory = new ProductHeaderViewFactory(productDetails, this, headerContainer);
		productHeaderViewFactory.createHeaderView();
	}
}
