package com.tescobank.mobile.ui.regpay;

import java.math.BigDecimal;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.payment.RetrieveRegularPaymentsResponse.RegularPayment;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.ui.HideAndShowProgressIndicator;

public class DefaultListItemViewConfigurator implements ListItemViewConfigurator {

	private static final String EMPTY_STRING = "";
	private ProductProvider productProvider;
	private DataFormatter dataFormatter = new DataFormatter();
	private Context context;

	DefaultListItemViewConfigurator(Context context, ProductProvider productProvider) {
		this.productProvider = productProvider;
		this.context = context;
	}

	@Override
	public void showProgress(View view) {
		new HideAndShowProgressIndicator(view).showInProgressIndicator();
		view.findViewById(R.id.progressIndicator).setVisibility(View.VISIBLE);
		view.findViewById(R.id.no_payments).setVisibility(View.INVISIBLE);
		view.findViewById(R.id.payment_item).setVisibility(View.INVISIBLE);
	}

	@Override
	public void showNoDirectDebitsMessage(View view) {
		TextView.class.cast(view.findViewById(R.id.no_payments)).setText(R.string.regular_payments_no_dd);
		view.findViewById(R.id.progressIndicator).setVisibility(View.INVISIBLE);
		view.findViewById(R.id.no_payments).setVisibility(View.VISIBLE);
		view.findViewById(R.id.payment_item).setVisibility(View.INVISIBLE);
	}

	@Override
	public void showNoStandingOrdersMessage(View view) {
		TextView.class.cast(view.findViewById(R.id.no_payments)).setText(R.string.regular_payments_no_so);
		view.findViewById(R.id.progressIndicator).setVisibility(View.INVISIBLE);
		view.findViewById(R.id.no_payments).setVisibility(View.VISIBLE);
		view.findViewById(R.id.payment_item).setVisibility(View.INVISIBLE);
	}

	@Override
	public void showNoOneOffPaymentsMessage(View view) {
		TextView.class.cast(view.findViewById(R.id.no_payments)).setText(R.string.regular_payments_no_oop);
		view.findViewById(R.id.progressIndicator).setVisibility(View.INVISIBLE);
		view.findViewById(R.id.no_payments).setVisibility(View.VISIBLE);
		view.findViewById(R.id.payment_item).setVisibility(View.INVISIBLE);
	}

	private void showPaymentDetails(View view) {
		view.findViewById(R.id.progressIndicator).setVisibility(View.INVISIBLE);
		view.findViewById(R.id.no_payments).setVisibility(View.INVISIBLE);
		view.findViewById(R.id.payment_item).setVisibility(View.VISIBLE);
	}

	@Override
	public void showDirectDebitDetails(View view, RegularPayment directDebit) {
		String originator = directDebit.getOriginatorName();
		BigDecimal amountBD = directDebit.getLastPaymentAmount();
		String amountString = dataFormatter.formatCurrency(amountBD);
		String lastPaymentDate = getLastPaymentDate(directDebit);

		ImageView.class.cast(view.findViewById(R.id.icon)).setImageResource(R.drawable.ic_regular_payment_dd);
		TextView.class.cast(view.findViewById(R.id.title)).setText(originator);
		TextView.class.cast(view.findViewById(R.id.payment_amount)).setText(EMPTY_STRING);
		if (showDirectDebitLastAmount(directDebit)) {
			TextView.class.cast(view.findViewById(R.id.info_text)).setText(context.getString(R.string.regular_payment_dd_last_amount, amountString));
		} else if (showDirectDebitLastDate(directDebit)) {
			TextView.class.cast(view.findViewById(R.id.info_text)).setText(context.getString(R.string.regular_payment_dd_last_date, lastPaymentDate));
		} else if (showDirectDebitLastAmountAndDate(directDebit)) {
			TextView.class.cast(view.findViewById(R.id.info_text)).setText(context.getString(R.string.regular_payment_dd_last_amount_and_date, amountString, lastPaymentDate));
		}

		showPaymentDetails(view);
	}
	
	private String getLastPaymentDate(RegularPayment directDebit) {
		return directDebit.getLastPaymentDate() != null ? dataFormatter.formatDateAsDayFullMonthAndYear(dataFormatter.parseDate(directDebit.getLastPaymentDate())) : EMPTY_STRING;
	}
	
	private boolean showDirectDebitLastAmount(RegularPayment directDebit) {
		return (directDebit.getLastPaymentDate() == null) && (directDebit.getLastPaymentAmount() != null);
	}
	
	private boolean showDirectDebitLastDate(RegularPayment directDebit) {
		return (directDebit.getLastPaymentDate() != null) && (directDebit.getLastPaymentAmount() == null);
	}
	
	private boolean showDirectDebitLastAmountAndDate(RegularPayment directDebit) {
		return (directDebit.getLastPaymentDate() != null) && (directDebit.getLastPaymentAmount() != null);
	}

	@Override
	public void showStandingOrderDetails(View view, RegularPayment standingOrder) {
		BigDecimal amountBD = standingOrder.getCurrentAmount();
		String amountString = dataFormatter.formatCurrency(amountBD);
		String nextPaymentDate = standingOrder.getNextPaymentDate() != null ? dataFormatter.formatDateAsDayFullMonthAndYear(dataFormatter.parseDate(standingOrder.getNextPaymentDate())) : EMPTY_STRING;

		ImageView.class.cast(view.findViewById(R.id.icon)).setImageResource(R.drawable.ic_regular_payment_standing_order);
		TextView.class.cast(view.findViewById(R.id.title)).setText(PayeeNameBuilder.build(standingOrder, productProvider));
		TextView.class.cast(view.findViewById(R.id.payment_amount)).setText(amountString);
		if (standingOrder.getNextPaymentDate() != null) {
			TextView.class.cast(view.findViewById(R.id.info_text)).setText(context.getString(R.string.regular_payment_standing_order_next_payment_date, nextPaymentDate));
		}

		showPaymentDetails(view);
	}

	@Override
	public void showOneOffPaymentDetails(View view, RegularPayment oneOffPayment) {
		BigDecimal amountBD = oneOffPayment.getCurrentAmount();
		String amountString = dataFormatter.formatCurrency(amountBD);
		String nextPaymentDate = oneOffPayment.getNextPaymentDate() != null ? dataFormatter.formatDateAsDayFullMonthAndYear(dataFormatter.parseDate(oneOffPayment.getNextPaymentDate())) : EMPTY_STRING;

		ImageView.class.cast(view.findViewById(R.id.icon)).setImageResource(R.drawable.ic_regular_payment_one_off);
		TextView.class.cast(view.findViewById(R.id.title)).setText(PayeeNameBuilder.build(oneOffPayment, productProvider));
		TextView.class.cast(view.findViewById(R.id.payment_amount)).setText(amountString);
		if (oneOffPayment.getNextPaymentDate() != null) {
			TextView.class.cast(view.findViewById(R.id.info_text)).setText(context.getString(R.string.regular_payment_one_off_date, nextPaymentDate));
		}

		showPaymentDetails(view);
	}

}
