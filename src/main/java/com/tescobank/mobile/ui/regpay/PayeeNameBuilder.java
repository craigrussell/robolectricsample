package com.tescobank.mobile.ui.regpay;

import com.tescobank.mobile.api.payment.RetrieveRegularPaymentsResponse.RegularPayment;
import com.tescobank.mobile.api.payment.RetrieveRegularPaymentsResponse.RegularPayment.Transactee;
import com.tescobank.mobile.model.product.Product;

public final class PayeeNameBuilder {
	
	private final static String INTERNAL = "INTERNAL";
	
	public static String build(RegularPayment payment, ProductProvider provider) {

		if (INTERNAL.equals(payment.getAccountType())) {
			return getProductName(payment, provider);
		} else {
			return getTransacteeName(payment);
		}
	}
	
	private static String getProductName(RegularPayment payment, ProductProvider provider) {
		Product product = provider.findByAccountNumber(payment.getTransactee().getBankAccount().getAccountNumber());
		return (product == null) ? "" : product.getProductName();
	}
	
	private static String getTransacteeName(RegularPayment payment) {
		Transactee t = payment.getTransactee();
		if(null == t) {
			return "";
		}
		
		String nickName = t.getTransacteeNickName();
		return ((nickName != null) && !nickName.isEmpty()) ? nickName : t.getTransacteeName();
	}
}
