package com.tescobank.mobile.ui.regpay;

import java.util.List;

import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.model.product.Product;

public class DefaultProductProvider implements ProductProvider {

	private List<Product> products;

	public DefaultProductProvider(ApplicationState applicationState) {
		super();
		products = applicationState.getProducts();
	}

	@Override
	public Product findByAccountNumber(String accountNumber) {
		for (Product product : products) {
			if ((product.getAccountNumber() != null) && product.getAccountNumber().equals(accountNumber)) {
					return product;
			}
		}
		return null;
	}

}
