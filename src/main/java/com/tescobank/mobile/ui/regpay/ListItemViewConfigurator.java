package com.tescobank.mobile.ui.regpay;

import android.view.View;

import com.tescobank.mobile.api.payment.RetrieveRegularPaymentsResponse.RegularPayment;

public interface ListItemViewConfigurator {

	void showProgress(View view);

	void showNoOneOffPaymentsMessage(View view);

	void showNoDirectDebitsMessage(View view);

	void showNoStandingOrdersMessage(View view);

	void showDirectDebitDetails(View view, RegularPayment payment);

	void showStandingOrderDetails(View view, RegularPayment standingOrder);

	void showOneOffPaymentDetails(View view, RegularPayment oneOffPayment);

}