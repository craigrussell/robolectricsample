package com.tescobank.mobile.ui.regpay;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.payment.RetrieveRegularPaymentsResponse.RegularPayment;

public class RegularPaymentsAdapter extends BaseExpandableListAdapter {

	private static final int GROUP_COUNT = 3;

	private final LayoutInflater inflater;
	private final ListItemViewConfigurator itemConfigurator;
	private List<RegularPayment> standingOrders;
	private List<RegularPayment> directDebits;
	private List<RegularPayment> oneOffPayments;
	private ProductProvider productProvider;
	private SortOrder sort = SortOrder.Date;

	public RegularPaymentsAdapter(Context context, ProductProvider productProvider, ListItemViewConfigurator itemConfigurator) {
		this.productProvider = productProvider;
		this.inflater = LayoutInflater.from(context);
		this.itemConfigurator = itemConfigurator;
	}

	public void setStandingOrders(List<RegularPayment> standingOrders) {
		this.standingOrders = standingOrders;
		sortStandingOrders();
		notifyDataSetChanged();
	}

	public List<RegularPayment> getStandingOrders() {
		return standingOrders;
	}

	public void setDirectDebits(List<RegularPayment> directDebits) {
		this.directDebits = directDebits;
		sortDirectDebits();
		notifyDataSetChanged();
	}

	public List<RegularPayment> getDirectDebits() {
		return directDebits;
	}

	public void setOneOffPayments(List<RegularPayment> regularPayments) {
		this.oneOffPayments = regularPayments;
		sortOneOffPayments();
		notifyDataSetChanged();
	}

	public List<RegularPayment> getOneOffPayments() {
		return oneOffPayments;
	}

	@Override
	public int getGroupCount() {
		return GROUP_COUNT;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		State state = getState(groupPosition);
		switch (groupPosition) {
		case 0:
			return getChildrenCountForGoup0(state);
		case 1:
			return getChildrenCountForGoup1(state);
		case 2:
			return getChildrenCountForGoup2(state);
		default:
			return 0;
		}
	}

	private int getChildrenCountForGoup0(State state) {
		return State.HasData.equals(state) ? standingOrders.size() : 1;
	}

	private int getChildrenCountForGoup1(State state) {
		return State.HasData.equals(state) ? directDebits.size() : 1;
	}

	private int getChildrenCountForGoup2(State state) {
		return State.HasData.equals(state) ? oneOffPayments.size() : 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		switch (groupPosition) {
		case 0:
			return R.string.regular_payments_group_header_standing_orders;

		case 1:
			return R.string.regular_payments_group_header_direct_debits;

		case 2:
			return R.string.regular_payments_group_header_one_off_payment;
		}
		return null;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		switch (groupPosition) {
		case 0:
			return standingOrders.get(childPosition);

		case 1:
			return directDebits.get(childPosition);

		case 2:
			return oneOffPayments.get(childPosition);
		}
		return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		TextView textView = TextView.class.cast(inflater.inflate(R.layout.list_item_regular_payments_group_header, null));
		textView.setText(Integer.class.cast(getGroup(groupPosition)));
		return textView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

		View theView = getChildView(convertView);

		State state = getState(groupPosition);
		switch (state) {

		case NoData:
			getChildViewForNoData(groupPosition, theView);
			break;

		case InProgress:
			itemConfigurator.showProgress(theView);
			break;

		case HasData:
			configureViewWithData(groupPosition, childPosition, theView);
			break;
		}

		return theView;
	}

	private View getChildView(View convertView) {
		return (convertView != null) ? convertView : inflater.inflate(R.layout.list_item_regular_payment, null);
	}

	private void getChildViewForNoData(int groupPosition, View theView) {
		switch (groupPosition) {
		case 0:
			itemConfigurator.showNoStandingOrdersMessage(theView);
			break;
		case 1:
			itemConfigurator.showNoDirectDebitsMessage(theView);
			break;
		case 2:
			itemConfigurator.showNoOneOffPaymentsMessage(theView);
			break;
		}
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

	public void sortBy(SortOrder sort) {
		this.sort = sort;

		sortStandingOrders();
		sortDirectDebits();
		sortOneOffPayments();

		notifyDataSetChanged();
	}

	private void sortOneOffPayments() {

		switch (sort) {
		case Date:
			sortOneOffPaymentsByDate();
			break;

		case Amount:
			sortOneOffPaymentsByAmount();
			break;

		case AtoZ:
			sortOneOffPaymentsByName();
			break;

		default:
			break;
		}

	}

	private void sortDirectDebits() {
		switch (sort) {
		case Date:
			sortDirectDebitsByDate();
			break;

		case Amount:
			sortDirectDebitsByAmount();
			break;

		case AtoZ:
			sortDirectDebitsByName();
			break;

		default:
			break;
		}
	}

	private void sortStandingOrders() {
		switch (sort) {
		case Date:
			sortStandingOrdersByDate();
			break;

		case Amount:
			sortStandingOrdersByAmount();
			break;

		case AtoZ:
			sortStandingOrdersByName();
			break;

		default:
			break;
		}

	}

	private void sortOneOffPaymentsByName() {
		sortNonDirectDebitPaymentsByName(oneOffPayments);
	}

	private void sortNonDirectDebitPaymentsByName(List<RegularPayment> payments) {
		if (payments == null) {
			return;
		}

		Collections.sort(payments, new PaymentNameComparator() {
			@Override
			String getName(RegularPayment payment) {
				return PayeeNameBuilder.build(payment, productProvider);
			}
		});

	}

	private void sortDirectDebitsByName() {
		if (directDebits == null) {
			return;
		}

		Collections.sort(directDebits, new PaymentNameComparator() {
			@Override
			String getName(RegularPayment payment) {
				return payment.getOriginatorName();
			}
		});
	}

	private void sortStandingOrdersByName() {
		sortNonDirectDebitPaymentsByName(standingOrders);
	}

	private void sortDirectDebitsByAmount() {
		if (directDebits == null) {
			return;
		}

		Collections.sort(directDebits, new PaymentAmountComparator() {

			@Override
			BigDecimal getAmount(RegularPayment payment) {
				return payment.getLastPaymentAmount();
			}

			@Override
			String getName(RegularPayment payment) {
				return payment.getOriginatorName();
			}

		});
	}

	private void sortNonDirectDebitsByAmount(List<RegularPayment> payments) {
		if (payments == null) {
			return;
		}

		Collections.sort(payments, new PaymentAmountComparator() {

			@Override
			String getName(RegularPayment payment) {
				return PayeeNameBuilder.build(payment, productProvider);
			}

			@Override
			BigDecimal getAmount(RegularPayment payment) {
				return payment.getCurrentAmount();
			}

		});

	}

	private void sortOneOffPaymentsByAmount() {
		sortNonDirectDebitsByAmount(oneOffPayments);
	}

	private void sortStandingOrdersByAmount() {
		sortNonDirectDebitsByAmount(standingOrders);
	}

	private void sortOneOffPaymentsByDate() {
		sortNonDirectDebitPaymentsByDate(oneOffPayments);
	}

	private void sortDirectDebitsByDate() {
		if (directDebits == null) {
			return;
		}

		Collections.sort(directDebits, new PaymentDateComparator() {
			@Override
			String getDate(RegularPayment payment) {
				return payment.getLastPaymentDate();
			}

			@Override
			String getName(RegularPayment payment) {
				return payment.getOriginatorName();
			}
		});
	}

	private void sortStandingOrdersByDate() {
		sortNonDirectDebitPaymentsByDate(standingOrders);
	}

	private void sortNonDirectDebitPaymentsByDate(List<RegularPayment> payments) {
		if (payments == null) {
			return;
		}

		Collections.sort(payments, new PaymentDateComparator() {
			@Override
			String getDate(RegularPayment payment) {
				return payment.getNextPaymentDate();
			}

			@Override
			String getName(RegularPayment payment) {
				return PayeeNameBuilder.build(payment, productProvider);
			}
		});
	}

	private void configureViewWithData(int groupPosition, int childPosition, View view) {

		switch (groupPosition) {
		case 0:
			itemConfigurator.showStandingOrderDetails(view, standingOrders.get(childPosition));
			break;

		case 1:
			itemConfigurator.showDirectDebitDetails(view, directDebits.get(childPosition));
			break;

		case 2:
			itemConfigurator.showOneOffPaymentDetails(view, oneOffPayments.get(childPosition));
			break;
		}
	}

	private State dataToState(List<RegularPayment> data) {
		if (data == null) {
			return State.InProgress;
		}
		return data.isEmpty() ? State.NoData : State.HasData;
	}

	private State getState(int groupPosition) {
		switch (groupPosition) {

		case 0:
			return dataToState(standingOrders);

		case 1:
			return dataToState(directDebits);

		case 2:
			return dataToState(oneOffPayments);
		}
		return null;
	}

	public static enum State {
		InProgress, NoData, HasData
	}

	/** Supported sort orders. */
	public static enum SortOrder {
		Date("DATE", R.string.regular_payments_sort_date), AtoZ("A_TO_Z", R.string.regular_payments_sort_a_z), Amount("AMOUNT", R.string.regular_payments_sort_amount);

		/**
		 * The valueOf(string) approach throws an exception if it can't be
		 * found. This will also, gracefully, survive an app upgrade where the
		 * enum constants change.
		 */
		private final String persistenceValue;

		private final int res;

		private SortOrder(String persistenceValue, int res) {
			this.persistenceValue = persistenceValue;
			this.res = res;
		}

		public String persistenceValue() {
			return persistenceValue;
		}

		public static SortOrder fromPersistenceValue(String value) {
			for (SortOrder sortOrder : values()) {
				if (sortOrder.persistenceValue.equals(value)) {
					return sortOrder;
				}
			}
			return null;
		}

		public int getDisplayResource() {
			return res;
		}
	}

	abstract static class PaymentAmountComparator implements Comparator<RegularPayment> {

		@Override
		public int compare(RegularPayment lhs, RegularPayment rhs) {

			int compareByAmount = compareByAmount(lhs, rhs);
			if (compareByAmount != 0) {
				return compareByAmount;
			}

			return compareByName(lhs, rhs);
		}

		abstract String getName(RegularPayment payment);

		abstract BigDecimal getAmount(RegularPayment payment);

		private int compareByAmount(RegularPayment lhs, RegularPayment rhs) {
			BigDecimal lhsAmount = getAmount(lhs);
			BigDecimal rhsAmount = getAmount(rhs);
			if (bothNull(lhsAmount, rhsAmount)) {
				return 0;
			} else if (lhsAmount == null) {
				return 1;
			} else if (rhsAmount == null) {
				// forces nulls to bottom of the list
				return -1;
			} else {
				return rhsAmount.compareTo(lhsAmount);
			}
		}

		private int compareByName(RegularPayment lhs, RegularPayment rhs) {
			String lhsName = getName(lhs);
			String rhsName = getName(rhs);
			if (bothNull(lhsName, rhsName)) {
				return 0;
			} else if (lhsName == null) {
				return 1;
			} else if (rhsName == null) {
				// forces nulls to bottom of the list
				return -1;
			}

			String leftName = String.valueOf(lhsName);
			String rightName = String.valueOf(rhsName);

			return leftName.compareTo(rightName);
		}

		private boolean bothNull(Object lhs, Object rhs) {
			return (lhs == null) && (rhs == null);
		}
	}

	abstract static class PaymentDateComparator implements Comparator<RegularPayment> {

		@Override
		public int compare(RegularPayment lhs, RegularPayment rhs) {

			int compareByDate = compareByDate(lhs, rhs);
			if (compareByDate != 0) {
				return compareByDate;
			}

			return compareByName(lhs, rhs);
		}

		abstract String getName(RegularPayment payment);

		abstract String getDate(RegularPayment payment);

		private int compareByDate(RegularPayment lhs, RegularPayment rhs) {
			String lhsDate = getDate(lhs);
			String rhsDate = getDate(rhs);
			if (bothNull(lhsDate, rhsDate)) {
				return 0;
			} else if (lhsDate == null) {
				return 1;
			} else if (rhsDate == null) {
				// forces nulls to bottom of the list
				return -1;
			} else {
				return rhsDate.compareTo(lhsDate);
			}
		}

		private int compareByName(RegularPayment lhs, RegularPayment rhs) {
			String lhsName = getName(lhs);
			String rhsName = getName(rhs);
			if (bothNull(lhsName, rhsName)) {
				return 0;
			} else if (lhsName == null) {
				return 1;
			} else if (rhsName == null) {
				// forces nulls to bottom of the list
				return -1;
			}

			String leftName = String.valueOf(lhsName);
			String rightName = String.valueOf(rhsName);

			return leftName.compareTo(rightName);
		}

		private boolean bothNull(String lhs, String rhs) {
			return (lhs == null) && (rhs == null);
		}
	}

	abstract static class PaymentNameComparator implements Comparator<RegularPayment> {

		@Override
		public int compare(RegularPayment lhs, RegularPayment rhs) {
			String lhsName = getName(lhs);
			String rhsName = getName(rhs);
			if (bothNull(lhsName, rhsName)) {
				return 0;
			} else if (empty(lhsName)) {
				return 1;
			} else if (empty(rhsName)) {
				return -1;
			} else {
				return lhsName.compareTo(rhsName);
			}
		}

		abstract String getName(RegularPayment payment);

		private boolean bothNull(String lhs, String rhs) {
			return (lhs == null) && (rhs == null);
		}

		private boolean empty(String name) {
			return (name == null) || "".equals(name);
		}
	}
}
