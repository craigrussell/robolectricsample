package com.tescobank.mobile.ui.carousel;

import static java.lang.String.format;
import static java.lang.String.valueOf;
import android.graphics.drawable.AnimationDrawable;
import android.location.Location;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.storefinder.Store;
import com.tescobank.mobile.application.TescoLocationProvider;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoFragment;
import com.tescobank.mobile.ui.TescoTextView;

public class ATMDisplayer {
	private static final float ZOOM = 12.0f;
	private static final String EMPTY_STRING = "";
	private View progressIndicator;
	private AnimationDrawable progressIndicatorAnimation;
	private TescoTextView nearest;
	private TescoTextView title;
	private TescoTextView content;
	private TescoActivity activity;
	private SupportMapFragment mapFragment;
	private TescoFragment fragment;
	private boolean googleServicesAreEnabled;
	private GoogleMap map;
	private ImageView mapImageView;
	private TescoLocationProvider locationProvider;
	private Marker currentLocation;
	private View mapContainer;

	public ATMDisplayer(TescoActivity activity, TescoFragment fragment) {
		this.activity = activity;
		this.fragment = fragment;	
	}

	protected View createView(LayoutInflater inflater, ViewGroup container, int layoutId) {
		View view = inflater.inflate(layoutId, container, false);
		progressIndicator = (View) view.findViewById(R.id.progressIndicator);
		progressIndicator.setBackgroundResource(R.drawable.tesco_spinner);
		progressIndicatorAnimation = (AnimationDrawable) progressIndicator.getBackground();
		mapImageView = (ImageView) view.findViewById(R.id.image);
		nearest = (TescoTextView) view.findViewById(R.id.atm_nearest);
		title = (TescoTextView) view.findViewById(R.id.atm_title);
		content = (TescoTextView) view.findViewById(R.id.atm_content);
		mapContainer = view.findViewById(R.id.map_container);
		locationProvider = activity.getApplicationState().getLocationProvider();
		return view;
	}
	
	protected void onResume() {
		if (mapFragment == null) {
			mapFragment = SupportMapFragment.newInstance();
			mapFragment.setRetainInstance(false);
		}
		FragmentTransaction fragmentTransaction = fragment.getChildFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.map_container, mapFragment);
		fragmentTransaction.commit();
		fragment.getChildFragmentManager().executePendingTransactions();
		onFragmentResume();
	}
	
	protected void onFragmentResume() {
		hideMap();
		startProgressIndicator();
	    enableGoogleMapServices();
	}

	protected void onStop() {
		if(mapFragment !=null) {
			FragmentTransaction fragmentTransaction = fragment.getChildFragmentManager().beginTransaction();
			fragmentTransaction.remove(mapFragment);
			fragmentTransaction.commitAllowingStateLoss();
			fragment.getChildFragmentManager().executePendingTransactions();	
		}	
	}

	protected void onLocationUserDisabled() {
		hideProgressIndicator();
		
		if(title != null) {
			nearest.setText("");
			title.setText(R.string.carousel_atm_location_user_disabled_title);
			nearest.setVisibility(View.GONE);
		} else {
			nearest.setText(R.string.carousel_atm_location_user_disabled_title);
		}
		content.setText(R.string.carousel_atm_location_user_disabled_message);
		hideMap();
	}

	protected void onLocationSystemDisabled() {
		hideProgressIndicator();
		
		if(title != null) {
			nearest.setText("");
			title.setText(R.string.carousel_atm_location_system_disabled_title);
			nearest.setVisibility(View.GONE);
		} else {
			nearest.setText(R.string.carousel_atm_location_system_disabled_title);
		}
		content.setText(R.string.carousel_atm_location_system_disabled_message);
		hideMap();
	}

	protected void onFailedToRetrieveATMs() {
		hideProgressIndicator();
		if(title != null) {
			nearest.setText("");
			title.setText(R.string.carousel_atm_location_request_failed_title);			
		} else {
			nearest.setText(R.string.carousel_atm_location_request_failed_title);
		}
		content.setText(R.string.carousel_atm_location_request_failed_message);
	}
	
	private boolean canShowGoogleMap() {
		if (googleServicesAreEnabled) {
			map = mapFragment.getMap();
			return map != null;
		}
		return false;
	}
	
	private void enableGoogleMapServices() {
		int serviceResult = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
		switch (serviceResult) {
		case ConnectionResult.SUCCESS:
			handleSuccess();
			break;
		}
	}
	
	private void handleSuccess() {
		if (mapFragment != null) {
			googleServicesAreEnabled = true;
		}
	}
	
	protected void hideMap() {
		if (canShowGoogleMap()) {
			mapImageView.setVisibility(View.VISIBLE);
			mapFragment.getView().setVisibility(View.INVISIBLE);
			mapContainer.setVisibility(View.INVISIBLE);
		}
	}
	
	protected void showMap() {
		if (canShowGoogleMap()) {
			configureMap();
			displayCurrentLocationOnMap();
			
			mapFragment.getView().setVisibility(View.VISIBLE);
			mapContainer.setVisibility(View.VISIBLE);

			mapImageView.setVisibility(View.INVISIBLE);
		} else {
			if(mapFragment != null && mapFragment.isVisible()) {
				mapFragment.getView().setVisibility(View.GONE);
				mapContainer.setVisibility(View.INVISIBLE);
				mapImageView.setVisibility(View.VISIBLE);
			}
		}
	}
	
	private void configureMap() {
		UiSettings uiSettings = map.getUiSettings();
		uiSettings.setAllGesturesEnabled(false);
		uiSettings.setCompassEnabled(false);
		uiSettings.setMyLocationButtonEnabled(false);
		uiSettings.setZoomControlsEnabled(false);
	}
	
	public void displayNearestStoreInformation(Store nearestStore) {
		
		if (nearestStore != null) {
			String distance = valueOf(nearestStore.getStoreMetadata().getDistanceInMiles());
			if(null != title) {
				title.setText(nearestStore.getStoreMetadata().getBranchName());
				nearest.setText(activity.getString(R.string.carousel_atm_nearest_title, distance, EMPTY_STRING));
			} else {
				nearest.setText(Html.fromHtml(activity.getString(R.string.carousel_atm_nearest_title, nearestStore.getStoreMetadata().getBranchName(), distance)));
			}
			content.setText(format(activity.getString(R.string.carousel_atm_content_format), nearestStore.getStoreMetadata().getAddress1(), nearestStore.getStoreMetadata().getTownCity(), nearestStore.getStoreMetadata().getPostcode(), nearestStore.getStoreMetadata().getCountry()));
		}
	}
	
	private void displayCurrentLocationOnMap() {
		Location location = locationProvider.getLocation();
		if (location == null) {
			return;
		}

		if (this.currentLocation != null) {
			this.currentLocation.remove();
			this.currentLocation = null;
		}

		LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
		this.currentLocation = map.addMarker(new MarkerOptions().position(position).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_current_location)));
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, ZOOM));
	}

	protected void setNearestStore(Store nearestStore) {
		showMap();
		displayNearestStoreOnMap(nearestStore);
		displayNearestStoreInformation(nearestStore);
		
	}
	
	private void displayNearestStoreOnMap(Store nearestStore) {
		if ((nearestStore != null) && canShowGoogleMap()) {
			LatLng position = new LatLng(nearestStore.getStoreMetadata().getLatitude(), nearestStore.getStoreMetadata().getLongitude());
			map.addMarker(new MarkerOptions().position(position).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_small)));
		}
	}
	
	public View getProgressIndicator() {
		return progressIndicator;
	}
	
	protected AnimationDrawable getProgressIndicatorAnimation() {
		return progressIndicatorAnimation;
	}
	
	protected void startProgressIndicator() {
		nearest.setVisibility(View.GONE);
		content.setVisibility(View.GONE);
		progressIndicator.setVisibility(View.VISIBLE);
		
		if(null != title) {
			title.setVisibility(View.GONE);
		}
		progressIndicatorAnimation.start();
	}

	protected void hideProgressIndicator() {
		progressIndicator.setVisibility(View.INVISIBLE);
		progressIndicatorAnimation.stop();
		nearest.setVisibility(View.VISIBLE);
		content.setVisibility(View.VISIBLE);
		if(null != title) {
			title.setVisibility(View.VISIBLE);
		}
	}
		
	protected TescoTextView getNearest() {
		return nearest;
	}
	
	protected TescoTextView getContent() {
		return content;
	}
	
	protected TescoTextView getTitle() {
		return title;
	}
}
