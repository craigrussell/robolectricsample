package com.tescobank.mobile.ui.carousel.detail;

import android.content.Context;
import android.view.View;

import com.tescobank.mobile.R;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.carousel.CarouselDataSetter;

/**
 * Displays detailed information for an ISA
 */
public class ISADetail extends SavingsProductDetail {
	
	public ISADetail(ProductDetails product, Context context) {
		super(product, context);
	}
	
	@Override
	protected int getContentViewId() {
		return R.layout.carousel_product_detail_isa_table;
	}
	
	@Override
	protected void populateFromProduct(ProductDetails product) {
		
		CarouselDataSetter carouselDataSetter = getCarouselDataSetter();
		View detailView = getModal();
		
		carouselDataSetter.setBalance(detailView);
		carouselDataSetter.setSubscriptionAmountSubscribed(detailView);
		carouselDataSetter.setSubscriptionAmountAvailable(detailView);
		carouselDataSetter.setSubscriptionLimit(detailView);
		carouselDataSetter.setGrossInterestRate(detailView);
		carouselDataSetter.setTaxYearStart(detailView);
		carouselDataSetter.setTaxYearEnd(detailView);
	}
}
