package com.tescobank.mobile.ui.carousel;

import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_CAROUSEL_LIST_COMPLETED;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoFragment;

public class CarouselListATMFragment extends TescoFragment implements LocationUpdateListener, StoresUpdateListener{
	private View rootView;
	private boolean fragmentBecameVisible;
	private boolean activityBecameVisible;
	private ATMDisplayer atmDisplayer;
	private ATMRetriever atmRetriever;
	private ImageView mapTransparentOverlay;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		atmRetriever = new ATMRetriever(getTescoActivity(), this, this);
		atmDisplayer = new CarouselListATMDisplayer(getTescoActivity(), this);
		rootView = atmDisplayer.createView(inflater,container, R.layout.fragment_carousel_list_atm);
		mapTransparentOverlay = (ImageView) rootView.findViewById(R.id.map_transparent_overlay);
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		activityBecameVisible = true;
		atmDisplayer.onResume();
		atmRetriever.start();
		attachMapClickListener();
	}

	@Override
	public void onPause() {
		onFragmentPause();
		super.onPause();
	}
	
	@Override
	public void onStop() {
		atmDisplayer.onStop();
		super.onStop();
	}

	private void onFragmentResume() {
		if (activityBecameVisible && fragmentBecameVisible) {
			atmDisplayer.onFragmentResume();
			atmRetriever.start();
		}
	}

	private void onFragmentPause() {
		if(atmRetriever != null) {
			atmRetriever.pause();
		}
	}

	/**
	 * Detect when the atm fragment becomes visible/hidden from the user. Cannot
	 * use onResume/onPause() alone as they report on the activity rather than
	 * fragment visibility.
	 */
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		if (isVisibleToUser) {
			fragmentBecameVisible = true;
			onFragmentResume();
		} else {
			onFragmentPause();
		}
		super.setUserVisibleHint(isVisibleToUser);
	}
	
	public void attachMapClickListener() {
		mapTransparentOverlay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(TescoActivity.class.cast(getActivity()), CarouselActivity.class);
				intent.putExtra(CarouselListActivity.PRODUCT_POSITION, 0);
				TescoActivity.class.cast(getActivity()).finishWithResult(RESULT_CAROUSEL_LIST_COMPLETED, intent);
			}
		});
	}
	
	@Override
	public void onRetrievedATMs() {
		atmDisplayer.hideProgressIndicator();
		if (atmRetriever.getStores() != null && atmRetriever.getStores().size() > 0) {
			setNearestStore();
		}
	}
	
	@Override
	public void onFailedToRetrieveATMs() {
		atmDisplayer.onFailedToRetrieveATMs();
	}

	@Override
	public void onLocationIsAvailable() {
		atmDisplayer.showMap();	
	}

	@Override
	public void onLocationSystemDisabled() {
		atmDisplayer.onLocationSystemDisabled();
	}
	
	@Override
	public void onLocationUserDisabled() {
		atmDisplayer.onLocationUserDisabled();
	}
	
	private void setNearestStore() {
		if (atmRetriever.getStores() != null && atmRetriever.getStores().size() > 0) {
			atmDisplayer.setNearestStore(atmRetriever.getStores().get(0));
		}
	}
}
