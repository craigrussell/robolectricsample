package com.tescobank.mobile.ui.carousel;

import static android.view.MotionEvent.ACTION_DOWN;
import static com.tescobank.mobile.ui.ActionBarConfiguration.setToStdHamburger;
import static com.tescobank.mobile.ui.ActivityRequestCode.REQUEST_DEFAULT;
import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_BACK_FROM_RETRY_PRESSED;
import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_CAROUSEL_LIST_COMPLETED;
import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_MOVE_MONEY_SUCCESS;
import static com.tescobank.mobile.ui.CredentialBroadcastAction.CREDENTIAL_OVERLAY_MESSAGE;
import static com.tescobank.mobile.ui.carousel.CarouselListActivity.PRODUCT_POSITION;
import static com.tescobank.mobile.ui.carousel.CarouselReloader.CAROUSEL_PRODUCT_RELOAD_PRODUCT_BROADCAST_EVENT;
import static com.tescobank.mobile.ui.carousel.CarouselReloader.CAROUSEL_PRODUCT_RELOAD_PRODUCT_STATUS_EXTRA;
import static com.tescobank.mobile.ui.carousel.CarouselResultOverlayRenderer.OVERLAY_MESSAGE_KEY;
import static com.tescobank.mobile.ui.fomenu.FlyoutMenuRootButtonBackgroundDrawable.imageDrawableForProduct;
import static com.tescobank.mobile.ui.movemoney.MoneyMovementActivity.SOURCE_PRODUCT_EXTRA;
import static com.tescobank.mobile.ui.paycreditcard.PayCreditCardActivity.PAY_CREDITCARD_PRODUCT_EXTRA;
import static com.tescobank.mobile.ui.regpay.RegularPaymentsActivity.REGULAR_PAYMENTS_PRODUCT_DETAILS_EXTRA;
import static com.tescobank.mobile.ui.saving.SavingActivity.DESTINATION_PRODUCT_EXTRA;
import static com.tescobank.mobile.ui.statement.ViewStatementListActivity.LAUNCHED_FROM_STATEMENT_ARCHIVE_LIST;
import static com.tescobank.mobile.ui.statement.ViewStatementListActivity.STATEMENT_PRODUCT_EXTRA;
import static com.tescobank.mobile.ui.transaction.TransactionsActivity.TRANSACTIONS_PRODUCT_EXTRA;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.PopupWindow;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.maps.model.LatLng;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.authentication.GetPinChallengeRequest;
import com.tescobank.mobile.api.configuration.FeaturesResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.storefinder.Store;
import com.tescobank.mobile.application.TescoLocationProvider;
import com.tescobank.mobile.application.TescoLocationProvider.Status;
import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.rest.processor.RestRequestProcessor;
import com.tescobank.mobile.ui.PayNoPayDisplay;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoAlertDialog;
import com.tescobank.mobile.ui.atm.ATMMapActivity;
import com.tescobank.mobile.ui.atm.ATMProvider;
import com.tescobank.mobile.ui.atm.ATMProvider.ATMRetrievalStatus;
import com.tescobank.mobile.ui.atm.ATMSearchHelper;
import com.tescobank.mobile.ui.auth.ProductDataLoadedListener;
import com.tescobank.mobile.ui.balancepeek.BalancePeekTutorialActivity;
import com.tescobank.mobile.ui.carousel.CarouselReloader.CarouselReloadProductsStatus;
import com.tescobank.mobile.ui.carousel.detail.ProductDetailDisplayer;
import com.tescobank.mobile.ui.errors.ErrorHandler;
import com.tescobank.mobile.ui.fomenu.ABSMenuInflator;
import com.tescobank.mobile.ui.fomenu.FlyoutMenu;
import com.tescobank.mobile.ui.fomenu.FlyoutMenu.TescoMenuInflator;
import com.tescobank.mobile.ui.fomenu.FlyoutMenuRootButtonBackgroundDrawable;
import com.tescobank.mobile.ui.movemoney.payment.PaymentActivity;
import com.tescobank.mobile.ui.movemoney.transfer.TransferActivity;
import com.tescobank.mobile.ui.paycreditcard.PayCreditCardActivity;
import com.tescobank.mobile.ui.regpay.RegularPaymentsActivity;
import com.tescobank.mobile.ui.saving.SavingActivity;
import com.tescobank.mobile.ui.seasonal.SeasonalAnimationManager;
import com.tescobank.mobile.ui.settings.AuthenticatedSettingsFragment;
import com.tescobank.mobile.ui.statement.ViewStatementListActivity;
import com.tescobank.mobile.ui.transaction.TransactionsActivity;
import com.tescobank.mobile.ui.transactioncalendar.CalendarActivity;
import com.tescobank.mobile.ui.tutorial.TooltipId;
import com.tescobank.mobile.ui.tutorial.groups.CarouselTooltipGroup;

public class CarouselActivity extends TescoActivity implements FlyoutMenu.Callback, ErrorHandler, ProductDataLoadedListener {

	private static final String TAG = CarouselActivity.class.getSimpleName();

	private static final String BUNDLE_KEY_DIRECTION_HELPER_SHOWING = "directionHelperDialogShowing";
	private static final String BUNDLE_KEY_STORES = "store";
	private static final String BUNDLE_KEY_STORE_LOCATION = "storeLocation";
	private static final String BUNDLE_KEY_CHANGE_PASSWORD_DIALOG_SHOWING = "changePasswordDialogShowing";
	private static final String BUNDLE_KEY_CHANGE_PNP_DIALOG_SHOWING = "pnpDialogShowing";
	private static final String BUNDLE_KEY_CHANGE_RENDERER_OVERLAY_MESSAGE = "BUNDLE_KEY_CHANGE_RENDERER_OVERLAY_MESSAGE";
	
	public static final String CAROUSEL_MESSAGE_KEY = "CAROUSEL_MESSAGE_KEY";
	public static final String IMAGE_RES = "imageRes";
	public static final String PRODUCTS = "Products";
	private static final String CAROUSEL = "Carousel";

	private ActionBar actionBar;
	private CarouselPagerAdapter carouselPagerAdapter;
	private ViewPager viewPager;
	private FlyoutMenu flyoutMenu;
	private CarouselResultOverlayRenderer resultOverlayRenderer;
	private List<ProductDetails> products;
	private CarouselProductAnalyticHelper productAnalyticHelper;
	private MenuItem directionsMenuItem;
	private MenuItem changeToMapItem;
	private MenuItem productListMenuItem;
	private TescoLocationProvider locationProvider;
	private Location currentLocation;
	private BroadcastReceiver storeBroadcastReceiver;
	private List<Store> stores;
	private FrameLayout content;
	private BroadcastReceiver reloadProductBroadcastReceiver;
	private BroadcastReceiver credentialBroadcastReceiver;
	private CarouselReloader carouselReloader;
	private FeaturesResponse features;
	private boolean isAtmEnabled;
	private CarouselFlyoutMenuItemManager flyoutMenuItemManager;
	private PayNoPayDisplay payNoPayDisplay;
	private boolean atmsRetrieved = false;

	private Integer selectedListProductPosition;

	private SlidingTabLayout tabs;

	private Dialog productInfoDialog;

	private AuthenticatedSettingsFragment authenticatedSettingsFragment;

	private DirectionsHelper directionsHelper;
	private AlertDialog payNoPayDialog;
	private TescoAlertDialog.Builder alertDialogBuilder;
	
	private int currentItemForReinitVersion;
	private boolean backProcessedOnReinitVersion;
	private boolean hasFocus;
	SeasonalAnimationManager seasonalAnimationManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onCreate");
		}
		
		createCredentialBroadcastReceiver();
		registerCredentialBroadcastReceiver();

		features = getApplicationState().getFeatures();
		isAtmEnabled = features.isAtmStoreFinderEnabled();
		createAuthenticationFragment();
		carouselReloader = new CarouselReloader(this);

		getTooltipManager().setGroup(createTooltipGroup());

		content = (FrameLayout) findViewById(R.id.content_frame);
		createFlyoutMenu();

		configureCarouselResultOverlayRenderer();
		
		actionBar = getSupportActionBar();
		setToStdHamburger(this, actionBar);

		products = getApplicationState().getProductDetails();
		productAnalyticHelper = new CarouselProductAnalyticHelper(getApplicationState().getAnalyticsTracker());

		viewPager = (ViewPager) findViewById(R.id.carousel_pager);
		initialiseAuthenticationFragment();
		flyoutMenuItemManager = new CarouselFlyoutMenuItemManager(flyoutMenu, features, getResources(), products);
		initialisePagerAndAdapter();

		locationProvider = getApplicationState().getLocationProvider();
		handleNoProducts();
		createProductBroadcastReceiver();
		registerProductBroadcastReceiver();
		directionsHelper = new DirectionsHelper(this);
		alertDialogBuilder = new TescoAlertDialog.Builder(this);

		seasonalAnimationManager = new SeasonalAnimationManager(this);
		managePayNoPayDisplay(savedInstanceState);
		restoreSavedState(savedInstanceState);
	}

	private void restoreSavedState(Bundle savedInstanceState) {
		if(savedInstanceState != null) {
			restoreDirectionsDialogCheck(savedInstanceState);	
			restoreCredentialManagementDialogs(savedInstanceState);
			restoreResultOverlayRenderer(savedInstanceState);
		}
	}

	private void configureCarouselResultOverlayRenderer() {
		resultOverlayRenderer = new CarouselResultOverlayRenderer(this);
		if(getFlow().getDataBundle().containsKey(OVERLAY_MESSAGE_KEY)) {
			resultOverlayRenderer.setMessage(getFlow().getDataBundle().getString(OVERLAY_MESSAGE_KEY));
		}
	}

	private void createCredentialBroadcastReceiver() {
		credentialBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				resultOverlayRenderer.setMessage(intent.getStringExtra(OVERLAY_MESSAGE_KEY));
			}
		};
	}

	private void registerCredentialBroadcastReceiver() {
		LocalBroadcastManager.getInstance(this).registerReceiver(credentialBroadcastReceiver, new IntentFilter(CREDENTIAL_OVERLAY_MESSAGE));
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(BUNDLE_KEY_DIRECTION_HELPER_SHOWING, directionsHelper.isShowing());
		outState.putSerializable(BUNDLE_KEY_STORES, (Serializable) stores);
		outState.putParcelable(BUNDLE_KEY_STORE_LOCATION, currentLocation);
		outState.putBoolean(BUNDLE_KEY_CHANGE_PASSWORD_DIALOG_SHOWING, authenticatedSettingsFragment.isChangePasswordWarningDialogShowing());
		outState.putString(BUNDLE_KEY_CHANGE_RENDERER_OVERLAY_MESSAGE, resultOverlayRenderer.getMessage());
		savePayNoPayInstanceState(outState);
		resultOverlayRenderer.dismissOverlay();
	}

	private void savePayNoPayInstanceState(Bundle outState) {
		if (null != payNoPayDialog && payNoPayDialog.isShowing()) {
			outState.putSerializable(BUNDLE_KEY_CHANGE_PNP_DIALOG_SHOWING, true);
		} else {
			outState.putSerializable(BUNDLE_KEY_CHANGE_PNP_DIALOG_SHOWING, false);
		}
	}

	private void managePayNoPayDisplay(Bundle savedInstanceState) {

		if (null == savedInstanceState || (Boolean) savedInstanceState.getSerializable(BUNDLE_KEY_CHANGE_PNP_DIALOG_SHOWING)) {
			payNoPayDisplay = new PayNoPayDisplay(this, products);
			if (resultOverlayRenderer.getMessage() == null) {
				handlePayNoPay();
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void restoreDirectionsDialogCheck(Bundle savedInstanceState) {
		boolean directionHelperShowing = (Boolean) savedInstanceState.getSerializable(BUNDLE_KEY_DIRECTION_HELPER_SHOWING);
		if (directionHelperShowing) {
			stores = (List<Store>) savedInstanceState.getSerializable(BUNDLE_KEY_STORES);
			currentLocation = (Location) savedInstanceState.getParcelable(BUNDLE_KEY_STORE_LOCATION);
			startDirections();
		}
	}

	private void restoreCredentialManagementDialogs(Bundle savedInstanceState) {
		if (savedInstanceState.getBoolean(BUNDLE_KEY_CHANGE_PASSWORD_DIALOG_SHOWING)) {
			authenticatedSettingsFragment.showConfirmChangePasswordRequiredDialog();
		}
	}

	private void restoreResultOverlayRenderer(Bundle savedInstanceState) {
		resultOverlayRenderer.setMessage(savedInstanceState.getString(BUNDLE_KEY_CHANGE_RENDERER_OVERLAY_MESSAGE));
	}

	private void createAuthenticationFragment() {
		authenticatedSettingsFragment = new AuthenticatedSettingsFragment();
		getDrawer().setContentViewWithDrawer(R.layout.activity_carousel, authenticatedSettingsFragment);
	}

	private void initialiseAuthenticationFragment() {
		authenticatedSettingsFragment.initialise(this);
	}

	protected void createFlyoutMenu() {
		flyoutMenu = new FlyoutMenu(this, content, this, getFlyoutMenuInflator());
	}

	protected TescoMenuInflator getFlyoutMenuInflator() {
		return new ABSMenuInflator(this);
	}

	private void handlePayNoPay() {

		if (null == payNoPayDisplay) {
			return;
		}

		if (payNoPayDisplay.shouldWarn()) {
			int index = payNoPayDisplay.getFirstWarningIndex();
			if (isAtmEnabled) {
				index++;
			}
			selectCarouselPage(index);
			String message = payNoPayDisplay.invokeWarning();
			payNoPayDialog = alertDialogBuilder.setTitle(R.string.pnp_title).setMessage(Html.fromHtml(message)).setPositiveButton(R.string.pnp_ok, null).create();
			payNoPayDialog.show();
		}
	}

	protected void selectCarouselPage(int index) {
		viewPager.setCurrentItem(index);
		if (isCurrentPageATM()) {
			retrieveStoreLocation();
			trackATMAnalytic();
		} else if (isCurrentPageAProduct()) {
			trackProductAnalytic(index - (isAtmEnabled ? 1 : 0));
		}
		refreshMenus();
		showTooltip();
	}

	@SuppressLint("NewApi")
	private void refreshMenus() {
		flyoutMenu.dismissWithAnimation();
		updateMenu();
		invalidateOptionsMenu();
		updateTooltipManager();
	}

	private void showTooltip() {
		if (hasFocus) {
			getTooltipManager().hide();
			getTooltipManager().show();
		}
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		Log.d(TAG, "NORMAL:" + event.getAction());
		Log.d(TAG, "MASKED:" + event.getActionMasked());

		if (event.getActionMasked() == ACTION_DOWN && seasonalAnimationManager.isShowing()) {
			seasonalAnimationManager.stopGradually(true);
			return true;
		}
		return super.dispatchTouchEvent(event);
	}

	@Override
	protected void onResume() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onResume - IN");
		}
		super.onResume();

		seasonalAnimationManager.install();
		getDrawer().refresh();

		if (shouldReinitaliseViewPager()) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					resetViewPager();
					initialisePagerAndAdapter();
				}
			});
		}
		showBalancePeekTutorial();
	}

	private void showBalancePeekTutorial() {
		if(getApplicationState().shouldDisplayBalancePeekTutorial()) {
			Intent intent = new Intent(this, BalancePeekTutorialActivity.class);
			startActivity(intent);
			getApplicationState().hasSeenBalancePeekTutorial();
		}
	}


	private boolean shouldReinitaliseViewPager() {
		return Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB;
	}

	private void createProductBroadcastReceiver() {
		reloadProductBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				handleProductReloadBroadcast(intent);
			}
		};
	}

	private void registerProductBroadcastReceiver() {
		LocalBroadcastManager.getInstance(this).registerReceiver(reloadProductBroadcastReceiver, new IntentFilter(CAROUSEL_PRODUCT_RELOAD_PRODUCT_BROADCAST_EVENT));
	}

	private void handleProductReloadBroadcast(Intent intent) {

		CarouselReloadProductsStatus status = CarouselReloadProductsStatus.class.cast(intent.getExtras().getSerializable(CAROUSEL_PRODUCT_RELOAD_PRODUCT_STATUS_EXTRA));

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "handleProductReloadBroadcast " + status);
		}

		switch (status) {

		case SUCCESS:
		case IN_PROGRESS:
			refreshActiveProducts();
			break;
		case FAIL:
			// Do nothing, default error handling will kick in
			break;
		}
	}

	private void updateTooltipManager() {
		getTooltipManager().setGroup(createTooltipGroup());
	}

	private CarouselTooltipGroup createTooltipGroup() {
		boolean hasCalendar = false;
		if (isCurrentPageAProduct()) {
			Product product = getCurrentItemFromViewPager();
			hasCalendar = product.isPersonalCurrentAccount();
		}
		return new CarouselTooltipGroup(this, true, isCurrentPageATM(), shouldShowATMTooltip(), hasCalendar);
	}

	private boolean isCurrentPageATM() {
		return viewPager != null && isAtmEnabled && viewPager.getCurrentItem() == 0;
	}

	private boolean isCurrentPageAProduct() {
		return viewPager != null && !isCurrentPageATM() && hasProducts();
	}

	private boolean shouldShowATMTooltip() {
		return isAtmEnabled && atmsRetrieved && locationProvider.getLocationStatus() == Status.OK;
	}

	private boolean hasProducts() {
		return products != null && !products.isEmpty();
	}

	/**
	 * Only ever called for Gingerbread 2.3.4, 2.3.5 and 2.3.6 versions as it
	 * does not process back button processing correctly in the carousel. When
	 * back is pressed on a child activity an blank screen is presented until a
	 * gesture event is intercepted
	 */
	private void resetViewPager() {
		currentItemForReinitVersion = viewPager.getCurrentItem();
		backProcessedOnReinitVersion = true;
		viewPager.removeAllViews();
		viewPager.setAdapter(null);
	}

	private void initialisePagerAndAdapter() {
		carouselPagerAdapter = new CarouselPagerAdapter(this, getSupportFragmentManager(), products, isAtmEnabled);
		viewPager.setAdapter(carouselPagerAdapter);

		tabs = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
		tabs.setViewPager(viewPager);

		attachOnPageChangeListener();

		if (shouldReinitaliseViewPager() && backProcessedOnReinitVersion) {
			selectCarouselPage(currentItemForReinitVersion);
			backProcessedOnReinitVersion = false;
		} else if (isAtmEnabled && hasProducts()) {
			selectCarouselPage(1);
		} else {
			refreshMenus();
		}
		content.invalidate();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		this.hasFocus = hasFocus;
		if (hasFocus) {
			if (products == null || products.isEmpty()) {
				productListMenuItem.setVisible(false);
			}

			resultOverlayRenderer.renderSuccessOverlay(resultOverlayRenderer.getMessage(), new PopupWindow.OnDismissListener() {
				@Override
				public void onDismiss() {
					handlePayNoPay();
				}
			});

			viewPager.postDelayed(new Runnable() {
				@Override
				public void run() {
					if (null != selectedListProductPosition) {
						selectCarouselPage(selectedListProductPosition);
						selectedListProductPosition = null;
					}
				}
			}, 100);
			showTooltip();
		}
	}

	@Override
	public void handleError(ErrorResponseWrapper errorResponseWrapper) {
		handleErrorResponse(errorResponseWrapper);
	}

	@Override
	protected void onPause() {
		super.onPause();
		cancelTasks();
		deregisterStoreReceiver();
		productAnalyticHelper.cancel();
		flyoutMenu.cleanUpMenuOnExit();
		flyoutMenu.dismiss();
		seasonalAnimationManager.uninstall();
	}

	private void deregisterCredentialBroadcastManager() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(credentialBroadcastReceiver);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		deregisterCredentialBroadcastManager();
		deregisterProductBroadcastReceiver();	
		if (null != resultOverlayRenderer) {
			resultOverlayRenderer.dismissOverlay();
		}

		if (null != directionsHelper) {
			directionsHelper.dismissDirectionsDialog();
		}

		if (null != payNoPayDialog) {
			payNoPayDialog.dismiss();
		}
	}

	private void deregisterProductBroadcastReceiver() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(reloadProductBroadcastReceiver);
	}

	private void cancelTasks() {
		resultOverlayRenderer.cancelTasks();
	}

	private void trackATMAnalytic() {
		getApplicationState().getAnalyticsTracker().trackViewATM();
	}

	private void trackProductAnalytic(int position) {

		if (position >= 0 && position < products.size()) {
			Product product = products.get(position);
			productAnalyticHelper.track(product);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onCreateOptionsMenu");
		}
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onPrepareOptionsMenu");
		}

		menu.clear();

		// always re-inflate. changing the visibility post "onCreateOptionsMenu"
		// is broken
		getSupportMenuInflater().inflate(R.menu.carousel, menu);

		changeToMapItem = menu.findItem(R.id.menu_item_change_to_map);
		directionsMenuItem = menu.findItem(R.id.menu_item_get_directions);
		productListMenuItem = menu.findItem(R.id.menu_item_product_list);

		// set the visibility appropriately
		changeToMapItem.setVisible(shouldShowMapMenuItems());
		directionsMenuItem.setVisible(shouldShowMapMenuItems());

		return true;
	}

	private boolean shouldShowMapMenuItems() {
		return getApplicationState().getLocationProvider().getLocationStatus() == Status.OK && 0 == viewPager.getCurrentItem() && stores != null && !stores.isEmpty();
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		if (directionsMenuItem.equals(item)) {
			startDirections();
			return true;
		} else if (changeToMapItem.equals(item)) {
			getTooltipManager().dismiss(TooltipId.ATMFinderFirstUse.ordinal());
			showATMMap();
			return true;
		} else if (productListMenuItem.equals(item)) {
			showProductListView();
			return true;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	private void startDirections() {
		Store store = stores.get(0);
		directionsHelper.showDirections(currentLocation.getLatitude(), currentLocation.getLongitude(), store.getStoreMetadata().getLatitude(), store.getStoreMetadata().getLongitude());
	}

	private void showATMMap() {
		Intent intent = new Intent(this, ATMMapActivity.class);
		intent.putExtra(ATMSearchHelper.FIT_BOUNDS_FOR_USER_EXTRA_NAME, true);
		intent.putExtra(ATMSearchHelper.STORES_EXTRA_NAME, new LinkedList<Store>(stores));
		intent.putExtra(ATMSearchHelper.REFERENCE_POINT, new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
		startActivity(intent);
	}

	private void showProductListView() {
		getTooltipManager().dismiss(TooltipId.CarouselList.ordinal());
		Intent intent = new Intent(this, CarouselListActivity.class);
		intent.putExtra(PRODUCTS, new ArrayList<ProductDetails>(products));
		intent.putExtra(CarouselListActivity.PRODUCT_POSITION, viewPager.getCurrentItem());
		startActivityForResult(intent, REQUEST_DEFAULT);
		getApplicationState().getAnalyticsTracker().trackToggleToListView();
	}

	private void attachOnPageChangeListener() {
		// Note, the page change listener MUST be set on the tabs as they are
		// chained together
		tabs.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				selectCarouselPage(position);
			}
		});
	}

	private void retrieveStoreLocation() {

		if (locationProvider.getLocation() == null) {
			return;
		}
		registerStoreReceiver();
		currentLocation = locationProvider.getLocation();
		LatLng latLng = CarouselActivity.latLngFromLocation(currentLocation);
		getApplicationState().getATMProvider().retrieveCachableATMsForLatLng(latLng);
	}

	private void registerStoreReceiver() {
		storeBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				handleATMRetrievalBroadcast(intent);
			}

		};
		LocalBroadcastManager.getInstance(this).registerReceiver(storeBroadcastReceiver, new IntentFilter(ATMProvider.ATM_RETRIEVAL_BROADCAST_EVENT));
	}

	private void handleATMRetrievalBroadcast(Intent intent) {
		ATMRetrievalStatus status = (ATMRetrievalStatus) intent.getSerializableExtra(ATMProvider.ATM_RETRIEVAL_STATUS_EXTRA);
		status = (status == null ? ATMRetrievalStatus.FAIL : status);

		switch (status) {
		case SUCCESS:
			// only do this once
			if (!atmsRetrieved) {
				updateTooltipManager();
			}
			stores = CarouselActivity.getStoresFromIntent(intent);
			supportInvalidateOptionsMenu();
			atmsRetrieved = true;
			break;

		case FAIL:
			supportInvalidateOptionsMenu();
			atmsRetrieved = false;
			break;
		}
	}

	private void deregisterStoreReceiver() {
		if (storeBroadcastReceiver != null) {
			LocalBroadcastManager.getInstance(this).unregisterReceiver(storeBroadcastReceiver);
		}
	}

	@Override
	public void itemSelected(FlyoutMenu menu, MenuItem item) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "itemSelected: " + item.getItemId() + "/" + item.getTitle());
		}

		Intent intent = null;
		switch (item.getItemId()) {
		case R.id.flyout_menu_item_pca_scheduled_payments:
			intent = new Intent(this, RegularPaymentsActivity.class);
			intent.putExtra(REGULAR_PAYMENTS_PRODUCT_DETAILS_EXTRA, getCurrentProduct());
			break;
		case R.id.flyout_menu_item_pca_transaction_calendar:
			getTooltipManager().dismiss(TooltipId.CalendarOnFlyoutMenu.ordinal());
			intent = new Intent(this, CalendarActivity.class);
			intent.putExtra(TRANSACTIONS_PRODUCT_EXTRA, getCurrentProduct());
			break;
		case R.id.flyout_menu_item_transfers:
			intent = new Intent(this, TransferActivity.class);
			intent.putExtra(SOURCE_PRODUCT_EXTRA, getCurrentProduct());
			break;
		case R.id.flyout_menu_item_payments:
			intent = new Intent(this, PaymentActivity.class);
			intent.putExtra(SOURCE_PRODUCT_EXTRA, getCurrentProduct());
			break;
		case R.id.flyout_menu_item_savings:
			intent = new Intent(this, SavingActivity.class);
			intent.putExtra(DESTINATION_PRODUCT_EXTRA, getCurrentProduct());
			break;
		case R.id.flyout_menu_item_transactions:
			intent = new Intent(this, TransactionsActivity.class);
			intent.putExtra(TRANSACTIONS_PRODUCT_EXTRA, getCurrentProduct());
			break;
		case R.id.flyout_menu_item_creditcard_pay_with_debitcard:
			intent = new Intent(this, PayCreditCardActivity.class);
			intent.putExtra(PAY_CREDITCARD_PRODUCT_EXTRA, getCurrentProduct());
			break;
		case R.id.flyout_menu_item_archive_statement:
			intent = new Intent(this, ViewStatementListActivity.class);
			intent.putExtra(STATEMENT_PRODUCT_EXTRA, getCurrentProduct());
			intent.putExtra(LAUNCHED_FROM_STATEMENT_ARCHIVE_LIST, CAROUSEL);
			break;
		default:
			throw new RuntimeException("Unexpected menu item: " + item.getItemId());
		}

		getApplicationState().getAnalyticsTracker().trackMenuItemSelected(item.getTitle().toString());
		intent.putExtra("title", item.getTitle());
		startActivityForResult(intent, REQUEST_DEFAULT);
	}

	private void updateMenu() {

		int menuRes = carouselPagerAdapter.getMenuResource(viewPager.getCurrentItem());

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "updateMenu: " + menuRes);
		}

		if (menuRes == -1) {
			flyoutMenu.hideButton();
		} else {
			updateFlyoutMenu(menuRes);
		}
	}

	private void updateFlyoutMenu(int menuRes) {
		ProductDetails currentProduct = getCurrentItemFromViewPager();
		flyoutMenu.setMenuResource(menuRes);
		flyoutMenuItemManager.updateMenu(currentProduct);
		applyRootMenuImageDrawableForProductType(currentProduct);
	}

	private void applyRootMenuImageDrawableForProductType(ProductDetails currentProduct) {
		FlyoutMenuRootButtonBackgroundDrawable flyoutMenuRootButtonBackgroundDrawable = imageDrawableForProduct(currentProduct.getProductType());
		int leftResourceId = flyoutMenuRootButtonBackgroundDrawable.getImageDrawableLeft();
		int rightResourceId = flyoutMenuRootButtonBackgroundDrawable.getImageDrawableRight();
		int selectedResourceId = flyoutMenuRootButtonBackgroundDrawable.getImageDrawableSelected();
		flyoutMenu.updateMenuButtonImage(leftResourceId, rightResourceId, selectedResourceId);
	}

	private ProductDetails getCurrentItemFromViewPager() {
		return products.get(isAtmEnabled ? viewPager.getCurrentItem() - 1 : viewPager.getCurrentItem());
	}

	@Override
	public final void onBackPressed() {

		if (seasonalAnimationManager.isShowing()) {
			seasonalAnimationManager.stopGradually(true);
			return;
		}

		if (flyoutMenu.handleBackPressed()) {
			return;
		}

		if (resultOverlayRenderer.dismissOverlay()) {
			return;
		}
		getFlow().onPolicyCancelled(this);
	}

	public void displayProductDetails(View view) {
		// prevent double tap from opening two dialogs
		if (productInfoDialog != null) {
			return;
		}

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Displayig Product Details");
		}

		ProductDetails currentProduct = getCurrentProduct();
		productInfoDialog = ProductDetailDisplayer.displayProductDetail(currentProduct, this);
		productInfoDialog.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				productInfoDialog = null;
			}
		});

		getApplicationState().getAnalyticsTracker().trackCarouselProductShown(currentProduct, true);
	}

	private ProductDetails getCurrentProduct() {
		return getCurrentItemFromViewPager();
	}

	@Override
	protected void onRetryPressed() {
		if (!(lastRequestTypeSaved() && GetPinChallengeRequest.class.isAssignableFrom(getLastRequestType()))) {
			carouselReloader.reloadCarousel();
		}
	}
	
	@Override
	public boolean backFromRetryErrorShouldRetry() {
		if (lastRequestTypeSaved() && GetPinChallengeRequest.class.isAssignableFrom(getLastRequestType())) {
			return false;
		}
		return true;
	}
	
	private Class<?> getLastRequestType() {
		RestRequestProcessor<ErrorResponseWrapper> processor = getApplicationState().getRestRequestProcessor();
		return processor.getLastRequestTypeForRequesterType(getClass());
	}
	
	private boolean lastRequestTypeSaved() {
		return getLastRequestType() != null;
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if (resultCode == RESULT_MOVE_MONEY_SUCCESS) {
			reloadCarouselProducts();
			return;
		}

		if (resultCode == RESULT_BACK_FROM_RETRY_PRESSED) {
			// Don't propagate any further.
			return;
		}

		if (resultCode == RESULT_CAROUSEL_LIST_COMPLETED) {
			selectProductSelectedInCarouselList(intent);
			return;
		}

		super.onActivityResult(requestCode, resultCode, intent);
	}

	private void selectProductSelectedInCarouselList(Intent intent) {
		selectedListProductPosition = null;
		if (intent.hasExtra(PRODUCT_POSITION)) {
			selectedListProductPosition = intent.getIntExtra(PRODUCT_POSITION, 0);
		}
	}

	private void reloadCarouselProducts() {
		carouselReloader.reloadCarousel();
	}

	@SuppressWarnings("unchecked")
	public static List<Store> getStoresFromIntent(Intent intent) {
		return (List<Store>) intent.getSerializableExtra(Store.class.getName());
	}

	public static LatLng latLngFromLocation(Location location) {
		return new LatLng(location.getLatitude(), location.getLongitude());
	}

	@Override
	public void onProductDataLoadComplete() {
		refreshActiveProducts();
	}

	private void refreshActiveProducts() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "refreshActiveProducts");
		}
		carouselPagerAdapter.updateProducts(getApplicationState().getProductDetails());
	}

	private void handleNoProducts() {
		if (!hasProducts()) {
			flyoutMenu.hideButton();
			showNoProductsDialog();
		}
	}

	private void showNoProductsDialog() {
		new TescoAlertDialog.Builder(this).setTitle(R.string.carousel_no_products_message_title).setMessage(R.string.carousel_no_products_message)
				.setNegativeButton(R.string.carousel_no_products_message_ok, initAtmActionBarItems()).create().show();
	}

	private OnClickListener initAtmActionBarItems() {
		return new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (isAtmEnabled) {
					retrieveStoreLocation();
				} else {
					logOut();
				}
			}
		};
	}
}
