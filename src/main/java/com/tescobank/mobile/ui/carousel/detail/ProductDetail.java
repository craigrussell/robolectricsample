package com.tescobank.mobile.ui.carousel.detail;

import static com.tescobank.mobile.model.product.ProductType.typeForLabel;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.tescobank.mobile.R;
import com.tescobank.mobile.model.product.FundingAccount;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.model.product.ProductType;
import com.tescobank.mobile.ui.TescoModal;
import com.tescobank.mobile.ui.carousel.CarouselDataSetter;

/**
 * Abstract base class for product details dialogs
 */
public abstract class ProductDetail extends TescoModal {

	private ProductDetails product;

	private CarouselDataSetter carouselDataSetter;

	private View modal;

	public ProductDetail(ProductDetails product, Context context) {
		super(context);

		this.product = product;
		
		populateContentView();
		modal = findViewById(R.id.tesco_dialog_container);
		carouselDataSetter = new CarouselDataSetter(product, getContext().getResources());

		setMessageText(context.getText(R.string.product_detail_title).toString());
		setHeadingText(product.getProductName());
		populateFromProduct(product);
	}

	private void populateContentView() {

		setContentView(R.layout.tesco_modal);

		LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout container = (RelativeLayout) findViewById(R.id.tesco_dialog_container);

		View view = layoutInflater.inflate(getContentViewId(), container, false);
		container.addView(view);
		
		List<FundingAccount> fundingAccounts = product.getFundingAccounts();
		ProductType productType = typeForLabel(product.getProductType());

		if (fundingAccounts != null && fundingAccounts.size() > 0 && !productType.equals(ProductType.FISA_PRODUCT) && !productType.equals(ProductType.FJISA_PRODUCT)) {
			populateViewTable(layoutInflater, view, fundingAccounts);
		}
	}

	protected void populateViewTable(LayoutInflater layoutInflater, View container, List<FundingAccount> fundingAccounts) {
	}

	protected abstract int getContentViewId();

	protected abstract void populateFromProduct(ProductDetails product);

	protected CarouselDataSetter getCarouselDataSetter() {
		return carouselDataSetter;
	}

	protected View getModal() {
		return modal;
	}

}
