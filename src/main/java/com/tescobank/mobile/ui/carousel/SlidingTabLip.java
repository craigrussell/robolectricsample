package com.tescobank.mobile.ui.carousel;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.tescobank.mobile.R;

class SlidingTabLip extends LinearLayout {

	private int mSelectedPosition;
	private float mSelectionOffset;

	private final Bitmap lipIndicator;
	private int lipIndicatorOffsetX;

	SlidingTabLip(Context context) {
		this(context, null);
	}

	SlidingTabLip(Context context, AttributeSet attrs) {
		super(context, attrs);
		setWillNotDraw(false);

		lipIndicator = BitmapFactory.decodeResource(getResources(), R.drawable.tab_selected);
		lipIndicatorOffsetX = (lipIndicator.getWidth() / 2);
	}

	void onViewPagerPageChanged(int position, float positionOffset) {
		mSelectedPosition = position;
		mSelectionOffset = positionOffset;
		invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		final int height = getHeight();
		final int childCount = getChildCount();

		if (childCount > 0) {
			View selectedTitle = getChildAt(mSelectedPosition);
			int left = selectedTitle.getLeft();
			int right = selectedTitle.getRight();

			if (mSelectionOffset > 0f && mSelectedPosition < (getChildCount() - 1)) {
				// draws the indicator part-way between items
				View nextTitle = getChildAt(mSelectedPosition + 1);
				left = (int) (mSelectionOffset * nextTitle.getLeft() + (1.0f - mSelectionOffset) * left);
				right = (int) (mSelectionOffset * nextTitle.getRight() + (1.0f - mSelectionOffset) * right);
			}

			int centerX = ((right - left) / 2);
			left = left + centerX - lipIndicatorOffsetX;

			int y = height - lipIndicator.getHeight();

			canvas.drawBitmap(lipIndicator, left, y, null);
		}

	}

}