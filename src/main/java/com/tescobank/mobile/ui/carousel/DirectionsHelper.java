package com.tescobank.mobile.ui.carousel;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoAlertDialog;

public class DirectionsHelper {

	private static final String TAG = DirectionsHelper.class.getSimpleName();

	private TescoActivity activity;
    private TescoAlertDialog.Builder alertDialogBuilder;
    private AlertDialog alertDialog;
    
	public DirectionsHelper(TescoActivity activity) {
		this.activity = activity;
		alertDialogBuilder = new TescoAlertDialog.Builder(activity);
	}

	public void showDirections(final double fromLat, final double fromLon, final double toLat, final double toLon) {
		
		alertDialog = alertDialogBuilder.setTitle(R.string.atm_confirm_directions_title).setMessage(R.string.atm_confirm_directions_message).setPositiveButton(R.string.atm_confirm_directions_positive, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				String format = "https://maps.google.co.uk/maps?saddr=%s,%s&daddr=%s,%s";
				String uri = String.format(format, fromLat, fromLon, toLat, toLon);
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "directions uri: " + uri);
				}
				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
				activity.startActivity(i);
				ActivityCompat.finishAffinity(activity);
			}
		}).setNegativeButton(R.string.atm_confirm_directions_negative, null).create();
		
		alertDialog.show();
	}
	
	public void dismissDirectionsDialog() {
		if(null != alertDialog) {
			alertDialog.dismiss();
		}	
	}
	
	public boolean isShowing() {
		if(null != alertDialog) {
			return alertDialog.isShowing();
		}
		return false;
	}
}
