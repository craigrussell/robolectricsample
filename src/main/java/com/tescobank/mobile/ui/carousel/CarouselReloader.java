package com.tescobank.mobile.ui.carousel;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.product.RetrieveProductDetailsRequest;
import com.tescobank.mobile.api.product.RetrieveProductDetailsResponse;
import com.tescobank.mobile.helper.ProductHelper;
import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.TescoActivity;

/**
 * Refreshes data rendered by the carousel
 */
public class CarouselReloader {

	private static final String TAG = CarouselReloader.class.getSimpleName();

	/** The activity */
	private TescoActivity activity;

	/** Whether the reload failed */
	private boolean reloadFailed;

	static final String CAROUSEL_PRODUCT_RELOAD_PRODUCT_BROADCAST_EVENT = CarouselReloader.class.getName();
	static final String CAROUSEL_PRODUCT_RELOAD_PRODUCT_STATUS_EXTRA = "CAROUSEL_PRODUCT_RELOAD_PRODUCT_STATUS_EXTRA";

	enum CarouselReloadProductsStatus {
		FAIL, SUCCESS, IN_PROGRESS;
	}

	/**
	 * Constructor
	 * 
	 * @param activity
	 *            the activity
	 * @param productDataLoadedListener
	 *            the product data loaded listener
	 */
	public CarouselReloader(TescoActivity activity) {
		super();

		this.activity = activity;
	}

	public void reloadCarousel() {
		List<ProductDetails> dirtyProductDetails = activity.getApplicationState().getDirtyProductDetails();
		List<Product> dirtyProducts = new ArrayList<Product>();
		dirtyProducts.addAll(dirtyProductDetails);
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Reloading " + dirtyProducts.size() + " Products");
		}
		reloadFailed = false;
		retrieveProductDetails(dirtyProducts);
	}

	/**
	 * @return whether the reload failed
	 */
	public boolean didReloadFail() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Reload Failed = " + reloadFailed);
		}
		return reloadFailed;
	}
	
	private List<Product> getProductsForProductDetails(List<ProductDetails> productDetails) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Getting Products For Product Details");
		}

		List<Product> products = new ArrayList<Product>();
		List<Product> allProducts = activity.getApplicationState().getProducts();
		for (ProductDetails productDetail : productDetails) {
			products.add(allProducts.get(allProducts.indexOf(productDetail)));
			allProducts.get(allProducts.indexOf(productDetail)).setDirty(true);
		}

		return products;
	}

	private void retrieveProductDetails(List<Product> products) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Retrieve Product Details Request for " + products.size() + " Products");
		}

		broadcastProductDataLoadInProgress();

		RetrieveProductDetailsRequest request = new ReloaderRetrieveProductDetailsRequest();
		request.setTbID(activity.getApplicationState().getTbId());
		request.setProductIdentifiers(products);
		activity.getApplicationState().getRestRequestProcessor().processRequest(request);
	}

	private void broadcastProductDataLoadInProgress() {
		Intent intent = new Intent(CAROUSEL_PRODUCT_RELOAD_PRODUCT_BROADCAST_EVENT);
		intent.putExtra(CAROUSEL_PRODUCT_RELOAD_PRODUCT_STATUS_EXTRA, CarouselReloadProductsStatus.IN_PROGRESS);
		LocalBroadcastManager.getInstance(activity.getApplicationContext()).sendBroadcast(intent);
	}

	private void broadcastProductDataLoadCompletedSuccess() {
		Intent intent = new Intent(CAROUSEL_PRODUCT_RELOAD_PRODUCT_BROADCAST_EVENT);
		intent.putExtra(CAROUSEL_PRODUCT_RELOAD_PRODUCT_STATUS_EXTRA, CarouselReloadProductsStatus.SUCCESS);
		LocalBroadcastManager.getInstance(activity.getApplicationContext()).sendBroadcast(intent);
	}

	private void broadcastProductDataLoadCompletedError() {
		Intent intent = new Intent(CAROUSEL_PRODUCT_RELOAD_PRODUCT_BROADCAST_EVENT);
		intent.putExtra(CAROUSEL_PRODUCT_RELOAD_PRODUCT_STATUS_EXTRA, CarouselReloadProductsStatus.FAIL);
		LocalBroadcastManager.getInstance(activity.getApplicationContext()).sendBroadcast(intent);
	}

	private void updateProductDetails(RetrieveProductDetailsResponse response) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Updating Product Details");
		}

		List<Product> products = getProductsForProductDetails(response.getProducts());
		List<ProductDetails> productDetails = ProductHelper.updateProductDetailsWithSummaryData(products,response.getProducts());
		ProductHelper.updateProductDetailsWithFundingAccounts(productDetails, activity.getApplicationState().getFundingAccounts());
		activity.getApplicationState().updateProductDetails(productDetails);
	}

	private class ReloaderRetrieveProductDetailsRequest extends RetrieveProductDetailsRequest {

		@Override
		public void onResponse(RetrieveProductDetailsResponse response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Retrieve Product Details Request Succeeded");
			}
			updateProductDetails(response);
			broadcastProductDataLoadCompletedSuccess();
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Retrieve Product Details Request Failed: " + errorResponseWrapper.getErrorResponse().getErrorCode());
			}
			reloadFailed = true;
			activity.handleErrorResponse(errorResponseWrapper);
			broadcastProductDataLoadCompletedError();
		}
	}
}
