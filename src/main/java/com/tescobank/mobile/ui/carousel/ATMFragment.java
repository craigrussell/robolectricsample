package com.tescobank.mobile.ui.carousel;

import static com.tescobank.mobile.ui.carousel.CarouselProductHintHelper.NEXT_ITEM_HINT;

import java.util.LinkedList;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.maps.model.LatLng;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.storefinder.Store;
import com.tescobank.mobile.application.TescoLocationProvider;
import com.tescobank.mobile.ui.TescoFragment;
import com.tescobank.mobile.ui.atm.ATMMapActivity;
import com.tescobank.mobile.ui.atm.ATMSearchHelper;

public class ATMFragment extends TescoFragment implements LocationUpdateListener, StoresUpdateListener{

	private static final String TAG = ATMFragment.class.getSimpleName();

	private TescoLocationProvider locationProvider;
	private String nextItemHint;
	private View rootView;
	private ImageView mapTransparentOverlay;
	private ImageView rightImageView;
	private boolean fragmentBecameVisible;
	private boolean activityBecameVisible;
	private ATMDisplayer atmDisplayer;
	private ATMRetriever atmRetriever;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating ATM fragment");
		}
		atmRetriever = new ATMRetriever(getTescoActivity(), this, this);
		atmDisplayer = new ATMDisplayer(getTescoActivity(), this);
		locationProvider = getApplicationState().getLocationProvider();
		nextItemHint = getArguments().getString(NEXT_ITEM_HINT);
		rootView = atmDisplayer.createView(inflater,container, R.layout.fragment_carousel_atm);
		mapTransparentOverlay = (ImageView) rootView.findViewById(R.id.map_transparent_overlay);
		rightImageView = (ImageView) rootView.findViewById(R.id.product_img_right);
		updateRightImage();
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		activityBecameVisible = true;
		atmDisplayer.onResume();
		atmRetriever.start();
	}

	@Override
	public void onPause() {
		onFragmentPause();
		super.onPause();
	}
	
	@Override
	public void onStop() {
		atmDisplayer.onStop();
		super.onStop();
	}

	private void onFragmentResume() {
		if (activityBecameVisible && fragmentBecameVisible) {
			atmDisplayer.onFragmentResume();
			atmRetriever.start();
		}
	}

	private void onFragmentPause() {
		if(atmRetriever != null) {
			atmRetriever.pause();
		}
	}

	/**
	 * Detect when the atm fragment becomes visible/hidden from the user. Cannot
	 * use onResume/onPause() alone as they report on the activity rather than
	 * fragment visibility.
	 */
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "ATM fragment is visible: " + isVisibleToUser);
		}
		if (isVisibleToUser) {
			fragmentBecameVisible = true;
			onFragmentResume();
		} else {
			onFragmentPause();
		}
		super.setUserVisibleHint(isVisibleToUser);
	}

	public void attachMapClickListener() {
		mapTransparentOverlay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Location location = locationProvider.getLocation();

				Intent intent = new Intent(getActivity(), ATMMapActivity.class);
				intent.putExtra(ATMSearchHelper.FIT_BOUNDS_FOR_USER_EXTRA_NAME, true);
				intent.putExtra(ATMSearchHelper.STORES_EXTRA_NAME, new LinkedList<Store>(atmRetriever.getStores()));
				intent.putExtra(ATMSearchHelper.REFERENCE_POINT, new LatLng(location.getLatitude(), location.getLongitude()));
				startActivity(intent);
			}
		});
	}

	public void attachRetryClickListener() {
		mapTransparentOverlay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				atmRetriever.start();
			}
		});
	}

	public void removeMapClickListener() {
		mapTransparentOverlay.setClickable(false);
	}

	private void updateRightImage() {

		int hintImageId = CarouselProductHintHelper.getRightProductHintImageId(nextItemHint);
		if (hintImageId != -1) {
			rightImageView.setImageDrawable(getResources().getDrawable(hintImageId));
			rightImageView.setVisibility(ImageView.VISIBLE);
		}
	}

	
	@Override
	public void onRetrievedATMs() {
		atmDisplayer.hideProgressIndicator();
		attachMapClickListener();
		if (atmRetriever.getStores() != null && atmRetriever.getStores().size() > 0) {
			setNearestStore();
		}
	}
	
	@Override
	public void onFailedToRetrieveATMs() {
		atmDisplayer.onFailedToRetrieveATMs();
		attachRetryClickListener();
	}

	@Override
	public void onLocationIsAvailable() {
		atmDisplayer.showMap();	
	}

	@Override
	public void onLocationSystemDisabled() {
		removeMapClickListener();
		atmDisplayer.onLocationSystemDisabled();
	}
	
	@Override
	public void onLocationUserDisabled() {
		removeMapClickListener();
		atmDisplayer.onLocationUserDisabled();
	}
	
	private void setNearestStore() {
		if (atmRetriever.getStores() != null && atmRetriever.getStores().size() > 0) {
			atmDisplayer.setNearestStore(atmRetriever.getStores().get(0));
		}
	}

}
