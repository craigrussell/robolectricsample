package com.tescobank.mobile.ui.carousel;

import static com.tescobank.mobile.model.product.ProductType.typeForLabel;

import java.math.BigDecimal;

import android.content.res.Resources;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.model.product.ProductType;
import com.tescobank.mobile.ui.TescoTextView;

public class ProductDisplayer {

	private ProductType productType;
	protected LayoutInflater inflater;
	protected CarouselDataSetter carouselDataSetter;
	protected ProductDetails product;
	private DataFormatter dataFormatter;
	private Resources resources;

	public ProductDisplayer(LayoutInflater inflater, ProductDetails product, CarouselDataSetter carouselDataSetter, Resources resources) {
		setProduct(product);
		this.inflater = inflater;
		this.carouselDataSetter = carouselDataSetter;
		this.resources = resources;

		dataFormatter = new DataFormatter();
	}

	private boolean isStandardProduct() {
		switch (productType) {
		case PCA_PRODUCT:
		case IAS_PRODUCT:
		case IS_PRODUCT:
		case CCP_PRODUCT:
		case FRS_PRODUCT:
			return true;
		default:
			return false;
		}
	}

	private boolean isIsaProduct() {
		switch (productType) {
		case IAI_PRODUCT:
		case JISA_PRODUCT:
			return true;
		default:
			return false;
		}
	}

	private boolean isFixedIsaProduct() {
		switch (productType) {
		case FISA_PRODUCT:
		case FJISA_PRODUCT:
			return true;
		default:
			return false;
		}
	}

	private View updateContentForStandardAccount(LayoutInflater inflater) {

		View content = inflateStd(inflater);

		carouselDataSetter.setProductName(content);
		carouselDataSetter.setBalance(content);
		carouselDataSetter.setAvailableBalance(content);
		carouselDataSetter.setPayNoPay(content);
		carouselDataSetter.setSortCodeAndAccountNumber(content);

		return content;
	}

	protected View inflateStd(LayoutInflater inflater) {
		return inflater.inflate(R.layout.carousel_product_content_std, null);
	}

	private View updateContentForLoan(LayoutInflater inflater) {
		View content = inflateLoan(inflater);
		TescoTextView amountRemaining = (TescoTextView) content.findViewById(R.id.product_balance);

		carouselDataSetter.setProductName(content);
		amountRemaining.setText(dataFormatter.formatCurrency(product.getAmountRemaining()));
		carouselDataSetter.setNextPaymentAmount(content);
		carouselDataSetter.setSortCodeAndAccountNumber(content);

		return content;
	}

	protected View inflateLoan(LayoutInflater inflater) {
		return inflater.inflate(R.layout.carousel_product_content_loan, null);
	}

	private View updateContentForFixedISA(LayoutInflater inflater) {
		View content = inflateISA(inflater);
		carouselDataSetter.setProductName(content);
		carouselDataSetter.setBalance(content);
		carouselDataSetter.setSortCodeAndAccountNumber(content);
		setFixedISASubscriptionSentence(content);
		return content;
	}

	protected View inflateISA(LayoutInflater inflater) {
		return inflater.inflate(R.layout.carousel_product_content_isa, null);
	}

	private View updateContentForISA(LayoutInflater inflater) {
		View content = inflateISA(inflater);
		carouselDataSetter.setProductName(content);
		carouselDataSetter.setBalance(content);
		carouselDataSetter.setSortCodeAndAccountNumber(content);
		setISASubscriptionSentence(content);
		return content;
	}

	private void setISASubscriptionSentence(View content) {
		TescoTextView subscriptionTextView = (TescoTextView) content.findViewById(R.id.product_isa_subscription_text);
		String amountAvailable = dataFormatter.formatCurrency(product.getSubscriptionAmountAvailable());
		String subscriptionLimit = dataFormatter.formatCurrency(product.getSubscriptionLimit());
		String unformatted = getResources().getString(R.string.product_isa_subscription_text);
		String subscriptionSentence = String.format(unformatted, amountAvailable, subscriptionLimit);
		subscriptionTextView.setText(Html.fromHtml(subscriptionSentence));
	}

	protected Resources getResources() {
		return resources;
	}

	private void setFixedISASubscriptionSentence(View content) {
		TescoTextView subscriptionTextView = (TescoTextView) content.findViewById(R.id.product_isa_subscription_text);
		String amountSubscribed = dataFormatter.formatCurrency(product.getSubscriptionAmountSubscribed());
		String unformatted = getResources().getString(R.string.product_frisa_subscription_text);
		String subscriptionSentence = String.format(unformatted, amountSubscribed);
		subscriptionTextView.setText(Html.fromHtml(subscriptionSentence));
	}

	protected View updateContentForCreditCard(LayoutInflater inflater) {

		View content = inflateListRow(inflater);
		TescoTextView cardNumber = (TescoTextView) content.findViewById(R.id.product_card_number);
		TescoTextView minimumPayment = (TescoTextView) content.findViewById(R.id.minimum_payment);
		TescoTextView dueBy = (TescoTextView) content.findViewById(R.id.payment_due_by);
		ImageView ccImage = (ImageView) content.findViewById(R.id.product_creditcard_Image);
		View paymentReceivedMessage = content.findViewById(R.id.product_cc_payment_received_message);
		View minimumPaymentReceivedMessage = content.findViewById(R.id.product_cc_minimum_payment_received_message);
		RelativeLayout productDueBy = (RelativeLayout) content.findViewById(R.id.product_due_by);
		RelativeLayout productMinimumPayment = (RelativeLayout) content.findViewById(R.id.product_minimum_payment);
		TescoTextView productNotCycled = (TescoTextView) content.findViewById(R.id.product_not_cycled);

		dueBy.setVisibility(View.GONE);
		paymentReceivedMessage.setVisibility(View.GONE);
		minimumPayment.setVisibility(View.GONE);
		minimumPaymentReceivedMessage.setVisibility(View.GONE);
		productMinimumPayment.setVisibility(View.GONE);
		productDueBy.setVisibility(View.GONE);
		productNotCycled.setVisibility(View.GONE);

		carouselDataSetter.setProductName(content);
		carouselDataSetter.setBalance(content);
		cardNumber.setText(DataFormatter.formatCardNumber(product.getCardNumber()));
		carouselDataSetter.setAvailableCredit(content);
		ccImage.setImageResource(R.drawable.ic_little_credit_card);

		if (noCycleDatesPresent()) {
			showProductNotCycledMessage(productNotCycled);
		} else if (minimumPaymentGreaterThanZero()) {
			// PIA-74: scenario 1
			showMinimumPayment(productMinimumPayment, minimumPayment);
			showDueDate(productDueBy, dueBy);
		} else if (minimumPaymentLessThanZero()) {
			// PIA-74: scenario 2
			showPaymentReceivedMessage(paymentReceivedMessage);
		} else if (minimumPaymentEqualToZero() && statementBalanceGreaterThanZero()) {
			// PIA-74: scenario 3
			showMinimumPaymentReceivedMessage(minimumPaymentReceivedMessage);
		} else if (minimumPaymentEqualToZero() && statementBalanceLessThanOrEqualToZero()) {
			// PIA-74: scenario 4
			showMinimumPayment(productMinimumPayment, minimumPayment);
		}

		return content;
	}

	private boolean statementBalanceLessThanOrEqualToZero() {
		return product.getStatementBalance().compareTo(BigDecimal.ZERO) <= 0;
	}

	private void showMinimumPaymentReceivedMessage(View minimumPaymentReceivedMessage) {
		minimumPaymentReceivedMessage.setVisibility(View.VISIBLE);
	}

	private boolean statementBalanceGreaterThanZero() {
		return product.getStatementBalance().compareTo(BigDecimal.ZERO) == 1;
	}

	private void showProductNotCycledMessage(TescoTextView productNotCycled) {
		productNotCycled.setVisibility(View.VISIBLE);
	}

	private void showPaymentReceivedMessage(View paymentReceivedMessage) {
		paymentReceivedMessage.setVisibility(View.VISIBLE);
	}

	private void showMinimumPayment(View productMinimumPayment, TextView minimumPayment) {
		productMinimumPayment.setVisibility(View.VISIBLE);
		minimumPayment.setText(dataFormatter.formatCurrency(product.getMinimumPayment()));
		minimumPayment.setVisibility(View.VISIBLE);
	}

	private void showDueDate(View productDueBy, TextView dueBy) {
		productDueBy.setVisibility(View.VISIBLE);
		dueBy.setText(dataFormatter.formatDate(product.getPaymentDue()));
		dueBy.setVisibility(View.VISIBLE);
	}

	private boolean minimumPaymentLessThanZero() {
		return null != product.getMinimumPayment() && product.getMinimumPayment().compareTo(BigDecimal.ZERO) == -1;
	}

	private boolean noCycleDatesPresent() {
		return product.getCycleDates() == null || product.getCycleDates().isEmpty();
	}

	private boolean minimumPaymentEqualToZero() {
		// PIA-74: null === zero
		return null == product.getMinimumPayment() || product.getMinimumPayment().compareTo(BigDecimal.ZERO) == 0;
	}

	private boolean minimumPaymentGreaterThanZero() {
		return null != product.getMinimumPayment() && product.getMinimumPayment().compareTo(BigDecimal.ZERO) > 0;
	}

	protected View inflateListRow(LayoutInflater inflater) {
		return inflater.inflate(R.layout.carousel_product_content_cc, null);
	}

	public static boolean isCCProduct(ProductType productType) {
		switch (productType) {
		case CC_PRODUCT:
			return true;
		default:
			return false;
		}
	}

	public static boolean isLoanProduct(ProductType productType) {
		switch (productType) {
		case LOAN_PRODUCT:
			return true;
		default:
			return false;
		}
	}

	public static boolean isPcaProduct(ProductType productType) {
		switch (productType) {
		case PCA_PRODUCT:
			return true;
		default:
			return false;
		}
	}

	public static boolean isOtherProduct(ProductType productType) {
		switch (productType) {
		case FRS_PRODUCT:
		case IAS_PRODUCT:
		case IS_PRODUCT:
		case IAI_PRODUCT:
		case FISA_PRODUCT:
		case JISA_PRODUCT:
		case FJISA_PRODUCT:
		case CCP_PRODUCT:
			return true;
		default:
			return false;
		}
	}

	public View createView() {

		View content = null;

		if (isStandardProduct()) {
			content = updateContentForStandardAccount(inflater);
		} else if (isCCProduct(productType)) {
			content = updateContentForCreditCard(inflater);
		} else if (isIsaProduct()) {
			content = updateContentForISA(inflater);
		} else if (isFixedIsaProduct()) {
			content = updateContentForFixedISA(inflater);
		} else if (isLoanProduct(productType)) {
			content = updateContentForLoan(inflater);
		}

		return content;
	}

	public void setProduct(ProductDetails product) {
		this.product = product;
		this.productType = typeForLabel(product.getProductType());
	}

}
