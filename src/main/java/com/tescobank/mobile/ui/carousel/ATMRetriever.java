package com.tescobank.mobile.ui.carousel;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.storefinder.Store;
import com.tescobank.mobile.application.TescoLocationProvider;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.atm.ATMProvider;
import com.tescobank.mobile.ui.atm.ATMProvider.ATMRetrievalStatus;

public class ATMRetriever {

	private static final String TAG = ATMRetriever.class.getSimpleName();

	private TescoActivity activity;
	private BroadcastReceiver locationChangedReceiver;
	private BroadcastReceiver storeBroadcastReceiver;
	private LocationUpdateListener locationUpdateListener;
	private TescoLocationProvider locationProvider;
	private StoresUpdateListener storesUpdateListener;
	private List<Store> stores;

	public ATMRetriever(TescoActivity activity, LocationUpdateListener locationUpdateListener, StoresUpdateListener storesUpdateListener) {
		this.activity = activity;
		this.locationUpdateListener = locationUpdateListener;
		this.storesUpdateListener = storesUpdateListener;
		locationProvider = TescoActivity.class.cast(activity).getApplicationState().getLocationProvider();
	}

	private void registerLocationReceiver() {
		if (null != locationChangedReceiver) {
			return;
		}

		locationChangedReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				start();
			}
		};
		LocalBroadcastManager.getInstance(activity).registerReceiver(locationChangedReceiver, new IntentFilter(TescoLocationProvider.LOCATION_STATUS_BROADCAST_EVENT));
	}

	private void retrieveStores(Location location) {
		LatLng latLng = CarouselActivity.latLngFromLocation(location);
		activity.getApplicationState().getATMProvider().retrieveCachableATMsForLatLng(latLng);
	}

	private void registerStoreReceiver() {
		if (null != storeBroadcastReceiver) {
			return;
		}

		storeBroadcastReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {

				ATMRetrievalStatus status = (ATMRetrievalStatus) intent.getSerializableExtra(ATMProvider.ATM_RETRIEVAL_STATUS_EXTRA);
				status = (status == null ? ATMRetrievalStatus.FAIL : status);
				switch (status) {
				case SUCCESS:
					setStores(CarouselActivity.getStoresFromIntent(intent));
					storesUpdateListener.onRetrievedATMs();
					break;
				case FAIL:
					storesUpdateListener.onFailedToRetrieveATMs();
					break;
				}
			}
		};
		LocalBroadcastManager.getInstance(activity).registerReceiver(storeBroadcastReceiver, new IntentFilter(ATMProvider.ATM_RETRIEVAL_BROADCAST_EVENT));
	}

	private void setStores(List<Store> stores) {
		this.stores = stores;
	}

	public void start() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "start()");
		}

		registerLocationReceiver();
		registerStoreReceiver();

		TescoLocationProvider.Status status = locationProvider.getLocationStatus();
		switch (status) {
		case OK:
			locationUpdateListener.onLocationIsAvailable();
			retrieveStores(locationProvider.getLocation());
			break;
		case IN_PROGRESS:
			break;
		case SYSTEM_DISABLED:
			locationUpdateListener.onLocationSystemDisabled();
			break;
		case USER_DISABLED:
			locationUpdateListener.onLocationUserDisabled();
			break;
		}
	}

	public List<Store> getStores() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "getStores()");
		}
		return stores;
	}

	public void pause() {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "pause()");
		}

		if (locationChangedReceiver != null) {
			LocalBroadcastManager.getInstance(activity).unregisterReceiver(locationChangedReceiver);
			locationChangedReceiver = null;
		}

		if (storeBroadcastReceiver != null) {
			LocalBroadcastManager.getInstance(activity).unregisterReceiver(storeBroadcastReceiver);
			storeBroadcastReceiver = null;
		}
	}
}
