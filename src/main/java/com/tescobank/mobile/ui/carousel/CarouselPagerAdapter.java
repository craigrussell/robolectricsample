package com.tescobank.mobile.ui.carousel;

import static com.tescobank.mobile.model.product.ProductType.typeForLabel;
import static com.tescobank.mobile.ui.carousel.CarouselProductHintHelper.NEXT_ITEM_HINT;
import static com.tescobank.mobile.ui.carousel.CarouselProductHintHelper.PREVIOUS_ITEM_HINT;

import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.model.product.ProductType;

public class CarouselPagerAdapter extends FragmentStatePagerAdapter {

	private static final String TAG = CarouselPagerAdapter.class.getSimpleName();

	private List<ProductDetails> products;

	private SparseArray<ProductFragment> productFragments = new SparseArray<ProductFragment>();
	private boolean isAtmEnabled;
	private String atmFinderTitle;

	public CarouselPagerAdapter(Context ctx, FragmentManager manager, List<ProductDetails> products, boolean isAtmEnabled) {
		super(manager);
		atmFinderTitle = ctx.getResources().getString(R.string.carousel_atm_page_title);
		this.products = products;
		this.isAtmEnabled = isAtmEnabled;
	}

	final String getItemTitle(int position) {
		if (isAtm(position)) {
			return "";
		} else {
			Product product = products.get(isAtmEnabled ? position - 1 : position);
			return product.getProductName();
		}
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return isAtm(position) ? atmFinderTitle : getItemTitle(position);
	}

	@Override
	public Fragment getItem(int position) {
		Fragment fragment;
		if (isAtm(position)) {
			fragment = buildATMFragment(position);
		} else {
			fragment = buildProductFragment(position);
		}
		return fragment;
	}

	private Bundle getFragmentArgs(int position) {
		Bundle args = new Bundle();
		args.putSerializable(PREVIOUS_ITEM_HINT, getItemHint(position - 1));
		args.putSerializable(NEXT_ITEM_HINT, getItemHint(position + 1));
		return args;
	}

	private Fragment buildATMFragment(int position) {
		Fragment fragment = new ATMFragment();
		fragment.setArguments(getFragmentArgs(position));
		return fragment;
	}

	private Fragment buildProductFragment(int position) {
		Fragment fragment = getFragment(position);
		if (fragment == null) {
			fragment = new ProductFragment();
			productFragments.put(Integer.valueOf(position), (ProductFragment) fragment);
			Bundle args = getFragmentArgs(position);
			Product product = products.get(isAtmEnabled ? position - 1 : position);
			args.putSerializable(Product.class.getName(), product);
			fragment.setArguments(args);
		}
		return fragment;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		super.destroyItem(container, position, object);
		productFragments.remove(Integer.valueOf(position));
	}

	private void reloadProductFragments() {
		for (int i = 0; i < productFragments.size(); i++) {
			int key = productFragments.keyAt(i);
			ProductFragment fragment = productFragments.get(key);
			fragment.updateProduct(products.get(isAtmEnabled ? key - 1 : key));
		}
	}

	public ProductFragment getFragment(int key) {
		if (isAtm(key)) {
			return null;
		}
		return productFragments.get(key);
	}

	public String getItemHint(int position) {
		if (hasNoPreviousItem(position) || hasNoNextItem(position)) {
			return "";
		}
		if (isAtm(position)) {
			return CarouselProductHintHelper.ATM_LABEL;
		}

		if (isAtmEnabled) {
			return products.get(position - 1).getProductType();
		} else {
			return products.get(position == products.size() ? position - 1 : position).getProductType();
		}
	}

	private boolean hasNoPreviousItem(int position) {
		return position < 0;
	}

	private boolean hasNoNextItem(int position) {
		return isAtmEnabled ? position - 1 >= products.size() : position >= products.size();
	}

	@Override
	public int getCount() {
		return isAtmEnabled ? products.size() + 1 : products.size();
	}

	/**
	 * Get the menu resource for the specified item or -1 if none should be shown
	 * 
	 * @return the menu resource or -1
	 */
	public int getMenuResource(int position) {
		if (isAtm(position)) {
			return -1;
		}

		Product product = isAtmEnabled ? products.get(position - 1) : products.get(position);
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "productType: " + product.getProductType());
		}

		ProductType type = typeForLabel(product.getProductType());
		switch (type) {
		case PCA_PRODUCT:
			return R.menu.flyout_menu_current_account;
		case CC_PRODUCT:
			return R.menu.flyout_menu_credit_card_flat;
		default:
			return R.menu.flyout_menu_product_default;
		}
	}
	
	private boolean isAtm(int position) {
		return isAtmEnabled && 0 == position;
	}

	public void updateProducts(List<ProductDetails> products) {
		this.products = products;
		reloadProductFragments();
	}
}
