package com.tescobank.mobile.ui.carousel;

public interface LocationUpdateListener {
	void onLocationIsAvailable();
	void onLocationSystemDisabled();
	void onLocationUserDisabled();
}
