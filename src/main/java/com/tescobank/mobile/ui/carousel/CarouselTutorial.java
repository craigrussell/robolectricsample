package com.tescobank.mobile.ui.carousel;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.FrameLayout;

import com.tescobank.mobile.R;

public class CarouselTutorial implements OnClickListener, AnimationListener {

	private CarouselActivity carouselActivity;
	private View tutorialLayout;

	public CarouselTutorial(CarouselActivity carouselActivity) {
		this.carouselActivity = carouselActivity;
	}

	public void show() {

		tutorialLayout = carouselActivity.getLayoutInflater().inflate(R.layout.tooltip, null);

		FrameLayout layout = FrameLayout.class.cast(carouselActivity.findViewById(android.R.id.content));
		layout.addView(tutorialLayout);

		tutorialLayout.findViewById(R.id.tutorial_content).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		v.setOnClickListener(null);
		Animation anim = new AlphaAnimation(1.0f, 0.0f);
		anim.setDuration(500);
		anim.setAnimationListener(this);
		tutorialLayout.startAnimation(anim);
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		FrameLayout layout = FrameLayout.class.cast(carouselActivity.findViewById(android.R.id.content));
		layout.removeView(tutorialLayout);
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
	}

	@Override
	public void onAnimationStart(Animation animation) {
	}

}
