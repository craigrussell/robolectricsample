package com.tescobank.mobile.ui.carousel;

import static java.lang.String.format;
import static java.lang.String.valueOf;
import android.view.View;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.storefinder.Store;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoFragment;

public class CarouselListATMDisplayer extends ATMDisplayer {
	private TescoActivity activity;

	public CarouselListATMDisplayer(TescoActivity activity, TescoFragment fragment) {
		super(activity, fragment);
		this.activity = activity;
	}

	@Override
	public void displayNearestStoreInformation(Store nearestStore) {
		if (nearestStore != null) {
			String distance = valueOf(nearestStore.getStoreMetadata().getDistanceInMiles());
			getNearest().setText(format(activity.getString((R.string.carousel_list_atm_nearest_title), distance)));
			String address = format("%s\n%s\n%s", nearestStore.getStoreMetadata().getAddress1(), nearestStore.getStoreMetadata().getTownCity(), nearestStore.getStoreMetadata().getPostcode());
			getContent().setText(address);
			getTitle().setVisibility(View.GONE);
		}
	}
}
