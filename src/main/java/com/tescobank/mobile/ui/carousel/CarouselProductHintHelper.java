package com.tescobank.mobile.ui.carousel;

import static com.tescobank.mobile.model.product.ProductType.typeForLabel;

import com.tescobank.mobile.R;
import com.tescobank.mobile.model.product.ProductType;

public final class CarouselProductHintHelper {

	public static final String ATM_LABEL = "ATM Finder";
	public static final String PREVIOUS_ITEM_HINT = "PREVIOUS_ITEM_HINT";
	public static final String NEXT_ITEM_HINT = "NEXT_ITEM_HINT";
	
	private CarouselProductHintHelper() {
		super();
	}
	
	public static int getLeftProductHintImageId(String hintLabel) {

		if (hintLabel.equals(ATM_LABEL)) {
			return R.drawable.carousel_atm_left_chevron;
		}
		
		ProductType productType = typeForLabel(hintLabel);
		if (productType == null) {
			return -1;
		}
		
		return getLeftProductHintImageId(productType);
	}
	
	private static int getLeftProductHintImageId(ProductType productType) {
		if (isPcaProduct(productType)) {
			return R.drawable.carousel_pca_left_chevron;
		} else if (isCCProduct(productType)) {
			return R.drawable.carousel_cc_left_chevron;
		} else if (isLoanProduct(productType)) {
			return R.drawable.carousel_loan_left_chevron;
		} else if (isOtherProduct(productType)) {
			return R.drawable.carousel_sav_left_chevron;
		} else {
			return -1;
		}
	}
	
	public static int getRightProductHintImageId(String hintLabel) {
		
		ProductType productType = typeForLabel(hintLabel);
		if (productType == null) {
			return -1;
		}
		
		return getRightProductHintImageId(productType);
	}
	
	private static int getRightProductHintImageId(ProductType productType) {
		if (isPcaProduct(productType)) {
			return R.drawable.carousel_pca_right_chevron;
		} else if (isCCProduct(productType)) {
			return R.drawable.carousel_cc_right_chevron;
		} else if (isLoanProduct(productType)) {
			return R.drawable.carousel_loan_right_chevron;
		} else if (isOtherProduct(productType)) {
			return R.drawable.carousel_sav_right_chevron;
		} else {
			return -1;
		}
	}
	
	private static boolean isPcaProduct(ProductType type) {
		switch (type) {
		case PCA_PRODUCT:
			return true;
		default:
			return false;
		}
	}
	
	private static boolean isCCProduct(ProductType type) {
		switch (type) {
		case CC_PRODUCT:
			return true;
		default:
			return false;
		}
	}
	
	private static boolean isLoanProduct(ProductType type) {
		switch (type) {
		case LOAN_PRODUCT:
			return true;
		default:
			return false;
		}
	}
	
	private static boolean isOtherProduct(ProductType type) {
		switch (type) {
		case FRS_PRODUCT:		
		case IAS_PRODUCT:
		case IS_PRODUCT:
		case IAI_PRODUCT:
		case JISA_PRODUCT:
		case FISA_PRODUCT:
		case FJISA_PRODUCT:
		case CCP_PRODUCT:
			return true;
		default:
			return false;
		}
	}
}
