package com.tescobank.mobile.ui.carousel;

import android.view.View;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.overlay.ResultOverlayRenderer;

/**
 * Renders result overlays required by the carousel activity
 */
public class CarouselResultOverlayRenderer extends ResultOverlayRenderer {

	public static final String OVERLAY_MESSAGE_KEY = "OVERLAY_MESSAGE_KEY";
	private String message;

	public CarouselResultOverlayRenderer(CarouselActivity carouselActivity) {
		super(carouselActivity);

	}

	public void renderSuccessOverlay(String message, OnDismissListener listener) {
		
		if (message != null) {
			View overlay = getActivity().getLayoutInflater().inflate(R.layout.result_overlay, null);
			TextView text = (TextView) overlay.findViewById(R.id.result_message);
			text.setText(message);
			renderOverlay(overlay, true, listener);
		}
	}
	
	@Override
	public boolean dismissOverlay() {
		boolean dismissDialog = super.dismissOverlay();
		setMessage(null);
		return dismissDialog;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
