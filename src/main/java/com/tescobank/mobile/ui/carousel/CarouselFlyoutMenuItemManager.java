package com.tescobank.mobile.ui.carousel;

import static com.tescobank.mobile.model.product.ProductType.CCP_PRODUCT;
import static com.tescobank.mobile.model.product.ProductType.CC_PRODUCT;
import static com.tescobank.mobile.model.product.ProductType.PCA_PRODUCT;
import static com.tescobank.mobile.model.product.ProductType.typeForLabel;

import java.util.List;

import android.content.res.Resources;

import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.configuration.FeaturesResponse;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.model.product.ProductType;
import com.tescobank.mobile.ui.calendar.CalendarMenuDayDrawer;
import com.tescobank.mobile.ui.fomenu.FlyoutMenuItemProvider;
import com.tescobank.mobile.ui.movemoney.transfer.TransferableProducts;

public class CarouselFlyoutMenuItemManager {
	
	private FlyoutMenuItemProvider flyoutMenu;
	private FeaturesResponse features;
	private Resources resources;
	private ProductDetails currentProduct;
	private TransferableProducts transferableProducts = new TransferableProducts();
	
	public CarouselFlyoutMenuItemManager(FlyoutMenuItemProvider flyoutMenu, FeaturesResponse features, Resources resources, List<ProductDetails> products) {
		this.flyoutMenu = flyoutMenu;
		this.features = features;
		this.resources = resources;
		transferableProducts.setProducts(products);
	}
	
	public void setFlyoutMenu(FlyoutMenuItemProvider flyoutMenu) {
		this.flyoutMenu = flyoutMenu;
	}

	public void updateMenu(ProductDetails currentProduct) {
		this.currentProduct = currentProduct;
		applyMenuItems();
	}

	private void buildCalendarMenuItem() {
		MenuItem calendarMenuItem = flyoutMenu.findItem(R.id.flyout_menu_item_pca_transaction_calendar);
		if (calendarMenuItem != null) {
			int textSize = resources.getInteger(R.integer.flyout_calendar_day_text_size);
			int yOffset = resources.getInteger(R.integer.flyout_calendar_day_y_offset);
			CalendarMenuDayDrawer drawer = new CalendarMenuDayDrawer(textSize, yOffset);
			drawer.drawTodaysDay(resources, calendarMenuItem);
		}
	}

	private void applyMenuItems() {
		buildCalendarMenuItem();
		applyTransferMenuItem();
		applyPaymentMenuItem();
		applySavingMenuItem();
		applyArchiveStatementMenuItem();
		applyPayCCWithDebitMenuItem();
	}
	
	private void applyPayCCWithDebitMenuItem() {
		MenuItem paymentMenuItem = flyoutMenu.findItem(R.id.flyout_menu_item_creditcard_pay_with_debitcard);
		if (null != paymentMenuItem){
			boolean visible = isCreditCardProduct(currentProduct.getProductType()) && features.isPayCCBillWithDebitCardEnabled()
					&& !"L1".equals(currentProduct.getAccountStatusCode());
			paymentMenuItem.setVisible(visible);
		}
	}
	
	private boolean isCreditCardProduct(String productType) {
		return productType.equals(CC_PRODUCT.getLabel()) || productType.equals(CCP_PRODUCT.getLabel()); 
	}

	private void applyTransferMenuItem() {
		MenuItem transferMenuItem = flyoutMenu.findItem(R.id.flyout_menu_item_transfers);
		if (null != transferMenuItem) {
			boolean visible = isPaymentsEnabled() && transferableProducts.canTransferFrom(currentProduct);
			transferMenuItem.setVisible(visible);
		}
	}

	private void applyPaymentMenuItem() {
		MenuItem paymentMenuItem = flyoutMenu.findItem(R.id.flyout_menu_item_payments);
		if (null != paymentMenuItem) {
			boolean visible = isPaymentsEnabled() && currentProduct.getCanBeSource();
			paymentMenuItem.setVisible(visible);
		}
	}

	private void applySavingMenuItem() {
		MenuItem savingMenuItem = flyoutMenu.findItem(R.id.flyout_menu_item_savings);
		if (null != savingMenuItem) {
			boolean canBeDestination = currentProduct.getCanBeDestination();
			boolean isNotClubCardPlus = !CCP_PRODUCT.equals(typeForLabel(currentProduct.getProductType()));
			boolean visible = canBeDestination && isNotClubCardPlus && features.isSaveNowEnabled();
			savingMenuItem.setVisible(visible);
		}
	}

	private void applyArchiveStatementMenuItem() {
		MenuItem archiveStatementMenuItem = flyoutMenu.findItem(R.id.flyout_menu_item_archive_statement);
		if (archiveStatementMenuItem != null) {
			ProductType productType = typeForLabel(currentProduct.getProductType());
			List<String> cycleDates = currentProduct.getCycleDates();
			boolean hasCurrentAccountStatements = hasCurrentAccountStatementsEnabled(productType);
			boolean hasCreditCardStatements = hasCreditCardStatementsEnabled(productType);
			boolean hasCycleDates = (cycleDates != null) && !cycleDates.isEmpty();
			boolean visible = (hasCurrentAccountStatements || hasCreditCardStatements) && hasCycleDates;
			archiveStatementMenuItem.setVisible(visible);
		}
	}

	private boolean hasCreditCardStatementsEnabled(ProductType productType) {
		return CC_PRODUCT.equals(productType) && features.isViewStatement();
	}
	
	private boolean hasCurrentAccountStatementsEnabled(ProductType productType) {
		return PCA_PRODUCT.equals(productType) && features.isViewCurrentAccountStatement();
	}
	
	private boolean isPaymentsEnabled() {
		return features.isImmediatePaymentsEnabled() || features.isFuturePaymentsEnabled();
	}
	
}
