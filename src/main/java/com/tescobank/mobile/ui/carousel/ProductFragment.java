package com.tescobank.mobile.ui.carousel;

import static com.tescobank.mobile.ui.ActivityRequestCode.REQUEST_DEFAULT;
import static com.tescobank.mobile.ui.transaction.TransactionsActivity.TRANSACTIONS_PRODUCT_EXTRA;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.model.product.ProductType;
import com.tescobank.mobile.ui.HideAndShowProgressIndicator;
import com.tescobank.mobile.ui.TescoFragment;
import com.tescobank.mobile.ui.transaction.TransactionsActivity;

public class ProductFragment extends TescoFragment {

	private static final String TAG = ProductFragment.class.getSimpleName();

	private ProductDetails product;
	private String previousItemHint;
	private String nextItemHint;

	private View rootView;
	private ImageView mainImageView;
	private ImageView middleImageView;
	private ImageView leftImageView;
	private ImageView rightImageView;
	private ViewGroup contentContainer;
	private ProductType productType;

	private LayoutInflater inflater;
	
	private ProductDisplayer displayer;
	
	private CarouselDataSetter carouselDataSetter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		product = (ProductDetails) getArguments().getSerializable(Product.class.getName());
		productType = ProductType.typeForLabel(product.getProductType());
		previousItemHint = getArguments().getString(CarouselProductHintHelper.PREVIOUS_ITEM_HINT);
		nextItemHint = getArguments().getString(CarouselProductHintHelper.NEXT_ITEM_HINT);

		rootView = inflater.inflate(R.layout.fragment_carousel_product, null, false);
		rootView.setTag(product.getProductType());
		
		mainImageView = (ImageView) rootView.findViewById(R.id.product_img_main);
		middleImageView = (ImageView) rootView.findViewById(R.id.product_img_middle);
		leftImageView = (ImageView) rootView.findViewById(R.id.product_img_left);
		rightImageView = (ImageView) rootView.findViewById(R.id.product_img_right);
		contentContainer = (ViewGroup) rootView.findViewById(R.id.product_content_containter);
		carouselDataSetter = new CarouselDataSetter(product, getResources());

		displayer = new ProductDisplayer(inflater, product, carouselDataSetter, getResources());

		mainImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				openTransactions();
			}
		});

		updateProductContent();		
		updateMainImages();
		updateLeftImage();
		updateRightImage();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Fragment created for product " + product.getProductName());
		}

		return rootView;
	}

	public void updateProduct(ProductDetails product) {
		this.product = product;
		carouselDataSetter.setProduct(product);
		displayer.setProduct(product);
		updateProductContent();
	}

	private void openTransactions() {
		Intent intent = new Intent(getActivity(), TransactionsActivity.class);
		intent.putExtra(TRANSACTIONS_PRODUCT_EXTRA, product);
		startActivityForResult(intent, REQUEST_DEFAULT);
	}

	private void updateProductContent() {
		View content = displayer.createView();
		
		contentContainer.removeAllViews();
		
		if (content == null) {
			middleImageView.setVisibility(ImageView.GONE);
			mainImageView.setVisibility(ImageView.GONE);
		}

		addProductContent(content);
		updateProductContentContainer();
	}
	
	
	private void addProductContent(View content) {
		if (content != null) {
			contentContainer.addView(content);
		}
	}
	
	private void updateProductContentContainer() {
		
		ImageView imageView = (ImageView) rootView.findViewById(R.id.progressIndicatorImage);
		HideAndShowProgressIndicator hideAndShowProgressIndicator = new HideAndShowProgressIndicator(imageView);
		
		if (product.isDirty()) {
			contentContainer.setVisibility(View.GONE);
			hideAndShowProgressIndicator.showInProgressIndicator();
		} else {
			contentContainer.setVisibility(View.VISIBLE);
			hideAndShowProgressIndicator.hideInProgressIndicator();
		}
	}



	private void updateMainImages() {
		
		if (ProductDisplayer.isPcaProduct(productType)) {
			mainImageView.setImageDrawable(getResources().getDrawable(R.drawable.carousel_pca_img));
			middleImageView.setImageDrawable(getResources().getDrawable(R.drawable.carousel_pca_middle));
		} else if (ProductDisplayer.isCCProduct(productType)) {
			mainImageView.setImageDrawable(getResources().getDrawable(R.drawable.carousel_cc_img));
			middleImageView.setImageDrawable(getResources().getDrawable(R.drawable.carousel_cc_middle));
		} else if (ProductDisplayer.isLoanProduct(productType)) {
			mainImageView.setImageDrawable(getResources().getDrawable(R.drawable.carousel_loans_img));
			middleImageView.setImageDrawable(getResources().getDrawable(R.drawable.carousel_loan_middle));
		} else if (ProductDisplayer.isOtherProduct(productType)) {
			mainImageView.setImageDrawable(getResources().getDrawable(R.drawable.carousel_savings_img));
			middleImageView.setImageDrawable(getResources().getDrawable(R.drawable.carousel_sav_middle));
		}
	}
	

	private void updateLeftImage() {

		int hintImageId = CarouselProductHintHelper.getLeftProductHintImageId(previousItemHint);
		if (hintImageId != -1) {
			leftImageView.setImageDrawable(getResources().getDrawable(hintImageId));
			leftImageView.setVisibility(ImageView.VISIBLE);
		}
	}

	private void updateRightImage() {

		int hintImageId = CarouselProductHintHelper.getRightProductHintImageId(nextItemHint);
		if (hintImageId != -1) {
			rightImageView.setImageDrawable(getResources().getDrawable(hintImageId));
			rightImageView.setVisibility(ImageView.VISIBLE);
		}
	}

	public void setUserVisibleHint(boolean isVisibleToUser) {

		if (isVisibleToUser && inflater != null) {
			updateProductContent();
		}
		
	}

}