package com.tescobank.mobile.ui.carousel;

public interface StoresUpdateListener {
	void onRetrievedATMs();
	void onFailedToRetrieveATMs();
}
