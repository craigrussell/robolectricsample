package com.tescobank.mobile.ui.carousel;

import android.content.res.Resources;
import android.view.View;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.PayNoPayProductDisplay;
import com.tescobank.mobile.ui.TescoTextView;

/**
 * Helper used to set product data on carousel UI components
 */
public class CarouselDataSetter {

	private ProductDetails product;

	private Resources resources;

	private DataFormatter dataFormatter;

	public CarouselDataSetter(ProductDetails product, Resources resources) {
		super();

		this.product = product;
		this.resources = resources;
		this.dataFormatter = new DataFormatter();
	}

	public void setProductName(View content) {
		TescoTextView productName = (TescoTextView) content.findViewById(R.id.product_name);
		productName.setText(product.getProductName());
	}

	public void setBalance(View content) {
		TescoTextView balance = (TescoTextView) content.findViewById(R.id.product_balance);
		balance.setText(dataFormatter.formatCurrency(product.getBalance()));
	}

	public void setAvailableCredit(View content) {
		TescoTextView availableCredit = (TescoTextView) content.findViewById(R.id.available_credit);
		availableCredit.setText(dataFormatter.formatCurrency(product.getAvailableCredit()));
	}

	public void setPayNoPay(View content) {
		PayNoPayProductDisplay pnpDisplay = new PayNoPayProductDisplay(product);
		if (pnpDisplay.shouldDisplay()) {
			View pnpAmountContainer = content.findViewById(R.id.product_pnp_amount_due);
			View pnpExtraContainer = content.findViewById(R.id.product_pnp_extra_needed);
			TescoTextView pnpAmount = (TescoTextView) content.findViewById(R.id.product_pnp_amount_due_value);
			TescoTextView pnpExtra = (TescoTextView) content.findViewById(R.id.product_pnp_extra_needed_value);
			pnpAmountContainer.setVisibility(View.VISIBLE);
			pnpExtraContainer.setVisibility(View.VISIBLE);
			pnpAmount.setText(dataFormatter.formatCurrency(product.getAmountOfPaymentsGoingOut()));
			pnpExtra.setText(dataFormatter.formatCurrency(product.getAmountDue()));
		}
	}

	public void setOverdraftLimit(View content) {
		TescoTextView overdraftLimit = (TescoTextView) content.findViewById(R.id.product_overdraft_limit_value);
		overdraftLimit.setText(dataFormatter.formatCurrency(product.getOverdraftLimit()));
	}

	public void setAvailableBalance(View content) {
		TescoTextView availableBalance = (TescoTextView) content.findViewById(R.id.product_available_balance_value);
		availableBalance.setText(dataFormatter.formatCurrency(null == product.getAvailableBalance() ? null : product.getAvailableBalance().abs()));
	}

	public void setGrossInterestRate(View content) {
		TescoTextView grossInterestRate = (TescoTextView) content.findViewById(R.id.product_interest_rate_value);
		grossInterestRate.setText(dataFormatter.formatPercentageRoundedToTwoDecimalPlaces(product.getInterestRate()));
	}

	public void setSubscriptionAmountSubscribed(View content) {
		TescoTextView subscriptionAmountSubscribed = (TescoTextView) content.findViewById(R.id.product_isa_subscribed_value);
		subscriptionAmountSubscribed.setText(dataFormatter.formatCurrency(product.getSubscriptionAmountSubscribed()));
	}

	public void setSubscriptionAmountAvailable(View content) {
		TescoTextView subscriptionAmountAvailable = (TescoTextView) content.findViewById(R.id.product_isa_remaining_value);
		subscriptionAmountAvailable.setText(dataFormatter.formatCurrency(product.getSubscriptionAmountAvailable()));
	}

	public void setSubscriptionLimit(View content) {
		TescoTextView subscriptionLimit = (TescoTextView) content.findViewById(R.id.product_isa_limit_value);
		subscriptionLimit.setText(dataFormatter.formatCurrency(product.getSubscriptionLimit()));
	}

	public void setTaxYearStart(View content) {
		TescoTextView taxYearStart = (TescoTextView) content.findViewById(R.id.current_start_tax_year_value);
		taxYearStart.setText(dataFormatter.formatDate(product.getTaxYearStart()));
	}

	public void setTaxYearEnd(View content) {
		TescoTextView taxYearEnd = (TescoTextView) content.findViewById(R.id.current_end_tax_year_value);
		taxYearEnd.setText(dataFormatter.formatDate(product.getTaxYearEnd()));
	}

	public void setNextPaymentAmount(View content) {
		TescoTextView nextPaymentAmount = (TescoTextView) content.findViewById(R.id.product_next_payment_amt_value);
		nextPaymentAmount.setText(dataFormatter.formatCurrency(product.getNextPaymentAmount()));
	}

	public void setFinalPaymentDate(View content) {
		TescoTextView finalPaymentDate = (TescoTextView) content.findViewById(R.id.product_final_payment_date_value);
		finalPaymentDate.setText(dataFormatter.formatDate(product.getFinalPaymentDate()));
	}

	public void setSortCodeAndAccountNumber(View content) {

		String sortCodeDesc = resources.getString(R.string.product_sort_code_desc);
		String acccountDesc = resources.getString(R.string.product_account_number_desc);

		TescoTextView sortCode = (TescoTextView) content.findViewById(R.id.product_sort_code);
		TescoTextView accountNum = (TescoTextView) content.findViewById(R.id.product_account_number);

		sortCode.setText(dataFormatter.formatSortCode(product.getSortCode()));
		sortCode.setContentDescription(sortCodeDesc + " " + product.getSortCode());

		accountNum.setText(product.getAccountNumber());
		accountNum.setContentDescription(acccountDesc + " " + product.getAccountNumber());
	}

	public ProductDetails getProduct() {
		return product;
	}

	public void setProduct(ProductDetails product) {
		this.product = product;
	}
}
