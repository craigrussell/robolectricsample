package com.tescobank.mobile.ui.carousel;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.analytics.TescoAnalyticsTracker;
import com.tescobank.mobile.model.product.Product;

public class CarouselProductAnalyticHelper {

	private static final String TAG = CarouselProductAnalyticHelper.class.getSimpleName();

	protected static final int PRODUCT_ANALYTIC_QUIET_PERIOD = 2 * 1000;

	private ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
	private Future<?> trackerFuture;

	private Product product;
	private TescoAnalyticsTracker tracker;

	public CarouselProductAnalyticHelper(TescoAnalyticsTracker tracker) {
		this.tracker = tracker;
	}

	public void track(final Product product) {
		cancel();
		this.product = product;

		trackerFuture = scheduledExecutorService.submit(new Runnable() {
			@Override
			public void run() {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "waiting two seconds before firing carousel ADMS tracking for product: " + product.getProductId());
				}

				try {
					Thread.sleep(PRODUCT_ANALYTIC_QUIET_PERIOD);
					finishTracking();
				} catch (InterruptedException e) {
					if (BuildConfig.DEBUG) {
						Log.d(TAG, "carousel ADMS tracking for product: " + product.getProductId() + " was cancelled");
					}
				}
			}
		});

	}

	public void finishTracking() {
		cancel();

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "firing carousel ADMS tracking for product: " + product.getProductId() + " was cancelled");
		}
		tracker.trackCarouselProductShown(product, false);
	}

	public void cancel() {
		if (null != trackerFuture) {
			trackerFuture.cancel(true);
			trackerFuture = null;
		}
	}

}