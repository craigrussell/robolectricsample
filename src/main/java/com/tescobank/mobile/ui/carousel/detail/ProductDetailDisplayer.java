package com.tescobank.mobile.ui.carousel.detail;

import static com.tescobank.mobile.model.product.ProductType.typeForLabel;
import android.app.Dialog;
import android.content.Context;

import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.model.product.ProductType;

/**
 * Displays detailed information for a product
 */
public final class ProductDetailDisplayer {
	
	private ProductDetailDisplayer() {
		super();
	}
	
	/**
	 * Displays detailed information for the specified product
	 * 
	 * @param product the product
	 * @param context the context
	 * 
	 * @return the dialog
	 */
	public static Dialog displayProductDetail(ProductDetails product, Context context) {
		
		ProductType productType = typeForLabel(product.getProductType());
		Dialog dialog = getDialog(product, context, productType);
		
		if (dialog != null) {
			dialog.show();
		}
		return dialog;
	}
	
	private static Dialog getDialog(ProductDetails product, Context context, ProductType productType) {
		
		Dialog dialog = null;
		
		if (isCCProduct(productType)) {
			dialog = new CreditCardDetail(product, context);
		} else if (isPCAProduct(productType)) {
			dialog = new PCADetail(product, context);
		} else if (isSaverProduct(productType)) {
			dialog = new SaverDetail(product, context);
		} else if (isClubCardPlusProduct(productType)) {
			dialog = new ClubCardPlusDetail(product, context);
		} else if (isFixedRateSaverProduct(productType)) {
			dialog = new FixedRateSaverDetail(product, context);
		} else if (isISAProduct(productType)) {
			dialog = new ISADetail(product, context);
		} else if (isLoanProduct(productType)) {
			dialog = new LoanDetail(product, context);
		}
		
		return dialog;
	}
	
	private static boolean isCCProduct(ProductType productType) {
		switch (productType) {
		case CC_PRODUCT:
			return true;
		default:
			return false;
		}
	}
	
	private static boolean isPCAProduct(ProductType productType) {
		switch (productType) {
		case PCA_PRODUCT:
			return true;
		default:
			return false;
		}
	}
	
	private static boolean isSaverProduct(ProductType productType) {
		switch (productType) {
		case IAS_PRODUCT:
		case IS_PRODUCT:
			return true;
		default:
			return false;
		}
	}
	
	private static boolean isClubCardPlusProduct(ProductType productType) {
		switch (productType) {
		case CCP_PRODUCT:
			return true;
		default:
			return false;
		}
	}
	
	private static boolean isFixedRateSaverProduct(ProductType productType) {
		switch (productType) {
		case FRS_PRODUCT:
			return true;
		default:
			return false;
		}
	}
	
	private static boolean isISAProduct(ProductType productType) {
		switch (productType) {
		case IAI_PRODUCT:
		case JISA_PRODUCT:
		case FISA_PRODUCT:
		case FJISA_PRODUCT:
			return true;
		default:
			return false;
		}
	}
	
	private static boolean isLoanProduct(ProductType productType) {
		switch (productType) {
		case LOAN_PRODUCT:
			return true;
		default:
			return false;
		}
	}
}
