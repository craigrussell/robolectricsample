package com.tescobank.mobile.ui.carousel;

import static com.tescobank.mobile.model.product.ProductType.typeForLabel;

import java.util.List;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.model.product.ProductType;
import com.tescobank.mobile.ui.TescoTextView;

public class CarouselListBuilder {

	private List<ProductDetails> products;
	private LayoutInflater inflater;
	private Resources resources;
	private ProductDetails product;
	private ProductType productType;
	private boolean atmEnabled;
	
	public CarouselListBuilder(Resources resources, LayoutInflater inflater, List<ProductDetails> products, boolean atmEnabled) {
		this.resources = resources;
		this.inflater = inflater;
		this.products = products;
		this.atmEnabled = atmEnabled;
	}

	public int getCount() {
		return products.size() + (atmEnabled ? 1 : 0);
	}
	
	public View createView(int position) {
		if(isATM(position)) {
			return createATMRowView();
		} else {
			product =  products.get(atmEnabled ? position - 1 : position);
			setProduct(product);
			return createProductRowView();
		} 
	}
	
	private View createATMRowView() {
		View content = inflateAtmListRow(inflater);
		
		if(content != null) {
			ImageView  imageView = (ImageView)content.findViewById(R.id.image);
			imageView.setImageDrawable(resources.getDrawable(R.drawable.list_atm_img));
		}
		return content;
	}
	
	private View createProductRowView() {
		View content = inflateProductListRow(inflater);

		if (content != null) {
			updateMainImages((ImageView)content.findViewById(R.id.image));
			updateContentForProduct(content);
			updateProductRowColour(content);
		}
		return content;
	}

	private void updateProductRowColour(View content) {
		View productRowColour = content.findViewById(R.id.product_colour);
		if (ProductDisplayer.isPcaProduct(productType)) {
			productRowColour.setBackgroundColor(resources.getColor(R.color.pca_product));
		} else if (ProductDisplayer.isCCProduct(productType)) {
			productRowColour.setBackgroundColor(resources.getColor(R.color.cc_product));
		} else if (ProductDisplayer.isLoanProduct(productType)) {
			productRowColour.setBackgroundColor(resources.getColor(R.color.loan_product));
		} else if (ProductDisplayer.isOtherProduct(productType)) {
			productRowColour.setBackgroundColor(resources.getColor(R.color.savings_product));
		}
	}

	private void updateMainImages(ImageView image) {
		if (ProductDisplayer.isPcaProduct(productType)) {
			image.setImageDrawable(resources.getDrawable(R.drawable.list_pca_img));
		} else if (ProductDisplayer.isCCProduct(productType)) {
			image.setImageDrawable(resources.getDrawable(R.drawable.list_cc_img));
		} else if (ProductDisplayer.isLoanProduct(productType)) {
			image.setImageDrawable(resources.getDrawable(R.drawable.list_loans_img));
		} else if (ProductDisplayer.isOtherProduct(productType)) {
			image.setImageDrawable(resources.getDrawable(R.drawable.list_savings_img));
		}
	}
	
	protected View updateContentForProduct(View content) {

		CarouselDataSetter carouselDataSetter = new CarouselDataSetter(product, resources);
		DataFormatter dataFormatter = new DataFormatter();
		
		carouselDataSetter.setProductName(content);
		carouselDataSetter.setBalance(content);
		TescoTextView productId = (TescoTextView) content.findViewById(R.id.product_id_number);
		ImageView ccImage = (ImageView) content.findViewById(R.id.product_creditcard_Image);
		
		if(ProductDisplayer.isCCProduct(productType)) {
			productId.setText(DataFormatter.formatCardNumber(product.getCardNumber()));
			ccImage.setImageResource(R.drawable.ic_little_credit_card);
			ccImage.setVisibility(View.VISIBLE);
			return content;
		} 
		
		if (ProductDisplayer.isLoanProduct(productType)) {
			amountRemainingOnLoan(content, dataFormatter);
		} 	
		
		updateStandardProductContent(dataFormatter, productId, ccImage);
		return content;
	}

	private void amountRemainingOnLoan(View content, DataFormatter dataFormatter) {
		TescoTextView amountRemaining = (TescoTextView) content.findViewById(R.id.product_balance);
		amountRemaining.setText(dataFormatter.formatCurrency(product.getAmountRemaining()));
	}

	private void updateStandardProductContent(DataFormatter dataFormatter, TescoTextView productId, ImageView ccImage) {
		productId.setText(dataFormatter.sortCodeAndAccoutNumberJoiner(product.getSortCode(), product.getAccountNumber()));
		ccImage.setVisibility(View.GONE);
	}
	

	private View inflateProductListRow(LayoutInflater inflater) {
		return inflater.inflate(R.layout.carousel_list_row, null);
	}
	
	private View inflateAtmListRow(LayoutInflater inflater) {
		return inflater.inflate(R.layout.carousel_list_atm_row, null);
	}
	
	public void setProduct(ProductDetails product) {
		this.product = product;
		this.productType = typeForLabel(product.getProductType());
	}
	
	private boolean isATM(int position) {
		return atmEnabled && position == 0; 
	}
}
