package com.tescobank.mobile.ui.carousel.detail;

import android.content.Context;
import android.view.View;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.TescoTextView;
import com.tescobank.mobile.ui.carousel.CarouselDataSetter;

/**
 * Displays detailed information for a credit card
 */
public class CreditCardDetail extends ProductDetail {

	public CreditCardDetail(ProductDetails product, Context context) {
		super(product, context);
	}

	@Override
	protected int getContentViewId() {
		return R.layout.carousel_product_detail_creditcard_table;
	}

	@Override
	protected void populateFromProduct(ProductDetails product) {
		DataFormatter dataFormatter = new DataFormatter();
		
		CarouselDataSetter carouselDataSetter = getCarouselDataSetter();
		View detailView = getModal();
		
		carouselDataSetter.setBalance(detailView);
		carouselDataSetter.setAvailableCredit(detailView);
		
		TescoTextView creditLimit = (TescoTextView)findViewById(R.id.credit_limit);
		creditLimit.setText(dataFormatter.formatCurrency(product.getCreditLimit()));
	}
}
