package com.tescobank.mobile.ui.carousel;

import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.util.Log;
import android.view.accessibility.AccessibilityManager;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.payment.GetTransacteesForUserRequest;
import com.tescobank.mobile.api.payment.GetTransacteesForUserResponse;
import com.tescobank.mobile.api.payment.RetrieveFundingAccountsRequest;
import com.tescobank.mobile.api.payment.RetrieveFundingAccountsResponse;
import com.tescobank.mobile.api.product.RetrieveProductDetailsRequest;
import com.tescobank.mobile.api.product.RetrieveProductDetailsResponse;
import com.tescobank.mobile.api.product.RetrieveProductsRequest;
import com.tescobank.mobile.api.product.RetrieveProductsResponse;
import com.tescobank.mobile.filter.KnownProductFilter;
import com.tescobank.mobile.helper.ProductHelper;
import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.sort.TescoProductComparator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.auth.ProductDataLoadedListener;
import com.tescobank.mobile.ui.seasonal.SeasonalAnimationImagePreloader;
import com.tescobank.mobile.ui.seasonal.SeasonalLogoImagePreloader;

public class CarouselLoader {
	private static final String TAG = CarouselLoader.class.getSimpleName();

	private final TescoActivity activity;
	private boolean productsFailed;
	private final ProductDataLoadedListener productDataLoadedListener;

	public CarouselLoader(TescoActivity activity) {
		this(activity, null);
	}

	public CarouselLoader(TescoActivity activity, ProductDataLoadedListener productDataLoadedListener) {
		super();
		this.activity = activity;
		this.productDataLoadedListener = productDataLoadedListener;
	}

	/**
	 * Load the carousel.
	 * 
	 * @param isSubsequentLogin
	 *            true if this was as the result of a login, false otherwise (e.g. registration/reset password/passcode/etc)
	 */
	public void loadCarousel(boolean isSubsequentLogin) {
		retrieveProducts();
		new SeasonalAnimationImagePreloader(activity, null).start();
		new SeasonalLogoImagePreloader(activity, null).start();
		productsFailed = false;
		if (isSubsequentLogin) {
			activity.getTooltipManager().newSession();
			activity.getApplicationState().incrementSubsequentLoginCount();
		}
	}

	public boolean didProductsFail() {
		return productsFailed;
	}

	public void retryLoadCarousel() {
		if (activity.getApplicationState().getProducts() == null) {
			retrieveProducts();
		} else if (activity.getApplicationState().getProductDetails() == null) {
			retrieveProductDetails(activity.getApplicationState().getProducts());
		} else if (activity.getApplicationState().getFundingAccounts() == null && hasBankingProducts()) {
			retrieveFundingAccounts(activity.getApplicationState().getProductDetails());
		} else if (activity.getApplicationState().getTransactees() == null && hasBankingProducts()) {
			retrieveTransactees();
		} 
	}

	private void retrieveProducts() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting retrieveProducts");
		}

		RetrieveProductsRequest request = new RetrieveProductsRequest() {

			@Override
			public void onResponse(RetrieveProductsResponse response) {
				KnownProductFilter<Product> filter = new KnownProductFilter<Product>(response.getProducts());
				activity.getApplicationState().setProducts(filter.getKnownProducts());
				retrieveProductDetails(response.getProducts());
			}

			@Override
			public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
				activity.handleErrorResponse(errorResponseWrapper);
				productsFailed = true;
			}
		};

		request.setTbID(activity.getApplicationState().getTbId());
		activity.getApplicationState().getRestRequestProcessor().processRequest(request, activity.getClass());
	}

	private void retrieveProductDetails(List<Product> products) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting retrieveProductDetails");
		}

		RetrieveProductDetailsRequest request = new LoaderRetrieveProductDetailsRequest();
		request.setTbID(activity.getApplicationState().getTbId());
		request.setProductIdentifiers(products);
		activity.getApplicationState().getRestRequestProcessor().processRequest(request, activity.getClass());
	}

	private void retrieveFundingAccounts(final List<ProductDetails> productDetails) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting retrieveFundingAccounts");
		}

		RetrieveFundingAccountsRequest request = new RetrieveFundingAccountsRequest() {

			@Override
			public void onResponse(RetrieveFundingAccountsResponse response) {
				activity.getApplicationState().setFundingAccounts(response.getAccounts());
				ProductHelper.updateProductDetailsWithFundingAccounts(productDetails, response.getAccounts());
				retrieveTransactees();
			}

			@Override
			public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
				activity.handleErrorResponse(errorResponseWrapper);
				productsFailed = true;
			}
		};

		request.setTbId(activity.getApplicationState().getTbId());
		activity.getApplicationState().getRestRequestProcessor().processRequest(request, activity.getClass());
	}

	private void retrieveTransactees() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting retrieveTransactees");
		}

		GetTransacteesForUserRequest request = new GetTransacteesForUserRequest() {

			@Override
			public void onResponse(GetTransacteesForUserResponse response) {
				activity.getApplicationState().setTransactees(response.getTransactee());
				if (productDataLoadedListener != null) {
					productDataLoadedListener.onProductDataLoadComplete();
				}
			}

			@Override
			public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
				activity.handleErrorResponse(errorResponseWrapper);
				productsFailed = true;
			}
		};

		request.setTbID(activity.getApplicationState().getTbId());
		activity.getApplicationState().getRestRequestProcessor().processRequest(request, activity.getClass());
	}

	public void trackLoginAnalytic() {
		boolean accessibilityEnabled;
		AccessibilityManager am = (AccessibilityManager) activity.getSystemService(Activity.ACCESSIBILITY_SERVICE);
		if(am!=null) {
			accessibilityEnabled = am.isEnabled();
		} else {
			accessibilityEnabled = false;
		}

		List<ProductDetails> products = activity.getApplicationState().getProductDetails();
		activity.getApplicationState().getAnalyticsTracker().trackCarouselCompletedLogin(products, accessibilityEnabled);
	}

	private boolean hasBankingProducts() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting has Banking products");
		}

		for (Product product : activity.getApplicationState().getProducts()) {
			if (product.isBankingProduct()) {
				return true;
			}
		}
		return false;
	}

	private class LoaderRetrieveProductDetailsRequest extends RetrieveProductDetailsRequest {

		@Override
		public void onResponse(RetrieveProductDetailsResponse response) {
			KnownProductFilter<ProductDetails> filter = new KnownProductFilter<ProductDetails>(response.getProducts());
			Collections.sort(filter.getKnownProducts(), new TescoProductComparator());
			List<ProductDetails> productDetails = ProductHelper.updateProductDetailsWithSummaryData(activity.getApplicationState().getProducts(), filter.getKnownProducts());
			activity.getApplicationState().setProductDetails(productDetails);
			if (hasBankingProducts()) {
				retrieveFundingAccounts(productDetails);
			} else {
				if (productDataLoadedListener != null) {
					productDataLoadedListener.onProductDataLoadComplete();
				}
			}
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
			activity.handleErrorResponse(errorResponseWrapper);
			productsFailed = true;
		}
	};
}
