package com.tescobank.mobile.ui.carousel.detail;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.model.product.FundingAccount;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.TescoTextView;

/**
 * Abstract base class for product details dialogs
 */
public abstract class SavingsProductDetail extends ProductDetail {

	private static final int MULTI_ACCOUNT_HEIGHT = 56;
	private static final int DOUBLE_ACCOUNT_HEIGHT = 38;
	private static final int SINGLE_ACCOUNT_HEIGHT = 20;

	public SavingsProductDetail(ProductDetails product, Context context) {
		super(product, context);
	}

	protected void populateViewTable(LayoutInflater layoutInflater, View container, List<FundingAccount> fundingAccounts) {

		TescoTextView nameText;
		TescoTextView sortCodeAccountNumberText;
		TableRow tableRow;
		ScrollView scrollView;
		int dpiHeight = 0;
		
		TextView linkedTitle = (TextView) container.findViewById(R.id.product_linked_account_title);
		linkedTitle.setVisibility(View.VISIBLE);
		linkedTitle.setText(getContext().getString((fundingAccounts.size() > 1) ? R.string.product_detail_linked_accounts : R.string.product_detail_linked_account)); 
		scrollView = (ScrollView)container.findViewById(R.id.linked_account_scrollbar);
		scrollView.setVisibility(View.VISIBLE);
		
		float density = getContext().getResources().getDisplayMetrics().density;
		
		if (fundingAccounts.size() == 1) {
			dpiHeight = (int) (SINGLE_ACCOUNT_HEIGHT * density);
			scrollView.setLayoutParams(new  LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,   dpiHeight));
		} else if (fundingAccounts.size() == 2) {
			dpiHeight = (int) (DOUBLE_ACCOUNT_HEIGHT * density);
			scrollView.setLayoutParams(new  LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,   dpiHeight));
		} else {
			dpiHeight = (int) (MULTI_ACCOUNT_HEIGHT * density);
			scrollView.setLayoutParams(new  LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,   dpiHeight));
		}
		
		TableLayout tableContainer = (TableLayout) container.findViewById(R.id.linked_account_tableLayout);
		tableContainer.setVisibility(View.VISIBLE);
		tableContainer.setBackgroundResource(R.drawable.carousel_product_detail_linked_account_background_tableshape);

		for (FundingAccount fundingAccount : fundingAccounts) {

			tableRow = (TableRow) layoutInflater.inflate(R.layout.carousel_product_detail_linked_account_tablerow, tableContainer, false);

			nameText = (TescoTextView) tableRow.findViewById(R.id.linked_account_name_value);
			nameText.setText(fundingAccount.getName());

			sortCodeAccountNumberText = (TescoTextView) tableRow.findViewById(R.id.linked_account_sortcode_accountnumber_value);
			sortCodeAccountNumberText.setText(fundingAccount.getAccountIdentifier());

			tableContainer.addView(tableRow);
		}
	}
}
