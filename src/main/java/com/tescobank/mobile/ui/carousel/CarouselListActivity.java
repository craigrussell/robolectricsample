package com.tescobank.mobile.ui.carousel;

import static com.tescobank.mobile.ui.ActionBarConfiguration.setToStdHamburger;
import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_CAROUSEL_LIST_COMPLETED;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.errors.ErrorHandler;
import com.tescobank.mobile.ui.settings.AuthenticatedSettingsFragment;

public class CarouselListActivity extends TescoActivity implements ErrorHandler {

	private final static String TAG = CarouselListActivity.class.getSimpleName();
	public static final String PRODUCT_POSITION = "productPosition";
	private ActionBar actionBar;
	private List<ProductDetails> products;
	private View viewAtPosition;
	private int position;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating Product List Activity");
		}
		super.onCreate(savedInstanceState);

		configureSettingsDrawer();
		configureActionBar();
		configureProductsFromIntentExtra();
		configureProductListView(position = getIntent().getIntExtra(PRODUCT_POSITION, 0));
	}

	private void configureSettingsDrawer() {
		AuthenticatedSettingsFragment authenticatedSettingsFragment = new AuthenticatedSettingsFragment();
		getDrawer().setContentViewWithDrawer(R.layout.activity_view_product_list, authenticatedSettingsFragment);
		authenticatedSettingsFragment.initialise(this);
	}

	@SuppressWarnings("unchecked")
	private void configureProductsFromIntentExtra() {
		products = (List<ProductDetails>) getIntent().getSerializableExtra(CarouselActivity.PRODUCTS);
	}

	private void configureActionBar() {
		actionBar = getSupportActionBar();
		setToStdHamburger(this, actionBar);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		if (hasFocus) {
			focusOnView();
		}
	}

	private void focusOnView() {
		ScrollView scrollView = (ScrollView) findViewById(R.id.view_product_scroll);
		int top = viewAtPosition.getTop();
		scrollView.scrollTo(0, top);
	}


	private void configureProductListView(int position) {
		LinearLayout container = (LinearLayout) findViewById(R.id.view_product_list);

		viewAtPosition = null;
		boolean atmEnabled = getApplicationState().getFeatures().isAtmStoreFinderEnabled();
		CarouselListBuilder builder = new CarouselListBuilder(getResources(), getLayoutInflater(), products, atmEnabled);
		for (int i = 0; i < builder.getCount(); i++) {

			View view = builder.createView(i);
			if (i == position) {
				viewAtPosition = view;
			}
			view.setTag(i);
			container.addView(view);
		}
	}

	public void onRowClick(View view) {
		Integer position = (Integer) view.getTag();
		returnToCarouselActivityForSelectedProduct(position);

	}

	protected void returnToCarouselActivityForSelectedProduct(int position) {
		Intent intent = new Intent(this, CarouselActivity.class);
		intent.putExtra(PRODUCT_POSITION, position);
		finishWithResult(RESULT_CAROUSEL_LIST_COMPLETED, intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
		getTooltipManager().show();
		getDrawer().refresh();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.product_list, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onOptionsItemSelected: " + item);
		}

		if (R.id.menu_item_carousel == item.getItemId()) {
			getApplicationState().getAnalyticsTracker().trackToggleToCarousel();
			returnToCarouselActivityForSelectedProduct(position);
		}

		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	public void handleError(ErrorResponseWrapper errorResponseWrapper) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Error");
		}
		handleErrorResponse(errorResponseWrapper);
	}

}
