package com.tescobank.mobile.ui.carousel.detail;

import android.content.Context;
import android.view.View;

import com.tescobank.mobile.R;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.carousel.CarouselDataSetter;

/**
 * Displays detailed information for a loan
 */
public class LoanDetail extends ProductDetail {
	
	public LoanDetail(ProductDetails product, Context context) {
		super(product, context);
	}
	
	@Override
	protected int getContentViewId() {
		return R.layout.carousel_product_detail_loan_table;
	}
	
	@Override
	protected void populateFromProduct(ProductDetails product) {
		
		CarouselDataSetter carouselDataSetter = getCarouselDataSetter();
		View detailView = getModal();
		
		carouselDataSetter.setNextPaymentAmount(detailView);
		carouselDataSetter.setFinalPaymentDate(detailView);
		carouselDataSetter.setGrossInterestRate(detailView);
	}
}
