package com.tescobank.mobile.ui;

import static android.app.Activity.RESULT_FIRST_USER;

/**
 * Supported activity result codes
 */
public final class ActivityResultCode {
	
	private ActivityResultCode() {
		super();
	}

	private static int value = RESULT_FIRST_USER;
	
    // Authentication
    public static final int RESULT_BACK_ON_PASSWORD_CHANGED_ONLINE = ++value;
    public static final int RESULT_TOO_MANY_SMS_REQUESTED = ++value;
    
    // Errors
    public static final int RESULT_RETRY_PRESSED = ++value;
    public static final int RESULT_BACK_FROM_RETRY_PRESSED = ++value;
        
    // Carousel
    public static final int RESULT_CAROUSEL_LOADED = ++value;
	public static final int RESULT_CAROUSEL_LIST_COMPLETED = ++value;
    
    // Pay CC
	public static final int RESULT_THREE_D_SECURE_COMPLETED = ++value;
	public static final int RESULT_THREE_D_SECURE_CANCELLED = ++value;
	public static final int RESULT_THREE_D_SECURE_ERROR = ++value;
	public static final int RESULT_CC_PAYMENT_COMPLETED = ++value;
	public static final int RESULT_CC_PAYMENT_FAILED = ++value;
	public static final int RESULT_CC_PAYMENT_CANCELLED = ++value;
    public static final int RESULT_CC_CARD_ADDED = ++value;
	
	// ATM
	public static final int RESULT_ATM_LIST_WITH_SELECTED_STORE = ++value;
	public static final int RESULT_ATM_LIST_WITH_LIST_OF_STORES = ++value;
	
	// Move Money
	public static final int RESULT_MOVE_MONEY_SUCCESS = ++value;
	
	// Savings
	public static final int RESULT_SAVINGS_FUNDING_SELECTED = ++value;
}
