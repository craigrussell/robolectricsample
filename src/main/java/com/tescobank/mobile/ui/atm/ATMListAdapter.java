package com.tescobank.mobile.ui.atm;

import java.text.DecimalFormat;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.storefinder.Store;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoTextView;

public class ATMListAdapter extends BaseAdapter {

	private List<Store> stores;
	private static LayoutInflater inflater = null;
	private TescoActivity activity;

	public ATMListAdapter(TescoActivity activity, List<Store> stores) {
		this.stores = stores;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.activity = activity;
	}

	public int getCount() {
		return stores.size();
	}

	public Object getItem(int position) {
		return stores.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View atmListRow, ViewGroup parent) {
		View atmListRowView = atmListRow;

		if (atmListRow == null) {
			atmListRowView = inflater.inflate(R.layout.activity_atm_list_row, null);
		}
		
		TescoTextView storeName = (TescoTextView) atmListRowView.findViewById(R.id.atm_store_name);
		TescoTextView storeDistance = (TescoTextView) atmListRowView.findViewById(R.id.atm_store_distance);
		Store currentStore = stores.get(position);

		// Setting all values in listview
		storeName.setText(currentStore.getStoreMetadata().getBranchName());
		String distance = new DecimalFormat("#.##").format(currentStore.getStoreMetadata().getDistanceInMiles());
		storeDistance.setText(String.format(activity.getResources().getString(R.string.atm_list_store_distance), distance));
		return atmListRowView;
	}

}
