package com.tescobank.mobile.ui.atm;

import static com.tescobank.mobile.ui.ActivityRequestCode.REQUEST_ATM_LIST_ACTIVITY;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnCloseListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.api.storefinder.Store;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.ActivityRequestCode;
import com.tescobank.mobile.ui.ActivityResultCode;
import com.tescobank.mobile.ui.SearchViewHelperFactory;
import com.tescobank.mobile.ui.SearchViewHelperFactory.SearchViewHelper;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoTextView;
import com.tescobank.mobile.ui.atm.ATMProvider.ATMRetrievalStatus;
import com.tescobank.mobile.ui.atm.ATMSearchHelper.Callback;
import com.tescobank.mobile.ui.carousel.CarouselActivity;
import com.tescobank.mobile.ui.carousel.DirectionsHelper;

public class ATMMapActivity extends TescoActivity implements InfoWindowAdapter, OnCameraChangeListener, OnMapClickListener, OnMarkerClickListener, OnInfoWindowClickListener, OnMyLocationButtonClickListener {

	private static final String TAG = ATMMapActivity.class.getSimpleName();
	
	public static final String BUNDLE_KEY_SELECTED_STORE = "selectedStore";
	public static final String BUNDLE_KEY_STORES = "stores";
	public static final String BUNDLE_KEY_STORE_DETAILS_VISIBILITY = "storeDetailsVisibility";
	public static final String BUNDLE_KEY_DIRECTION_HELPER_SHOWING = "directionHelperDialogShowing";
	public static final String BUNDLE_KEY_SEARCH_VIEW_ICONIFIED = "searchViewIconified";
	public static final String BUNDLE_KEY_SEARCH_VIEW_SEARCH_TEXT = "searchViewSearchText";
	public static final String BUNDLE_KEY_CAMERA_POSITION = "cameraPosition";

	private static final int STORE_LOCATION_PADDING = 100;
	private static final float STORE_LOCATION_ZOOM = 15.0f;
	private static final double HUNDRED = 100.0;
	private static final int TEN = 10;
	private static final double METRES_PER_MILE = 1600.0;
	private static final float USER_LOCATION_ZOOM = 15f;
	private static final String HYPHEN = " - ";
	

	/** If a store is selected show the info balloon. */
	private Store store;

	/** The list of stores to show. */
	private List<Store> stores;
	private Map<String, Store> storeMarkerLookup;
	private Map<String, Marker> markerStoreLookup;

	private GoogleMap map;

	private CameraPosition cameraPosition;

	private View infoWindow;

	private MenuItem directions;

	private BroadcastReceiver storesBroadcastReceiver;

	private MenuItem changeToList;

	private Marker lastMarker;

	private boolean shouldFitBoundsForUser;

	private LatLng referencePoint;

	private String query;

	private SearchViewHelper searchViewHelper;

	private boolean storesReceivedFromATMLIstActivity;

	private boolean searchInvoked;

	private boolean recalculateDistances;

	private boolean myLocationButtonClicked;

	private DirectionsHelper directionsHelper;
	
	private boolean searchViewIconified;
	
	private String searchViewQueryText;

	private boolean closeSearchView;

	private Bundle savedInstanceState;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		setContentView(R.layout.activity_atm_map);
		findViewById(R.id.atm_details).setVisibility(View.GONE);
		setupMap();
		infoWindow = getLayoutInflater().inflate(R.layout.map_infowindow_atm, null);
		shouldFitBoundsForUser = getIntent().getBooleanExtra(ATMSearchHelper.FIT_BOUNDS_FOR_USER_EXTRA_NAME, false);
		setReferencePoint((LatLng) getIntent().getParcelableExtra(ATMSearchHelper.REFERENCE_POINT));

		extractStoresFromIntent();

		if (null != map) {
			map.setOnMapClickListener(this);
			map.setOnMarkerClickListener(this);
			map.setOnInfoWindowClickListener(this);
			map.setOnCameraChangeListener(this);
			map.setOnMyLocationButtonClickListener(this);

			map.setMyLocationEnabled(true);
			map.getUiSettings().setMyLocationButtonEnabled(true);

			populatePinLocations();
			populateAndToggleStoreDetails();		
			setDirectionsHelper(new DirectionsHelper(this));
			
			if(null != savedInstanceState) {
				this.savedInstanceState = savedInstanceState;
				restoreMapState(savedInstanceState);
			} else {
				setSearchViewIconified(true);
				zoomToUserLocation();
				zoomToStoreLocation();
			}
		}
	}

	private void restoreMapState(Bundle savedInstanceState) {
		ATMMapActivityStateRestorer atmMapActivityStateRestorer = new ATMMapActivityStateRestorer(this, savedInstanceState);
		atmMapActivityStateRestorer.restoreSavedData();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(BUNDLE_KEY_SELECTED_STORE, store);
		outState.putSerializable(BUNDLE_KEY_STORES, (Serializable) stores);
		outState.putInt(BUNDLE_KEY_STORE_DETAILS_VISIBILITY, findViewById(R.id.atm_details).getVisibility());
		outState.putBoolean(BUNDLE_KEY_DIRECTION_HELPER_SHOWING, directionsHelper.isShowing());

		if(null != map) {
			outState.putParcelable(BUNDLE_KEY_CAMERA_POSITION, map.getCameraPosition());
		}
		
		if(null != searchViewHelper) {
			outState.putBoolean(BUNDLE_KEY_SEARCH_VIEW_ICONIFIED, searchViewHelper.isIconified());
			outState.putString(BUNDLE_KEY_SEARCH_VIEW_SEARCH_TEXT, searchViewHelper.getQuery());
		}	
	}
	

	@Override
	public void onBackPressed() {

		boolean callSuper = !searchViewHelper.handleBackPressed();
		
		// always show the > icon again
		infoWindow.findViewById(R.id.atm_infowindow_image).setVisibility(View.VISIBLE);
		
		if (findViewById(R.id.atm_details).getVisibility() == View.VISIBLE) {
			findViewById(R.id.atm_details).setVisibility(View.GONE);
			map.getUiSettings().setMyLocationButtonEnabled(true);
			callSuper = false;
		} else if (lastMarker != null) {
			lastMarker.hideInfoWindow();
			lastMarker = null;
			showNavigationAndHideListIcon(false);
			callSuper = false;
		}

		if (callSuper) {
			super.onBackPressed();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (null != map) {
			registerStoreReceiver();
		}

		if (null != store) {
			getApplicationState().getAnalyticsTracker().trackATMDetail(store);
		} else {
			getApplicationState().getAnalyticsTracker().trackATMFinderMap();
		}
	}

	@Override
	protected void onPause() {
		deregisterStoreReceiver();
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		if(null != directionsHelper) {
			directionsHelper.dismissDirectionsDialog();	
		}	
		super.onDestroy();
	}
	
	@Override
	public View getInfoContents(Marker marker) {
		return null;
	}

	@Override
	public View getInfoWindow(Marker marker) {
		if (store == null) {
			return null;
		}
		showNavigationAndHideListIcon(true);
		popupateInfoWindow(store);
		return infoWindow;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onOptionsItemSelected: " + item.getItemId());
		}

		if (changeToList.equals(item)) {
			Intent intent = new Intent(this, ATMListActivity.class);
			intent.putExtra(ATMSearchHelper.STORES_EXTRA_NAME, new LinkedList<Store>(stores));
			startActivityForResult(intent, REQUEST_ATM_LIST_ACTIVITY);
			return true;
		}
		
		if (directions.equals(item)) {
			launchGoogleDirections();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void launchGoogleDirections() {
		Location location = getApplicationState().getLocationProvider().getLocation();
		if(location != null && store.getStoreMetadata() != null) {
			directionsHelper.showDirections(location.getLatitude(), location.getLongitude(), store.getStoreMetadata().getLatitude(), store.getStoreMetadata().getLongitude());
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getSupportMenuInflater().inflate(R.menu.atm_map, menu);

		directions = menu.findItem(R.id.menu_item_navigate);
		directions.setVisible(store != null);

		changeToList = menu.findItem(R.id.menu_item_change_to_list);
		changeToList.setVisible(store == null);

		searchViewHelper = SearchViewHelperFactory.create((SearchView) menu.findItem(R.id.menu_item_search).getActionView());
		searchViewHelper.setIconified(searchViewIconified);
		
		if(null != searchViewQueryText && searchViewQueryText.length() > 0) {
			searchViewHelper.setQuery(searchViewQueryText);
		}
		
		searchViewHelper.setSubmitQueryCallback(new ATMSearchHelper(this, getApplicationState(), new Callback() {

			@Override
			public void locationReceived(String query, LatLng location) {
				ATMMapActivity.this.query = query;
				setReferencePoint(location);
				searchInvoked = true;
				setStore(null);
				populateAndToggleStoreDetails();
			}

			@Override
			public void handleError(ErrorResponseWrapper errorResponseWrapper) {
				handleErrorResponse(errorResponseWrapper);
			}

			@Override
			public void noResults() {
				handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.LocationSearchNoResults));
			}
		}));

		searchViewHelper.setOnCloseListener(new OnCloseListener() {

			@Override
			public boolean onClose() {
				closeSearchView = true;
				Location location = getApplicationState().getLocationProvider().getLocation();
				setReferencePoint(new LatLng(location.getLatitude(), location.getLongitude()));
				calculateDistanceToStoresAndSort(stores, referencePoint.latitude, referencePoint.longitude);
				if (null != store) {
					// if a store has been set a reference to it may not be in the list due to serialisation
					calculateDistanceToStoresAndSort(Arrays.asList(store), referencePoint.latitude, referencePoint.longitude);
				}
				populateAndToggleStoreDetails();
				return false;
			}
		});

		return true;
	}
	
	@Override
	public void onCameraChange(CameraPosition newCameraPosition) {
		
		if(null != savedInstanceState) {
			cameraPosition = savedInstanceState.getParcelable(BUNDLE_KEY_CAMERA_POSITION);
			map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));	
			savedInstanceState = null;
			return;
		}

		if (storesReceivedFromATMLIstActivity) {
			storesReceivedFromATMLIstActivity = false;
			return;
		}

		if (cameraPosition == null) {
			zoomToUserNearestStoreBounds();
			cameraPosition = newCameraPosition;
			return;
		}

		if (wasRefreshThresholdReached(newCameraPosition)) {
			// set query to null else the view will zoom
			query = null;
			cameraPosition = newCameraPosition;

			if (myLocationButtonClicked) {
				setReferencePoint(cameraPosition.target);
				recalculateDistances = false;
				myLocationButtonClicked = false;
			} else {
				recalculateDistances = true;
			}
			getApplicationState().getATMProvider().retrieveATMsForLatLng(newCameraPosition.target);
		}
	}

	@Override
	public void onMapClick(LatLng latLong) {
		if (null != store) {
			setStore(null);
			lastMarker = null;
			populateAndToggleStoreDetails();
			showNavigationAndHideListIcon(false);
		}
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		setStore(storeMarkerLookup.get(marker.getId()));
		infoWindow.findViewById(R.id.atm_infowindow_image).setVisibility(View.VISIBLE);
		if (View.VISIBLE == findViewById(R.id.atm_details).getVisibility()) {
			populateAndToggleStoreDetails();
		}
		lastMarker = marker;
		return false;
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
		populateAndToggleStoreDetails();
	}

	@Override
	public boolean onMyLocationButtonClick() {
		myLocationButtonClicked = true;
		return false;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == ActivityRequestCode.REQUEST_ATM_LIST_ACTIVITY && null != map) {

			if (resultCode == ActivityResultCode.RESULT_ATM_LIST_WITH_SELECTED_STORE) {
				setStore((Store) data.getSerializableExtra(ATMSearchHelper.SELECTED_STORE_EXTRA_NAME));

				commonActivtyResulProcessing(data);
				populatePinLocations();
				markerStoreLookup.get(store.getStoreMetadata().getBranchID()).showInfoWindow();
				populateAndToggleStoreDetails();

			} else if (resultCode == ActivityResultCode.RESULT_ATM_LIST_WITH_LIST_OF_STORES) {
				commonActivtyResulProcessing(data);
			} else {
				super.onActivityResult(requestCode, resultCode, data);
			}
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}

	}

	private boolean wasRefreshThresholdReached(CameraPosition newCameraPosition) {

		Location previousLocation = new Location("P");
		previousLocation.setLatitude(cameraPosition.target.latitude);
		previousLocation.setLongitude(cameraPosition.target.longitude);

		Location newLocation = new Location("N");
		newLocation.setLatitude(newCameraPosition.target.latitude);
		newLocation.setLongitude(newCameraPosition.target.longitude);

		float distance = previousLocation.distanceTo(newLocation);

		if (BuildConfig.DEBUG) {
			Log.v(TAG, "Pan distance in meters: " + distance);
		}

		return distance > ATMSearchHelper.PAN_REFRESH_DISTANCE_THRESHOLD_METERS;
	}

	private void zoomToUserLocation() {
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(referencePoint, USER_LOCATION_ZOOM));
	}

	private void calculateDistanceToStoresAndSort(List<Store> stores, double lat, double lon) {

		for (Store theStore : stores) {
			double distanceInMiles = distanceTo(theStore, lat, lon) / METRES_PER_MILE;
			if (distanceInMiles > TEN) {
				distanceInMiles = Math.round(distanceInMiles);
			} else {
				distanceInMiles = Math.round(distanceInMiles * HUNDRED) / HUNDRED;
			}
			theStore.getStoreMetadata().setDistanceInMiles(distanceInMiles);
		}

		// sort the stores by distance
		Collections.sort(stores, new Comparator<Store>() {
			@Override
			public int compare(Store lhs, Store rhs) {
				if (lhs.getStoreMetadata().getDistanceInMiles() < rhs.getStoreMetadata().getDistanceInMiles()) {
					return -1;
				}
				return 1;
			}
		});

	}

	private double distanceTo(Store store, double lat, double lon) {
		float[] results = new float[1];
		Location.distanceBetween(store.getStoreMetadata().getLatitude(), store.getStoreMetadata().getLongitude(), lat, lon, results);
		return results[0];
	}

	private void registerStoreReceiver() {
		storesBroadcastReceiver = new ATMBroadcastReceiver();
		LocalBroadcastManager.getInstance(this).registerReceiver(storesBroadcastReceiver, new IntentFilter(ATMProvider.ATM_RETRIEVAL_BROADCAST_EVENT));
	}

	private void deregisterStoreReceiver() {
		if (storesBroadcastReceiver != null) {
			LocalBroadcastManager.getInstance(this).unregisterReceiver(storesBroadcastReceiver);
		}
	}

	protected void zoomToStoreLocation() {
		if (null != store) {
			LatLng position = new LatLng(store.getStoreMetadata().getLatitude(), store.getStoreMetadata().getLongitude());
			map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, STORE_LOCATION_ZOOM));
		}
	}

	private void zoomToUserNearestStoreBounds() {
		// a store is selected, so don't do this
		if (null != store || !shouldFitBoundsForUser) {
			return;
		}

		if (stores.isEmpty()) {
			return;
		}

		Store theStore = stores.get(0);
		LatLng nearestStorePosition = new LatLng(theStore.getStoreMetadata().getLatitude(), theStore.getStoreMetadata().getLongitude());

		// if the map is already showing the
		if (map.getProjection().getVisibleRegion().latLngBounds.contains(nearestStorePosition)) {
			return;
		}

		LatLngBounds bounds = LatLngBounds.builder().include(referencePoint).include(nearestStorePosition).build();
		map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, STORE_LOCATION_PADDING));
	}

	private void setupMap() {
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		map = mapFragment.getMap();
	}

	protected void populatePinLocations() {
		storeMarkerLookup = new HashMap<String, Store>();
		markerStoreLookup = new HashMap<String, Marker>();

		map.clear();
		map.setInfoWindowAdapter(this);
		for (Store theStore : stores) {
			LatLng position = new LatLng(theStore.getStoreMetadata().getLatitude(), theStore.getStoreMetadata().getLongitude());
			MarkerOptions markerOptions = new MarkerOptions().position(position).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_action_bar));
			Marker marker = map.addMarker(markerOptions);
			storeMarkerLookup.put(marker.getId(), theStore);
			markerStoreLookup.put(theStore.getStoreMetadata().getBranchID(), marker);
			if (this.store != null && theStore.getStoreMetadata().getBranchName().equals(this.store.getStoreMetadata().getBranchName())) {
				marker.showInfoWindow();
				lastMarker = marker;
			}
		}
	}

	protected void popupateInfoWindow(Store store) {
		TextView storeType = (TextView) infoWindow.findViewById(R.id.atm_infowindow_store_type);
		storeType.setText(store.getStoreMetadata().getStoreType());

		TextView storeName = (TextView) infoWindow.findViewById(R.id.atm_infowindow_title);
		storeName.setText(store.getStoreMetadata().getBranchName());
	}

	@SuppressWarnings("unchecked")
	private void extractStoresFromIntent() {
		setStores((List<Store>) getIntent().getSerializableExtra(ATMSearchHelper.STORES_EXTRA_NAME));
	}

	protected void populateAndToggleStoreDetails() {
		if (store == null || searchInvoked || closeSearchView) {
			if (searchInvoked) {
				searchInvoked = false;
			} else if(closeSearchView) {
				closeSearchView = false;
			}
			findViewById(R.id.atm_details).setVisibility(View.GONE);
			map.getUiSettings().setMyLocationButtonEnabled(true);
			return;
		}
		infoWindow.findViewById(R.id.atm_infowindow_image).setVisibility(View.GONE);
		findViewById(R.id.atm_details).setVisibility(View.VISIBLE);
		map.getUiSettings().setMyLocationButtonEnabled(false);

		getApplicationState().getAnalyticsTracker().trackATMDetail(store);

		TextView storeName = (TextView) findViewById(R.id.atm_detail_store_name);
		storeName.setText(store.getStoreMetadata().getBranchName());

		TextView storeFacilities = (TextView) findViewById(R.id.atm_detail_store_facilities);
		storeFacilities.setText(toFacilitiesCSVList(store.getFacilitiesAvailable()));

		TescoTextView distanceToStore = (TescoTextView) findViewById(R.id.atm_detail_distance);
		String distanceInMiles = new DecimalFormat("#.##").format(store.getStoreMetadata().getDistanceInMiles());
		distanceToStore.setText(String.format(getResources().getString(R.string.atm_list_store_distance, distanceInMiles)));

		TescoTextView mondayOpeningTimes = (TescoTextView) findViewById(R.id.atm_detail_store_opening_time_monday);
		mondayOpeningTimes.setText(formatOpeningTimes(store.getOpeningTimes().getMonOpen(), store.getOpeningTimes().getMonClose()));

		TescoTextView tuesdayOpeningTimes = (TescoTextView) findViewById(R.id.atm_detail_store_opening_time_tuesday);
		tuesdayOpeningTimes.setText(formatOpeningTimes(store.getOpeningTimes().getTueOpen(), store.getOpeningTimes().getTueClose()));

		TescoTextView wednesdayOpeningTimes = (TescoTextView) findViewById(R.id.atm_detail_store_opening_time_wednesday);
		wednesdayOpeningTimes.setText(formatOpeningTimes(store.getOpeningTimes().getWedOpen(), store.getOpeningTimes().getWedClose()));

		TescoTextView thursdayOpeningTimes = (TescoTextView) findViewById(R.id.atm_detail_store_opening_time_thursday);
		thursdayOpeningTimes.setText(formatOpeningTimes(store.getOpeningTimes().getThuOpen(), store.getOpeningTimes().getThuClose()));

		TescoTextView fridayOpeningTimes = (TescoTextView) findViewById(R.id.atm_detail_store_opening_time_friday);
		fridayOpeningTimes.setText(formatOpeningTimes(store.getOpeningTimes().getFriOpen(), store.getOpeningTimes().getFriClose()));

		TescoTextView saturdayOpeningTimes = (TescoTextView) findViewById(R.id.atm_detail_store_opening_time_saturday);
		saturdayOpeningTimes.setText(formatOpeningTimes(store.getOpeningTimes().getSatOpen(), store.getOpeningTimes().getSatClose()));

		TescoTextView sundayOpeningTimes = (TescoTextView) findViewById(R.id.atm_detail_store_opening_time_sunday);
		sundayOpeningTimes.setText(formatOpeningTimes(store.getOpeningTimes().getSunOpen(), store.getOpeningTimes().getSunClose()));

		populateStoreAddress();
	}

	private String formatOpeningTimes(String open, String close) {
		
		if ("0001".equals(open) && ("2359".equals(close))) {
			return getString(R.string.atm_detail_24hours);
		}
		
		return open + HYPHEN + close;
	}

	private void showNavigationAndHideListIcon(boolean showNavigate) {
		if (null != directions) {
			directions.setVisible(showNavigate);
		}

		if (null != changeToList) {
			changeToList.setVisible(!showNavigate);
		}
	}

	private CharSequence toFacilitiesCSVList(List<String> facilitiesAvailable) {
		StringBuilder builder = new StringBuilder();

		for (String facility : facilitiesAvailable) {
			String actualFacility = ATMSearchHelper.FACILITIES.get(facility);
			if (null == actualFacility) {
				actualFacility = facility;
			}

			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append(actualFacility);
		}

		return builder.toString();
	}

	private void populateStoreAddress() {

		populateStoreAddressLine1();
		populateStoreTown();
		populateStoreCounty();
		populateStorePostCode();
		populateStoreCountry();
		populateStorePhone();
	}

	private void populateStoreAddressLine1() {
		String storeAddressLine1 = store.getStoreMetadata().getAddress1();
		TescoTextView storeAddressLine1View = (TescoTextView) findViewById(R.id.atm_detail_store_address1);
		if (storeAddressLine1 != null && storeAddressLine1.length() > 0) {
			storeAddressLine1View.setText(store.getStoreMetadata().getAddress1());
			storeAddressLine1View.setVisibility(View.VISIBLE);
		} else {
			storeAddressLine1View.setVisibility(View.GONE);
		}
	}

	private void populateStoreTown() {
		String storeTownCity = store.getStoreMetadata().getTownCity();
		TescoTextView storeTownCityView = (TescoTextView) findViewById(R.id.atm_detail_store_town_city);
		if (storeTownCity != null && storeTownCity.length() > 0) {
			storeTownCityView.setText(store.getStoreMetadata().getTownCity());
			storeTownCityView.setVisibility(View.VISIBLE);
		} else {
			storeTownCityView.setVisibility(View.GONE);
		}
	}

	private void populateStoreCounty() {
		String storeCounty = store.getStoreMetadata().getCounty();
		TescoTextView storeCountyView = (TescoTextView) findViewById(R.id.atm_detail_store_county);
		if (storeCounty != null && storeCounty.length() > 0) {
			storeCountyView.setText(store.getStoreMetadata().getCounty());
			storeCountyView.setVisibility(View.VISIBLE);
		} else {
			storeCountyView.setVisibility(View.GONE);
		}
	}

	private void populateStorePostCode() {
		String storePostCode = store.getStoreMetadata().getPostcode();
		TescoTextView storePostCodeView = (TescoTextView) findViewById(R.id.atm_detail_store_postcode);
		if (storePostCode != null && storePostCode.length() > 0) {
			storePostCodeView.setText(store.getStoreMetadata().getPostcode());
			storePostCodeView.setVisibility(View.VISIBLE);
		} else {
			storePostCodeView.setVisibility(View.GONE);
		}
	}

	private void populateStoreCountry() {
		String storeCountry = store.getStoreMetadata().getCountry();
		TescoTextView storeCountryView = (TescoTextView) findViewById(R.id.atm_detail_store_country);
		if (storeCountry != null && storeCountry.length() > 0) {
			storeCountryView.setText(store.getStoreMetadata().getCountry());
			storeCountryView.setVisibility(View.VISIBLE);
		} else {
			storeCountryView.setVisibility(View.GONE);
		}
	}

	private void populateStorePhone() {
		String storeTeleNumber = store.getStoreMetadata().getTelephone();
		TescoTextView storeTeleNumberView = (TescoTextView) findViewById(R.id.atm_detail_store_telephone);
		if (storeTeleNumber != null && storeTeleNumber.length() > 0) {
			storeTeleNumberView.setText(store.getStoreMetadata().getTelephone());
			storeTeleNumberView.setVisibility(View.VISIBLE);
		} else {
			storeTeleNumberView.setVisibility(View.GONE);
		}
	}

	private void commonActivtyResulProcessing(Intent data) {
		extractStoresFormIntent(data);
		setReferencePoint((LatLng) data.getParcelableExtra(ATMSearchHelper.REFERENCE_POINT));
		populateSearchView((String) data.getSerializableExtra(ATMSearchHelper.SEARCH_TEXT));
		storesReceivedFromATMLIstActivity = true;
		zoomToStoreLocation();
	}

	private void populateSearchView(String searchtext) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "starting populateSearchView with query extra " + searchtext);
		}
		if (searchtext != null) {
			searchViewHelper.showQuery(searchtext);
		}
	}

	@SuppressWarnings("unchecked")
	private void extractStoresFormIntent(Intent data) {
		setStores((List<Store>) data.getSerializableExtra(ATMSearchHelper.STORES_EXTRA_NAME));
	}
	
	
	protected Store getStore() {
		return store;
	}
	
	protected void setStore(Store store) {
		this.store = store;
	}

	protected void setStores(List<Store> stores) {
		this.stores = stores;
	}

	protected DirectionsHelper getDirectionsHelper() {
		return directionsHelper;
	}
	
	protected Map<String, Marker> getMarkerStoreLookup() {
		return markerStoreLookup;
	}
	
	protected LatLng getReferencePoint() {
		return referencePoint;
	}
	
	protected void setReferencePoint(LatLng referencePoint) {
		this.referencePoint = referencePoint;
	}

	protected void setSearchViewQueryText(String searchViewQueryText) {
		this.searchViewQueryText =  searchViewQueryText;
	}

	
	protected void setDirectionsHelper(DirectionsHelper directionsHelper) {
		this.directionsHelper = directionsHelper;
	}
	
	public void setSearchViewIconified(boolean searchViewIconified) {
		this.searchViewIconified = searchViewIconified;
	}

	private class ATMBroadcastReceiver extends BroadcastReceiver {

		private static final int DISTANCE_THRESHOLD = 50;

		@Override
		public void onReceive(Context context, final Intent intent) {

			ATMRetrievalStatus status = getStatus(intent);
			if (ATMRetrievalStatus.SUCCESS.equals(status)) {
				handleSuccess(intent);
			} else {
				handleFailure(intent);
			}
		}

		private ATMRetrievalStatus getStatus(Intent intent) {
			ATMRetrievalStatus status = (ATMRetrievalStatus) intent.getSerializableExtra(ATMProvider.ATM_RETRIEVAL_STATUS_EXTRA);
			return (status == null) ? ATMRetrievalStatus.FAIL : status;
		}

		private void handleSuccess(Intent intent) {
			setStores(CarouselActivity.getStoresFromIntent(intent));

			if (recalculateDistances) {
				recalculateDistances = false;
				calculateDistanceToStoresAndSort(stores, referencePoint.latitude, referencePoint.longitude);
			}

			if (stores.isEmpty() || stores.get(0).getStoreMetadata().getDistanceInMiles() > DISTANCE_THRESHOLD) {
				handleNoResultsCloseBy();
			} else {
				handleSuccessCloseBy();
			}
		}

		private void handleSuccessCloseBy() {
			populatePinLocations();
			if (query != null) {
				zoomToUserLocation();
				zoomToUserNearestStoreBounds();
				hideKeyboard();
				searchViewHelper.clearFocus();
			}
		}

		private void handleNoResultsCloseBy() {
			if (query != null) {
				handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.LocationSearchNoResults));
			}			
		}
		
		private void handleFailure(Intent intent) {
			ErrorResponseWrapper errorResponseWrapper = (ErrorResponseWrapper) intent.getSerializableExtra(ErrorResponseWrapper.class.getName());
			if (errorResponseWrapper != null) {
				handleErrorResponse(errorResponseWrapper);
			}
		}
	}
}
