package com.tescobank.mobile.ui.atm;

import static com.tescobank.mobile.ui.atm.ATMMapActivity.BUNDLE_KEY_DIRECTION_HELPER_SHOWING;
import static com.tescobank.mobile.ui.atm.ATMMapActivity.BUNDLE_KEY_SEARCH_VIEW_ICONIFIED;
import static com.tescobank.mobile.ui.atm.ATMMapActivity.BUNDLE_KEY_SEARCH_VIEW_SEARCH_TEXT;
import static com.tescobank.mobile.ui.atm.ATMMapActivity.BUNDLE_KEY_SELECTED_STORE;
import static com.tescobank.mobile.ui.atm.ATMMapActivity.BUNDLE_KEY_STORES;
import static com.tescobank.mobile.ui.atm.ATMMapActivity.BUNDLE_KEY_STORE_DETAILS_VISIBILITY;

import java.util.List;

import android.os.Bundle;
import android.view.View;

import com.tescobank.mobile.api.storefinder.Store;


public class ATMMapActivityStateRestorer  {
	private ATMMapActivity activity;
	private Bundle savedInstanceState;

	public ATMMapActivityStateRestorer(ATMMapActivity activity, Bundle savedInstanceState) {
		this.activity = activity;
		this.savedInstanceState = savedInstanceState;		
	}

	@SuppressWarnings("unchecked")
	public void restoreSavedData() {
		if(null == activity || null == savedInstanceState) {
			return;
		}

		restoreLocationAndStoreData((Store) savedInstanceState.getSerializable(BUNDLE_KEY_SELECTED_STORE), (List<Store>) savedInstanceState.getSerializable(BUNDLE_KEY_STORES), savedInstanceState.getInt(BUNDLE_KEY_STORE_DETAILS_VISIBILITY));	
		restoreDirectionsDialogCheck(savedInstanceState);
		restoreSearchViewIconStatus(savedInstanceState);
		resetSearchViewQueryText(savedInstanceState);
	}
	
	private void restoreLocationAndStoreData(Store selectedStoreToRestore, List<Store>storesToRestore, int storeDetailsVisible) {
		activity.setStore(selectedStoreToRestore);
		activity.setStores(storesToRestore);
		activity.populatePinLocations();
		
		if(null != activity.getStore()) {
			activity.zoomToStoreLocation();
			activity.getMarkerStoreLookup().get(activity.getStore().getStoreMetadata().getBranchID()).showInfoWindow();
			activity.popupateInfoWindow(activity.getStore());
			if(storeDetailsVisible == View.VISIBLE) {
				activity.populateAndToggleStoreDetails();
			}
		}
	}
	
	public void restoreDirectionsDialogCheck(Bundle savedInstanceState) {
		if(savedInstanceState.getBoolean(BUNDLE_KEY_DIRECTION_HELPER_SHOWING)) {
			activity.getDirectionsHelper().showDirections(activity.getReferencePoint().latitude, activity.getReferencePoint().longitude, activity.getStore().getStoreMetadata().getLatitude(), activity.getStore().getStoreMetadata().getLongitude());
		}
	}
	
	private void restoreSearchViewIconStatus(Bundle savedInstanceState) {
		activity.setSearchViewIconified(savedInstanceState.getBoolean(BUNDLE_KEY_SEARCH_VIEW_ICONIFIED));
	}
	
	private void resetSearchViewQueryText(Bundle savedInstanceState) {
		activity.setSearchViewQueryText(savedInstanceState.getString(BUNDLE_KEY_SEARCH_VIEW_SEARCH_TEXT));
	}
	
}