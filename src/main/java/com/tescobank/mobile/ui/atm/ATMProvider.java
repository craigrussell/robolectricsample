package com.tescobank.mobile.ui.atm;

import com.google.android.gms.maps.model.LatLng;

public interface ATMProvider {
	
	String ATM_RETRIEVAL_BROADCAST_EVENT = ATMProvider.class.getName();
	String ATM_RETRIEVAL_STATUS_EXTRA = "ATM_RETRIEVAL_STATUS_EXTRA";

	enum ATMRetrievalStatus {
		FAIL,
		SUCCESS
	}
	
	void retrieveCachableATMsForLatLng(LatLng latLng);
	void retrieveATMsForLatLng(LatLng latLng);
}