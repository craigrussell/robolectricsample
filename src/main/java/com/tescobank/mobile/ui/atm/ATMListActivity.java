package com.tescobank.mobile.ui.atm;

import java.util.LinkedList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.google.android.gms.maps.model.LatLng;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.api.storefinder.Store;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.ActivityResultCode;
import com.tescobank.mobile.ui.SearchViewHelperFactory;
import com.tescobank.mobile.ui.SearchViewHelperFactory.SearchViewHelper;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoTextView;
import com.tescobank.mobile.ui.atm.ATMProvider.ATMRetrievalStatus;
import com.tescobank.mobile.ui.atm.ATMSearchHelper.Callback;
import com.tescobank.mobile.ui.carousel.CarouselActivity;

public class ATMListActivity extends TescoActivity {

	private static final String TAG = ATMListActivity.class.getSimpleName();
	private static final String BUNDLE_KEY_SEARCH_VIEW_ICONIFIED = "searchViewIconified";
	private static final String BUNDLE_KEY_SEARCH_VIEW_SEARCH_TEXT = "searchViewSearchText";

	private ListView list;
	private List<Store> stores;
	private LatLng referencePoint;
	private String query;
	private BroadcastReceiver storesBroadcastReceiver;
	private SearchViewHelper searchViewHelper;
	private boolean searchViewIconified;
	private String searchViewQueryText;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		setContentView(R.layout.activity_atm_list);
		extractStoresFormIntent();
		list = (ListView) findViewById(R.id.atm_list);
		TescoTextView numberOfStores = (TescoTextView) findViewById(R.id.atm_list_result);
		numberOfStores.setText(String.format(getResources().getString(R.string.atm_list_header_result_number), stores.size()));
		populateListView();
		
		if(null != savedInstanceState) {
			restoreSavedData(savedInstanceState);
		} else {
			setSearchViewIconified(true);
		}
	}

	private void restoreSavedData(Bundle savedInstanceState) {
		restoreSearchViewIconStatus(savedInstanceState);
		restoreSearchViewQueryText(savedInstanceState);
	}
	
	private void restoreSearchViewIconStatus(Bundle savedInstanceState) {
		setSearchViewIconified(savedInstanceState.getBoolean(BUNDLE_KEY_SEARCH_VIEW_ICONIFIED, false));
	}
	
	private void restoreSearchViewQueryText(Bundle savedInstanceState) {
		setSearchViewQueryText(savedInstanceState.getString(BUNDLE_KEY_SEARCH_VIEW_SEARCH_TEXT));
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(null != searchViewHelper) {
			outState.putBoolean(BUNDLE_KEY_SEARCH_VIEW_ICONIFIED, searchViewHelper.isIconified());
			outState.putString(BUNDLE_KEY_SEARCH_VIEW_SEARCH_TEXT, searchViewHelper.getQuery());
		}
	}

	@SuppressWarnings("unchecked")
	private void extractStoresFormIntent() {
		stores = (List<Store>) getIntent().getSerializableExtra(ATMSearchHelper.STORES_EXTRA_NAME);
	}

	@Override
	protected void onResume() {
		super.onResume();
		getApplicationState().getAnalyticsTracker().trackATMFinderList();
	}

	@Override
	public void onBackPressed() {
		if (searchViewHelper.handleBackPressed()) {
			return;
		}
		super.onBackPressed();
	}

	@Override
	protected void onPause() {
		deregisterStoreReceiver();
		super.onPause();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onOptionsItemSelected: " + item);
		}

		if (R.id.menu_item_change_to_map == item.getItemId()) {
			startATMDetailActivityForStores();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.atm_list, menu);
		searchViewHelper = SearchViewHelperFactory.create((SearchView) menu.findItem(R.id.menu_item_search).getActionView());
		searchViewHelper.setIconified(searchViewIconified);
		if(null != searchViewQueryText && searchViewQueryText.length() > 0) {
			searchViewHelper.setQuery(searchViewQueryText);
		}
		searchViewHelper.setSubmitQueryCallback(new ATMSearchHelper(this, getApplicationState(), new ATMCallback()));
		return true;
	}

	private void populateListView() {
		ATMListAdapter atmListAdapter = new ATMListAdapter(this, stores);
		list.setAdapter(atmListAdapter);
		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				startATMDetailActivityForSelectedStore(position);
			}

		});
	}

	private void registerStoreReceiver() {
		if (null != storesBroadcastReceiver) {
			return;
		}
		
		storesBroadcastReceiver = new ATMBroadcastReceiver();
		LocalBroadcastManager.getInstance(this).registerReceiver(storesBroadcastReceiver, new IntentFilter(ATMProvider.ATM_RETRIEVAL_BROADCAST_EVENT));
	}

	private void deregisterStoreReceiver() {
		if (storesBroadcastReceiver != null) {
			LocalBroadcastManager.getInstance(this).unregisterReceiver(storesBroadcastReceiver);
		}
	}

	private void startATMDetailActivityForSelectedStore(int position) {
		Intent intent = new Intent(this, ATMMapActivity.class);
		intent.putExtra(ATMSearchHelper.SELECTED_STORE_EXTRA_NAME, stores.get(position));
		referencePoint = new LatLng(stores.get(position).getStoreMetadata().getLatitude(), stores.get(position).getStoreMetadata().getLongitude());
		commonATMDetailIntentProcessing(intent, ActivityResultCode.RESULT_ATM_LIST_WITH_SELECTED_STORE);
	}

	private void startATMDetailActivityForStores() {
		Intent intent = new Intent(this, ATMMapActivity.class);
		commonATMDetailIntentProcessing(intent, ActivityResultCode.RESULT_ATM_LIST_WITH_LIST_OF_STORES);
	}

	private void commonATMDetailIntentProcessing(Intent intent, int responseCode) {
		intent.putExtra(ATMSearchHelper.STORES_EXTRA_NAME, new LinkedList<Store>(stores));
		intent.putExtra(ATMSearchHelper.REFERENCE_POINT, referencePoint);
		intent.putExtra(ATMSearchHelper.SEARCH_TEXT, query);
		
		finishWithResult(responseCode, intent);
	}
	
	private void setSearchViewQueryText(String searchViewQueryText) {
		this.searchViewQueryText =  searchViewQueryText;
	}
	
	private void setSearchViewIconified(boolean searchViewIconified) {
		this.searchViewIconified = searchViewIconified;
	}
	
	private class ATMCallback implements Callback {

		@Override
		public void locationReceived(String query, LatLng location) {
			ATMListActivity.this.query = query;
			referencePoint = location;
			registerStoreReceiver();
			hideKeyboard();
			searchViewHelper.clearFocus();

			// update the header text
			TextView text = (TextView) findViewById(R.id.atm_list_header);
			text.setText(getString(R.string.atm_list_header_text_stores_close_to_search, query));
		}
		
		@Override
		public void handleError(ErrorResponseWrapper errorResponseWrapper) {
			handleErrorResponse(errorResponseWrapper);
		}

		@Override
		public void noResults() {
			handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.LocationSearchNoResults));
		}
	}
	
	private class ATMBroadcastReceiver extends BroadcastReceiver {
		
		private static final int DISTANCE_THRESHOLD = 50;

		@Override
		public void onReceive(Context context, final Intent intent) {

			ATMRetrievalStatus status = getStatus(intent);
			if (ATMRetrievalStatus.SUCCESS.equals(status)) {
				handleSuccess(intent);
			} else {
				handleFailure(intent);
			}
		}
		
		private ATMRetrievalStatus getStatus(Intent intent) {
			ATMRetrievalStatus status = (ATMRetrievalStatus) intent.getSerializableExtra(ATMProvider.ATM_RETRIEVAL_STATUS_EXTRA);
			return (status == null) ? ATMRetrievalStatus.FAIL : status;
		}
		
		private void handleSuccess(Intent intent) {
			stores = CarouselActivity.getStoresFromIntent(intent);

			if (stores.isEmpty() || stores.get(0).getStoreMetadata().getDistanceInMiles() > DISTANCE_THRESHOLD) {
				handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.LocationSearchNoResults));
			} else {
				populateListView();
			}
		}
		
		private void handleFailure(Intent intent) {
			ErrorResponseWrapper errorResponseWrapper = (ErrorResponseWrapper)intent.getSerializableExtra(ErrorResponseWrapper.class.getName());
			if (errorResponseWrapper != null) {
				handleErrorResponse(errorResponseWrapper);
			}
		}
	}
}
