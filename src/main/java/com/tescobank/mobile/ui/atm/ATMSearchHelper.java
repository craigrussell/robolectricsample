package com.tescobank.mobile.ui.atm;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.network.NetworkStatusChecker;
import com.tescobank.mobile.ui.SearchViewHelperFactory.SubmitQueryCallback;

public class ATMSearchHelper implements SubmitQueryCallback {

	private static final String TAG = ATMSearchHelper.class.getSimpleName();

	public static final String STORES_EXTRA_NAME = "stores";
	public static final String SELECTED_STORE_EXTRA_NAME = "store";
	public static final String REFERENCE_POINT = "referencePoint";
	public static final String SEARCH_TEXT = "searchText";
	public static final String FIT_BOUNDS_FOR_USER_EXTRA_NAME = "fitBoundsForUser";

	public static final float PAN_REFRESH_DISTANCE_THRESHOLD_METERS = 2 * 1000f;

	public static final Map<String, String> FACILITIES = new HashMap<String, String>();

	static {
		FACILITIES.put("Hour24", "24 Hour");
		FACILITIES.put("DirectCC", "Click & Collect");
		FACILITIES.put("DirectOC", "Direct Ordering");
		FACILITIES.put("WIFI", "In-Store Wifi");
		FACILITIES.put("TravelDesk", "Travel Money");
		FACILITIES.put("TechSupport", "Tech Support");
		FACILITIES.put("RecycleCenter", "Recycle Center");
		FACILITIES.put("Photo1Hr", "1 Hour Photo");
		FACILITIES.put("BabyChange", "Baby Change");
		FACILITIES.put("GiftCard", "Gift Card Store");
		FACILITIES.put("Telecoms", "Phone Shop");
	}

	private Activity activity;
	
	private ApplicationState applicationState;
	
	private Callback callback;
	
	private NetworkStatusChecker networkStatusChecker;
	
	private ErrorResponseWrapperBuilder errorBuilder;

	public ATMSearchHelper(Activity activity, ApplicationState applicationState, Callback callback) {
		this.activity = activity;
		this.applicationState = applicationState;
		this.callback = callback;
		this.networkStatusChecker = new NetworkStatusChecker();
		this.errorBuilder = new ErrorResponseWrapperBuilder(activity);
	}

	@Override
	public void textSubmitted(final String query) {
		
		Executors.newSingleThreadExecutor().execute(new Runnable() {
			@Override
			public void run() {
				final LatLng location = getLatLongFromAddress(query);
				activity.runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						if (null != location) {
							callback.locationReceived(query, location);
							applicationState.getATMProvider().retrieveATMsForLatLng(location);
						} else if (!networkStatusChecker.isAvailable(activity)) {
							callback.handleError(errorBuilder.buildErrorResponse(InAppError.NoInternetConnection));
						} else {
							callback.noResults();
						}			
					}
				});
			}
		});
	}

	@Override
	public void textChanged(String text) {
		// no-op
	}
	
	private LatLng getLatLongFromAddress(String youraddress) {
		
		if (!networkStatusChecker.isAvailable(activity)) {
			return null;
		}
		
		StringBuilder stringBuilder = new StringBuilder();
		performSearch(youraddress, stringBuilder);
		
		return parseResponse(stringBuilder);
	}
	
	private void performSearch(String youraddress, StringBuilder stringBuilder) {
		
		String encodedAddress = getEncodedAddress(youraddress);
		String uri = "http://maps.google.com/maps/api/geocode/json?address=" + encodedAddress + "&sensor=false";
		HttpGet httpGet = new HttpGet(uri);
		HttpClient client = new DefaultHttpClient();
		
		try {
			HttpResponse response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		} catch (Exception e) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Exception thrown when receiving address search!", e);
			}
		}
	}
	
	private String getEncodedAddress(String youraddress) {
		String encodedAddress = youraddress;
		try {
			encodedAddress = URLEncoder.encode(youraddress, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "UnsupportedEncodingException thrown when receiving address search!", e1);
			}
		}
		return encodedAddress;
	}
	
	private LatLng parseResponse(StringBuilder stringBuilder) {
		try {
			JSONObject jsonObject = new JSONObject(stringBuilder.toString());
			Double lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getDouble("lng");
			Double lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getDouble("lat");
			return new LatLng(lat, lng);
		} catch (JSONException e) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Exception thrown when receiving address search!", e);
			}
			return null;
		}
	}

	/**
	 * 
	 * @author toj
	 * This runs on the UI Thread!
	 */
	public interface Callback {
		
		void locationReceived(String query, LatLng location);
		void handleError(ErrorResponseWrapper errorResponseWrapper);
		void noResults();
	}
}
