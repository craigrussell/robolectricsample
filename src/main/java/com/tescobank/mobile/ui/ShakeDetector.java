package com.tescobank.mobile.ui;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

// inspired by http://jasonmcreynolds.com/?p=388

public class ShakeDetector implements SensorEventListener {

	private static final float SHAKE_THRESHOLD_GRAVITY = 5f;
	private static final int SHAKE_SLOP_TIME_MS = 1500;

	private final SensorManager sensorMgr;

	private long shakeTimestamp;
	private final Listener listener;
	private TescoActivity tescoActivity;

	public ShakeDetector(TescoActivity tescoActivity, Listener listener) {
		this.tescoActivity = tescoActivity;
		this.listener = listener;

		sensorMgr = (SensorManager) tescoActivity.getSystemService(Context.SENSOR_SERVICE);
		if (sensorMgr != null) {
			Sensor accelerometerSensor = sensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			if (null != accelerometerSensor) {
				sensorMgr.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_GAME);
			}
		}
	}

	public void dispose() {
		if (null != sensorMgr) {
			sensorMgr.unregisterListener(this);
		}
		tescoActivity = null;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// no-op
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		onSensorChanged(System.currentTimeMillis(), event.values);
	}

	void onSensorChanged(long now, float[] values) {
		// ignore shake events too close to each other (500ms)
		if (shakeTimestamp + SHAKE_SLOP_TIME_MS > now) {
			return;
		}

		float x = values[0];
		float y = values[1];
		float z = values[2];

		float gX = x / SensorManager.GRAVITY_EARTH;
		float gY = y / SensorManager.GRAVITY_EARTH;
		float gZ = z / SensorManager.GRAVITY_EARTH;

		// gForce will be close to 1 when there is no movement.
		float gForce = (gX * gX) + (gY * gY) + (gZ * gZ);

		if (gForce > SHAKE_THRESHOLD_GRAVITY) {

			shakeTimestamp = now;
			
			tescoActivity.onUserActivity();
			
			listener.onShake();
		}

	}

	public static interface Listener {

		void onShake();

	}

}
