package com.tescobank.mobile.ui.auth.password;

/**
 * A password submitter
 */
public interface PasswordSubmitter {
	
	/**
	 * Submits the current password
	 */
	void submitPassword();
}
