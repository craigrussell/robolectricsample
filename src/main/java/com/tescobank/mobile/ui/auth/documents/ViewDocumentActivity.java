package com.tescobank.mobile.ui.auth.documents;

import static android.os.Build.VERSION_CODES.JELLY_BEAN_MR2;

import java.io.InputStream;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.ShareActionProvider;
import com.actionbarsherlock.widget.ShareActionProvider.OnShareTargetSelectedListener;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.analytics.AnalyticsEventFactory.DocumentStatus;
import com.tescobank.mobile.api.content.RetrieveContentResponse;
import com.tescobank.mobile.api.content.RetrieveContentResponse.Document;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.download.WebViewDownloadHelper;
import com.tescobank.mobile.network.NetworkStatusChecker;
import com.tescobank.mobile.properties.TescoApplicationPropertiesReader.ApplicationPropertyKeys;
import com.tescobank.mobile.ui.TescoActivity;

public class ViewDocumentActivity extends TescoActivity implements OnShareTargetSelectedListener {

	private static final String TAG = ViewDocumentActivity.class.getSimpleName();
	private static final String SHARE_FILE = "share_history_view_document.xml";
	private static final String DOCUMENT_INDEX = "DOCUMENT_INDEX";

	private ProgressBar progressBar;
	private WebView webView;
	private Button continueButton;
	private NetworkStatusChecker networkStatusChecker;
	private Document document;
	private String documentUrl;
	private Uri pdfUrl;
	private List<Document> documents;
	private int index;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onCreate(" + savedInstanceState + ")");
		}

		setContentView(R.layout.activity_view_document);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		webView = (WebView) findViewById(R.id.documentWebView);
		webView.getSettings().setAllowFileAccess(false);
		continueButton = (Button) findViewById(R.id.documentAcceptButton);
		documents = getAllDocuments();
		document = getDocument();
		documentUrl = getApplicationState().getApplicationPropertyValueForKey(ApplicationPropertyKeys.BASE_DOCUMENT_URL) + "/" + document.getHtmlURL();
		String pdfUrlBase = getApplicationState().getApplicationPropertyValueForKey(ApplicationPropertyKeys.PDF_API_BASE);
		pdfUrl = Uri.parse(pdfUrlBase + document.getPdfURL());

		progressBar.setVisibility(View.VISIBLE);
		webView.setVisibility(View.GONE);
		continueButton.setEnabled(false);

		networkStatusChecker = new NetworkStatusChecker();

		setupWebView();

		int textID = document.isMandatory() ? R.string.document_mandatory_button : R.string.document_optional_button;
		continueButton.setText(textID);
	}

	private Document getDocument() {
		return documents.get(getFlow().getDataBundle().getInt(DOCUMENT_INDEX));
	}

	@Override
	protected void onStart() {
		trackDocumentAnalytic(DocumentStatus.Viewed);
		super.onStart();
	}

	private void trackDocumentAnalytic(DocumentStatus status) {
		getApplicationState().getAnalyticsTracker().trackDocument(document, getApplicationState().isRegistered(), getApplicationState().isPassword(), status);
	}

	private void trackUrlClickedAnalytic(String url) {
		getApplicationState().getAnalyticsTracker().trackClickedUrlInDocument(url);
	}

	@Override
	protected void onRetryPressed() {
		loadPageInWebView();
	}

	public void onAcceptPressed(View view) {
		if (document.isMandatory()) {
			trackDocumentAnalytic(DocumentStatus.Accepted);
		}
		onDone();
	}
	
	private void onDone() {
		getFlow().getDataBundle().putInt(DOCUMENT_INDEX, ++index);		
		getFlow().onPolicyComplete(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onCreateOptionsMenu");
		}
	
		getSupportMenuInflater().inflate(R.menu.share, menu);
		MenuItem shareItem = menu.findItem(R.id.menu_item_share);
		if (null == pdfUrl) {
			shareItem.setVisible(false);
			shareItem.setEnabled(false);
		} else {
			ShareActionProvider actionProvider = (ShareActionProvider) shareItem.getActionProvider();
			actionProvider.setShareHistoryFileName(SHARE_FILE);
			actionProvider.setShareIntent(createShareIntent());
			actionProvider.setOnShareTargetSelectedListener(this);
		}
		return true;
	}

	private Intent createShareIntent() {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating share intent for PDF at " + document);
		}

		Intent shareIntent = new Intent(Intent.ACTION_VIEW);
		shareIntent.setType("application/pdf");
		shareIntent.setData(pdfUrl);
		return shareIntent;
	}
	
	@Override
	public boolean onShareTargetSelected(ShareActionProvider source, Intent intent) {
		trackDocumentAnalytic(DocumentStatus.Exported);
		startActivity(intent);
		return true;
	}

	@SuppressWarnings("unchecked")
	private List<Document> getAllDocuments() {
		return(List<Document>) getFlow().getDataBundle().getSerializable(RetrieveContentResponse.class.getName());
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void setupWebView() {
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setBackgroundColor(Color.TRANSPARENT);
		webView.setWebViewClient(new DocumentWebViewClient());
		loadPageInWebView();
	}
	
	private void loadPageInWebView() {
		if (!networkStatusChecker.isAvailable(this)) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Not attempting to show WebView - there is no internet connection");
			}
			ErrorResponseWrapper error = getErrorBuilder().buildErrorResponse(InAppError.NoInternetConnection);
			handleErrorResponse(error);
			return;
		}
		webView.loadUrl(documentUrl);
	}

	private void onWebError(int statusCode) {
		handleErrorResponse(getErrorBuilder().buildErrorResponse(statusCode));
	}

	private class DocumentWebViewClient extends WebViewClient {

		@Override
		public void onPageFinished(WebView view, String url) {
			progressBar.setVisibility(View.GONE);
			webView.setVisibility(View.VISIBLE);
			continueButton.setEnabled(true);
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, errorCode + ": " + description);
			}
			onWebError(errorCode);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			trackUrlClickedAnalytic(url);
			return super.shouldOverrideUrlLoading(view, url);
		}
		
		@Override
		@SuppressLint("NewApi")
		public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
			if (Build.VERSION.SDK_INT == JELLY_BEAN_MR2) {
				return new WebResourceResponse("","", downloadWebContent(url));
			}
			return super.shouldInterceptRequest(view, url);
		}
		
		private InputStream downloadWebContent(String url) {
			WebViewDownloadHelper resourceDownloader = new WebViewDownloadHelper(ViewDocumentActivity.this);
			return resourceDownloader.performDownload(url);
		}
	}
}
