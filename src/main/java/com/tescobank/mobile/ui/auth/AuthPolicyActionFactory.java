package com.tescobank.mobile.ui.auth;

import static com.tescobank.mobile.api.audit.AuditRequest.OperationName.SET_CREDENTIAL;
import static com.tescobank.mobile.api.audit.AuditRequest.Reason.CHANGE;
import static com.tescobank.mobile.api.audit.AuditRequest.Reason.FORGOTTEN;
import static com.tescobank.mobile.api.audit.AuditRequest.Reason.SWITCHED_TO_PASSCODE;
import static com.tescobank.mobile.api.audit.AuditRequest.Reason.SWITCHED_TO_PASSWORD;
import static com.tescobank.mobile.ui.Transitions.applyLoginTransition;

import com.tescobank.mobile.R;
import com.tescobank.mobile.analytics.AnalyticsEvent;
import com.tescobank.mobile.analytics.AnalyticsEventFactory;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.flow.PolicyAction;
import com.tescobank.mobile.flow.PolicyErrorAction;
import com.tescobank.mobile.flow.TescoConfirmAndExitAction;
import com.tescobank.mobile.flow.TescoConfirmAndExitResetApplicationAction;
import com.tescobank.mobile.flow.TescoConfirmAndRestartAction;
import com.tescobank.mobile.flow.TescoConfirmation;
import com.tescobank.mobile.flow.TescoPolicyAction;
import com.tescobank.mobile.flow.TescoPolicyErrorAction;
import com.tescobank.mobile.ui.CredentialBroadcastAction;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.Transitions;

public class AuthPolicyActionFactory {

	public static PolicyAction createDefaultCancellationAction() {
		return new TescoPolicyAction() {
			private static final long serialVersionUID = -6346803504000170508L;

			@Override
			public void execute(TescoActivity activity) {
				activity.finish();
			}
		};
	}
	
	public static PolicyAction createConfirmAndExitAction() {
		TescoConfirmation confirmation = new TescoConfirmation(R.string.confirm_application_exit_title, R.string.confirm_application_exit_message);
		return new TescoConfirmAndExitAction(confirmation);
	}
	
	public static PolicyAction createConfirmAndExitResetApplicationAction() {
		TescoConfirmation confirmation = new TescoConfirmation(R.string.confirm_application_exit_title, R.string.confirm_application_exit_message);
		return new TescoConfirmAndExitResetApplicationAction(confirmation);
	}
	
	public static PolicyAction createConfirmAndRestartLoginAction() {
		TescoConfirmation confirmation = new TescoConfirmation(R.string.confirm_login_exit_title, R.string.confirm_login_exit_message);
		return new TescoConfirmAndRestartAction(confirmation);
	}
	
	public static PolicyErrorAction createResetApplicationOnErrorAction() { 
		return new TescoPolicyErrorAction() {

			private static final long serialVersionUID = -2839683764432711481L;
			@Override
			public void executeError(TescoActivity activity, ErrorResponseWrapper errorResponseWrapper) {
				if(errorResponseWrapper.getErrorResponse().isErrorTypeFatal()) {
					activity.getApplicationState().getRestRequestProcessor().cancelAllRequests();
					activity.getApplicationState().resetApplication();
				}
				onErrorActionCompleted();
			}		
		}; 

	}

	public static PolicyAction createChangePasscodeCompletePolicyAction() {
		return new TescoPolicyAction() {
			private static final long serialVersionUID = -1711962608329145528L;

			@Override
			public void execute(TescoActivity activity) {
				activity.audit(SET_CREDENTIAL, CHANGE);
				activity.getApplicationState().getAnalyticsTracker().trackUserChangePasscodeCreatePasscodeCompletedSuccessfully();
				new CredentialBroadcastAction(activity.getString(R.string.update_passcode_success_message), activity).sendMessage();
				onActionCompleted();
			}		
		};
	}
		
	public static PolicyAction createSwitchPasscodeCompletePolicyAction() {
		return new TescoPolicyAction() {
			private static final long serialVersionUID = -6525760864567765184L;

			@Override
			public void execute(TescoActivity activity) {
				activity.audit(SET_CREDENTIAL, SWITCHED_TO_PASSCODE);
				activity.getApplicationState().setIsPassword(false);
				activity.getApplicationState().getAnalyticsTracker().trackUserSwitchToPasscodeCreatePasscodeCompletedSuccessfully();
				new CredentialBroadcastAction(activity.getString(R.string.switch_to_passcode_success_message), activity).sendMessage();
				onActionCompleted();
			}
		};
	}

	public static PolicyAction createChangedPasswordCompletePolicyAction() {
		return new TescoPolicyAction() {

			private static final long serialVersionUID = 6011907827694936155L;

			@Override
			public void execute(TescoActivity activity) {
				activity.getApplicationState().getAnalyticsTracker().trackUserChangePasswordCreatePasswordCompletedSuccessfully();
				new CredentialBroadcastAction(activity.getString(R.string.change_password_success_message), activity).sendMessage();
				onActionCompleted();
			}
		};
	}
	
	public static PolicyAction createSwitchPasswordCompletePolicyAction() {
		return new TescoPolicyAction() {

			private static final long serialVersionUID = 6011907827694936155L;

			@Override
			public void execute(TescoActivity activity) {
				activity.audit(SET_CREDENTIAL, SWITCHED_TO_PASSWORD);
				activity.getApplicationState().getAnalyticsTracker().trackUserSwitchToPasswordCreatePasswordCompletedSuccessfully();
				new CredentialBroadcastAction(activity.getString(R.string.switch_to_password_success_message), activity).sendMessage();
				onActionCompleted();
			}
		};
	}
	
	public static PolicyAction createForgottenPasswordCompletePolicyAction() {
		AnalyticsEventFactory analyticsEventFactory = new AnalyticsEventFactory();
		return new AuthenticationCompleteAction(false, SET_CREDENTIAL, FORGOTTEN, analyticsEventFactory.eventForUserForgottenPasswordCreatePasswordCompletedSuccessfully(), R.string.reset_password_success_message);
	}
	
	public static PolicyAction createForgottenPasscodeCompletePolicyAction() {
		AnalyticsEventFactory analyticsEventFactory = new AnalyticsEventFactory();
		return new AuthenticationCompleteAction(false, SET_CREDENTIAL, FORGOTTEN, analyticsEventFactory.eventForUserForgottenPasscodeCreatePasscodeCompletedSuccessfully(),  R.string.auth_create_passcode_success_message);
	}
	
	public static PolicyAction createCreatePasscodeCompletePolicyAction(AnalyticsEvent analyticEvent) {
		return new CreatePasscodeCompletionAction(analyticEvent); 
	}
	
	public static PolicyAction createLoginAnimationPolicyAction() { 
		return new TescoPolicyAction() {
			private static final long serialVersionUID = -6676340672582238451L;

			@Override
			public void execute(TescoActivity activity) {
				applyLoginTransition(activity);
				onActionCompleted();
			}
		};
	}
	
	public static PolicyAction createDefaultAnimationPolicyAction() { 
		return new TescoPolicyAction() {
			private static final long serialVersionUID = -8421534031356840370L;

			@Override
			public void execute(TescoActivity activity) {
				Transitions.applyDefaultTransition(activity);
				onActionCompleted();
			}
		};
	}
}
