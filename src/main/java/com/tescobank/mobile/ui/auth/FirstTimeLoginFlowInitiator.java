package com.tescobank.mobile.ui.auth;

import static com.tescobank.mobile.api.content.RetrieveContentRequest.Journey.TERMS_FIRST_TIME_LOGIN;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.flow.Flow;
import com.tescobank.mobile.rest.processor.BatchRestRequestCompletionListener;
import com.tescobank.mobile.rest.processor.BatchRestRequestProcessor;

/**
 * Splash activity first time login API flow
 */
public class FirstTimeLoginFlowInitiator extends LoginFlowInitiator implements BatchRestRequestCompletionListener {

	private static final String TAG = FirstTimeLoginFlowInitiator.class.getSimpleName();

	private BatchRestRequestProcessor requestOrganiser;
	private ApplicationState applicationState;

	public FirstTimeLoginFlowInitiator(SplashActivity splashActivity, Flow activityFlow) {
		super(splashActivity, activityFlow, TERMS_FIRST_TIME_LOGIN);
		this.applicationState = splashActivity.getApplicationState();
		this.requestOrganiser = new BatchRestRequestProcessor(applicationState.getRestRequestProcessor(), this);
		resetApplicationIfPartiallyRegistered();
	}

	private void resetApplicationIfPartiallyRegistered() {
		if (applicationState.getDeviceId() != null) {
			applicationState.resetApplication();
		}
	}

	@Override
	protected void startApiRequests() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "InAuth: ALL SPLASH SERVICES STARTING!!!");
		}
		requestOrganiser.addRequest(new SplashCheckDeviceRequest(), false);
		requestOrganiser.addRequest(new SplashFeaturesRequest(), true);
		requestOrganiser.addRequest(new SplashSettingsRequest(), true);
		requestOrganiser.addRequest(new SplashRetrieveContentRequest(), true);
		requestOrganiser.execute();
	}
}
