package com.tescobank.mobile.ui.auth;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.flow.Flow;
import com.tescobank.mobile.flow.PolicyAction;
import com.tescobank.mobile.flow.PolicyActionListener;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.TescoActivity;

/**
 * Activity for RegistrationCompletion
 */
public class RegistrationCompleteActivity extends TescoActivity implements PolicyActionListener{

	public static final String BUNDLE_KEY_REGISTRATION_COMPLETE_TITLE = "BUNDLE_KEY_REGISTRATION_COMPLETE_TITLE";

	private String password;
	private InProgressIndicator inProgressIndicator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayout());
		ActionBarConfiguration.setToPreLogin(this, getSupportActionBar());
		password = getFlow().getDataBundle().getString(PASSWORD_EXTRA);
		inProgressIndicator = new InProgressIndicator(this, true);
		configureTitle();
		configureContinueButton();
	}

	private void configureTitle() {
		TextView title = TextView.class.cast(findViewById(R.id.login_registration_complete_title));
		title.setText(getFlow().getDataBundle().getInt(BUNDLE_KEY_REGISTRATION_COMPLETE_TITLE));
	}
	
	private boolean isFirstTimeRegistration() {
		return getFlow().getDataBundle().getInt(BUNDLE_KEY_REGISTRATION_COMPLETE_TITLE) == R.string.auth_registration_complete_title ? true : false;
	}

	private void configureContinueButton() {
		Button button = (Button) findViewById(R.id.continueButton);
		button.setText(R.string.auth_registration_skip_passcode_button);
	}

	protected int getLayout() {
		return R.layout.activity_auth_registration_complete;
	}

	@Override
	protected void onStart() {
		trackAnalytic();
		super.onStart();
	}

	protected void trackAnalytic() {
		if(isFirstTimeRegistration()) {
			getApplicationState().getAnalyticsTracker().trackEvent(getFlow().getDisplayAnalytic());
		}	
	}

	@Override
	protected void onRetryPressed() {
		ProductDataLoaderAction carouselLoaderAction = new ProductDataLoaderAction(false, true);
		carouselLoaderAction.setListener(this);
		carouselLoaderAction.execute(this);
	}

	public void onCreatePasscodeButtonPressed(View view) {
		Flow flow = AuthFlowFactory.createPasscodeFlow(isFirstTimeRegistration());
		flow.getDataBundle().putString(PASSWORD_EXTRA, password);
		flow.begin(this);
	}

	public void onContinuePressed(View view) {
		
		if(isFirstTimeRegistration()) {
			getApplicationState().getAnalyticsTracker().trackUserFirstTimeLoginCompletedSuccessfully();
		} 
		
		inProgressIndicator.showInProgressIndicator();
		ProductDataLoaderAction carouselLoaderAction = new ProductDataLoaderAction(false, false);
		carouselLoaderAction.setListener(this);
		carouselLoaderAction.execute(this);
	}
	
	@Override
	public void onActionComplete(PolicyAction action) {
		getFlow().onPolicyComplete(this);
	}
}
