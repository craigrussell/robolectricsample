package com.tescobank.mobile.ui.auth;

import java.util.concurrent.Executors;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.inauth.mme.InAuthManager;
import com.tescobank.mobile.R;
import com.tescobank.mobile.inauth.MobileInAuthLogger;
import com.tescobank.mobile.persist.Persister;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoAlertDialog;

public class GettingStartedActivity extends TescoActivity {

	private static final String BUNDLE_LOCATION_DIALOG = "locationDialog";

	private PiggyPartAnimator leftPigTailAnimator;
	private AlertDialog locationDialog;
	private boolean shouldShowLocationDialog;

	@Override
	protected void onUserLeaveHint() {
		// don't allow the welcome to close if it is backgrounded
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auth_registration_getting_started);
		ActionBarConfiguration.setToPreLogin(this, getSupportActionBar());

		ImageView leftPigTail = ImageView.class.cast(findViewById(R.id.welcome_left_pigtail));

		leftPigTailAnimator = new PiggyPartAnimator(leftPigTail, R.drawable.pigtail_wiggle, false);

		fadeIn(R.id.getting_started_req1_container, 1000);
		fadeIn(R.id.getting_started_req2_container, 2000);
		fadeIn(R.id.getting_started_req3_container, 3000);

		if (null != savedInstanceState) {
			shouldShowLocationDialog = savedInstanceState.getBoolean(BUNDLE_LOCATION_DIALOG);
		}
	}

	@Override
	protected void onRetryPressed() {
	}

	private void fadeIn(int id, int delayMillis) {
		final View view = findViewById(id);
		view.setVisibility(View.GONE);
		view.postDelayed(new Runnable() {
			@Override
			public void run() {
				view.setVisibility(View.VISIBLE);
			}
		}, delayMillis);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus) {
			leftPigTailAnimator.animate();

			if (shouldShowLocationDialog) {
				requestToShareLocation();
				shouldShowLocationDialog = false;
			}

		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		destroyLocationDialog();
		leftPigTailAnimator.destroy();
	}

	public void onGetStartedPressed(View view) {
		requestToShareLocation();
	}

	private void requestToShareLocation() {
		locationDialog = new TescoAlertDialog.Builder(this).setTitle(R.string.share_location_confirm_title).setMessage(R.string.share_location_confirm_message).setPositiveButton(R.string.share_location_confirm_yes, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				destroyLocationDialog();
				updateUserLocationPreferencesAndContinue(true);
			}
		}).setNeutralButton(R.string.share_location_confirm_no, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				destroyLocationDialog();
				updateUserLocationPreferencesAndContinue(false);
			}
		}).create();
		locationDialog.show();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (null != locationDialog) {
			outState.putBoolean(BUNDLE_LOCATION_DIALOG, true);
			destroyLocationDialog();
		}
	}

	private void updateUserLocationPreferencesAndContinue(boolean useLocation) {
		getApplicationState().setUseLocationPreference(useLocation ? Persister.UseLocationPreference.YES : Persister.UseLocationPreference.NO);
		if(InAuthManager.getInstance().isInitialized()) {
			sendInAuthGPSLog();	
		}
		getFlow().onPolicyComplete(this);
	}

	private void sendInAuthGPSLog() {
		Executors.newSingleThreadExecutor().execute(new Runnable() {
			@Override
			public void run() {
				MobileInAuthLogger.sendGPSLog(getApplicationState());
			}
		});
	}

	private void destroyLocationDialog() {
		if(locationDialog != null) {
			locationDialog.dismiss();
			locationDialog = null;
		}
	}
}