package com.tescobank.mobile.ui.auth;

import static com.tescobank.mobile.api.audit.AuditRequest.OperationName.SUBSEQUENT_LOGIN_PASSCODE;
import static com.tescobank.mobile.api.audit.AuditRequest.OperationName.SUBSEQUENT_LOGIN_PASSWORD;
import static com.tescobank.mobile.flow.Flow.StackBehaviour.CLEAR_ALL;
import static com.tescobank.mobile.flow.Flow.StackBehaviour.KEEP_ALIVE;
import static com.tescobank.mobile.ui.ActionBarConfiguration.Style.UNAUTHENTICATED;
import static com.tescobank.mobile.ui.ActionBarConfiguration.Type.HOME;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createChangePasscodeCompletePolicyAction;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createChangedPasswordCompletePolicyAction;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createConfirmAndExitAction;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createConfirmAndExitResetApplicationAction;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createConfirmAndRestartLoginAction;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createCreatePasscodeCompletePolicyAction;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createForgottenPasscodeCompletePolicyAction;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createForgottenPasswordCompletePolicyAction;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createLoginAnimationPolicyAction;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createResetApplicationOnErrorAction;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createSwitchPasscodeCompletePolicyAction;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createSwitchPasswordCompletePolicyAction;
import static com.tescobank.mobile.ui.auth.CreatePasscodeActivity.BUNDLE_KEY_ENTER_PROMPT;
import static com.tescobank.mobile.ui.auth.CreatePasscodeActivity.BUNDLE_KEY_MODAL_HEADER;
import static com.tescobank.mobile.ui.auth.CreatePasscodeActivity.BUNDLE_KEY_MODAL_MESSAGE;
import static com.tescobank.mobile.ui.auth.CreatePasscodeActivity.BUNDLE_KEY_REENTER_PROMPT;
import static com.tescobank.mobile.ui.auth.EnterPasscodeActivity.BUNDLE_KEY_EXISTING_PROMPT;
import static com.tescobank.mobile.ui.auth.EnterPasswordActivity.BUNDLE_KEY_ENTER_PASSWORD_LAYOUT;
import static com.tescobank.mobile.ui.auth.EnterPasswordActivity.BUNDLE_KEY_GET_OTP_CREATE_ARCOT;
import static com.tescobank.mobile.ui.auth.PinDigitsActivity.BUNDLE_KEY_PIN_CREATE_ARCOT;
import static com.tescobank.mobile.ui.auth.PinDigitsActivity.BUNDLE_KEY_PIN_LAYOUT;
import static com.tescobank.mobile.ui.auth.RegistrationCompleteActivity.BUNDLE_KEY_REGISTRATION_COMPLETE_TITLE;
import static com.tescobank.mobile.ui.auth.SMSActivity.BUNDLE_KEY_SMS_LAYOUT;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;

import com.tescobank.mobile.R;
import com.tescobank.mobile.analytics.AnalyticsEventFactory;
import com.tescobank.mobile.flow.Flow;
import com.tescobank.mobile.flow.Flow.StackBehaviour;
import com.tescobank.mobile.flow.Policy;
import com.tescobank.mobile.flow.TescoFlow;
import com.tescobank.mobile.flow.TescoPolicy;
import com.tescobank.mobile.flow.TescoPolicyInventory;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.ActionBarConfiguration.Style;
import com.tescobank.mobile.ui.ActionBarConfiguration.Type;
import com.tescobank.mobile.ui.auth.documents.ViewDocumentsActivity;
import com.tescobank.mobile.ui.carousel.CarouselActivity;

public class AuthFlowFactory {

	public static Flow createBlankFlow() {
		List<Policy> policies = new ArrayList<Policy>();
		return new TescoFlow(new Bundle(), new TescoPolicyInventory(policies));
	}

	public static Flow createLoginWithPasscodeFlow() {
		ActionBarConfiguration config = new ActionBarConfiguration(Style.UNAUTHENTICATED, Type.HOME_HAMBURGER);
		AnalyticsEventFactory analyticsEventFactory = new AnalyticsEventFactory();

		List<Policy> policies = new ArrayList<Policy>();
		policies.add(new TescoPolicy(SplashActivity.class));
		
		policies.add(new TescoPolicy.Builder(ViewDocumentsActivity.class)
			.setActionBarConfig(config)
			.setAnimation(createLoginAnimationPolicyAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(LoginWithPasscodeActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForLoginWithPasscode())
			.setActionBarConfig(config)
			.setAnimation(createLoginAnimationPolicyAction())
			.setCompletion(new AuthenticationCompleteAction(true, SUBSEQUENT_LOGIN_PASSCODE, null))
			.setCancellation(createConfirmAndExitAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(CarouselActivity.class)
			.setStackBehaviour(StackBehaviour.KEEP_ALIVE)
			.setCancellation(createConfirmAndExitAction())
			.create());
		
		return new TescoFlow(new Bundle(), new TescoPolicyInventory(policies));
	}

	public static Flow createLoginWithPasswordFlow() {
		
		AnalyticsEventFactory analyticsEventFactory = new AnalyticsEventFactory();

		List<Policy> policies = new ArrayList<Policy>();
		policies.add(new TescoPolicy(SplashActivity.class));
		
		policies.add(new TescoPolicy.Builder(ViewDocumentsActivity.class)
			.setAnimation(createLoginAnimationPolicyAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(LoginWithPasswordActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForLoginWithPassword())
			.setAnimation(createLoginAnimationPolicyAction())
			.setCompletion(new AuthenticationCompleteAction(true, SUBSEQUENT_LOGIN_PASSWORD, null))
			.setCancellation(createConfirmAndRestartLoginAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(CarouselActivity.class)
			.setCancellation(createConfirmAndExitAction())
			.create());
		
		return new TescoFlow(new Bundle(), new TescoPolicyInventory(policies));
	}

	public static Flow createChangePasscodeFlow() {
		Bundle bundle = new Bundle();
		bundle.putInt(BUNDLE_KEY_EXISTING_PROMPT, R.string.change_passcode_existing_prompt);
		bundle.putInt(BUNDLE_KEY_ENTER_PROMPT, R.string.change_passcode_enter_prompt);
		bundle.putInt(BUNDLE_KEY_REENTER_PROMPT, R.string.change_passcode_reenter_prompt);
		bundle.putInt(BUNDLE_KEY_MODAL_HEADER, R.string.auth_passcode_creation_modal_header);
		bundle.putInt(BUNDLE_KEY_MODAL_MESSAGE, R.string.auth_passcode_creation_modal_message);
		bundle.putBoolean(BUNDLE_KEY_GET_OTP_CREATE_ARCOT, true);

		ActionBarConfiguration config = new ActionBarConfiguration(Style.AUTHENTICATED, Type.UP);
		AnalyticsEventFactory analyticsEventFactory = new AnalyticsEventFactory();

		List<Policy> policies = new ArrayList<Policy>();
		policies.add(new TescoPolicy.Builder(EnterPasscodeActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory
			.eventForUserChangePasscodeExistingPasscode())
			.setActionBarConfig(config).create());
		
		policies.add(new TescoPolicy.Builder(CreatePasscodeActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory
			.eventForUserChangePasscodeCreatePasscode())
			.setAnimation(createLoginAnimationPolicyAction())
			.setActionBarConfig(config)
			.setCompletion(createChangePasscodeCompletePolicyAction())
			.create());
		
		TescoFlow flow = new TescoFlow(bundle, new TescoPolicyInventory(policies));
		flow.setTitleId(R.string.change_passcode_title);
		flow.setInitialStackBehaviour(KEEP_ALIVE);
		return flow;
	}

	public static Flow createSwitchToPasscodeFlow() {
		Bundle bundle = new Bundle();
		bundle.putInt(BUNDLE_KEY_ENTER_PASSWORD_LAYOUT, R.layout.activity_auth_password_enter);
		bundle.putInt(BUNDLE_KEY_ENTER_PROMPT, R.string.switch_to_passcode_enter_prompt);
		bundle.putInt(BUNDLE_KEY_REENTER_PROMPT, R.string.switch_to_passcode_reenter_prompt);
		bundle.putInt(BUNDLE_KEY_MODAL_HEADER, R.string.switch_to_passcode_modal_header);
		bundle.putInt(BUNDLE_KEY_MODAL_MESSAGE, R.string.switch_to_passcode_modal_message);
		bundle.putBoolean(BUNDLE_KEY_GET_OTP_CREATE_ARCOT, true);

		ActionBarConfiguration config = new ActionBarConfiguration(Style.AUTHENTICATED, Type.UP);
		AnalyticsEventFactory analyticsEventFactory = new AnalyticsEventFactory();

		List<Policy> policies = new ArrayList<Policy>();
		policies.add(new TescoPolicy.Builder(EnterPasswordActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForUserSwitchToPasscodePassword())
			.setActionBarConfig(config)
			.create());
		
		policies.add(new TescoPolicy.Builder(CreatePasscodeActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForUserSwitchToPasscodeCreatePasscode())
			.setAnimation(createLoginAnimationPolicyAction())
			.setActionBarConfig(config)
			.setCompletion(createSwitchPasscodeCompletePolicyAction())
			.create());

		TescoFlow flow = new TescoFlow(bundle, new TescoPolicyInventory(policies));
		flow.setTitleId(R.string.switch_to_passcode_title);
		flow.setInitialStackBehaviour(KEEP_ALIVE);
		return flow;
	}

	public static Flow createForgottenPasscodeFlow() {
		Bundle bundle = new Bundle();
		bundle.putInt(BUNDLE_KEY_ENTER_PASSWORD_LAYOUT, R.layout.activity_auth_password_enter);
		bundle.putInt(BUNDLE_KEY_ENTER_PROMPT, R.string.reset_passcode_prompt);
		bundle.putInt(BUNDLE_KEY_REENTER_PROMPT, R.string.reset_passcode_reenter_prompt);
		bundle.putInt(BUNDLE_KEY_MODAL_HEADER, R.string.auth_passcode_creation_modal_header);
		bundle.putInt(BUNDLE_KEY_MODAL_MESSAGE, R.string.auth_passcode_creation_modal_message);
		bundle.putBoolean(BUNDLE_KEY_GET_OTP_CREATE_ARCOT, true);
		
		ActionBarConfiguration config = new ActionBarConfiguration(Style.UNAUTHENTICATED, Type.HOME);
		AnalyticsEventFactory analyticsEventFactory = new AnalyticsEventFactory();

		List<Policy> policies = new ArrayList<Policy>();
		policies.add(new TescoPolicy.Builder(EnterPasswordActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForUserForgottenPasscodePassword())
			.setAnimation(createLoginAnimationPolicyAction()).setActionBarConfig(config)
			.setCancellation(createConfirmAndRestartLoginAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(CreatePasscodeActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForUserForgottenPasscode())
			.setAnimation(createLoginAnimationPolicyAction())
			.setActionBarConfig(config)
			.setCompletion(createForgottenPasscodeCompletePolicyAction())
			.setCancellation(createConfirmAndRestartLoginAction()).create());
		
		policies.add(new TescoPolicy.Builder(CarouselActivity.class).setCancellation(createConfirmAndExitAction()).create());
		
		TescoFlow flow = new TescoFlow(bundle, new TescoPolicyInventory(policies));
		flow.setTitleId(R.string.reset_passcode_title);
		return flow;
	}

	public static Flow createForgottenPasswordFlow() {
		Bundle bundle = new Bundle();
		bundle.putInt(BUNDLE_KEY_SMS_LAYOUT, R.layout.activity_auth_sms);
		bundle.putInt(BUNDLE_KEY_PIN_LAYOUT, R.layout.activity_auth_pindigits);

		ActionBarConfiguration config = new ActionBarConfiguration(Style.UNAUTHENTICATED, Type.HOME);
		AnalyticsEventFactory analyticsEventFactory = new AnalyticsEventFactory();

		List<Policy> policies = new ArrayList<Policy>();
		policies.add(new TescoPolicy.Builder(PinDigitsActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForUserForgottenPasswordPin())
			.setActionBarConfig(config)
			.setAnimation(createLoginAnimationPolicyAction())
			.setCompletion(new RetrieveSMSAction())
			.setCancellation(createConfirmAndRestartLoginAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(SMSActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForUserForgottenPasswordSMS())
			.setAnimation(createLoginAnimationPolicyAction()).setActionBarConfig(config)
			.setCancellation(createConfirmAndRestartLoginAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(CreatePasswordActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForUserForgottenPassword())
			.setAnimation(createLoginAnimationPolicyAction()).setActionBarConfig(config)
			.setCompletion(createForgottenPasswordCompletePolicyAction())
			.setCancellation(createConfirmAndRestartLoginAction())
			.setActionBarConfig(config)
			.create());
		
		policies.add(new TescoPolicy.Builder(CarouselActivity.class).setCancellation(createConfirmAndExitAction()).create());

		TescoFlow flow = new TescoFlow(bundle, new TescoPolicyInventory(policies));
		flow.setTitleId(R.string.reset_password_title);
		return flow;
	}

	public static Flow createChangePasswordFlow() {
		Bundle bundle = new Bundle();
		bundle.putInt(BUNDLE_KEY_PIN_LAYOUT, R.layout.activity_auth_pindigits);

		ActionBarConfiguration config = new ActionBarConfiguration(Style.AUTHENTICATED, Type.UP);
		AnalyticsEventFactory analyticsEventFactory = new AnalyticsEventFactory();

		List<Policy> policies = new ArrayList<Policy>();
		policies.add(new TescoPolicy.Builder(PinDigitsActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForUserChangePasswordPin())
			.setActionBarConfig(config)
			.create());
		
		policies.add(new TescoPolicy.Builder(CreatePasswordActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory
			.eventForUserChangePasswordCreatePassword())
			.setAnimation(createLoginAnimationPolicyAction())
			.setActionBarConfig(config)
			.setCompletion(createChangedPasswordCompletePolicyAction()).create());

		TescoFlow flow = new TescoFlow(bundle, new TescoPolicyInventory(policies));
		flow.setTitleId(R.string.change_password_title);
		flow.setInitialStackBehaviour(KEEP_ALIVE);
		return flow;
	}

	public static Flow createSwitchToPasswordFlow() {
		Bundle bundle = new Bundle();
		bundle.putInt(BUNDLE_KEY_PIN_LAYOUT, R.layout.activity_auth_pindigits);
		bundle.putInt(BUNDLE_KEY_ENTER_PASSWORD_LAYOUT, R.layout.activity_auth_password_enter);
		bundle.putBoolean(BUNDLE_KEY_PIN_CREATE_ARCOT, true);

		ActionBarConfiguration config = new ActionBarConfiguration(Style.AUTHENTICATED, Type.UP);
		AnalyticsEventFactory analyticsEventFactory = new AnalyticsEventFactory();

		List<Policy> policies = new ArrayList<Policy>();
		
		policies.add(new TescoPolicy.Builder(PinDigitsActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForUserSwitchToPasswordPin())
			.setActionBarConfig(config)
			.create());
		
		policies.add(new TescoPolicy.Builder(EnterPasswordActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForUserSwitchToPasswordCreatePassword())
			.setAnimation(createLoginAnimationPolicyAction())
			.setActionBarConfig(config)
			.setCompletion(createSwitchPasswordCompletePolicyAction())
			.create());

		TescoFlow flow = new TescoFlow(bundle, new TescoPolicyInventory(policies));
		flow.setTitleId(R.string.switch_to_password_title);
		flow.setInitialStackBehaviour(KEEP_ALIVE);

		return flow;
	}

	public static Flow createRegistrationLoginFlow() {
		Bundle bundle = new Bundle();
		bundle.putInt(BUNDLE_KEY_ENTER_PASSWORD_LAYOUT, R.layout.activity_auth_registration_password);
		bundle.putInt(BUNDLE_KEY_PIN_LAYOUT, R.layout.activity_auth_registration_pindigits);
		bundle.putInt(BUNDLE_KEY_SMS_LAYOUT, R.layout.activity_auth_registration_sms);
		bundle.putBoolean(BUNDLE_KEY_PIN_CREATE_ARCOT, true);
		bundle.putInt(BUNDLE_KEY_REGISTRATION_COMPLETE_TITLE, R.string.auth_registration_complete_title);

		ActionBarConfiguration config = new ActionBarConfiguration(UNAUTHENTICATED, HOME);
		AnalyticsEventFactory analyticsEventFactory = new AnalyticsEventFactory();

		List<Policy> policies = new ArrayList<Policy>();
		policies.add(new TescoPolicy(SplashActivity.class));
		
		policies.add(new TescoPolicy.Builder(WelcomeActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForWelcome())
			.setStackBehaviour(KEEP_ALIVE)
			.setCancellation(createConfirmAndExitAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(GettingStartedActivity.class)
			.setAnimation(createLoginAnimationPolicyAction())
			.setStackBehaviour(CLEAR_ALL).create());
		
		policies.add(new TescoPolicy.Builder(ViewDocumentsActivity.class)
			.setActionBarConfig(config)
			.setCancellation(createConfirmAndRestartLoginAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(UsernameActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForRegistrationEnterUsername())
			.setAnimation(createLoginAnimationPolicyAction())
			.setActionBarConfig(config)
			.setCancellation(createConfirmAndRestartLoginAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(PAMActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForRegistrationPAM())
			.setAnimation(createLoginAnimationPolicyAction())
			.setActionBarConfig(config)
			.setCancellation(createConfirmAndRestartLoginAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(PinDigitsActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory
			.eventForRegistrationEnterPin())
			.setAnimation(createLoginAnimationPolicyAction())
			.setCompletion(new RetrieveSMSAction())
			.setActionBarConfig(config)
			.setCancellation(createConfirmAndRestartLoginAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(SMSActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForRegistrationSMS())
			.setAnimation(createLoginAnimationPolicyAction())
			.setActionBarConfig(config)
			.setCancellation(createConfirmAndRestartLoginAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(EnterPasswordActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForRegistrationEnterPassword())
			.setAnimation(createLoginAnimationPolicyAction())
			.setActionBarConfig(config)
			.setCompletion(new RegistrationCompletionAction(true))
			.setCancellation(createConfirmAndRestartLoginAction()).create());
		
		return new TescoFlow(bundle, new TescoPolicyInventory(policies));
	}

	public static Flow createPasswordChangedOnlineFlow() {
		
		Bundle bundle = new Bundle();
		bundle.putInt(BUNDLE_KEY_SMS_LAYOUT, R.layout.activity_auth_sms);
		bundle.putInt(BUNDLE_KEY_PIN_LAYOUT, R.layout.activity_auth_pindigits_passwordchanged);
		bundle.putInt(BUNDLE_KEY_ENTER_PASSWORD_LAYOUT, R.layout.activity_auth_password_enter);
		bundle.putBoolean(BUNDLE_KEY_PIN_CREATE_ARCOT, true);
		bundle.putInt(BUNDLE_KEY_REGISTRATION_COMPLETE_TITLE, R.string.passwordchanged_password_title);

		ActionBarConfiguration config = new ActionBarConfiguration(Style.UNAUTHENTICATED, Type.HOME);
		AnalyticsEventFactory analyticsEventFactory = new AnalyticsEventFactory();

		List<Policy> policies = new ArrayList<Policy>();
		policies.add(new TescoPolicy.Builder(PinDigitsActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory.eventForLoginPasswordChangedOnlinePinEntered())
			.setAnimation(createLoginAnimationPolicyAction())
			.setActionBarConfig(config)
			.setError(createResetApplicationOnErrorAction())
			.setCancellation(createConfirmAndExitResetApplicationAction())
			.setCompletion(new RetrieveSMSAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(SMSActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory
			.eventForLoginPasswordChangedOnlineSMSEntered())
			.setAnimation(createLoginAnimationPolicyAction())
			.setActionBarConfig(config)
			.setError(createResetApplicationOnErrorAction())
			.setCancellation(createConfirmAndExitResetApplicationAction())
			.create());
		
		policies.add(new TescoPolicy.Builder(EnterPasswordActivity.class)
			.setActivityDisplayedAnalytic(analyticsEventFactory
			.eventForLoginPasswordChangedOnlinePasswordEntered())
			.setAnimation(createLoginAnimationPolicyAction())
			.setActionBarConfig(config)
			.setError(createResetApplicationOnErrorAction())
			.setCompletion(new RegistrationCompletionAction(false))
			.setCancellation(createConfirmAndExitResetApplicationAction())
			.create());
		
		TescoFlow flow = new TescoFlow(bundle, new TescoPolicyInventory(policies));
		flow.setTitleId(R.string.passwordchanged_password_title);
		flow.setInitialStackBehaviour(CLEAR_ALL);
		return flow;
		
	}
	
	public static Flow createPasscodeFlow(boolean isRegistration) {
		Bundle bundle = new Bundle();
		bundle.putInt(BUNDLE_KEY_ENTER_PROMPT, R.string.auth_create_passcode_initial_message);
		bundle.putInt(BUNDLE_KEY_REENTER_PROMPT, R.string.auth_create_passcode_verify_message);
		bundle.putInt(BUNDLE_KEY_MODAL_HEADER, R.string.auth_passcode_creation_modal_header);
		bundle.putInt(BUNDLE_KEY_MODAL_MESSAGE, R.string.auth_passcode_creation_modal_message);

		ActionBarConfiguration config = new ActionBarConfiguration(Style.UNAUTHENTICATED, Type.HOME);
		AnalyticsEventFactory analyticsEventFactory = new AnalyticsEventFactory();

		List<Policy> policies = new ArrayList<Policy>();
		policies.add(new TescoPolicy.Builder(CreatePasscodeActivity.class)
			.setStackBehaviour(CLEAR_ALL)
			.setActivityDisplayedAnalytic(isRegistration ? analyticsEventFactory.eventForRegistrationSetupPasscode() : analyticsEventFactory.eventForLoginPasswordChangedOnlineCreatePasscode())
			.setAnimation(createLoginAnimationPolicyAction())
			.setActionBarConfig(config)
			.setCompletion(createCreatePasscodeCompletePolicyAction(isRegistration ? analyticsEventFactory.eventForFirstTimeLoginCompletedSuccessfully() : analyticsEventFactory.eventForUserPasswordChangedOnlineCreatePasscodeCompletedSuccessfully()))
			.create());
		
		policies.add(new TescoPolicy.Builder(CarouselActivity.class)
			.setCancellation(createConfirmAndExitAction())
			.create());
		
		TescoFlow flow = new TescoFlow(bundle, new TescoPolicyInventory(policies));
		flow.setInitialStackBehaviour(KEEP_ALIVE);
		flow.setTitleId(R.string.auth_create_passcode_title);
		return flow;
	}
}
