package com.tescobank.mobile.ui.auth;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.TescoActivity;

public class WelcomeActivity extends TescoActivity {

	private static final String TAG = WelcomeActivity.class.getSimpleName();

	private PiggyPartAnimator leftPigTailAnimator;
	private PiggyPartAnimator leftPigFaceAnimator;
	private PiggyPartAnimator rightPigFaceAnimator;
	private boolean showLandingPage; 


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "InAuth: -> Welcome Screen created!");
		}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auth_registration_welcome);
		ActionBarConfiguration.setToPreLogin(this, getSupportActionBar());

		ImageView leftPigTail = ImageView.class.cast(findViewById(R.id.welcome_left_pigtail));
		ImageView leftPigFace = ImageView.class.cast(findViewById(R.id.welcome_left_pigface));
		ImageView rightPigFace = ImageView.class.cast(findViewById(R.id.welcome_right_pigface));

		leftPigTailAnimator = new PiggyPartAnimator(leftPigTail, R.drawable.pigtail_wiggle, true);
		leftPigFaceAnimator = new PiggyPartAnimator(leftPigFace, R.drawable.pigface_wink, true);
		rightPigFaceAnimator = new PiggyPartAnimator(rightPigFace, R.drawable.pigface_wink, false);

		if (getResources().getBoolean(R.bool.show_hudl_landing_page) && null == savedInstanceState) {
			showLandingPage = true;
		}
	}	

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus) {
			leftPigTailAnimator.animate();
			leftPigFaceAnimator.animate();
			rightPigFaceAnimator.animate();
		}
	}

	@Override
	protected void onStart() {
		trackAnalytic();
		super.onStart();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if(showLandingPage) {
			showLandingPage();
		}
	}
	
	private void showLandingPage() {
		showLandingPage = false;
		Intent intent = new Intent(this, LandingPageActivity.class);
		intent.putExtra(LandingPageActivity.LANDING_PAGE_URL, getApplicationState().getSettings().getLandingPageURL());
		startActivity(intent);
	}

	private void trackAnalytic() {
		getApplicationState().getAnalyticsTracker().trackEvent(getFlow().getDisplayAnalytic());
	}

	public void onGetStartedPressed(View view) {
		getFlow().onPolicyComplete(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		leftPigFaceAnimator.destroy();
		leftPigTailAnimator.destroy();
		rightPigFaceAnimator.destroy();
	}
}
