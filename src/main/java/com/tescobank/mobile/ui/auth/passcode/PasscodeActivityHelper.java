package com.tescobank.mobile.ui.auth.passcode;

import java.util.List;

import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.InvalidValueIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoEditText;
import com.tescobank.mobile.ui.balancepeek.BalancePeekSlidingPanelTouchListener;

/**
 * Helper for passcode-related activities
 */
public class PasscodeActivityHelper implements TextWatcher {

	private static final String TAG = PasscodeActivityHelper.class.getSimpleName();

	/** The passcode submitter */
	private PasscodeSubmitter passcodeSubmitter;

	/** The invalid value indicator for the activity */
	private InvalidValueIndicator invalidValueIndicator;

	/** The passcode data helper */
	private PasscodeDataHelper passcodeDataHelper;

	private EditText hiddenEditText;

	/**
	 * Constructor
	 *
	 * @param activity
	 *            the activity
	 * @param passcodeSubmitter
	 *            the passcode submitter
	 * @param invalidValueIndicator
	 *            the invalid value indicator for the activity
	 */
	public PasscodeActivityHelper(final TescoActivity activity, PasscodeSubmitter passcodeSubmitter, InvalidValueIndicator invalidValueIndicator) {
		this(activity, passcodeSubmitter, invalidValueIndicator, null);
	}

	public PasscodeActivityHelper(final TescoActivity activity, PasscodeSubmitter passcodeSubmitter, InvalidValueIndicator invalidValueIndicator,
			final BalancePeekSlidingPanelTouchListener balancePeekTouchListener) {
		super();

		this.passcodeSubmitter = passcodeSubmitter;
		this.invalidValueIndicator = invalidValueIndicator;

		hiddenEditText = EditText.class.cast(activity.findViewById(R.id.hiddenInputField));
		hiddenEditText.addTextChangedListener(this);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

			OnTouchListener viewTouchListener = new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
						if (balancePeekEnabledAndShowing()) {
							closePanels(balancePeekTouchListener);
							return true;
						}
						hiddenEditText.requestFocus();
						activity.showKeyboard();
						return true;
					}
					return false;
				}

				private void closePanels(final BalancePeekSlidingPanelTouchListener balancePeekTouchListener) {
					balancePeekTouchListener.closePanels(BalancePeekSlidingPanelTouchListener.ANIMATION_DURATION_DEFAULT);
				}

				private boolean balancePeekEnabledAndShowing() {
					if (balancePeekTouchListener != null && balancePeekTouchListener.panelsAreOpen()) {
						return true;
					}
					return false;
				}
			};
			activity.findViewById(R.id.passcodeInputScreen).setOnTouchListener(viewTouchListener);
		}

		TescoEditText field1 = TescoEditText.class.cast(activity.findViewById(R.id.inputField1));
		TescoEditText field2 = TescoEditText.class.cast(activity.findViewById(R.id.inputField2));
		TescoEditText field3 = TescoEditText.class.cast(activity.findViewById(R.id.inputField3));
		TescoEditText field4 = TescoEditText.class.cast(activity.findViewById(R.id.inputField4));
		TescoEditText field5 = TescoEditText.class.cast(activity.findViewById(R.id.inputField5));

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			field1.setTransformationMethod(PasswordTransformationMethod.getInstance());
			field2.setTransformationMethod(PasswordTransformationMethod.getInstance());
			field3.setTransformationMethod(PasswordTransformationMethod.getInstance());
			field4.setTransformationMethod(PasswordTransformationMethod.getInstance());
			field5.setTransformationMethod(PasswordTransformationMethod.getInstance());
		}

		passcodeDataHelper = new PasscodeDataHelper(field1, field2, field3, field4, field5);
		clearFields();
	}

	/**
	 * @return the passcode data helper
	 */
	public PasscodeDataHelper getPasscodeDataHelper() {
		return passcodeDataHelper;
	}

	/**
	 * Handles retry
	 */
	public void handleRetry() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Retry");
		}

		clearFields();
	}

	public void requestFocus() {
		hiddenEditText.requestFocus();
	}

	private void clearFields() {
		hiddenEditText.setText(null);
		List<TescoEditText> inputFields = passcodeDataHelper.getInputFieldsForRequiredValues();
		for (EditText inputField : inputFields) {
			inputField.setText(null);
			inputField.setBackgroundResource(R.drawable.textfield_default);
		}

		EditText first = inputFields.get(0);
		first.setBackgroundResource(R.drawable.textfield_cursor_blink);
		AnimationDrawable.class.cast(first.getBackground()).start();
	}

	/**
	 * Handles retry for a passive error
	 */
	public void handleRetryForPassiveError() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Retry for Passive Error");
		}

		clearFields();

		List<TescoEditText> inputFields = passcodeDataHelper.getInputFieldsForRequiredValuesWithNoValue();
		if (inputFields.isEmpty()) {
			invalidValueIndicator.indicateMissingValue(passcodeDataHelper.getInputFieldsForRequiredValues());
		} else {
			invalidValueIndicator.indicateMissingValue(inputFields);
		}
	}

	@Deprecated
	public void setCursorVisible(boolean visible) {
	}

	@Override
	public void afterTextChanged(Editable s) {

		List<TescoEditText> inputFields = passcodeDataHelper.getInputFieldsForRequiredValues();
		int index = 0;
		for (EditText inputField : inputFields) {
			inputField.setText(s.length() > index ? "" + s.charAt(index) : null);
			if (s.length() == index) {
				inputField.setBackgroundResource(R.drawable.textfield_cursor_blink);
				AnimationDrawable.class.cast(inputField.getBackground()).start();
			} else {
				inputField.setBackgroundResource(R.drawable.textfield_default);
			}
			index++;
		}

		if (s.length() >= 5) {
			passcodeSubmitter.submitPasscode();
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	public void reset() {
		clearFields();
	}

}
