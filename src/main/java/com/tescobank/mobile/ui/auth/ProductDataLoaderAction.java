package com.tescobank.mobile.ui.auth;

import com.tescobank.mobile.flow.TescoPolicyAction;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.carousel.CarouselLoader;

public class ProductDataLoaderAction extends TescoPolicyAction implements ProductDataLoadedListener{

	private static final long serialVersionUID = -5107496876702789609L;
	public static final String OVERLAY_MESSAGE_KEY = "OVERLAY_MESSAGE_KEY";
	
	private transient TescoActivity activity;
	private transient CarouselLoader carouselLoader;

	private boolean isSubsequentLogin;
	private boolean isRetryLoad;
	private int successMessageId;
	
	public ProductDataLoaderAction(boolean isSubsequentLogin, boolean isRetryLoad) {
		this.isSubsequentLogin = isSubsequentLogin;
		this.isRetryLoad = isRetryLoad;
	}

	public ProductDataLoaderAction(boolean isSubsequentLogin, boolean isRetryLoad, int successMessageId) {
		this.isSubsequentLogin = isSubsequentLogin;
		this.isRetryLoad = isRetryLoad;
		this.successMessageId = successMessageId;
	}

	@Override
	public void execute(TescoActivity activity) {		
		this.activity = activity;
		carouselLoader = new CarouselLoader(activity, this);
		if(isRetryLoad) {
			carouselLoader.retryLoadCarousel();
			return;
		}
		carouselLoader.loadCarousel(isSubsequentLogin);
	}

	@Override
	public void onProductDataLoadComplete() {
		if(hasSuccessMessageId()) {
			activity.getFlow().getDataBundle().putString(OVERLAY_MESSAGE_KEY, activity.getString(successMessageId));
		}
		carouselLoader.trackLoginAnalytic();
		onActionCompleted();
	}

	private boolean hasSuccessMessageId() {	
		return successMessageId > 0;
	}

}
