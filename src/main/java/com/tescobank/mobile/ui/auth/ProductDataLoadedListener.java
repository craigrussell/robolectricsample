package com.tescobank.mobile.ui.auth;

public interface ProductDataLoadedListener {
	
	/**
	 * Responds to completion of all required product data
	 */
	void onProductDataLoadComplete();
}
