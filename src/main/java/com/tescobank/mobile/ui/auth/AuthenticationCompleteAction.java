package com.tescobank.mobile.ui.auth;

import java.util.List;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.analytics.AnalyticsEvent;
import com.tescobank.mobile.api.audit.AuditRequest.OperationName;
import com.tescobank.mobile.api.audit.AuditRequest.Reason;
import com.tescobank.mobile.api.content.RetrieveContentResponse;
import com.tescobank.mobile.api.content.RetrieveContentResponse.Document;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.flow.PolicyAction;
import com.tescobank.mobile.flow.PolicyActionListener;
import com.tescobank.mobile.flow.TescoPolicyAction;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.auth.documents.AcknowledgeDocumentRequest;
import com.tescobank.mobile.ui.auth.documents.DocumentAcknowledgementListener;
import com.tescobank.mobile.ui.auth.documents.DocumentsDataHelper;
import com.tescobank.mobile.ui.errors.ErrorHandler;

public class AuthenticationCompleteAction extends TescoPolicyAction implements ErrorHandler, PolicyActionListener, DocumentAcknowledgementListener {

	private static final long serialVersionUID = -6492774461107393261L;
	private static final String TAG = AuthenticationCompleteAction.class.getName();
	
	private transient TescoActivity activity;
	private DocumentsDataHelper documentsDataHelper;
	private final boolean isSubsequentLogin;
	private final OperationName operationName;
	private Reason reason;
	private int successMessageId;
	private final AnalyticsEvent analyticsEvent;
	
	public AuthenticationCompleteAction(boolean isSubsequentLogin, OperationName operationName, AnalyticsEvent analyticsEvent) {
		this.isSubsequentLogin = isSubsequentLogin;
		this.operationName = operationName;
		this.analyticsEvent = analyticsEvent;
	}

	public AuthenticationCompleteAction(boolean isSubsequentLogin, OperationName operationName, Reason reason, AnalyticsEvent analyticsEvent, int successMessageId) {
		this(isSubsequentLogin, operationName, analyticsEvent);
		this.reason = reason;
		this.successMessageId = successMessageId;
	}
	
	@Override
	public void execute(TescoActivity activity) {
		this.activity = activity;
		trackAnalytic();
		documentsDataHelper = new DocumentsDataHelper(getDocuments());
		acknowledgeDocuments();
	}
	
	private void trackAnalytic() {
		if(analyticsEvent == null) {
			return;
		}
		activity.getApplicationState().getAnalyticsTracker().trackEvent(analyticsEvent);
	}

	private void acknowledgeDocuments() {
		Document document = documentsDataHelper.getNextDocumentToAcknowledge(null);
		if (document != null) {
			startAcknowledgeDocumentRequest(document);
		} else {
			createCarouselLoaderAction();
		}
	}
	
	private void startAcknowledgeDocumentRequest(Document document) {
		ApplicationState application = activity.getApplicationState(); 
		AcknowledgeDocumentRequest request = new AcknowledgeDocumentRequest(activity, document, documentsDataHelper, this, this, application.getRestRequestProcessor(), application);
		application.getRestRequestProcessor().processRequest(request, activity.getClass());
	}
	
	@Override
	public void onDocumentAcknowledgementComplete() {
		if(BuildConfig.DEBUG) {
			Log.d(TAG, "onDocumentAcknowledgementComplete callback received");
		}
		activity.audit(operationName,reason);
		createCarouselLoaderAction();
	}

	private void createCarouselLoaderAction() {
		ProductDataLoaderAction carouselLoaderAction = new ProductDataLoaderAction(isSubsequentLogin, false, successMessageId);
		carouselLoaderAction.setListener(this);
		carouselLoaderAction.execute(activity);
	}
		
	@SuppressWarnings({"unchecked"})
	private List<Document> getDocuments() {
		return (List<Document>) activity.getFlow().getDataBundle().get(RetrieveContentResponse.class.getName());
	}

	@Override
	public void handleError(ErrorResponseWrapper errorResponseWrapper) {
		activity.handleErrorResponse(errorResponseWrapper);	
	}

	@Override
	public void onActionComplete(PolicyAction action) {
		onActionCompleted();
	}
}
