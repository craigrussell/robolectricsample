package com.tescobank.mobile.ui.auth;

import static com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError.CriticalMalwareDetected;

import java.io.Serializable;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.os.Process;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.balancepeek.CheckDeviceNonInteractiveEvalRiskRequest;
import com.tescobank.mobile.api.balancepeek.GetPortfolioSummaryRequest;
import com.tescobank.mobile.api.balancepeek.GetPortfolioSummaryResponse;
import com.tescobank.mobile.api.configuration.FeaturesRequest;
import com.tescobank.mobile.api.configuration.FeaturesResponse;
import com.tescobank.mobile.api.configuration.SettingsRequest;
import com.tescobank.mobile.api.configuration.SettingsResponse;
import com.tescobank.mobile.api.content.RetrieveContentRequest;
import com.tescobank.mobile.api.content.RetrieveContentRequest.Journey;
import com.tescobank.mobile.api.content.RetrieveContentResponse;
import com.tescobank.mobile.api.deviceregistration.CheckDeviceRequest;
import com.tescobank.mobile.api.deviceregistration.CheckDeviceResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder;
import com.tescobank.mobile.api.success.SuccessResponseWrapper;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.application.TescoDeviceMetrics;
import com.tescobank.mobile.filter.KnownProductFilter;
import com.tescobank.mobile.flow.Flow;
import com.tescobank.mobile.flow.FlowInitiator;
import com.tescobank.mobile.inauth.InAuthController;
import com.tescobank.mobile.inauth.MalwareDetector;
import com.tescobank.mobile.inauth.RootedDetector;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.sort.TescoPortfolioSummaryComparator;

/**
 * Abstract class for splash activity API flows
 */
public abstract class LoginFlowInitiator implements FlowInitiator {

	private static final String TAG = LoginFlowInitiator.class.getSimpleName();
	
	private SplashActivity splashActivity;
	private ApplicationState applicationState;
	private Flow activityFlow;
	private Journey loginJourney;

	public LoginFlowInitiator(SplashActivity splashActivity, Flow activityFlow, Journey loginJourney) {
		this.splashActivity = splashActivity;
		this.applicationState = splashActivity.getApplicationState();
		this.activityFlow = activityFlow;
		this.loginJourney = loginJourney;
	}

	@Override
	public void execute() {
		if (applicationState.getInAuthController().isOnlineInitialised() || applicationState.getInAuthController().isOfflineInitialised()) {
			performSecurityChecksAndMoveOn();
		} else {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "InAuth: Invoking offlineInit in SPLASH as inAuth not Initialised!");
			}
			inAuthOfflineInitialisation();
		}
	}

	public void performSecurityChecksAndMoveOn() {
		detectRooted();
	}

	private void detectRooted() {
		applicationState.getInAuthController().getRootedDetector().detectRooted(new RootedDetector.RootedDetectionListener() {
			@Override
			public void rootedCompleted() {
				detectMalware();
			}
		}, false);
	}

	public void updateConfig() {
		applicationState.getInAuthController().getRootedDetector().updateRootSigFile();
		applicationState.getInAuthController().getMalwareDetector().updateMalwareConfig();
	}

	private void inAuthOfflineInitialisation() {
		applicationState.getInAuthController().initialiseOffline(new InAuthController.InAuthOfflineInitialisationListener() {

			@Override
			public void inAuthOfflineInitialised() {
				performSecurityChecksAndMoveOn();
			}

			@Override
			public void inAuthOfflineFailed() {
				startApiRequests();
			}
		});
	}

	private void detectMalware() {
		applicationState.getInAuthController().getMalwareDetector().detectMalware(new MalwareDetector.MalwareDetectionListener() {

			@Override
			public void malwareDetected() {
				handleCriticalMalwareDetected();
			}

			@Override
			public void malwareNotDetected() {
				startApiRequests();
			}
		}, false);
	}

	public void inAuthOnlineInitialisation() {

		if (applicationState.getInAuthController().isOnlineInitialised()) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "InAuth: Online Initialised so just carrying out checks and config updates");
			}
			inAuthOnlineChecks();
			return;
		}

		inAuthOnlineInitAndChecks();
	}

	private void inAuthOnlineInitAndChecks() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "InAuth: Offline Initialised only so carrying out online Init and subsequent checks");
		}
		final long startTime = System.currentTimeMillis();
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				Process.setThreadPriority(Process.THREAD_PRIORITY_LOWEST);
				applicationState.getInAuthController().initialiseOnline(new InAuthController.InAuthOnlineInitialisationListener() {
					@Override
					public void inAuthOnlineInitialised() {
						if (BuildConfig.DEBUG) {
							Log.d(TAG, "InAuth: Online Initialisation took " + (System.currentTimeMillis() - startTime) + " ms");
						}
						inAuthOnlineChecks();

					}
				});
			}
		});
		executorService.shutdown();
	}

	private void inAuthOnlineChecks() {
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				Process.setThreadPriority(Process.THREAD_PRIORITY_LOWEST);
				applicationState.getInAuthController().getRootedDetector().detectRooted(null, true);
				applicationState.getInAuthController().getMalwareDetector().detectMalware(null, true);
				updateConfig();
			}
		});
		executorService.shutdown();
	}
	
	private void handleCriticalMalwareDetected() {
		ErrorResponseWrapperBuilder builder = new ErrorResponseWrapperBuilder(splashActivity);
		splashActivity.handleErrorResponse(builder.buildErrorResponse(CriticalMalwareDetected));
	}

	/**
	 * Starts required api requests
	 */
	protected abstract void startApiRequests();
	
	public void onBatchRequestsSuceeded() {
		activityFlow.onPolicyComplete(splashActivity);
		inAuthOnlineInitialisation();
	}

	public void onBatchRequestsFailed(ErrorResponseWrapper errorResponse) {
		inAuthOnlineInitialisation();
		splashActivity.handleErrorResponse(errorResponse);
	}
	
	protected class SplashCheckDeviceRequest extends CheckDeviceRequest {

		public SplashCheckDeviceRequest() {
			prepareCheckDeviceRequest();
		}

		@Override
		public void onResponse(CheckDeviceResponse response) {
			applicationState.setDeviceId(response.getDeviceID());
			super.onResponse(response);
		}

		private void prepareCheckDeviceRequest() {
			TescoDeviceMetrics deviceMetrics = new TescoDeviceMetrics(applicationState.getDeviceId(), applicationState.getDeviceInfo());
			setDeviceMetrics(deviceMetrics);
			if (applicationState.getCssoId() != null) {
				setCssoID(applicationState.getCssoId());
			}
		}
	}
	
	protected class SplashCheckDeviceNonInteractiveRequest extends CheckDeviceNonInteractiveEvalRiskRequest {

		public SplashCheckDeviceNonInteractiveRequest() {
			setCssoId(applicationState.getCssoId());
		}

		@Override
		public void onResponse(SuccessResponseWrapper response) {
			super.onResponse(response);
		}
	}
	
	protected class SplashGetProductSummaryRequest extends GetPortfolioSummaryRequest {

		@Override
		public void onResponse(GetPortfolioSummaryResponse response) {
			KnownProductFilter<ProductDetails> filter = new KnownProductFilter<ProductDetails>(response.getProducts());
			Collections.sort(filter.getKnownProducts(), new TescoPortfolioSummaryComparator());
			applicationState.setProductDetails(filter.getKnownProducts());
			super.onResponse(response);
		}
	}

	protected class SplashFeaturesRequest extends FeaturesRequest {

		@Override
		public void onResponse(FeaturesResponse response) {
			applicationState.setFeatures(response);
			super.onResponse(response);
		}
	}

	protected class SplashSettingsRequest extends SettingsRequest {

		public SplashSettingsRequest() {
			setDeviceID(applicationState.getDeviceId());
		}

		@Override
		public void onResponse(SettingsResponse response) {
			applicationState.setSettings(response);
			super.onResponse(response);
		}
	}

	protected class SplashRetrieveContentRequest extends RetrieveContentRequest {

		public SplashRetrieveContentRequest() {
			setTbId(applicationState.getTbId());
			setJourneyID(loginJourney);
		}

		@Override
		public void onResponse(RetrieveContentResponse response) {
			activityFlow.getDataBundle().putSerializable(RetrieveContentResponse.class.getName(), (Serializable) response.getDocuments());
			super.onResponse(response);
		}
	}
}
