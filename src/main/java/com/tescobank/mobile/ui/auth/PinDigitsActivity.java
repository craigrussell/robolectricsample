package com.tescobank.mobile.ui.auth;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.authentication.GetPinChallengeResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.security.SecurityException;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.InvalidValueIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoModal;
import com.tescobank.mobile.ui.errors.ErrorHandler;


public class PinDigitsActivity extends TescoActivity implements PinDigitsSubmitter, ErrorHandler {

	private static final String TAG = PinDigitsActivity.class.getSimpleName();
	private static final String BUNDLE_KEY_HELP_DIALOG_SHOWING = " BUNDLE_KEY_HELP_DIALOG_SHOWING";
	public static final String BUNDLE_KEY_PIN_LAYOUT = "BUNDLE_KEY_PIN_LAYOUT";
	public static final String BUNDLE_KEY_PIN_CREATE_ARCOT = "BUNDLE_KEY_PIN_CREATE_ARCOT";

	private PinDigitsActivityHelper pinDigitsActivityHelper;

	private InProgressIndicator inProgressIndicator;

	private TescoModal modal;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating PIN Digits Activity");
		}
		super.onCreate(savedInstanceState);
		setContentView(getLayout());
		configureTitle();
		
		InvalidValueIndicator invalidValueIndicator = new InvalidValueIndicator(this);
		inProgressIndicator = new InProgressIndicator(this, false);
		pinDigitsActivityHelper = new PinDigitsActivityHelper(this, this, this, inProgressIndicator, invalidValueIndicator, getApplicationState(), false);
		updatePinDigitsForResponse();
		
		restoreSavedState(savedInstanceState);
	}
	
	private void restoreSavedState(Bundle savedInstanceState) {
		if (savedInstanceState == null) {
			return;
		}
		if (savedInstanceState.getBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING)) {
			showHelpDialog();
		}
	}
	
	private int getLayout() {
		return getFlow().getDataBundle().getInt(BUNDLE_KEY_PIN_LAYOUT);
	}
	
	private void configureTitle() {
		TextView title = TextView.class.cast(findViewById(R.id.pin_digits_title));
		if(title != null) {
			title.setText(getFlow().getTitleId());
		}
	}
	
	private void updatePinDigitsForResponse() {
		GetPinChallengeResponse pinChallengeResponse = (GetPinChallengeResponse) getFlow().getDataBundle().getSerializable(GetPinChallengeResponse.class.getName());
		pinDigitsActivityHelper.updatePinDigitsForResponse(pinChallengeResponse);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onOptionsItemSelected: " + item);
		}
		switch (item.getItemId()) {
		case R.id.menu_item_information:
			showHelpDialog();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onCreateOptionsMenu");
		}
		getSupportMenuInflater().inflate(R.menu.pin, menu);
		return true;
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (modal != null) {
			outState.putBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING, modal.isShowing());
		}
	}

	protected void showHelpDialog() {
		modal = new TescoModal(this);
		modal.setHeadingText(getString(R.string.auth_pin_modal_header));
		modal.setMessageText(getString(R.string.auth_pin_modal_message));
		modal.show();
	}

	@Override
	protected void onStart() {
		trackAnalytic();
		super.onStart();
	}

	private void trackAnalytic() {
		getApplicationState().getAnalyticsTracker().trackEvent(getFlow().getDisplayAnalytic());
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (hasFocus) {
			showKeyboard();
		}
		super.onWindowFocusChanged(hasFocus);
	}

	@Override
	protected void onRetryPressed() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Retry");
		}

		pinDigitsActivityHelper.handleRetry();
	}

	@Override
	protected void onRetryForPassiveError() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Retry for Passive Error");
		}

		pinDigitsActivityHelper.handleRetryForPassiveError();
		showKeyboard();
	}

	@Override
	public void handleError(ErrorResponseWrapper errorResponseWrapper) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Error");
		}
		
		getFlow().onPolicyError(this, errorResponseWrapper);

		inProgressIndicator.hideInProgressIndicator();
		handleErrorResponse(errorResponseWrapper);
	}

	@Override
	public void submitPinDigits() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Submitting PIN Digits");
		}
		onUserActivity();
	
		// Check that we have PIN digits
		if (!getPinDigitsDataHelper().allRequiredValuesProvided()) {
			handleError(getErrorBuilder().buildErrorResponse(InAppError.IncompletePIN));
			return;
		}

		inProgressIndicator.showInProgressIndicator();
		pinDigitsActivityHelper.startVerifyChallengeResponseRequest();
	}
	
	public void onVerifyChallengeResponseComplete(String encryptedToken) {
		if(shouldCreateArcotAccount()) {
			createArcotAccount(encryptedToken);
		} else {
			getFlow().onPolicyComplete(PinDigitsActivity.this);
		}
	}
	
	private void createArcotAccount(String encryptedToken) {
		try {
			getApplicationState().setIsPassword(true);
			getApplicationState().getSecurityProvider().createAccount(encryptedToken);
			getFlow().onPolicyComplete(PinDigitsActivity.this);			
		} catch (SecurityException e) {
			handleError(getErrorBuilder().buildErrorResponse(InAppError.CreateAccount));
		}
	}

	@Override
	protected void onDestroy() {
		if (null != modal) {
			modal.dismiss();
		}
		super.onDestroy();
	}

	private PinDigitsDataHelper getPinDigitsDataHelper() {
		return pinDigitsActivityHelper.getPinDigitsDataHelper();
	}
	
	public boolean shouldCreateArcotAccount() {
		return getFlow().getDataBundle().getBoolean(BUNDLE_KEY_PIN_CREATE_ARCOT, false);
	}
}
