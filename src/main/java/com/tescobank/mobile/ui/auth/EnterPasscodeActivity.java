package com.tescobank.mobile.ui.auth;

import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_BACK_ON_PASSWORD_CHANGED_ONLINE;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.authentication.AuthenticateOTPRequest;
import com.tescobank.mobile.api.authentication.AuthenticateOTPResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.rest.processor.RestRequestProcessor;
import com.tescobank.mobile.security.OTPGenerationListener;
import com.tescobank.mobile.security.SecurityProvider;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.InvalidValueIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoModal;
import com.tescobank.mobile.ui.auth.passcode.PasscodeActivityHelper;
import com.tescobank.mobile.ui.auth.passcode.PasscodeDataHelper;
import com.tescobank.mobile.ui.auth.passcode.PasscodeSubmitter;
import com.tescobank.mobile.ui.errors.ErrorHandler;

/**
 * Activity for entering an existing passcode when changing passcode
 */
public class EnterPasscodeActivity extends TescoActivity implements PasscodeSubmitter, OTPGenerationListener, ErrorHandler {

	private static final String BUNDLE_KEY_HELP_DIALOG_SHOWING = "helpDialogShowing";
	public static final String BUNDLE_KEY_EXISTING_PROMPT = "BUNDLE_KEY_EXISTING_PROMPT";
	private ApplicationState applicationState;
	private RestRequestProcessor<ErrorResponseWrapper> requestProcessor;
	private SecurityProvider securityProvider;
	private InProgressIndicator inProgressIndicator;
	private PasscodeActivityHelper passcodeActivityHelper;
	private TescoModal modal;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auth_passcode);
		configureInitialMessagePrompt();
		configureTitle();
		applicationState = getApplicationState();
		securityProvider = applicationState.getSecurityProvider();
		requestProcessor = applicationState.getRestRequestProcessor();

		InvalidValueIndicator invalidValueIndicator = new InvalidValueIndicator(this);
		inProgressIndicator = new InProgressIndicator(this, false);
		passcodeActivityHelper = new PasscodeActivityHelper(this, this, invalidValueIndicator);
		passcodeActivityHelper.requestFocus();
		restoreSavedState(savedInstanceState);
	}

	private void restoreSavedState(Bundle savedInstanceState) {
		if(savedInstanceState == null) {
			return;
		}
		if (savedInstanceState.getBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING)) {
			showHelpDialog();
		}
	}
	
	private void configureInitialMessagePrompt() {
		TextView message = TextView.class.cast(findViewById(R.id.passcode_message));
		message.setText(getFlow().getDataBundle().getInt(BUNDLE_KEY_EXISTING_PROMPT));
	}
	
	private void configureTitle() {
		TextView title = TextView.class.cast(findViewById(R.id.passcode_title));
		if(title != null) {
			title.setText(getFlow().getTitleId());
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_item_information:
			showHelpDialog();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	private void showHelpDialog() {
		modal = new TescoModal(this);
		modal.setHeadingText(getString(R.string.auth_passcode_entry_modal_header));
		modal.setMessageText(getString(R.string.auth_passcode_entry_modal_message));
		modal.show();
	}

	@Override
	protected void onStart() {
		trackAnalytic();
		super.onStart();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(null != modal) {
			outState.putBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING, modal.isShowing());
		}
	}
	
	@Override
	protected void onDestroy() {
		if(null != modal) {
			modal.dismiss();
		}
		super.onDestroy();
	}

	/**
	 * Tracks analytics
	 */
	private void trackAnalytic() {
		applicationState.getAnalyticsTracker().trackUserChangePasscodeExistingPasscode();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if(hasFocus) {
			showKeyboard();
		}
		super.onWindowFocusChanged(hasFocus);
	}
	
	@Override
	protected void onRetryPressed() {
		returnToEnterPasscode();
	}

	@Override
	protected void onRetryForPassiveError() {
		passcodeActivityHelper.handleRetryForPassiveError();
	}

	@Override
	public void handleError(ErrorResponseWrapper errorResponseWrapper) {
		handleErrorResponse(errorResponseWrapper);
	}

	@Override
	public void submitPasscode() {
		// Before we do anything lets validate
		if (!getPasscodeDataHelper().allRequiredValuesProvided()) {
			// No passcode
			returnToEnterPasscode();
			handleError(getErrorBuilder().buildErrorResponse(InAppError.IncompletePasscode));

		} else {
		
			//passed client validation so lets proceed!
			inProgressIndicator.showInProgressIndicator();
			// Generate an OTP
			generateOTP();
		}
	}

	/**
	 * Generates an OTP
	 */
	private void generateOTP() {
		securityProvider.generateOTP(passcodeActivityHelper.getPasscodeDataHelper().getPasscode(), this);
	}

	@Override
	public void otpGenerated(String otp) {
		if (otp == null) {
			handleError(getErrorBuilder().buildErrorResponse(InAppError.GenerateOTP));
		} else {
			startAuthenticateOTPRequest(otp);
		}
	}

	/**
	 * Starts an authenticate OTP request for the specified OTP
	 * 
	 * @param otp
	 *            the OTP
	 */
	private void startAuthenticateOTPRequest(String otp) {
		AuthenticateOTPRequest request = new PasscodeAuthenticateOTPRequest();

		request.setTbId(applicationState.getTbId());
		request.setDeviceId(applicationState.getDeviceId());
		request.setOtp(otp);

		requestProcessor.processRequest(request);
	}

	private void onDone() {
		getFlow().getDataBundle().putString(PASSWORD_EXTRA, passcodeActivityHelper.getPasscodeDataHelper().getPasscode());
		getFlow().onPolicyComplete(this);
	}
	
	private void returnToEnterPasscode() {		
		inProgressIndicator.hideInProgressIndicator();
		passcodeActivityHelper.handleRetry();
		passcodeActivityHelper.getPasscodeDataHelper().setInputFieldsEnabled(true);
	}

	
	/**
	 * @return the passcode activity helper's data helper
	 */
	private PasscodeDataHelper getPasscodeDataHelper() {
		return passcodeActivityHelper.getPasscodeDataHelper();
	}
	
	protected class PasscodeAuthenticateOTPRequest extends AuthenticateOTPRequest {

		@Override
		public void onResponse(AuthenticateOTPResponse response) {

			if (!response.getAuthenticated()) {
				returnToEnterPasscode();
				handleError(getErrorBuilder().buildErrorResponse(InAppError.HttpStatus200));
				showKeyboard();
				return;
			}

			securityProvider.lastOTPWasSuccessful();
			onDone();
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
			returnToEnterPasscode();
			handleError(errorResponseWrapper);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.passcode, menu);
		return true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_BACK_ON_PASSWORD_CHANGED_ONLINE) {
			passcodeActivityHelper.handleRetry();
			return;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
