package com.tescobank.mobile.ui.auth;

import android.os.Bundle;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.flow.FlowInitiator;
import com.tescobank.mobile.network.NetworkStatusChecker;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.TescoActivity;

/**
 * Activity for loading displaying a splash progress screen while coordinating startup web services calls
 */
public class SplashActivity extends TescoActivity {

	private NetworkStatusChecker networkStatusChecker;
	private InProgressIndicator inProgressIndicator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getApplicationState().resetForNewSession();
		setContentView(R.layout.activity_auth_splash);
		networkStatusChecker = new NetworkStatusChecker();
		inProgressIndicator = new InProgressIndicator(this, false);
		startLogin();
	}

	private void startLogin() {
		if (!networkStatusChecker.isAvailable(this)) {
			handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.NoInternetConnectionAtLogin));
			return;
		}
		getLoginFlowInitiator().execute();
	}

	private FlowInitiator getLoginFlowInitiator() {
		if (!getApplicationState().isRegistered()) {
			return new FirstTimeLoginFlowInitiator(this, getFlow());
		} else {
			return new SubsequentLoginFlowInitiator(this, getFlow());
		}
	}
	
	@Override
	protected void registerNetworkReceivers() {
	}

	@Override
	protected void unregisterNetworkReceivers() {
	}
	
	@Override
	protected void onRetryPressed() {
		startLogin();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		inProgressIndicator.showInProgressIndicator();
		hideKeyboard();
		super.onWindowFocusChanged(hasFocus);
	}
}
