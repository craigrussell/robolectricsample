package com.tescobank.mobile.ui.auth.password;

import java.util.regex.Pattern;

/**
 * Password validator
 */
public class PasswordValidator {
	
	/** The validation pattern */
	private static final Pattern PATTERN = Pattern.compile("[A-Za-z0-9]{7,}");
	
	/** Empty string */
	private static final String EMPTY_STRING = "";
	
	/**
	 * Constructor
	 */
	public PasswordValidator() {
		super();
	}
	
	/**
	 * Validates the specified password
	 * 
	 * @param password the password
	 * 
	 * @return true if the password is valid
	 */
	public boolean isValid(String password) {
		return PATTERN.matcher((password == null) ? EMPTY_STRING : password.trim()).matches();
	}
}
