package com.tescobank.mobile.ui.auth;

import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_BACK_ON_PASSWORD_CHANGED_ONLINE;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createConfirmAndExitAction;
import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createConfirmAndRestartLoginAction;
import static com.tescobank.mobile.ui.auth.PAMActivity.PAM_IMAGE;
import static com.tescobank.mobile.ui.auth.PAMActivity.PAM_PHRASE;

import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.authentication.AuthenticateOTPRequest;
import com.tescobank.mobile.api.authentication.AuthenticateOTPResponse;
import com.tescobank.mobile.api.authentication.GetPinChallengeResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.rest.processor.RestRequestProcessor;
import com.tescobank.mobile.security.OTPGenerationListener;
import com.tescobank.mobile.security.SecurityProvider;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.InvalidValueIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoModal;
import com.tescobank.mobile.ui.auth.password.PasswordActivityHelper;
import com.tescobank.mobile.ui.auth.password.PasswordSubmitter;
import com.tescobank.mobile.ui.balancepeek.BalancePeekListRowBuilder;
import com.tescobank.mobile.ui.balancepeek.BalancePeekPanelPositioner;
import com.tescobank.mobile.ui.balancepeek.BalancePeekSlidingPanelTouchListener;
import com.tescobank.mobile.ui.balancepeek.BalancePeekViewAnimator;
import com.tescobank.mobile.ui.balancepeek.SlidingPanelPositioner;
import com.tescobank.mobile.ui.balancepeek.SlidingPanelUIWrapper;
import com.tescobank.mobile.ui.errors.ErrorHandler;
import com.tescobank.mobile.ui.settings.UnauthenticatedSettingsFragment;

/**
 * Activity for entering PIN & password during subsequent login
 */
public class LoginWithPasswordActivity extends TescoActivity implements PinDigitsSubmitter, PasswordSubmitter, OTPGenerationListener, ErrorHandler {

	private static final String TAG = LoginWithPasswordActivity.class.getSimpleName();
	private static final String BUNDLE_KEY_RESET_PASSWORD_DIALOG_SHOWING = "resetPasswordDialogShowing";
	private static final String BUNDLE_KEY_HELP_DIALOG_SHOWING = "BUNDLE_KEY_HELP_DIALOG_SHOWING";
	private static final String BUNDLE_KEY_ENTERING_PIN = "BUNDLE_KEY_ENTERING_PIN";
	private static long INACTIVITY_DEFAULT_TIMEOUT = 5000l;
	private static final int ADJUST_BY_ONE = 1;
	
	private boolean enteringPin = true;
	private View pinContent;
	private View passwordContent;
	private ApplicationState applicationState;
	private SecurityProvider securityProvider;
	private RestRequestProcessor<ErrorResponseWrapper> requestProcessor;
	private InvalidValueIndicator invalidValueIndicator;
	private InProgressIndicator inProgressIndicator;
	private PinDigitsActivityHelper pinDigitsActivityHelper;
	private PasswordActivityHelper passwordActivityHelper;
	private ImageView pamImageView;
	private TextView textView;
	private UnauthenticatedSettingsFragment unauthenticatedSettingsFragment;
	private TescoModal modal;

	private float origin;
	private float originOffset;
	private ViewGroup panelTop;
	private ViewGroup panelBottom;
	private ViewGroup accountLayoutContainer;
	private ViewGroup root;
	private LinearLayout balancePeekAccountList;
	private BalancePeekSlidingPanelTouchListener balancePeekSlidingPanelTouchListener;
	private ImageView dragButton;
	private boolean calculatedBalancePeekValues;
	private ImageView trapDoorTop;
	private ImageView trapDoorBottom;
	private int balancePeekAnimationDuration;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		unauthenticatedSettingsFragment = new UnauthenticatedSettingsFragment();
		getDrawer().setContentViewWithDrawer(R.layout.activity_auth_login_with_password, unauthenticatedSettingsFragment);
		ActionBarConfiguration.setToPreLoginHamburger(this, getSupportActionBar());

		pinContent = findViewById(R.id.activity_login_pin_content);
		passwordContent = findViewById(R.id.activity_login_password_content);

		applicationState = getApplicationState();
		securityProvider = applicationState.getSecurityProvider();
		requestProcessor = applicationState.getRestRequestProcessor();

		// Create the helpers
		invalidValueIndicator = new InvalidValueIndicator(this);
		inProgressIndicator = new InProgressIndicator(this, false);

		passwordActivityHelper = new PasswordActivityHelper(this, this, invalidValueIndicator);
		pamImageView = (ImageView) this.findViewById(R.id.imageView1);
		textView = (TextView) this.findViewById(R.id.pamText);
		unauthenticatedSettingsFragment.initialise(this, inProgressIndicator);

		setBalancePeekViewReferences();
		hideBalancePeekViews();
		configureBalancePeek();

		if ( savedInstanceState != null) {	
			restoreSavedState(savedInstanceState);
		}
	}

	private void configurePinDigits() {
		pinDigitsActivityHelper = new PinDigitsActivityHelper(this, this, this, inProgressIndicator, invalidValueIndicator, applicationState, true, balancePeekSlidingPanelTouchListener);
		GetPinChallengeResponse pinChallengeResponse = (GetPinChallengeResponse) getFlow().getDataBundle().getSerializable(GetPinChallengeResponse.class.getName());
		pinDigitsActivityHelper.updatePinDigitsForResponse(pinChallengeResponse);
	}

	private void restoreSavedState(Bundle savedInstanceState) {
		enteringPin = savedInstanceState.getBoolean(BUNDLE_KEY_ENTERING_PIN);
		if (!enteringPin) {
			hideBalancePeekViews();
			showPasswordContent();
		}

		if (savedInstanceState.getBoolean(BUNDLE_KEY_RESET_PASSWORD_DIALOG_SHOWING)) {
			unauthenticatedSettingsFragment.showConfirmResetPasswordRequiredDialog();
		}
		if (savedInstanceState.getBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING)) {
			showHelpDialog();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (modal != null) {
			outState.putBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING, modal.isShowing());
		}
		outState.putBoolean(BUNDLE_KEY_ENTERING_PIN, enteringPin);
		outState.putBoolean(BUNDLE_KEY_RESET_PASSWORD_DIALOG_SHOWING, unauthenticatedSettingsFragment.isResetPasswordWarningDialogShowing());
	}

	@Override
	protected void onStart() {
		Bitmap image = getFlow().getDataBundle().getParcelable(PAM_IMAGE);
		pamImageView.setImageBitmap(image);
		textView.setText(getFlow().getDataBundle().getString(PAM_PHRASE));
		trackAnalytic();
		super.onStart();
	}

	@Override
	protected void onDestroy() {
		unauthenticatedSettingsFragment.dismissResetPasswordWarningDialog();
		if (null != modal) {
			modal.dismiss();
		}
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.pin, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_item_information:
			closeDrawer();
			showHelpDialog();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void showHelpDialog() {
		modal = new TescoModal(this);
		if (enteringPin) {
			modal.setHeadingText(getString(R.string.auth_pin_modal_header));
			modal.setMessageText(getString(R.string.auth_pin_modal_message));
			modal.show();
		} else {
			modal.setHeadingText(getString(R.string.auth_password_modal_header));
			modal.setMessageText(getString(R.string.auth_password_modal_message));
			modal.show();
		}
	}

	private void trackAnalytic() {
		applicationState.getAnalyticsTracker().trackEvent(getFlow().getDisplayAnalytic());
	}

	@Override
	protected void onPause() {
		super.onPause();
		securityProvider.cancelOTPGeneration();

		if (balancePeekSlidingPanelTouchListener != null) {
			balancePeekSlidingPanelTouchListener.closePanels(BalancePeekSlidingPanelTouchListener.ANIMATION_DURATION_NONE);
		}

	}

	@Override
	protected void onResume() {
		passwordActivityHelper.setCursorVisible(true);
		getTooltipManager().show();
		super.onResume();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (hasFocus) {
			if (!calculatedBalancePeekValues) {
				calculateBalancePeekLayoutValues();
			}
			if (enteringPin) {
				showBalancePeekViewsWithAnimation();
				configurePinDigits();
			}
		}
		super.onWindowFocusChanged(hasFocus);
	}

	@Override
	protected void onRetryPressed() {
		if (enteringPin) {
			pinDigitsActivityHelper.handleRetry();
		} else if (PasswordAuthenticateOTPRequest.class.equals(getApplicationState().getRestRequestProcessor().getLastRequestTypeForRequesterType(getClass()))) {
			returnToEnterPassword();
		} else {
			onDone();
		}
	}

	@Override
	protected void onBackFromRetryPressed() {
		returnToEnterPassword();
	}

	private void returnToEnterPassword() {
		inProgressIndicator.hideInProgressIndicator();
		passwordActivityHelper.handleRetry();
		passwordActivityHelper.setCursorVisible(true);
		showKeyboard();
	}

	@Override
	protected void onRetryForPassiveError() {
		if (enteringPin) {
			pinDigitsActivityHelper.handleRetry();
			pinDigitsActivityHelper.handleRetryForPassiveError();
		} else {
			passwordActivityHelper.handleRetry();
			passwordActivityHelper.handleRetryForPassiveError();
		}

		showKeyboard();
	}

	@Override
	public void handleError(ErrorResponseWrapper errorResponseWrapper) {
		handleErrorResponse(errorResponseWrapper);
	}

	public void onContinuePressed(View view) {
		submitPassword();
	}

	@Override
	public void onBackPressed() {
		if (enteringPin) {
			getFlow().updatePolicyCancellation(createConfirmAndExitAction());
		} else {
			getFlow().updatePolicyCancellation(createConfirmAndRestartLoginAction());
		}
		super.onBackPressed();
	}

	@Override
	public void submitPinDigits() {
		onUserActivity();
		// Check that we have PIN digits
		if (!getPinDigitsDataHelper().allRequiredValuesProvided()) {
			handleError(getErrorBuilder().buildErrorResponse(InAppError.IncompletePIN));
			return;
		}
		inProgressIndicator.showInProgressIndicator();

		// Verify the challenge response
		pinDigitsActivityHelper.startVerifyChallengeResponseRequest();
	}

	@Override
	public void onVerifyChallengeResponseComplete(String encryptedToken) {

		enteringPin = false;
		hideBalancePeekViewsWithAnimation();
		showPasswordContentWithAnimation();
	}

	private void showPasswordContent() {
		passwordActivityHelper.givePasswordFieldFocus();
		inProgressIndicator.setIncludeContinue(true);
		inProgressIndicator.showContinue();
		pinContent.setVisibility(View.GONE);
		passwordContent.setVisibility(View.VISIBLE);
		showKeyboard();
	}

	/**
	 * Replaces PIN content with password content
	 */
	private void showPasswordContentWithAnimation() {

		final Animation enterRight = AnimationUtils.loadAnimation(this, R.anim.anim_enter_right);
		Animation exitLeft = AnimationUtils.loadAnimation(this, R.anim.anim_exit_left);

		exitLeft.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				deregisterBalancePeekListeners();
				showPasswordContent();
				passwordContent.startAnimation(enterRight);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});

		pinContent.startAnimation(exitLeft);
	}

	@Override
	public void submitPassword() {
		// Check that we have a password
		if (passwordActivityHelper.getPassword().trim().isEmpty()) {
			handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.BlankPassword));
			return;
		}

		passwordActivityHelper.setCursorVisible(false);
		inProgressIndicator.showInProgressIndicator();

		// Generate an OTP
		generateOTP();
	}

	private void generateOTP() {
		securityProvider.generateOTP(passwordActivityHelper.getPassword(), this);
	}

	@Override
	public void otpGenerated(String otp) {
		if (otp == null) {
			handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.GenerateOTP));

		} else {
			startAuthenticateOTPRequest(otp);
		}
	}

	private void startAuthenticateOTPRequest(String otp) {
		AuthenticateOTPRequest request = new PasswordAuthenticateOTPRequest();

		request.setTbId(applicationState.getTbId());
		request.setDeviceId(applicationState.getDeviceId());
		request.setOtp(otp);

		requestProcessor.processRequest(request, getClass());
	}

	private PinDigitsDataHelper getPinDigitsDataHelper() {
		return pinDigitsActivityHelper.getPinDigitsDataHelper();
	}

	private void onDone() {
		getFlow().onPolicyComplete(this);
	}

	private void configureBalancePeek() {
		balancePeekAnimationDuration = BalancePeekViewAnimator.DEFAULT_FADE_ANIMATION_DURATION;
		boolean isNonInteractiveProductsEnabled = getApplicationState().getFeatures().isNonInteractiveProductsEnabled();
		boolean isBalancePeekEnabled = getApplicationState().getPersister().isBalancePeekEnabled();
		List<ProductDetails> products = getApplicationState().getProductDetails();
		BalancePeekListRowBuilder builder = new BalancePeekListRowBuilder(getResources(), getLayoutInflater(), products, R.layout.balance_peek_list_row, isNonInteractiveProductsEnabled,
				isBalancePeekEnabled);
		for (int i = 0; i < builder.getCount(); i++) {
			View view = builder.createView(i);
			balancePeekAccountList.addView(view);
		}
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		if (balancePeekSlidingPanelTouchListener != null) {
			balancePeekSlidingPanelTouchListener.userInteractionOccurred();
		}

		return super.dispatchTouchEvent(event);
	}

	private void deregisterBalancePeekListeners() {
		panelBottom.setOnTouchListener(null);
		balancePeekSlidingPanelTouchListener.closePanels(0);
		balancePeekSlidingPanelTouchListener = null;
	}

	private void showBalancePeekViewsWithAnimation() {
		BalancePeekViewAnimator viewAnimator = new BalancePeekViewAnimator();
		viewAnimator.addTransitionView(dragButton);
		viewAnimator.addTransitionView(trapDoorTop);
		viewAnimator.addTransitionView(trapDoorBottom);
		viewAnimator.addPostTransitionView(accountLayoutContainer);
		viewAnimator.showComponents(balancePeekAnimationDuration);
	}

	private void hideBalancePeekViews() {
		float noAlpha = 0f;
		accountLayoutContainer.setAlpha(noAlpha);
		dragButton.setAlpha(noAlpha);
		trapDoorTop.setAlpha(noAlpha);
		trapDoorBottom.setAlpha(noAlpha);
	}

	private void hideBalancePeekViewsWithAnimation() {
		BalancePeekViewAnimator viewAnimator = new BalancePeekViewAnimator();
		viewAnimator.addPreTransitionView(accountLayoutContainer);
		viewAnimator.addTransitionView(dragButton);
		viewAnimator.addTransitionView(trapDoorTop);
		viewAnimator.addTransitionView(trapDoorBottom);
		viewAnimator.hideComponents(balancePeekAnimationDuration);
	}

	private void setBalancePeekViewReferences() {
		balancePeekAccountList = LinearLayout.class.cast(findViewById(R.id.balance_peek_list));
		panelTop = ViewGroup.class.cast(findViewById(R.id.topPanel));
		panelBottom = ViewGroup.class.cast(findViewById(R.id.bottomPanel));
		accountLayoutContainer = ViewGroup.class.cast(findViewById(R.id.balancePeekContainer));
		balancePeekAccountList = LinearLayout.class.cast(findViewById(R.id.balance_peek_list));
		root = ViewGroup.class.cast(findViewById(android.R.id.content));
		dragButton = ImageView.class.cast(findViewById(R.id.dragButton));
		trapDoorTop = ImageView.class.cast(findViewById(R.id.trapdoorTopImage));
		trapDoorBottom = ImageView.class.cast(findViewById(R.id.bottomPanelImage));
	}

	private void calculateBalancePeekLayoutValues() {
		if (BuildConfig.DEBUG) {
			Log.i(TAG, "calculating balance peek layout values");
		}

		calculateOriginOffset();
		calculateOrigin();

		float bottomOfWindow = panelBottom.getY() + panelBottom.getHeight();

		int accountActualHeight = balancePeekAccountList.getHeight();
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "accountActualHeight: " + accountActualHeight);
		}
		if (accountActualHeight < bottomOfWindow) {
			positionAccountListOnCentre(bottomOfWindow);
		}

		initialiseSlidingPanel();
		calculatedBalancePeekValues = true;
	}

	private void positionAccountListOnCentre(float bottomOfWindow) {
		float y = origin - (accountLayoutContainer.getHeight() / 2);
		Log.i(TAG, "account layout height: " + accountLayoutContainer.getHeight());
		accountLayoutContainer.setY(y);

		if (accountLayoutContainer.getY() < 0) {
			Log.i(TAG, "Account list off screen above allowed position, moving to panelAYLimit");
			accountLayoutContainer.setY(0);
		} else if (accountLayoutContainer.getY() + accountLayoutContainer.getHeight() > bottomOfWindow) {
			Log.i(TAG, "Account list off screen below allowed position, moving so that bottom is equal to panelBYLimit");
			accountLayoutContainer.setY(bottomOfWindow - accountLayoutContainer.getHeight());
		} else {
			Log.i(TAG, "Account list not requiring moving");
		}
	}

	private void calculateOrigin() {
		origin = panelTop.getY() + panelTop.getHeight() - originOffset;
		Log.i(TAG, String.format("Origin is %f", origin));
	}

	private void calculateOriginOffset() {
		originOffset = (trapDoorTop.getHeight() / 2.0f) + ADJUST_BY_ONE;
	}

	private void initialiseSlidingPanel() {
		SlidingPanelUIWrapper panelUIWrapper = new SlidingPanelUIWrapper(panelTop, panelBottom, accountLayoutContainer, root);
		SlidingPanelPositioner panelPositioner = new BalancePeekPanelPositioner(panelUIWrapper, origin, originOffset);
		balancePeekSlidingPanelTouchListener = BalancePeekSlidingPanelTouchListener.create(this, panelUIWrapper, panelPositioner, origin, originOffset);
		balancePeekSlidingPanelTouchListener.configureInactivityTimeout(this, INACTIVITY_DEFAULT_TIMEOUT);
		balancePeekSlidingPanelTouchListener.setDraggingIndicator(dragButton);
		panelBottom.setOnTouchListener(balancePeekSlidingPanelTouchListener);
		panelBottom.setY(origin - originOffset);

		OnTouchListener closePanelsIfOpenedListener = new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
					if (balancePeekEnabledAndShowing()) {
						balancePeekSlidingPanelTouchListener.closePanels(BalancePeekSlidingPanelTouchListener.ANIMATION_DURATION_DEFAULT);
						return true;
					}
				}
				return false;
			}

			private boolean balancePeekEnabledAndShowing() {
				return balancePeekSlidingPanelTouchListener != null && balancePeekSlidingPanelTouchListener.panelsAreOpen();
			}
		};
		panelTop.setOnTouchListener(closePanelsIfOpenedListener);
	}

	private class PasswordAuthenticateOTPRequest extends AuthenticateOTPRequest {

		@Override
		public void onResponse(AuthenticateOTPResponse response) {

			if (!response.getAuthenticated()) {
				returnToEnterPassword();
				handleError(getErrorBuilder().buildErrorResponse(InAppError.HttpStatus200));
				return;
			}

			securityProvider.lastOTPWasSuccessful();
			onDone();
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
			returnToEnterPassword();
			handleError(errorResponseWrapper);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_BACK_ON_PASSWORD_CHANGED_ONLINE) {
			pinDigitsActivityHelper.handleRetry();
			return;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void setBalancePeekAnimationDuration(int balancePeekAnimationDuration) {
		this.balancePeekAnimationDuration = balancePeekAnimationDuration;
	}
}
