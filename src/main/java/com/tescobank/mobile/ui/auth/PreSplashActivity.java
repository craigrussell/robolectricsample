package com.tescobank.mobile.ui.auth;

import android.os.Bundle;

import com.tescobank.mobile.flow.Flow;
import com.tescobank.mobile.ui.TescoActivity;

public class PreSplashActivity extends TescoActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		restartMain();
	}

	public void restartMain() {
		Flow flow;
		if (!getApplicationState().isRegistered()) {
			flow = AuthFlowFactory.createRegistrationLoginFlow();
		} else if (getApplicationState().isPassword()) {
			flow = AuthFlowFactory.createLoginWithPasswordFlow();
		} else {
			flow = AuthFlowFactory.createLoginWithPasscodeFlow();
		}
		flow.begin(this);
	}
}
