package com.tescobank.mobile.ui.auth;

import java.util.ArrayList;
import java.util.List;

import com.tescobank.mobile.ui.TescoEditText;

/**
 * SMS data helper; handles null / bad data by ignoring it
 */
public class SMSDataHelper extends AuthDataHelper {

	/** Indices of required values. */
	private static final Integer[] INDICES_OF_REQUIRED_VALUES = new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8 };

	/** Empty string */
	private static final String EMPTY_STRING = "";

	/**
	 * Constructor
	 * 
	 * @param inputFieldsForAllValues
	 *            input fields for all values, e.g. [I1, I2, I3, I4, I5, I6, I7, I8]
	 */
	public SMSDataHelper(List<TescoEditText> inputFieldsForAllValues) {
		super(INDICES_OF_REQUIRED_VALUES, inputFieldsForAllValues);
	}

	/**
	 * @param pasteText
	 *            paste text, e.g. "12X3Y4"
	 * 
	 * @return values for input fields for required values, e.g. ["1", "2", "3", "4", "", "", "", ""]
	 */
	public List<String> getValuesForInputFieldsForRequiredValues(String pasteText) {

		List<String> values = new ArrayList<String>();

		int inputFieldCount = getInputFieldsForRequiredValues().size();

		// Add as much paste text as we have room for
		addPasteText(pasteText, values, inputFieldCount);

		// Fill the remainder with empty strings
		fillWithEmptyStrings(values, inputFieldCount);

		return values;
	}
	
	private void addPasteText(String pasteText, List<String> values, int inputFieldCount) {
		if (pasteText != null) {
			for (int pasteTextIndex = 0; (pasteTextIndex < pasteText.length()) && (values.size() < inputFieldCount); pasteTextIndex++) {

				Integer pasteTextDigit = getDigitValue(String.valueOf(pasteText.charAt(pasteTextIndex)));
				if (pasteTextDigit != null) {
					values.add(pasteTextDigit.toString());
				}
			}
		}
	}
	
	private void fillWithEmptyStrings(List<String> values, int inputFieldCount) {
		while (values.size() < inputFieldCount) {
			values.add(EMPTY_STRING);
		}
	}

	public void setCursorVisible(boolean visible) {
		List<TescoEditText> inputFields = getInputFieldsForRequiredValues();
		for (TescoEditText inputField : inputFields) {
			inputField.setCursorVisible(visible);
		}
	}
}
