package com.tescobank.mobile.ui.auth;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.authentication.AuthenticateOTPRequest;
import com.tescobank.mobile.api.authentication.AuthenticateOTPResponse;
import com.tescobank.mobile.api.authentication.FetchOTPTokenRequest;
import com.tescobank.mobile.api.authentication.FetchOTPTokenResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.rest.processor.RestRequestProcessor;
import com.tescobank.mobile.security.OTPGenerationListener;
import com.tescobank.mobile.security.SecurityException;
import com.tescobank.mobile.security.SecurityProvider;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.InvalidValueIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoModal;
import com.tescobank.mobile.ui.auth.password.PasswordActivityHelper;
import com.tescobank.mobile.ui.auth.password.PasswordSubmitter;
import com.tescobank.mobile.ui.errors.ErrorHandler;

public class EnterPasswordActivity extends TescoActivity implements PasswordSubmitter, ErrorHandler, OTPGenerationListener {

	private static final String TAG = EnterPasswordActivity.class.getSimpleName();
	private static final String BUNDLE_KEY_HELP_DIALOG_SHOWING = "helpDialogShowing";
	public static final String BUNDLE_KEY_ENTER_PASSWORD_LAYOUT = "BUNDLE_KEY_ENTER_PASSWORD_LAYOUT";
	public static final String BUNDLE_KEY_GET_OTP_CREATE_ARCOT = "BUNDLE_KEY_GET_OTP_CREATE_ARCOT";

	
	private ApplicationState applicationState;
	private SecurityProvider securityProvider;
	private RestRequestProcessor<ErrorResponseWrapper> requestProcessor;
	private InProgressIndicator inProgressIndicator;
	private PasswordActivityHelper passwordActivityHelper;
	private TescoModal modal;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayout());
		configureTitle();

		applicationState = getApplicationState();
		securityProvider = applicationState.getSecurityProvider();
		requestProcessor = applicationState.getRestRequestProcessor();

		InvalidValueIndicator invalidValueIndicator = new InvalidValueIndicator(this);
		inProgressIndicator = new InProgressIndicator(this, true);
		passwordActivityHelper = new PasswordActivityHelper(this, this, invalidValueIndicator);
		
		restoreSavedState(savedInstanceState);
	}
	
	private int getLayout() {
		return getFlow().getDataBundle().getInt(BUNDLE_KEY_ENTER_PASSWORD_LAYOUT);
	}
	
	private void configureTitle() {
		TextView title = TextView.class.cast(findViewById(R.id.enter_password_title));
		if(title != null) {
			title.setText(getFlow().getTitleId());
		}
	}
	
	public boolean shouldGetOTPAndCreateArcot() {
		return getFlow().getDataBundle().getBoolean(BUNDLE_KEY_GET_OTP_CREATE_ARCOT, false);
	}
	
	private void restoreSavedState(Bundle savedInstanceState) {
		if (savedInstanceState == null) {
			return;
		}
		if (savedInstanceState.getBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING)) {
			showHelpDialog();
		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(null != modal) {
			outState.putBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING, modal.isShowing());
		}	
	}
	
	@Override
	protected void onDestroy() {
		if(null != modal) {
			modal.dismiss();
		}
		super.onDestroy();
	}

	@Override
	protected void onStart() {
		trackAnalytic();
		super.onStart();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onCreateOptionsMenu");
		}
		getSupportMenuInflater().inflate(R.menu.password, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onOptionsItemSelected: " + item);
		}
		switch (item.getItemId()) {
		case R.id.menu_item_information:
			showHelpDialog();
			return true;
		}
		hideKeyboard();
		return super.onOptionsItemSelected(item);
	}

	private void showHelpDialog() {
		modal = new TescoModal(this);
		modal.setHeadingText(getString(R.string.auth_password_modal_header));
		modal.setMessageText(getString(R.string.auth_password_modal_message));
		modal.show();
	}

	private void trackAnalytic() {
		applicationState.getAnalyticsTracker().trackEvent(getFlow().getDisplayAnalytic());
	}

	@Override
	protected void onPause() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Pause");
		}

		super.onPause();
		securityProvider.cancelOTPGeneration();
	}

	@Override
	protected void onResume() {
		passwordActivityHelper.setCursorVisible(true);
		super.onResume();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (hasFocus) {
			showKeyboard();
		}
		super.onWindowFocusChanged(hasFocus);
	}

	private void onDone() {
		getFlow().getDataBundle().putString(PASSWORD_EXTRA, passwordActivityHelper.getPassword());
		getFlow().onPolicyComplete(this);
	}

	@Override
	protected void onRetryPressed() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Retry");
		}

		passwordActivityHelper.handleRetry();
		showKeyboard();
	}

	@Override
	protected void onRetryForPassiveError() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Retry for Passive Error");
		}

		onRetryPressed();
		passwordActivityHelper.handleRetryForPassiveError();
	}

	@Override
	public void handleError(ErrorResponseWrapper errorResponseWrapper) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Error");
		}
		getFlow().onPolicyError(this, errorResponseWrapper);
		inProgressIndicator.hideInProgressIndicator();
		handleErrorResponse(errorResponseWrapper);
	}

	public void onContinuePressed(View view) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Continue");
		}
		submitPassword();
	}

	@Override
	public void submitPassword() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Submitting Password");
		}

		// Check that we have a password
		if (passwordActivityHelper.getPassword().trim().isEmpty()) {
			handleError(getErrorBuilder().buildErrorResponse(InAppError.BlankPassword));
			return;
		}

		passwordActivityHelper.setCursorVisible(false);
		inProgressIndicator.showInProgressIndicator();

		if(shouldGetOTPAndCreateArcot()) {
			fetchOTPToken();
		} else {
			generateOTP();
		}
	}
	
	private void fetchOTPToken() {
		FetchOTPTokenRequest request = new FetchOTPTokenRequest() {

			@Override
			public void onResponse(FetchOTPTokenResponse response) {
				createArcotAccount(response.getEncryptedToken());
			}

			@Override
			public void onErrorResponse(ErrorResponseWrapper errorResponse) {
				handleError(errorResponse);
			}
		};
		request.setCssoId(applicationState.getCssoId());
		request.setDeviceId(applicationState.getDeviceId());
		
		passwordActivityHelper.setCursorVisible(false);
		inProgressIndicator.showInProgressIndicator();
		applicationState.getRestRequestProcessor().processRequest(request);
	}

	private void createArcotAccount(String encryptedToken) {
		try {	
			getApplicationState().setIsPassword(true);
			getApplicationState().getSecurityProvider().createAccount(encryptedToken);
			generateOTP();
			
		} catch (SecurityException e) {
			handleError(getErrorBuilder().buildErrorResponse(InAppError.CreateAccount));
		}
	}
	
	private void generateOTP() {
		securityProvider.generateOTP(passwordActivityHelper.getPassword(), this);
	}

	@Override
	public void otpGenerated(String otp) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Generated OTP");
		}

		if (otp == null) {
			handleError(getErrorBuilder().buildErrorResponse(InAppError.GenerateOTP));
		} else {
			startAuthenticateOTPRequest(otp);
		}
	}

	private void startAuthenticateOTPRequest(String otp) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Authenticate OTP Request");
		}

		AuthenticateOTPRequest request = new PasswordAuthenticateOTPRequest();

		request.setTbId(applicationState.getTbId());
		request.setDeviceId(applicationState.getDeviceId());
		request.setOtp(otp);

		requestProcessor.processRequest(request);
	}

	private class PasswordAuthenticateOTPRequest extends AuthenticateOTPRequest {

		@Override
		public void onResponse(AuthenticateOTPResponse response) {
			if (!response.getAuthenticated()) {
				handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.HttpStatus200));
				return;
			}
			securityProvider.lastOTPWasSuccessful();
			hideKeyboard();
			onDone();
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
			handleError(errorResponseWrapper);
		}
	}
}
