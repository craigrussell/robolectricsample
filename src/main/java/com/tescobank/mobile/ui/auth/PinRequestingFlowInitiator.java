package com.tescobank.mobile.ui.auth;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.authentication.GetPinChallengeRequest;
import com.tescobank.mobile.api.authentication.GetPinChallengeResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.flow.Flow;
import com.tescobank.mobile.flow.FlowInitiator;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.TescoActivity;

/**
 * Helper used to initiate a change password
 */
public class PinRequestingFlowInitiator implements FlowInitiator {
	
	private static final String TAG = PinRequestingFlowInitiator.class.getSimpleName();

	/** The activity */
	private TescoActivity activity;

	private Flow flow;
	
	/** The in-progress indicator */
	private InProgressIndicator inProgressIndicator;
	
	/** The application state */
	private ApplicationState applicationState;
		
	/**
	 * Constructor
	 * 
	 * @param activity the activity
	 * @param applicationState the application state
	 */
	public PinRequestingFlowInitiator(TescoActivity activity, Flow flow) {
		this.activity = activity;
		this.flow = flow;
		this.applicationState = activity.getApplicationState();
	}
	
	@Override
	public void execute() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Get PIN Challenge Request");
		}

		startInProgressIndicator();
		
		GetPinChallengeRequest request = new GetPinChallengeRequest() {
			@Override
			public void onResponse(GetPinChallengeResponse response) {
				hideInProgressIndicator();
				startFlow(response);
			}

			@Override
			public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
				hideInProgressIndicator();
				activity.handleErrorResponse(errorResponseWrapper);
			}
		};
		request.setCssoId(applicationState.getCssoId());
		applicationState.getRestRequestProcessor().processRequest(request, activity.getClass());
	}
	
	private void startInProgressIndicator() {
		if(inProgressIndicator != null) {
			inProgressIndicator.showInProgressIndicator();
		}
	}
	
	private void hideInProgressIndicator() {
		if(inProgressIndicator != null) {
			inProgressIndicator.hideInProgressIndicator();
		}
	}

	public void setInProgressIndicator(InProgressIndicator inProgressIndicator) {
		this.inProgressIndicator = inProgressIndicator;
	}
	
	/**
	 * Starts the flow
	 * @param response the get PIN challenge response
	 */
	private void startFlow(GetPinChallengeResponse response) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Flow");
		}
		flow.getDataBundle().putSerializable(GetPinChallengeResponse.class.getName(), response);
		flow.begin(activity);
	}
}
