package com.tescobank.mobile.ui.auth;

import com.tescobank.mobile.R;
import com.tescobank.mobile.analytics.AnalyticsEvent;
import com.tescobank.mobile.flow.PolicyAction;
import com.tescobank.mobile.flow.PolicyActionListener;
import com.tescobank.mobile.flow.TescoPolicyAction;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.TescoActivity;

public class CreatePasscodeCompletionAction extends TescoPolicyAction implements PolicyActionListener{

	private static final long serialVersionUID = 2047888497099357503L;
	private AnalyticsEvent analyticEvent;
	private transient TescoActivity activity;
	private transient InProgressIndicator inProgressIndicator;


	public CreatePasscodeCompletionAction(AnalyticsEvent analyticEvent) {
		this.analyticEvent = analyticEvent;
	}

	@Override
	public void execute(TescoActivity activity) {
		this.activity = activity;
		this.inProgressIndicator = new InProgressIndicator(activity, false);
		activity.getApplicationState().getAnalyticsTracker().trackEvent(analyticEvent);
		createCarouselLoaderAction();
	}

	
	private void createCarouselLoaderAction() {
		inProgressIndicator.showInProgressIndicator();
		ProductDataLoaderAction carouselLoaderAction = new ProductDataLoaderAction(false, false, R.string.auth_create_passcode_success_message);
		carouselLoaderAction.setListener(this);
		carouselLoaderAction.execute(activity);
	}

	@Override
	public void onActionComplete(PolicyAction action) {
		onActionCompleted();
	}
}
