package com.tescobank.mobile.ui.auth;

import static com.tescobank.mobile.ui.auth.PAMActivity.PAM_IMAGE;
import static com.tescobank.mobile.ui.auth.PAMActivity.PAM_PHRASE;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.credential.RetrievePAMRequest;
import com.tescobank.mobile.api.credential.RetrievePAMResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.api.user.GetUserIDsRequest;
import com.tescobank.mobile.api.user.GetUserIDsResponse;
import com.tescobank.mobile.download.DownloadHelper;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.InvalidValueIndicator;
import com.tescobank.mobile.ui.TescoActivity;

public class UsernameActivity extends TescoActivity {

	private static final String TAG = UsernameActivity.class.getSimpleName();

	private InvalidValueIndicator invalidValueIndicator;
	private InProgressIndicator inProgressIndicator;
	private EditText usernameField;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auth_registration_username);
		invalidValueIndicator = new InvalidValueIndicator(this);
		inProgressIndicator = new InProgressIndicator(this, true);
		usernameField = (EditText) findViewById(R.id.inputField);
		usernameField.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_NEXT) {
					onNextPressed();
				}
				return false;
			}
		});

	}

	@Override
	protected void onStart() {
		trackAnalytic();
		super.onStart();
	}

	@Override
	protected void onResume() {
		usernameField.setCursorVisible(true);
		super.onResume();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if(hasFocus) {
			showKeyboard();
		}
		super.onWindowFocusChanged(hasFocus);
	}
	
	private void trackAnalytic() {
		getApplicationState().getAnalyticsTracker().trackEvent(getFlow().getDisplayAnalytic());
	}

	@Override
	protected void onRetryPressed() {

		// clear the entry field text
		usernameField.setText(null);
		// set the focus
		usernameField.requestFocus();

		// Re-display the soft keyboard
		showKeyboard();

	}

	@Override
	protected void onRetryForPassiveError() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Commencing Retry for Passive Error");
		}

		onRetryPressed();

		invalidValueIndicator.indicateMissingValue(usernameField);
	}

	public void onContinuePressed(View view) {
		onNextPressed();
	}

	private void onNextPressed() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Next");
		}

		// Check that we have a password
		if (usernameField.getText().toString().trim().isEmpty()) {

			handleErrorResponse(getErrorBuilder().buildErrorResponse(
					InAppError.BlankUsername));
			return;
		}

		usernameField.setCursorVisible(false);
		inProgressIndicator.showInProgressIndicator();

		GetUserIDsRequest request = new GetUserIDsRequest() {
			@Override
			public void onErrorResponse(
					ErrorResponseWrapper errorResponseWrapper) {
				handleError(errorResponseWrapper);
			}

			@Override
			public void onResponse(GetUserIDsResponse response) {
				getApplicationState().setCssoId(response.getCssoId());
				getApplicationState().setTbId(response.getTbId());

				startPAMRequest();
			}

		};
		request.setUID(usernameField.getEditableText().toString().trim());
		getApplicationState().getRestRequestProcessor().processRequest(request);
	}

	private void handleError(ErrorResponseWrapper errorResponseWrapper) {
		inProgressIndicator.hideInProgressIndicator();

		handleErrorResponse(errorResponseWrapper);
	}

	private void startPAMRequest() {
		RetrievePAMRequest request = new RetrievePAMRequest() {

			@Override
			public void onResponse(RetrievePAMResponse response) {
				startPAMImageDownload(response);
			}

			@Override
			public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
				handleError(errorResponseWrapper);
			}
		};
		request.setTbID(getApplicationState().getTbId());
		getApplicationState().getRestRequestProcessor().processRequest(request);
	}

	private void startPAMImageDownload(final RetrievePAMResponse response) {

		DownloadHelper<Bitmap> helper = new DownloadHelper<Bitmap>(this, PAMActivity.IMAGE_MEDIA_TYPE) {

			@Override
			public void onResponse(Bitmap result) {
				onDone(response, result);
			}

			@Override
			public void onErrorResponse(
				ErrorResponseWrapper errorResponseWrapper) {
				handleError(errorResponseWrapper);
			}

			@Override
			public Bitmap convertInputStreamToType(InputStream inputStream) {
				return BitmapFactory.decodeStream(inputStream);
			}

		};

		helper.execute(response.getPamUrl());
	}

	private void onDone(RetrievePAMResponse response, Bitmap bitmap) {
		getFlow().getDataBundle().putSerializable(PAM_PHRASE, response.getPamPhrase());
		getFlow().getDataBundle().putParcelable(PAM_IMAGE, bitmap);
		getFlow().onPolicyComplete(this);
	}
}
