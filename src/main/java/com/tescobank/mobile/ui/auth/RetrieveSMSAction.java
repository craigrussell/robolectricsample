package com.tescobank.mobile.ui.auth;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.authentication.GetSMSChallengeRequest;
import com.tescobank.mobile.api.authentication.GetSMSChallengeResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.flow.TescoPolicyAction;
import com.tescobank.mobile.ui.TescoActivity;

public class RetrieveSMSAction extends TescoPolicyAction {

	private static final long serialVersionUID = -3789323813896696843L;
	private static final String TAG = RetrieveSMSAction.class.getName();
	
	private transient TescoActivity activity;
	
	@Override
	public void execute(TescoActivity activity) {
		this.activity = activity;
		startGetSMSChallengeRequest();
	}
	
	public void startGetSMSChallengeRequest() {
		ApplicationState application = activity.getApplicationState(); 
		SMSThrottler smsThrottler = new SMSThrottler(application.getSettings().getConfiguration(), application.getPersister());

		// Adds to the count to the shared preferences as well as settings the timestamp
		smsThrottler.isThrottled(true);

		GetSMSChallengeRequest request = new PinDigitsGetSMSChallengeRequest();
		request.setTbID(application.getTbId());
		request.setCssoID(application.getCssoId());
		application.getRestRequestProcessor().processRequest(request);
	}

	private class PinDigitsGetSMSChallengeRequest extends GetSMSChallengeRequest {
		@Override
		public void onResponse(GetSMSChallengeResponse response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Get SMS Challenge Request Succeeded");
			}

			if (!response.getSmsIssued()) {
				ErrorResponseWrapperBuilder builder = new ErrorResponseWrapperBuilder(activity);
				builder.buildErrorResponse(ErrorResponseWrapperBuilder.InAppError.HttpStatus200);
			} else {
				activity.getFlow().getDataBundle().putSerializable(GetSMSChallengeResponse.class.getName(), response);
				onActionCompleted();
			}
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Get SMS Challenge Request Failed: " + errorResponseWrapper.getErrorResponse().getErrorCode());
			}
			activity.handleErrorResponse(errorResponseWrapper);
		}
	}
}
