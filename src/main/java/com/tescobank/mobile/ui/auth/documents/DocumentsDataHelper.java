package com.tescobank.mobile.ui.auth.documents;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tescobank.mobile.api.content.RetrieveContentResponse.Document;

/**
 * Documents data helper
 */
public class DocumentsDataHelper implements Serializable {
	
	private static final long serialVersionUID = -1291320526384158011L;

	/** Documents to acknowledge, e.g [D1, D4] */
	private List<Document> documentsToAcknowledge;
	
	/** Mapping from document to acknowledge to next document to acknowledge, e.g. D1 => D4 */
	private Map<Document, Document> nextDocumentsToAcknowledge;
	
	/**
	 * Constructor
	 * 
	 * @param allDocuments all documents, e.g. [D1, D1, D3, D4]
	 */
	public DocumentsDataHelper(List<Document> allDocuments) {
		super();
		
		documentsToAcknowledge = deriveDocumentsToAcknowledge(allDocuments);
		nextDocumentsToAcknowledge = deriveNextDocumentsToAcknowledge(documentsToAcknowledge);
	}
	
	/**
	 * @param documentToAcknowledge a document to acknowledge, e.g. D1
	 * 
	 * @return the next document to acknowledge, e.g. D4
	 */
	public Document getNextDocumentToAcknowledge(Document documentToAcknowledge) {
		
		// If no document is specified, return the first document to acknowledge
		if (documentToAcknowledge == null) {
			return documentsToAcknowledge.isEmpty() ? null : documentsToAcknowledge.get(0);
		}
		
		// Return the next document to acknowledge
		return nextDocumentsToAcknowledge.get(documentToAcknowledge);
	}
	
	/**
	 * @param allDocuments allDocuments all documents, e.g. [D1, D1, D3, D4]
	 * 
	 * @return documents to acknowledge, e.g [D1, D4]
	 */
	private List<Document> deriveDocumentsToAcknowledge(List<Document> allDocuments) {
		
		List<Document> toAcknowledge = new ArrayList<Document>();
		
		// Check that we have documents
		if (isDocumentsEmpty(allDocuments)) {
			return toAcknowledge;
		}
		
		// Add each document to be acknowledged
		for (Document document : allDocuments) {
			if (isAcknowldgementRequired(document)) {
				toAcknowledge.add(document);
			}
		}
		return toAcknowledge;
	}
	
	private boolean isDocumentsEmpty(List<Document> documents) {
		return (documents == null) || documents.isEmpty();
	}
	
	private boolean isAcknowldgementRequired(Document document) {
		return (document != null) && Boolean.TRUE.equals(document.isMandatory()) && !document.isAcknowledged();
	}
	
	/**
	 * @param documentsToAcknowledge documents to acknowledge, e.g [D1, D4]
	 * 
	 * @return mapping from document to acknowledge to next document to acknowledge, e.g. D1 => D4
	 */
	private Map<Document, Document> deriveNextDocumentsToAcknowledge(List<Document> documentsToAcknowledge) {
		
		Map<Document, Document> mapping = new HashMap<Document, Document>();
		
		int documentCount = documentsToAcknowledge.size();
		for (int i = 0; i < (documentCount - 1); i++) {
			
			Document currentDocument = documentsToAcknowledge.get(i);
			Document nextDocument = documentsToAcknowledge.get(i + 1);
			
			mapping.put(currentDocument, nextDocument);
		}
		
		return mapping;
	}
}
