package com.tescobank.mobile.ui.auth.documents;

import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createLoginAnimationPolicyAction;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;

import com.tescobank.mobile.api.content.RetrieveContentResponse;
import com.tescobank.mobile.api.content.RetrieveContentResponse.Document;
import com.tescobank.mobile.flow.Policy;
import com.tescobank.mobile.flow.TescoPolicy;
import com.tescobank.mobile.ui.TescoActivity;

public class ViewDocumentsActivity extends TescoActivity {

	private List<Document> documents;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		documents = getAllDocuments();
		addDocumentPoliciesToFlow();
		getFlow().onPolicyComplete(this);
	}

	private void addDocumentPoliciesToFlow() {
		List<Policy> documentPolicies = new ArrayList<Policy>();
		for (int i = 0; i < documents.size(); i++) {
			documentPolicies.add(new TescoPolicy.Builder(ViewDocumentActivity.class)
							.setAnimation(createLoginAnimationPolicyAction())
			                .setActionBarConfig(getFlow().getActionBarConfig())
			                .setCancellation(getFlow().getCurrentCancellationAction())
			                .create());
		}
		getFlow().addPolices(documentPolicies);
	}

	@SuppressWarnings("unchecked")
	private List<Document> getAllDocuments() {
		return (List<Document>) getFlow().getDataBundle().getSerializable(RetrieveContentResponse.class.getName());
	}
}
