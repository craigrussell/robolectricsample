package com.tescobank.mobile.ui.auth;

import java.io.Serializable;

import android.os.Bundle;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.credential.SetCredentialRequest;
import com.tescobank.mobile.api.credential.SetCredentialRequest.Credential;
import com.tescobank.mobile.api.credential.SetCredentialResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.application.TescoDeviceMetrics;
import com.tescobank.mobile.security.SecurityException;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.InvalidValueIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoModal;
import com.tescobank.mobile.ui.auth.passcode.PasscodeActivityHelper;
import com.tescobank.mobile.ui.auth.passcode.PasscodeAnimator;
import com.tescobank.mobile.ui.auth.passcode.PasscodeDataHelper;
import com.tescobank.mobile.ui.auth.passcode.PasscodeSubmitter;
import com.tescobank.mobile.ui.errors.ErrorHandler;

/**
 * Activity for entering and confirming a new passcode when changing passcode
 */
public class CreatePasscodeActivity extends TescoActivity implements PasscodeSubmitter, ErrorHandler {
	private static final String BUNDLE_KEY_RESET_PASSCODE_PREVIOUS_ENTERED_VALUE = "resetPasscodePreviousEnteredValue";	
	private static final String BUNDLE_KEY_RESET_PASSCODE_MESSAGE_TEXT = "passcodeMessageText";	
	private static final String BUNDLE_KEY_HELP_DIALOG_SHOWING = "helpDialogShowing";
	public static final String BUNDLE_KEY_ENTER_PROMPT = "BUNDLE_KEY_ENTER_PROMPT";
	public static final String BUNDLE_KEY_REENTER_PROMPT = "BUNDLE_KEY_REENTER_PROMPT";
	public static final String BUNDLE_KEY_MODAL_HEADER = "BUNDLE_KEY_MODAL_HEADER";
	public static final String BUNDLE_KEY_MODAL_MESSAGE = "BUNDLE_KEY_MODAL_MESSAGE";


	private ApplicationState applicationState;
	private String passcode;
	private String previousEnteredPasscode;
	private PasscodeActivityHelper passcodeActivityHelper;
	private InProgressIndicator inProgressIndicator;
	private TextView message;

	private PasscodeAnimator passcodeAnimator;
	private TescoModal modal;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auth_passcode);
		configureTitle();
		configureInitialMessagePrompt();
		applicationState = getApplicationState();
		passcode = getFlow().getDataBundle().getString(PASSWORD_EXTRA);

		int enterPromptMessageId = getFlow().getDataBundle().getInt(BUNDLE_KEY_ENTER_PROMPT);
		int reenterPromptMessageId = getFlow().getDataBundle().getInt(BUNDLE_KEY_REENTER_PROMPT);
		InvalidValueIndicator invalidValueIndicator = new InvalidValueIndicator(this);
		inProgressIndicator = new InProgressIndicator(this, false);
		passcodeActivityHelper = new PasscodeActivityHelper(this, this, invalidValueIndicator);
		passcodeActivityHelper.requestFocus();
		passcodeAnimator = new PasscodeAnimator(this, passcodeActivityHelper, enterPromptMessageId, reenterPromptMessageId);
		
		restoreSavedData(savedInstanceState);
	}

	private void configureInitialMessagePrompt() {
		message = TextView.class.cast(findViewById(R.id.passcode_message));
		message.setText(getFlow().getDataBundle().getInt(BUNDLE_KEY_ENTER_PROMPT));
	}

	private void configureTitle() {
		TextView title = TextView.class.cast(findViewById(R.id.passcode_title));
		if(title != null) {
			title.setText(getFlow().getTitleId());
		}
	}
	
	@Override
	protected void onDestroy() {
		if(null != modal) {
			modal.dismiss();
		}
		super.onDestroy();
	}
	
	private void restoreSavedData(Bundle savedInstanceState) {
		if (savedInstanceState == null) {
			return;
		}
			
		Serializable resetPasscodePreviousEnteredValue = savedInstanceState.getSerializable(BUNDLE_KEY_RESET_PASSCODE_PREVIOUS_ENTERED_VALUE);
		if (resetPasscodePreviousEnteredValue != null) {
			previousEnteredPasscode = (String) resetPasscodePreviousEnteredValue;
		}		
		
		Serializable passcodeMessageText = savedInstanceState.getSerializable(BUNDLE_KEY_RESET_PASSCODE_MESSAGE_TEXT);
		if (passcodeMessageText != null) {
			message.setText(passcodeMessageText.toString());
		}
		
		if(savedInstanceState.getBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING)) {
			showHelpDialog();
		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(BUNDLE_KEY_RESET_PASSCODE_PREVIOUS_ENTERED_VALUE, previousEnteredPasscode);
		outState.putSerializable(BUNDLE_KEY_RESET_PASSCODE_MESSAGE_TEXT, (Serializable) message.getText());
		if(null != modal) {
			outState.putBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING, modal.isShowing());
		}	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.passcode, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_item_information:
			showHelpDialog();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void showHelpDialog() {
		modal = new TescoModal(this);
		modal.setHeadingText(getString(getFlow().getDataBundle().getInt(BUNDLE_KEY_MODAL_HEADER)));
		modal.setMessageText(getString(getFlow().getDataBundle().getInt(BUNDLE_KEY_MODAL_MESSAGE)));
		modal.show();
	}

	@Override
	protected void onStart() {
		trackAnalytic();
		super.onStart();
	}

	private void trackAnalytic() {
		applicationState.getAnalyticsTracker().trackEvent(getFlow().getDisplayAnalytic());	
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (hasFocus) {
			showKeyboard();
		}
		super.onWindowFocusChanged(hasFocus);
	}

	@Override
	public void onBackPressed() {
		if (previousEnteredPasscode == null) {
			super.onBackPressed();
		} else {
			previousEnteredPasscode = null;
			updateUIForFirstOrSecondEntry(true);
		}
	}

	@Override
	protected void onRetryPressed() {
		if (PasscodeSetCredentialRequest.class.equals(getApplicationState().getRestRequestProcessor().getLastRequestTypeForRequesterType(getClass()))) {
			passcodeActivityHelper.handleRetry();
			passcodeActivityHelper.getPasscodeDataHelper().setInputFieldsEnabled(true);
			showKeyboard();
		} else {
			getFlow().onPolicyComplete(this);
		}
		
	}

	@Override
	protected void onRetryForPassiveError() {
		passcodeActivityHelper.handleRetryForPassiveError();
	}

	@Override
	public void handleError(ErrorResponseWrapper errorResponseWrapper) {
		handleErrorResponse(errorResponseWrapper);
	}

	@Override
	public void submitPasscode() {
		if (!getPasscodeDataHelper().allRequiredValuesProvided()) {

			// No passcode
			handleError(getErrorBuilder().buildErrorResponse(InAppError.IncompletePasscode));

		} else if (previousEnteredPasscode == null) {

			// First passcode: validate client-side
			validatePasscode();

		} else if (!getPasscodeDataHelper().passcodeMatches(previousEnteredPasscode)) {

			// Second passcode: passcode mismatch
			handleError(getErrorBuilder().buildErrorResponse(InAppError.PasscodeMismatch));
			previousEnteredPasscode = null;
			updateUIForFirstOrSecondEntry(true);

		} else {

			// Valid client-side: validate server-side
			startSetCredentialRequest();
		}
	}

	private void validatePasscode() {
		if (!getPasscodeDataHelper().isValid()) {
			// Invalid
			handleError(getErrorBuilder().buildErrorResponse(InAppError.PasscodeValidation));

		} else {
			// Valid client-side: prompt to re-enter
			previousEnteredPasscode = getPasscodeDataHelper().getPasscode();
			updateUIForFirstOrSecondEntry(false);
		}
	}

	/**
	 * Starts the set credentials request
	 */
	private void startSetCredentialRequest() {
		TescoDeviceMetrics deviceMetrics = new TescoDeviceMetrics(applicationState.getDeviceId(), applicationState.getDeviceInfo());
		String credentialValue = getPasscodeDataHelper().getPasscode();
		SetCredentialRequest request = new PasscodeSetCredentialRequest();
		request.setCssoID(applicationState.getCssoId());
		request.setCredential(Credential.PASSCODE);
		request.setCredentialValue(credentialValue);
		request.setDeviceMetrics(deviceMetrics);

		inProgressIndicator.showInProgressIndicator();
		applicationState.getRestRequestProcessor().processRequest(request, getClass());
	}

	/**
	 * Changes the PIN
	 */
	private void changePin() {
		try {
			getApplicationState().getSecurityProvider().changePin(passcode, previousEnteredPasscode);
			getApplicationState().setIsPassword(false);
			inProgressIndicator.hideInProgressIndicator();
			hideKeyboard();
			getFlow().onPolicyComplete(this);
		} catch (SecurityException ex) {
			handleError(getErrorBuilder().buildErrorResponse(InAppError.ChangePin));
		}
	}

	/**
	 * Updates the UI based on whether the user is enter the first or second passcode
	 */
	private void updateUIForFirstOrSecondEntry(boolean startAgain) {
		if (startAgain) {
			passcodeAnimator.startAgain();
		} else {
			passcodeAnimator.showConfirmation();
		}

		inProgressIndicator.hideInProgressIndicator();
	}

	/**
	 * @return the passcode activity helper's data helper
	 */
	private PasscodeDataHelper getPasscodeDataHelper() {
		return passcodeActivityHelper.getPasscodeDataHelper();
	}

	public void onErrorMoveBackToInitialPasscodeEntryField() {
		inProgressIndicator.hideInProgressIndicator();
		previousEnteredPasscode = null;
		updateUIForFirstOrSecondEntry(true);
	}

	private class PasscodeSetCredentialRequest extends SetCredentialRequest {

		@Override
		public void onResponse(SetCredentialResponse response) {
			if (!response.getSuccessful()) {
				onErrorMoveBackToInitialPasscodeEntryField();
				handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.PasscodeValidation));
			} else {
				changePin();
			}
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponse) {
			onErrorMoveBackToInitialPasscodeEntryField();
			handleError(errorResponse);
		}
	}

}
