package com.tescobank.mobile.ui.auth;

import static com.tescobank.mobile.api.content.RetrieveContentRequest.Journey.TERMS_LOGIN;
import static com.tescobank.mobile.ui.auth.PAMActivity.PAM_IMAGE;
import static com.tescobank.mobile.ui.auth.PAMActivity.PAM_PHRASE;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.authentication.GetPinChallengeRequest;
import com.tescobank.mobile.api.authentication.GetPinChallengeResponse;
import com.tescobank.mobile.api.credential.RetrievePAMRequest;
import com.tescobank.mobile.api.credential.RetrievePAMResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.download.DownloadHelper;
import com.tescobank.mobile.flow.Flow;
import com.tescobank.mobile.persist.Persister;
import com.tescobank.mobile.rest.processor.BatchRestRequestCompletionListener;
import com.tescobank.mobile.rest.processor.BatchRestRequestProcessor;

/**
 * Splash activity subsequent login API flow
 */
public class SubsequentLoginFlowInitiator extends LoginFlowInitiator implements BatchRestRequestCompletionListener {

	private static final String TAG = SubsequentLoginFlowInitiator.class.getSimpleName();

	private SplashActivity splashActivity;
	private ApplicationState applicationState;
	private Flow activityFlow;
	private RetrievePAMResponse pamResponse;
	private BatchRestRequestProcessor requestOrganiser;
	private Persister persister;
	
	public SubsequentLoginFlowInitiator(SplashActivity splashActivity, Flow activityFlow) {
		super(splashActivity, activityFlow, TERMS_LOGIN);
		this.splashActivity = splashActivity;
		this.applicationState = splashActivity.getApplicationState();
		this.activityFlow = activityFlow;
		persister = applicationState.getPersister();
		this.requestOrganiser = new BatchRestRequestProcessor(applicationState.getRestRequestProcessor(),this);
	}
	
	@Override
	protected void startApiRequests() {
		executeConfigAndFeatures();
	}

	private void executeConfigAndFeatures() {
		SettingsAndFeaturesRequestExecutor settingsAndFeaturesRequestExecutor = new SettingsAndFeaturesRequestExecutor();
		settingsAndFeaturesRequestExecutor.execute();
	}
	
	@Override
	public void onBatchRequestsSuceeded() {
		startPAMImageDownload();
	}
	
	@Override
	public void onBatchRequestsFailed(ErrorResponseWrapper errorResponse) {
		
		if(!errorResponse.getErrorResponse().isAuthFailure()) {
			super.onBatchRequestsFailed(errorResponse);
			return;
		}
		
		resetBalancePeekSharedPreferences();	
	}
	
	private void executeCheckDevice(boolean isBalancePeekEnabled) {	
		if(!isBalancePeekEnabled) {
			startNonBalancePeekRequests();
			return;
		}
		
		startBalancePeekRequests();		
	}

	private void startBalancePeekRequests() {
		requestOrganiser.addRequest(new SplashCheckDeviceNonInteractiveRequest(),false);
		requestOrganiser.addRequest(new SplashGetProductSummaryRequest(), false);
		startCommonRequests();
		requestOrganiser.execute();
	}

	private void startCommonRequests() {	
		requestOrganiser.addRequest(new SplashRetrieveContentRequest(),true);
		requestOrganiser.addRequest(new SplashRetrievePAMRequest(),true);
		if(applicationState.isPassword()) {
			requestOrganiser.addRequest(new SplashGetPinChallengeRequest(), false);
		}
	}	

	private void resetBalancePeekSharedPreferences() {
		applicationState.getPersister().removeBalancePeekProductIdentifiers();
		applicationState.getPersister().persistIsBalancePeekEnabled(false);
		requestOrganiser.resetBatchRequestProcessor();
		startNonBalancePeekRequests();
	}

	private void startNonBalancePeekRequests() {
		requestOrganiser.addRequest(new SplashCheckDeviceRequest(),true);
		startCommonRequests();
		requestOrganiser.execute();
	}

	private class SplashGetPinChallengeRequest extends GetPinChallengeRequest {
		
		public SplashGetPinChallengeRequest() {
			setCssoId(applicationState.getCssoId());
		}
		
		@Override
		public void onResponse(GetPinChallengeResponse response) {
			activityFlow.getDataBundle().putSerializable(GetPinChallengeResponse.class.getName(), response);
			super.onResponse(response);
		}
	}
	
	private class SplashRetrievePAMRequest extends RetrievePAMRequest {
		
		public SplashRetrievePAMRequest() {
			setTbID(applicationState.getTbId());
		}
		
		@Override
		public void onResponse(RetrievePAMResponse response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "PAM Request Succeeded");
			}
			pamResponse = response;
			super.onResponse(response);
		}
	}
	
	private boolean isBalancePeekEnabled() {	
		if(persister.isBalancePeekEnabled()) {
			return true;
		}	
		return false;
	}
	
	private boolean areBalancePeekProductsAvailable() {
		if(persister.getBalancePeekProductIdentifiers() != null && !persister.getBalancePeekProductIdentifiers().isEmpty()) {
			return true;
		}
		return false;
	}
	
	private boolean isNonInteractiveProductsEnabled() {
		if(applicationState.getFeatures().isNonInteractiveProductsEnabled()) {
			return true;
		}
		return false;
	}
	
	private class SettingsAndFeaturesRequestExecutor implements BatchRestRequestCompletionListener {
		private BatchRestRequestProcessor requestOrganiser;

		public SettingsAndFeaturesRequestExecutor() {
			this.requestOrganiser = new BatchRestRequestProcessor(applicationState.getRestRequestProcessor(),this);
		}
		
		private void execute() {
			requestOrganiser.addRequest(new SplashFeaturesRequest(),true);		
			requestOrganiser.addRequest(new SplashSettingsRequest(),true);	
			requestOrganiser.execute();	
		}

		@Override
		public void onBatchRequestsSuceeded() {
			executeCheckDevice(isNonInteractiveProductsEnabled() && isBalancePeekEnabled() && areBalancePeekProductsAvailable());
		}

		@Override
		public void onBatchRequestsFailed(ErrorResponseWrapper errorResponse) {
			SubsequentLoginFlowInitiator.super.onBatchRequestsFailed(errorResponse);
		}
	}
	
	private void startPAMImageDownload() {

		DownloadHelper<Bitmap> downloadHelper = new DownloadHelper<Bitmap>(splashActivity, PAMActivity.IMAGE_MEDIA_TYPE) {

			@Override
			public void onResponse(Bitmap result) {
				activityFlow.getDataBundle().putString(PAM_PHRASE, pamResponse.getPamPhrase());
				activityFlow.getDataBundle().putParcelable(PAM_IMAGE, result);
				SubsequentLoginFlowInitiator.super.onBatchRequestsSuceeded();
			}

			@Override
			public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
				onBatchRequestsFailed(errorResponseWrapper);
			}

			@Override
			public Bitmap convertInputStreamToType(InputStream inputStream) {
				return BitmapFactory.decodeStream(inputStream);
			}
		};
		downloadHelper.execute(pamResponse.getPamUrl());
	}
}
