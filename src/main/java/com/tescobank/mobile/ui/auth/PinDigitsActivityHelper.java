package com.tescobank.mobile.ui.auth;

import java.util.ArrayList;
import java.util.List;

import android.os.Build;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.TextView;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.authentication.GetPinChallengeRequest;
import com.tescobank.mobile.api.authentication.GetPinChallengeResponse;
import com.tescobank.mobile.api.authentication.VerifyChallengeResponseRequest;
import com.tescobank.mobile.api.authentication.VerifyChallengeResponseResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.InvalidValueIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoEditText;
import com.tescobank.mobile.ui.TescoEditTextDigitHandler;
import com.tescobank.mobile.ui.balancepeek.BalancePeekSlidingPanelTouchListener;
import com.tescobank.mobile.ui.errors.ErrorHandler;

/**
 * Helper for PIN digit-related activities
 */
public class PinDigitsActivityHelper {

	private static final String TAG = PinDigitsActivityHelper.class.getSimpleName();
	private TescoActivity activity;
	private PinDigitsSubmitter pinDigitsSubmitter;
	private ErrorHandler errorHandler;
	private InProgressIndicator inProgressIndicator;
	private InvalidValueIndicator invalidValueIndicator;
	private ApplicationState applicationState;
	private PinDigitsDataHelper pinDigitsDataHelper;
	private String challengeID;
	private boolean verifyTokenVersion = true;
	private OnTouchListener viewTouchListener;
	private TescoEditTextDigitHandler editTextDigitHandler;
	private BalancePeekSlidingPanelTouchListener balancePeekSlidingPanelTouchListener;
	private boolean isError;

	public PinDigitsActivityHelper(TescoActivity activity, final PinDigitsSubmitter pinDigitsSubmitter, ErrorHandler errorHandler, InProgressIndicator inProgressIndicator, InvalidValueIndicator invalidValueIndicator, ApplicationState applicationState, boolean verifyTokenVersion) {
		this(activity, pinDigitsSubmitter, errorHandler, inProgressIndicator, invalidValueIndicator, applicationState, verifyTokenVersion, null);
	}

	public PinDigitsActivityHelper(final TescoActivity activity, final PinDigitsSubmitter pinDigitsSubmitter, ErrorHandler errorHandler, InProgressIndicator inProgressIndicator, InvalidValueIndicator invalidValueIndicator, ApplicationState applicationState, boolean verifyTokenVersion,
			final BalancePeekSlidingPanelTouchListener balancePeekSlidingPanelTouchListener) {

		this.activity = activity;
		this.pinDigitsSubmitter = pinDigitsSubmitter;
		this.errorHandler = errorHandler;
		this.inProgressIndicator = inProgressIndicator;
		this.invalidValueIndicator = invalidValueIndicator;
		this.applicationState = applicationState;
		this.verifyTokenVersion = verifyTokenVersion;
		this.balancePeekSlidingPanelTouchListener = balancePeekSlidingPanelTouchListener;
		createEditTextDigitHandler();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

			viewTouchListener = new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {

						if (byPassBalancePeekTouchEventProcessing(v)) {
							activity.showKeyboard();
							return true;
						}

						if (balancePeekEnabledAndShowing()) {
							closePanels(balancePeekSlidingPanelTouchListener);
							return true;
						}

						editTextDigitHandler.allowAllTextForFirstAvailableEditTextField(pinDigitsDataHelper.getInputFieldsForRequiredValues().get(0));
						activity.showKeyboard();
						return true;

					}
					return false;
				}

				private void closePanels(final BalancePeekSlidingPanelTouchListener balancePeekTouchListener) {
					balancePeekTouchListener.closePanels(BalancePeekSlidingPanelTouchListener.ANIMATION_DURATION_DEFAULT);
				}

				private boolean balancePeekEnabledAndShowing() {
					if (balancePeekSlidingPanelTouchListener != null && balancePeekSlidingPanelTouchListener.panelsAreOpen()) {
						return true;
					}
					return false;
				}
			};			
		}
	}

	private void createEditTextDigitHandler() {
		editTextDigitHandler = new TescoEditTextDigitHandler() {
			@Override
			public void onInputsFilledIn() {
				pinDigitsSubmitter.submitPinDigits();
			}
		};
	}

	public void updatePinDigitsForResponse(GetPinChallengeResponse getPinChallengeResponse) {

		// Obtain details from the challenge
		challengeID = getPinChallengeResponse.getChallengeID();
		Integer[] pinDigits = getPinChallengeResponse.getPinDigits();

		// Identify input fields
		List<TescoEditText> inputFields = new ArrayList<TescoEditText>();
		inputFields.add((TescoEditText) activity.findViewById(R.id.inputField1));
		inputFields.add((TescoEditText) activity.findViewById(R.id.inputField2));
		inputFields.add((TescoEditText) activity.findViewById(R.id.inputField3));
		inputFields.add((TescoEditText) activity.findViewById(R.id.inputField4));
		inputFields.add((TescoEditText) activity.findViewById(R.id.inputField5));
		inputFields.add((TescoEditText) activity.findViewById(R.id.inputField6));

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			for (TescoEditText edit : inputFields) {
				edit.setTransformationMethod(PasswordTransformationMethod.getInstance());
			}
		}

		// Identify labels
		List<TextView> labels = new ArrayList<TextView>();
		labels.add((TextView) activity.findViewById(R.id.inputLabel1));
		labels.add((TextView) activity.findViewById(R.id.inputLabel2));
		labels.add((TextView) activity.findViewById(R.id.inputLabel3));
		labels.add((TextView) activity.findViewById(R.id.inputLabel4));
		labels.add((TextView) activity.findViewById(R.id.inputLabel5));
		labels.add((TextView) activity.findViewById(R.id.inputLabel6));

		// Create the helpers
		pinDigitsDataHelper = new PinDigitsDataHelper(pinDigits, inputFields, labels);

		// Configure inputs
		configureInputs();
	}

	/**
	 * @return the PIN digits data helper
	 */
	public PinDigitsDataHelper getPinDigitsDataHelper() {
		return pinDigitsDataHelper;
	}

	/**
	 * Handles retry
	 */
	public void handleRetry() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Retry");
		}

		inProgressIndicator.showInProgressIndicator();

		resetAndClearFields();

		GetPinChallengeRequest request = new PinGetPinChallengeRequest();
		request.setCssoId(applicationState.getCssoId());
		applicationState.getRestRequestProcessor().processRequest(request);
	}

	/**
	 * Handles retry for a passive error
	 */
	public void handleRetryForPassiveError() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Retry for Passive Error");
		}

		clearFields();

		List<TescoEditText> inputFields = pinDigitsDataHelper.getInputFieldsForRequiredValuesWithNoValue();
		if (inputFields.isEmpty()) {
			invalidValueIndicator.indicateMissingValue(pinDigitsDataHelper.getInputFieldsForRequiredValues());
		} else {
			invalidValueIndicator.indicateMissingValue(inputFields);
		}
	}

	private void resetAndClearFields() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Resetting Field");
		}

		// Reset all input fields
		List<TescoEditText> inputFieldsForAllValues = pinDigitsDataHelper.getInputFieldsForAllValues();
		for (EditText inputField : inputFieldsForAllValues) {

			inputField.setEnabled(false);
			inputField.setOnEditorActionListener(null);
			inputField.setOnTouchListener(null);

			TescoEditText tescoEditText = (TescoEditText) inputField;
			tescoEditText.setOnDeleteListener(null);
		}

		// Reset all labels
		List<TextView> labelsForAllValues = pinDigitsDataHelper.getLabelsForAllValues();
		for (TextView label : labelsForAllValues) {
			label.setVisibility(View.GONE);
		}

		clearFields();
	}

	public void clearFields() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Clearing Field");
		}

		if (pinDigitsDataHelper.allRequiredValuesProvided()) {

			// Clear values
			List<TescoEditText> inputFields = pinDigitsDataHelper.getInputFieldsForRequiredValues();
			for (EditText inputField : inputFields) {
				inputField.setText(null);
			}

			setFocusToFirstEntryField();
		}
	}

	private void setFocusToFirstEntryField() {
		// Give the first input field the focus
		EditText firstInputField = pinDigitsDataHelper.getFirstInputFieldForRequiredValue();
		if (firstInputField != null) {
			firstInputField.requestFocus();
		}
	}

	/**
	 * Starts a verify challenge response request
	 */
	public void startVerifyChallengeResponseRequest() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Verify Challenge Response Request");
		}

		((EditText) activity.findViewById(R.id.inputField1)).clearFocus();
		((EditText) activity.findViewById(R.id.inputField2)).clearFocus();
		((EditText) activity.findViewById(R.id.inputField3)).clearFocus();
		((EditText) activity.findViewById(R.id.inputField4)).clearFocus();
		((EditText) activity.findViewById(R.id.inputField5)).clearFocus();
		((EditText) activity.findViewById(R.id.inputField6)).clearFocus();

		VerifyChallengeResponseRequest request = new PinVerifyChallengeResponseRequest();
		request.setCssoId(applicationState.getCssoId());
		request.setChallengeId(challengeID);
		request.setPinDigits(pinDigitsDataHelper.getRequiredValues().toArray(new Integer[0]));
		request.setDeviceId(applicationState.getDeviceId());
		request.setVerifyTokenVersion(verifyTokenVersion);

		applicationState.getRestRequestProcessor().processRequest(request);
	}

	/**
	 * Configures the inputs
	 */
	private void configureInputs() {

		// Enable input fields for required value
		List<TescoEditText> inputFieldsForRequiredValues = pinDigitsDataHelper.getInputFieldsForRequiredValues();
		for (TescoEditText inputField : inputFieldsForRequiredValues) {
			inputField.setEnabled(true);
		}

		// Show labels for required values
		List<TextView> labelsForRequiredValues = pinDigitsDataHelper.getLabelsForRequiredValues();
		for (TextView label : labelsForRequiredValues) {
			label.setVisibility(View.VISIBLE);
		}

		attachTouchListenersToAllRequiredInputFields();
		clearAndCreateTescoEditTextDigitHandlers(inputFieldsForRequiredValues);
		consumeFirstInputFieldValuesForBalancePeek(inputFieldsForRequiredValues);
	}

	private void attachTouchListenersToAllRequiredInputFields() {
		for (TescoEditText editText : pinDigitsDataHelper.getInputFieldsForRequiredValues()) {
			editText.setOnTouchListener(viewTouchListener);
		}
	}

	private void clearAndCreateTescoEditTextDigitHandlers(List<TescoEditText> inputFieldsForRequiredValues) {

		if (editTextDigitHandler != null) {
			editTextDigitHandler.configure(inputFieldsForRequiredValues);
			editTextDigitHandler.requestFocus(inputFieldsForRequiredValues);
		}
	}

	private void consumeFirstInputFieldValuesForBalancePeek(List<TescoEditText> inputFieldsForRequiredValues) {
		if (balancePeekSlidingPanelTouchListener != null && !isError) {
			editTextDigitHandler.consumeAllTextForFirstAvailableEditTextField(inputFieldsForRequiredValues.get(0));
		}
	}

	/* check to see if the user has touched the first required entry field again after previously selecting it. If so return true and as a result the 
	 * touch event for the balance peek will be ignored*/
	private boolean byPassBalancePeekTouchEventProcessing(View v) {
		TescoEditText firstRequiredInputField = pinDigitsDataHelper.getInputFieldsForRequiredValues().get(0);
		TescoEditText secondRequiredInputField = pinDigitsDataHelper.getInputFieldsForRequiredValues().get(1);
		if ((firstRequiredInputField != null && firstRequiredInputField.getText().length() > 0 && TescoEditText.class.cast(v).getText().length() > 0)) {
			if (secondRequiredInputField.getText().toString().trim().isEmpty()) {
				TescoEditText previous = firstRequiredInputField;
				if (previous != null) {
					previous.requestFocus();
					return true;
				}
			}
			return true;
		}
		return false;
	}

	private class PinGetPinChallengeRequest extends GetPinChallengeRequest {

		@Override
		public void onResponse(GetPinChallengeResponse response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Get PIN Challenge Request Succeeded");
			}

			updatePinDigitsForResponse(response);
			inProgressIndicator.hideInProgressIndicator();
			activity.showKeyboard();
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Get PIN Challenge Request Failed: " + errorResponseWrapper.getErrorResponse().getErrorCode());
			}
			errorHandler.handleError(errorResponseWrapper);
		}
	}

	private class PinVerifyChallengeResponseRequest extends VerifyChallengeResponseRequest {

		@Override
		public void onResponse(VerifyChallengeResponseResponse response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Verify Challenge Response Request Succeeded");
			}

			if (null == response.getEncryptedToken() || response.getEncryptedToken().trim().isEmpty()) {
				errorHandler.handleError(new ErrorResponseWrapperBuilder(activity).buildErrorResponse(InAppError.HttpStatus200));
			} else {
				pinDigitsSubmitter.onVerifyChallengeResponseComplete(response.getEncryptedToken());
			}
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Verify Challenge Response Request Failed: " + errorResponseWrapper.getErrorResponse().getErrorCode());
			}
			isError = true;
			errorHandler.handleError(errorResponseWrapper);
		}
	}

}
