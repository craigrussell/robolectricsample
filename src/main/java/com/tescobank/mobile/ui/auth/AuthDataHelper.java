package com.tescobank.mobile.ui.auth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tescobank.mobile.ui.TescoEditText;

import android.view.View;
import android.widget.EditText;

/**
 * Base class for login data helpers; handles null / bad data by ignoring it
 */
public class AuthDataHelper {
	
	private static final int MIN_DIGIT = 0;
	private static final int MAX_DIGIT = 9;

	/** One-based indices of required values, e.g. [1, 3] */
	private Integer[] indicesOfRequiredValues;
	
	/** Input fields for all values, e.g. [I1, I2, I3, I4, I5, I6] */
	private List<TescoEditText> inputFieldsForAllValues;

	/** Input fields for required values, e.g. [I1, I3] */
	private List<TescoEditText> inputFieldsForRequiredValues;

	/** The first input field for required values, e.g. I1 */
	private TescoEditText firstInputFieldForRequiredValues;
	
	/** Mapping from input field for required value to previous input field for required value, e.g. I3 => I1 */
	private Map<TescoEditText, TescoEditText> previousInputFieldsForRequiredValues;

	/** Mapping from input field for required value to next input field for required value, e.g. I1 => I3 */
	private Map<TescoEditText, TescoEditText> nextInputFieldsForRequiredValues;

	/**
	 * Constructor
	 * 
	 * @param indicesOfRequiredValues one-based indices of required values, e.g. [1, 3]
	 * @param inputFieldsForAllValues input fields for all values, e.g. [I1, I2, I3, I4, I5, I6]
	 */
	public AuthDataHelper(Integer[] indicesOfRequiredValues, List<TescoEditText> inputFieldsForAllValues) {
		super();

		this.indicesOfRequiredValues = (indicesOfRequiredValues == null) ? null : Arrays.copyOf(indicesOfRequiredValues, indicesOfRequiredValues.length);
		this.inputFieldsForAllValues = inputFieldsForAllValues;
		this.inputFieldsForRequiredValues = getViewsForRequiredValues(inputFieldsForAllValues);
		this.firstInputFieldForRequiredValues = deriveFirstInputFieldForRequiredValue();
		this.previousInputFieldsForRequiredValues = derivePreviousInputFieldsForRequiredValues();
		this.nextInputFieldsForRequiredValues = deriveNextInputFieldsForRequiredValues();
	}
	
	/**
	 * @return input fields for all values, e.g. [I1, I2, I3, I4, I5, I6]
	 */
	public List<TescoEditText> getInputFieldsForAllValues() {
		return inputFieldsForAllValues;
	}

	/**
	 * @return input fields for required values, e.g. [I1, I3]
	 */
	public List<TescoEditText> getInputFieldsForRequiredValues() {
		return inputFieldsForRequiredValues;
	}

	/**
	 * @return the first input field for required values, e.g. I1
	 */
	public TescoEditText getFirstInputFieldForRequiredValue() {
		return firstInputFieldForRequiredValues;
	}

	/**
	 * @param inputFieldForRequiredValue an input field for required value, e.g. I3
	 * 
	 * @return the previous input field for required value, e.g. I1
	 */
	public TescoEditText getPreviousInputFieldForRequiredValue(EditText inputFieldForRequiredValue) {
		return previousInputFieldsForRequiredValues.get(inputFieldForRequiredValue);
	}
	
	/**
	 * @param inputFieldForRequiredValue an input field for required value, e.g. I1
	 * 
	 * @return the next input field for required value, e.g. I3
	 */
	public TescoEditText getNextInputFieldForRequiredValue(EditText inputFieldForRequiredValue) {
		return nextInputFieldsForRequiredValues.get(inputFieldForRequiredValue);
	}

	/**
	 * @return input fields for required values that have no value, e.g. [I1]
	 */
	public List<TescoEditText> getInputFieldsForRequiredValuesWithNoValue() {

		List<TescoEditText> inputFields = new ArrayList<TescoEditText>();
		
		for (TescoEditText inputField : inputFieldsForRequiredValues) {
			if (getDigitValue(inputField) == null) {
				inputFields.add(inputField);
			}
		}

		return inputFields;
	}

	/**
	 * @return true if all required values have been provided, e.g. [I1 has value, I3 has value]
	 */
	public boolean allRequiredValuesProvided() {
		return getInputFieldsForRequiredValuesWithNoValue().isEmpty();
	}
	
	/**
	 * @return values of input fields for required values, e.g. [9, 9]
	 */
	public List<Integer> getRequiredValues() {

		List<Integer> requiredValues = new ArrayList<Integer>();

		for (TescoEditText inputField : getInputFieldsForRequiredValues()) {
			Integer value = getDigitValue(inputField);
			if (value != null) {
				requiredValues.add(value);
			}
		}

		return requiredValues;
	}

	/**
	 * @param viewsForAllValues views for all values, e.g. [L1, L2, L3, L4, L5, L6]
	 * 
	 * @return views for required values, e.g. [L1, L3]
	 */
	protected final <T extends View> List<T> getViewsForRequiredValues(List<T> viewsForAllValues) {

		List<T> viewsForRequiredValues = new ArrayList<T>();

		// Check that we have indices
		if (isIndicesOfRequiredValuesEmpty()) {
			return viewsForRequiredValues;
		}

		// Check that we have views
		if (isViewsForAllValuesEmpty(viewsForAllValues)) {
			return viewsForRequiredValues;
		}

		// Add a view for each index
		addViewsForRequiredValues(viewsForAllValues, viewsForRequiredValues);

		return viewsForRequiredValues;
	}
	
	private boolean isIndicesOfRequiredValuesEmpty() {
		return (indicesOfRequiredValues == null) || (indicesOfRequiredValues.length == 0);
	}
	
	private <T extends View> boolean isViewsForAllValuesEmpty(List<T> viewsForAllValues) {
		return (viewsForAllValues == null) || viewsForAllValues.isEmpty();
	}
	
	private <T extends View> void addViewsForRequiredValues(List<T> viewsForAllValues, List<T> viewsForRequiredValues) {
		
		int viewCount = viewsForAllValues.size();
		for (Integer index : indicesOfRequiredValues) {

			// Check that we have a valid index
			if (isIndexValid(viewCount, index)) {
				continue;
			}

			// Check that we have a view at that index
			T view = viewsForAllValues.get(index - 1);
			if (view == null) {
				continue;
			}

			// Add the view
			viewsForRequiredValues.add(view);
		}
	}
	
	private boolean isIndexValid(int viewCount, Integer index) {
		return (index == null) || (index < 1) || (index > viewCount);
	}
	
	/**
	 * @param stringValue a string value
	 * 
	 * @return the corresponding digit value
	 */
	protected Integer getDigitValue(String stringValue) {

		// Check that we have a value
		if (isStringValieEmpty(stringValue)) {
			return null;
		}

		// Check that we have an integer value
		Integer integerValue = getIntegerValue(stringValue);
		if (integerValue == null) {
			return null;
		}

		// Check that we have a digit value
		if (!isDigit(integerValue)) {
			return null;
		}

		return integerValue;
	}
	
	private boolean isStringValieEmpty(String stringValue) {
		return (stringValue == null) || stringValue.trim().isEmpty();
	}
	
	private Integer getIntegerValue(String stringValue) {
		Integer integerValue;
		try {
			integerValue = Integer.valueOf(stringValue.trim());
		} catch (NumberFormatException e) {
			integerValue = null;
		}
		return integerValue;
	}
	
	private boolean isDigit(Integer integerValue) {
		return (integerValue >= MIN_DIGIT) && (integerValue <= MAX_DIGIT);
	}

	/**
	 * @return the first input field for required values, e.g. I1
	 */
	private TescoEditText deriveFirstInputFieldForRequiredValue() {
		return inputFieldsForRequiredValues.isEmpty() ? null : inputFieldsForRequiredValues.get(0);
	}

	/**
	 * @return mapping from input field for required value to previous input field for required value, e.g. I3 => I1
	 */
	private Map<TescoEditText, TescoEditText> derivePreviousInputFieldsForRequiredValues() {

		Map<TescoEditText, TescoEditText> mapping = new HashMap<TescoEditText, TescoEditText>();

		int fieldCount = inputFieldsForRequiredValues.size();
		for (int i = 1; i < fieldCount; i++) {

			TescoEditText currentField = inputFieldsForRequiredValues.get(i);
			TescoEditText previousField = inputFieldsForRequiredValues.get(i - 1);

			mapping.put(currentField, previousField);
		}

		return mapping;
	}
	
	/**
	 * @return mapping from input field for required value to next input field for required value, e.g. I1 => I3
	 */
	private Map<TescoEditText, TescoEditText> deriveNextInputFieldsForRequiredValues() {

		Map<TescoEditText, TescoEditText> mapping = new HashMap<TescoEditText, TescoEditText>();

		int fieldCount = inputFieldsForRequiredValues.size();
		for (int i = 0; i < (fieldCount - 1); i++) {

			TescoEditText currentField = inputFieldsForRequiredValues.get(i);
			TescoEditText nextField = inputFieldsForRequiredValues.get(i + 1);

			mapping.put(currentField, nextField);
		}

		return mapping;
	}
	
	/**
	 * @param inputField an input field
	 * 
	 * @return the field's digit value, e.g. 9
	 */
	private Integer getDigitValue(TescoEditText inputField) {

		// Check that we have an input field
		if (inputField == null) {
			return null;
		}

		return getDigitValue(inputField.getText().toString());
	}
}
