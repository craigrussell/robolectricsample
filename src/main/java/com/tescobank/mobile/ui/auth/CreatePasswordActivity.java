package com.tescobank.mobile.ui.auth;

import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createConfirmAndExitResetApplicationAction;

import java.io.Serializable;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.authentication.AuthenticateOTPRequest;
import com.tescobank.mobile.api.authentication.AuthenticateOTPResponse;
import com.tescobank.mobile.api.authentication.FetchOTPTokenRequest;
import com.tescobank.mobile.api.authentication.FetchOTPTokenResponse;
import com.tescobank.mobile.api.credential.SetCredentialRequest;
import com.tescobank.mobile.api.credential.SetCredentialRequest.Credential;
import com.tescobank.mobile.api.credential.SetCredentialResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.application.TescoDeviceMetrics;
import com.tescobank.mobile.security.OTPGenerationListener;
import com.tescobank.mobile.security.SecurityException;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.InvalidValueIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoModal;
import com.tescobank.mobile.ui.auth.password.PasswordActivityHelper;
import com.tescobank.mobile.ui.auth.password.PasswordSubmitter;
import com.tescobank.mobile.ui.errors.ErrorHandler;

public class CreatePasswordActivity extends TescoActivity implements PasswordSubmitter, ErrorHandler, OTPGenerationListener {

	private static final String TAG = CreatePasswordActivity.class.getSimpleName();
	private static final String BUNDLE_KEY_RESET_PASSWORD_RENTERED_VISIBILITY = "resetPasswordEnteredVisibility";
	private static final String BUNDLE_KEY_RESET_PASSWORD_PREVIOUS_ENTERED_VALUE = "resetPasswordPreviousEnteredValue";	
	private static final String BUNDLE_KEY_HELP_DIALOG_SHOWING = "helpDialogShowing";
	
	private ApplicationState applicationState;
	private PasswordActivityHelper enterPasswordActivityHelper;
	private PasswordActivityHelper renterPasswordActivityHelper;
	private InProgressIndicator inProgressIndicator;
	private String previousEnteredPassword;
	private View enterPasswordLabel;
	private View reenterPasswordLabel;
	private View enterPasswordInputField;
	private View reenterPasswordInputField;
	private boolean resetAppOnBackPressedToExitApp;
	private TescoModal modal;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating Reset Password Create Password Activity");
		}

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auth_password_create);
		configureTitle();

		applicationState = getApplicationState();

		enterPasswordLabel = findViewById(R.id.enter_password_label);
		reenterPasswordLabel = findViewById(R.id.reenter_password_label);
		enterPasswordInputField = findViewById(R.id.enter_password_inputField);
		reenterPasswordInputField = findViewById(R.id.reenter_password_inputField);

		InvalidValueIndicator invalidValueIndicator = new InvalidValueIndicator(this);
		inProgressIndicator = new InProgressIndicator(this, true);
		createPasswordActivityHelpers(invalidValueIndicator);
		restoreSavedData(savedInstanceState);
	}
	
	private void configureTitle() {
		TextView title = TextView.class.cast(findViewById(R.id.create_password_title));
		title.setText(getFlow().getTitleId());
	}

	private void createPasswordActivityHelpers(InvalidValueIndicator invalidValueIndicator) {
		enterPasswordActivityHelper = new PasswordActivityHelper(R.id.enter_password_inputField, this, this, invalidValueIndicator);
		renterPasswordActivityHelper = new PasswordActivityHelper(R.id.reenter_password_inputField, this, this, invalidValueIndicator);	
	}

	private void restoreSavedData(Bundle savedInstanceState) {
		if (savedInstanceState == null) {
			return;
		}
		restorePasswordEntryScreen(savedInstanceState);
		restorePreviousEnteredPassword(savedInstanceState);	
		restoreHelpDialog(savedInstanceState);
	}

	private void restorePasswordEntryScreen(Bundle savedInstanceState) {
		Serializable createPasswordRentered = savedInstanceState.getSerializable(BUNDLE_KEY_RESET_PASSWORD_RENTERED_VISIBILITY);
		if (createPasswordRentered != null && (Integer)createPasswordRentered == View.VISIBLE) {
			reenterPasswordLabel.setVisibility(View.VISIBLE);
			reenterPasswordInputField.setVisibility(View.VISIBLE);
			enterPasswordLabel.setVisibility(View.GONE);
			enterPasswordInputField.setVisibility(View.GONE);
		}	else {
			reenterPasswordLabel.setVisibility(View.GONE);
			reenterPasswordInputField.setVisibility(View.GONE);
			enterPasswordLabel.setVisibility(View.VISIBLE);
			enterPasswordInputField.setVisibility(View.VISIBLE);
		}
	}

	private void restorePreviousEnteredPassword(Bundle savedInstanceState) {
		Serializable resetPasswordPreviousEnteredValue = savedInstanceState.getSerializable(BUNDLE_KEY_RESET_PASSWORD_PREVIOUS_ENTERED_VALUE);
		if (resetPasswordPreviousEnteredValue != null) {
			previousEnteredPassword = (String) resetPasswordPreviousEnteredValue;
		}
	}

	private void restoreHelpDialog(Bundle savedInstanceState) {
		if(savedInstanceState.getBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING)) {
			showHelpDialog();
		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(BUNDLE_KEY_RESET_PASSWORD_RENTERED_VISIBILITY, reenterPasswordLabel.getVisibility());
		outState.putSerializable(BUNDLE_KEY_RESET_PASSWORD_PREVIOUS_ENTERED_VALUE, previousEnteredPassword);
		if(null != modal) {
			outState.putBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING, modal.isShowing());
		}	
	}
	
	@Override
	protected void onStart() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Reset Password Create Password Activity");
		}
		trackAnalytic();
		super.onStart();
	}
	
	@Override
	protected void onDestroy() {
		if(null != modal) {
			modal.dismiss();
		}
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onCreateOptionsMenu");
		}
		getSupportMenuInflater().inflate(R.menu.password, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onOptionsItemSelected: " + item);
		}
		switch (item.getItemId()) {
		case R.id.menu_item_information:
			showHelpDialog();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void showHelpDialog() {
		modal = new TescoModal(this);
		modal.setHeadingText(getString(R.string.change_password_modal_header));
		modal.setMessageText(getString(R.string.change_password_modal_message));
		modal.show();
	}

	private void trackAnalytic() {
		applicationState.getAnalyticsTracker().trackEvent(getFlow().getDisplayAnalytic());
	}

	@Override
	protected void onResume() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Resume");
		}

		enterPasswordActivityHelper.setCursorVisible(true);
		renterPasswordActivityHelper.setCursorVisible(true);
		super.onResume();
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if(hasFocus) {
			showKeyboard();
		}
		super.onWindowFocusChanged(hasFocus);
	}

	@Override
	public boolean backFromRetryErrorShouldRetry() {
		return false;
	}

	@Override
	protected void onRetryPressed() {
		returnToEnterPassword();
	}

	@Override
	protected void onBackFromRetryPressed() {
		returnToEnterPassword();
	}

	private void returnToEnterPassword() {
		previousEnteredPassword = null;
		showKeyboard();
		inProgressIndicator.hideInProgressIndicator();
		updateUIForFirstOrSecondEntry();
		enterPasswordActivityHelper.handleRetry();
	}

	@Override
	protected void onRetryForPassiveError() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Retry for Passive Error");
		}
		
		inProgressIndicator.hideInProgressIndicator();
		
		if (previousEnteredPassword == null) {
			enterPasswordActivityHelper.handleRetryForPassiveError();
		} else {
			renterPasswordActivityHelper.handleRetryForPassiveError();
		}
	}

	@Override
	public void handleError(ErrorResponseWrapper errorResponseWrapper) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Error");
		}
		handleErrorResponse(errorResponseWrapper);
	}

	public void onContinuePressed(View view) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Continue");
		}

		submitPassword();
	}

	@Override
	public void submitPassword() {
		String password = getPassword();
		if (isPasswordEmpty(password)) {

			// No password
			handleError(getErrorBuilder().buildErrorResponse(InAppError.BlankPassword));

		} else if (previousEnteredPassword == null) {

			// First password: validate client-side
			validatePassword(password);

		} else if (!password.equals(previousEnteredPassword)) {

			// Second password: password mismatch
			handleError(getErrorBuilder().buildErrorResponse(InAppError.PasswordMismatch));
			previousEnteredPassword = null;
			updateUIForFirstOrSecondEntry();
			inProgressIndicator.hideInProgressIndicator();

		} else {

			// Second password matches: set credential
			startSetCredentialRequest();
		}
	}

	private String getPassword() {
		return (previousEnteredPassword == null) ? enterPasswordActivityHelper.getPassword() : renterPasswordActivityHelper.getPassword();
	}

	private boolean isPasswordEmpty(String password) {
		return (password == null) || password.trim().isEmpty();
	}

	private void validatePassword(String password) {
		if (!enterPasswordActivityHelper.isValid()) {

			// Invalid
			handleError(getErrorBuilder().buildErrorResponse(InAppError.PasswordValidation));

		} else {

			// Valid: prompt for second password
			previousEnteredPassword = password;
			updateUIForFirstOrSecondEntry();
			inProgressIndicator.hideInProgressIndicator();
		}
	}

	/**
	 * Starts the set credentials request
	 */
	private void startSetCredentialRequest() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Set Credential Request");
		}

		TescoDeviceMetrics deviceMetrics = new TescoDeviceMetrics(applicationState.getDeviceId(), applicationState.getDeviceInfo());
		String password = renterPasswordActivityHelper.getPassword();
		SetCredentialRequest request = new PasswordSetCredentialRequest(password);
		request.setCssoID(applicationState.getCssoId());
		request.setCredential(Credential.PASSWORD);
		request.setCredentialValue(password);
		request.setDeviceMetrics(deviceMetrics);
		enterPasswordActivityHelper.setCursorVisible(false);
		renterPasswordActivityHelper.setCursorVisible(false);
		inProgressIndicator.showInProgressIndicator();
		applicationState.getRestRequestProcessor().processRequest(request, getClass());
	}

	/**
	 * @param errorResponse the error response
	 * @return true if the error requires the application to be reset on back
	 */
	private boolean isErrorResetApplication(ErrorResponseWrapper errorResponse) {
		return errorResponse.getErrorResponse().isErrorCommunicatingWithServer();
	}

	/**
	 * Starts the fetch OTP token request
	 */
	private void startFetchOTPTokenRequest(String password) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Fetch OTP Token Request");
		}
		FetchOTPTokenRequest request = new PasswordFetchOTPTokenRequest(password);
		request.setCssoId(applicationState.getCssoId());
		request.setDeviceId(applicationState.getDeviceId());

		enterPasswordActivityHelper.setCursorVisible(false);
		renterPasswordActivityHelper.setCursorVisible(false);
		inProgressIndicator.showInProgressIndicator();
		applicationState.getRestRequestProcessor().processRequest(request, getClass());
	}

	private void createArcotAccountAndGenerateOTP(String encryptedToken, String password) {
		try {
			applicationState.setIsPassword(true);
			applicationState.getSecurityProvider().createAccount(encryptedToken);
			applicationState.getSecurityProvider().generateOTP(password, this);
		} catch (SecurityException e) {
			handleSecurityException();
		}
	}

	@Override
	public void otpGenerated(String otp) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Authenticate OTP Request");
		}

		AuthenticateOTPRequest request = new PasswordAuthenticateOTPRequest();

		request.setTbId(applicationState.getTbId());
		request.setDeviceId(applicationState.getDeviceId());
		request.setOtp(otp);

		applicationState.getRestRequestProcessor().processRequest(request);
	}

	private void handleSecurityException() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Commencing Arcot Account Creation Failed");
		}

		previousEnteredPassword = null;
		updateUIForFirstOrSecondEntry();
		handleError(getErrorBuilder().buildErrorResponse(InAppError.CreateAccount));
	}

	/**
	 * Updates the UI based on whether the user is enter the first or second password
	 */
	private void updateUIForFirstOrSecondEntry() {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Updating UI for First or Second Entry");
		}

		Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim_exit_left);
		animation.setAnimationListener(new PasswordAnimationListener());

		if (enterPasswordLabel.getVisibility() == View.VISIBLE) {
			enterPasswordLabel.startAnimation(animation);
			enterPasswordInputField.startAnimation(animation);
		} else {
			reenterPasswordLabel.startAnimation(animation);
			reenterPasswordInputField.startAnimation(animation);
		}
	}
	
	private void onAuthenticationSuceeded() {
		getApplicationState().getSecurityProvider().lastOTPWasSuccessful();
		getFlow().onPolicyComplete(this);
	}

	@Override
	public void onBackPressed() {
		if (reenterPasswordLabel.getVisibility() == View.VISIBLE) {
			previousEnteredPassword = null;
			updateUIForFirstOrSecondEntry();
			inProgressIndicator.hideInProgressIndicator();
			return;
		}
		if (resetAppOnBackPressedToExitApp) {
			getFlow().updatePolicyCancellation(createConfirmAndExitResetApplicationAction());	
		}
		super.onBackPressed();
	}

	private class PasswordSetCredentialRequest extends SetCredentialRequest {

		private String password;

		PasswordSetCredentialRequest(String password) {
			this.password = password;
		}

		@Override
		public void onResponse(SetCredentialResponse response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Set Credential Request Succeeded");
			}

			if (!response.getSuccessful()) {
				returnToEnterPassword();
				handleError(getErrorBuilder().buildErrorResponse(InAppError.HttpStatus200));
			} else {
				// From this point forward the password on the server may have been updated
				// so the user going back should always exit and reset the app
				resetAppOnBackPressedToExitApp = true;
				startFetchOTPTokenRequest(password);
			}

		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponse) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Set Credential Request Failed: " + errorResponse.getErrorResponse().getErrorCode());
			}

			// From this point forward the password on the server may have been updated
			// so the user going back should always exit and reset the app
			if (isErrorResetApplication(errorResponse)) {
				resetAppOnBackPressedToExitApp = true;
			}
			returnToEnterPassword();
			handleError(errorResponse);
		}
	};

	private class PasswordFetchOTPTokenRequest extends FetchOTPTokenRequest {

		private String password;

		PasswordFetchOTPTokenRequest(String password) {
			this.password = password;
		}

		@Override
		public void onResponse(FetchOTPTokenResponse response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Fetch OTP Token Request Succeeded");
			}
			createArcotAccountAndGenerateOTP(response.getEncryptedToken(), password);
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponse) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Fetch OTP Token Request Failed: " + errorResponse.getErrorResponse().getErrorCode());
			}
			handleError(errorResponse);
		}
	}

	private class PasswordAuthenticateOTPRequest extends AuthenticateOTPRequest {

		@Override
		public void onResponse(AuthenticateOTPResponse response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Authenticate OTP Request Succeeded");
			}
			
			if (!response.getAuthenticated()) {
				returnToEnterPassword();
				handleError(getErrorBuilder().buildErrorResponse(InAppError.HttpStatus200));
				return;
			}
			onAuthenticationSuceeded();
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Authenticate OTP Request Failed: " + errorResponseWrapper.getErrorResponse().getErrorCode());
			}
			returnToEnterPassword();
			handleError(errorResponseWrapper);
		}
	}
	
	private class PasswordAnimationListener implements Animation.AnimationListener {

		@Override
		public void onAnimationStart(Animation animation) {
			// Do nothing
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			Animation enterRight = AnimationUtils.loadAnimation(CreatePasswordActivity.this, R.anim.anim_enter_right);
			
			if (enterPasswordLabel.getVisibility() == View.VISIBLE) {
				// Hide first and show second
				enterPasswordInputField.setVisibility(View.GONE);
				enterPasswordLabel.setVisibility(View.GONE);
				reenterPasswordInputField.setVisibility(View.VISIBLE);
				reenterPasswordLabel.setVisibility(View.VISIBLE);
				reenterPasswordInputField.startAnimation(enterRight);
				reenterPasswordLabel.startAnimation(enterRight);
				
			} else {
				// Hide second and show first
				enterPasswordInputField.setVisibility(View.VISIBLE);
				enterPasswordLabel.setVisibility(View.VISIBLE);
				enterPasswordInputField.startAnimation(enterRight);
				enterPasswordLabel.startAnimation(enterRight);
				reenterPasswordInputField.setVisibility(View.GONE);
				reenterPasswordLabel.setVisibility(View.GONE);
				
			}

			// Clear both and give focus to first then show keyboard
			renterPasswordActivityHelper.handleRetry();
			enterPasswordActivityHelper.handleRetry();
			showKeyboard();
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// Do nothing
		}
	}
}
