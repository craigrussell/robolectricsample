package com.tescobank.mobile.ui.auth.documents;

import android.content.Context;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.content.AcknowledgeContentRequest;
import com.tescobank.mobile.api.content.AcknowledgeContentResponse;
import com.tescobank.mobile.api.content.RetrieveContentResponse.Document;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.rest.processor.RestRequestProcessor;
import com.tescobank.mobile.ui.errors.ErrorHandler;

/**
 * Request used to acknowledge documents
 */
public class AcknowledgeDocumentRequest extends AcknowledgeContentRequest {
	
	private static final String TAG = AcknowledgeDocumentRequest.class.getSimpleName();
	
	/** The document to acknowledge */
	private Document document;
	
	/** The document data helper */
	private DocumentsDataHelper documentsDataHelper;
	
	/** The document acknowledgement listener */
	private DocumentAcknowledgementListener documentAcknowledgementListener;
	
	/** The error handler */
	private ErrorHandler errorHandler;
	
	/** The request processor */
	private RestRequestProcessor<ErrorResponseWrapper> requestProcessor;
	
	/** The application state */
	private ApplicationState applicationState;

	/** The context. */
	private Context ctx;
	
	/**
	 * Constructor
	 * 
	 * @param ctx the context
	 * @param document the document to acknowledge
	 * @param documentsDataHelper the document data helper
	 * @param documentAcknowledgementListener the document acknowledgement listener
	 * @param errorHandler the error handler
	 * @param requestProcessor the request processor
	 * @param applicationState the application state
	 */
	public AcknowledgeDocumentRequest(Context ctx, Document document, DocumentsDataHelper documentsDataHelper, DocumentAcknowledgementListener documentAcknowledgementListener, ErrorHandler errorHandler, RestRequestProcessor<ErrorResponseWrapper> requestProcessor, ApplicationState applicationState) {
		super();
		
		this.ctx = ctx;
		this.document = document;
		this.documentsDataHelper = documentsDataHelper;
		this.documentAcknowledgementListener = documentAcknowledgementListener;
		this.errorHandler = errorHandler;
		this.requestProcessor = requestProcessor;
		this.applicationState = applicationState;

		setContentID(document.getDocID());
		setContentversion(document.getVersion());
		setTbId(applicationState.getTbId());

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Acknowledging Document with ID = " + getContentID() + ", Version = " + getContentversion());
		}
	}
	
	@Override
	public void onResponse(AcknowledgeContentResponse response) {
		if (!response.getContentAccepted()) {
			errorHandler.handleError(new ErrorResponseWrapperBuilder(ctx).buildErrorResponse(InAppError.HttpStatus200));
			return;
		}
		onResponse();
	}
	
	private void onResponse() {
		document.setAcknowledged(true);

		Document nextDocument = documentsDataHelper.getNextDocumentToAcknowledge(document);
		if (nextDocument != null) {
			onResponseSuccess(nextDocument);
		} else {
			onResponseFailure();
		}
	}
	
	private void onResponseSuccess(Document nextDocument) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Acknowledge Document Request for Next Document");
		}

		AcknowledgeDocumentRequest acknowledgeDocumentRequest = new AcknowledgeDocumentRequest(ctx, nextDocument, documentsDataHelper, documentAcknowledgementListener, errorHandler, requestProcessor, applicationState);
		requestProcessor.processRequest(acknowledgeDocumentRequest);
	}
	
	private void onResponseFailure() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "No More Documents to Acknowledge, Notify Document Acknowledgement Listener");
		}

		documentAcknowledgementListener.onDocumentAcknowledgementComplete();
	}
	
	@Override
	public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Acknowledge Document Request Failed: " + errorResponseWrapper.getErrorResponse().getErrorCode());
		}
		errorHandler.handleError(errorResponseWrapper);
	}
}
