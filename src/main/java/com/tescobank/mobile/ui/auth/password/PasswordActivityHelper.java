package com.tescobank.mobile.ui.auth.password;

import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.InvalidValueIndicator;
import com.tescobank.mobile.ui.TescoActivity;

/**
 * Helper for password-related activities
 */
public class PasswordActivityHelper {

	private static final String TAG = PasswordActivityHelper.class.getSimpleName();

	/** The invalid value indicator for the activity */
	private InvalidValueIndicator invalidValueIndicator;

	/** The password field */
	private EditText passwordField;

	/** The password validator */
	private PasswordValidator passwordValidator = new PasswordValidator();

	/**
	 * Constructor
	 * 
	 * @param activity
	 *            the activity
	 * @param passwordSubmitter
	 *            the password submitter
	 * @param invalidValueIndicator
	 *            the invalid value indicator for the activity
	 */
	public PasswordActivityHelper(TescoActivity activity, final PasswordSubmitter passwordSubmitter, InvalidValueIndicator invalidValueIndicator) {
		this(R.id.inputField, activity, passwordSubmitter, invalidValueIndicator);
	}

	/**
	 * Constructor
	 * 
	 * @param passwordFieldId
	 *            the password field ID
	 * @param activity
	 *            the activity
	 * @param passwordSubmitter
	 *            the password submitter
	 * @param invalidValueIndicator
	 *            the invalid value indicator for the activity
	 */
	public PasswordActivityHelper(int passwordFieldId, TescoActivity activity, final PasswordSubmitter passwordSubmitter, InvalidValueIndicator invalidValueIndicator) {
		super();

		this.invalidValueIndicator = invalidValueIndicator;

		this.passwordField = (EditText) activity.findViewById(passwordFieldId);
		this.passwordField.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_NEXT) {
					passwordSubmitter.submitPassword();
				}
				return false;
			}
		});
	}

	/**
	 * Handles retry
	 */
	public void handleRetry() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Retry");
		}

		// Clear values
		passwordField.setText(null);

		// Give the input field the focus
		passwordField.requestFocus();
	}

	/**
	 * Handles retry for a passive error
	 */
	public void handleRetryForPassiveError() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Retry for Passive Error");
		}

		invalidValueIndicator.indicateMissingValue(passwordField);
	}

	/**
	 * Gives the password field focus
	 */
	public void givePasswordFieldFocus() {
		passwordField.requestFocus();
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return passwordField.getText().toString();
	}

	/**
	 * @return true if the password is valid
	 */
	public boolean isValid() {
		return passwordValidator.isValid(getPassword());
	}

	public void setCursorVisible(boolean visible) {
		passwordField.setCursorVisible(visible);
	}

}
