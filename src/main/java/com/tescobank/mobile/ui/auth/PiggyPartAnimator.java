package com.tescobank.mobile.ui.auth;

import java.util.Random;

import android.graphics.drawable.AnimationDrawable;
import android.widget.ImageView;


public class PiggyPartAnimator {

	private final ImageView piggyPart;
	private final AnimationDrawable anim;
	private final long duration;

	private DelayGenerator delayGenerator;
	private boolean destroyed;

	protected PiggyPartAnimator(DelayGenerator delayGenerator, ImageView piggyPart, int animationDrawableId, boolean flip) {
		this.delayGenerator = delayGenerator;
		this.piggyPart = piggyPart;

		piggyPart.setImageDrawable(null);
		piggyPart.setBackgroundResource(animationDrawableId);

		this.anim = (AnimationDrawable) piggyPart.getBackground();
		long duration = 0l;
		for (int i = 0; i < anim.getNumberOfFrames(); i++) {
			duration += anim.getDuration(i);
		}
		this.duration = duration;

		if (flip) {
		}
	}

	public PiggyPartAnimator(ImageView piggyPart, int animationDrawableId, boolean flip) {
		this(new DefaultDelayGenerator(), piggyPart, animationDrawableId, flip);
	}

	/**
	 * Begin animating. Use of {@link View#postDelayed(Runnable, long)} means that we don't need to worry about the activity lifecycle.
	 */
	public void animate() {
		if (destroyed) {
			return;
		}
		long delay = delayGenerator.nextDelay();
		piggyPart.postDelayed(new Runnable() {
			@Override
			public void run() {
				anim.start();
				waitForAnimationFinishThenAnimate();
			}
		}, delay);
	}

	private void waitForAnimationFinishThenAnimate() {
		if (destroyed) {
			return;
		}

		piggyPart.postDelayed(new Runnable() {
			@Override
			public void run() {
				anim.stop();
				animate();
			}
		}, duration);
	}

	interface DelayGenerator {
		long nextDelay();
	}

	static class DefaultDelayGenerator implements DelayGenerator {
		private final Random rnd = new Random();

		@Override
		public long nextDelay() {
			return rnd.nextInt(4000) + 1000;
		}
	}

	public void destroy() {
		destroyed = true;
		
		if (anim.isRunning()) {
			anim.stop();
		}
		piggyPart.setBackgroundResource(0);
	}

}
