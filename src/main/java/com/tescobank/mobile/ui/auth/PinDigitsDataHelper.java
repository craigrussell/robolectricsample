package com.tescobank.mobile.ui.auth;

import java.util.List;

import android.widget.TextView;

import com.tescobank.mobile.ui.TescoEditText;

/**
 * PIN digits data helper; handles null / bad data by ignoring it
 */
public class PinDigitsDataHelper extends AuthDataHelper {

	/** Labels for all values, e.g. [L1, L2, L3, L4, L5, L6] */
	private List<TextView> labelsForAllValues;

	/** Labels for required values, e.g. [L1, L3] */
	private List<TextView> labelsForRequiredValues;

	/**
	 * Constructor
	 * 
	 * @param indicesOfRequiredValues one-based indices of required values, e.g. [1, 3]
	 * @param inputFieldsForAllValues input fields for all values, e.g. [I1, I2, I3, I4, I5, I6]
	 * @param labelsForAllValues labels for all values, e.g. [L1, L2, L3, L4, L5, L6]
	 */
	public PinDigitsDataHelper(Integer[] indicesOfRequiredValues, List<TescoEditText> inputFieldsForAllValues, List<TextView> labelsForAllValues) {
		super(indicesOfRequiredValues, inputFieldsForAllValues);
		
		this.labelsForAllValues = labelsForAllValues;
		this.labelsForRequiredValues = getViewsForRequiredValues(labelsForAllValues);
	}
	
	/**
	 * @return labels for all values, e.g. [L1, L2, L3, L4, L5, L6]
	 */
	public List<TextView> getLabelsForAllValues() {
		return labelsForAllValues;
	}

	/**
	 * @return labels for required values, e.g. [L1, L3]
	 */
	public List<TextView> getLabelsForRequiredValues() {
		return labelsForRequiredValues;
	}
}
