package com.tescobank.mobile.ui.auth;

import static com.tescobank.mobile.ui.auth.AuthPolicyActionFactory.createConfirmAndExitAction;

import java.util.List;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.analytics.AnalyticsEventFactory;
import com.tescobank.mobile.api.content.RetrieveContentResponse;
import com.tescobank.mobile.api.content.RetrieveContentResponse.Document;
import com.tescobank.mobile.api.deviceregistration.RegisterDeviceRequest;
import com.tescobank.mobile.api.deviceregistration.RegisterDeviceResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.application.TescoDeviceMetrics;
import com.tescobank.mobile.flow.PolicyAction;
import com.tescobank.mobile.flow.PolicyActionListener;
import com.tescobank.mobile.flow.TescoPolicy;
import com.tescobank.mobile.flow.TescoPolicyAction;
import com.tescobank.mobile.rest.processor.RestRequestProcessor;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.ActionBarConfiguration.Style;
import com.tescobank.mobile.ui.ActionBarConfiguration.Type;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.auth.documents.AcknowledgeDocumentRequest;
import com.tescobank.mobile.ui.auth.documents.DocumentAcknowledgementListener;
import com.tescobank.mobile.ui.auth.documents.DocumentsDataHelper;
import com.tescobank.mobile.ui.carousel.CarouselActivity;
import com.tescobank.mobile.ui.errors.ErrorHandler;

public class RegistrationCompletionAction extends TescoPolicyAction implements ErrorHandler, DocumentAcknowledgementListener, PolicyActionListener {

	private static final String TAG = RegistrationCompletionAction.class.getSimpleName();
	private static final long serialVersionUID = -6026080270568750802L;

	private transient TescoActivity activity;
	private transient ApplicationState applicationState;
	private transient RestRequestProcessor<ErrorResponseWrapper> requestProcessor;
	private DocumentsDataHelper documentsDataHelper;
	private boolean isRegistration;
	
	public RegistrationCompletionAction(boolean isRegistration) {
		this.isRegistration = isRegistration;
	}
	
	@Override
	public void execute(TescoActivity activity) {
		this.activity = activity;
		applicationState = activity.getApplicationState();
		requestProcessor = applicationState.getRestRequestProcessor();
		documentsDataHelper = new DocumentsDataHelper(getDocuments());
		startRegisterDeviceRequest();
	}
	
	@SuppressWarnings({"unchecked"})
	private List<Document> getDocuments() {
		return (List<Document>) activity.getFlow().getDataBundle().get(RetrieveContentResponse.class.getName());
	}
	
	private void startRegisterDeviceRequest() {
		TescoDeviceMetrics deviceMetrics = new TescoDeviceMetrics(applicationState.getDeviceId(), applicationState.getDeviceInfo());
		RegisterDeviceRequest request = new PasswordRegisterDeviceRequest();
		request.setCssoID(applicationState.getCssoId());
		request.setDeviceMetrics(deviceMetrics);
		requestProcessor.processRequest(request);
	}

	private void acknowledgeDocuments() {
		Document document = documentsDataHelper.getNextDocumentToAcknowledge(null);
		if (document == null) {
			addRemainingPolicies();
			return;
		}
		startAcknowledgeDocumentRequest(document);
	}

	private void startAcknowledgeDocumentRequest(Document document) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Acknowledge Document Request");
		}
		AcknowledgeDocumentRequest request = new AcknowledgeDocumentRequest(activity, document, documentsDataHelper, this, this, requestProcessor, applicationState);
		requestProcessor.processRequest(request);
	}

	@Override
	public void onDocumentAcknowledgementComplete() {
		activity.getApplicationState().persistSensitiveData();		
		addRemainingPolicies();
	}

	private void addRemainingPolicies() {
		if(isSetPasscodeEnabled()) {
			addRegistrationAndCarouselPolicy();
		} else {
			loadProductsAndAddCarouselPolicy();
		}
	}
	
	private boolean isSetPasscodeEnabled() {
		return activity.getApplicationState().getFeatures().isSetPasscodeEnabled();
	}
	
	private void addRegistrationAndCarouselPolicy() {
		addRegistrationPolicy();
		addCarouselPolicy();
		onActionCompleted();
	}

	private void loadProductsAndAddCarouselPolicy() {
		addCarouselPolicy();
		createCarouselLoaderAction();
	}
	
	public void createCarouselLoaderAction() {
		ProductDataLoaderAction carouselLoaderAction = new ProductDataLoaderAction(false, false);
		carouselLoaderAction.setListener(this);
		carouselLoaderAction.execute(activity);
	}
	
	private void addRegistrationPolicy() {
		ActionBarConfiguration config = new ActionBarConfiguration(Style.UNAUTHENTICATED, Type.HOME);
		AnalyticsEventFactory analyticsEventFactory = new AnalyticsEventFactory();
		TescoPolicy registrationCompletePolicy = new TescoPolicy.Builder(RegistrationCompleteActivity.class)
		.setActivityDisplayedAnalytic(isRegistration ? analyticsEventFactory.eventForRegistrationComplete() : null)
		.setActionBarConfig(config)
		.setCancellation(createConfirmAndExitAction()).create();
		activity.getFlow().addPolicy(registrationCompletePolicy);
	}
	
	private void addCarouselPolicy() {
		TescoPolicy carouselActivity = new TescoPolicy.Builder(CarouselActivity.class).setCancellation(createConfirmAndExitAction()).create();
		activity.getFlow().addPolicy(carouselActivity);
	}

	@Override
	public void handleError(ErrorResponseWrapper errorResponseWrapper) {
		activity.handleErrorResponse(errorResponseWrapper);	
	}
	
	private class PasswordRegisterDeviceRequest extends RegisterDeviceRequest {
		
		@Override
		public void onResponse(RegisterDeviceResponse response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Register Device Request Succeeded");
			}

			if (!response.getRegistrationComplete()) {
				ErrorResponseWrapperBuilder builder = new ErrorResponseWrapperBuilder(activity);
				activity.handleErrorResponse(builder.buildErrorResponse(InAppError.HttpStatus200));
				return;
			}
			acknowledgeDocuments();
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Register Device Request Failed: " + errorResponseWrapper.getErrorResponse().getErrorCode());
			}
			handleError(errorResponseWrapper);
		}
	}
	
	@Override
	public void onActionComplete(PolicyAction action) {
		onActionCompleted();
	}
}
