package com.tescobank.mobile.ui.auth;

import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_TOO_MANY_SMS_REQUESTED;

import java.util.ArrayList;
import java.util.List;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.authentication.GetSMSChallengeRequest;
import com.tescobank.mobile.api.authentication.GetSMSChallengeResponse;
import com.tescobank.mobile.api.authentication.VerifySMSChallengeRequest;
import com.tescobank.mobile.api.authentication.VerifySMSChallengeResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapper.ErrorResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.InvalidValueIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoAlertDialog;
import com.tescobank.mobile.ui.TescoEditText;
import com.tescobank.mobile.ui.TescoEditText.OnDeleteListener;
import com.tescobank.mobile.ui.TescoModal;
import com.tescobank.mobile.ui.tutorial.TooltipId;
import com.tescobank.mobile.ui.tutorial.groups.SmsGroup;

public class SMSActivity extends TescoActivity {

	private static final String TAG = SMSActivity.class.getSimpleName();
	public static final String BUNDLE_KEY_SMS_LAYOUT = "BUNDLE_KEY_SMS_LAYOUT";
	private static final String BUNDLE_KEY_HELP_DIALOG_SHOWING = "helpDialogShowing";
	public static final String OTP_LOCKED_ERROR_CODE = "EVS-01";
	private static final int MAX_MOBILE_LENGTH = 4;

	private SMSDataHelper smsDataHelper;
	private InvalidValueIndicator invalidValueIndicator;
	private InProgressIndicator inProgressIndicator;
	private boolean ignoreTextChanges;
	private SMSThrottler smsThrottler;
	private ErrorResponse retryErrorResponse = null;
	private boolean activityHasHadFocus;
	private TescoModal modal;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating SMS Activity");
		}

		super.onCreate(savedInstanceState);
		setContentView(getLayout());
		configureTitle();
		
		// Obtain details from the challenge
		GetSMSChallengeResponse getSMSChallengeResponse = (GetSMSChallengeResponse) getFlow().getDataBundle().getSerializable(GetSMSChallengeResponse.class.getName());
		String mobileNumber = getSMSChallengeResponse.getMobileNumber();

		smsThrottler = new SMSThrottler(getApplicationState().getSettings().getConfiguration(), getApplicationState().getPersister());

		// Populate information text
		String lastFourDigitsOfMobileNumber = null;
		if ((mobileNumber != null) && (mobileNumber.length() >= MAX_MOBILE_LENGTH)) {
			lastFourDigitsOfMobileNumber = mobileNumber.substring(mobileNumber.length() - MAX_MOBILE_LENGTH, mobileNumber.length());
		} else {
			lastFourDigitsOfMobileNumber = getResources().getString(R.string.auth_sms_dummy_mobile_label);
		}
		TextView smsInfoText = (TextView) findViewById(R.id.smsInfoText);
		smsInfoText.setText(String.format(getResources().getString(R.string.auth_sms_label), lastFourDigitsOfMobileNumber));

		// Identify input fields
		List<TescoEditText> inputFields = new ArrayList<TescoEditText>();
		inputFields.add((TescoEditText) findViewById(R.id.inputField1));
		inputFields.add((TescoEditText) findViewById(R.id.inputField2));
		inputFields.add((TescoEditText) findViewById(R.id.inputField3));
		inputFields.add((TescoEditText) findViewById(R.id.inputField4));
		inputFields.add((TescoEditText) findViewById(R.id.inputField5));
		inputFields.add((TescoEditText) findViewById(R.id.inputField6));
		inputFields.add((TescoEditText) findViewById(R.id.inputField7));
		inputFields.add((TescoEditText) findViewById(R.id.inputField8));

		smsDataHelper = new SMSDataHelper(inputFields);
		invalidValueIndicator = new InvalidValueIndicator(this);
		inProgressIndicator = new InProgressIndicator(this, true);
		configureSMSDigitControls();
		configureTimeoutManager();
		
		if(null != savedInstanceState) {
			restoreSavedData(savedInstanceState);
		}
	}
	
	private void configureTitle() {
		TextView title = TextView.class.cast(findViewById(R.id.sms_title));
		if(title != null) {
			title.setText(getFlow().getTitleId());
		}
	}
	
	private void restoreSavedData(Bundle savedInstanceState) {
		if (savedInstanceState == null) {
			return;
		}
		getApplicationState().restoreData(savedInstanceState);
		restoreHelpDialog(savedInstanceState);
	}

	private void restoreHelpDialog(Bundle savedInstanceState) {
		if (savedInstanceState.getBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING)) {
			showHelpDialog();
		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		getApplicationState().saveData(outState);
		if(modal != null) {
			outState.putBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING, modal.isShowing());
		}
	}
	
	@Override
	protected void onDestroy() {
		if(null != modal) {
			modal.dismiss();
		}
		super.onDestroy();
	}

	@Override
	protected void onStart() {
		trackAnalytic();
		super.onStart();
	}

	private void trackAnalytic() {
		getApplicationState().getAnalyticsTracker().trackEvent(getFlow().getDisplayAnalytic());
	}

	@Override
	protected void onResume() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Commencing onResume");
		}

		smsDataHelper.setCursorVisible(true);
		super.onResume();

	}

	@Override
	protected void onRetryPressed() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Commencing Retry");
		}

		if (isNoMoreRetriesAndOtpLocked()) {
			displayNoMoreRetriesAndOtpLockedDialog();
		} else {
			retryLastRequestIfRequired();
		}

		if (smsDataHelper.allRequiredValuesProvided()) {
			resetFields();
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus && !activityHasHadFocus) {
			activityHasHadFocus = true;
			if(portraitOnly()) {
				getTooltipManager().setGroup(new SmsGroup(this));
				getTooltipManager().show();
			}	
		}
	}

	private void retryLastRequestIfRequired() {
		Class<?> lastRequest = getApplicationState().getRestRequestProcessor().getLastRequestTypeForRequesterType(getClass());
		if (SMSGetSMSChallengeRequest.class.equals(lastRequest)) {
			resetFields();
			resendSMS();
		}
	}

	public boolean backFromRetryErrorShouldRetry() {
		return false;
	}

	@Override
	protected void onBackFromRetryPressed() {
		// Do nothing
	}

	private boolean isNoMoreRetriesAndOtpLocked() {
		return (retryErrorResponse != null) && retryErrorResponse.getErrorCode().equals(OTP_LOCKED_ERROR_CODE) && smsThrottler.isThrottled(false);
	}

	@Override
	protected void onRetryForPassiveError() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Commencing Retry for Passive Error");
		}

		onRetryPressed();

		List<TescoEditText> inputFields = smsDataHelper.getInputFieldsForRequiredValuesWithNoValue();
		if (inputFields.isEmpty()) {
			invalidValueIndicator.indicateMissingValue(smsDataHelper.getInputFieldsForRequiredValues());
		} else {
			invalidValueIndicator.indicateMissingValue(inputFields);
		}
	}

	public void onContinuePressed(View view) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Continue");
		}
		List<TescoEditText> inputFields = smsDataHelper.getInputFieldsForRequiredValuesWithNoValue();
		if (inputFields.isEmpty()) {
			verfiySMSChallenge();
		} else {
			handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.IncompleteSMS));
		}
	}

	private void onTooManyRequests() {
		finishWithResult(RESULT_TOO_MANY_SMS_REQUESTED);
		smsThrottler.resetCount();
	}

	private void verfiySMSChallenge() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Verify SMS Challenge");
		}

		// Check that all required values have been provided
		if (!smsDataHelper.allRequiredValuesProvided()) {
			return;
		}

		smsDataHelper.setCursorVisible(false);
		showProgress(true);

		// Create the API request
		VerifySMSChallengeRequest request = new SMSVerifySMSChallengeRequest();

		request.setCssoID(getApplicationState().getCssoId());
		request.setSmsDigits(smsDataHelper.getRequiredValues().toArray(new Integer[0]));

		// Submit the API request
		getApplicationState().getRestRequestProcessor().processRequest(request, getClass());
	}

	private void onDone() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Password Activity");
		}
		getFlow().onPolicyComplete(this);
	}
	
	private int getLayout() {
		return getFlow().getDataBundle().getInt(BUNDLE_KEY_SMS_LAYOUT);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onCreateOptionsMenu");
		}
		getSupportMenuInflater().inflate(R.menu.sms, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_item_information:
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "showing info box");
			}
			showHelpDialog();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void showHelpDialog() {
		modal = new TescoModal(this);
		modal.setHeadingText(getString(R.string.auth_sms_modal_header));
		modal.setMessageText(getString(R.string.auth_sms_modal_message));
		modal.show();
	}

	public void onResendPressed(View view) {
		if (smsThrottler.isThrottled(false)) {
			displaySMSLimitDialog();
		} else {
			resendConfirmation();
		}
	}

	private void resendConfirmation() {
		new TescoAlertDialog.Builder(this).setTitle(R.string.auth_confirm_resend_sms_title).setMessage(R.string.auth_confirm_resend_sms_message).setPositiveButton(R.string.dialog_confirm_ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				resendSMS();
			}

		}).setNegativeButton(R.string.dialog_confirm_cancel, null).create().show();
	}

	private void displaySMSLimitDialog() {
		new TescoAlertDialog.Builder(this).setTitle(R.string.auth_max_sms_reached_title).setMessage(R.string.auth_max_sms_reached_description).setPositiveButton(R.string.dialog_confirm_ok, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				onTooManyRequests();
			}

		}).setNegativeButton(R.string.dialog_confirm_cancel, null).setCancelable(true).create().show();
	}

	private void displayNoMoreRetriesAndOtpLockedDialog() {
		new TescoAlertDialog.Builder(this).setTitle(R.string.error_display_title_otp_locked_no_retries).setMessage(R.string.error_display_message_otp_locked_no_retries).setPositiveButton(R.string.dialog_confirm_ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				onTooManyRequests();
			}

		}).setCancelable(false).create().show();
	}

	private void resendSMS() {
		showProgress(true);

		hideKeyboard();

		GetSMSChallengeRequest request = new SMSGetSMSChallengeRequest();

		request.setCssoID(getApplicationState().getCssoId());
		request.setTbID(getApplicationState().getTbId());

		getApplicationState().getRestRequestProcessor().processRequest(request, getClass());
	}

	private void showProgress(boolean show) {
		findViewById(R.id.progressIndicatorImage).setVisibility(show ? View.VISIBLE : View.GONE);
		findViewById(R.id.continueButton).setVisibility(show ? View.GONE : View.VISIBLE);
		findViewById(R.id.resend_sms_button).setVisibility(show ? View.GONE : View.VISIBLE);
		if (show) {
			inProgressIndicator.showInProgressIndicator();
		} else {
			inProgressIndicator.hideInProgressIndicator();
		}
	}

	private void handleError(ErrorResponseWrapper errorResponseWrapper) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Error");
		}
		getFlow().onPolicyError(this, errorResponseWrapper);
		showProgress(false);

		// Handle as a standard error
		handleErrorResponse(errorResponseWrapper);
	}

	private void configureSMSDigitControls() {

		List<TescoEditText> inputFields = smsDataHelper.getInputFieldsForRequiredValues();
		enableInputFields(inputFields);
		respondToNextKey(inputFields);
		respondToTextChanges(inputFields);
		respondToDeleteKey(inputFields);
	}
	
	private void configureTimeoutManager() {
		long smsTimeout = getApplicationState().getSettings().getSmsTimeOut() * MILLIS_PER_SECOND;
		setTimeoutManager(getApplicationState().createTimeoutManager(this, smsTimeout, smsTimeout));
	}

	private void enableInputFields(List<TescoEditText> inputFields) {
		for (TescoEditText inputField : inputFields) {
			inputField.setEnabled(true);
		}
	}

	private void respondToNextKey(List<TescoEditText> inputFields) {
		NextKeyListener nextKeyListener = new NextKeyListener();
		for (TescoEditText inputField : inputFields) {
			inputField.setOnEditorActionListener(nextKeyListener);
		}
	}

	private void respondToTextChanges(List<TescoEditText> inputFields) {
		for (TescoEditText inputField : inputFields) {
			inputField.addTextChangedListener(new TextChangeListener(inputField));
		}
	}

	private void respondToDeleteKey(List<TescoEditText> inputFields) {
		OnDeleteListener deleteListener = new DeleteListener();
		for (TescoEditText inputField : inputFields) {
			TescoEditText tesco = (TescoEditText) inputField;
			tesco.setOnDeleteListener(deleteListener);
		}
	}

	private void resetFields() {
		// Clear values
		List<TescoEditText> inputFields = smsDataHelper.getInputFieldsForRequiredValues();
		for (TescoEditText inputField : inputFields) {
			inputField.setText(null);
			inputField.setCursorVisible(true);
		}

		// Give the first input field the focus
		TescoEditText firstInputField = smsDataHelper.getFirstInputFieldForRequiredValue();
		if (firstInputField != null) {
			firstInputField.requestFocus();
		}
	}

	/**
	 * Responds to Next being pressed
	 */
	private class NextKeyListener implements OnEditorActionListener {

		@Override
		public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
			if (actionId == EditorInfo.IME_ACTION_NEXT) {
				onContinuePressed(textView);
				return true;
			}

			return false;
		}
	}

	/**
	 * Responds to SMS digit changes
	 */
	private class TextChangeListener implements TextWatcher {

		private TescoEditText inputField;

		private int charsBeforeChange;

		public TextChangeListener(TescoEditText inputField) {
			super();

			this.inputField = inputField;
		}

		@Override
		public void afterTextChanged(Editable editable) {

			// Check that we should be responding to updates
			if (ignoreTextChanges) {
				return;
			}

			// Prevent other listeners from responding to updates (i.e. prevent
			// looping)
			ignoreTextChanges = true;

			if (isOneCharacter(editable)) {

				// We have one character so move the focus forward if
				// appropriate
				moveFocusForwardIfAppropriate();

			} else if (isChangeFromOneToTwoCharacters(editable)) {

				// We have changed from one to two characters so the user has
				// typed more than they should. Just take the first character
				populateInputFieldWithFirstCharacter(editable);
				moveFocusForwardIfAppropriate();

			} else if (isMoreThanTwoCharacters(editable)) {

				// We have more than two characters so the user has pasted.
				// Populate input fields from the paste
				populateInputFieldsFromPaste(editable);
			}

			// Allow other listeners to respond to updates again
			ignoreTextChanges = false;
		}

		private boolean isOneCharacter(Editable editable) {
			return editable.length() == 1;
		}

		private boolean isChangeFromOneToTwoCharacters(Editable editable) {
			return (editable.length() == 2) && (charsBeforeChange == 1);
		}

		private boolean isMoreThanTwoCharacters(Editable editable) {
			return editable.length() >= 2;
		}

		private void moveFocusForwardIfAppropriate() {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Moving Focus Forward If Appropriate");
			}

			TescoEditText nextInputField = smsDataHelper.getNextInputFieldForRequiredValue(inputField);
			if (nextInputField != null) {

				if (BuildConfig.DEBUG) {
					Log.d(TAG, "Giving Next Field Focus And Selecting Value If Any");
				}

				nextInputField.requestFocus();
			}
		}

		private void populateInputFieldWithFirstCharacter(Editable editable) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Populating Input Field With First Character: Text = " + editable.toString());
			}

			inputField.setText(String.valueOf(editable.charAt(0)));
			inputField.setSelection(1);
		}

		private void populateInputFieldsFromPaste(Editable pasteText) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Populating Input Fields From Paste: Text = " + pasteText.toString());
			}

			List<TescoEditText> inputFields = smsDataHelper.getInputFieldsForRequiredValues();
			List<String> values = smsDataHelper.getValuesForInputFieldsForRequiredValues(pasteText.toString());
			int inputFieldCount = inputFields.size();

			// Populate input fields from paste text
			for (int i = 0; (i < inputFieldCount) && (i < values.size()); i++) {
				inputFields.get(i).setText(values.get(i));
			}

			if (inputFieldCount > pasteText.length()) {

				// We have more fields than paste text so go give the first
				// empty field the focus
				inputFields.get(pasteText.length()).requestFocus();

			} else {

				// All fields are populate so give the last field the focus
				TescoEditText lastInputField = inputFields.get(inputFieldCount - 1);
				lastInputField.requestFocus();
				lastInputField.setSelection(1);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			charsBeforeChange = (s == null) ? 0 : s.length();
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if (count != 0) {
				getTooltipManager().dismiss(TooltipId.SmsCopyPaste.ordinal());
			}
		}
	}

	class DeleteListener implements OnDeleteListener {
		@Override
		public void onDeletePressed(TescoEditText editText) {
			if (editText.getText().toString().trim().isEmpty()) {
				TescoEditText previous = smsDataHelper.getPreviousInputFieldForRequiredValue(editText);
				if (previous != null) {
					previous.requestFocus();
				}
			}
		}
	}

	private class SMSVerifySMSChallengeRequest extends VerifySMSChallengeRequest {

		@Override
		public void onResponse(VerifySMSChallengeResponse response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Verify SMS Challenge Response Request Succeeded");
			}

			if (!response.getSmsValid()) {
				handleError(getErrorBuilder().buildErrorResponse(InAppError.HttpStatus200));
			} else {
				onDone();
			}
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Verify SMS Challenge Response Request Failed: " + errorResponseWrapper.getErrorResponse().getErrorCode());
			}
			retryErrorResponse = errorResponseWrapper.getErrorResponse();
			handleError(errorResponseWrapper);
		}
	}

	private class SMSGetSMSChallengeRequest extends GetSMSChallengeRequest {

		@Override
		public void onResponse(GetSMSChallengeResponse response) {
			if (!response.getSmsIssued()) {
				handleError(getErrorBuilder().buildErrorResponse(InAppError.HttpStatus200));
			} else {
				smsThrottler.isThrottled(true);
				showProgress(false);
				resetFields();
			}
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponse) {
			showProgress(false);
			handleError(errorResponse);
		}
	}
}
