package com.tescobank.mobile.ui.auth.passcode;

import java.util.Arrays;

import android.widget.EditText;

import com.tescobank.mobile.ui.TescoEditText;
import com.tescobank.mobile.ui.auth.AuthDataHelper;

/**
 * Passcode data helper; handles null / bad data by ignoring it
 */
public class PasscodeDataHelper extends AuthDataHelper {

	private static final int FIVE = 5;
	private static final int FOUR = 4;
	private static final int THREE = 3;
	private static final int TWO = 2;
	private static final int ONE = 1;

	public PasscodeDataHelper(TescoEditText field1, TescoEditText field2, TescoEditText field3, TescoEditText field4, TescoEditText field5) {
		super(new Integer[] { ONE, TWO, THREE, FOUR, FIVE }, Arrays.asList(field1, field2, field3, field4, field5));
	}

	public String getPasscode() {
		StringBuilder builder = new StringBuilder();
		for (EditText text : getInputFieldsForRequiredValues()) {
			builder.append(text.getText().toString());
		}
		return builder.toString();
	}

	/**
	 * @param previouslyEnteredPasscode
	 *            the previously entered passcode to verify matches what has been currently entered
	 * @return true if the current passcode matches the previously entered passcode
	 */
	public boolean passcodeMatches(String previouslyEnteredPasscode) {
		return getPasscode().equals(previouslyEnteredPasscode);
	}

	/**
	 * TODO check the validity of the current passcode
	 * 
	 * @return currently always returns true
	 */
	public boolean isValid() {
		return PasscodeValidator.isValid(getPasscode());
	}

	public void resetInputFields() {
		for (EditText text : getInputFieldsForRequiredValues()) {
			text.setText("");
		}
	}

	public void setInputFieldsEnabled(boolean enabled) {
		for (EditText field : getInputFieldsForRequiredValues()) {
			field.setEnabled(enabled);
		}
	}

}
