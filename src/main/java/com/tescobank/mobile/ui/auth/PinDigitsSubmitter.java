package com.tescobank.mobile.ui.auth;

/**
 * A PIN digits submitter
 */
public interface PinDigitsSubmitter {
	
	/**
	 * Submits the current PIN digits
	 */
	void submitPinDigits();
	
	/**
	 * Responds to completion of the verify challenge response request
	 * 
	 * @param encryptedToken the encrypted token
	 */
	void onVerifyChallengeResponseComplete(String encryptedToken);
}
