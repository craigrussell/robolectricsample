package com.tescobank.mobile.ui.auth;

import static java.lang.Integer.parseInt;

import java.util.Date;
import java.util.Map;

import com.tescobank.mobile.persist.Persister;

public class SMSThrottler {

	public static final String SMS_TIMEOUT = "smsResentTimeOut";
	public static final String SMS_RESENT_MAX_COUNT = "smsResentMaxCount";
	private static final Integer DEFAULT_TIME_OUT_IN_MILLIS = 300000;
	private static final Integer DEFAULT_RESENT_MAX_COUNT = 3;
	private static final int SECOND = 1000;
	
	private final int smsTimeOut;
	private final int smsResentMaxCount;
	private Persister sharedPreferencesPersister;

	public SMSThrottler(Map<String,String> configuration, Persister sharedPreferences) {
		String configTimeOutAsString = configuration.get(SMS_TIMEOUT);
		String configMaxCount = configuration.get(SMS_RESENT_MAX_COUNT);
		this.smsTimeOut = configTimeOutAsString == null ? DEFAULT_TIME_OUT_IN_MILLIS : parseInt(configTimeOutAsString) * SECOND;
		this.smsResentMaxCount = configMaxCount == null ? DEFAULT_RESENT_MAX_COUNT : parseInt(configMaxCount);
		this.sharedPreferencesPersister = sharedPreferences;
	}

	public boolean isThrottled(boolean incrementCount) {

		long initialSmsRequestTime = sharedPreferencesPersister.getInitialSmsRequestTime();
		long now = new Date().getTime();

		if (now - initialSmsRequestTime > smsTimeOut) {
			sharedPreferencesPersister.persistSmsRequestCount(1);
			sharedPreferencesPersister.persistInitialSmsRequestTime(now);
			return false;
		}
		if (sharedPreferencesPersister.getSmsRequestCount() >= smsResentMaxCount) {
			return true;
		}
		
		if (incrementCount) {
			addToCount();
		}
		return false;
	}

	private void addToCount() {
		int attemptCount = sharedPreferencesPersister.getSmsRequestCount();
		sharedPreferencesPersister.persistSmsRequestCount(attemptCount + 1);
	}
	
	public void resetCount() {
		sharedPreferencesPersister.persistSmsRequestCount(0);
		sharedPreferencesPersister.persistInitialSmsRequestTime(0);
	}

}
