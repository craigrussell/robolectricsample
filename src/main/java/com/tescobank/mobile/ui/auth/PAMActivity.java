package com.tescobank.mobile.ui.auth;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.authentication.GetPinChallengeRequest;
import com.tescobank.mobile.api.authentication.GetPinChallengeResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.TescoActivity;

/**
 * Activity for viewing and accepting a PAM
 */
public class PAMActivity extends TescoActivity {

	private static final String TAG = PAMActivity.class.getSimpleName();

	public static final String IMAGE_MEDIA_TYPE = "image/jpeg";
	public static final String PAM_IMAGE = "PAM_IMAGE";
	public static final String PAM_PHRASE = "PAM_PHRASE";
	
	private ImageView pamImageView;
	private TextView textView;
	private InProgressIndicator inProgressIndicator;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating PAM Activity");
		}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auth_registration_pam);
		pamImageView = (ImageView)this.findViewById(R.id.imageView1);
		textView = (TextView)this.findViewById(R.id.pamText); 
		inProgressIndicator = new InProgressIndicator(this, true);
	}
	
	@Override
	protected void onStart() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting PAM Activity");
		}
		Bitmap image = getFlow().getDataBundle().getParcelable(PAM_IMAGE);
		pamImageView.setImageBitmap(image);
		textView.setText(getFlow().getDataBundle().getString(PAM_PHRASE));
		trackAnalytic();
		super.onStart();
	}
	
	private void trackAnalytic() {
		getApplicationState().getAnalyticsTracker().trackEvent(getFlow().getDisplayAnalytic());
	}
	
	public void onContinuePressed(View view) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Continue Pressed");
		}
		
		onNextPressed();
	}
	
	private void onNextPressed() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Get PIN Challenge Request");
		}
		
		inProgressIndicator.showInProgressIndicator();
		
		GetPinChallengeRequest request = new GetPinChallengeRequest() {

			@Override
			public void onResponse(GetPinChallengeResponse response) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "Get PIN Challenge Request Succeeded");
				}
				startPinDigitsActivity(response);
			}

			@Override
			public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "Get PIN Challenge Request Failed: " + errorResponseWrapper.getErrorResponse().getErrorCode());
				}
				inProgressIndicator.hideInProgressIndicator();
				handleErrorResponse(errorResponseWrapper);
			}
		};
		request.setCssoId(getApplicationState().getCssoId());
		getApplicationState().getRestRequestProcessor().processRequest(request);
	}
	
	@Override
	protected void onRetryPressed() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Retry Pressed");
		}
		// Do nothing
	}

	private void startPinDigitsActivity(GetPinChallengeResponse response) {
		getFlow().getDataBundle().putSerializable(GetPinChallengeResponse.class.getName(), response);
		getFlow().onPolicyComplete(this);
	}
}
