package com.tescobank.mobile.ui.auth.passcode;

import java.text.StringCharacterIterator;
import java.util.Arrays;

public final class PasscodeValidator {
	
	private static final int REPETITION_LENGTH = 3;
	private static final int VALID_PASSCODE_LENGTH = 5;
	private static final String[] ILLEGAL_SEQUENCES = new String[] { "13579", "12321", "90210", "38317", "42069" };
	
	private PasscodeValidator() {
		super();
	}

	public static boolean isValid(String passcode) {
		return isLengthValid(passcode) && isSequenceValid(passcode) && isRepetitionValid(passcode);
	}
	
	private static boolean isLengthValid(String passcode) {
		return passcode.length() == VALID_PASSCODE_LENGTH;
	}
	
	private static boolean isSequenceValid(String passcode) {
		return !containsIllegalSequence(passcode) && !containsSequence(passcode) && !containsBackwardsSequence(passcode);
	}
	
	private static boolean isRepetitionValid(String passcode) {
		return !containsRepetition(passcode, REPETITION_LENGTH);
	}

	private static boolean containsIllegalSequence(String passcode) {
		return Arrays.asList(ILLEGAL_SEQUENCES).contains(passcode);
	}

	private static boolean containsSequence(String str) {
		StringCharacterIterator charIterator = new StringCharacterIterator(str);
		char previous = charIterator.first();
		char next = charIterator.next();
		while (next != StringCharacterIterator.DONE) {
			if (!isSequentialNumber(previous, next)) {
				return false;
			}
			previous = next;
			next = charIterator.next();
		}
		return true;
	}

	private static boolean containsBackwardsSequence(String str) {
		return containsSequence(new StringBuffer(str).reverse().toString());
	}

	private static boolean isSequentialNumber(char first, char second) {
		if (second == first + 1) {
			return true;
		}
		if (first == '9' && second == '0') {
			return true;
		}
		return false;
	}

	private static boolean containsRepetition(String passcode, int maxReps) {
		char[] chars = passcode.toCharArray();
		for (int i = 0; i < chars.length - 1; i++) {
			int count = getCount(maxReps, chars, i);
			if (count >= maxReps) {
				return true;
			}
		}
		return false;
	}
	
	private static int getCount(int maxReps, char[] chars, int i) {
		int count = 1;
		for (int j = i + 1; j < chars.length && count < maxReps; j++) {
			if (chars[j] == chars[i]) {
				count++;
			}
		}
		return count;
	}
}
