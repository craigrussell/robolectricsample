package com.tescobank.mobile.ui.auth.passcode;

/**
 * A passcode submitter
 */
public interface PasscodeSubmitter {
	
	/**
	 * Submits the current passcode
	 */
	void submitPasscode();
}
