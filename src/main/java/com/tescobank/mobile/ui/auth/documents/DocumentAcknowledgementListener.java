package com.tescobank.mobile.ui.auth.documents;

/**
 * Document acknowledgement listener
 */
public interface DocumentAcknowledgementListener {
	
	/**
	 * Responds to completion of all required document acknowledgements
	 */
	void onDocumentAcknowledgementComplete();
}
