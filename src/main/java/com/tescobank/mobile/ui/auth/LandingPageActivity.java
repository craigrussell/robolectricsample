package com.tescobank.mobile.ui.auth;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.TescoActivity;

public class LandingPageActivity extends TescoActivity {

	public static final String LANDING_PAGE_URL = LandingPageActivity.class.getName() + ".landingPageUrl";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_landing_page);
	}

	public void onClickSignup(View view) {

		String landingPageUrl = getIntent().getStringExtra(LANDING_PAGE_URL);
		if (landingPageUrl == null) {
			landingPageUrl = getString(R.string.landing_page_url);
		}

		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(landingPageUrl));
		startActivity(i);
		ActivityCompat.finishAffinity(this);
	}

	public void onClickRegister(View view) {
		finish();
	}
}
