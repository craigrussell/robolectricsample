package com.tescobank.mobile.ui.auth.passcode;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.TescoActivity;

public class PasscodeAnimator {

	private final Animation exitRightThenEnterRightAnimation;
	private Animation exitLeftThenEnterRightAnimation;
	private View container;

	/**
	 * 
	 * @param activity
	 *            the activity that contains an <code>R.id.common_passcode_data</code> view
	 * @param passcodeActivityHelper
	 *            the passcode activity helper so the entries can be reset
	 * @param initialMessageResId
	 *            the first message to show when asking for a passcode
	 * @param confirmationMessageResId
	 *            the message to show when asking for confirmation
	 */
	public PasscodeAnimator(final TescoActivity activity, final PasscodeActivityHelper passcodeActivityHelper, final int initialMessageResId, final int confirmationMessageResId) {

		container = activity.findViewById(R.id.common_passcode_data);
		final TextView message = TextView.class.cast(activity.findViewById(R.id.passcode_message));

		final Animation enterRightAnimation = AnimationUtils.loadAnimation(activity, R.anim.anim_enter_right);
		exitRightThenEnterRightAnimation = AnimationUtils.loadAnimation(activity, R.anim.anim_exit_right);
		exitRightThenEnterRightAnimation.setAnimationListener(new DefaultAnimationListener() {
			@Override
			public void onAnimationEnd(Animation animation) {
				container.startAnimation(enterRightAnimation);

				passcodeActivityHelper.reset();
				message.setText(initialMessageResId);
				activity.showKeyboard();
			}
		});

		exitLeftThenEnterRightAnimation = AnimationUtils.loadAnimation(activity, R.anim.anim_exit_left);
		exitLeftThenEnterRightAnimation.setAnimationListener(new DefaultAnimationListener() {
			@Override
			public void onAnimationEnd(Animation animation) {
				container.startAnimation(enterRightAnimation);

				passcodeActivityHelper.reset();
				message.setText(confirmationMessageResId);
				activity.showKeyboard();
			}
		});

	}

	public void showConfirmation() {
		container.startAnimation(exitLeftThenEnterRightAnimation);
	}

	public void startAgain() {
		container.startAnimation(exitRightThenEnterRightAnimation);
	}

	/**
	 * Default implementation that does nothing to keep code above a bit tidier. Oh, where art though lambdas!
	 */
	static class DefaultAnimationListener implements AnimationListener {

		@Override
		public void onAnimationEnd(Animation animation) {
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
		}

		@Override
		public void onAnimationStart(Animation animation) {
		}
	}

}
