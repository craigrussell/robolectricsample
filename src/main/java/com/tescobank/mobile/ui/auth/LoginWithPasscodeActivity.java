package com.tescobank.mobile.ui.auth;

import static com.tescobank.mobile.api.audit.AuditRequest.OperationName.SUBSEQUENT_LOGIN_PASSCODE;
import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_BACK_ON_PASSWORD_CHANGED_ONLINE;

import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.authentication.AuthenticateOTPRequest;
import com.tescobank.mobile.api.authentication.AuthenticateOTPResponse;
import com.tescobank.mobile.api.authentication.GetPinChallengeRequest;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.rest.processor.RestRequestProcessor;
import com.tescobank.mobile.security.OTPGenerationListener;
import com.tescobank.mobile.security.SecurityProvider;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.InvalidValueIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoModal;
import com.tescobank.mobile.ui.auth.passcode.PasscodeActivityHelper;
import com.tescobank.mobile.ui.auth.passcode.PasscodeDataHelper;
import com.tescobank.mobile.ui.auth.passcode.PasscodeSubmitter;
import com.tescobank.mobile.ui.balancepeek.BalancePeekListRowBuilder;
import com.tescobank.mobile.ui.balancepeek.BalancePeekPanelPositioner;
import com.tescobank.mobile.ui.balancepeek.BalancePeekSlidingPanelTouchListener;
import com.tescobank.mobile.ui.balancepeek.BalancePeekViewAnimator;
import com.tescobank.mobile.ui.balancepeek.SlidingPanelPositioner;
import com.tescobank.mobile.ui.balancepeek.SlidingPanelUIWrapper;
import com.tescobank.mobile.ui.errors.ErrorHandler;
import com.tescobank.mobile.ui.settings.UnauthenticatedSettingsFragment;

public class LoginWithPasscodeActivity extends TescoActivity implements PasscodeSubmitter, OTPGenerationListener, ErrorHandler {

	private static final String TAG = LoginWithPasscodeActivity.class.getSimpleName();

	private static final String BUNDLE_KEY_RESET_PASSCODE_DIALOG_SHOWING = "BUNDLE_KEY_RESET_PASSCODE_DIALOG_SHOWING";
	private static final String BUNDLE_KEY_HELP_DIALOG_SHOWING = "BUNDLE_KEY_HELP_DIALOG_SHOWING";
	private static final String PAM_IMAGE = "PAM_IMAGE";
	private static final String PAM_PHRASE = "PAM_PHRASE";
	private static final int ADJUST_BY_ONE = 1;

	private static long INACTIVITY_DEFAULT_TIMEOUT = 5000l;

	private ApplicationState applicationState;
	private SecurityProvider securityProvider;
	private RestRequestProcessor<ErrorResponseWrapper> requestProcessor;
	private InProgressIndicator inProgressIndicator;
	private PasscodeActivityHelper passcodeActivityHelper;
	private ImageView pamImageView;
	private TextView pamPhraseTextView;
	private UnauthenticatedSettingsFragment unauthenticatedSettingsFragment;
	private TescoModal modal;

	private float origin;
	private float originOffset;
	private ViewGroup panelTop;
	private ViewGroup panelBottom;
	private ViewGroup accountLayoutContainer;
	private ViewGroup root;
	private BalancePeekSlidingPanelTouchListener balancePeekSlidingPanelTouchListener;
	private LinearLayout balancePeekAccountList;
	private ImageView dragButton;
	private boolean calculatedBalancePeekValues;
	private ImageView trapDoorTop;
	private ImageView trapDoorBottom;
	private int balancePeekAnimationDuration;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating Passcode Activity");
		}
		unauthenticatedSettingsFragment = new UnauthenticatedSettingsFragment();
		getDrawer().setContentViewWithDrawer(R.layout.activity_auth_login_with_passcode, unauthenticatedSettingsFragment);

		applicationState = getApplicationState();
		securityProvider = applicationState.getSecurityProvider();
		requestProcessor = applicationState.getRestRequestProcessor();

		pamImageView = (ImageView) this.findViewById(R.id.imageView1);
		pamPhraseTextView = (TextView) this.findViewById(R.id.pamText);
		unauthenticatedSettingsFragment.initialise(this, inProgressIndicator);
		inProgressIndicator = new InProgressIndicator(this, false);
		setBalancePeekViewReferences();
		hideBalancePeekUIComponents();
		configureBalancePeek();
		restoreSavedState(savedInstanceState);
	}


	private void configureBalancePeek() {
		balancePeekAnimationDuration = BalancePeekViewAnimator.DEFAULT_FADE_ANIMATION_DURATION;
		boolean isNonInteractiveProductsEnabled = getApplicationState().getFeatures().isNonInteractiveProductsEnabled();
		boolean isBalancePeekEnabled = getApplicationState().getPersister().isBalancePeekEnabled();
		List<ProductDetails> products = getApplicationState().getProductDetails();
		BalancePeekListRowBuilder builder = new BalancePeekListRowBuilder(getResources(), getLayoutInflater(), products, R.layout.balance_peek_list_row, isNonInteractiveProductsEnabled,
				isBalancePeekEnabled);
		for (int i = 0; i < builder.getCount(); i++) {
			View view = builder.createView(i);
			balancePeekAccountList.addView(view);
		}
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		balancePeekSlidingPanelTouchListener.userInteractionOccurred();
		return super.dispatchTouchEvent(event);
	}

	private void setBalancePeekViewReferences() {
		balancePeekAccountList = LinearLayout.class.cast(findViewById(R.id.balance_peek_list));
		panelTop = ViewGroup.class.cast(findViewById(R.id.topPanel));
		panelBottom = ViewGroup.class.cast(findViewById(R.id.bottomPanel));
		accountLayoutContainer = ViewGroup.class.cast(findViewById(R.id.balancePeekContainer));
		balancePeekAccountList = LinearLayout.class.cast(findViewById(R.id.balance_peek_list));
		root = ViewGroup.class.cast(findViewById(android.R.id.content));
		dragButton = ImageView.class.cast(findViewById(R.id.dragButton));
		trapDoorTop = ImageView.class.cast(findViewById(R.id.trapdoorTopImage));
		trapDoorBottom = ImageView.class.cast(findViewById(R.id.bottomPanelImage));
	}

	private void restoreSavedState(Bundle savedState) {
		if (savedState == null) {
			return;
		}
		if (savedState.getBoolean(BUNDLE_KEY_RESET_PASSCODE_DIALOG_SHOWING)) {
			unauthenticatedSettingsFragment.showConfirmResetPasscodeRequiredDialog();
		}
		if (savedState.getBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING)) {
			showHelpDialog();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (modal != null) {
			outState.putBoolean(BUNDLE_KEY_HELP_DIALOG_SHOWING, modal.isShowing());
		}
		outState.putBoolean(BUNDLE_KEY_RESET_PASSCODE_DIALOG_SHOWING, unauthenticatedSettingsFragment.isResetPasscodeWarningDialogShowing());
	}

	@Override
	protected void onStart() {
		extractIntentExtras();
		trackAnalytic();
		super.onStart();
	}

	private void extractIntentExtras() {
		Bitmap image = getFlow().getDataBundle().getParcelable(PAM_IMAGE);
		pamImageView.setImageBitmap(image);
		pamPhraseTextView.setText(getFlow().getDataBundle().getString(PAM_PHRASE));
	}

	@Override
	protected void onDestroy() {
		unauthenticatedSettingsFragment.dismissResetPasscodeWarningDialog();
		if (null != modal) {
			modal.dismiss();
		}
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onCreateOptionsMenu");
		}
		getSupportMenuInflater().inflate(R.menu.passcode, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_item_information:
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "showing info box");
			}
			closeDrawer();
			showHelpDialog();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void showHelpDialog() {
		modal = new TescoModal(this);
		modal.setHeadingText(getString(R.string.auth_passcode_entry_modal_header));
		modal.setMessageText(getString(R.string.auth_passcode_entry_modal_message));
		modal.show();
	}

	protected UnauthenticatedSettingsFragment getUnauthenticatedSettingsFragment() {
		return unauthenticatedSettingsFragment;
	}

	private void trackAnalytic() {
		getApplicationState().getAnalyticsTracker().trackEvent(getFlow().getDisplayAnalytic());
	}

	@Override
	protected void onPause() {
		super.onPause();
		securityProvider.cancelOTPGeneration();
		balancePeekSlidingPanelTouchListener.closePanels(BalancePeekSlidingPanelTouchListener.ANIMATION_DURATION_NONE);
	}

	@Override
	protected void onResume() {
		getTooltipManager().show();
		super.onResume();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (hasFocus) {
			if (!calculatedBalancePeekValues) {
				calculateBalancePeekLayoutValues();
			}
		}
		super.onWindowFocusChanged(hasFocus);
	}

	private void calculateBalancePeekLayoutValues() {
		if (BuildConfig.DEBUG) {
			Log.i(TAG, "calculating balance peek layout values");
		}

		calculateOriginOffset();
		calculateOrigin();

		float bottomOfWindow = panelBottom.getY() + panelBottom.getHeight();
		int accountActualHeight = balancePeekAccountList.getHeight();
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "accountActualHeight: " + accountActualHeight);
		}
		if (accountActualHeight < bottomOfWindow) {
			positionAccountListOnCentre(bottomOfWindow);
		}

		initialiseSlidingPanel();
		showBalancePeekUIComponents();
		calculatedBalancePeekValues = true;
	}

	private void hideBalancePeekUIComponents() {
		float noAlpha = 0f;
		accountLayoutContainer.setAlpha(noAlpha);
		trapDoorTop.setAlpha(noAlpha);
		trapDoorBottom.setAlpha(noAlpha);
		dragButton.setAlpha(noAlpha);
	}

	private void showBalancePeekUIComponents() {
		BalancePeekViewAnimator viewAnimator = new BalancePeekViewAnimator();
		viewAnimator.addTransitionView(dragButton);
		viewAnimator.addTransitionView(trapDoorTop);
		viewAnimator.addTransitionView(trapDoorBottom);
		viewAnimator.addPostTransitionView(accountLayoutContainer);
		viewAnimator.showComponents(balancePeekAnimationDuration);
	}

	private void positionAccountListOnCentre(float bottomOfWindow) {
		float y = origin - (accountLayoutContainer.getHeight() / 2);
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "account layout height: " + accountLayoutContainer.getHeight());
		}
		accountLayoutContainer.setY(y);

		if (accountLayoutContainer.getY() < 0) {
			if (BuildConfig.DEBUG) {
				Log.i(TAG, "Account list off screen above allowed position, moving to panelAYLimit");
			}
			accountLayoutContainer.setY(0);
		} else if (accountLayoutContainer.getY() + accountLayoutContainer.getHeight() > bottomOfWindow) {
			if (BuildConfig.DEBUG) {
				Log.i(TAG, "Account list off screen below allowed position, moving so that bottom is equal to panelBYLimit");
			}
			accountLayoutContainer.setY(bottomOfWindow - accountLayoutContainer.getHeight());
		} else {
			if (BuildConfig.DEBUG) {
				Log.i(TAG, "Account list not requiring moving");
			}
		}
	}

	private void calculateOrigin() {
		origin = panelTop.getY() + panelTop.getHeight() - originOffset;
		if (BuildConfig.DEBUG) {
			Log.i(TAG, String.format("Origin is %f", origin));
		}
	}

	private void calculateOriginOffset() {
		originOffset = (trapDoorTop.getHeight() / 2.0f) + ADJUST_BY_ONE;
	}

	private void initialiseSlidingPanel() {
		SlidingPanelUIWrapper panelUIWrapper = new SlidingPanelUIWrapper(panelTop, panelBottom, accountLayoutContainer, root);
		SlidingPanelPositioner panelPositioner = new BalancePeekPanelPositioner(panelUIWrapper, origin, originOffset);
		balancePeekSlidingPanelTouchListener = BalancePeekSlidingPanelTouchListener.create(this, panelUIWrapper, panelPositioner, origin, originOffset);
		balancePeekSlidingPanelTouchListener.configureInactivityTimeout(this, INACTIVITY_DEFAULT_TIMEOUT);
		balancePeekSlidingPanelTouchListener.setDraggingIndicator(dragButton);
		panelBottom.setOnTouchListener(balancePeekSlidingPanelTouchListener);
		panelBottom.setY(origin - originOffset);

		OnTouchListener closePanelsIfOpenedListener = new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
					if (balancePeekEnabledAndShowing()) {
						balancePeekSlidingPanelTouchListener.closePanels(BalancePeekSlidingPanelTouchListener.ANIMATION_DURATION_DEFAULT);
						return true;
					}
				}
				return false;
			}

			private boolean balancePeekEnabledAndShowing() {
				return balancePeekSlidingPanelTouchListener != null && balancePeekSlidingPanelTouchListener.panelsAreOpen();
			}
		};
		panelTop.setOnTouchListener(closePanelsIfOpenedListener);

		InvalidValueIndicator invalidValueIndicator = new InvalidValueIndicator(this);
		passcodeActivityHelper = new PasscodeActivityHelper(this, this, invalidValueIndicator, balancePeekSlidingPanelTouchListener);
	}

	@Override
	public boolean backFromRetryErrorShouldRetry() {
		return false;
	}

	@Override
	protected void onRetryPressed() {
		if (PasscodeAuthenticateOTPRequest.class.equals(getApplicationState().getRestRequestProcessor().getLastRequestTypeForRequesterType(getClass()))
				|| GetPinChallengeRequest.class.isAssignableFrom(getLastRequestType())) {
			returnToEnterPasscode();
		} else {
			getFlow().onPolicyComplete(this);
		}
	}

	private Class<?> getLastRequestType() {
		RestRequestProcessor<ErrorResponseWrapper> processor = getApplicationState().getRestRequestProcessor();
		return processor.getLastRequestTypeForRequesterType(getClass());
	}

	@Override
	protected void onBackFromRetryPressed() {
		returnToEnterPasscode();
	}

	private void returnToEnterPasscode() {
		inProgressIndicator.hideInProgressIndicator();
		passcodeActivityHelper.handleRetry();
		passcodeActivityHelper.getPasscodeDataHelper().setInputFieldsEnabled(true);
	}

	@Override
	protected void onRetryForPassiveError() {
		passcodeActivityHelper.handleRetryForPassiveError();
	}

	@Override
	public void handleError(ErrorResponseWrapper errorResponseWrapper) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Error");
		}

		handleErrorResponse(errorResponseWrapper);
		hideKeyboard();
	}

	@Override
	public void submitPasscode() {
		// Check that we have a passcode
		if (!getPasscodeDataHelper().allRequiredValuesProvided()) {
			handleError(getErrorBuilder().buildErrorResponse(InAppError.IncompletePasscode));
			return;
		}

		inProgressIndicator.showInProgressIndicator();

		// Generate an OTP
		generateOTP();
	}

	private void generateOTP() {
		securityProvider.generateOTP(getPasscodeDataHelper().getPasscode(), this);
	}

	@Override
	public void otpGenerated(String otp) {
		if (otp == null) {
			handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.GenerateOTP));

		} else {
			startAuthenticateOTPRequest(otp);
		}
	}

	private void startAuthenticateOTPRequest(String otp) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Authenticate OTP Request");
		}

		AuthenticateOTPRequest request = new PasscodeAuthenticateOTPRequest();
		request.setTbId(applicationState.getTbId());
		request.setDeviceId(applicationState.getDeviceId());
		request.setOtp(otp);

		requestProcessor.processRequest(request, getClass());
	}

	private PasscodeDataHelper getPasscodeDataHelper() {
		return passcodeActivityHelper.getPasscodeDataHelper();
	}

	public class PasscodeAuthenticateOTPRequest extends AuthenticateOTPRequest {

		@Override
		public void onResponse(AuthenticateOTPResponse response) {
			if (!response.getAuthenticated()) {
				returnToEnterPasscode();
				handleError(getErrorBuilder().buildErrorResponse(InAppError.HttpStatus200));
				return;
			}

			securityProvider.lastOTPWasSuccessful();
			audit(SUBSEQUENT_LOGIN_PASSCODE, null);
			getFlow().onPolicyComplete(LoginWithPasscodeActivity.this);

		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Authenticate OTP Request Failed: " + errorResponseWrapper.getErrorResponse().getErrorCode());
			}
			returnToEnterPasscode();
			handleError(errorResponseWrapper);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_BACK_ON_PASSWORD_CHANGED_ONLINE) {
			passcodeActivityHelper.handleRetry();
			return;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void setBalancePeekAnimationDuration(int balancePeekAnimationDuration) {
		this.balancePeekAnimationDuration = balancePeekAnimationDuration;
	}
}
