package com.tescobank.mobile.ui;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.tescobank.mobile.R;

/**
 * Used to provide a visual indication that values are missing / invalid
 */
public class InvalidValueIndicator {
	
	/** The underlying animation to be applied */
	private Animation jiggleAnimation;
	
	/**
	 * Constructor
	 * 
	 * @param context the context
	 */
	public InvalidValueIndicator(Context context) {
		super();
		
		jiggleAnimation = AnimationUtils.loadAnimation(context, R.anim.jiggle);
	}
	
	/**
	 * Provides a visual indication that the value is missing / invalid for the specified view
	 * 
	 * @param view the view
	 */
	public void indicateMissingValue(View view) {
		view.startAnimation(jiggleAnimation);
	}
	
	/**
	 * Provides a visual indication that values are missing / invalid for the specified views
	 * 
	 * @param views the views
	 */
	public <T extends View> void indicateMissingValue(List<T> views) {
		for (T view : views) {
			indicateMissingValue(view);
		}
	}
}
