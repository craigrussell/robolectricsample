package com.tescobank.mobile.ui;

import android.view.View;
import android.widget.Button;

import com.tescobank.mobile.R;

/**
 * Provides support for showing / hiding the option to continue and providing a visual indication that an operation is in progress
 */
public class InProgressIndicator extends HideAndShowProgressIndicator {

	/** The activity */
	private TescoActivity activity;

	/** The layout */
	private View layout;

	/** Whether the option to continue should be provided */
	private boolean includeContinue;

	/** The underlying continue button */
	private Button continueButton;

	/** Whether we are currently displaying the in progress indicator */
	private boolean showingInProgress;

	/**
	 * Constructor
	 * 
	 * @param activity
	 *            the activity
	 * @param includeContinue
	 *            whether the option to continue should be provided
	 */
	public InProgressIndicator(TescoActivity activity, boolean includeContinue) {
		super(activity);

		this.activity = activity;
		this.layout = activity.findViewById(R.id.progressbutton);
		this.includeContinue = includeContinue;

		// Configure the continue button, if any
		continueButton = (Button) activity.findViewById(R.id.continueButton);
		setIncludeContinue(includeContinue);

		// Note whether we are currently displaying the in progress indicator
		showingInProgress = (getSpinner().getVisibility() == View.VISIBLE);
	}

	/**
	 * Configures the option to continue
	 * 
	 * @param includeContinue
	 *            whether the option to continue should be provided
	 */
	public final void setIncludeContinue(boolean includeContinue) {

		this.includeContinue = includeContinue;

		if (continueButton != null) {
			if (includeContinue) {
				continueButton.setEnabled(true);
			} else {
				continueButton.setVisibility(View.INVISIBLE);
			}
		}
	}

	/**
	 * Configure the continue button with a message id, background drawable, etc
	 * 
	 * @param config
	 *            the config for the button
	 */
	public final void updateContinueButtonConfiguration(ContinueButtonConfiguration config) {
		if (continueButton != null) {
			continueButton.setText(config.getMessageId());
			continueButton.setBackgroundResource(config.getBackgroundDrawable());
			continueButton.setTextColor(activity.getResources().getColorStateList(config.getTextColors()));
		}
	}

	/**
	 * Sets whether the option to continue is enabled
	 * 
	 * @param enabled
	 *            whether the option to continue is enabled
	 */
	public void setContinueEnabled(boolean enabled) {
		if (continueButton != null) {
			continueButton.setEnabled(enabled);
		}
	}

	/**
	 * Provides a visual indication that an operation is in progress
	 */
	public void showInProgressIndicator() {

		// Hide the continue button, if any
		if (includeContinue && (continueButton != null)) {
			continueButton.setVisibility(View.INVISIBLE);
		}

		// Show the in progress indicator
		getSpinner().setVisibility(View.VISIBLE);
		getSpinnerAnimation().start();

		// Block user interaction
		if (!showingInProgress) {
			
			//lock the screenOrientation
			activity.lockScreenOrientation();
			activity.setUserInteractionBlocked(true);
		}

		// Note that we are currently displaying the in progress indicator
		showingInProgress = true;
	}

	/**
	 * Hides the visual indication that an operation is in progress
	 */
	public void hideInProgressIndicator() {

		// Hide the in progress indicator
		getSpinner().setVisibility(View.INVISIBLE);

		// Show the continue button, if any
		if (includeContinue && (continueButton != null)) {
			continueButton.setVisibility(View.VISIBLE);
		}

		// Allow user interaction
		if (showingInProgress) {
			//unlock the screenOrientation
			activity.unlockScreenOrientation();
			activity.setUserInteractionBlocked(false);
		}
		
		// Note that we aren't currently displaying the in progress indicator
		showingInProgress = false;
	}

	/**
	 * Removes the visual indication that an operation is in progress
	 */
	public void removeInProgressIndicator() {
		if (layout != null) {
			layout.setVisibility(View.GONE);
		}
	}

	/**
	 * Shows the option to continue, if provided (hides the visual indication that an operation is in progress)
	 */
	public void showContinue() {
		hideInProgressIndicator();
	}

	/**
	 * Hides both the visual indication that an operation is in progress and the option to continue, if any
	 */
	public void hideAll() {

		// Hide the in progress indicator, if any
		if (getSpinner() != null) {
			getSpinner().setVisibility(View.INVISIBLE);
		}

		// Hide the continue button, if any
		if (continueButton != null) {
			continueButton.setVisibility(View.INVISIBLE);
		}

		// Block user interaction
		activity.setUserInteractionBlocked(true);

		// Note that we aren't currently displaying the in progress indicator
		showingInProgress = false;
	}

	/**
	 * @return the showingInProgress
	 */
	public boolean isShowingInProgress() {
		return showingInProgress;
	}

	public interface ContinueButtonConfiguration {

		int getMessageId();

		int getBackgroundDrawable();
		
		int getTextColors();
	}
}
