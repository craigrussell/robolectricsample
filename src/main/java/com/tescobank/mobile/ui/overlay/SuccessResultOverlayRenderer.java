package com.tescobank.mobile.ui.overlay;

import android.view.View;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.Display;
import com.tescobank.mobile.ui.TescoActivity;

/**
 * Renders success result overlay
 */
public class SuccessResultOverlayRenderer extends ResultOverlayRenderer {

	private Display display;
	
	public SuccessResultOverlayRenderer(TescoActivity activity, Display display) {
		super(activity);
		this.display = display;
	}

	public void renderOverlay(OnDismissListener listener) {
		View overlay = getActivity().getLayoutInflater().inflate(R.layout.result_overlay, null);
		TextView title = (TextView) overlay.findViewById(R.id.result_title);
		TextView message = (TextView) overlay.findViewById(R.id.result_message);
		title.setText(display.getTitle());		
		message.setText(display.getMessage());
		renderOverlay(overlay, true, listener);
	}
}
