package com.tescobank.mobile.ui.overlay;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.PopupWindow.OnDismissListener;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.ui.TescoPopupWindow;

/**
 * Renders result overlays
 */
public class ResultOverlayRenderer {
	
	private static final int OVERLAY_TIMEOUT_MS = 5000;
	
	private Activity activity;
	
	private DataFormatter dataFormatter;
	
	private TescoPopupWindow overlayWindow;
	
	private AsyncTask<Void, Void, Void> overlayWaitTask;
	
	/**
	 * Constructor
	 * 
	 * @param activity the activity
	 */
	public ResultOverlayRenderer(Activity activity) {
		super();
		
		this.activity = activity;
		this.dataFormatter = new DataFormatter();
	}
	
	/**
	 * Dismisses the overlay, if any
	 * 
	 * @return whether an overlay was dismissed
	 */
	public boolean dismissOverlay() {
		boolean dismissOverlay = isDialogShowing();
		if(dismissOverlay) {
			overlayWindow.dismiss();
		}
		overlayWindow = null;
		overlayWaitTask = null;
		return dismissOverlay;
	}

	private boolean isDialogShowing() {
		return overlayWindow != null && overlayWindow.isShowing();
	}
	
	/**
	 * Cancels any tasks
	 */
	public void cancelTasks() {
		if (overlayWaitTask != null) {
			overlayWaitTask.cancel(true);
		}
	}
	
	protected Activity getActivity() {
		return activity;
	}
	
	protected DataFormatter getDataFormatter() {
		return dataFormatter;
	}
	
	protected void renderOverlay(View overlay, boolean autoDismiss, OnDismissListener onDismissListener) {
		
		attachClickListener(overlay);
		
		overlayWindow = new TescoPopupWindow(activity, overlay);
		overlayWindow.setAnimationStyle(R.style.fadeOutOnlyAnimation);
		overlayWindow.setOnDismissListener(onDismissListener);
		overlayWindow.showFullScreen();
		
		if (autoDismiss) {
			overlayWaitTask = new OverlayWaitTask().execute();
		}
	}
	
	private void attachClickListener(View overlay) {
		overlay.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View view) {
				dismissOverlay();
			}
		});
	}
	
	private class OverlayWaitTask extends AsyncTask<Void, Void, Void> {
		
		@Override
		protected Void doInBackground(Void... params) {
			try {
				Thread.sleep(OVERLAY_TIMEOUT_MS);
			} catch (InterruptedException e) {
				// Do nothing
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			dismissOverlay();
		}

		@Override
		protected void onCancelled() {
			dismissOverlay();
		}
	}

}
