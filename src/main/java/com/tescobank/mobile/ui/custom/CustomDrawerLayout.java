package com.tescobank.mobile.ui.custom;

import com.tescobank.mobile.BuildConfig;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;

public class CustomDrawerLayout extends DrawerLayout {

	public CustomDrawerLayout(Context context) {
		super(context);
	}

	public CustomDrawerLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CustomDrawerLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		
		int theWidthMeasureSpec = widthMeasureSpec;
		int theHeightMeasureSpec = heightMeasureSpec;
		
		// The following is required when running in Robolectric :(
		if (BuildConfig.DEBUG) {
			theWidthMeasureSpec = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.EXACTLY);
			theHeightMeasureSpec = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(heightMeasureSpec), MeasureSpec.EXACTLY);
		}
		
		super.onMeasure(theWidthMeasureSpec, theHeightMeasureSpec);
	}

}