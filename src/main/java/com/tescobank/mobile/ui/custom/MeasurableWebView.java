package com.tescobank.mobile.ui.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewParent;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.tescobank.mobile.BuildConfig;

/**
 * When testing with Robolectric WebViews cause an NPE during create because they return a null layoutparams. This class builds a layout param depending on the container. If you get an exception using this class it probably does not support your container. Update the {@link #getLayoutParams()}
 * method.<br/>
 * <br/>
 * Note: THe logic for returning a non-null layoutparams is not available in release mode, i.e. depends on {@link BuildConfig} DEBUG being true.
 */
public class MeasurableWebView extends WebView {

	public MeasurableWebView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public MeasurableWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MeasurableWebView(Context context) {
		super(context);
	}

	@Override
	@ExportedProperty(deepExport = true, prefix = "layout_")
	public android.view.ViewGroup.LayoutParams getLayoutParams() {
		android.view.ViewGroup.LayoutParams layoutParams = super.getLayoutParams();
		if (BuildConfig.DEBUG) {
			if (layoutParams == null) {
				layoutParams = getLayoutParams(getParent());
			}
		}
		return layoutParams;
	}
	
	private android.view.ViewGroup.LayoutParams getLayoutParams(ViewParent parent) {
		if (parent instanceof FrameLayout) {
			return new FrameLayout.LayoutParams(0, 0);
		} else if (parent instanceof RelativeLayout) {
			return new RelativeLayout.LayoutParams(0, 0);
		} else if (parent instanceof LinearLayout) {
			return new LinearLayout.LayoutParams(0, 0);
		} else {
			return null;
		}
	}
}
