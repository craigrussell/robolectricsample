package com.tescobank.mobile.ui.fomenu;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

final class HideMenuTextAnimationBuilder {

	private static final int DURATION = 100;

	private HideMenuTextAnimationBuilder() {
		super();
	}
	
	public static Animation build(final TextView text) {
		AlphaAnimation fadeOut = new AlphaAnimation(1.0f, 0.0f);
		fadeOut.setDuration(DURATION);
		fadeOut.setFillAfter(true);
		fadeOut.setAnimationListener(new DefaultAnimationListener() {
			@Override
			public void onAnimationEnd(Animation animation) {
				text.setVisibility(View.INVISIBLE);
				text.setAnimation(null);
			}
		});
		return fadeOut;
	}

}
