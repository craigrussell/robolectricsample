package com.tescobank.mobile.ui.fomenu;

import com.tescobank.mobile.R;

public enum FlyoutMenuRootButtonBackgroundDrawable {
	PCA_PRODUCT_MENU_ROOT_BACKGROUND_DRAWABLE("Personal Current Account", R.drawable.fo_unselected_left_pca, R.drawable.fo_unselected_right_pca, R.drawable.fo_pca_button), 
	CC_PRODUCT_MENU_ROOT_BACKGROUND_DRAWABLE("Credit Card", R.drawable.fo_unselected_left_cca,R.drawable.fo_unselected_right_cca,  R.drawable.fo_cca_button), 
	IAS_PRODUCT_MENU_ROOT_BACKGROUND_DRAWABLE("Instant Access Savings", R.drawable.fo_unselected_left_savings,R.drawable.fo_unselected_right_savings, R.drawable.fo_savings_button), 
	IS_PRODUCT_MENU_ROOT_BACKGROUND_DRAWABLE("Internet Savings", R.drawable.fo_unselected_left_savings, R.drawable.fo_unselected_right_savings, R.drawable.fo_savings_button), 
	IAI_PRODUCT_MENU_ROOT_BACKGROUND_DRAWABLE("Instant Access ISA", R.drawable.fo_unselected_left_savings, R.drawable.fo_unselected_right_savings, R.drawable.fo_savings_button), 
	JISA_PRODUCT_MENU_ROOT_BACKGROUND_DRAWABLE("Junior ISA", R.drawable.fo_unselected_left_savings, R.drawable.fo_unselected_right_savings, R.drawable.fo_savings_button), 
	FISA_PRODUCT_MENU_ROOT_BACKGROUND_DRAWABLE("Fixed ISAs", R.drawable.fo_unselected_left_savings, R.drawable.fo_unselected_right_savings, R.drawable.fo_savings_button), 
	FJISA_PRODUCT_MENU_ROOT_BACKGROUND_DRAWABLE("Fixed Junior ISAs", R.drawable.fo_unselected_left_savings, R.drawable.fo_unselected_right_savings, R.drawable.fo_savings_button), 
	FRS_PRODUCT_MENU_ROOT_BACKGROUND_DRAWABLE("Fixed Rate Saver", R.drawable.fo_unselected_left_savings, R.drawable.fo_unselected_right_savings, R.drawable.fo_savings_button), 
	CCP_PRODUCT_MENU_ROOT_BACKGROUND_DRAWABLE("Clubcard Plus", R.drawable.fo_unselected_left_savings,R.drawable.fo_unselected_right_savings, R.drawable.fo_savings_button), 
	LOAN_PRODUCT_MENU_ROOT_BACKGROUND_DRAWABLE("Loan",R.drawable.fo_unselected_left_loans,R.drawable.fo_unselected_right_loans,  R.drawable.fo_loan_button) ;

	private int imageDrawableIdLeft;
	private int imageDrawableIdRight;
	private int imageDrawableIdSelected;
	private String label;

	FlyoutMenuRootButtonBackgroundDrawable(String label, int imageDrawableIdLeft, int imageDrawableIdRight, int imageDrawableIdSelected) {
		this.imageDrawableIdLeft = imageDrawableIdLeft;
		this.imageDrawableIdRight = imageDrawableIdRight;
		this.imageDrawableIdSelected = imageDrawableIdSelected;
		this.label = label;
	}

	public int getImageDrawableLeft() {
		return imageDrawableIdLeft;
	}
	
	public int getImageDrawableRight() {
		return imageDrawableIdRight;
	}
	
	public int getImageDrawableSelected() {
		return imageDrawableIdSelected;
	}

	public String getLabel() {
		return label;
	}

	public static FlyoutMenuRootButtonBackgroundDrawable imageDrawableForProduct(String label) {

		for (FlyoutMenuRootButtonBackgroundDrawable flyoutMenuRootButtonBackgroundDrawable : FlyoutMenuRootButtonBackgroundDrawable.values()) {
			if (flyoutMenuRootButtonBackgroundDrawable.getLabel().equalsIgnoreCase(label)) {
				return flyoutMenuRootButtonBackgroundDrawable;
			}
		}
		return null;
	}
}
