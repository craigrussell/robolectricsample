package com.tescobank.mobile.ui.fomenu;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

final class HideMenuButtonAnimationBuilder {
	
	private static final int DURATION = 100;

	private HideMenuButtonAnimationBuilder() {
		super();
	}

	public static Animation build(final ImageView menuButton) {
		AlphaAnimation fadeOut = new AlphaAnimation(1.0f, 0.0f);
		fadeOut.setDuration(DURATION);
		fadeOut.setAnimationListener(new DefaultAnimationListener() {
			@Override
			public void onAnimationEnd(Animation animation) {
				menuButton.setVisibility(View.GONE);
			}
		});
		return fadeOut;
	}

}
