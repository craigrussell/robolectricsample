package com.tescobank.mobile.ui.fomenu;

import android.view.animation.Animation;
import android.view.animation.RotateAnimation;

final class RotationAnimationBuilder {

	private static final float PIVOT = 0.5f;

	private RotationAnimationBuilder() {
		super();
	}
	
	public static Animation build(float from, float to, int duration, boolean persist) {
		Animation a = new RotateAnimation(from, to, Animation.RELATIVE_TO_SELF, PIVOT, Animation.RELATIVE_TO_SELF, PIVOT);
		a.setDuration(duration);
		a.setFillAfter(true);
		return a;
	}

}
