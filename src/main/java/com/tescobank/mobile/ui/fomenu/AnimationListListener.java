package com.tescobank.mobile.ui.fomenu;

import java.util.List;

import android.util.Log;
import android.view.View;
import android.view.animation.Animation;

class AnimationListListener extends DefaultAnimationListener {

	private static final String TAG = AnimationListListener.class.getName();

	private List<Animation> anis;
	private View view;

	public AnimationListListener(View view, List<Animation> anis) {
		this.view = view;
		this.anis = anis;
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		Log.d(TAG, "onAnimationEnd: " + anis.size());
		if (!anis.isEmpty()) {
			Animation next = anis.get(0);
			next.setAnimationListener(this);
			Log.d(TAG, "onAnimationEnd: starting " + next);
			view.startAnimation(next);
		}
	}

	@Override
	public void onAnimationStart(Animation animation) {
		Log.d(TAG, "onAnimationStart: " + anis.size());
		if (!anis.isEmpty()) {
			anis.remove(0);
		}
	}
}