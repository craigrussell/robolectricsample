package com.tescobank.mobile.ui.fomenu;

import android.view.animation.Animation;

public class DismissAndResetAnimationListener extends DefaultAnimationListener {

	private FlyoutMenu menu;

	public DismissAndResetAnimationListener(FlyoutMenu menu) {
		this.menu = menu;
	}

	
	@Override
	public void onAnimationEnd(Animation animation) {
		menu.dismiss();
	}

}
