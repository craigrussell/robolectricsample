package com.tescobank.mobile.ui.fomenu;

import android.app.Activity;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.tescobank.mobile.ui.fomenu.FlyoutMenu.TescoMenuInflator;

public class ABSMenuInflator implements TescoMenuInflator {

	private Activity activity;

	public ABSMenuInflator(Activity activity) {
		this.activity = activity;
	}

	@Override
	public void inflate(int menuRes, Menu menu) {
		new MenuInflater(activity).inflate(menuRes, menu);
	}

}
