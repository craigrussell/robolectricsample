package com.tescobank.mobile.ui.fomenu;

import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

final class MenuItemDropAwayAnimationBuilder {

	private static final int START_OFFSET = 100;
	private static final int DURATION = 300;

	private MenuItemDropAwayAnimationBuilder() {
		super();
	}
	
	public static Animation build(final View view, int delay, boolean onLeft, int sidePaddingPx, int bottomPaddingPx) {
		// @formatter:off
		TranslateAnimation dropAway = new TranslateAnimation(
				TranslateAnimation.RELATIVE_TO_SELF, 0, 
				TranslateAnimation.ABSOLUTE, (int) (onLeft ? -view.getPaddingLeft() + sidePaddingPx : view.getPaddingRight() - sidePaddingPx), 
				TranslateAnimation.RELATIVE_TO_SELF, 0, 
				TranslateAnimation.ABSOLUTE, (int) (view.getPaddingBottom() - bottomPaddingPx));
		// @formatter:on
		dropAway.setInterpolator(new AccelerateInterpolator());
		dropAway.setDuration(DURATION);
		dropAway.setStartOffset(delay + START_OFFSET);
		dropAway.setFillAfter(true);

		return dropAway;
	}

}
