package com.tescobank.mobile.ui.fomenu;

import com.actionbarsherlock.view.MenuItem;

public interface FlyoutMenuItemProvider {
	
	MenuItem findItem(int id);
	
}