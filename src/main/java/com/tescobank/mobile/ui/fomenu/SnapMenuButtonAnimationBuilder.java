package com.tescobank.mobile.ui.fomenu;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;

final class SnapMenuButtonAnimationBuilder {
	
	private static final int DURATION = 200;

	private SnapMenuButtonAnimationBuilder() {
		super();
	}
	
	public static Animation build(View v, float fromX) {
		// @formatter:off
		TranslateAnimation anim = new TranslateAnimation(
				TranslateAnimation.ABSOLUTE, fromX, 
				TranslateAnimation.RELATIVE_TO_SELF, 0, 
				TranslateAnimation.RELATIVE_TO_SELF, 0, 
				TranslateAnimation.RELATIVE_TO_SELF, 0);
		// @formatter:on

		anim.setDuration(DURATION);
		anim.setInterpolator(new OvershootInterpolator());
		return anim;
	}

}
