package com.tescobank.mobile.ui.fomenu;

import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

final class TextFlyInAnimationBuilder {
	
	private static final int DURATION = 250;

	private TextFlyInAnimationBuilder() {
		super();
	}
	
	public static Animation build(boolean onLeft) {
		// @formatter:off
		TranslateAnimation anim = new TranslateAnimation(
				TranslateAnimation.RELATIVE_TO_PARENT, onLeft ? -1.0f : 1.0f, 
				TranslateAnimation.RELATIVE_TO_PARENT, 0, 
				TranslateAnimation.RELATIVE_TO_PARENT, 0, 
				TranslateAnimation.RELATIVE_TO_PARENT, 0);
		// @formatter:on
		anim.setDuration(DURATION);
		return anim;
	}

}
