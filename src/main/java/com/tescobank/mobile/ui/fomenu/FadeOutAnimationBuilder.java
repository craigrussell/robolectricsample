package com.tescobank.mobile.ui.fomenu;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

public final class FadeOutAnimationBuilder {
	
	private FadeOutAnimationBuilder() {
		super();
	}

	public static Animation build(long duration, AnimationListener listener) {
		Animation fadeOut = new AlphaAnimation(1.0f, 0.0f);
		fadeOut.setDuration(duration);
		fadeOut.setAnimationListener(listener);
		return fadeOut;
	}

}
