package com.tescobank.mobile.ui.fomenu;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

final class MenuItemFlyInAnimationBuilder {

	private static final int DURATION = 500;

	private MenuItemFlyInAnimationBuilder() {
		super();
	}
	
	public static Animation build(final TextView text, View menuItem, final boolean onLeft, int delay, int paddingPixels) {

		// @formatter:off
		TranslateAnimation anim = new TranslateAnimation(
				TranslateAnimation.ABSOLUTE, onLeft ? -menuItem.getPaddingLeft() + paddingPixels : menuItem.getPaddingRight() - paddingPixels, 
				TranslateAnimation.RELATIVE_TO_SELF, 0, 
				TranslateAnimation.ABSOLUTE, menuItem.getPaddingBottom() - paddingPixels, 
				TranslateAnimation.RELATIVE_TO_SELF, 0);
		// @formatter:on

		anim.setStartOffset(delay);
		anim.setDuration(DURATION);
		anim.setInterpolator(new OvershootInterpolator());

		anim.setAnimationListener(new DefaultAnimationListener() {
			@Override
			public void onAnimationEnd(Animation animation) {
				text.setVisibility(View.VISIBLE);
				text.startAnimation(TextFlyInAnimationBuilder.build(onLeft));
			}
		});
		return anim;
	}

}
