package com.tescobank.mobile.ui.fomenu;

import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

/**
 * A default implementation of animation listener, used as anonymous inner classes for chaining animations.
 * 
 * Why doesn't Android provide one of these? :'(
 */
class DefaultAnimationListener implements AnimationListener {

	@Override
	public void onAnimationEnd(Animation animation) {
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
	}

	@Override
	public void onAnimationStart(Animation animation) {
	}

}
