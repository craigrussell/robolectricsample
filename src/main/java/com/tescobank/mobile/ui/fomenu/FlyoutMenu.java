package com.tescobank.mobile.ui.fomenu;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.internal.view.menu.MenuBuilder;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.application.TescoApplication;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class FlyoutMenu implements FlyoutMenuItemProvider {

	private static final String TAG = FlyoutMenu.class.getSimpleName();

	private static final int FADE_OUT_DURATION = 750;
	private static final String SIDE_PREFERENCE = "onLeft";
	private static final int ITEM_TIMING_OFFSET = 25;
	private static final String BROADCAST_OPEN_MENU = FlyoutMenu.class.getName() + "/toggle";
	private static final String PREFS_NAME = "flyout";

	private final TescoActivity activity;
	private final Callback callback;

	private final Animation fadeIn;
	private final ImageView menuButton;
	private final int paddingSidePx;
	private final int paddingBottomPx;
	private final LayoutParams buttonParams;

	private Menu rootMenu;
	private Menu currentMenu;
	private float screenWidth;
	private boolean onLeft;

	private BroadcastReceiver openMenuBroadcastReceiver;

	private final TescoMenuInflator inflator;

	private Dialog menuWindow;

	private ViewGroup popupLayout;

	private int selectedResourceId;

	private int rightResourceId;

	private int leftResourceId;

	private FrameLayout targetView;

	/**
	 * Call this after the content view has been set on the activity (preferably in the onCreate method).
	 * 
	 * @param activity
	 *            the activity that owns this menu
	 * 
	 * @param callback
	 *            a callback which is executed when items are pressed
	 */
	public FlyoutMenu(final TescoActivity activity, FrameLayout targetView, Callback callback, TescoMenuInflator inflator) {
		this.inflator = inflator;
		this.onLeft = isConfiguredOnLeft(activity);

		this.activity = activity;
		this.callback = callback;
		this.fadeIn = AnimationUtils.loadAnimation(activity, R.anim.anim_fast_fade_in);
		this.targetView = targetView;

		targetView.setClipChildren(false);
		targetView.setClipToPadding(false);

		menuButton = new ImageView(activity);
		menuButton.setId(R.id.flyout_menu);
		menuButton.setClickable(true);
		menuButton.setOnTouchListener(new MoveButtonTouchListener(this));

		screenWidth = activity.getResources().getDisplayMetrics().widthPixels;
		paddingBottomPx = (int)activity.getResources().getDimension(R.dimen.flyout_menu_item0_padding_bottom);
		paddingSidePx = (int)activity.getResources().getDimension(R.dimen.flyout_menu_item0_padding_side);
		
		buttonParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		buttonParams.gravity = Gravity.BOTTOM | (onLeft ? Gravity.LEFT : Gravity.RIGHT);
		buttonParams.rightMargin = paddingSidePx;
		buttonParams.leftMargin = paddingSidePx;
		buttonParams.bottomMargin = paddingBottomPx;

		targetView.addView(menuButton, buttonParams);
		createOpenMenuBroadcastEventReceiver();
		initialiseMenu();
	}

	private void createOpenMenuBroadcastEventReceiver() {
		openMenuBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				onMenuButtonClicked();
			}
		};
	}

	public void setMenuResource(int menuRes) {
		rootMenu = new MenuBuilder(activity);
		currentMenu = rootMenu;
		inflator.inflate(menuRes, rootMenu);
		showButton();
	}

	public MenuItem findItem(int menuId) {
		return rootMenu.findItem(menuId);
	}

	/**
	 * Handle a back press.
	 * 
	 * @return true if the back press was handled in some way, false otherwise.
	 */
	public boolean handleBackPressed() {
		if (isOpen()) {
			onBackPressed();
			return true;
		}
		return false;
	}

	public void cleanUpMenuOnExit() {
		deregisterOpenMenuBroadcastReceiver();
	}

	public void hideButton() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "hiding menu button");
		}

		if (menuButton.getVisibility() != View.GONE) {
			menuButton.startAnimation(HideMenuButtonAnimationBuilder.build(menuButton));
		}
		deregisterOpenMenuBroadcastReceiver();
	}

	private void deregisterOpenMenuBroadcastReceiver() {
		LocalBroadcastManager.getInstance(activity).unregisterReceiver(openMenuBroadcastReceiver);
	}

	public void showButton() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "showing menu button");
		}
		LocalBroadcastManager.getInstance(activity).registerReceiver(openMenuBroadcastReceiver, new IntentFilter(BROADCAST_OPEN_MENU));
		menuButton.setVisibility(View.VISIBLE);
	}

	private void expand() {
		refreshRootItemBackground(popupLayout);
		runAnimationDrawable(getRootMenuImage(), getOpenAnimationDrawable());
		View background = popupLayout.findViewById(R.id.flyout_menu_background);
		background.startAnimation(fadeIn);
		menuWindow.show();
	}
	
	public void dismiss() {
		menuWindow.dismiss();
	}
	
	public void dismissWithAnimation() {
		if(!isOpen()) {
			return;
		}

		if (isDismissing()) {
			return;
		}
		
		runAnimationDrawable(menuButton, getCloseAnimationDrawable());	
		if (rootMenu.equals(currentMenu)) {
			ImageView rootItem = getRootMenuImage();
			runAnimationDrawable(rootItem, getCloseAnimationDrawable());
		}
		collapseMenuItems(0, null);
		popupLayout.startAnimation(FadeOutAnimationBuilder.build(FADE_OUT_DURATION, new DismissAndResetAnimationListener(this)));
	}
	
	private void runAnimationDrawable(ImageView target, int resId) {
		target.setBackgroundResource(resId);
		AnimationDrawable anim = AnimationDrawable.class.cast(target.getBackground());
		anim.setOneShot(true);
		anim.start();
	}

	private ImageView getRootMenuImage() {
		View view = menuWindow.getWindow().getDecorView();
		return ImageView.class.cast(view.findViewById(R.id.flyout_menu_root));
	}

	private int getCloseAnimationDrawable() {
		return onLeft ? R.drawable.flyout_menu_button_anim_close_left : R.drawable.flyout_menu_button_anim_close_right;
	}

	public void updateMenuButtonImage(int leftResourceId, int rightResourceId, int selectedResourceId) {
		this.selectedResourceId = selectedResourceId;
		this.rightResourceId = rightResourceId;
		this.leftResourceId = leftResourceId;
		updateMenuButtonImageForSide(onLeft);
	}

	private void updateMenuButtonImageForSide(boolean showOnLeft) {
		if (showOnLeft) {
			menuButton.setImageResource(leftResourceId);
		} else {
			menuButton.setImageResource(rightResourceId);
		}
	}

	private void setCurrentMenu(Menu menu) {
		this.currentMenu = menu;
	}

	private void collapseMenuItems(int subMenuItemTapped, final Runnable then) {

		View container = menuWindow.getWindow().getDecorView();

		int delay = 0;
		DropAwayAnimationBuilder.build(activity, container, R.id.flyout_menu_item1, null, delay, (subMenuItemTapped != R.id.flyout_menu_item1), onLeft, paddingSidePx, paddingBottomPx);

		delay += ITEM_TIMING_OFFSET;
		DropAwayAnimationBuilder.build(activity, container, R.id.flyout_menu_item2, null, delay, (subMenuItemTapped != R.id.flyout_menu_item2), onLeft, paddingSidePx, paddingBottomPx);

		delay += ITEM_TIMING_OFFSET;
		DropAwayAnimationBuilder.build(activity, container, R.id.flyout_menu_item3, null, delay, (subMenuItemTapped != R.id.flyout_menu_item3), onLeft, paddingSidePx, paddingBottomPx);

		delay += ITEM_TIMING_OFFSET;
		DropAwayAnimationBuilder.build(activity, container, R.id.flyout_menu_item4, null, delay, (subMenuItemTapped != R.id.flyout_menu_item4), onLeft, paddingSidePx, paddingBottomPx);

		delay += ITEM_TIMING_OFFSET;
		DropAwayAnimationBuilder.build(activity, container, R.id.flyout_menu_item5, null, delay, (subMenuItemTapped != R.id.flyout_menu_item5), onLeft, paddingSidePx, paddingBottomPx);

		delay += ITEM_TIMING_OFFSET;
		DropAwayAnimationBuilder.build(activity, container, R.id.flyout_menu_item6, then, delay, (subMenuItemTapped != R.id.flyout_menu_item6), onLeft, paddingSidePx, paddingBottomPx);
	}

	private boolean isOpen() {
		return menuWindow.isShowing();
	}
	
	private boolean isDismissing() {
		return popupLayout.getAnimation() != null && !popupLayout.getAnimation().hasEnded();
	}
	
	private void onBackPressed() {
		if (!rootMenu.equals(currentMenu)) {
			onRootItemClicked();
		} else {
			dismiss();
		}
	}

	private void onRootItemClicked() {
		if (rootMenu.equals(currentMenu)) {
			dismissWithAnimation();
		} else {
			TextView text = (TextView) menuWindow.getWindow().getDecorView().findViewById(R.id.flyout_menu_root_text);
			text.startAnimation(HideMenuTextAnimationBuilder.build(text));
			collapseMenuItems(0, new ThenShowRootMenu(this, rootMenu));
		}
	}

	private void onMenuButtonClicked() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onMenuButtonClicked");
		}
		
		expand();
		applyMenuItemsToMenuLayout();
		expandMenuLayout();
	}

	private void initialiseMenu() {

		int menuItemsRes = getMenuItemsResource();
		popupLayout = (ViewGroup) activity.getLayoutInflater().inflate(menuItemsRes, null);
		popupLayout.setOnClickListener(createMenuLayoutOnClickListener());
		popupLayout.findViewById(R.id.flyout_menu_root).setOnClickListener(createRootItemOnClickListener());

		Dialog dlg = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
		dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dlg.getWindow().setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.transparent_background));
		dlg.setContentView(popupLayout, new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		dlg.setCanceledOnTouchOutside(true);

		menuWindow = dlg;
		currentMenu = rootMenu;
	}

	private int getMenuItemsResource() {
		return onLeft ? R.layout.flyout_menu_items_left : R.layout.flyout_menu_items_right;
	}

	private OnClickListener createMenuLayoutOnClickListener() {
		return new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "menu layout clicked");
				}
				dismissWithAnimation();
			}
		};
	}

	private OnClickListener createRootItemOnClickListener() {
		return new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "root item clicked");
				}
				onRootItemClicked();
			}
		};
	}

	private void refreshRootItemBackground(ViewGroup popupLayout) {
		ImageView menuBg = (ImageView) popupLayout.findViewById(R.id.flyout_menu_root_background);
		menuBg.setImageResource(selectedResourceId);
	}

	private int getOpenAnimationDrawable() {
		return onLeft ? R.drawable.flyout_menu_button_anim_open_left : R.drawable.flyout_menu_button_anim_open_right;
	}

	private void expandMenuLayout() {
		View rootLayout = menuWindow.getWindow().getDecorView();

		int delay = 0;
		animateInMenuItem(rootLayout, R.id.flyout_menu_item1, delay);

		delay += ITEM_TIMING_OFFSET;
		animateInMenuItem(rootLayout, R.id.flyout_menu_item2, delay);

		delay += ITEM_TIMING_OFFSET;
		animateInMenuItem(rootLayout, R.id.flyout_menu_item3, delay);

		delay += ITEM_TIMING_OFFSET;
		animateInMenuItem(rootLayout, R.id.flyout_menu_item4, delay);

		delay += ITEM_TIMING_OFFSET;
		animateInMenuItem(rootLayout, R.id.flyout_menu_item5, delay);

		delay += ITEM_TIMING_OFFSET;
		animateInMenuItem(rootLayout, R.id.flyout_menu_item6, delay);
	}

	private void animateInMenuItem(View rootLayout, int menuItemRes, int delay) {
		View menuItem = rootLayout.findViewById(menuItemRes);
		if (menuItem.getVisibility() != View.VISIBLE) {
			return;
		}

		final TextView text = (TextView) menuItem.findViewWithTag("text");
		text.setVisibility(View.INVISIBLE);
		menuItem.startAnimation(MenuItemFlyInAnimationBuilder.build(text, menuItem, onLeft, delay, paddingSidePx));
	}

	private void applyMenuItemsToMenuLayout() {

		int[] flyoutMenuItemResIds = { R.id.flyout_menu_item1, R.id.flyout_menu_item2, R.id.flyout_menu_item3, R.id.flyout_menu_item4, R.id.flyout_menu_item5, R.id.flyout_menu_item6, };

		View layout = menuWindow.getWindow().getDecorView();
		int index = 0;

		// show any visible menu items
		for (int i = 0; i < currentMenu.size(); i++) {
			MenuItem item = currentMenu.getItem(i);
			if (item.isVisible()) {
				applyMenuItem(layout, item, flyoutMenuItemResIds[index++]);
			}
		}

		// hide the remaining items
		for (; index < flyoutMenuItemResIds.length; index++) {
			applyMenuItem(layout, null, flyoutMenuItemResIds[index]);
		}

	}

	private void applyMenuItem(View layout, MenuItem item, int menuItemRes) {

		View menuItem = layout.findViewById(menuItemRes);

		TextView text = (TextView) menuItem.findViewWithTag("text");
		ImageView image = (ImageView) menuItem.findViewWithTag("image");
	
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "applyMenuItem: VISIBLE");
		}

		if (null != item) {
			menuItem.setVisibility(View.VISIBLE);
			image.setVisibility(View.VISIBLE);
			text.setVisibility(View.VISIBLE);

			MenuItemClickListener menuItemClickListener = new MenuItemClickListener(this, item, menuItemRes);
			ViewGroup container = ViewGroup.class.cast(image.getParent());
			container.setClickable(true);
			container.setOnClickListener(menuItemClickListener);
			text.setText(item.getTitle());
			image.setImageDrawable(item.getIcon());
			image.setContentDescription(item.getTitle());

		} else {
			menuItem.setVisibility(View.INVISIBLE);
			image.setVisibility(View.INVISIBLE);
			text.setVisibility(View.INVISIBLE);
			text.setText("");
		}
	}

	static class MenuItemClickListener implements OnClickListener {

		private MenuItem item;
		private int menuItemViewId;
		private FlyoutMenu menu;

		public MenuItemClickListener(FlyoutMenu menu, MenuItem item, int menuItemViewId) {
			this.menu = menu;
			this.item = item;
			this.menuItemViewId = menuItemViewId;
		}

		@Override
		public void onClick(View v) {

			if (BuildConfig.DEBUG) {
				Log.d(TAG, "MenuItemClickListener:onClick:" + v);
			}

			if(menu.isDismissing()) {
				return;
			}
			
			if (item.hasSubMenu()) {
				menu.collapseMenuItems(menuItemViewId, new ThenShowSubmenu(menu, item));
			} else {
				menu.callback.itemSelected(menu, item);
			}
		}

	}

	static class ThenShowRootMenu implements Runnable {

		private FlyoutMenu menu;
		private Menu rootMenu;

		public ThenShowRootMenu(FlyoutMenu menu, Menu rootMenu) {
			this.menu = menu;
			this.rootMenu = rootMenu;
		}

		@Override
		public void run() {
			menu.setCurrentMenu(rootMenu);

			TextView text = (TextView) menu.menuWindow.getWindow().getDecorView().findViewById(R.id.flyout_menu_root_text);
			text.setText("");

			menu.applyMenuItemsToMenuLayout();
			menu.expandMenuLayout();
		}
	}

	static class ThenShowSubmenu implements Runnable {

		private MenuItem item;
		private FlyoutMenu menu;

		public ThenShowSubmenu(FlyoutMenu menu, MenuItem item) {
			this.menu = menu;
			this.item = item;
		}

		@Override
		public void run() {
			menu.setCurrentMenu(item.getSubMenu());

			ImageView rootButton = (ImageView) menu.menuWindow.getWindow().getDecorView().findViewById(R.id.flyout_menu_root);
			rootButton.setImageDrawable(item.getIcon());
			rootButton.startAnimation(RotationAnimationBuilder.build(0, 0, 0, true));

			TextView text = (TextView) menu.menuWindow.getWindow().getDecorView().findViewById(R.id.flyout_menu_root_text);
			text.setText(item.getTitle());
			text.setVisibility(View.VISIBLE);

			menu.applyMenuItemsToMenuLayout();
			menu.expandMenuLayout();
		}
	}

	/**
	 * Implements the dragging of the menu button from one side to another.
	 */
	static class MoveButtonTouchListener implements OnTouchListener {

		private boolean moved;
		private int startX;

		private FlyoutMenu menu;

		public MoveButtonTouchListener(FlyoutMenu menu) {
			this.menu = menu;
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			int distance = (int) Math.abs(startX - event.getRawX());

			switch (event.getAction()) {

			case MotionEvent.ACTION_DOWN:
				handleActionDown(event);
				break;

			case MotionEvent.ACTION_MOVE:
				handleActionMove(distance, v, event);
				break;

			case MotionEvent.ACTION_UP:
				handleActionUp(distance, v, event);
				break;
			}

			return false;
		}

		private void handleActionDown(MotionEvent event) {
			startX = (int) event.getRawX();
		}

		private void handleActionMove(int distance, View v, MotionEvent event) {
			if (distance > menu.paddingSidePx / 2) {
				performMove(v, event);
				moved = true;
			}
		}

		private void performMove(View v, MotionEvent event) {

			menu.buttonParams.gravity = Gravity.BOTTOM | Gravity.LEFT;
			menu.buttonParams.leftMargin = (int) event.getRawX() - (v.getWidth() / 2);

			if (menu.buttonParams.leftMargin < menu.paddingSidePx) {
				menu.buttonParams.leftMargin = menu.paddingSidePx;
			}

			int rightBorder = (int) (menu.screenWidth - v.getWidth() - menu.paddingSidePx);
			if (menu.buttonParams.leftMargin > rightBorder) {
				menu.buttonParams.leftMargin = rightBorder;
			}

			v.setLayoutParams(menu.buttonParams);

			boolean onLeft = !isOnRight(menu.buttonParams.leftMargin);
			menu.updateMenuButtonImageForSide(onLeft);
			menu.activity.getTooltipManager().hide();
			menu.activity.getTooltipManager().show();
		}

		private boolean isOnRight(int x) {
			return x > (menu.screenWidth / 2);
		}

		private void handleActionUp(int distance, View v, MotionEvent event) {
			boolean open = true;

			if (moved) {
				moved = false;
				snapMenuButton(v, event.getRawX());
				open = false;
			}

			if (open) {
				menu.activity.getTooltipManager().dismiss(TooltipId.FlyoutMenuIntro.ordinal());
				menu.onMenuButtonClicked();
			}

		}

		private void snapMenuButton(View v, float x) {

			menu.buttonParams.leftMargin = menu.paddingSidePx;

			if (isOnRight((int) x)) {
				snapRight();
			} else {
				snapLeft();
			}
			v.setLayoutParams(menu.buttonParams);
			menu.updateMenuButtonImage(menu.leftResourceId, menu.rightResourceId, menu.selectedResourceId);
			v.startAnimation(SnapMenuButtonAnimationBuilder.build(v, getFromX(v, x)));
			FlyoutMenu.configureOnLeft(menu.activity, menu.onLeft);
			menu.initialiseMenu();
			menu.showButton();
			menu.activity.getTooltipManager().dismiss(TooltipId.FlyoutMenuDrag.ordinal());
		}

		private void snapRight() {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Snapping RIGHT");
			}
			menu.buttonParams.gravity = Gravity.BOTTOM | Gravity.RIGHT;
			menu.onLeft = false;
		}

		private void snapLeft() {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Snapping LEFT");
			}
			menu.buttonParams.gravity = Gravity.BOTTOM | Gravity.LEFT;
			menu.onLeft = true;
		}

		private float getFromX(View v, float x) {
			return menu.onLeft ? x : Math.min(x - menu.screenWidth + v.getWidth(), 0);
		}
	}

	/**
	 * Implement this to receive a message when a menu item has been selected. You don't receive notifications for sub menus.
	 */
	public interface Callback {
		void itemSelected(FlyoutMenu menu, MenuItem item);
	}

	public interface TescoMenuInflator {
		void inflate(int menuRes, Menu rootMenu);
	}

	private static SharedPreferences getSharedPreferences(Context context) {
		return context.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE);
	}

	public static void configureOnLeft(Context context, boolean onLeft) {
		getSharedPreferences(context).edit().putBoolean(SIDE_PREFERENCE, onLeft).commit();
	}

	public static boolean isConfiguredOnLeft(Context context) {
		return getSharedPreferences(context).getBoolean(SIDE_PREFERENCE, true);
	}

	public static void reset(TescoApplication application) {
		application.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE).edit().clear().commit();
	}

	public void removeButton() {
		hideButton();
		targetView.removeView(menuButton);
	}
}
