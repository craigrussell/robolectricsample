package com.tescobank.mobile.ui.fomenu;

import android.app.Activity;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

public final class DropAwayAnimationBuilder {
	
	private DropAwayAnimationBuilder() {
		super();
	}

	public static void build(final Activity activity, View container, int res, final Runnable then, final int delay, boolean hideText, boolean onLeft, int sidePaddingPx, int bottomPaddingPx) {
		final View view = container.findViewById(res);
		final TextView text = (TextView) view.findViewWithTag("text");

		if (hideText) {
			text.startAnimation(HideMenuTextAnimationBuilder.build(text));
		}

		Animation dropAway = MenuItemDropAwayAnimationBuilder.build(view, delay, onLeft, sidePaddingPx, bottomPaddingPx);
		dropAway.setAnimationListener(new DefaultAnimationListener() {
			@Override
			public void onAnimationEnd(Animation animation) {
				view.setVisibility(View.INVISIBLE);
				if (null != then) {
					activity.runOnUiThread(then);
				}
			}
		});
		view.startAnimation(dropAway);
	}

}
