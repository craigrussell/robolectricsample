package com.tescobank.mobile.ui;

import java.util.List;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;

public class PayNoPayDisplay {

	private Context context;
	private List<ProductDetails> products;
	private boolean hasWarned;
	private int warningCount;
	private DataFormatter formatter;

	private int firstWarningIndex = -1;

	public PayNoPayDisplay(Context context, List<ProductDetails> products) {
		this.context = context;
		this.products = products;
		this.formatter = new DataFormatter();
		if (products != null) {
			updateWarnings();
		}
	}

	private void updateWarnings() {
		for (int i = 0; i < products.size(); i++) {
			if (shouldWarnForProductAtIndex(i)) {
				warningCount++;
				if (warningCount == 1) {
					firstWarningIndex = i;
				}
			}
		}
	}

	private boolean shouldWarnForProductAtIndex(int i) {
		ProductDetails current = products.get(i);
		PayNoPayProductDisplay pnpProductDisplay = new PayNoPayProductDisplay(current);
		return pnpProductDisplay.shouldDisplay();
	}

	public boolean shouldWarn() {
		return warningCount > 0 && !hasWarned;
	}

	public int getFirstWarningIndex() {
		return firstWarningIndex;
	}

	/**
	 * Invokes the warning message. The warning is only displayed once, thus subsequent invocations will return null.
	 * 
	 * @return The warning that should be displayed, or null if no warning should be displayed
	 */
	public String invokeWarning() {

		if (!shouldWarn()) {
			return null;
		}

		hasWarned = true;
		if (warningCount > 1) {
			return context.getString(R.string.pnp_message_generic);
		} else {
			ProductDetails product = products.get(firstWarningIndex);
			return context.getString(R.string.pnp_message, formatter.formatCurrency(product.getAmountOfPaymentsGoingOut()), formatter.formatCurrency(product.getAmountDue()));
		}
	}
}
