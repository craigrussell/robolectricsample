package com.tescobank.mobile.ui;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.widget.ImageView;

import com.tescobank.mobile.R;

public class HideAndShowProgressIndicator {

	private View spinner;
	private AnimationDrawable spinnerAnimation;

	public HideAndShowProgressIndicator(Activity activity) {
		this(activity.findViewById(android.R.id.content));
	}
	
	/*
	 * Uses the relative layout view as the spinner view to show/hide and
	 * the ImageView to animate.
	 */
	public HideAndShowProgressIndicator(View view) {
		super();
		spinner = view.findViewById(R.id.progressIndicator);
		ImageView spinnerImage = (ImageView) view.findViewById(R.id.progressIndicatorImage);
		spinnerImage.setBackgroundResource(R.drawable.tesco_spinner);
		spinnerAnimation = (AnimationDrawable) spinnerImage.getBackground();
	}
	
	/*
	 * Uses the image view as the spinner to animate and show/hide. 
	 */
	public HideAndShowProgressIndicator(ImageView spinner) {
		super();
		this.spinner = spinner;
		this.spinner.setBackgroundResource(R.drawable.tesco_spinner);
		spinnerAnimation = (AnimationDrawable) this.spinner.getBackground();
	}

	public void showInProgressIndicator() {

		spinner.setVisibility(View.VISIBLE);
		spinnerAnimation.start();
	}

	public void hideInProgressIndicator() {

		spinner.setVisibility(View.GONE);
		spinnerAnimation.stop();
	}

	public View getSpinner() {
		return spinner;
	}

	public AnimationDrawable getSpinnerAnimation() {
		return spinnerAnimation;
	}

}
