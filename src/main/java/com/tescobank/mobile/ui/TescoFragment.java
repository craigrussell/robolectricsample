package com.tescobank.mobile.ui;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.tescobank.mobile.application.ApplicationState;

public class TescoFragment extends Fragment {

	public ApplicationState getApplicationState() {
		FragmentActivity activity = getActivity();
		if (activity instanceof TescoActivity) {
			return getTescoActivity().getApplicationState();
		}
		return null;
	}

	public TescoActivity getTescoActivity() {
		return TescoActivity.class.cast(getActivity());
	}

	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
		getActivity().startActivityForResult(intent, requestCode);
	}
	
	@Override
	public void startActivity(Intent intent) {
		getActivity().startActivity(intent);
	}
	
}
