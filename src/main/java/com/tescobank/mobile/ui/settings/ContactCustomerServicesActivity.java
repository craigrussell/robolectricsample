package com.tescobank.mobile.ui.settings;

import java.util.List;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.properties.TescoApplicationPropertiesReader.ApplicationPropertyKeys;
import com.tescobank.mobile.ui.TescoAlertDialog;

public class ContactCustomerServicesActivity extends ViewHTMLDocumentActivity {

	private static final String TAG = ContactCustomerServicesActivity.class.getSimpleName();

	private boolean shouldExit;

	/**
	 * Default modifier because it's called directly from tests as there's no good way to get the webview to to generate the relevant {@link android.webkit.WebViewClient} event.
	 * 
	 * @param uri the uri to process.
	 * 
	 * @return true if the link was handled
	 */
	protected boolean handleFollowLink(final Uri uri) {
		
		if(!isTelephony(uri)) {
			return false;
		}
		
		Intent intent = createIntent(uri);
		if (canHandle(intent)) {
			showConfirmationDialog(intent);
		}
		
		return true;
	}

	private void showConfirmationDialog(final Intent intent) {
		new TescoAlertDialog.Builder(this)
			.setTitle(R.string.contact_customer_services_confirm_title)
			.setMessage(R.string.contact_customer_services_confirm_message)
			.setNegativeButton(android.R.string.cancel, null)
			.setPositiveButton(R.string.contact_customer_services_confirm_ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				startActivity(intent);
				shouldExit = true;
			}
		}).create().show();
	}

	private boolean isTelephony(final Uri uri) {
		return uri.getScheme().equals("tel");
	}
	
	private boolean canHandle(Intent intent) {
		List<ResolveInfo> intentHandlers = getPackageManager().queryIntentActivities(intent, 0);
		return !intentHandlers.isEmpty();
	}

	private Intent createIntent(final Uri uri) {
		Intent intent = new Intent(Intent.ACTION_DIAL);
		intent.setData(uri);
		return intent;
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onResume: shouldExit - " + shouldExit);
		}
		if (shouldExit) {
			ActivityCompat.finishAffinity(this);
		}
	}

	@Override
	protected String getUrl() {
		return getApplicationState().getApplicationPropertyValueForKey(ApplicationPropertyKeys.CONTACTUS_URL);
	}

	@Override
	protected void trackClick(String url) {
		getApplicationState().getAnalyticsTracker().trackViewContactUsLinkClicked(url);
	}

	@Override
	protected void trackView() {
		getApplicationState().getAnalyticsTracker().trackViewContactUs();
	}

}
