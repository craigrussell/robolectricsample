package com.tescobank.mobile.ui.settings;

import com.tescobank.mobile.properties.TescoApplicationPropertiesReader.ApplicationPropertyKeys;

public class ViewFAQActivity extends ViewHTMLDocumentActivity {

	@Override
	protected String getUrl() {
		String postLoginFaqUrl = getApplicationState().getApplicationPropertyValueForKey(ApplicationPropertyKeys.POST_LOGIN_FAQ_URL);
		String preLoginFaqUrl = getApplicationState().getApplicationPropertyValueForKey(ApplicationPropertyKeys.PRE_LOGIN_FAQ_URL);
		return isAuthenticated() ? postLoginFaqUrl:preLoginFaqUrl;
	}

	@Override
	protected void trackClick(String url) {
		getApplicationState().getAnalyticsTracker().trackViewFaqLinkClicked(isAuthenticated(), url);
	}

	@Override
	protected void trackView() {
		getApplicationState().getAnalyticsTracker().trackViewFaq(isAuthenticated());
	}

}
