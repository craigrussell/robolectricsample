package com.tescobank.mobile.ui.settings;

import android.annotation.SuppressLint;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.TescoActivity;

public class SettingsDrawerHelper {

	private static final String TAG = SettingsDrawerHelper.class.getSimpleName();

	private TescoActivity activity;
	private DrawerLayout drawerLayout;
	private SettingsFragment settingsFragment;

	public SettingsDrawerHelper(TescoActivity activity) {
		this.activity = activity;
	}

	/**
	 * Call from {@link Activity#onCreate}, once only.
	 */
	public void setContentViewWithDrawer(int contentLayoutId, SettingsFragment settingsFragment) {
		this.settingsFragment = settingsFragment;
		initialiseDrawer();
		addActivityContent(contentLayoutId);
		addSettingsFragment(settingsFragment);
	}

	/**
	 * Refreshes the drawer
	 */
	public void refresh() {
		((SettingsFragment) settingsFragment).refreshOptions();
	}

	private void initialiseDrawer() {
		drawerLayout = (DrawerLayout) activity.getLayoutInflater().inflate(R.layout.settings_drawer, null);
		activity.setContentView(drawerLayout);
		addDrawerToggle(drawerLayout);
	}

	private void addActivityContent(int contentLayoutId) {
		View contentView = activity.getLayoutInflater().inflate(contentLayoutId, null);
		ViewGroup contentContainer = (ViewGroup) drawerLayout.findViewById(R.id.content_frame);
		contentContainer.addView(contentView);
	}

	private void addSettingsFragment(Fragment navigationFragment) {
		FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
		ft.replace(R.id.settings_frame, navigationFragment);
		ft.commit();
	}

	private void addDrawerToggle(DrawerLayout drawer) {
		drawer.setDrawerListener(new ActionBarDrawerToggle(activity, drawer, R.drawable.actionbar_hamburger, R.string.drawer_open_description, R.string.drawer_close_description) {

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				settingsFragment.resetMenuLocation();
			}
		});
	}

	@SuppressLint("InlinedApi")
	public void openDrawer() {
		if (drawerLayout != null) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Drawer opened");
			}
			drawerLayout.openDrawer(Gravity.START);
			settingsFragment.onDrawerOpen();
		} else {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Failed to open drawer: drawer is null");
			}
		}
	}

	public void closeDrawer() {
		if (drawerLayout != null) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Drawer closed");
			}
			drawerLayout.closeDrawers();
		} else {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Failed to close drawer: drawer is null");
			}
		}
	}

	public void toggleDrawer() {
		if (drawerLayout != null) {
			if (drawerLayout.isDrawerOpen(Gravity.START)) {
				closeDrawer();
			} else {
				activity.hideKeyboard();
				openDrawer();
			}
		}
	}

	public boolean isAvailable() {
		return drawerLayout != null;
	}
}
