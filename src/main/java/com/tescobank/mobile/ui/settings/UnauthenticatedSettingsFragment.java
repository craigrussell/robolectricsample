package com.tescobank.mobile.ui.settings;

import static com.tescobank.mobile.ui.auth.AuthFlowFactory.createForgottenPasscodeFlow;
import static com.tescobank.mobile.ui.auth.AuthFlowFactory.createForgottenPasswordFlow;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

import com.tescobank.mobile.R;
import com.tescobank.mobile.flow.FlowConfirmInitiator;
import com.tescobank.mobile.flow.TescoConfirmation;
import com.tescobank.mobile.flow.TescoFlowConfirmInitiator;
import com.tescobank.mobile.flow.TescoFlowInitiator;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.auth.PinRequestingFlowInitiator;

/**
 * Fragment for the unauthenticated settings menu
 */
public class UnauthenticatedSettingsFragment extends SettingsFragment {

	private FlowConfirmInitiator confirmAndResetPasswordInitiator;
	private FlowConfirmInitiator confirmAndResetPasscodeInitiator;
	private InProgressIndicator inProgressIndicator;

	@Override
	protected int getFragmentLayoutId() {
		return R.layout.fragment_unauthenticated_settings;
	}

	public void initialise(TescoActivity activity, InProgressIndicator inProgressIndicator) {
		super.initialise(activity);
		this.inProgressIndicator = inProgressIndicator;
	}
	
	/**
	 * Initialises the options
	 */
	protected boolean initialiseOptions() {
		if (!super.initialiseOptions()) {
			return false;
		}

		View fragmentView = getFragmentView();
		if (fragmentView == null) {
			return false;
		}

		if ((getPasswordOption() == null) || (getPasscodeOption() == null)) {
			return false;
		}

		attachPasswordOptionListener();
		attachPasscodeOptionListener();
		attachContactUsListener(fragmentView);
		attachFaqsListener(fragmentView);
		attachTermsListener(fragmentView);
		return true;
	}

	@Override
	protected void disableFeatures() {
		super.disableFeatures();

		if (!getTescoActivity().getApplicationState().getFeatures().isResetForgottenPasswordEnabled()) {
			hideRow(R.id.settings_password_row);
		}
	}

	private void attachPasswordOptionListener() {
		getPasswordOption().setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				closeDrawer();
				showConfirmResetPasswordRequiredDialog();
			}
		});
	}

	public void showConfirmResetPasswordRequiredDialog() {
		TescoConfirmation resetPasswordConfirmation = new TescoConfirmation(R.string.update_credential_confirm_title, R.string.reset_password_confirm_message);
		PinRequestingFlowInitiator resetPasswordInitiator = new PinRequestingFlowInitiator(getTescoActivity(), createForgottenPasswordFlow());
		resetPasswordInitiator.setInProgressIndicator(inProgressIndicator);
		confirmAndResetPasswordInitiator = new TescoFlowConfirmInitiator(getTescoActivity(), resetPasswordConfirmation, resetPasswordInitiator);
		confirmAndResetPasswordInitiator.showDialog();
	}

	private void attachPasscodeOptionListener() {
		getPasscodeOption().setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				closeDrawer();
				showConfirmResetPasscodeRequiredDialog();
			}
		});
	}

	public void showConfirmResetPasscodeRequiredDialog() {
		TescoConfirmation resetPasscodeConfirmation = new TescoConfirmation(R.string.update_credential_confirm_title, R.string.reset_passcode_confirm_message);
		confirmAndResetPasscodeInitiator = new TescoFlowConfirmInitiator(getTescoActivity(), resetPasscodeConfirmation, new TescoFlowInitiator(getTescoActivity(), createForgottenPasscodeFlow()));
		confirmAndResetPasscodeInitiator.showDialog();
	}
	
	public void dismissResetPasswordWarningDialog() {
		if(confirmAndResetPasswordInitiator != null && confirmAndResetPasswordInitiator.isDialogShowing()) {
			confirmAndResetPasswordInitiator.dismissDialog();
		} 	
	}
	
	public void dismissResetPasscodeWarningDialog() {
		if(confirmAndResetPasscodeInitiator != null && confirmAndResetPasscodeInitiator.isDialogShowing()) {
			confirmAndResetPasscodeInitiator.dismissDialog();
		}	
	}
	
	public boolean isResetPasswordWarningDialogShowing() {
		if(confirmAndResetPasswordInitiator != null) {
			return confirmAndResetPasswordInitiator.isDialogShowing();
		}
		return false;
	}
	
	public boolean isResetPasscodeWarningDialogShowing() {
		if(confirmAndResetPasscodeInitiator != null) {
			return confirmAndResetPasscodeInitiator.isDialogShowing();
		}
		return false;
	}

	private void attachContactUsListener(View fragmentView) {
		View contactUs = fragmentView.findViewById(R.id.unauthenticated_settings_contact_us);
		contactUs.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				closeDrawer();
				startActivity(new Intent(getActivity(), ContactCustomerServicesActivity.class));
			}
		});
	}

	private void attachFaqsListener(View fragmentView) {
		View faqs = fragmentView.findViewById(R.id.unauthenticated_settings_faqs);
		faqs.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				closeDrawer();
				startActivity(new Intent(getActivity(), ViewFAQActivity.class));
			}
		});
	}

	private void attachTermsListener(View fragmentView) {
		View terms = fragmentView.findViewById(R.id.unauthenticated_settings_terms);
		terms.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				closeDrawer();
				startActivity(new Intent(getActivity(), ViewTermsAndConditionsActivity.class));
			}
		});
	}

	public void refreshOptions() {
		// DO Nothing
	}

	@Override
	public void onDrawerOpen() {
		getTescoActivity().getApplicationState().getAnalyticsTracker().trackViewSettings();
	}
}
