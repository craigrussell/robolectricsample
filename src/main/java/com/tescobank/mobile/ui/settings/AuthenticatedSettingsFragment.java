package com.tescobank.mobile.ui.settings;

import static com.tescobank.mobile.ui.auth.AuthFlowFactory.createChangePasswordFlow;
import static com.tescobank.mobile.ui.auth.AuthFlowFactory.createSwitchToPasswordFlow;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.flow.Flow;
import com.tescobank.mobile.flow.FlowConfirmInitiator;
import com.tescobank.mobile.flow.FlowInitiator;
import com.tescobank.mobile.flow.TescoConfirmation;
import com.tescobank.mobile.flow.TescoFlowConfirmInitiator;
import com.tescobank.mobile.persist.Persister;
import com.tescobank.mobile.ui.TescoAlertDialog;
import com.tescobank.mobile.ui.auth.AuthFlowFactory;
import com.tescobank.mobile.ui.auth.PinRequestingFlowInitiator;
import com.tescobank.mobile.ui.balancepeek.BalancePeekFlowFactory;
import com.tescobank.mobile.ui.tutorial.TooltipId;

/**
 * Fragment for the authenticated settings menu
 */
public class AuthenticatedSettingsFragment extends SettingsFragment {

	private static final String TAG = AuthenticatedSettingsFragment.class.getSimpleName();

	private FlowConfirmInitiator confirmAndChangePasswordInitiator;

	/** Whether listeners should respond to changes */
	private boolean disableListeners;
	
	private CompoundButton useLocationSwitch;
	private CompoundButton passcodeSwitch;
	
	@Override
	public void onDrawerOpen() {
		getTescoActivity().getApplicationState().getAnalyticsTracker().trackViewSettings();
		getTescoActivity().getTooltipManager().dismiss(TooltipId.AuthorisedSettings.ordinal());
		getTescoActivity().getTooltipManager().hide();
	}
	
	@Override
	protected int getFragmentLayoutId() {
		return R.layout.fragment_authenticated_settings;
	}

	@Override
	protected void disableFeatures() {
		super.disableFeatures();

		if (!getTescoActivity().getApplicationState().getFeatures().isChangedPasswordEnabled()) {
			hideRow(R.id.settings_password_row);
		}

		if (!getTescoActivity().getApplicationState().getFeatures().isSetPasscodeEnabled()) {
			hideRow(R.id.settings_passcode_switch_row);
		}
		
		if (!getTescoActivity().getApplicationState().getFeatures().isNonInteractiveProductsEnabled()) {
			hideRow(R.id.authenticated_settings_balance_peek);
		}
	}

	@Override
	protected boolean initialiseOptions() {
		if (!super.initialiseOptions()) {
			return false;
		}

		View fragmentView = getFragmentView();
		if (fragmentView == null) {
			return false;
		}

		if ((getPasswordOption() == null) || (getPasscodeOption() == null)) {
			return false;
		}

		passcodeSwitch = (CompoundButton) fragmentView.findViewById(R.id.authenticated_settings_switch_to_passcode);
		if (passcodeSwitch == null) {
			return false;
		}

		useLocationSwitch = (CompoundButton) fragmentView.findViewById(R.id.authenticated_settings_use_location);
		if (useLocationSwitch == null) {
			return false;
		}

		initialiseContactUs(fragmentView);
		initialiseFaq(fragmentView);
		initialiseTerms(fragmentView);
		attachPasscodeSwitchListener();
		attachPasswordOptionListener();
		attachPasscodeOptionListener();
		attachLogoutListener(fragmentView);
		attachLocationSwitchListener();
		attachBalancePeekListener(fragmentView);

		return true;
	}

	private void initialiseContactUs(View fragmentView) {
		View contactUs = fragmentView.findViewById(R.id.authenticated_settings_contact_us);
		contactUs.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				closeDrawer();
				Intent intent = new Intent(getActivity(), ContactCustomerServicesActivity.class);
				intent.putExtra("authenticated", true);
				startActivity(intent);
			}
		});
	}

	private void initialiseFaq(View fragmentView) {
		View faq = fragmentView.findViewById(R.id.authenticated_settings_faqs);
		faq.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				closeDrawer();
				Intent intent = new Intent(getActivity(), ViewFAQActivity.class);
				intent.putExtra("authenticated", true);
				startActivity(intent);
			}
		});
	}

	private void initialiseTerms(View fragmentView) {
		View terms = fragmentView.findViewById(R.id.authenticated_settings_terms);
		terms.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				closeDrawer();
				Intent intent = new Intent(getActivity(), ViewTermsAndConditionsActivity.class);
				intent.putExtra("authenticated", true);
				startActivity(intent);
			}
		});
	}

	private void attachPasscodeSwitchListener() {
		passcodeSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (disableListeners) {
					return;
				}
				if (getTescoActivity().getApplicationState().isPassword()) {
					closeDrawer();
					startSwitchToPasscodeFlow();
				} else {
					closeDrawer();
					startSwitchToPasswordFlow();
				}
			}
		});
	}

	private void attachPasswordOptionListener() {
		getPasswordOption().setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (disableListeners) {
					return;
				}
				closeDrawer();
				showDialogAndStartChangePasswordFlow();
			}
		});
	}
	
	public void showConfirmChangePasswordRequiredDialog() {
		showDialogAndStartChangePasswordFlow();
	}
	
	public void dismissResetPasswordWarningDialog() {
		if(confirmAndChangePasswordInitiator != null && confirmAndChangePasswordInitiator.isDialogShowing()) {
			confirmAndChangePasswordInitiator.dismissDialog();
		} 	
	}
	
	public boolean isChangePasswordWarningDialogShowing() {
		if(confirmAndChangePasswordInitiator != null) {
			return confirmAndChangePasswordInitiator.isDialogShowing();
		}
		return false;
	}

	private void attachPasscodeOptionListener() {
		getPasscodeOption().setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (disableListeners) {
					return;
				}

				closeDrawer();
				startChangePasscodeFlow();
			}
		});
	}

	private void attachLogoutListener(View fragmentView) {
		View logout = fragmentView.findViewById(R.id.authenticated_settings_logout);
		logout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				closeDrawer();
				getTescoActivity().logOut();
			}
		});
	}

	private void attachLocationSwitchListener() {
		useLocationSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (disableListeners) {
					return;
				}

				if (isChecked) {
					requestToShareLocation();
				} else {
					getTescoActivity().getApplicationState().setUseLocationPreference(Persister.UseLocationPreference.NO);
					if (BuildConfig.DEBUG) {
						Log.d(TAG, "FromCheckedListener - " + getTescoActivity().getApplicationState().getUseLocationPreference().toString());
					}
				}
			}
		});
	}

	private void requestToShareLocation() {

		new TescoAlertDialog.Builder(this.getTescoActivity()).setTitle(R.string.share_location_confirm_title).setMessage(R.string.share_location_confirm_message).setOnCancelListener(new DialogInterface.OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {

				useLocationSwitch.setChecked(false);
				if (BuildConfig.DEBUG) {
					Log.d(TAG, getTescoActivity().getApplicationState().getUseLocationPreference().toString());
				}
			}
		}).setPositiveButton(R.string.share_location_confirm_yes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				closeDrawer();
				getTescoActivity().getApplicationState().setUseLocationPreference(Persister.UseLocationPreference.YES);
				if (BuildConfig.DEBUG) {
					Log.d(TAG, getTescoActivity().getApplicationState().getUseLocationPreference().toString());
				}
			}
		}).setNeutralButton(R.string.share_location_confirm_no, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				closeDrawer();
				getTescoActivity().getApplicationState().setUseLocationPreference(Persister.UseLocationPreference.NO);
				useLocationSwitch.setChecked(false);
				if (BuildConfig.DEBUG) {
					Log.d(TAG, getTescoActivity().getApplicationState().getUseLocationPreference().toString());
				}
			}
		}).create().show();
	}
	
	private void attachBalancePeekListener(View fragmentView) {
		View balancePeek = fragmentView.findViewById(R.id.authenticated_settings_balance_peek);
		balancePeek.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				closeDrawer();
				startBalancePeek();
			}
		});
		
	}

	protected void startBalancePeek() {
		Flow flow = BalancePeekFlowFactory.createBalancePeekSetUpFlow();
		flow.begin(getTescoActivity());
	}

	public void refreshOptions() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Refreshing Options");
		}

		// Disable listeners while we change state
		disableListeners = true;

		View fragmentView = getFragmentView();

		boolean isPassword = getTescoActivity().getApplicationState().isPassword();

		passcodeSwitch.setChecked(!isPassword);
		useLocationSwitch.setChecked(Persister.UseLocationPreference.YES == getTescoActivity().getApplicationState().getUseLocationPreference());

		TextView passcodeSwitchInfoLabel = (TextView) fragmentView.findViewById(R.id.passcode_switch_info);
		passcodeSwitchInfoLabel.setText(isPassword ? getResources().getString(R.string.authenticated_settings_using_password_info) : getResources().getString(R.string.authenticated_settings_using_passcode_info));

		View passwordOption = (View) fragmentView.findViewById(R.id.settings_password);
		View passcodeOption = (View) fragmentView.findViewById(R.id.settings_passcode);

		if (isPassword) {
			passcodeOption.setVisibility(View.GONE);
			passwordOption.setVisibility(View.VISIBLE);
		} else {
			passcodeOption.setVisibility(View.VISIBLE);
			passwordOption.setVisibility(View.GONE);
		}

		// Enable listeners again
		disableListeners = false;
	}
	
	private void showDialogAndStartChangePasswordFlow() {
		TescoConfirmation confirmation = new TescoConfirmation(R.string.update_credential_confirm_title,R.string.change_password_confirm_message);
		confirmAndChangePasswordInitiator = new TescoFlowConfirmInitiator(getTescoActivity(), confirmation, new PinRequestingFlowInitiator(getTescoActivity(), createChangePasswordFlow()));
		confirmAndChangePasswordInitiator.showDialog();
	}
	
	private void startSwitchToPasswordFlow() {
		FlowInitiator switchToPasswordInitiator = new PinRequestingFlowInitiator(getTescoActivity(), createSwitchToPasswordFlow());
		switchToPasswordInitiator.execute();
	}

	private void startChangePasscodeFlow() {
		Flow flow = AuthFlowFactory.createChangePasscodeFlow();
		flow.begin(getTescoActivity());
	}

	private void startSwitchToPasscodeFlow() {
		Flow flow = AuthFlowFactory.createSwitchToPasscodeFlow();
		flow.begin(getTescoActivity());
	}
}
