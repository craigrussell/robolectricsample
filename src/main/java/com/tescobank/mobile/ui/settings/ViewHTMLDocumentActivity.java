package com.tescobank.mobile.ui.settings;

import static android.os.Build.VERSION_CODES.JELLY_BEAN_MR2;

import java.io.InputStream;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.download.WebViewDownloadHelper;
import com.tescobank.mobile.network.NetworkStatusChecker;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.TescoActivity;

public abstract class ViewHTMLDocumentActivity extends TescoActivity {

	private static final String TAG = ViewHTMLDocumentActivity.class.getSimpleName();
	
	private WebView webView;
	private NetworkStatusChecker networkStatusChecker;
	private ProgressBar progressBar;

	@Override
	protected final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		setContentView(R.layout.fullsize_webview);

		networkStatusChecker = new NetworkStatusChecker();
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		webView = (WebView) findViewById(R.id.webview);
		webView.getSettings().setAllowFileAccess(false);
		progressBar.setVisibility(View.VISIBLE);
		webView.setVisibility(View.GONE);
		webView.setWebViewClient(new DocumentsWebViewClient());
        webView.addJavascriptInterface(new DocumentsWebViewInterface(),"Android");
        enableJavaScript();
		
		loadPageInWebView();
		trackView();
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void enableJavaScript() {
		webView.getSettings().setJavaScriptEnabled(true);
	}

	protected void loadPageInWebView() {
		if (!networkStatusChecker.isAvailable(this)) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Not attempting to show WebView - there is no internet connection");
			}
			ErrorResponseWrapper error = getErrorBuilder().buildErrorResponse(InAppError.NoInternetConnection);
			handleErrorResponse(error);
			return;
		}
		webView.loadUrl(getUrl());
	}

	@Override
	protected void onRetryPressed() {
		progressBar.setVisibility(View.VISIBLE);
		webView.setVisibility(View.GONE);
		loadPageInWebView();
	}

	protected final boolean isAuthenticated() {
		return getIntent() != null && getIntent().getBooleanExtra("authenticated", false);
	}

	/**
	 * @param uri
	 *            the link to follow
	 * @return true if the link was handled explicitly, false otherwise
	 */
	protected boolean handleFollowLink(Uri uri) {
		return false;
	}

	protected abstract String getUrl();

	protected abstract void trackView();

	protected abstract void trackClick(String url);
	
	private class DocumentsWebViewClient extends WebViewClient {

		@Override
		public void onPageFinished(WebView view, String url) {
			progressBar.setVisibility(View.GONE);
			webView.setVisibility(View.VISIBLE);
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, errorCode + ": " + description);
			}
			handleErrorResponse(getErrorBuilder().buildErrorResponse(errorCode));
		}

        @Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
            trackClick(url);
			return handleFollowLink(Uri.parse(url));
		}
        
		@Override
		@SuppressLint("NewApi")
		public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
			if (Build.VERSION.SDK_INT == JELLY_BEAN_MR2) {
				return new WebResourceResponse("","", downloadWebContent(url));
			}
			return super.shouldInterceptRequest(view, url);
		}
		
		private InputStream downloadWebContent(String url) {
			WebViewDownloadHelper resourceDownloader = new WebViewDownloadHelper(ViewHTMLDocumentActivity.this);
			return resourceDownloader.performDownload(url);
		}
	}

    private class DocumentsWebViewInterface{

        @JavascriptInterface
        public void trackFaqClick(String tappedLinkUrl){
            trackClick(tappedLinkUrl);
        }
    }
}
