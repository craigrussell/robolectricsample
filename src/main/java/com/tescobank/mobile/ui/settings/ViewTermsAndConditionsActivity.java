package com.tescobank.mobile.ui.settings;

import static android.content.Intent.ACTION_VIEW;
import static android.net.Uri.parse;
import android.content.Intent;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.ShareActionProvider;
import com.actionbarsherlock.widget.ShareActionProvider.OnShareTargetSelectedListener;
import com.tescobank.mobile.R;
import com.tescobank.mobile.properties.TescoApplicationPropertiesReader.ApplicationPropertyKeys;

/**
 * Activity for viewing T&Cs and privacy
 */
public class ViewTermsAndConditionsActivity extends ViewHTMLDocumentActivity implements OnShareTargetSelectedListener {
	
	private static final String SHARE_FILE = "share_history_terms_conditions.xml";
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.share, menu);
		MenuItem actionItem = menu.findItem(R.id.menu_item_share);
		ShareActionProvider actionProvider = (ShareActionProvider) actionItem.getActionProvider();
		actionProvider.setShareHistoryFileName(SHARE_FILE);
		actionProvider.setShareIntent(createShareIntent());
		actionProvider.setOnShareTargetSelectedListener(this);
		return true;
	}
	
	private Intent createShareIntent() {
		Intent shareIntent = new Intent(ACTION_VIEW);
		shareIntent.setType("application/pdf");
		shareIntent.setData(parse(getDownloadUrl()));
		return shareIntent;
	}
	
	@Override
	public boolean onShareTargetSelected(ShareActionProvider source, Intent intent) {
		trackShare();
		startActivity(intent);
		return true;
	}

	@Override
	protected String getUrl() {
		return getApplicationState().getApplicationPropertyValueForKey(ApplicationPropertyKeys.TERMS_AND_CONDITIONS_URL);
	}
	
	private String getDownloadUrl() {
		return getApplicationState().getApplicationPropertyValueForKey(ApplicationPropertyKeys.TERMS_AND_CONDITIONS_DOWNLOAD_URL);
	}

	@Override
	protected void trackView() {
		getApplicationState().getAnalyticsTracker().trackViewTermsAndConditions();
	}

	@Override
	protected void trackClick(String url) {
		getApplicationState().getAnalyticsTracker().trackViewTermsAndConditionsLinkClicked(url);
	}
	
	private void trackShare() {
		getApplicationState().getAnalyticsTracker().trackExportTerms();
	}
}
