package com.tescobank.mobile.ui.settings;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoAlertDialog;
import com.tescobank.mobile.ui.TescoFragment;

/**
 * Abstract base class for settings menus
 */
public abstract class SettingsFragment extends TescoFragment {

	/** The activity */
	private TescoActivity activity;

	/** The fragment view */
	private ScrollView view;

	/** The reset / change password option */
	private View passwordOption;

	/** The reset / change passcode option */
	private View passcodeOption;

	/** The app version label */
	private TextView appVersionLabel;

	public void initialise(TescoActivity activity) {
		this.activity = activity;
		doInit();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = (ScrollView) inflater.inflate(getFragmentLayoutId(), container, false);
		passwordOption = view.findViewById(R.id.settings_password);
		passcodeOption = view.findViewById(R.id.settings_passcode);
		appVersionLabel = (TextView) view.findViewById(R.id.settings_app_version);
		doInit();
		return view;
	}

	private void doInit() {
		if (initialiseOptions()) {
			refreshOptions();
			disableFeatures();
		}
	}

	/**
	 * @return the ID of the fragment layout
	 */
	protected abstract int getFragmentLayoutId();

	/**
	 * @return the fragment view
	 */
	protected View getFragmentView() {
		return view;
	}

	/**
	 * @return the reset / change password option
	 */
	protected View getPasswordOption() {
		return passwordOption;
	}

	/**
	 * @return the reset / change passcode option
	 */
	protected View getPasscodeOption() {
		return passcodeOption;
	}

	/**
	 * Initialises the options
	 */
	protected boolean initialiseOptions() {

		if ((activity == null) || (passcodeOption == null) || (passwordOption == null) || (appVersionLabel == null)) {
			return false;
		}

		initialiseCredentialOptions();
		attachResetListener();
		initialiseAppVersion();
		return true;
	}

	private void initialiseCredentialOptions() {
		if (activity.getApplicationState().isPassword()) {
			passcodeOption.setVisibility(View.GONE);
		} else {
			passwordOption.setVisibility(View.GONE);
		}
	}

	private void attachResetListener() {
		View resetapp = getFragmentView().findViewById(R.id.settings_reset_app);
		resetapp.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				confirmResetApplication();
			}
		});
	}

	private void initialiseAppVersion() {
		appVersionLabel.setText(String.format(getResources().getString(R.string.settings_app_version), activity.getApplicationState().getDeviceInfo().getVersionName()));
	}

	private void confirmResetApplication() {
		closeDrawer();

		DialogInterface.OnClickListener resetApplicationSelectedListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				resetApplication();
			}
		};

		new TescoAlertDialog.Builder(getTescoActivity()).setTitle(R.string.confirm_application_reset_title).setMessage(R.string.confirm_application_reset_message).setPositiveButton(R.string.dialog_confirm_ok, resetApplicationSelectedListener)
				.setNegativeButton(R.string.dialog_confirm_cancel, null).show();
	}

	private void resetApplication() {
		getApplicationState().resetApplication();
		getTescoActivity().logOut();
	}

	protected void disableFeatures() {
		if (!getTescoActivity().getApplicationState().getFeatures().isUnlinkCustomerAndDeviceEnabled()) {
			hideRow(R.id.settings_reset_app_row);
		}
	}

	protected final void hideRow(int id) {
		View view = getFragmentView().findViewById(id);
		if (null != view) {
			view.setVisibility(View.GONE);
		}
	}

	protected void closeDrawer() {
		getTescoActivity().closeDrawer();
		resetMenuLocation();
	}

	public void resetMenuLocation() {
		view.scrollTo(0, 0);
	}
	
	public TescoActivity getTescoActivity() {
		return activity;
	}

	/**
	 * @return will refresh the drawer when when it is opened
	 */
	public abstract void refreshOptions();

	/**
	 * Called when the drawer has opened
	 */
	public abstract void onDrawerOpen();
}
