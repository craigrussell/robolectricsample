package com.tescobank.mobile.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.tescobank.mobile.R;

/**
 * A Blank Popup Window with a transparent background.
 */

@SuppressLint("ViewConstructor")
public class TescoPopupWindow extends PopupWindow {

	private Activity activity;

	public TescoPopupWindow(Activity activity, View popupContent) {
		super(activity);
		this.activity = activity;
		setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.transparent_background));
		setContentView(popupContent);
	}

	public void showFullScreen() {
		if(!activity.isFinishing()) {
			setWindowLayoutMode(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
			showAtLocation(Gravity.TOP, 0, 0);
		}	
	}

	public void showAtLocation(int gravity, int x, int y) {
		View content = activity.findViewById(android.R.id.content);
		showAtLocation(content, gravity, x, y);
	}

	@Override
	public void showAtLocation(View parent, int gravity, int x, int y) {
		super.showAtLocation(parent, gravity, x, y);
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
			// This is not required for later versions of Android!
			update(0, 0, 0, 0, true);
		}
		update();
	}
}
