package com.tescobank.mobile.ui;

import android.widget.ImageView;

import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnCloseListener;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.tescobank.mobile.R;

public final class SearchViewHelperFactory {
	
	private SearchViewHelperFactory() {
		super();
	}

	public static SearchViewHelper create(SearchView searchView) {

		if (null == searchView) {
			return new NullSearchViewHelper();
		}

		return new ActiveSearchViewHelper(searchView);
	}

	public interface SearchViewHelper {

		boolean handleBackPressed();

		void setSubmitQueryCallback(final SubmitQueryCallback callback);

		void clearFocus();

		void setOnCloseListener(OnCloseListener onCloseListener);

		void showQuery(String searchtext);
		
		boolean isIconified();
		
		void setIconified(boolean iconified);
		
		String getQuery();
		
		void setQuery(String searchText);

	}

	public interface SubmitQueryCallback {

		void textSubmitted(String search);

		void textChanged(String text);
	}

	static class NullSearchViewHelper implements SearchViewHelper {

		@Override
		public boolean handleBackPressed() {
			return false;
		}
		
		@Override
		public boolean isIconified() {
			return false;
		}

		@Override
		public void setSubmitQueryCallback(SubmitQueryCallback callback) {
		}

		@Override
		public void clearFocus() {
		}

		@Override
		public void setOnCloseListener(OnCloseListener onCloseListener) {
		}

		public void showQuery(String searchtext) {
		}
		
		@Override
		public void setIconified(boolean iconified) {
		}
		
		@Override
		public String getQuery() {
			return "";
		}
		
		@Override
		public void setQuery(String searchtext) {
		}

	}

	static class ActiveSearchViewHelper implements SearchViewHelper {

		private SearchView searchView;

		public ActiveSearchViewHelper(SearchView searchView) {
			this.searchView = searchView;
			ImageView v = (ImageView) searchView.findViewById(com.actionbarsherlock.R.id.abs__search_button);
			v.setImageResource(R.drawable.ic_search);
		}
		
		@Override
		public boolean isIconified() {
			return searchView.isIconified();
		}

		@Override
		public boolean handleBackPressed() {
			if (!isIconified()) {
				if (getQuery().length() > 0) {
					setQuery(null);
				}
				setIconified(true);
				return true;
			}
			return false;
		}

		@Override
		public void setSubmitQueryCallback(final SubmitQueryCallback callback) {

			searchView.setOnQueryTextListener(new OnQueryTextListener() {

				@Override
				public boolean onQueryTextSubmit(String text) {
					callback.textSubmitted(text);
					return false;
				}

				@Override
				public boolean onQueryTextChange(String text) {
					callback.textChanged(text);
					return false;
				}
			});
		}

		@Override
		public void clearFocus() {
			searchView.clearFocus();
		}

		@Override
		public void setOnCloseListener(OnCloseListener onCloseListener) {
			searchView.setOnCloseListener(onCloseListener);
		}

		@Override
		public void showQuery(String searchtext) {
			setIconified(false);
			setQuery(searchtext);
		}
		
		@Override
		public void setIconified(boolean iconified) {
			searchView.setIconified(iconified);
		}
			
		@Override
		public String getQuery() {
			return searchView.getQuery().toString();
		}
		
		@Override
		public void setQuery(String searchtext) {
			searchView.setQuery(searchtext, false);
		}
	}
}
