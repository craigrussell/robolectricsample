package com.tescobank.mobile.ui.seasonal;

public interface DownloadListener {
	void success(byte[] data);

	void failed();
}
