package com.tescobank.mobile.ui.seasonal;

import java.io.File;

import android.graphics.Bitmap;

public interface BitmapLoader {
	public Bitmap load(String fileName, File folder);
}
