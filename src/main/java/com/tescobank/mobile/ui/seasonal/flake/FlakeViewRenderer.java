package com.tescobank.mobile.ui.seasonal.flake;

import java.util.List;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;

public class FlakeViewRenderer {

	public static final int MIN_ALPHA = 0;
	public static final int MAX_ALPHA = 255;
	
	private static final float NUMBER_MILLIS_IN_SECOND = 1000f;
	private static final int DEBUG_STRING_X_OFFSET = 40;
	private static final int NUM_FLAKES_TEXT_Y_OFFSET = -50;
	private static final int FPS_TEXT_Y_OFFSET = -120;
	private static final int UPDATE_TIME_THRESHOLD = 300;
	private static final int DEBUG_STRING_TEXT_SIZE = 50;

	private final Paint textPaint;
	private final Paint bitmapPaint;

	// frames per second
	private float framesPerSecond = 0;
	private int frames = 0;

	// Matrix used to translate/rotate each flake during rendering
	private final Matrix m = new Matrix();

	private long startTime;
	private long prevTime;

	private boolean showOutputText = false;

	private String fpsString = "fps: ";
	private String flakeString = "flakes: ";

	public FlakeViewRenderer() {
		textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		textPaint.setColor(Color.RED);
		textPaint.setTextSize(DEBUG_STRING_TEXT_SIZE);

		bitmapPaint = new Paint();
		bitmapPaint.setAlpha(MAX_ALPHA);
	}

	public void setShowOutputText(boolean showText) {
		this.showOutputText = showText;
	}

	public void draw(Canvas canvas, List<Flake> flakes, int height, long timeNow) {
		flakeString = "flakes: " + flakes.size();
		
		repositionFlakes(canvas, flakes);

		calculateFramesPerSecond(timeNow);

		if (showOutputText) {
			canvas.drawText(flakeString, DEBUG_STRING_X_OFFSET, height + NUM_FLAKES_TEXT_Y_OFFSET, textPaint);
			canvas.drawText(fpsString, DEBUG_STRING_X_OFFSET, height + FPS_TEXT_Y_OFFSET, textPaint);
		}
	}

	private void repositionFlakes(Canvas canvas, List<Flake> flakes) {
		// For each flake: back-translate by half its size (this allows it to
		// rotate around its center), rotate by its current rotation, translate
		// by its location, then draw its bitmap
		for (int i = 0; i < flakes.size(); ++i) {
			Flake flake = flakes.get(i);
			if (!flake.isRenderable()) {
				continue;
			}
			m.setTranslate(-flake.getWidth() / 2, -flake.getHeight() / 2);
			m.postRotate(flake.getRotationAngle());
			m.postTranslate(flake.getWidth() / 2 + flake.getX(), flake.getHeight() / 2 + flake.getY());
			canvas.drawBitmap(flake.getBitmap(), m, bitmapPaint);
		}
	}

	private void calculateFramesPerSecond(long timeNow) {
		++frames;
		long deltaTime = timeNow - startTime;
		if (deltaTime > UPDATE_TIME_THRESHOLD) {
			float secs = deltaTime / NUMBER_MILLIS_IN_SECOND;
			framesPerSecond = frames / secs;
			fpsString = "fps: " + framesPerSecond;
			startTime = timeNow;
			frames = 0;
		}
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public void setPrevTime(long prevTime) {
		this.prevTime = prevTime;
	}

	public long getStartTime() {
		return startTime;
	}

	public long getPrevTime() {
		return prevTime;
	}

	public void setFrames(int frames) {
		this.frames = frames;
	}

	public int getBitmapPaintAlpha() {
		return bitmapPaint.getAlpha();
	}

	public void setBitmapPaintAlpha(int alpha) {
		int newAlpha = alpha;
		if (alpha < MIN_ALPHA) {
			newAlpha = MIN_ALPHA;
		} else if (alpha > MAX_ALPHA) {
			newAlpha = MAX_ALPHA;
		}
		bitmapPaint.setAlpha(newAlpha);
	}

	public String getFpsString() {
		return fpsString;
	}

	public void setFpsString(String fpsString) {
		this.fpsString = fpsString;
	}

	public String getFlakeString() {
		return flakeString;
	}

	public void setFlakeString(String flakeString) {
		this.flakeString = flakeString;
	}
}
