package com.tescobank.mobile.ui.seasonal.flake;

import com.tescobank.mobile.ui.seasonal.flake.Flake.Direction;


public interface FlakePropertyGenerator {
	int calculateHeight(Flake flake, float hwRatio);
	int calculateWidth(Flake flake, int containerWidth);
	float calculateRotationSpeed(Flake flake);
	float calculateRotationAngle(Flake flake);
	float calculateXPosition(Flake flake, int containerWidth);
	float calculateYPosition(Flake flake, int containerHeight);
	Direction calculateXDirection();
	float calculateXSpeed();
	float calculateSpeed(Flake flake, int screenHeight);
}
