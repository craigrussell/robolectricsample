package com.tescobank.mobile.ui.seasonal;

import java.io.File;
import java.util.List;

import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.seasonal.imagequalifier.ImageNameDensityQualifier;

public class SeasonalAnimationImagePreloader extends AbstractImagePreloader {

	protected static final String TAG = SeasonalAnimationImagePreloader.class.getSimpleName();

	/**
	 * Construct a pre-loader.
	 * 
	 * @param activity
	 *            An activity to use for settings / file locations, etc
	 * @param listener
	 *            can be null
	 */
	public SeasonalAnimationImagePreloader(TescoActivity activity, SeasonalImageObserver listener) {
		this(activity, listener, new SeasonalImageDownloader(activity), new BitmapLoaderFileSystem());
	}

	SeasonalAnimationImagePreloader(TescoActivity activity, SeasonalImageObserver listener, ImageDownloader downloader, BitmapLoader bitmapLoader) {
		this.activity = activity;
		this.setListener(listener);
		this.setDownloader(downloader);
		this.setBitmapLoader(bitmapLoader);
		setImageCache(new SeasonalImageCache());
		nameQualifier = new ImageNameDensityQualifier(activity);
	}

	@Override
	protected List<String> getImagePaths() {
		return activity.getApplicationState().getSettings().getSeasonalImages();
	}

	@Override
	protected File getImageCacheFolder() {
		return new File(activity.getCacheDir(), "seasonal");
	}
}
