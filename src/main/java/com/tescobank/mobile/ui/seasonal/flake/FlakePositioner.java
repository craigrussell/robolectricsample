package com.tescobank.mobile.ui.seasonal.flake;

import java.util.Random;

public class FlakePositioner {
	private static final int DEGREES_FULL_CIRCLE = 360;
	private Random random;
	private int screenWidth;
	private int screenHeight;

	public FlakePositioner(int screenWidth, int screenHeight) {
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
		random = new Random();
	}

	public void moveFlakeDownScreen(Flake flake, float secs) {
		flake.setY(flake.getY() + (flake.getSpeed() * secs));
	}

	public void moveFlakeAcrossScreen(Flake flake, float secs) {
		float currentX = flake.getX();
		switch (flake.getXDirection()) {
		case LEFT:
			flake.setX(currentX - flake.getXSpeed() * secs);
			break;
		case RIGHT:
			flake.setX(currentX + flake.getXSpeed() * secs);
			break;
		default:
			break;
		}
	}

	public void moveFlakeToLeftBound(Flake flake) {
		flake.setX(-flake.getWidth());
	}

	public void moveFlakeToRightBound(Flake flake) {
		flake.setX(screenWidth + flake.getWidth());
	}

	public void moveFlakeAboveView(Flake flake) {
		float range = screenHeight / 4;
		int nearestInteger = Math.round(range);
		if (nearestInteger == 0) {
			nearestInteger = 1;
		}
		int randomOffset = random.nextInt(nearestInteger);
		flake.setY(-randomOffset);
	}

	public boolean flakeOutOfLeftBound(Flake flake) {
		return flake.getX() + flake.getWidth() <= 0;
	}

	public boolean flakeOutOfRightBound(Flake flake) {
		return flake.getX() - flake.getWidth() >= screenWidth;
	}

	public boolean flakeBelowLowerBound(Flake flake) {
		return flake.getY() - flake.getHeight() >= screenHeight;
	}

	public void rotateFlake(Flake flake, float secs) {
		float oldRotationAngle = flake.getRotationAngle();
		float rotationSpeed = flake.getRotationSpeed();
		float newRotationAngle = oldRotationAngle + rotationSpeed * secs;
		float remainder = newRotationAngle % DEGREES_FULL_CIRCLE;
		flake.setRotationAngle(remainder);
	}

	public boolean flakeHasLeftScreen(Flake flake) {
		if (flakeOutOfLeftBound(flake)) {
			return true;
		}
		if (flakeOutOfRightBound(flake)) {
			return true;
		}
		if (flakeBelowLowerBound(flake)) {
			return true;
		}

		return false;
	}

	void setRandom(Random random) {
		this.random = random;
	}

	public void setContainerWidth(int containerWidth) {
		this.screenWidth = containerWidth;
	}
	
	public void setContainerHeight(int containerHeight) {
		this.screenHeight = containerHeight;
	}

}
