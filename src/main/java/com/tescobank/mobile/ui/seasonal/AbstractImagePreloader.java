package com.tescobank.mobile.ui.seasonal;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.properties.TescoApplicationPropertiesReader.ApplicationPropertyKeys;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.seasonal.imagequalifier.ImageNameQualifier;

public abstract class AbstractImagePreloader {
	protected static final String TAG = AbstractImagePreloader.class.getSimpleName();

	protected TescoActivity activity;
	private SeasonalImageObserver listener;
	private ImageDownloader downloader;
	private BitmapLoader bitmapLoader;
	private ImageCache imageCache;

	protected ImageNameQualifier nameQualifier;
	protected int finishedCount;

	protected abstract File getImageCacheFolder();

	protected abstract List<String> getImagePaths();

	public AbstractImagePreloader() {
		
	}

	public void start() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "start()");
		}

		processImages();
	}

	protected void processImages() {
		List<String> images = getImagePaths();

		if (images == null || images.isEmpty()) {
			deleteImageCache();
			notifyFinished();
		} else {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "using images: " + images);
			}
			preloadImages(images);
		}
	}

	protected void deleteImageCache() {
		getImageCache().deleteFolderRecursively(getImageCacheFolder());
	}

	protected void updatedFinishedCount(int max) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Updating finished count (" + finishedCount + "/" + max + ")");
		}

		finishedCount++;
		if (finishedCount >= max) {
			notifyFinished();
		}
	}

	protected void preloadImages(List<String> images) {
		for (String image : images) {
			image = image.trim();

			if (!fileAlreadyExists(image)) {
				download(image, images.size());
			} else {
				updatedFinishedCount(images.size());
			}
		}
	}

	private boolean fileAlreadyExists(String image) {
		return getImageCache().exists(image, getImageCacheFolder());
	}

	public List<Bitmap> getBitmapImages() {
		List<String> imagePaths = getImagePaths();
		File folder = getImageCacheFolder();
		return getBitmapImages(imagePaths, folder);
	}

	protected List<Bitmap> getBitmapImages(List<String> imagePaths, File folder) {
		if (null == imagePaths) {
			return Collections.<Bitmap> emptyList();
		}

		List<Bitmap> images = new LinkedList<Bitmap>();

		for (String imagePath : imagePaths) {
			Bitmap bm = getBitmap(folder, imagePath);
			if (bm != null) {
				images.add(bm);
			}
		}
		return images;
	}

	protected Bitmap getBitmap(File folder, String imagePath) {
		String absoluteUrl = getAbsoluteImageUrl(imagePath);
		String encodedImagePath = Base64.encodeToString(absoluteUrl.getBytes(), Base64.DEFAULT);
		Bitmap bm = getBitmapLoader().load(encodedImagePath, folder);
		return bm;
	}

	protected void notifyFinished() {
		if (activity.isFinishing()) {
			return;
		}

		if (getListener() == null) {
			return;
		}

		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				getListener().finished();
			}
		});
	}

	protected String getAbsoluteImageUrl(String image) {
		if (image.startsWith("http")) {
			return image;
		}
		String pdfBase = activity.getApplicationState().getApplicationPropertyValueForKey(ApplicationPropertyKeys.PDF_API_BASE);
		return pdfBase + image;
	}

	protected void download(final String image, final int imageCount) {
		final String absoluteUrl = getAbsoluteImageUrl(image);

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "downloading " + image + " from " + absoluteUrl);
		}

		DownloadListener downloadListener = buildDownloadListener(absoluteUrl, imageCount);
		getDownloader().download(absoluteUrl, downloadListener);
	}

	protected DownloadListener buildDownloadListener(final String imageName, final int imageCount) {
		return new DownloadListener() {

			@Override
			public void success(byte[] data) {
				File imageFile = getImageCache().getCachedImageFile(imageName, getImageCacheFolder());
				try {
					getImageCache().saveImage(imageFile, data);
				} catch (IOException e) {
					if (BuildConfig.DEBUG) {
						Log.w(TAG, "Failed to save image to file system", e);
					}
				}
				updatedFinishedCount(imageCount);
			}

			@Override
			public void failed() {
				if (BuildConfig.DEBUG) {
					Log.w(TAG, "Failed to download: " + imageName);
				}
				updatedFinishedCount(imageCount);
			}

		};
	}

	public BitmapLoader getBitmapLoader() {
		return bitmapLoader;
	}

	public void setBitmapLoader(BitmapLoader bitmapLoader) {
		this.bitmapLoader = bitmapLoader;
	}

	public ImageDownloader getDownloader() {
		return downloader;
	}

	public void setDownloader(ImageDownloader downloader) {
		this.downloader = downloader;
	}

	public SeasonalImageObserver getListener() {
		return listener;
	}

	public void setListener(SeasonalImageObserver listener) {
		this.listener = listener;
	}

	public ImageCache getImageCache() {
		return imageCache;
	}

	public void setImageCache(ImageCache imageCache) {
		this.imageCache = imageCache;
	}

	public ImageNameQualifier getNameQualifier() {
		return nameQualifier;
	}

	public void setNameQualifier(ImageNameQualifier nameQualifier) {
		this.nameQualifier = nameQualifier;
	}
	
}
