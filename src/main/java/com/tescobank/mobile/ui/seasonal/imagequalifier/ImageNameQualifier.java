package com.tescobank.mobile.ui.seasonal.imagequalifier;

public interface ImageNameQualifier {
	public String qualifyImageName(String absoluteUrl);
}
