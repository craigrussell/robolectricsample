package com.tescobank.mobile.ui.seasonal.flake;

import android.graphics.Bitmap;


public interface FlakeFactory {
	Flake createFlake(int containerWidth, int containerHeight, int dpWidth, int dpHeight, Bitmap flakeBitmap);
}
