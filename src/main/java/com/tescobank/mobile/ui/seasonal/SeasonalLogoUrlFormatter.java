package com.tescobank.mobile.ui.seasonal;

public class SeasonalLogoUrlFormatter {

	private static final String DOT = ".";
	private static final String SLASH = "/";

	static final String ACTIONBAR_LOGO_PREFIX = "";
	static final String ACTIONBAR_LOGO_PADDED_PREFIX = "actionbar_padded_";
	static final String ACTIONBAR_LOGO_HAMBURGER_PREFIX = "actionbar_hamburger_";
	static final String ACTIONBAR_LOGO_UP_PREFIX = "actionbar_up_";

	private final String filename;

	public SeasonalLogoUrlFormatter(String filename) {
		verifyValidFilename(filename);
		this.filename = filename;
	}

	private void verifyValidFilename(String filename) {
		if (filename == null || filename.length() == 0) {
			throw new IllegalArgumentException("A filename must be provided");
		}
	}

	public String getFilename() {
		String lastPartOfUrl = extractLastPartOfUrl();
		if (lastPartOfUrl == null) {
			return null;
		}

		return lastPartOfUrl;
	}

	public String getNameWithoutExtension() {
		String lastPartOfUrl = extractLastPartOfUrl();
		if (lastPartOfUrl == null) {
			return null;
		}

		if (!lastPartOfUrl.contains(DOT))
		{
			return lastPartOfUrl;
		}

		return extractFilenameWithoutExtension(lastPartOfUrl);
	}

	private String extractFilenameWithoutExtension(String file) {
		return file.substring(0, file.lastIndexOf(DOT));
	}

	private String extractLastPartOfUrl() {
		String lastPartOfUrl;
		if (filename.endsWith(SLASH)) {
			return null;
		}
		if (filename.contains(SLASH)) {
			lastPartOfUrl = filename.substring(filename.lastIndexOf(SLASH) + 1, filename.length());
		}
		else {
			lastPartOfUrl = filename;
		}
		return lastPartOfUrl;
	}

	public String getExtension() {
		String lastPartOfUrl = extractLastPartOfUrl();
		if (lastPartOfUrl == null) {
			return null;
		}

		if (!lastPartOfUrl.contains(DOT)) {
			return null;
		}

		if (lastPartOfUrl.endsWith(DOT)) {
			return null;
		}

		return lastPartOfUrl.substring(lastPartOfUrl.lastIndexOf(DOT) + 1);
	}

	public String getPathWithoutFilename() {
		String lastPartOfUrl = extractLastPartOfUrl();
		if (lastPartOfUrl == null) {
			return lastPartOfUrl;
		}

		if (lastPartOfUrl.equals(filename)) {
			return null;
		}

		return filename.substring(0, filename.lastIndexOf(lastPartOfUrl));
	}

	public String getNameForPaddedLogo() {
		return buildUrlForType(ACTIONBAR_LOGO_PADDED_PREFIX);
	}

	public String getNameForHamburgerLogo() {
		return buildUrlForType(ACTIONBAR_LOGO_HAMBURGER_PREFIX);
	}

	public String getNameForLogo() {
		return buildUrlForType(ACTIONBAR_LOGO_PREFIX);
	}

	public String getNameForLogoUp() {
		return buildUrlForType(ACTIONBAR_LOGO_UP_PREFIX);
	}

	private String buildUrlForType(String type) {
		String filename = extractLastPartOfUrl();
		String path = getPathWithoutFilename();

		StringBuilder sb = new StringBuilder();
		if (path != null) {
			sb.append(path);
		}
		sb.append(type);
		sb.append(filename);
		return sb.toString();
	}
}
