package com.tescobank.mobile.ui.seasonal;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.util.Base64;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;

public class SeasonalImageCache implements ImageCache {

	private static final String TAG = SeasonalImageCache.class.getSimpleName();

	@Override
	public void saveImage(File imageFile, byte[] bytes) throws IOException {
		verifyValidImageToSave(imageFile, bytes);

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Saving image to " + imageFile);
		}

		imageFile.getParentFile().mkdirs();
		writeImageToFile(imageFile, bytes);
	}

	private void verifyValidImageToSave(File imageFile, byte[] bytes) {
		if (bytes == null) {
			throw new IllegalArgumentException("image bytes cannot be null");
		}

		if (imageFile == null) {
			throw new IllegalArgumentException("image path must be specified");
		}
	}

	private void writeImageToFile(File imageFile, byte[] bytes) throws IOException {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(imageFile);
			out.write(bytes);
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				// ignore
			}
		}
	}

	@Override
	public void deleteFolderRecursively(File folder) {
		File[] files = folder.listFiles();

		if(files == null) {
			deleteEmptyFolder(folder);
			return;
		}
		
		for (File file : files) {
			if (file.isDirectory()) {
				deleteFolderRecursively(file);
			} else {
				file.delete();
			}
		}

		deleteEmptyFolder(folder);
	}
	
	private void deleteEmptyFolder(File folder) {
		boolean successfulDeletion = folder.delete();
		if(BuildConfig.DEBUG) {
			Log.d(TAG, String.format("Deletion of folder %s, was removed successfully? %s", folder.getAbsolutePath(), successfulDeletion));
		}
	}

	@Override
	public File getCachedImageFile(String inputImagePath, File folder) {
		String decodedImagePath = Base64.encodeToString(inputImagePath.getBytes(), Base64.DEFAULT);
		return new File(folder, decodedImagePath);
	}

	@Override
	public boolean exists(String imageName, File folder) {
		return getCachedImageFile(imageName, folder).exists();
	}
}
