package com.tescobank.mobile.ui.seasonal.imagequalifier;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.seasonal.SeasonalLogoUrlFormatter;

public class ImageNameDensityQualifier implements ImageNameQualifier {
	
	private final String screenDensity;
	
	public ImageNameDensityQualifier(TescoActivity activity) {
		if (activity == null) {
			throw new IllegalArgumentException("Activty cannot be null");
		}
		screenDensity = activity.getString(R.string.seasonal_logo_screen_density_url_qualifier);
	}
	
	@Override
	public String qualifyImageName(String absoluteUrl) {
		SeasonalLogoUrlFormatter formatter = new SeasonalLogoUrlFormatter(absoluteUrl);

		StringBuilder sb = new StringBuilder();
		appendIfNotNull(sb, formatter.getPathWithoutFilename());
		appendIfNotNull(sb, formatter.getNameWithoutExtension());
		sb.append("-");
		appendIfNotNull(sb, screenDensity);
		appendExtensionIfExists(formatter, sb);
		return sb.toString();
	}

	private void appendExtensionIfExists(SeasonalLogoUrlFormatter formatter, StringBuilder sb) {
		String extension = formatter.getExtension();
		if (extension != null) {
			sb.append(".");
			sb.append(extension);
		}
	}

	private void appendIfNotNull(StringBuilder sb, String nullableString) {
		if (nullableString != null) {
			sb.append(nullableString);
		}
	}
}
