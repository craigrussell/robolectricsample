/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tescobank.mobile.ui.seasonal.flake;

import android.graphics.Bitmap;

/**
 * This class represents a single flake, with properties representing its size,
 * rotation, location, and speed.
 */
public class Flake {

	private float x;
	private float y;
	private float rotationAngle;
	private float speed;
	private float rotationSpeed;
	private int width;
	private int height;
	private Bitmap bitmap;
	private Direction xDirection;
	private float xSpeed;
	private boolean renderable;

	public enum Direction {
		
		LEFT(), STATIONERY(), RIGHT();
		
		public static final Direction DEFAULT_DIRECTION = STATIONERY;

		public static Direction fromInt(int value) {
			if(value < 0) {
				return DEFAULT_DIRECTION;
			}
			
			if(value > Direction.values().length - 1) {
				return DEFAULT_DIRECTION;
			}
			
			return Direction.values()[value];
		}
	}

	public Flake() {
		xDirection = Direction.STATIONERY;
		renderable = true;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getRotationAngle() {
		return rotationAngle;
	}

	public void setRotationAngle(float rotation) {
		this.rotationAngle = rotation;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public float getRotationSpeed() {
		return rotationSpeed;
	}

	public void setRotationSpeed(float rotationSpeed) {
		this.rotationSpeed = rotationSpeed;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	public Direction getXDirection() {
		return xDirection;
	}

	public void setXDirection(Direction xDirection) {
		this.xDirection = xDirection;
	}

	public float getXSpeed() {
		return xSpeed;
	}

	public void setXSpeed(float xSpeed) {
		this.xSpeed = xSpeed;
	}

	public boolean isRenderable() {
		return renderable;
	}

	public void setRenderable(boolean renderable) {
		this.renderable = renderable;
	}
}
