package com.tescobank.mobile.ui.seasonal.flake;

/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.View;

/**
 * This class is the custom view where all of the Droidflakes are drawn. This
 * class has all of the logic for adding, subtracting, and rendering flakes.
 * 
 * Inspired by the examples given at http://nineoldandroids.com/
 */
public class FlakeView extends View {

	private static final int NUMBER_RANDOM_OPTIONS_FOR_FLAKE_DEATH = 4;
	private static final int FLAKE_ANIMATION_DURATION = 3000;
	private List<Bitmap> flakeBitmaps;
	private final List<Flake> flakes = new ArrayList<Flake>();


	private FlakeViewRenderer flakeRenderer;
	private FlakePositioner flakePositioner;

	private int numberFlakes = 0;
	private final Random random = new Random();

	private ValueAnimator flakeAnimator;

	private int containerWidth;
	private int containerHeight;

	private int dpWidth;
	private int dpHeight;

	private boolean stopping = false;

	private FlakeViewShutdownListener shutdownListener;

	private FlakeFactory flakeFactory = new RandomFlakeFactory();

	private FlakeView(Context context) {
		super(context);
	}

	FlakeView(Context context, List<Bitmap> flakeBitmaps, FlakeViewRenderer renderer, ValueAnimator animator, FlakePositioner positioner, int initialNumberFlakes) {
		super(context);

		if (flakeBitmaps == null || flakeBitmaps.size() == 0) {
			throw new IllegalArgumentException("Must specify at least one bitmap");
		}

		this.flakeBitmaps = flakeBitmaps;
		this.flakeRenderer = renderer;
		this.flakePositioner = positioner;
		this.flakeAnimator = animator;
		this.setNumberFlakes(initialNumberFlakes);
		setup();
	}

	public static FlakeView createFlakeView(Context context, List<Bitmap> bitmaps) {
		return createFlakeView(context, bitmaps, 0, 0, 0);
	}

	public static FlakeView createFlakeView(Context context, List<Bitmap> bitmaps, int containerWidth, int containerHeight, int initialNumberOfFlakes) {
		FlakePositioner positioner = new FlakePositioner(containerWidth, containerHeight);
		FlakeViewRenderer renderer = new FlakeViewRenderer();
		ValueAnimator animator = ValueAnimator.ofFloat(0, 1);

		FlakeView view = new FlakeView(context, bitmaps, renderer, animator, positioner, initialNumberOfFlakes);
		view.setContainerWidth(containerWidth);
		view.setContainerHeight(containerHeight);

		DisplayMetrics dm = context.getResources().getDisplayMetrics();

		view.dpWidth = calculateDPWidth(dm);
		view.dpHeight = calculdateDPHeight(dm);
		return view;
	}

	private static int calculdateDPHeight(DisplayMetrics dm) {
		return Math.round(dm.heightPixels / dm.density);
	}

	private static int calculateDPWidth(DisplayMetrics dm) {
		return Math.round(dm.widthPixels / dm.density);
	}

	private void setup() {

		ValueAnimator.AnimatorUpdateListener animatorUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator arg0) {
				if (isFinished()) {
					return;
				}

				long nowTime = System.currentTimeMillis();
				float secondsSinceLastUpdate = calculateNumberOfSecondsSinceLastUpdate(nowTime);

				getFlakeRenderer().setPrevTime(nowTime);
				updateFlakeLayout(secondsSinceLastUpdate);

				invalidate();
			}
		};

		flakeAnimator.addUpdateListener(animatorUpdateListener);
		flakeAnimator.setRepeatCount(ValueAnimator.INFINITE);
		flakeAnimator.setDuration(FLAKE_ANIMATION_DURATION);
	}

	private void updateFlakeLayout(float secondsSinceLastUpdate) {
		for (int i = 0; i < flakes.size(); ++i) {
			Flake flake = flakes.get(i);

			getFlakePositioner().moveFlakeDownScreen(flake, secondsSinceLastUpdate);
			getFlakePositioner().moveFlakeAcrossScreen(flake, secondsSinceLastUpdate);
			getFlakePositioner().rotateFlake(flake, secondsSinceLastUpdate);

			if (getFlakePositioner().flakeHasLeftScreen(flake)) {
				recycleOrRemoveFlake(flake);
			}
		}
	}

	private void recycleOrRemoveFlake(Flake flake) {
		if (shouldRecycleFlake()) {
			recycleFlake(flake);
		} else {
			flakes.remove(flake);
		}
	}

	void recycleFlake(Flake flake) {
		if (getFlakePositioner().flakeOutOfLeftBound(flake)) {
			getFlakePositioner().moveFlakeToRightBound(flake);
		} else if (getFlakePositioner().flakeOutOfRightBound(flake)) {
			getFlakePositioner().moveFlakeToLeftBound(flake);
		}
		if (getFlakePositioner().flakeBelowLowerBound(flake)) {
			recycleFlakeYAxis(flake);
		}
	}

	public final boolean isFinished() {
		if (stopping && flakes.size() == 0) {
			stop();
			if (shutdownListener != null) {
				shutdownListener.finished();
			}
			return true;
		}
		return false;
	}

	boolean shouldRecycleFlake() {
		return !stopping;
	}

	private void recycleFlakeYAxis(Flake flake) {
		flake.setY(new RandomFlakePropertyGenerator().calculateYPosition(flake, getHeight()));
	}

	private float calculateNumberOfSecondsSinceLastUpdate(long nowTime) {
		return (nowTime - getFlakeRenderer().getPrevTime()) / 1000f;
	}

	public int getNumberOfFlakes() {
		return numberFlakes;
	}

	public void addFlakes(int quantity) {
		for (int i = 0; i < quantity; ++i) {
			Flake flake = flakeFactory.createFlake(containerWidth, containerHeight, dpWidth, dpHeight, getRandomFlakeBitmap());
			flakes.add(flake);
		}
		numberFlakes = flakes.size();
	}

	public void subtractFlakes(int quantity) {
		if (quantity >= flakes.size()) {
			flakes.clear();
		} else {
			for (int i = 0; i < quantity; i++) {
				flakes.remove(flakes.size() - 1);
			}
		}
		numberFlakes = flakes.size();
	}

	private Bitmap getRandomFlakeBitmap() {
		int numAvailable = flakeBitmaps.size();
		int randomFlakeIndex = new Random().nextInt(numAvailable);
		return flakeBitmaps.get(randomFlakeIndex);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		getFlakeRenderer().draw(canvas, flakes, getHeight(), System.currentTimeMillis());
	}

	public void stop() {
		flakeAnimator.cancel();
		flakes.clear();
	}

	public void initiateFadeout(final FlakeViewShutdownListener flakeViewShutdownListener) {

		stopping = true;

		final ValueAnimator fadeoutAnimator = ValueAnimator.ofInt(255, 0);
		fadeoutAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				Integer alpha = (Integer) animation.getAnimatedValue();
				getFlakeRenderer().setBitmapPaintAlpha(alpha);
				if (alpha == 0 && flakeViewShutdownListener != null) {
					flakeViewShutdownListener.finished();
				}

				invalidate();
			}
		});

		fadeoutAnimator.setDuration(1500);
		fadeoutAnimator.setRepeatMode(ValueAnimator.INFINITE);
		fadeoutAnimator.start();
	}

	public void initiateShutdown(FlakeViewShutdownListener listener) {
		this.shutdownListener = listener;

		stopping = true;
		List<Flake> flakesToRemove = new ArrayList<Flake>();
		for (Flake flake : flakes) {
			if (flakeIsAboveView(flake)) {
				boolean flakeWillSurvive = shouldFlakeSurvive();
				if (!flakeWillSurvive) {
					flake.setRenderable(false);
					flakesToRemove.add(flake);
				} else {
					getFlakePositioner().moveFlakeAboveView(flake);
				}
			}
		}

		for (Flake flake : flakesToRemove) {
			flakes.remove(flake);
		}

		numberFlakes = flakes.size();
	}

	private boolean shouldFlakeSurvive() {
		int chance = random.nextInt(NUMBER_RANDOM_OPTIONS_FOR_FLAKE_DEATH);
		if (chance > 0) {
			return false;
		}
		return true;
	}

	private boolean flakeIsAboveView(Flake flake) {
		return flake.getY() < 0;
	}

	public void start() {
		// Set up fps tracking and start the animation
		long startTime = System.currentTimeMillis();
		flakeRenderer.setStartTime(startTime);
		flakeRenderer.setPrevTime(startTime);
		flakeRenderer.setFrames(0);
		flakeRenderer.setBitmapPaintAlpha(FlakeViewRenderer.MAX_ALPHA);

		flakeAnimator.start();
	}

	public boolean isRunning() {
		return flakeAnimator.isRunning();
	}

	public FlakeFactory getFlakeFactory() {
		return flakeFactory;
	}

	public void setFlakeFactory(FlakeFactory flakeFactory) {
		this.flakeFactory = flakeFactory;
	}

	boolean isStopping() {
		return stopping;
	}

	public void setStopping(boolean stopping) {
		this.stopping = stopping;
	}

	public FlakePositioner getFlakePositioner() {
		return flakePositioner;
	}

	public void setFlakePositioner(FlakePositioner flakePositioner) {
		this.flakePositioner = flakePositioner;
	}

	public FlakeViewRenderer getFlakeRenderer() {
		return flakeRenderer;
	}

	public void setFlakeRenderer(FlakeViewRenderer flakeRenderer) {
		this.flakeRenderer = flakeRenderer;
	}

	public int getContainerWidth() {
		return containerWidth;
	}

	public void setContainerWidth(int containerWidth) {
		this.containerWidth = containerWidth;
		this.flakePositioner.setContainerWidth(containerWidth);
	}

	public int getContainerHeight() {
		return containerHeight;
	}

	public void setContainerHeight(int containerHeight) {
		this.containerHeight = containerHeight;
		this.flakePositioner.setContainerHeight(containerHeight);
	}

	public final void setNumberFlakes(int numberFlakes) {
		flakes.clear();
		addFlakes(numberFlakes);
	}

	public List<Flake> getFlakes() {
		return flakes;
	}

}
