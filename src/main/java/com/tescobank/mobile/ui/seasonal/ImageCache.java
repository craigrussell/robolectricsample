package com.tescobank.mobile.ui.seasonal;

import java.io.File;
import java.io.IOException;

public interface ImageCache {
	public void saveImage(File imageFile, byte[] bytes) throws IOException;
	public void deleteFolderRecursively(File folder);
	public File getCachedImageFile(String image, File seasonalImageCacheFolder);
	public boolean exists(String imageName, File folder);
}
