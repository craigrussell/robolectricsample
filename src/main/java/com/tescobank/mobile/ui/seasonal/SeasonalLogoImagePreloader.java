package com.tescobank.mobile.ui.seasonal;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.configuration.SettingsResponse;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.seasonal.imagequalifier.ImageNameDensityQualifier;

public class SeasonalLogoImagePreloader extends AbstractImagePreloader {

	private static final String CACHE_DIRECTORY = "seasonal-logo";

	protected static final String TAG = SeasonalLogoImagePreloader.class.getSimpleName();

	public SeasonalLogoImagePreloader(TescoActivity activity, SeasonalImageObserver listener) {
		this(activity, listener, new SeasonalImageDownloader(activity), new BitmapLoaderFileSystem());
	}

	SeasonalLogoImagePreloader(TescoActivity activity, SeasonalImageObserver listener, ImageDownloader downloader, BitmapLoader bitmapLoader) {
		super();
		this.activity = activity;
		this.setListener(listener);
		this.setDownloader(downloader);
		this.setBitmapLoader(bitmapLoader);
		setImageCache(new SeasonalImageCache());
		nameQualifier = new ImageNameDensityQualifier(activity);
	}

	public Bitmap getBitmapForLogo() {
		SeasonalLogoUrlFormatter formatter = buildFormatterForBasePath();
		if (formatter == null) {
			return null;
		}
		String imageName = formatter.getNameForLogo();
		return getBitmap(getImageCacheFolder(), imageName);
	}

	public Bitmap getBitmapForHamburger() {
		SeasonalLogoUrlFormatter formatter = buildFormatterForBasePath();
		if (formatter == null) {
			return null;
		}
		String imageName = formatter.getNameForHamburgerLogo();
		return getBitmap(getImageCacheFolder(), imageName);
	}

	public Bitmap getBitmapForUp() {
		SeasonalLogoUrlFormatter formatter = buildFormatterForBasePath();
		if (formatter == null) {
			return null;
		}
		String imageName = formatter.getNameForLogoUp();
		return getBitmap(getImageCacheFolder(), imageName);
	}

	public Bitmap getBitmapForPadded() {
		SeasonalLogoUrlFormatter formatter = buildFormatterForBasePath();
		if (formatter == null) {
			return null;
		}
		String imageName = formatter.getNameForPaddedLogo();
		return getBitmap(getImageCacheFolder(), imageName);
	}

	private SeasonalLogoUrlFormatter buildFormatterForBasePath() {
		SettingsResponse settings = activity.getApplicationState().getSettings();
		if (settings == null) {
			return null;
		}

		String basePath = settings.getSeasonalLogoPath();
		if (basePath == null) {
			return null;
		}

		String absoluteUrl = getAbsoluteImageUrl(basePath);
		return new SeasonalLogoUrlFormatter(absoluteUrl);
	}

	@Override
	protected List<String> getImagePaths() {
		String logoImage = activity.getApplicationState().getSettings().getSeasonalLogoPath();
		if (logoImage != null) {
			return buildListOfImagesToDownload(logoImage);
		}
		return Collections.emptyList();
	}

	@Override
	protected File getImageCacheFolder() {
		return new File(activity.getCacheDir(), CACHE_DIRECTORY);
	}

	@Override
	protected void download(final String image, final int imageCount) {
		String absoluteUrl = getAbsoluteImageUrl(image);
		downloadImages(absoluteUrl, imageCount);
	}

	private void downloadImages(String image, int imageCount) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "downloading " + image);
		}

		DownloadListener downloadListener = buildDownloadListener(image, imageCount);
		getDownloader().download(image, downloadListener);
	}

	@Override
	protected Bitmap getBitmap(File folder, String imagePath) {
		String qualifiedImage = nameQualifier.qualifyImageName(imagePath);
		String encodedImagePath = Base64.encodeToString(qualifiedImage.getBytes(), Base64.DEFAULT);
		Bitmap bm = getBitmapLoader().load(encodedImagePath, folder);
		if(bm != null) {
			int density = ScreenDensityFormatter.getDensity(activity);
			bm.setDensity(density);
		}
		return bm;
	}

	private List<String> buildListOfImagesToDownload(String url) {
		SeasonalLogoUrlFormatter formatter = new SeasonalLogoUrlFormatter(url);
		List<String> images = new ArrayList<String>();

		images.add(nameQualifier.qualifyImageName(formatter.getNameForLogo()));
		images.add(nameQualifier.qualifyImageName(formatter.getNameForPaddedLogo()));
		images.add(nameQualifier.qualifyImageName(formatter.getNameForHamburgerLogo()));
		images.add(nameQualifier.qualifyImageName(formatter.getNameForLogoUp()));

		return images;
	}
}
