package com.tescobank.mobile.ui.seasonal;

import android.app.Activity;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;

public abstract class ScreenDensityFormatter {

	static final int DEFAULT_DENSITY = 240;
	private static final String TAG = ScreenDensityFormatter.class.getSimpleName();

	public static int getDensity(Activity activity) {
		String densityString = activity.getString(R.string.seasonal_logo_screen_density_url_qualifier);
		String densityFromConfig = activity.getString(R.string.seasonal_logo_screen_density_int);
		if (BuildConfig.DEBUG) {
			Log.d(TAG, String.format("Retrieved density qualifier %s with density value %s from resources", densityString, densityFromConfig));
		}
		return convertDensityToInt(densityFromConfig);
	}

	private static int convertDensityToInt(String densityFromConfig) {
		try {
			return Integer.parseInt(densityFromConfig);
		} catch (NumberFormatException e) {
			if (BuildConfig.DEBUG) {
				Log.w(TAG, String.format("Failed to convert value %s to integer. Using default of %d instead", densityFromConfig, DEFAULT_DENSITY));
			}
			return DEFAULT_DENSITY;
		}
	}

}
