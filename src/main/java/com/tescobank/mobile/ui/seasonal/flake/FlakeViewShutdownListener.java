package com.tescobank.mobile.ui.seasonal.flake;

public interface FlakeViewShutdownListener {
	void finished();
}
