package com.tescobank.mobile.ui.seasonal.flake;

import java.util.Random;

import android.graphics.Bitmap;

import com.tescobank.mobile.ui.seasonal.flake.Flake.Direction;

public class RandomFlakePropertyGenerator implements FlakePropertyGenerator {

	private static final int DEFAULT_MAXIMUM_FLAKE_WIDTH_PERCENTAGE = 10;
	private static final int DEFAULT_MINIMUM_FLAKE_WIDTH_PERCENTAGE = 2;
	private static final int DEGREES_45 = 45;
	private static final int DEGREES_90 = 90;

	private Random random;

	/**
	 * This is the minimum percentage of the screen width which the flake width
	 * can be.
	 */
	private int minimumFlakeWidthPercentage;

	/**
	 * This is the maximum percentage of the screen width which the flake width
	 * can be.
	 */
	private int maximumFlakeWidthPercentage;

	public RandomFlakePropertyGenerator() {
		random = new Random();
		minimumFlakeWidthPercentage = DEFAULT_MINIMUM_FLAKE_WIDTH_PERCENTAGE;
		maximumFlakeWidthPercentage = DEFAULT_MAXIMUM_FLAKE_WIDTH_PERCENTAGE;
	}

	@Override
	public float calculateRotationSpeed(Flake flake) {
		// rotate between -45 and 45 degrees per second
		return (float) Math.random() * DEGREES_90 - DEGREES_45;
	}

	@Override
	public float calculateRotationAngle(Flake flake) {
		return randomFloatWithinRangeExclusive(-DEGREES_90, DEGREES_90);
	}

	@Override
	public float calculateSpeed(Flake flake, int containerHeight) {
		int minScalePercentage = 5;
		int maxScalePercentage = 30;
		int randomScalePercentage = random.nextInt((maxScalePercentage - minScalePercentage) + 1) + minScalePercentage;
		float pixelsPerSecond = containerHeight * (randomScalePercentage / 100f);
		return pixelsPerSecond;
	}

	@Override
	public float calculateYPosition(Flake flake, int containerHeight) {
		// Position the flake vertically slightly off the top of the display
		return 0 - (float) Math.random() * containerHeight * 1.25f - flake.getHeight();
	}

	@Override
	public float calculateXPosition(Flake flake, int containerWidth) {
		// Position the flake horizontally between the left and right of the
		// range
		return (float) Math.random() * (containerWidth - flake.getWidth());
	}

	public float calculateHeightWidthRatio(Bitmap flakeBitmap) {
		verifyValidBitmap(flakeBitmap);
		
		return (float)flakeBitmap.getHeight() / (float)flakeBitmap.getWidth();
	}

	private void verifyValidBitmap(Bitmap flakeBitmap) {
		if(flakeBitmap == null) {
			throw new IllegalArgumentException("Bitmap cannot be null");
		}

		if(flakeBitmap.getHeight() == 0 || flakeBitmap.getWidth() == 0) {
			throw new IllegalArgumentException("Invalid bitmap");
		}
	}

	@Override
	public int calculateHeight(Flake flake, float hwRatio) {
		return (int) (flake.getWidth() * hwRatio);
	}

	@Override
	public int calculateWidth(Flake flake, int dpWidth) {
		int randomScalePercentage = randomIntWithinRangeInclusive(minimumFlakeWidthPercentage, maximumFlakeWidthPercentage);
		if (randomScalePercentage == 0) {
			return 0;
		}
		
		float width = dpWidth * (randomScalePercentage/100f);
		return Math.round(width);
	}

	public void setRandom(Random random) {
		this.random = random;
	}

	@Override
	public Direction calculateXDirection() {
		int max = 2;
		int min = 0;
		int dir = randomIntWithinRangeInclusive(min, max);
		return Direction.fromInt(dir);
	}

	private int randomIntWithinRangeInclusive(int min, int max) {
		return random.nextInt((max - min) + 1) + min;
	}
	
	float randomFloatWithinRangeExclusive(float min, float max) {
		return random.nextFloat() * (max-min) + min;
	}

	@Override
	public float calculateXSpeed() {
		int random = randomIntWithinRangeInclusive(0, 100);
		return random;
	}

	int getMinimumFlakeWidthPercentage() {
		return minimumFlakeWidthPercentage;
	}

	void setMinimumFlakeWidthPercentage(int minimumFlakeWidthPercentage) {
		if (minimumFlakeWidthPercentage < 0) {
			throw new IllegalArgumentException("Minimum width percentage cannot be negative");
		}
		this.minimumFlakeWidthPercentage = minimumFlakeWidthPercentage;
	}

	int getMaximumFlakeWidthPercentage() {
		return maximumFlakeWidthPercentage;
	}

	void setMaximumFlakeWidthPercentage(int maximumFlakeWidthPercentage) {
		if (maximumFlakeWidthPercentage < 0) {
			throw new IllegalArgumentException("Maximum width percentage cannot be negative");
		}
		this.maximumFlakeWidthPercentage = maximumFlakeWidthPercentage;
	}
}
