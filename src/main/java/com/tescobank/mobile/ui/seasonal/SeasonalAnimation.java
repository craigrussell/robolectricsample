package com.tescobank.mobile.ui.seasonal;

import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.PopupWindow;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.seasonal.flake.FlakeView;
import com.tescobank.mobile.ui.seasonal.flake.FlakeViewShutdownListener;

public class SeasonalAnimation {

	private static final String TAG = SeasonalAnimation.class.getSimpleName();

	private final PopupWindow popupWindow;
	private FlakeView flakeView;
	private final int numberItemsToAnimate;

	public SeasonalAnimation(Activity activity, List<Bitmap> flakeBitmaps, int numberItemsToAnimate) {
		verifyValidityNumberOfItems(numberItemsToAnimate);
		verifyEnoughBitmapsProvided(flakeBitmaps);

		this.numberItemsToAnimate = numberItemsToAnimate;

		LayoutInflater layoutInflater = LayoutInflater.from(activity);
		FrameLayout popupViewContainer = (FrameLayout) layoutInflater.inflate(R.layout.seasonal_animation_overlay, null);

		this.flakeView = createFlakeView(flakeBitmaps, activity);
		this.popupWindow = createPopupWindow(popupViewContainer);
		addFlakeView(popupViewContainer);
	}

	private void verifyEnoughBitmapsProvided(List<Bitmap> flakeBitmaps) {
		if (flakeBitmaps == null || flakeBitmaps.size() == 0) {
			throw new IllegalArgumentException("Must provide at least one bitmap");
		}
	}

	private void verifyValidityNumberOfItems(int numberItemsToAnimate) {
		if (numberItemsToAnimate < 0) {
			throw new IllegalArgumentException("Number of items cannot be negative");
		}
	}

	public void dismissGradually(boolean fadeoutAnimation) {
		FlakeViewShutdownListener listener = new FlakeViewShutdownListener() {
			@Override
			public void finished() {
				cleanupViews();
			}
		};

		if (fadeoutAnimation) {
			flakeView.initiateFadeout(listener);
		} else {
			flakeView.initiateShutdown(listener);
		}
	}

	public void dismiss() {
		cleanupViews();
	}

	private void cleanupViews() {
		flakeView.stop();
		safelyDismissPopup();
	}

	private void safelyDismissPopup() {
		try {
			popupWindow.dismiss();
		} catch (IllegalArgumentException e) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "View failed to dismiss; likely not attached", e);
			}
		}
	}

	public void show(Activity activity) {
		show(activity, numberItemsToAnimate);
	}

	public void show(Activity activity, int numberFlakes) {
		if (!isShowing()) {
			View windowDecor = activity.getWindow().getDecorView();
			flakeView.setContainerWidth(windowDecor.getWidth());
			flakeView.setContainerHeight(windowDecor.getHeight());
			flakeView.setNumberFlakes(numberFlakes);
			flakeView.setStopping(false);
			displayPopupWindow(activity);
			animateFlakeView();
		}
	}

	private void animateFlakeView() {
		flakeView.start();
	}

	private void addFlakeView(FrameLayout container) {
		container.addView(flakeView);
	}

	private void displayPopupWindow(Activity activity) {
		View contentView = activity.findViewById(android.R.id.content);
		popupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
		popupWindow.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
		popupWindow.update();
		popupWindow.showAtLocation(contentView, Gravity.TOP | Gravity.LEFT, 0, 0);
	}

	private PopupWindow createPopupWindow(View contentView) {
		PopupWindow popupWindow = new PopupWindow(contentView);
		popupWindow.setTouchable(false);
		popupWindow.update();
		return popupWindow;
	}

	private FlakeView createFlakeView(List<Bitmap> flakeBitmaps, Activity activity) {
		return FlakeView.createFlakeView(activity, flakeBitmaps);
	}

	public boolean isShowing() {
		return flakeView.isRunning();
	}

	public void setFlakeView(FlakeView mockFlakeView) {
		this.flakeView = mockFlakeView;
	}

}
