package com.tescobank.mobile.ui.seasonal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.download.DownloadHelper;
import com.tescobank.mobile.ui.TescoActivity;

public class SeasonalImageDownloader implements ImageDownloader {

	private static final String TAG = SeasonalImageDownloader.class.getSimpleName();
	
	private static final int NUM_BYTES_IN_MEG = 1024;
	private TescoActivity activity = null;
	
	public SeasonalImageDownloader(TescoActivity activity) {
		this.activity = activity;
	}
	
	@Override
	public void download (String pathOfImageToDownload, final DownloadListener listener) {
		getDownloadHelper(listener).execute(pathOfImageToDownload);
	}
	
	protected DownloadHelper<byte[]> getDownloadHelper(final DownloadListener listener) {
		return new DownloadHelper<byte[]>(activity, "image/png") {

			@Override
			public void onResponse(byte[] result) {
				listener.success(result);
			}

			@Override
			public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
				listener.failed();
			}

			@Override
			public byte[] convertInputStreamToType(InputStream inputStream) {
				try {
					return streamToBytes(inputStream);
				} catch (IOException e) {
					if(BuildConfig.DEBUG) {
						Log.w(TAG, "Failed to convert input stream to byte[]", e);
					}
					return null;
				}
			}

			protected byte[] streamToBytes(InputStream inputStream) throws IOException {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				int read = 0;
				byte[] buffer = new byte[NUM_BYTES_IN_MEG];
				while (-1 != (read = inputStream.read(buffer))) {
					out.write(buffer, 0, read);
				}
				return out.toByteArray();
			}
		};
	}
}
