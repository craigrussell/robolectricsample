package com.tescobank.mobile.ui.seasonal;

public interface ImageDownloader {
	public void download(String image, DownloadListener listener);
}
