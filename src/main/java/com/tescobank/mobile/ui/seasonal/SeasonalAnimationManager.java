package com.tescobank.mobile.ui.seasonal;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.ui.ShakeDetector;
import com.tescobank.mobile.ui.ShakeDetector.Listener;
import com.tescobank.mobile.ui.TescoActivity;

public class SeasonalAnimationManager implements Listener {

	private static final int HO_HO_HO_DURATION = 25120;
	private static final int NUMBER_ITEMS_TO_ANIMATE = 80;

	private final TescoActivity activity;
	private SeasonalAnimationImagePreloader imagePreloader;

	private ShakeDetector shakeDetector;
	private SeasonalAnimation seasonalAnimation;
	private ScheduledExecutorService schedule;

	public SeasonalAnimationManager(TescoActivity activity) {
		this.activity = activity;
		this.imagePreloader = new SeasonalAnimationImagePreloader(activity, null);
	}

	public void install() {
		if (imagePreloader.getBitmapImages().isEmpty()) {
			if (BuildConfig.DEBUG) {
				Log.d(getClass().getSimpleName(), "NOT installing ShakeDetector");
			}
			return;
		}

		if (BuildConfig.DEBUG) {
			Log.d(getClass().getSimpleName(), "installing ShakeDetector");
		}
		shakeDetector = new ShakeDetector(activity, this);
	}

	public void uninstall() {
		stopNow();
		if (null != shakeDetector) {
			shakeDetector.dispose();
			shakeDetector = null;
		}
	}

	public void stopNow() {
		if (BuildConfig.DEBUG) {
			Log.d(getClass().getSimpleName(), "stopNow() seasonalAnimation: " + seasonalAnimation);
		}

		if (null != seasonalAnimation) {
			seasonalAnimation.dismiss();
		}
	}
	
	public boolean isShowing() {
		if(seasonalAnimation == null) {
			return false;
		}
		
		return seasonalAnimation.isShowing();
	}
	
	public void stopGradually(boolean useFadeoutAnimation) {
		if (BuildConfig.DEBUG) {
			Log.d(getClass().getSimpleName(), "stopGradually() shakeDetector: " + shakeDetector);
		}

		if (null != seasonalAnimation) {
			endAnimationGradually(useFadeoutAnimation);
		}
	}

	@Override
	public synchronized void onShake() {
		if (BuildConfig.DEBUG) {
			Log.d(getClass().getSimpleName(), "onShake() seasonalAnimation: " + seasonalAnimation);
		}

		if (null == seasonalAnimation || !seasonalAnimation.isShowing()) {
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					startAnimationAndTimer();
				}
			});
		} else {
			endAnimationGradually(true);
		}
	}

	/**
	 * MUST run on UI thread
	 */
	 public void startAnimationAndTimer() {
		if (BuildConfig.DEBUG) {
			Log.d(getClass().getSimpleName(), "startAnimation()");
		}

		showAnimation();
		killExpiryTimer();
		startExpiryTimer();
	}

	private void startExpiryTimer() {
		schedule = new ScheduledThreadPoolExecutor(1);
		schedule.schedule(new Runnable() {
			@Override
			public void run() {
				if (BuildConfig.DEBUG) {
					Log.d(getClass().getSimpleName(), "timer expired");
				}

				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						endAnimationGradually(false);
					}
				});
			}
		}, HO_HO_HO_DURATION, TimeUnit.MILLISECONDS);
	}

	private void showAnimation() {
		if (null == seasonalAnimation) {
			if (BuildConfig.DEBUG) {
				Log.d(getClass().getSimpleName(), "creating SeasonalAnimation");
			}
			seasonalAnimation = new SeasonalAnimation(activity, getImagePreloader().getBitmapImages(), NUMBER_ITEMS_TO_ANIMATE);
		}
		seasonalAnimation.show(activity);
		trackSeasonalEffectStartedAnalytic();
	}

	/** MUST run on the UI thread. 
	 * @param userManuallyDismissed */
	private synchronized void endAnimationGradually(boolean userManuallyDismissed) {
		if (BuildConfig.DEBUG) {
			Log.d(getClass().getSimpleName(), "endAnimationGradually()");
		}

		killExpiryTimer();
		if (null != seasonalAnimation) {
			trackSeasonalEffectStoppedAnalytic(userManuallyDismissed);
			seasonalAnimation.dismissGradually(userManuallyDismissed);
		}
	}

	private void killExpiryTimer() {
		if (null != schedule) {
			schedule.shutdownNow();
			schedule = null;
		}
	}

	public SeasonalAnimationImagePreloader getImagePreloader() {
		return imagePreloader;
	}

	public void setImagePreloader(SeasonalAnimationImagePreloader imagePreloader) {
		this.imagePreloader = imagePreloader;
	}

	public ShakeDetector getShakeDetector() {
		return shakeDetector;
	}

	public void setShakeDetector(ShakeDetector shakeDetector) {
		this.shakeDetector = shakeDetector;
	}

	public SeasonalAnimation getSeasonalAnimation() {
		return seasonalAnimation;
	}

	public void setSeasonalAnimation(SeasonalAnimation seasonalAnimation) {
		this.seasonalAnimation = seasonalAnimation;
	}
	
	private void trackSeasonalEffectStartedAnalytic() {
		activity.getApplicationState().getAnalyticsTracker().trackSeasonalEffectStarted();
	}
	
	private void trackSeasonalEffectStoppedAnalytic(boolean userManuallyDismissed) {
		if(userManuallyDismissed) {
			trackSeasonalEffectStoppedManuallyAnalytic();
		} else {
			trackSeasonalEffectStoppedAutomaticallyAnalytic();
		}
	}

	private void trackSeasonalEffectStoppedAutomaticallyAnalytic() {
		activity.getApplicationState().getAnalyticsTracker().trackSeasonalEffectStoppedAutomatically();
	}

	private void trackSeasonalEffectStoppedManuallyAnalytic() {
		activity.getApplicationState().getAnalyticsTracker().trackSeasonalEffectStoppedManually();
	}
}
