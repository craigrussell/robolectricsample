package com.tescobank.mobile.ui.seasonal.flake;

import android.graphics.Bitmap;
import android.util.SparseArray;

import com.tescobank.mobile.ui.seasonal.flake.Flake.Direction;

public class RandomFlakeFactory implements FlakeFactory {

	private final SparseArray<Bitmap> bitmapCache;
	
	private FlakePropertyGenerator propertyGenerator;
	
	public RandomFlakeFactory() {
		bitmapCache = new SparseArray<Bitmap>();
		propertyGenerator = new RandomFlakePropertyGenerator();
	}
	
	/**
	 * Creates a new flake in the given xRange and with the given bitmap.
	 * Parameters of location, size, rotation, and speed are randomly
	 * determined.
	 */
	@Override
	public Flake createFlake(int containerWidth, int containerHeight, int dpWidth, int dpHeight, Bitmap flakeBitmap) {
		Flake flake = new Flake();
		
		// width
		int width = propertyGenerator.calculateWidth(flake, dpWidth);
		flake.setWidth(width);
		
		// height
		float hwRatio = calculateHeightWidthRatio(flakeBitmap);
		int height = propertyGenerator.calculateHeight(flake, hwRatio);
		flake.setHeight(height);

		// x position
		float x = propertyGenerator.calculateXPosition(flake, containerWidth);
		flake.setX(x);
		
		// y position
		float y = propertyGenerator.calculateYPosition(flake, containerHeight);
		flake.setY(y);

		// speed
		float speed = propertyGenerator.calculateSpeed(flake, containerHeight);
		flake.setSpeed(speed);

		// rotation angle
		float rotationAngle = propertyGenerator.calculateRotationAngle(flake);
		flake.setRotationAngle(rotationAngle);
		
		// rotation speed
		float rotationSpeed = propertyGenerator.calculateRotationSpeed(flake);
		flake.setRotationSpeed(rotationSpeed);

		// bitmap
		Bitmap scaledBitmap = generateScaledBitmap(flakeBitmap, flake);
		flake.setBitmap(scaledBitmap);
		
		// x-direction (is it moving to the left, right or not at all across the screen)
		Direction xDirection = propertyGenerator.calculateXDirection();
		flake.setXDirection(xDirection);
		
		float xSpeed = propertyGenerator.calculateXSpeed();
		flake.setXSpeed(xSpeed);
		
		return flake;
	}

	private float calculateHeightWidthRatio(Bitmap flakeBitmap) {
		return (float)flakeBitmap.getHeight() / flakeBitmap.getWidth();
	}

	private Bitmap generateScaledBitmap(Bitmap flakeBitmap, Flake flake) {
		// Get the cached bitmap for this size if it exists, otherwise create
		// and cache one
		Bitmap scaledBitmap = getCachedBitmap(flake.getWidth());
		if(scaledBitmap == null) {
			scaledBitmap = createNewBitmap(flakeBitmap, flake.getWidth(), flake.getHeight());
			addBitmapToCache(scaledBitmap, flake.getWidth());
		}

		return scaledBitmap;
	}

	void addBitmapToCache(Bitmap newBitmap, int width) {
		bitmapCache.put(width, newBitmap);
	}

	Bitmap createNewBitmap(Bitmap flakeBitmap, int width, int height) {
		return Bitmap.createScaledBitmap(flakeBitmap, width, height, true);
	}

	Bitmap getCachedBitmap(int width) {
		return bitmapCache.get(width);
	}
	
	SparseArray<Bitmap> getCache() {
		return bitmapCache;
	}
	
	public FlakePropertyGenerator getPropertyGenerator() {
		return propertyGenerator;
	}

	public void setPropertyGenerator(FlakePropertyGenerator propertyGenerator) {
		this.propertyGenerator = propertyGenerator;
	}

}
