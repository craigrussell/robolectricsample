package com.tescobank.mobile.ui.seasonal;

import java.io.File;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BitmapLoaderFileSystem implements BitmapLoader {
	
	@Override
	public Bitmap load(String fileName, File folder) {
		validateRequestedFile(fileName, folder);
		
		File fullImageFile = new File(folder, fileName);
		if(!isReadableFile(fullImageFile)) {
			return null;
		}
		return BitmapFactory.decodeFile(fullImageFile.getAbsolutePath());
	}

	private void validateRequestedFile(String fileName, File folder) {
		if(fileName == null) {
			throw new IllegalArgumentException("filename must be specified");
		}
		if(folder == null) {
			throw new IllegalArgumentException("folder cannot be null");
		}
	}

	private boolean isReadableFile(File fullImageFile) {
		if(!fullImageFile.exists()) {
			return false;
		}
		if(!fullImageFile.canRead()) {
			return false;
		}
		return true;
	}
}
