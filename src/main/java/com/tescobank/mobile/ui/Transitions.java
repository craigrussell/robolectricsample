package com.tescobank.mobile.ui;

import android.app.Activity;

import com.tescobank.mobile.R;

public class Transitions {

	public static void applyDefaultTransition(Activity activity) {
		// Do nothing
	}

	public static void applyLoginTransition(Activity activity) {
		activity.overridePendingTransition(R.anim.anim_enter_right, R.anim.anim_exit_left);
	}

}