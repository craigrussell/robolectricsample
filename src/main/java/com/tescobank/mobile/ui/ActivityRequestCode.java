package com.tescobank.mobile.ui;


/**
 * Supported activity request codes
 */
public final class ActivityRequestCode {
	
	private ActivityRequestCode() {
		super();
	}

	private static int value = 0;
	
	/** Use this request code when you don't need to specify a particular one */
	public static final int REQUEST_DEFAULT = ++value;
    
    // Errors
    public static final int REQUEST_RETRY_ERROR = ++value;
    public static final int REQUEST_FATAL_ERROR = ++value;
	
    // Pay CC
    public static final int REQUEST_3D_SECURE = ++value;
	public static final int REQUEST_CC_PAYMENT = ++value;
	
	// ATM
    public static final int REQUEST_ATM_LIST_ACTIVITY = ++value;
    
    // Savings
    public static final int REQUEST_SAVINGS_FUNDING_SOURCE = ++value;
}
