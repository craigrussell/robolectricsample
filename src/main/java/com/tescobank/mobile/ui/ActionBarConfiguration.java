package com.tescobank.mobile.ui;

import java.io.Serializable;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

import com.actionbarsherlock.app.ActionBar;
import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.seasonal.SeasonalLogoImagePreloader;

public class ActionBarConfiguration implements Serializable {
	
	private static final long serialVersionUID = 2617052963362914689L;

	public enum Style {
		UNAUTHENTICATED,
		AUTHENTICATED
	}
	
	public enum Type {
		HOME,
		HOME_HAMBURGER,
		UP
	}
	
	private final Style style;
	private final Type type;
	
	public ActionBarConfiguration(Style style, Type type) {
		this.style = style;
		this.type = type;
	}

	public static Style getStyle(ActionBar actionBar) {
		if (isUnauthenticatedActionBar(actionBar)) {
			return Style.UNAUTHENTICATED;
		}
		return Style.AUTHENTICATED;
	}
	
	private static boolean isUnauthenticatedActionBar(ActionBar actionBar) {
		return actionBar == null || (actionBar.getCustomView() != null && actionBar.getCustomView().getId() == R.id.actionBarPreLogin);
	}

	public void apply(TescoActivity activity, ActionBar actionBar) {
		if(actionBar == null) {
			return;
		}
		
		switch (style) {
		case UNAUTHENTICATED:
			applyUnauthenticatedConfiguration(activity, actionBar);
			break;
		case AUTHENTICATED:
			applyAuthenticatedConfiguration(activity, actionBar);
			break;
		}
	}
	
	private void applyUnauthenticatedConfiguration(TescoActivity activity, ActionBar actionBar) {
		switch (type) {
		case HOME:
			setToPreLogin(activity, actionBar);			
			break;
		case HOME_HAMBURGER:			
			setToPreLoginHamburger(activity, actionBar);
			break;
		case UP:
			setToPreLoginUp(activity, actionBar);
			break;
		}
	}
	private void applyAuthenticatedConfiguration(TescoActivity activity, ActionBar actionBar) {
		switch (type) {
		case HOME:
			setToStd(activity, actionBar);			
			break;
		case HOME_HAMBURGER:			
			setToStdHamburger(activity, actionBar);
			break;
		case UP:
			setToStdUp(activity, actionBar);
			break;
		}
	}

	public static void setToPreLogin(TescoActivity activity, ActionBar actionBar) {
		actionBar.setIcon(null);
		actionBar.setCustomView(R.layout.actionbar_prelogin);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM);
		setPreLoginLogo(activity, actionBar);
	}

	public static void setToPreLoginUp(TescoActivity activity, ActionBar actionBar) {
		actionBar.setIcon(R.drawable.actionbar_up);
		actionBar.setCustomView(R.layout.actionbar_prelogin);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_CUSTOM);
		setPreLoginLogo(activity, actionBar);
	}

	public static void setToPreLoginHamburger(TescoActivity activity, ActionBar actionBar) {
		actionBar.setIcon(R.drawable.actionbar_hamburger);
		actionBar.setCustomView(R.layout.actionbar_prelogin);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setHomeButtonEnabled(true);
		setPreLoginLogo(activity, actionBar);
	}

	public static void setToStd(TescoActivity activity, ActionBar actionBar) {
		SeasonalLogoImagePreloader logoLoader = getSeasonalImageLoader(activity);
		Bitmap bm = logoLoader.getBitmapForPadded();
		if (bm != null) {
			actionBar.setIcon(new BitmapDrawable(activity.getResources(), bm));
		}
		else {
			actionBar.setIcon(R.drawable.actionbar_padded_logo);
		};
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME);
	}

	public static void setToStdUp(TescoActivity activity, ActionBar actionBar) {
		SeasonalLogoImagePreloader logoLoader = getSeasonalImageLoader(activity);
		Bitmap bm = logoLoader.getBitmapForUp();
		if (bm != null) {
			actionBar.setIcon(new BitmapDrawable(activity.getResources(), bm));
		}
		else {
			actionBar.setIcon(R.drawable.actionbar_up_logo);
		}
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
	}

	public static void setToStdHamburger(TescoActivity activity, ActionBar actionBar) {
		SeasonalLogoImagePreloader logoLoader = getSeasonalImageLoader(activity);
		Bitmap bm = logoLoader.getBitmapForHamburger();
		if (bm != null) {
			actionBar.setIcon(new BitmapDrawable(activity.getResources(), bm));
		}
		else {
			actionBar.setIcon(R.drawable.actionbar_hamburger_logo);
		}
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME);
		actionBar.setHomeButtonEnabled(true);
	}
	
	private static void setPreLoginLogo(TescoActivity activity, ActionBar actionBar) {
		SeasonalLogoImagePreloader logoLoader = getSeasonalImageLoader(activity);
		Bitmap bm = logoLoader.getBitmapForLogo();
		if (bm != null) {
			ImageView imageView = (ImageView)actionBar.getCustomView().findViewById(R.id.actionBarPreLogin);
			imageView.setImageBitmap(bm);
		}
	}

	private static SeasonalLogoImagePreloader getSeasonalImageLoader(TescoActivity activity) {
		SeasonalLogoImagePreloader logoLoader = new SeasonalLogoImagePreloader(activity, null);
		return logoLoader;
	}
}
