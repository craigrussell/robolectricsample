package com.tescobank.mobile.ui;

import com.tescobank.mobile.model.product.ProductDetails;

public class PayNoPayProductDisplay {
	
	private ProductDetails product;
	
	public PayNoPayProductDisplay(ProductDetails product) {
		this.product = product;
	}
	
	/**
	 * @return true if pay no pay details should be displayed for the product
	 */
	public boolean shouldDisplay() {
		if(product.isPersonalCurrentAccount() && hasAmountDue()) {
			return true;
		}
		return false;
	}
	
	private boolean hasAmountDue() {
		if(product.getAmountDue() == null || product.getAmountDue().doubleValue() == 0.0) {
			return false;
		}
		return true;
	}
}
