package com.tescobank.mobile.ui;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

public class CredentialBroadcastAction {

	private static final String OVERLAY_MESSAGE_KEY = "OVERLAY_MESSAGE_KEY";
	public static final String CREDENTIAL_OVERLAY_MESSAGE = "CREDENTIAL_OVERLAY_MESSAGE";
	private String message;
	private TescoActivity activity;

	public CredentialBroadcastAction(String message, TescoActivity activity) {
		this.message = message;
		this.activity = activity;
	}

	public void sendMessage() {
		Intent intent = new Intent(CREDENTIAL_OVERLAY_MESSAGE);
		intent.putExtra(OVERLAY_MESSAGE_KEY, message);
		LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
	}

}
