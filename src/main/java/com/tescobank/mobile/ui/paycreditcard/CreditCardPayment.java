package com.tescobank.mobile.ui.paycreditcard;

import static com.tescobank.mobile.analytics.AnalyticsEventFactory.ProcessCompletedName.PayCreditCard;

import java.util.GregorianCalendar;

import com.tescobank.mobile.api.payment.RetrievePaymentMethodsResponse.PaymentMethod;
import com.tescobank.mobile.model.movemoney.MoneyMovement;
import com.tescobank.mobile.model.product.ProductDetails;

public class CreditCardPayment extends MoneyMovement<PaymentMethod, ProductDetails> {

	private static final long serialVersionUID = -7975822096290903415L;

	public enum CreditCardPaymentType {
		Min, FullBalance, Other
	}

	private CreditCardPaymentType creditCardPaymentType;

	public CreditCardPayment(PaymentMethod source, ProductDetails destination) {
		super(source, destination);
		setDate(new GregorianCalendar().getTime());
	}

	@Override
	public String getProcessCompleteAnalyticsEventName() {
		return PayCreditCard.getName();
	}

	/**
	 * @return the creditCardPaymentType
	 */
	public CreditCardPaymentType getCreditCardPaymentType() {
		return creditCardPaymentType;
	}

	/**
	 * @param creditCardPaymentType
	 *            the creditCardPaymentType to set
	 */
	public void setCreditCardPaymentType(CreditCardPaymentType creditCardPaymentType) {
		this.creditCardPaymentType = creditCardPaymentType;
	}
}
