package com.tescobank.mobile.ui.paycreditcard.model;

import java.util.Date;
import java.util.List;

public interface AddDebitCardModelValidator {

	public boolean validatePanLength(String pan);
	public boolean validatePanCharacters(String pan);
	public boolean validateExpiryDate(Date expiryDate, Date comparisonDate);
	public boolean validateNicknameLength(String nickname);
	public boolean validateNicknameCharacters(String nickname);
	public boolean validateNicknameUnique(String nickname, List<String> existingNicknames);
	public boolean validatePostcodeLength(String postcode);
	public boolean validatePostcodeCharacters(String postcode);
	public boolean validateAddressLength(String addressLine);
	public boolean validateAddressCharacters(String addressLine);
	
	public boolean hasErrors();
	public void clearErrors();		
}
