package com.tescobank.mobile.ui.paycreditcard;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.payment.RetrievePaymentMethodsResponse.PaymentMethod;
import com.tescobank.mobile.ui.TescoAutoResizeTextView;
import com.tescobank.mobile.ui.TescoFragment;

public class PaymentMethodsGalleryAdapter extends GalleryAdapter {

	private boolean managePaymentsEnabled;
	private boolean hasValidPaymentMethod;

	public PaymentMethodsGalleryAdapter(boolean managePaymentsEnabled, FragmentManager manager) {
		super(manager);
		this.managePaymentsEnabled = managePaymentsEnabled;
	}

	public void updatePaymentMethods(List<PaymentMethod> paymentMethods) {
		hasValidPaymentMethod = containsValidCard(paymentMethods);
		super.updateTiles(createGalleryTiles(managePaymentsEnabled, hasValidPaymentMethod, paymentMethods));
	}

	private boolean containsValidCard(List<PaymentMethod> paymentMethods) {
		for (PaymentMethod paymentMethod : paymentMethods) {
			if (!paymentMethod.hasExpired()) {
				return true;
			}
		}
		return false;
	}

	private static List<GalleryTile> createGalleryTiles(boolean managePaymentsEnabled, boolean hasValidPaymentMethod, List<PaymentMethod> paymentMethods) {
		List<GalleryTile> tiles = new ArrayList<GalleryTile>();
		if (managePaymentsEnabled) {
			tiles.add(new AddCardTile());
		}
		if (!managePaymentsEnabled && !hasValidPaymentMethod) {
			tiles.add(new AddCardOnlineTile());
		}
		for (PaymentMethod paymentMethod : paymentMethods) {
			tiles.add(new PaymentMethodTile(paymentMethod, managePaymentsEnabled));
		}
		return tiles;
	}

	public int getDefaultPosition() {
		if (managePaymentsEnabled && hasValidPaymentMethod) {
			return 1;
		}
		return 0;
	}

	public boolean hasValidPaymentMethod() {
		return hasValidPaymentMethod;
	}

	public boolean isPaymentTile(int position) {
		return getTile(position) instanceof PaymentMethodTile;
	}

	public static class PaymentMethodTile implements GalleryTile {

		private PaymentMethod data;
		private boolean canDelete;

		public PaymentMethodTile(PaymentMethod data, boolean canDelete) {
			this.data = data;
			this.canDelete = canDelete;
		}

		@Override
		public PaymentMethod getData() {
			return data;
		}

		@Override
		public Fragment getFragment() {
			return PaymentMethodFragment.newInstance(data, canDelete);
		}
	}

	public static class AddCardTile implements GalleryTile {

		@Override
		public Object getData() {
			return null;
		}

		@Override
		public Fragment getFragment() {
			return new AddCardFragment();
		}
	}

	public static class AddCardOnlineTile implements GalleryTile {

		@Override
		public Object getData() {
			return null;
		}

		@Override
		public Fragment getFragment() {
			return new AddCardOnlineFragment();
		}
	}

	public static class PaymentMethodFragment extends TescoFragment {

		private static final String CAN_DELETE_KEY = "CAN_DELETE_KEY";
		private boolean canDelete;
		private PaymentMethod paymentMethod;

		public static PaymentMethodFragment newInstance(PaymentMethod data, boolean canDelete) {
			PaymentMethodFragment fragment = new PaymentMethodFragment();
		    Bundle bundle = new Bundle();
			bundle.putSerializable(PaymentMethod.class.getName(), data);
			bundle.putBoolean(CAN_DELETE_KEY, canDelete);
			fragment.setArguments(bundle);
			return fragment;
		}
		
		@Override
		public void onCreate(Bundle savedState) {
			super.onCreate(savedState);
			restoreFromBundle(getArguments());
		}

		private void restoreFromBundle(Bundle bundle) {
			if (bundle == null) {
				return;
			}
			paymentMethod = PaymentMethod.class.cast(bundle.get(PaymentMethod.class.getName()));
			canDelete = bundle.getBoolean(CAN_DELETE_KEY);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			return inflater.inflate(R.layout.include_pay_credit_card_method, null, false);
		}
		
		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			super.onViewCreated(view, savedInstanceState);
			PaymentMethodViewUpdater updater = new PaymentMethodViewUpdater(getActivity(), view);
			updater.update(paymentMethod);
			configureNicknameTextSize(view);
			configureDeleteButton(view);
		}

		private void configureNicknameTextSize(View view) {
			float minSize = getActivity().getResources().getDimension(R.dimen.pay_card_select_payment_card_title_min_size);
			TescoAutoResizeTextView nickName = TescoAutoResizeTextView.class.cast(view.findViewById(R.id.payment_method_nickname));
			nickName.setMinTextSize(minSize);
			nickName.setAddEllipsis(false);
		}

		private void configureDeleteButton(View container) {
			View view = container.findViewById(R.id.payment_method_delete_image);
			if (!canDelete) {
				view.setVisibility(View.INVISIBLE);
			}
		}
	}

	public static class AddCardFragment extends TescoFragment {
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			return inflater.inflate(R.layout.include_pay_credit_card_method_add, null, false);
		}
	}

	public static class AddCardOnlineFragment extends TescoFragment {
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			return inflater.inflate(R.layout.include_pay_credit_card_method_addonline, null, false);
		}
	}
}
