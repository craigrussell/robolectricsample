package com.tescobank.mobile.ui.paycreditcard.model;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class DebitCardValidator implements AddDebitCardModelValidator {

	private static final String ADDRESS_VALID_CHARSET = "[^0-9 a-zA-Z'\\-,\\.]+";
	private static final String NICKNAME_VALID_CHARSET = "[^0-9 a-zA-Z'\\-,\\.]+";
	private static final String POSTCODE_VALID_CHARSET = "[^0-9 a-zA-Z'\\ ]+";

	private static final String NUMERIC_ONLY_REGEX = "[0-9]+";

	private static final int MAX_NICKNAME_LENGTH = 25;
	private static final int PAN_LENGTH = 16;
	private static final int MAX_ADDRESS_LENGTH = 36;
	private static final int MIN_POSTCODE_LENGTH = 5;
	private static final int MAX_POSTCODE_LENGTH = 9;

	private boolean hasErrors;

	@Override
	public boolean validatePanLength(String pan) {
		if (pan == null || panIncorrectLength(pan)) {
			hasErrors = true;
			return false;
		}
		return true;
	}
	
	private boolean panIncorrectLength(String pan) {
		return pan.length() != PAN_LENGTH;
	}

	@Override
	public boolean validatePanCharacters(String pan) {
		if (pan == null || !containsOnlyNumbers(pan)) {
			hasErrors = true;
			return false;
		}
		return true;
	}
	
	private boolean containsOnlyNumbers(String pan) {
		return pan.matches(NUMERIC_ONLY_REGEX);
	}

	@Override
	public boolean validateExpiryDate(Date expiryDate, Date comparisonDate) {
		if (expiryDate == null || comparisonDate == null || expiryDate.before(comparisonDate)) {
			hasErrors = true;
			return false;
		}
		return true;
	}

	@Override
	public boolean validateNicknameLength(String nickname) {
		if (nickname == null || invalidNicknameLength(nickname)) {
			hasErrors = true;
			return false;
		}
		return true;
	}

	@Override
	public boolean validateNicknameCharacters(String nickname) {
		if (nickname == null || containsInvalidChars(nickname, NICKNAME_VALID_CHARSET)) {
			hasErrors = true;
			return false;
		}
		return true;
	}

	@Override
	public boolean validateNicknameUnique(String nickname, List<String> existingNicknames) {
		if (nickname == null || duplicateNickname(nickname, existingNicknames)) {
			hasErrors = true;
			return false;
		}
		return true;
	}
	
	private boolean duplicateNickname(String nickname, List<String> existingNicknames) {
		if(existingNicknames == null || existingNicknames.isEmpty()) {
			return false;
		}
		
		for(String existingNickname : existingNicknames) {
			if(existingNickname.equalsIgnoreCase(nickname)) {
				return true;
			}
		}
		return false;
	}

	private boolean containsInvalidChars(String stringToValidate, String acceptableCharacterPattern) {
		Scanner scanner = new Scanner(stringToValidate);
		String validationResult = scanner.findInLine(acceptableCharacterPattern);
		if (validationResult != null) {
			return true;
		}
		return false;
	}

	private boolean invalidNicknameLength(String nickname) {
		return nickname.length() == 0 || (nickname.length() > MAX_NICKNAME_LENGTH);
	}

	@Override
	public boolean validatePostcodeLength(String postcode) {
		if (postcode == null || postcodeTooShort(postcode) || postcodeTooLong(postcode)) {
			hasErrors = true;
			return false;
		}
		return true;
	}

	private boolean postcodeTooShort(String postcode) {
		return postcode.length() < MIN_POSTCODE_LENGTH;
	}

	private boolean postcodeTooLong(String postcode) {
		return postcode.length() > MAX_POSTCODE_LENGTH;
	}
	
	@Override
	public boolean validatePostcodeCharacters(String postcode) {
		if (postcode == null || containsInvalidChars(postcode, POSTCODE_VALID_CHARSET)) {
			hasErrors = true;
			return false;
		}
		return true;
	}

	@Override
	public boolean validateAddressLength(String addressLine) {
		if (addressLine == null || invalidAddressLength(addressLine)) {
			hasErrors = true;
			return false;
		}
		return true;
	}
	
	@Override
	public boolean validateAddressCharacters(String addressLine) {
		if (addressLine == null || containsInvalidChars(addressLine, ADDRESS_VALID_CHARSET)) {
			hasErrors = true;
			return false;
		}
		return true;
	}

	private boolean invalidAddressLength(String addressLine) {
		return addressLine.length() == 0 || addressLine.length() > MAX_ADDRESS_LENGTH;
	}

	@Override
	public boolean hasErrors() {
		return hasErrors;
	}

	@Override
	public void clearErrors() {
		hasErrors = false;
	}
}
