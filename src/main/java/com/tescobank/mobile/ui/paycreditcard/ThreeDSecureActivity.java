package com.tescobank.mobile.ui.paycreditcard;

import static android.os.Build.VERSION_CODES.JELLY_BEAN_MR2;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.payment.ThreeDSecureEnrollmentStatusResponse;
import com.tescobank.mobile.properties.TescoApplicationPropertiesReader.ApplicationPropertyKeys;
import com.tescobank.mobile.ui.ActivityResultCode;
import com.tescobank.mobile.ui.HideAndShowProgressIndicator;
import com.tescobank.mobile.ui.TescoActivity;

@SuppressLint("SetJavaScriptEnabled")
public class ThreeDSecureActivity extends TescoActivity {

	private static final String TAG = ThreeDSecureActivity.class.getSimpleName();

	private static final String TB_JAVASCRIPT_BRIDGE = "TBJavascriptBridge";
	private static final int COMPLETE = 100;
	private static final String MD = "93333311";
	private static final String PA_RES_KEY = "PaRes";
	public static final String THREEDS_PARES_EXTRA = "3DS_PARES_EXTRA";

	private WebView threeDSecureView;
	private HideAndShowProgressIndicator inProgressIndicator;
	private ThreeDSecureEnrollmentStatusResponse enrollmentResponse;
	private String termUrl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onCreate(" + savedInstanceState + ")");
		}
		setContentView(R.layout.activity_3d_secure);
		threeDSecureView = (WebView) findViewById(R.id.threeDSecureView);
		threeDSecureView.getSettings().setAllowFileAccess(false);
		ImageView inProgressView = (ImageView) findViewById(R.id.inProgressView);
		inProgressIndicator = new HideAndShowProgressIndicator(inProgressView);
		enrollmentResponse = (ThreeDSecureEnrollmentStatusResponse) getIntent().getSerializableExtra(ThreeDSecureEnrollmentStatusResponse.class.getName());
		termUrl = getApplicationState().getApplicationPropertyValueForKey(ApplicationPropertyKeys.THREE_D_SECURE_URL);
		configureWebView();
	}

	@Override
	protected void onResume() {
		super.onResume();
		track3DSecureAnalytic();
		loadAuthorisationPage();
	}

	@Override
	protected void onPause() {
		super.onPause();
		hideKeyboard();
		onCancelled3DSecure();
	}

	private void configureWebView() {

		threeDSecureView.getSettings().setJavaScriptEnabled(true);
		threeDSecureView.getSettings().setBuiltInZoomControls(true);
		threeDSecureView.getSettings().setAppCacheEnabled(false);
		threeDSecureView.addJavascriptInterface(new ThreeDSecureJavscriptInterface(), TB_JAVASCRIPT_BRIDGE);
		threeDSecureView.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				if (newProgress == COMPLETE) {
					inProgressIndicator.hideInProgressIndicator();
				}
			}
		});

		if (Build.VERSION.SDK_INT == JELLY_BEAN_MR2) {
			threeDSecureView.setWebViewClient(new ThreeDSecureBouncyCastleAvoidingClient());
		} else {
			threeDSecureView.setWebViewClient(new ThreeDSecureClient());
		}
	}

	private void loadAuthorisationPage() {
		inProgressIndicator.showInProgressIndicator();
		beginAuthorisation(enrollmentResponse.getUrl(), enrollmentResponse.getPaymentAuthenticationRequest());
	}

	private void beginAuthorisation(String acsUrl, String paReq) {

		List<NameValuePair> params = new LinkedList<NameValuePair>();
		params.add(new BasicNameValuePair("MD", MD));
		params.add(new BasicNameValuePair("TermUrl", termUrl));
		params.add(new BasicNameValuePair("PaReq", paReq));

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			new UrlEncodedFormEntity(params, HTTP.UTF_8).writeTo(bos);
		} catch (IOException e) {
			if (BuildConfig.DEBUG) {
				Log.e(TAG, "Parsing initial post params failed", e);
			}
		}

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Term Url " + termUrl);
		}
		threeDSecureView.postUrl(acsUrl, bos.toByteArray());
	}

	private void on3DSecureCompleted(String paRes) {
		if (paRes != null) {
			on3DSecureCompletedSuccessfully(paRes);
		} else {
			onErrorLoading3DSecurePage();
		}
	}

	private void on3DSecureCompletedSuccessfully(String paRes) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Authorisation complete");
			Log.d(TAG, "paRes" + paRes);
		}

		Intent resultIntent = new Intent();
		resultIntent.putExtra(THREEDS_PARES_EXTRA, paRes);
		finishWithResult(ActivityResultCode.RESULT_THREE_D_SECURE_COMPLETED, resultIntent);
	}

	private void onErrorLoading3DSecurePage() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "3D Secure failed to load");
		}
		finishWithResult(ActivityResultCode.RESULT_THREE_D_SECURE_ERROR);
	}

	private void onCancelled3DSecure() {
		finishWithResult(ActivityResultCode.RESULT_THREE_D_SECURE_CANCELLED);
	}

	public void onCancelPressed(View view) {
		onCancelled3DSecure();
	}

	@Override
	public void onBackPressed() {
		onCancelled3DSecure();
	}

	private void track3DSecureAnalytic() {
		getApplicationState().getAnalyticsTracker().trackThreeDSecure();
	}

	private String tryParsingPaResFromUrl(String url) {
		String paRes = null;
		try {
			paRes = parsePaResFromUrl(url);
		} catch (Exception e) {
			if (BuildConfig.DEBUG) {
				Log.e(TAG, "Could not extract paRes", e);
			}
		}
		return paRes;
	}

	private String parsePaResFromUrl(String url) throws UnsupportedEncodingException {
		Uri uri = Uri.parse(url);
		return uri.getQueryParameter(PA_RES_KEY);
	}

	private String parsePaResFromHtml(String html) {

		Pattern fieldPattern = Pattern.compile(".*?(<input[^<>]* name=\\\"PaRes\\\"[^<>]*>).*?", 32);
		Pattern valuePattern = Pattern.compile(".*? value=\\\"(.*?)\\\"", 32);

		Matcher matcher = fieldPattern.matcher(html);
		if (matcher.find()) {
			String paresGroup = matcher.group(1);
			if (paresGroup != null) {
				Matcher valueMatcher = valuePattern.matcher(paresGroup);
				if (valueMatcher.find()) {
					return valueMatcher.group(1);
				}
			}
		}
		return null;
	}

	protected class ThreeDSecureClient extends WebViewClient {

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			if (url.contains(termUrl) && url.contains(PA_RES_KEY)) {
				on3DSecureCompleted(tryParsingPaResFromUrl(url));
				return;
			}
			super.onPageStarted(view, url, favicon);
		}

		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			onErrorLoading3DSecurePage();
		};
	}

	protected class ThreeDSecureBouncyCastleAvoidingClient extends WebViewClient {

		@Override
		@SuppressLint("NewApi")
		public WebResourceResponse shouldInterceptRequest(final WebView view, String url) {

			if (url.equals(termUrl)) {
				injectJavaScriptForParsingPaRes(view);
				return new WebResourceResponse("", "", new ByteArrayInputStream("".getBytes()));
			}
			return super.shouldInterceptRequest(view, url);
		}

		private void injectJavaScriptForParsingPaRes(final WebView view) {
			final Semaphore mutex = new Semaphore(0);
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					view.loadUrl(String.format("javascript:window.%s.processHTML(document.getElementsByTagName('html')[0].innerHTML);", TB_JAVASCRIPT_BRIDGE));
					mutex.release();
				}
			});
			try {
				mutex.acquire();
			} catch (InterruptedException e) {
				if (BuildConfig.DEBUG) {
					Log.e(TAG, "Could not extract paRes", e);
				}
			}
		}

		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			onErrorLoading3DSecurePage();
		};
	}

	private class ThreeDSecureJavscriptInterface {
		@android.webkit.JavascriptInterface
		public void processHTML(final String paramString) {
			final String paRes = parsePaResFromHtml(paramString);
			if (paRes != null) {
				on3DSecureCompleted(paRes);
			}
		}
	}
}
