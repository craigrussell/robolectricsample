package com.tescobank.mobile.ui.paycreditcard;

import java.util.ArrayList;
import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


public class GalleryAdapter extends FragmentStatePagerAdapter {

	private List<GalleryTile> tiles;
	
	public GalleryAdapter(FragmentManager manager) {
		super(manager);
		this.tiles = new ArrayList<GalleryAdapter.GalleryTile>();
	}
	
	public void updateTiles(List<GalleryTile> tiles) {
		this.tiles = tiles;
		notifyDataSetChanged();
	}

	@Override
	public Fragment getItem(int position) {
		return getTile(position).getFragment();
	}
	
	@Override
	public int getItemPosition(Object object) {
		return GalleryAdapter.POSITION_NONE;
	}	
	
	public GalleryTile getTile(int position) {
		return tiles.get(position);
	}

	@Override
	public int getCount() {
		return tiles.size();
	}
		
	public interface GalleryTile {
		Object getData();
		Fragment getFragment();
	}
}
