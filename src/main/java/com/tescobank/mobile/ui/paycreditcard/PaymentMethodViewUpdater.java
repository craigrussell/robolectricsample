package com.tescobank.mobile.ui.paycreditcard;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.payment.RetrievePaymentMethodsResponse.PaymentMethod;
import com.tescobank.mobile.format.DataFormatter;

public class PaymentMethodViewUpdater {
	
	private PaymentMethod paymentMethod;

	private Context context;
	private ImageView imageView;
	private TextView nickName;
	private TextView number;
	private TextView expiry;
	
	public PaymentMethodViewUpdater(Context context, View container) {
		this.context = context;
		imageView = ImageView.class.cast(container.findViewById(R.id.payment_method_image));
		nickName = TextView.class.cast(container.findViewById(R.id.payment_method_nickname));
		number = TextView.class.cast(container.findViewById(R.id.payment_method_number));
		expiry = TextView.class.cast(container.findViewById(R.id.payment_method_expiry));	
	}

	public void update(PaymentMethod newPaymentMethod) {
		this.paymentMethod = newPaymentMethod;
		update();
	}
	
	private void update() {
		int drawableId = PaymentType.getDrawableForPaymentType(paymentMethod.getCardType());
		imageView.setImageDrawable(context.getResources().getDrawable(drawableId));
		nickName.setText(paymentMethod.getCardName());
		number.setText(DataFormatter.formatCardNumber(paymentMethod.getCardNumber()));
		updateExpiry();
	}

	private void updateExpiry() {
		if(expiry == null) {
			return;
		}
		if(paymentMethod.hasExpired()) {
			updateInvalidExpiry();
		} else {
			updateValidExpiry();
		}
	}

	private void updateInvalidExpiry() {
		int color = context.getResources().getColor(R.color.red_bright);
		expiry.setText(context.getString(R.string.pay_creditcard_payment_method_expired));
		expiry.setTextColor(color);		
	}
	
	private void updateValidExpiry() {
		int color = context.getResources().getColor(R.color.blue_tesco);
		String text = context.getString(R.string.pay_creditcard_payment_method_expiry, paymentMethod.getCardExpiryMonth(), paymentMethod.getCardExpiryYear());
		expiry.setText(text);		
		expiry.setTextColor(color);
	}

	private enum PaymentType {
		VDC(R.drawable.cardtype_visadebit),
		Delta(R.drawable.cardtype_visadebit),
		MDC(R.drawable.cardtype_mastercarddebit),
		ELECTR(R.drawable.cardtype_electron),
		MSTROD(R.drawable.cardtype_maestro),
		MSTROI(R.drawable.cardtype_maestro_int);
	
		private int drawableId;
		
		PaymentType(int drawableId) {
			this.drawableId = drawableId;
		}
		
		public static int getDrawableForPaymentType(String type) {
			try {
				return PaymentType.valueOf(type).drawableId;
			} catch (IllegalArgumentException e) {
				return R.drawable.cardtype_debitcard;
			} catch (NullPointerException e) {
				return R.drawable.cardtype_debitcard;
			}
		}
	}
}
