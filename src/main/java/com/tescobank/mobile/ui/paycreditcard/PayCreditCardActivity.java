package com.tescobank.mobile.ui.paycreditcard;

import static com.tescobank.mobile.ui.ActivityRequestCode.REQUEST_CC_PAYMENT;
import static com.tescobank.mobile.ui.ActivityRequestCode.REQUEST_DEFAULT;
import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_CC_CARD_ADDED;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.Display;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.api.payment.RetrievePaymentMethodsRequest;
import com.tescobank.mobile.api.payment.RetrievePaymentMethodsResponse;
import com.tescobank.mobile.api.payment.RetrievePaymentMethodsResponse.PaymentMethod;
import com.tescobank.mobile.api.portfolio.DeleteCardPaymentMethodRequest;
import com.tescobank.mobile.api.success.SuccessResponseWrapper;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.ActivityRequestCode;
import com.tescobank.mobile.ui.ActivityResultCode;
import com.tescobank.mobile.ui.AmountFilter;
import com.tescobank.mobile.ui.HideAndShowProgressIndicator;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.InProgressIndicator.ContinueButtonConfiguration;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoAlertDialog;
import com.tescobank.mobile.ui.TescoEditText;
import com.tescobank.mobile.ui.overlay.SuccessResultOverlayRenderer;
import com.tescobank.mobile.ui.paycreditcard.CreditCardPayment.CreditCardPaymentType;
import com.tescobank.mobile.ui.paycreditcard.PaymentMethodsGalleryAdapter.PaymentMethodTile;
import com.tescobank.mobile.ui.prodheader.ProductHeaderViewFactory;
import com.tescobank.mobile.ui.tutorial.TooltipId;
import com.tescobank.mobile.ui.tutorial.groups.PayCreditCardTooltipGroup;

public class PayCreditCardActivity extends TescoActivity implements ContinueButtonConfiguration {


	private static final String TAG = PayCreditCardActivity.class.getSimpleName();

	public static final String PAY_CREDITCARD_PRODUCT_EXTRA = "PAY_CREDITCARD_PRODUCT_EXTRA";
	public static final String PAY_CREDITCARD_NICKNAMES_EXTRA = "PAY_CREDITCARD_NICKNAMES_EXTRA";
	public static final String PAY_CREDITCARD_DETAILS_EXTRA = "PAY_CREDITCARD_DETAILS_EXTRA";
	public static final String PAY_CREDITCARD_SELECTED_PAYMENT_METHOD_ID_EXTRA = "selectedPaymentMethodId";
	private static final String BUNDLE_KEY_SELECTED_PAYMENT_METHOD = "BUNDLE_KEY_SELECTED_PAYMENT_METHOD";
	private static final String BUNDLE_KEY_RETRIEVED_PAYMENT_METHODS = "BUNDLE_KEY_RETRIEVED_PAYMENT_METHODS";
	private static final String BUNDLE_KEY_PAYMENT_GALLERY_POSITION = "BUNDLE_KEY_PAYMENT_GALLERY_POSITION";
	private static final String BUNDLE_KEY_CONFIRMATION_DIALOG_SHOWING = "BUNDLE_KEY_CONFIRMATION_DIALOG";
	private static final int NO_SAVED_PAYMENT_GALLERY_POSITION = -1;
	
	private TextView statementDate;
	private TextView dueByDate;
	private boolean managePaymentsEnabled;
	private boolean paymentRetrievalInProgress;
	private ViewPager paymentMethodsGallery;
	private int lastPaymentGalleryPosition = NO_SAVED_PAYMENT_GALLERY_POSITION;
	private PaymentMethodsGalleryAdapter paymentMethodsAdapter;

	private HideAndShowProgressIndicator hideAndShowProgressIndicator;
	private InProgressIndicator inProgressIndicator;
	private SuccessResultOverlayRenderer successOverlay;
	private AlertDialog confirmationDialog;
	
	private DataFormatter dataFormatter;
	private RetrievePaymentMethodsResponse retrievePaymentMethodsResponse;

	private ProductDetails creditCardProductDetails;
	private PaymentMethod paymentMethod;

	private TextView statementBalanceAmount;
	private CheckBox statementBalanceCheckbox;
	private RelativeLayout statementBalanceContainer;

	private TextView minimumPaymentAmount;
	private CheckBox minimumAmountCheckbox;

	private TescoEditText otherAmount;
	private CheckBox otherAmountCheckbox;
	private RelativeLayout minimumBalanceContainer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting PayCreditCardActivity");
		}
		super.onCreate(savedInstanceState);

		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		setContentView(R.layout.activity_pay_creditcard);
		restoreSavedState(savedInstanceState);

		ImageView imageView = (ImageView) findViewById(R.id.progress_indicator_image);
		hideAndShowProgressIndicator = new HideAndShowProgressIndicator(imageView);
		dataFormatter = new DataFormatter();
		creditCardProductDetails = (ProductDetails) getIntent().getExtras().getSerializable(PAY_CREDITCARD_PRODUCT_EXTRA);

		configureHeader();
		configureStatementBalance();
		configureMinimumAmount();
		configureOtherAmount();
		configureInProgressIndicator();
		configurePaymentMethodGallery();
		populateCardPaymentDetails();
		trackPayCreditCardAnalytic();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus) {
			retrievePaymentMethodsIfMissing();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (retrievePaymentMethodsResponse != null) {
			outState.putSerializable(BUNDLE_KEY_RETRIEVED_PAYMENT_METHODS, retrievePaymentMethodsResponse);
		}
		if (paymentMethod != null) {
			outState.putSerializable(BUNDLE_KEY_SELECTED_PAYMENT_METHOD, paymentMethod);
		}
		if(paymentMethodsGallery != null) {
			outState.putInt(BUNDLE_KEY_PAYMENT_GALLERY_POSITION, paymentMethodsGallery.getCurrentItem());
		}
		outState.putBoolean(BUNDLE_KEY_CONFIRMATION_DIALOG_SHOWING, isConfirmationDialogShowing());
	}

	private void restoreSavedState(Bundle inState) {
		if (inState == null) {
			return;
		}

		if (inState.containsKey(BUNDLE_KEY_RETRIEVED_PAYMENT_METHODS)) {
			retrievePaymentMethodsResponse = RetrievePaymentMethodsResponse.class.cast(inState.getSerializable(BUNDLE_KEY_RETRIEVED_PAYMENT_METHODS));
		}

		if (inState.containsKey(BUNDLE_KEY_SELECTED_PAYMENT_METHOD)) {
			paymentMethod = PaymentMethod.class.cast(inState.getSerializable(BUNDLE_KEY_SELECTED_PAYMENT_METHOD));
		}
		
		if(inState.getBoolean(BUNDLE_KEY_CONFIRMATION_DIALOG_SHOWING, false)) {
			confirmDelete();
		}
		
		lastPaymentGalleryPosition = inState.getInt(BUNDLE_KEY_PAYMENT_GALLERY_POSITION, NO_SAVED_PAYMENT_GALLERY_POSITION);
	}

	private void trackPayCreditCardAnalytic() {
		getApplicationState().getAnalyticsTracker().trackPayCreditCard(creditCardProductDetails);
	}

	private void configureStatementBalance() {
		statementBalanceAmount = (TextView) findViewById(R.id.pay_creditcard_payoff_card_statementbalance_value);
		statementDate = (TextView) findViewById(R.id.pay_creditcard_payoff_card_statementdate_value);
		statementBalanceCheckbox = (CheckBox) findViewById(R.id.pay_creditcard_payoff_card_statementbalance_checkbox);
		statementBalanceContainer = (RelativeLayout) findViewById(R.id.pay_creditcard_payoff_card_container);
	}

	private void configureMinimumAmount() {
		dueByDate = (TextView) findViewById(R.id.pay_creditcard_payoff_card_duebydate_value);
		minimumPaymentAmount = (TextView) findViewById(R.id.pay_creditcard_payoff_card_minbalance_value);
		minimumAmountCheckbox = (CheckBox) findViewById(R.id.pay_creditcard_payoff_card_minbalance_checkbox);
		minimumBalanceContainer = (RelativeLayout) findViewById(R.id.pay_creditcard_payoff_card_minbalance_container);
	}

	private void configurePaymentMethodGallery() {
		int fadingEdgeLengthPx = getResources().getDimensionPixelOffset(R.dimen.pay_card_select_payment_container_fading_edge);
		managePaymentsEnabled = getApplicationState().getFeatures().isManageCreditCardPaymensEnabled();
		paymentMethodsAdapter = new PaymentMethodsGalleryAdapter(managePaymentsEnabled, getSupportFragmentManager());
		paymentMethodsGallery = ViewPager.class.cast(findViewById(R.id.pay_creditcard_select_payment_pager));
		paymentMethodsGallery.setOffscreenPageLimit(8);
		paymentMethodsGallery.setClipChildren(false);
		paymentMethodsGallery.setHorizontalFadingEdgeEnabled(true);
		paymentMethodsGallery.setFadingEdgeLength(fadingEdgeLengthPx);
		paymentMethodsGallery.setAdapter(paymentMethodsAdapter);
	}

	private void configureOtherAmount() {

		otherAmount = (TescoEditText) findViewById(R.id.pay_creditcard_payoff_card_otheramount_edittext);
		otherAmountCheckbox = (CheckBox) findViewById(R.id.pay_creditcard_payoff_card_otheramount_checkbox);
		otherAmount.setFilters(new InputFilter[] { new AmountFilter() });
		enableOtherAmountEditText(false);

		otherAmount.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				handleTextChange(otherAmount);
				refreshInProgressIndicator();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
		});
	}

	private void configureHeader() {
		ViewGroup headerContainer = (ViewGroup) findViewById(R.id.transfer_header);
		ProductHeaderViewFactory productHeaderViewFactory = new ProductHeaderViewFactory(creditCardProductDetails, this, headerContainer);
		productHeaderViewFactory.createHeaderView();
	}

	private void populateCardPaymentDetails() {
		if (cycleDatesAvailable()) {
			statementDate.setText(dataFormatter.formatDate(creditCardProductDetails.getCycleDates().get(0)));
			dueByDate.setText(dataFormatter.formatDate(creditCardProductDetails.getPaymentDue()));
			populateStatementBalancePaymentField();
			populateMinimumPaymentField();
		} else {
			initialisePaymentValuesToZero();
		}
	}

	private void populateStatementBalancePaymentField() {
		BigDecimal amount = creditCardProductDetails.getStatementBalance();
		if (!amountGreaterThanZero(amount)) {
			disableStatementBalancePaymentOption();
		}
		statementBalanceAmount.setText(dataFormatter.formatCurrency(configureNegativeStatementBalance(amount)));
	}

	private void populateMinimumPaymentField() {
		BigDecimal minPaymentAmount = creditCardProductDetails.getMinimumPayment();
		if (null != minPaymentAmount && minPaymentAmount.compareTo(BigDecimal.ZERO) == -1) {
			minPaymentAmount = BigDecimal.ZERO;
		}

		if (!amountGreaterThanZero(minPaymentAmount)) {
			disableMinimumPaymentOption();
		}
		
		minimumPaymentAmount.setText(dataFormatter.formatCurrency(minPaymentAmount));
	}

	private boolean amountGreaterThanZero(BigDecimal amount) {
		if (amount == null) {
			return false;
		}
		if (amount.compareTo(BigDecimal.ZERO) <= 0) {
			return false;
		}
		return true;
	}

	private void initialisePaymentValuesToZero() {
		statementBalanceAmount.setText(dataFormatter.formatCurrency(BigDecimal.ZERO));
		minimumPaymentAmount.setText(dataFormatter.formatCurrency(BigDecimal.ZERO));
		TextView statementNotReadyText = (TextView) findViewById(R.id.pay_creditcard_payoff_card_statement_date_not_cycled);
		TextView statementDateTitleText = (TextView) findViewById(R.id.pay_creditcard_payoff_card_statementdate_text);
		statementDateTitleText.setVisibility(View.GONE);
		TextView minimumPaymentDueByTitleDate = (TextView) findViewById(R.id.pay_creditcard_payoff_card_duebydate_text);
		minimumPaymentDueByTitleDate.setVisibility(View.GONE);
		statementNotReadyText.setVisibility(View.VISIBLE);
		statementDate.setVisibility(View.GONE);
		dueByDate.setVisibility(View.GONE);

		disableMinimumPaymentOption();
		disableStatementBalancePaymentOption();
	}

	private void disableStatementBalancePaymentOption() {
		statementBalanceCheckbox.setEnabled(false);
		statementBalanceContainer.setEnabled(false);
	}

	private void disableMinimumPaymentOption() {
		minimumBalanceContainer.setEnabled(false);
		minimumAmountCheckbox.setEnabled(false);
	}

	private boolean cycleDatesAvailable() {
		return creditCardProductDetails.getCycleDates() != null && !creditCardProductDetails.getCycleDates().isEmpty();
	}

	private BigDecimal configureNegativeStatementBalance(BigDecimal statementBalance) {
		if (statementBalance == null) {
			return BigDecimal.ZERO;
		}
		return BigDecimal.ZERO.max(statementBalance);
	}

	private void configureInProgressIndicator() {
		inProgressIndicator = new InProgressIndicator(this, true);
		inProgressIndicator.updateContinueButtonConfiguration(this);
		refreshInProgressIndicator();
	}

	@Override
	public int getMessageId() {
		return R.string.pay_creditcard_next_button;
	}

	@Override
	public int getBackgroundDrawable() {
		return R.drawable.button_default_background_selector;
	}

	@Override
	public int getTextColors() {
		return R.color.button_default_text_selector;
	}

	private void refreshInProgressIndicator() {
		boolean show = hasSelectedPaymentMethod() && selectedPaymentType();
		inProgressIndicator.setContinueEnabled(show);
	}

	private boolean selectedPaymentType() {
		return statementBalanceCheckbox.isChecked() || minimumAmountCheckbox.isChecked() || hasOtherAmount();
	}

	private boolean hasOtherAmount() {
		return otherAmountCheckbox.isChecked() && otherAmount.getText().length() > 0;
	}

	private boolean hasSelectedPaymentMethod() {
		return paymentMethod != null && !paymentMethod.hasExpired();
	}

	private void retrievePaymentMethodsIfMissing() {
		if(paymentRetrievalInProgress) {
			return;
		}
		if (retrievePaymentMethodsResponse == null) {
			retrievePaymentMethods();
		} else {
			refreshPaymentMethodGallery();
		}
	}
		
	private void retrievePaymentMethods() {
		paymentRetrievalInProgress = true;
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting getPaymentMethods");
		}
		RetrievePaymentMethodsRequest request = new PayyCCRetrievePaymentMethodsRequest();
		hidePaymentMethodGallery();
		request.setProductId(creditCardProductDetails.getProductId());
		getApplicationState().getRestRequestProcessor().processRequest(request);
	}

	private void showPaymentMethodGallery() {
		showTooltip();
		hideAndShowProgressIndicator.hideInProgressIndicator();
		paymentMethodsGallery.setPageMargin(getPaymentCardMargin());
		paymentMethodsGallery.setVisibility(View.VISIBLE);
		paymentMethodsGallery.setCurrentItem(getPaymentGalleryPosition());		
		onPaymentMethodGalleryTileChanged(getPaymentGalleryPosition());
		paymentMethodsGallery.setOnPageChangeListener(new GalleryPageSelectionListener());
	}

	private int getPaymentGalleryPosition() {
		int position = hasSavedGalleryPosition() ? lastPaymentGalleryPosition : paymentMethodsAdapter.getDefaultPosition();
		return position;
	}

	private boolean hasSavedGalleryPosition() {
		return lastPaymentGalleryPosition != NO_SAVED_PAYMENT_GALLERY_POSITION;
	}

	/**
	 * ViewPagers assume items will fill width. We provide a negative margin to allow
	 * items to take up only the space they need and allow additional items to be visible
	 */
	private int getPaymentCardMargin() {
		View pager = findViewById(R.id.pay_creditcard_select_payment_pager);
		float pagerWidthPx = pager.getWidth();		
		float cardWidthPx = getResources().getDimension(R.dimen.pay_card_select_payment_card_width);
		float freeHorizontalSpacePx = pagerWidthPx - cardWidthPx;
		float cardMarginPx = getResources().getDimension(R.dimen.pay_card_select_payment_card_margin);
		float negativePageMarginPx = -1 * (freeHorizontalSpacePx - cardMarginPx);
		return (int) negativePageMarginPx;
	}

	private void showTooltip() {
		getTooltipManager().setGroup(new PayCreditCardTooltipGroup(managePaymentsEnabled, hasActivePaymentMethods()));
		getTooltipManager().show();
	}
	public void onAddDebitCardPressed(View view) {
		startAddDebitCardActivty();
	}

	public void onDeleteDebitCardPressed(View view) {
		confirmDelete();
	}
	
	private void confirmDelete() {
		TescoAlertDialog.Builder builder = new TescoAlertDialog.Builder(this);
		builder.setTitle(R.string.confirm_delete_debit_card_title);
		builder.setMessage(R.string.confirm_delete_debit_card_message);
		builder.setNegativeButton(R.string.dialog_confirm_cancel, null);
		builder.setPositiveButton(R.string.dialog_confirm_ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				trackDeleteDebitCardConfirmationAnalytic();
				startDeleteDebitCardRequest();
			}
		});
		confirmationDialog = builder.show();
	}
	
	private void trackDeleteDebitCardConfirmationAnalytic() {
		getApplicationState().getAnalyticsTracker().trackDeleteDebitCardConfirmation();
	}

	private void trackDeleteDebitCardCompletedSuccessfullyAnalytic() {
		getApplicationState().getAnalyticsTracker().trackDeleteDebitCardCompletedSuccessfully(creditCardProductDetails);
	}
	
	private void startDeleteDebitCardRequest() {
		PayCCDeleteDebitCardRequest request = new PayCCDeleteDebitCardRequest();
		request.setProductTokenID(creditCardProductDetails.getProductId());
		request.setPaymentMethodTokenID(paymentMethod.getPaymentMethodId());
		getApplicationState().getRestRequestProcessor().processRequest(request);
		hidePaymentMethodGallery();
	}
	
	private void displaySuccessOverlay(Display display) {
		successOverlay = new SuccessResultOverlayRenderer(this, display);
		successOverlay.renderOverlay(null);
	}

	private void dismissSuccessOverlay() {
		if (successOverlay != null) {
			successOverlay.dismissOverlay();
		}		
	}
	
	private void dismissConfirmationDialog() {
		if(isConfirmationDialogShowing()) {
			confirmationDialog.dismiss();
		}
	}

	private boolean isConfirmationDialogShowing() {
		return confirmationDialog != null && confirmationDialog.isShowing();
	}
	
	private void hidePaymentMethodGallery() {
		getTooltipManager().hide();
		paymentMethodsGallery.setVisibility(View.INVISIBLE);
		hideAndShowProgressIndicator.showInProgressIndicator();
	}
	
	private void refreshPaymentMethodGallery() {
		paymentMethodsAdapter.updatePaymentMethods(getPaymentMethods());
		showPaymentMethodGallery();
	}

	private List<PaymentMethod> getPaymentMethods() {
		return hasPaymentMethods() ? retrievePaymentMethodsResponse.getPaymentMethods() : new ArrayList<PaymentMethod>();
	}

	private void showAnyErrors() {
		if (!hasPaymentMethods()) {
			handleError(getErrorBuilder().buildErrorResponse(InAppError.NoDebitCards));
		} else if(!hasActivePaymentMethods()) {
			handleError(getErrorBuilder().buildErrorResponse(InAppError.NoActiveCards));	
		}
	}

	private void handleError(ErrorResponseWrapper errorResponseWrapper) {
		handleErrorResponse(errorResponseWrapper);
	}

	public void doPayOffCreditCardBalance(View view) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Clicking doPayOffCreditCardBalance");
		}

		if (statementBalanceCheckbox.isChecked()) {
			statementBalanceCheckbox.setChecked(false);
			refreshInProgressIndicator();
		} else {
			statementBalanceCheckbox.setChecked(true);
			doPayOffCreditCardBalanceCheckBoxChecked(view);
		}

	}

	public void doPayOffCreditCardBalanceCheckBoxChecked(View view) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Clicking doPayOffCreditCardBalanceCheckBoxChecked");
		}

		minimumAmountCheckbox.setChecked(false);
		otherAmountCheckbox.setChecked(false);
		enableOtherAmountEditText(false);
		refreshInProgressIndicator();
	}

	private void handleTextChange(TescoEditText otherAmountField) {

		try {
			if (otherAmountField.length() > 0) {
				otherAmountField.setPrefix(getResources().getString(R.string.movemoney_amount_selector_pound_symbol));
				otherAmountField.setTextSuffixColour(getResources().getColor(R.color.blue_tesco));
				otherAmountField.setSelection(otherAmountField.getText().length());
			} else {
				otherAmountField.setPrefix("");
			}
		} catch (NumberFormatException e) {
			otherAmountField.setText("");
		}
	}

	public void doPayOffCreditCardMinBalance(View view) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Clicking doPayOffCredidCardMinBalance");
		}
		if (minimumAmountCheckbox.isChecked()) {
			minimumAmountCheckbox.setChecked(false);
			refreshInProgressIndicator();
		} else {
			minimumAmountCheckbox.setChecked(true);
			doPayOffCreditCardMinBalanceCheckboxChecked(view);
		}
	}

	public void doPayOffCreditCardMinBalanceCheckboxChecked(View view) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Clicking doPayOffCreditCardMinBalanceCheckboxChecked");
		}

		statementBalanceCheckbox.setChecked(false);
		otherAmountCheckbox.setChecked(false);
		enableOtherAmountEditText(false);
		refreshInProgressIndicator();
	}

	public void doPayOffCreditCardOtherAmount(View view) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Clicking doPayOffCredidCardOtherAmount");
		}
		if (otherAmountCheckbox.isChecked()) {
			otherAmountCheckbox.setChecked(false);
		} else {
			otherAmountCheckbox.setChecked(true);
		}
		doPayOffCreditCardOtherAmountCheckBoxChecked(view);
	}

	public void doPayOffCreditCardOtherAmountCheckBoxChecked(View view) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Clicking doPayOffCredidCardOtherAmountCheckBoxChecked");
		}

		enableOtherAmountEditText(otherAmountCheckbox.isChecked());
		statementBalanceCheckbox.setChecked(false);
		minimumAmountCheckbox.setChecked(false);
		refreshInProgressIndicator();
	}

	public void doPayOffCreditCardOtherAmountEditTextChecked(View view) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Clicking doPayOffCreditCardOtherAmountEditTextChecked");
		}
		if (!otherAmount.isEnabled()) {
			doPayOffCreditCardOtherAmount(view);
		}
	}

	private void enableOtherAmountEditText(boolean enable) {

		otherAmount.setEnabled(enable);
		otherAmount.setFocusable(enable);
		otherAmount.setFocusableInTouchMode(enable);
		otherAmount.setText("");

		if (enable) {
			otherAmount.requestFocus();
			showKeyboard();
		} else {
			otherAmount.clearFocus();
			hideKeyboard();
		}
	}

	private void startAddDebitCardActivty() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting startAddDeditCardPaymentMethodsActivity");
		}

		Intent intent = new Intent(this, AddDebitCardActivity.class);
		intent.putExtra(PAY_CREDITCARD_PRODUCT_EXTRA, creditCardProductDetails);
		intent.putExtra(PAY_CREDITCARD_NICKNAMES_EXTRA, getCardNickNames());
		startActivityForResult(intent, REQUEST_DEFAULT);
	}
	
	private void onPaymentMethodGalleryTileChanged(int position) {
		lastPaymentGalleryPosition = position;
		if(paymentMethodsAdapter.isPaymentTile(position)) {
			PaymentMethodTile galleryTile = PaymentMethodTile.class.cast(paymentMethodsAdapter.getTile(position));
			paymentMethod = galleryTile.getData();
			trackPayCreditCardSelectDebitCardAnalytic();
		} else {
			paymentMethod = null;
		}
		refreshInProgressIndicator();
	}
	
	private boolean hasPaymentMethods() {
		return retrievePaymentMethodsResponse != null && retrievePaymentMethodsResponse.getPaymentMethods() != null && !retrievePaymentMethodsResponse.getPaymentMethods().isEmpty();
	}
	
	private boolean hasActivePaymentMethods() {
		for(PaymentMethod paymentMethod : getPaymentMethods()) {
			if(!paymentMethod.hasExpired()) {
				return true;
			}
		}
		return false;
	}
	
	private ArrayList<String> getCardNickNames() {
		return getCardNickNames(getPaymentMethods());
	}
	
	private ArrayList<String> getCardNickNames(List<PaymentMethod> paymentMethods) {
		ArrayList<String> nicknames = new ArrayList<String>();		
		for(PaymentMethod paymentMethod : paymentMethods) {
			nicknames.add(paymentMethod.getCardName());
		}
		return nicknames;
	}

	private void trackPayCreditCardSelectDebitCardAnalytic() {
		getApplicationState().getAnalyticsTracker().trackPayCreditCardSelectDebitCard();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onActivityResult: " + resultCode);
		}

		if (resultCode == RESULT_CC_CARD_ADDED) {
			retrievePaymentMethods();
		} else if (requestCode == REQUEST_CC_PAYMENT) {
			onActivityCvvResult(requestCode, resultCode, data);
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	protected void onActivityCvvResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == ActivityResultCode.RESULT_CC_PAYMENT_COMPLETED) {
			finishWithResult(resultCode);
		} else if (resultCode == ActivityResultCode.RESULT_CC_PAYMENT_CANCELLED) {
			doNothing();
		} else if (resultCode == ActivityResultCode.RESULT_CC_PAYMENT_FAILED) {
			doNothing();
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
	
	private void doNothing() {
		// Do nothing
	}

	@Override
	protected void onPause() {
		super.onPause();
		getTooltipManager().hide();
	}

	@Override
	protected void onDestroy() {
		dismissConfirmationDialog();
		dismissSuccessOverlay();
		super.onDestroy();
	}

	@Override
	protected void onRetryPressed() {
		retrievePaymentMethods();
	}

	@Override
	public boolean backFromRetryErrorShouldRetry() {
		return false;
	}

	public void onContinuePressed(View view) {
		CreditCardPayment payment = new CreditCardPayment(paymentMethod, creditCardProductDetails);

		if (statementBalanceCheckbox.isChecked()) {
			payment.setAmount(creditCardProductDetails.getStatementBalance());
			payment.setCreditCardPaymentType(CreditCardPaymentType.FullBalance);
		} else if (minimumAmountCheckbox.isChecked()) {
			payment.setAmount(creditCardProductDetails.getMinimumPayment());
			payment.setCreditCardPaymentType(CreditCardPaymentType.Min);
		} else if (otherAmountCheckbox.isChecked()) {
			String amt = otherAmount.getText().toString();
			payment.setAmount(new BigDecimal(amt));
			payment.setCreditCardPaymentType(CreditCardPaymentType.Other);
		} else {
			handleError(getErrorBuilder().buildErrorResponse(InAppError.CannotPayZero));
			return;
		}

		if (payment.getAmount().setScale(2).equals(BigDecimal.ZERO.setScale(2))) {
			handleError(getErrorBuilder().buildErrorResponse(InAppError.CannotPayZero));
		} else {
			displayCvvEntryActivity(payment);
		}
	}

	private void displayCvvEntryActivity(CreditCardPayment payment) {
		Intent intent = new Intent(this, EnterCvvAndConfirmPaymentActivity.class);
		intent.putExtra(EnterCvvAndConfirmPaymentActivity.CC_PAYMENT_EXTRA, payment);
		startActivityForResult(intent, ActivityRequestCode.REQUEST_CC_PAYMENT);
	}

	private class PayyCCRetrievePaymentMethodsRequest extends RetrievePaymentMethodsRequest {

		@Override
		public void onResponse(RetrievePaymentMethodsResponse response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Successfully received PaymentMethods");
			}
			paymentRetrievalInProgress = false;
			retrievePaymentMethodsResponse = response;
			lastPaymentGalleryPosition = NO_SAVED_PAYMENT_GALLERY_POSITION;
			showAnyErrors();
			refreshPaymentMethodGallery();
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
			paymentRetrievalInProgress = false;
			hidePaymentMethodGallery();
			handleError(errorResponseWrapper);
		}
	}
	
	private class PayCCDeleteDebitCardRequest extends DeleteCardPaymentMethodRequest {

		@Override
		public void onResponse(SuccessResponseWrapper response) {
			retrievePaymentMethods();
			displaySuccessOverlay(response.getSuccessResponse().getDisplay());
			trackDeleteDebitCardCompletedSuccessfullyAnalytic();
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponse) {
			handleError(errorResponse);
		}		
	}
	
	public class GalleryPageSelectionListener extends ViewPager.SimpleOnPageChangeListener {
		@Override
		public void onPageSelected(int position) {
			onPaymentMethodGalleryTileChanged(position);
			getTooltipManager().dismiss(TooltipId.AddACard.ordinal());
		}
	}
}
