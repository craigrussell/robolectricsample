package com.tescobank.mobile.ui.paycreditcard;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow.OnDismissListener;

import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.api.payment.MakeCreditCardPaymentRequest;
import com.tescobank.mobile.api.payment.MakeCreditCardPaymentResponse;
import com.tescobank.mobile.api.payment.ThreeDSecureEnrollmentStatusRequest;
import com.tescobank.mobile.api.payment.ThreeDSecureEnrollmentStatusResponse;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.ActivityRequestCode;
import com.tescobank.mobile.ui.ActivityResultCode;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.InProgressIndicator.ContinueButtonConfiguration;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoEditText;
import com.tescobank.mobile.ui.TescoTextView;
import com.tescobank.mobile.ui.movemoney.MoneyMovementResultOverlayRenderer;
import com.tescobank.mobile.ui.prodheader.ProductHeaderViewFactory;

public class EnterCvvAndConfirmPaymentActivity extends TescoActivity implements ContinueButtonConfiguration {

	private static final String TAG = EnterCvvAndConfirmPaymentActivity.class.getSimpleName();

	public static final String CC_PAYMENT_EXTRA = "CC_PAYMENT_EXTRA";

	private DataFormatter dataFormatter;
	private String cvv;
	private TescoEditText cvvInput;
	private InProgressIndicator inProgressIndicator;
	private CreditCardPayment payment;
	private MoneyMovementResultOverlayRenderer<CreditCardPayment> resultRenderer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting PayCreditCardActivity");
		}
		super.onCreate(savedInstanceState);

		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		setContentView(R.layout.activity_enter_cvv);

		dataFormatter = new DataFormatter();
		resultRenderer = new MoneyMovementResultOverlayRenderer<CreditCardPayment>(this) {
			@Override
			protected String getMoveMoneyTypeForDisplay() {
				return "Payment";
			}
		};
		payment = (CreditCardPayment) getIntent().getSerializableExtra(CC_PAYMENT_EXTRA);

		configureHeader();
		configurePaymentDetails();
		configureTextFields();
		configureInProgressIndicator();
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if(hasFocus && cvvInput.getText().length() != 3) {
			showKeyboard();
		}
		super.onWindowFocusChanged(hasFocus);
	}

	@Override
	protected void onStart() {
		trackCvvAnalytic();
		super.onStart();
	}

	private void trackCvvAnalytic() {
		getApplicationState().getAnalyticsTracker().trackPayCreditCardEnterCvv();
	}

	private void trackConfirmationAnalytic() {
		getApplicationState().getAnalyticsTracker().trackPayCreditCardConfirmation(payment, payment.getCreditCardPaymentType().name());
	}
	
	private void trackPayCreditCardSuccessfullAnalytic() {
		getApplicationState().getAnalyticsTracker().trackPayCreditCardCompletedSuccessfully(payment);
	}
	
	private void trackPayCreditCardUnsuccessfullAnalytic() {
		getApplicationState().getAnalyticsTracker().trackPayCreditCardCompletedUnsuccessfully(payment);
	}

	private void configureHeader() {
		ViewGroup headerContainer = (ViewGroup) findViewById(R.id.transfer_header);
		ProductHeaderViewFactory productHeaderViewFactory = new ProductHeaderViewFactory(payment.getDestination(), this, headerContainer);
		productHeaderViewFactory.createHeaderView();
	}

	private void configurePaymentDetails() {
		View cardView = findViewById(R.id.enter_cvv_card_container);
		PaymentMethodViewUpdater cardViewUpdater = new PaymentMethodViewUpdater(getApplicationContext(),cardView);
		cardViewUpdater.update(payment.getSource());

		TescoTextView amount = (TescoTextView) findViewById(R.id.enter_cvv_amount_text);
		amount.setText(dataFormatter.formatCurrency(payment.getAmount()));
	}

	private void configureTextFields() {
		cvvInput = (TescoEditText) findViewById(R.id.cvvInput);
		cvvInput.requestFocus();
		cvvInput.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				handleTextChanged(s);
			}

		});
	}

	private void handleTextChanged(Editable s) {
		if (s.length() == 3) {
			hideKeyboard();
			inProgressIndicator.setContinueEnabled(true);
		} else {
			inProgressIndicator.setContinueEnabled(false);
		}
	}
	
	private void configureInProgressIndicator() {
		inProgressIndicator = new InProgressIndicator(this, true);
		inProgressIndicator.updateContinueButtonConfiguration(this);
		refreshInProgressIndicator();
	}

	private void refreshInProgressIndicator() {
		boolean show = cvvInput.getText().length() >= 3;
		inProgressIndicator.setContinueEnabled(show);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			onPaymentCancelled();
			return true;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	public void onContinuePressed(View view) {
		trackConfirmationAnalytic();
		inProgressIndicator.showInProgressIndicator();
		cvvInput.setEnabled(false);
		cvv = cvvInput.getText().toString();
		getThreeDSecureEnrollmentStatus();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onActivityResult: " + resultCode);
		}

		if (requestCode == ActivityRequestCode.REQUEST_3D_SECURE) {
			onActivityThreeDSecureResult(resultCode, data);
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	protected void onActivityThreeDSecureResult(int resultCode, Intent data) {
		if (resultCode == ActivityResultCode.RESULT_THREE_D_SECURE_COMPLETED) {
			String paRes = data.getStringExtra(ThreeDSecureActivity.THREEDS_PARES_EXTRA);
			makeCreditCardPayment(paRes);
		} else if (resultCode == ActivityResultCode.RESULT_THREE_D_SECURE_ERROR) {
			handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.ThreeDSecureNotLoaded));
		} else if (resultCode == ActivityResultCode.RESULT_THREE_D_SECURE_CANCELLED) {
			inProgressIndicator.hideInProgressIndicator();
			cvvInput.setEnabled(true);
		} else {
			super.onActivityResult(ActivityRequestCode.REQUEST_3D_SECURE, resultCode, data);
		}
	}

	private void getThreeDSecureEnrollmentStatus() {

		ThreeDSecureEnrollmentStatusRequest request = new ThreeDSecureEnrollmentStatusRequest() {
			@Override
			public void onResponse(ThreeDSecureEnrollmentStatusResponse response) {
				onEnrollmenetStatusReceived(response);
			}

			@Override
			public void onErrorResponse(ErrorResponseWrapper errorResponse) {
				handleErrorResponse(errorResponse);
			}
		};
		request.setProductId(payment.getDestination().getProductId());
		request.setPaymentMethodId(payment.getSource().getPaymentMethodId());
		request.setAmount(payment.getAmount());
		getApplicationState().getRestRequestProcessor().processRequest(request);
	}

	private void makeCreditCardPayment(String paRes) {

		MakeCreditCardPaymentRequest request = new MakeCreditCardPaymentRequest() {

			@Override
			public void onResponse(MakeCreditCardPaymentResponse response) {
				trackPayCreditCardSuccessfullAnalytic();
				onPaymentSucceeded();
			}

			@Override
			public void onErrorResponse(ErrorResponseWrapper errorResponse) {
				if (errorResponse.getErrorResponse().isDeclined()) {
					trackPayCreditCardUnsuccessfullAnalytic();
					onPaymentDeclined(errorResponse);
				} else {
					handleErrorResponse(errorResponse);
				}
			}
		};
		
		request.setCvv(cvv);
		request.setAmount(payment.getAmount());
		request.setProductId(payment.getDestination().getProductId());
		request.setDated(payment.getDate());
		request.setPaymentMethodId(payment.getSource().getPaymentMethodId());
		request.setSecondaryAccountID(payment.getSource().getCardNumber());

		if (paRes != null) {
			request.setPaRes(paRes);
		}
		getApplicationState().getRestRequestProcessor().processRequest(request);			
	}

	private void onEnrollmenetStatusReceived(ThreeDSecureEnrollmentStatusResponse response) {
		if (response.getEnrollmentStatus()) {
			Intent intent = new Intent(this, ThreeDSecureActivity.class);
			intent.putExtra(ThreeDSecureEnrollmentStatusResponse.class.getName(), response);
			startActivityForResult(intent, ActivityRequestCode.REQUEST_3D_SECURE);
		} else {
			makeCreditCardPayment(null);
		}
	}

	private void onPaymentSucceeded() {
		resultRenderer.renderMoneyMovementSuccessOverlay(payment, new OnDismissListener() {
			@Override
			public void onDismiss() {
				onPaymentSuccessOverlayDismissed();
			}
		});
		inProgressIndicator.hideAll();
	}

	private void onPaymentDeclined(ErrorResponseWrapper errorResponse) {
		resultRenderer.renderMoneyMovementDeclinedOverlay(payment, errorResponse, new OnDismissListener() {
			@Override
			public void onDismiss() {
				onPaymentDeclinedOverlayDismissed();
			}
		});
		inProgressIndicator.hideAll();
	}

	public void onPaymentSuccessOverlayDismissed() {
		finishWithResult(ActivityResultCode.RESULT_CC_PAYMENT_COMPLETED);
	}

	public void onPaymentDeclinedOverlayDismissed() {
		finishWithResult(ActivityResultCode.RESULT_CC_PAYMENT_FAILED);
	}

	private void onPaymentCancelled() {
		finishWithResult(ActivityResultCode.RESULT_CC_PAYMENT_CANCELLED);
	}
	
	@Override
	public void unlockScreenOrientation() {
		//do nothing
	}

	@Override
	public void onBackPressed() {
		if (resultRenderer.dismissOverlay()) {
			return;
		}

		if (!inProgressIndicator.isShowingInProgress()) {
			onPaymentCancelled();
		}
	}

	@Override
	protected void onRetryPressed() {
		inProgressIndicator.hideInProgressIndicator();
		cvvInput.setEnabled(true);
		cvvInput.setText(null);
	}

	@Override
	public int getMessageId() {
		return R.string.progress_confirm;
	}

	@Override
	public int getBackgroundDrawable() {
		return R.drawable.button_default_background_selector;
	}

	@Override
	public int getTextColors() {
		return R.color.button_default_text_selector;
	}
}
