package com.tescobank.mobile.ui.paycreditcard;

import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_CC_CARD_ADDED;
import static com.tescobank.mobile.ui.paycreditcard.PayCreditCardActivity.PAY_CREDITCARD_NICKNAMES_EXTRA;
import static com.tescobank.mobile.ui.paycreditcard.PayCreditCardActivity.PAY_CREDITCARD_PRODUCT_EXTRA;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.Display;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.api.portfolio.AddCardPaymentMethodRequest;
import com.tescobank.mobile.api.portfolio.AddCardPaymentMethodRequestBody;
import com.tescobank.mobile.api.portfolio.AddCardPaymentMethodRequestBody.CardPaymentMethod;
import com.tescobank.mobile.api.success.SuccessResponseWrapper;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.overlay.SuccessResultOverlayRenderer;
import com.tescobank.mobile.ui.paycreditcard.model.AddDebitCardModelValidator;
import com.tescobank.mobile.ui.paycreditcard.model.DebitCardValidator;

public class AddDebitCardActivity extends TescoActivity implements TextWatcher {

	private static final String TAG = AddDebitCardActivity.class.getSimpleName();
	private static final int EXPIRY_YEARS_NUMBER_YEARS_TO_SHOW = 9;

	private TextView informationTextView;
	private TextView cardPanTextView;
	private TextView nicknameTextView;
	private TextView postcodeTextView;
	private TextView addressLineTextView;
	private Spinner expiryDateMonth;
	private Spinner expiryDateYear;
	private InProgressIndicator inProgressIndicator;
	private SuccessResultOverlayRenderer successOverlay;

	private ProductDetails productDetails;
	private List<String> cardNicknames;
	private AddDebitCardModelValidator modelValidator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting AddDebitCardActivity");
		}
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_debit_card);
		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		Bundle extras = getIntent().getExtras();
		validateExtras(extras);
		productDetails = extractProductDetails(extras);
		cardNicknames = extractCardNicknames(extras);
		modelValidator = new DebitCardValidator();

		initialiseInProgressIndicator();
		initialiseViewReferences();
		initialiseTextViews();
		initialiseExpiryDateMonthSpinner();
		initialiseExpiryDateYearSpinner();
		initialiseInformationText();
		hideKeyboard();
		trackAddDebitCardAnalytic();
	}

	private void initialiseInformationText() {
		informationTextView.setText(getString(R.string.addDebitCardBillingAddressInstructions));
	}

	private void initialiseViewReferences() {
		cardPanTextView = TextView.class.cast(findViewById(R.id.enter_pan_text));
		nicknameTextView = TextView.class.cast(findViewById(R.id.enter_nickname_text));
		postcodeTextView = TextView.class.cast(findViewById(R.id.enter_billing_address_postcode_text));
		addressLineTextView = TextView.class.cast(findViewById(R.id.enter_billing_address_first_line_text));
		expiryDateMonth = Spinner.class.cast(findViewById(R.id.enter_expiry_valid_from_month));
		expiryDateYear = Spinner.class.cast(findViewById(R.id.enter_expiry_valid_from_year));
		informationTextView = TextView.class.cast(findViewById(R.id.informationText));
	}
	
	private void initialiseTextViews() {
		cardPanTextView.addTextChangedListener(this);
		nicknameTextView.addTextChangedListener(this);
		postcodeTextView.addTextChangedListener(this);
		addressLineTextView.addTextChangedListener(this);
		informationTextView.addTextChangedListener(this);
	}
	
	private void initialiseExpiryDateMonthSpinner() {
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.expiry_months, R.layout.expiry_date_spinner_text);
		adapter.setDropDownViewResource(R.layout.expiry_date_spinner_text_dropdown);
		expiryDateMonth.setAdapter(adapter);
	}

	private void initialiseExpiryDateYearSpinner() {
		List<Integer> years = buildYearChoices();
		ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this, R.layout.expiry_date_spinner_text, years);
		adapter.setDropDownViewResource(R.layout.expiry_date_spinner_text_dropdown);
		expiryDateYear.setAdapter(adapter);
	}
	
	private void initialiseInProgressIndicator() {
		inProgressIndicator = new InProgressIndicator(this, true);
		inProgressIndicator.setContinueEnabled(false);
	}
	
	private List<Integer> buildYearChoices() {
		Calendar c = Calendar.getInstance();
		int currentYear = c.get(Calendar.YEAR);

		List<Integer> years = new ArrayList<Integer>();

		int upperYear = currentYear + EXPIRY_YEARS_NUMBER_YEARS_TO_SHOW;
		for (int i = currentYear; i < upperYear; i++) {
			years.add(i);
		}

		return years;
	}

	private void validateExtras(Bundle extras) {
		if (extras == null) {
			throw new IllegalArgumentException("Extras cannot be null");
		}
		validateExtraPresentAndNotNull(PAY_CREDITCARD_PRODUCT_EXTRA);
		validateExtraPresentAndNotNull(PAY_CREDITCARD_NICKNAMES_EXTRA);
	}

	private void validateExtraPresentAndNotNull(String key) {
		if (!getIntent().getExtras().containsKey(key)) {
			throw new IllegalArgumentException("Missing required extra: " + key);
		}
		if (getIntent().getExtras().get(key) == null) {
			throw new IllegalArgumentException(String.format("Extra: %s cannot be null", key));
		}
	}

	private ProductDetails extractProductDetails(Bundle extras) {
		return (ProductDetails) getIntent().getExtras().getSerializable(PAY_CREDITCARD_PRODUCT_EXTRA);
	}
	
	@SuppressWarnings("unchecked")
	private List<String> extractCardNicknames(Bundle extras) {
		return List.class.cast(getIntent().getExtras().getSerializable(PAY_CREDITCARD_NICKNAMES_EXTRA));
	}
	
	private void trackAddDebitCardAnalytic() {
		getApplicationState().getAnalyticsTracker().trackAddDebitCard();
	}
	
	private void trackAddDebitCardConfirmationAnalytic() {
		getApplicationState().getAnalyticsTracker().trackAddDebitCardConfirmation();
	}
	
	private void trackAddDebitCardCompletedSuccessfullyAnalytic() {
		getApplicationState().getAnalyticsTracker().trackAddDebitCardCompletedSuccessfully(productDetails);
	}

	public void onContinuePressed(View view) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Continue");
		}

		validateInput();
		if (!modelValidator.hasErrors()) {
			trackAddDebitCardConfirmationAnalytic();
			submitRequest();
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		dismissSuccessOverlay();
	}
	
	@Override
	protected void onDestroy() {
		dismissSuccessOverlay();
		super.onDestroy();
	}

	private void dismissSuccessOverlay() {
		if (successOverlay != null) {
			successOverlay.dismissOverlay();
		}		
	}
	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	@Override
	public void afterTextChanged(Editable s) {
		refreshInProgressIndicator();
	}

	private void refreshInProgressIndicator() {
		inProgressIndicator.setContinueEnabled(allTextFieldsHaveText());
	}

	private boolean allTextFieldsHaveText() {
		return hasText(nicknameTextView) && hasText(cardPanTextView) && hasText(addressLineTextView) && hasText(postcodeTextView);
	}
	
	private boolean hasText(TextView textView) {
		return textView != null && textView.getText() != null && textView.getText().length() > 0;
	}

	private void validateInput() {
		modelValidator.clearErrors();
		validateTextInput();
		validateExpiryDateIfNoOtherErrors();
	}
	
	private void validateTextInput() {
		validatePamLength();
		validatePamCharacters();
		validateNicknameUniqueness();
		validateNicknameLength();
		validateNicknameCharacters();
		validatePostcodeLength();
		validatePostcodeCharacters();
		validateAddressLineLength();
		validateAddressLineCharacters();
		displayPassiveTextInputError();
	}	
	
	private void validatePamLength() {
		if (!modelValidator.validatePanLength(getCardPan())) {
			cardPanTextView.setError(getString(R.string.error_debit_card_form_pan_length));
		}
	}

	private void validatePamCharacters() {
		if (!modelValidator.validatePanCharacters(getCardPan())) {
			cardPanTextView.setError(getString(R.string.error_debit_card_form_pan_chars));
		}
	}
	
	private void validateNicknameLength() {
		if (!modelValidator.validateNicknameLength(getNickname())) {
			nicknameTextView.setError(getString(R.string.error_debit_card_form_nickname_too_long));
		}
	}
	
	private void validateNicknameUniqueness() {
		if(!modelValidator.validateNicknameUnique(getNickname(), cardNicknames)) {
			nicknameTextView.setError(getString(R.string.error_debit_card_form_nickname_in_use));			
		}
	}

	private void validateNicknameCharacters() {
		if(!modelValidator.validateNicknameCharacters(getNickname())) {
			nicknameTextView.setError(getString(R.string.error_debit_card_form_nickname_chars));
		}
	}
	
	private void validatePostcodeLength() {
		if (!modelValidator.validatePostcodeLength(getPostcode())) {
			postcodeTextView.setError(getString(R.string.error_debit_card_form_postcode_too_long));
		}
	}

	private void validatePostcodeCharacters() {
		if(!modelValidator.validatePostcodeCharacters(getPostcode())) {
			postcodeTextView.setError(getString(R.string.error_debit_card_form_postcode_chars));			
		}
	}

	private void validateAddressLineLength() {
		if (!modelValidator.validateAddressLength(getAddressLine())) {
			addressLineTextView.setError(getString(R.string.error_debit_card_form_address_too_long));			
		}
	}

	private void validateAddressLineCharacters() {
		if (!modelValidator.validateAddressCharacters(getAddressLine())) {
			addressLineTextView.setError(getString(R.string.error_debit_card_form_address_chars));
		}
	}
	
	private void validateExpiryDateIfNoOtherErrors() {
		if(!modelValidator.hasErrors()) {
			validateExpiryDate();
		}
	}

	private void validateExpiryDate() {
		Date selectedDate = buildSelectedDate();
		if (!modelValidator.validateExpiryDate(selectedDate, getMonthStart())) {
			displayPassiveExpiryDateError();
		}
	}
	
	private Date getMonthStart() {
		Calendar now = Calendar.getInstance();
		Calendar startOfMonth = Calendar.getInstance();
		startOfMonth.clear();
		startOfMonth.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH), 1);
		return startOfMonth.getTime();
	}
	
	private void displayPassiveTextInputError() {
		if(modelValidator.hasErrors()) {
			handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.FormValidation));
		}
	}
	
	private void displayPassiveExpiryDateError() {
		handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.ExpiryDateValidation));
	}

	private void submitRequest() {
		inProgressIndicator.showInProgressIndicator();
		CardPaymentMethod paymentMethod = buildPaymentMethod();
		AddCardPaymentMethodRequestBody body = buildPaymentRequestBody(paymentMethod);
		AddCardPaymentMethodRequest request = buildPaymentRequest(body);
		getApplicationState().getRestRequestProcessor().processRequest(request);
	}

	private CardPaymentMethod buildPaymentMethod() {
		CardPaymentMethod cpm = new CardPaymentMethod();
		cpm.setAddressLineOne(getAddressLine());
		cpm.setPostCode(getPostcode());
		cpm.setCardNickName(getNickname());
		cpm.setCardPan(getCardPan());
		cpm.setExpiresEndMonth(getExpiryMonth());
		cpm.setExpiresEndYear(getExpiryYear());
		return cpm;
	}

	private AddCardPaymentMethodRequestBody buildPaymentRequestBody(CardPaymentMethod paymentMethod) {
		AddCardPaymentMethodRequestBody requestBody = new AddCardPaymentMethodRequestBody();
		requestBody.setCardPaymentMethod(paymentMethod);
		return requestBody;
	}

	private AddCardPaymentMethodRequest buildPaymentRequest(AddCardPaymentMethodRequestBody body) {
		AddCardPaymentMethodRequest request = new AddDebitCardPaymentRequest(body);
		request.setProductTokenID(productDetails.getProductId());
		return request;
	}

	private Date buildSelectedDate() {
		int expiryMonth = expiryDateMonth.getSelectedItemPosition();
		int expiryYear = Integer.class.cast(expiryDateYear.getSelectedItem());
		Calendar c = Calendar.getInstance();
		c.clear();
		c.set(Calendar.MONTH, expiryMonth);
		c.set(Calendar.YEAR, expiryYear);
		return c.getTime();
	}
	
	private void onSuccess(SuccessResponseWrapper response) {
		inProgressIndicator.hideAll();
		displaySuccessOverlay(response.getSuccessResponse().getDisplay());
		trackAddDebitCardCompletedSuccessfullyAnalytic();
	}

	private void displaySuccessOverlay(Display display) {
		successOverlay = new SuccessResultOverlayRenderer(this, display);
		successOverlay.renderOverlay(new OnDismissListener() {
			@Override
			public void onDismiss() {
				onDismissSuccessOverlay();
			}
		});
	}
	
	private void onDismissSuccessOverlay() {
		finishWithResult(RESULT_CC_CARD_ADDED );
	}
	
	private void onError(ErrorResponseWrapper errorResponse) {
		inProgressIndicator.hideInProgressIndicator();
		handleErrorResponse(errorResponse);
	}

	private String getAddressLine() {
		return addressLineTextView.getText().toString();
	}

	private String getPostcode() {
		return postcodeTextView.getText().toString();
	}

	private String getCardPan() {
		return cardPanTextView.getText().toString();
	}

	private String getNickname() {
		return nicknameTextView.getText().toString();
	}
	
	private int getExpiryMonth() {
		return expiryDateMonth.getSelectedItemPosition() + 1;
	}
	
	private int getExpiryYear() {
		int expiryYear = Integer.class.cast(expiryDateYear.getSelectedItem());
		return expiryYear % 100;
	}

	public void setModelValidator(AddDebitCardModelValidator modelValidator) {
		this.modelValidator = modelValidator;
	}
			
	private class AddDebitCardPaymentRequest extends AddCardPaymentMethodRequest {
		public AddDebitCardPaymentRequest(AddCardPaymentMethodRequestBody bodyObject) {
			super(bodyObject);
		}

		@Override
		public void onResponse(SuccessResponseWrapper response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Successfully received response AddDebitCardPaymentRequest");
			}
			onSuccess(response);
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponse) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Failed receiving response AddDebitCardPaymentRequest: " + errorResponse.getErrorResponse().toString());
			}
			onError(errorResponse);
		}
	}
}
