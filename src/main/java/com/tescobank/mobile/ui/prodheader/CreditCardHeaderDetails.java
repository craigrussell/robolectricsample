package com.tescobank.mobile.ui.prodheader;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;

public class CreditCardHeaderDetails extends AccountHeaderDetails {

	private DataFormatter dataFormatter;

	public CreditCardHeaderDetails(ProductDetails product, Context context) {
		super(product, context);
		this.dataFormatter = new DataFormatter();
	}

	@Override
	public String getProductNumber() {
		return DataFormatter.formatCardNumber(getProduct().getCardNumber());
	}

	@Override
	public String getAvailableTitle() {
		return getContext().getString(R.string.product_header_available_credit_title);
	}

	@Override
	public String getAvailable() {
		return dataFormatter.formatCurrency(getProduct().getAvailableCredit());
	}
}
