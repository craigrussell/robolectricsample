package com.tescobank.mobile.ui.prodheader;

import static com.tescobank.mobile.model.product.ProductType.typeForLabel;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tescobank.mobile.R;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.model.product.ProductType;
import com.tescobank.mobile.ui.TescoTextView;

public class ProductHeaderViewFactory {

	private Context context;
	private ViewGroup root;

	private ProductDetails product;
	private ProductHeaderDetails productHeaderDetails;

	public ProductHeaderViewFactory(ProductDetails product, Context context, ViewGroup root) {
		this.product = product;
		this.context = context;
		this.root = root;
		this.productHeaderDetails = generateProductHeaderDetails();
	}

	public View createHeaderView() {

		View view = LayoutInflater.from(context).inflate(productHeaderDetails.getProductHeaderViewId(), root);

		TescoTextView name = (TescoTextView) view.findViewById(R.id.product_header_name);
		TescoTextView number = (TescoTextView) view.findViewById(R.id.product_header_number);
		TescoTextView balanceTitle = (TescoTextView) view.findViewById(R.id.product_header_balance_title);
		TescoTextView balance = (TescoTextView) view.findViewById(R.id.product_header_balance);
		TescoTextView availableTitle = (TescoTextView) view.findViewById(R.id.product_header_available_balance_title);
		TescoTextView available = (TescoTextView) view.findViewById(R.id.product_header_available_balance);

		name.setText(productHeaderDetails.getProductName());
		number.setText(productHeaderDetails.getProductNumber());
		balanceTitle.setText(productHeaderDetails.getBalanceTitle());
		balance.setText(productHeaderDetails.getBalance());
		availableTitle.setText(productHeaderDetails.getAvailableTitle());
		available.setText(productHeaderDetails.getAvailable());

		return view;
	}

	private ProductHeaderDetails generateProductHeaderDetails() {
		ProductType productType = typeForLabel(product.getProductType());
		if (isCCProduct(productType)) {
			return new CreditCardHeaderDetails(product, context);
		} else if (isFixedRateSaverProduct(productType)) {
			return new FRSHeaderDetails(product, context);
		} else if (isISAProduct(productType)) {
			return new ISAHeaderDetails(product, context);
		} else if (isLoanProduct(productType)) {
			return new LoanHeaderDetails(product, context);
		} else if (isOtherProduct(productType)) {
			return new AccountHeaderDetails(product, context);
		} else {
			return null;
		}
	}
	
	private boolean isCCProduct(ProductType productType) {
		switch (productType) {
		case CC_PRODUCT:
			return true;
		default:
			return false;
		}
	}
	
	private boolean isFixedRateSaverProduct(ProductType productType) {
		switch (productType) {
		case FRS_PRODUCT:
			return true;
		default:
			return false;
		}
	}
	
	private boolean isISAProduct(ProductType productType) {
		switch (productType) {
		case IAI_PRODUCT:
		case JISA_PRODUCT:
		case FISA_PRODUCT:
		case FJISA_PRODUCT:
			return true;
		default:
			return false;
		}
	}
	
	private boolean isLoanProduct(ProductType productType) {
		switch (productType) {
		case LOAN_PRODUCT:
			return true;
		default:
			return false;
		}
	}
	
	private boolean isOtherProduct(ProductType productType) {
		switch (productType) {
		case PCA_PRODUCT:
		case IAS_PRODUCT:
		case IS_PRODUCT:
		case CCP_PRODUCT:
			return true;
		default:
			return false;
		}
	}
}
