package com.tescobank.mobile.ui.prodheader;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;

public class AccountHeaderDetails implements ProductHeaderDetails {

	private Context context;
	private DataFormatter dataFormatter;
	private ProductDetails product;

	public AccountHeaderDetails(ProductDetails product,Context context) {
		this.context = context;
		this.dataFormatter = new DataFormatter();
		this.product = product;
	}

	@Override
	public int getProductHeaderViewId() {
		return R.layout.product_header;
	}

	@Override
	public String getProductName() {
		return product.getProductName();
	}

	@Override
	public String getProductNumber() {
		return dataFormatter.formatSortCode(product.getSortCode()) + " " + product.getAccountNumber();
	}

	@Override
	public String getBalanceTitle() {
		return context.getString(R.string.product_header_balance_title);
	}

	@Override
	public String getBalance() {
		return dataFormatter.formatCurrency(product.getBalance());
	}

	@Override
	public String getAvailableTitle() {
		return context.getString(R.string.product_header_available_balance_title);
	}

	@Override
	public String getAvailable() {
		return dataFormatter.formatCurrency(null == product.getAvailableBalance() ? null : product.getAvailableBalance().abs());
	}

	@Override
	public ProductDetails getProduct() {
		return product;
	}
	
	public Context getContext() {
		return context;
	}
}
