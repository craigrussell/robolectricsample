package com.tescobank.mobile.ui.prodheader;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;

public class ISAHeaderDetails extends AccountHeaderDetails {

	private DataFormatter dataFormatter;
	
	public ISAHeaderDetails(ProductDetails product,Context context) {
		super(product,context);
		this.dataFormatter = new DataFormatter();
	}

	@Override
	public String getAvailableTitle() {
		return getContext().getString(R.string.product_header_available_isa_title);
	}

	@Override
	public String getAvailable() {
		return dataFormatter.formatCurrency(getProduct().getSubscriptionAmountAvailable());
	}
}
