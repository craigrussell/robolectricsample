package com.tescobank.mobile.ui.prodheader;

import com.tescobank.mobile.model.product.ProductDetails;

public interface ProductHeaderDetails {

	int getProductHeaderViewId();
	String getProductName();
	String getProductNumber();
	String getBalanceTitle();
	String getBalance();
	String getAvailableTitle();
	String getAvailable();
	ProductDetails getProduct();
}