package com.tescobank.mobile.ui.prodheader;

import android.content.Context;

import com.tescobank.mobile.model.product.ProductDetails;

public class FRSHeaderDetails extends AccountHeaderDetails {

	public FRSHeaderDetails(ProductDetails product,Context context) {
		super(product,context);
	}

	@Override
	public String getAvailableTitle() {
		return "";
	}

	@Override
	public String getAvailable() {
		return "";
	}

}
