package com.tescobank.mobile.ui.prodheader;

import java.math.BigDecimal;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;

public class LoanHeaderDetails extends AccountHeaderDetails {

	private DataFormatter dataFormatter;
	
	public LoanHeaderDetails(ProductDetails product,Context context) {
		super(product,context);
		this.dataFormatter = new DataFormatter();
	}
	
	@Override
	public String getBalanceTitle() {
		return super.getContext().getString(R.string.product_header_current_balance_title);
	}
	
	@Override
	public String getBalance() {
		BigDecimal amoutnRemaining = super.getProduct().getAmountRemaining();
		return dataFormatter.formatCurrency(amoutnRemaining);
	}

	@Override
	public String getAvailableTitle() {
		return "";
	}

	@Override
	public String getAvailable() {
		return "";
	}
}
