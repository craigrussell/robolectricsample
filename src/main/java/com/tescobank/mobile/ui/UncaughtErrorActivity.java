package com.tescobank.mobile.ui;

import android.app.Activity;
import android.os.Bundle;
import android.os.Process;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;

public class UncaughtErrorActivity extends Activity {

	private static final String TAG = UncaughtErrorActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating Activity");
		}
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_uncaught_error);
	}

	public void onButtonPressed(View view) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Closing Application");
		}
		ActivityCompat.finishAffinity(this);
		Process.killProcess(Process.myPid());
	}
}
