package com.tescobank.mobile.ui;

import android.graphics.Typeface;

public interface TypefaceDuckType {

	void setTypeface(Typeface typeface);
}
