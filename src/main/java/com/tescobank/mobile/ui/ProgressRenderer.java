package com.tescobank.mobile.ui;

/**
 * An object that shows / hides 'in progress'
 */
public interface ProgressRenderer {
	
	/**
	 * Provides a visual indication that an operation is in progress
	 */
	void showInProgressIndicator();
	
	/**
	 * Hides the visual indication that an operation is in progress
	 */
	void hideInProgressIndicator();
}
