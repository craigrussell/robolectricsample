package com.tescobank.mobile.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.ui.TescoEditText.OnDeleteListener;


/**
 * Handles the forward/backward navigation between a group of single digit text fields
 */
public class TescoEditTextDigitHandler {

	private static final String TAG = TescoEditTextDigitHandler.class.getSimpleName();
	
	private List<TescoEditText> inputFields;
	private Map<TescoEditText, TextChangeListener> editTextFieldTextChangeListenerMap = new HashMap<TescoEditText, TextChangeListener>();

	public void configure(List<TescoEditText> inputFields) {
		
		clearListeners();
		
		this.inputFields = inputFields;
		
		NextKeyListener nextKeyListener = new NextKeyListener();
		OnDeleteListener listener = new DeleteListener();
		
		for (TescoEditText inputField : inputFields) {
			// Respond to Next being pressed			
			inputField.setOnEditorActionListener(nextKeyListener);
			
			// Respond to text changes
			TextChangeListener textChangeListener = new TextChangeListener(inputField);
			inputField.addTextChangedListener(textChangeListener);
			editTextFieldTextChangeListenerMap.put(inputField, textChangeListener);
			
			// Respond to Delete being pressed
			inputField.setOnDeleteListener(listener);
		}
	}
	
	private void clearListeners() {		
		if(!editTextFieldTextChangeListenerMap.isEmpty()) {			
			for (TescoEditText inputField : inputFields) {
				TextChangeListener editTextListener = (TextChangeListener)editTextFieldTextChangeListenerMap.get(inputField);
				inputField.removeTextChangedListener(editTextListener);
			}	
			editTextFieldTextChangeListenerMap.clear();
		}
	}
	
	public void consumeAllTextForFirstAvailableEditTextField(TescoEditText firstAvailableEditText) {

		firstAvailableEditText.setFilters(new InputFilter[] {
			    new InputFilter() {
			    	public CharSequence filter(CharSequence source, int start,
			    		int end, Spanned dest, int dstart, int dend) {
			    		return source.length() < 1 ? dest.subSequence(dstart, dend) : "";
			    	}
			    }
			});
	}
	
	public void allowAllTextForFirstAvailableEditTextField(TescoEditText firstAvailableEditText) {
		firstAvailableEditText.setFilters(new InputFilter[] {});
	}

	public void clearValues() {
		for (TescoEditText inputField : inputFields) {
			inputField.setText("");
		}
	}
	
	public void disableAllFields() {
		for (TescoEditText inputField : inputFields) {
			inputField.setEnabled(false);
		}
	}
	
	public void enableAllFields() {
		for (TescoEditText inputField : inputFields) {
			inputField.setEnabled(true);
		}
	}
	
	
	public TescoEditText firstInputField() {
		return inputFields.isEmpty() ? null : inputFields.get(0);
	}
	
	
	public TescoEditText previousInputField(TescoEditText inputField) {
		int currentIndex = inputFields.indexOf(inputField);
		if(currentIndex > 0) {
			return inputFields.get(currentIndex-1);
		}
		return null;
	}
	
	public TescoEditText nextInputField(EditText inputField) {
		
		int currentIndex = inputFields.indexOf(inputField);
		int lastIndex = inputFields.size()-1;
		if(currentIndex < lastIndex) {
			return inputFields.get(currentIndex+1);
		}
		return null;
	}
	
	public boolean areInputsFilledIn() {
		for (TescoEditText inputField : inputFields) {
			if(!(inputField.getText().length() > 0)) {
				return false;
			}
		}
		return true;
	}
	
	public String getText() {
		StringBuilder text = new StringBuilder();
		for (TescoEditText inputField : inputFields) {
			text.append(inputField.getText().toString());
		}
		return text.toString();
	}
	
	public void onInputsFilledIn() {
		//do nothing
	}
	
	public void onInputsChanged() {
		//do nothing
	}
		
	/**
	 * Responds to Next being pressed
	 */
	private class NextKeyListener implements OnEditorActionListener {

		@Override
		public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
			
			if (actionId == EditorInfo.IME_ACTION_NEXT) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "Submitting");
				}
				return true;
			}			
			return false;
		}
	}

	/**
	 * Responds to text changes
	 */
	private class TextChangeListener implements TextWatcher {

		private TescoEditText inputField;

		public TextChangeListener(TescoEditText inputField) {
			super();

			this.inputField = inputField;
		}

		@Override
		public void afterTextChanged(Editable editable) {
			
			onInputsChanged();
			if (editable.length() > 0) {
				if (areInputsFilledIn()) {
					handleAllFieldsFilledIn();
				} else {
					handleNotAllFieldsFilledIn();
				}
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
		
		private void handleAllFieldsFilledIn() {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "All fields filled in");
			}
			onInputsFilledIn();
		}
		
		private void handleNotAllFieldsFilledIn() {
			EditText nextInputField = nextInputField(inputField);
			if (nextInputField != null) {
				
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "Moving focus to next input");
				}
				nextInputField.requestFocus();
			}
		}
	}
	
	/**
	 * Responds to Delete being pressed
	 */
	class DeleteListener implements OnDeleteListener {
		
		@Override
		public void onDeletePressed(TescoEditText editText) {
			if (editText.getText().toString().trim().isEmpty()) {
				TescoEditText previous = previousInputField(editText);
				if (previous != null) {
					
					if (BuildConfig.DEBUG) {
						Log.d(TAG, "Moving focus to previous input");
					}
					previous.requestFocus();
				}
			}
		}
	}

	public void requestFocus(List<TescoEditText> inputFieldsForRequiredValues) {
		for(TescoEditText inputFieldsForRequiredValue : inputFieldsForRequiredValues) {
			if(inputFieldsForRequiredValue.getText().length() == 0) {
				inputFieldsForRequiredValue.requestFocus();
				break;
			}
		}		
	}
}
