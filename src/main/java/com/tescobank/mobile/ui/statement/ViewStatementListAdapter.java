package com.tescobank.mobile.ui.statement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoTextView;

public class ViewStatementListAdapter extends BaseAdapter {

	private List<String> cycleDates;
	private LayoutInflater inflater;
	private DataFormatter dataFormatter = new DataFormatter();

	public ViewStatementListAdapter(TescoActivity activity, List<String> cycleDates) {
		this.cycleDates = cycleDates;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return cycleDates.size();
	}

	@Override
	public Object getItem(int position) {	
		return cycleDates.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View viewStatementListRow, ViewGroup parent) {
		View viewStatementListRowView = viewStatementListRow;

		if (viewStatementListRow == null) {
			viewStatementListRowView = inflater.inflate(R.layout.activity_view_statement_list_row, null);
		}
		TescoTextView statementDescription = (TescoTextView) viewStatementListRowView.findViewById(R.id.view_statement_description);
		
		String rawStatementDate = cycleDates.get(position);
		String statementDateString = dataFormatter.formatDate(rawStatementDate);
		try {
			Date statementDate = new SimpleDateFormat("yyyy-MM-dd", Locale.UK).parse(rawStatementDate);
			statementDescription.setText(dataFormatter.formatDateAsFullMonthAndYear(statementDate));
		} catch (ParseException e) {
			// Will fall back to default text
		}
		TescoTextView statementDateTextView = (TescoTextView) viewStatementListRowView.findViewById(R.id.statement_date);
		statementDateTextView.setText(statementDateString);		
		return viewStatementListRowView;
	}

}
