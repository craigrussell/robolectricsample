package com.tescobank.mobile.ui.statement;

import android.app.Activity;

import com.radaee.pdf.Document;
import com.radaee.pdf.Global;

/**
 * Defaults to the androidpdf.mobi which relies on native code.
 */
public class DefaultPDFInitialiser implements PDFInitialiser<Document> {

	@Override
	public void init(Activity activity) {
		Global.Init(activity);
	}

	@Override
	public PDFDocument<Document> createDocument() {
		final Document document = new Document();
		return new PDFDocument<Document>() {
			@Override
			public Document get() {
				return document;
			}
		};
	}

}
