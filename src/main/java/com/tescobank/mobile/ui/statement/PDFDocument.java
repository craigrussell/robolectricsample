package com.tescobank.mobile.ui.statement;

public interface PDFDocument<T> {

	T get();

}
