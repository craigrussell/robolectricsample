package com.tescobank.mobile.ui.statement;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.tescobank.mobile.model.product.ProductType.typeForLabel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.ShareActionProvider;
import com.actionbarsherlock.widget.ShareActionProvider.OnShareTargetSelectedListener;
import com.radaee.pdf.Document;
import com.radaee.reader.PDFReader;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.api.product.RetrieveStatementRequest;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.model.product.ProductType;
import com.tescobank.mobile.rest.request.RestByteArrayResponse;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoAlertDialog;
import com.tescobank.mobile.ui.paycreditcard.PayCreditCardActivity;

/**
 * Activity for viewing a statement
 */
public class ViewStatementActivity extends TescoActivity {

	private static final String TAG = ViewStatementActivity.class.getSimpleName();
	private static final String SHARE_FILE = "share_history_statments.xml";

	public static final String STATEMENT_PRODUCT_EXTRA = "statementProduct";
	public static final String STATEMENT_PRODUCT_CYCLEDATE_POSITION_EXTRA = "selectedCycleDatePosition";
	public static final String STATEMENT_DATE_EXTRA = "statementDate";
	public static final String BACK_TO_CAROUSEL = "back_to_carousel";
	public static final String STATEMENT_TRANSACTION_BEAN = "statement_transaction_bean";

	private DataFormatter dataFormatter;
	private ProductDetails product;
	private String cycleDateString;
	private Date cycleDate;
	private InProgressIndicator inProgressIndicator;
	private PDFInitialiser<Document> pdfInitialiser;
	private PDFReader pdfReader;
	private File potentialPdfLocation;
	private boolean isPayCCBillWithDebitCardEnabled;
	
	private int position;
	private byte[] pdfBytes;

	private boolean issueWritingToExternalStorage;
	
	public ViewStatementActivity() {
		super();

		pdfInitialiser = new DefaultPDFInitialiser();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating View Statement Activity");
		}
		super.onCreate(savedInstanceState);

		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		setContentView(R.layout.activity_view_statement);
		isPayCCBillWithDebitCardEnabled = getApplicationState().getFeatures().isPayCCBillWithDebitCardEnabled();
		createPotentialPdfLocation();

		dataFormatter = new DataFormatter();
		product = (ProductDetails) getIntent().getExtras().getSerializable(STATEMENT_PRODUCT_EXTRA);
		position = (Integer) getIntent().getExtras().getSerializable(STATEMENT_PRODUCT_CYCLEDATE_POSITION_EXTRA);
		cycleDateString = (String) getIntent().getExtras().getSerializable(STATEMENT_DATE_EXTRA);
		cycleDate = dataFormatter.parseDate(cycleDateString);
		inProgressIndicator = new InProgressIndicator(this, false);
		pdfInitialiser.init(this);
		pdfReader = PDFReader.class.cast(findViewById(R.id.pdfreader));

		configureHeader();
		configurePdfReader();
		retrieveStatement();
	}

	private void createPotentialPdfLocation() {
		try {
			createPdfFile();
			verifyPdfFile();
		} catch (IOException e) {
			issueWritingToExternalStorage = true;		
		} catch (Exception e) {
			throw new PdfException(e);
		}
	}
	
	private void createPdfFile() {
		File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "TescoBank");
		dir.mkdirs();

		potentialPdfLocation = new File(dir, "statement.pdf");
		
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Potential PDF location: " + potentialPdfLocation);
		}
	}
	
	private void verifyPdfFile() throws IOException {
		if (!potentialPdfLocation.exists() && !potentialPdfLocation.createNewFile()) {
			// can't be shared because we can't write to the file
			potentialPdfLocation = null;
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Failed to create new file at " + potentialPdfLocation);
			}
		}
	}
	
	@Override
	protected void onDestroy() {
		pdfReader.PDFClose();
		super.onDestroy();
	}

	/**
	 * @param pdfInitialiser
	 *            the pdfInitialiser
	 */
	public void setPDFInitialiser(PDFInitialiser<Document> pdfInitialiser) {
		this.pdfInitialiser = pdfInitialiser;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.statements, menu);

		configurePayCCOptionsMenuItem(menu);
		configureShareOptionsMenuItem(menu);

		return true;
	}

	protected void configurePayCCOptionsMenuItem(Menu menu) {
		MenuItem payCCItem = menu.findItem(R.id.menu_item_pay_cc_from_statement);
		ProductType productType = typeForLabel(product.getProductType());
		boolean isCreditCard = ProductType.CC_PRODUCT.equals(productType);
		boolean isRestricted = "L1".equals(product.getAccountStatusCode());
		payCCItem.setVisible(isCreditCard && position==0 && isPayCCBillWithDebitCardEnabled && !isRestricted);
	}

	protected void configureShareOptionsMenuItem(Menu menu) {
		MenuItem actionItem = menu.findItem(R.id.menu_item_share);
		boolean canHandle = canHandlePDF() && !issueWritingToExternalStorage;
		actionItem.setVisible(canHandle);
		actionItem.setEnabled(canHandle);
		if (canHandle) {
			prepareShare(actionItem);
		}
	}

	private boolean canHandlePDF() {
		if (null == potentialPdfLocation) {
			return false;
		}

		List<ResolveInfo> activities = getPackageManager().queryIntentActivities(createShareIntent(), 0);
		return !activities.isEmpty();
	}

	private void prepareShare(MenuItem actionItem) {
		ShareActionProvider actionProvider = (ShareActionProvider) actionItem.getActionProvider();
		actionProvider.setShareHistoryFileName(SHARE_FILE);
		actionProvider.setShareIntent(createShareIntent());
		actionProvider.setOnShareTargetSelectedListener(new OnShareTargetSelectedListener() {
			@Override
			public boolean onShareTargetSelected(ShareActionProvider source, Intent intent) {
				confirmShare(intent);
				return true;
			}
		});
	}

	private void confirmShare(final Intent intent) {

		new TescoAlertDialog.Builder(this).setTitle(R.string.view_statement_confirm_share_title).setMessage(R.string.view_statement_confirm_share_body).setPositiveButton(R.string.view_statement_confirm_share_confirm, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				performShare(intent);
			}
		}).setNegativeButton(R.string.view_statement_confirm_share_cancel, null).show();

	}

	private void performShare(Intent intent) {

		// assume writing this to disk is going to be quick otherwise we'll have
		// to show a progress
		OutputStream out = null;
		try {
			out = new FileOutputStream(potentialPdfLocation);
			out.write(pdfBytes);

		} catch (IOException e) {
			handleErrorResponse(getErrorBuilder().buildErrorResponse(InAppError.UnableToWriteFile));
			return;
		} finally {
			if (null != out) {
				try {
					out.close();
				} catch (IOException e) {
					// do what?
				}
			}
		}

		// No request/result makes sense when exiting the app.
		getApplicationState().getAnalyticsTracker().trackViewStatement(product, 0, true);
		startActivity(intent);
	}

	private Intent createShareIntent() {
		Intent shareIntent = new Intent();
		shareIntent.setAction(Intent.ACTION_SEND);
		shareIntent.setType("application/pdf");
		shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(potentialPdfLocation));
		return shareIntent;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_item_pay_cc_from_statement:
			startPayCreditCardActivity();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onRetryPressed() {
		retrieveStatement();
	}

	@Override
	public boolean backFromRetryErrorShouldRetry() {
		return false;
	}

	private void configureHeader() {

		String formattedDate = dataFormatter.formatDate(cycleDateString);
		String titleText = getResources().getString(R.string.view_statement_title, formattedDate);

		TextView textView = (TextView) findViewById(R.id.statement_header_title);
		textView.setText(titleText);
	}

	private void configurePdfReader() {
		pdfReader.setVisibility(GONE);
	}

	private void retrieveStatement() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Retrieving Statement");
		}

		inProgressIndicator.showInProgressIndicator();


		RetrieveStatementRequest request = new StatementRetrieveStatementRequest(ProductType.CC_PRODUCT.equals(typeForLabel(product.getProductType())));

		request.setTbId(getApplicationState().getTbId());
		request.setProductCategory(product.getProductCategory());
		request.setProductId(product.getProductId());
		request.setCif(product.getCustomerId());
		request.setDate(cycleDate);

		getApplicationState().getRestRequestProcessor().processRequest(request);
	}

	private void renderStatement() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Rendering Statement of size " + pdfBytes.length);
		}

		Document document = pdfInitialiser.createDocument().get();
		document.OpenMem(pdfBytes, null);
		pdfReader.PDFOpen(document, false, null);
		pdfReader.setVisibility(VISIBLE);
	}

	private void startPayCreditCardActivity() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Starting Pay Credit Card Activity");
		}

		Intent intent = new Intent(this, PayCreditCardActivity.class);
		intent.putExtra(PayCreditCardActivity.PAY_CREDITCARD_PRODUCT_EXTRA, product);
		startActivity(intent);
	}

	private static class PdfException extends RuntimeException {

		private static final long serialVersionUID = -7481874418337371121L;

		public PdfException(Throwable throwable) {
			super(throwable);
		}
	}

	private class StatementRetrieveStatementRequest extends RetrieveStatementRequest {

		public StatementRetrieveStatementRequest(boolean isCreditCard) {
			super(isCreditCard);
		}

		@Override
		public void onResponse(RestByteArrayResponse response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Retrieve Statement Request Succeeded");
			}
			inProgressIndicator.hideInProgressIndicator();
			inProgressIndicator.removeInProgressIndicator();
			pdfBytes = response.getBytes();
			renderStatement();
			getApplicationState().getAnalyticsTracker().trackViewStatement(product, 0, false);
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponse) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Retrieve Statement Request Failed: " + errorResponse.getErrorResponse().getErrorCode());
			}
			inProgressIndicator.hideInProgressIndicator();
			handleErrorResponse(errorResponse);
		}		
	}

}
