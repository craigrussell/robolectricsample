package com.tescobank.mobile.ui.statement;

import static android.content.Intent.FLAG_ACTIVITY_REORDER_TO_FRONT;
import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_CAROUSEL_LOADED;
import static com.tescobank.mobile.ui.transaction.TransactionsActivity.TRANSACTIONS_PRODUCT_EXTRA;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.carousel.CarouselActivity;
import com.tescobank.mobile.ui.prodheader.ProductHeaderViewFactory;
import com.tescobank.mobile.ui.transaction.TransactionsActivity;
import com.tescobank.mobile.ui.tutorial.TooltipId;
import com.tescobank.mobile.ui.tutorial.groups.StatementArchiveTooltipGroup;

public class ViewStatementListActivity extends TescoActivity {

	private static final String TAG = ViewStatementListActivity.class.getSimpleName();
	
	private static final String STATEMENT_PRODUCT_CYCLEDATE_POSITION_EXTRA = "selectedCycleDatePosition";
	private static final String STATEMENT_DATE_EXTRA = "statementDate";
	public static final String STATEMENT_PRODUCT_EXTRA = "statementProduct";
	public static final String LAUNCHED_FROM_STATEMENT_ARCHIVE_LIST ="launched_from_statement_archive_list";
	
	private ProductDetails product;
	private ListView listForPreviousCycleDates;
	private ListView listForCurrentCycleDate;
	private ViewStatementListAdapter viewStatementListAdapter;
	private String activityLaunchedFrom;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating View Statement List Activity");
		}
		super.onCreate(savedInstanceState);

		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		setContentView(R.layout.activity_view_statement_list);
		
		product = (ProductDetails) getIntent().getExtras().getSerializable(STATEMENT_PRODUCT_EXTRA);
		listForPreviousCycleDates = (ListView) findViewById(R.id.view_previous_statement_list);
		listForCurrentCycleDate = (ListView) findViewById(R.id.view_current_statement_list);
		activityLaunchedFrom = (String) getIntent().getExtras().getSerializable(LAUNCHED_FROM_STATEMENT_ARCHIVE_LIST);
		
		configureHeader();
		configureStatementView();
		getTooltipManager().setGroup(new StatementArchiveTooltipGroup());
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		getTooltipManager().show();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		getTooltipManager().hide();
		getTooltipManager().show();		
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
	    super.onNewIntent(intent);
	    activityLaunchedFrom = (String) intent.getExtras().getSerializable(LAUNCHED_FROM_STATEMENT_ARCHIVE_LIST);
	}
	
	@Override
	protected void onStart() {
		trackAnalytic();
		super.onStart();
	}
	
	private void trackAnalytic() {
		getApplicationState().getAnalyticsTracker().trackViewSatementArchive(product,activityLaunchedFrom);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onOptionsItemSelected: " + item);
		}
		
		if (android.R.id.home == item.getItemId()) {
			Intent carousel = new Intent(this, CarouselActivity.class);
			carousel.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(carousel);
			finishWithResult(RESULT_CAROUSEL_LOADED);
		}

		if (R.id.menu_item_transactions == item.getItemId()) {
			getTooltipManager().dismiss(TooltipId.StatementTransactionToggle.ordinal());
			startTransactionListActivity();
		}
		
		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.view_statements, menu);
		return true;
	}

	private void configureHeader() {
		ViewGroup headerContainer = (ViewGroup) findViewById(R.id.transfer_header);
		ProductHeaderViewFactory productHeaderViewFactory = new ProductHeaderViewFactory(product, this, headerContainer);
		productHeaderViewFactory.createHeaderView();
	}

	private void configureStatementView() {

		if (product.getCycleDates() != null && !product.getCycleDates().isEmpty()) {
			populateCurrentStatement();
			if (product.getCycleDates().size() > 1) {
				findViewById(R.id.view_statement_list_past_text).setVisibility(View.VISIBLE);
				populateListOfPastStatements();
			}
		}
	}

	private void populateCurrentStatement() {
		final List<String> currentStatementCycleDateOnly = new ArrayList<String>();
		currentStatementCycleDateOnly.add(product.getCycleDates().get(0));
		viewStatementListAdapter = new ViewStatementListAdapter(this, currentStatementCycleDateOnly);
		listForCurrentCycleDate.setAdapter(viewStatementListAdapter);
		listForCurrentCycleDate.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				startViewStatementActivityForSelectedStatement(currentStatementCycleDateOnly.get(0), position);
			}
		});
	}

	private void populateListOfPastStatements() {

		final List<String> cycleDatesLessFirstElement = new ArrayList<String>();
		cycleDatesLessFirstElement.addAll(product.getCycleDates());
		cycleDatesLessFirstElement.remove(0);
		viewStatementListAdapter = new ViewStatementListAdapter(this, cycleDatesLessFirstElement);
		listForPreviousCycleDates.setAdapter(viewStatementListAdapter);
		listForPreviousCycleDates.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				startViewStatementActivityForSelectedStatement(cycleDatesLessFirstElement.get(position), position+1);
			}
		});
	}

	private void startViewStatementActivityForSelectedStatement(String cycleDate,int position) {		
 		Intent intent = new Intent(this, ViewStatementActivity.class);
		intent.putExtra(STATEMENT_PRODUCT_EXTRA, product);
		intent.putExtra(STATEMENT_DATE_EXTRA, cycleDate);
		intent.putExtra(STATEMENT_PRODUCT_CYCLEDATE_POSITION_EXTRA, position);
		startActivity(intent);
	}

	private void startTransactionListActivity() {
		Intent intent = new Intent(this, TransactionsActivity.class);
		intent.setFlags(FLAG_ACTIVITY_REORDER_TO_FRONT);
		intent.putExtra(TRANSACTIONS_PRODUCT_EXTRA, product);
		startActivity(intent);
	}
}
