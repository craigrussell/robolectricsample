package com.tescobank.mobile.ui.statement;

import android.app.Activity;

public interface PDFInitialiser<T> {

	void init(Activity activity);

	PDFDocument<T> createDocument();

}
