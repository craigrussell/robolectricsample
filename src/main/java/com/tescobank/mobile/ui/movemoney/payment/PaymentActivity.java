package com.tescobank.mobile.ui.movemoney.payment;

import static android.view.View.VISIBLE;

import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.payment.GetTransacteesForUserResponse.Transactee;
import com.tescobank.mobile.model.movemoney.Payment;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.InProgressIndicator.ContinueButtonConfiguration;
import com.tescobank.mobile.ui.movemoney.MoneyMovementActivity;
import com.tescobank.mobile.ui.movemoney.MoneyMovementResultOverlayRenderer;
import com.tescobank.mobile.ui.movemoney.ValueSelector;
import com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectionListener;

/**
 * Activity for making a payment
 */
public class PaymentActivity extends MoneyMovementActivity<Payment, Transactee> implements ContinueButtonConfiguration {

	private static final String BUNDLE_KEY_REFERENCE_TEXT = "BUNDLE_KEY_REFERENCE_TEXT";
	private static final String BUNDLE_KEY_REFERENCE_EDITING = "BUNDLE_KEY_REFERENCE_EDITING";

	private TransacteeSelector selector;
	
	private boolean hasSavedData;
	private String savedReferenceText;
	private boolean savedReferenceIsEditing;

	private List<Transactee> transactees;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		selector = (TransacteeSelector) getDestinationSelector();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (selector.isValid()) {
			outState.putBoolean(BUNDLE_KEY_REFERENCE_EDITING, selector.isReferenceEditing());
			outState.putString(BUNDLE_KEY_REFERENCE_TEXT, selector.getReferenceText());
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void restoreSavedData(Bundle savedInstanceState) {
		if (savedInstanceState == null) {
			return;
		}
		
		if(savedInstanceState.containsKey(BUNDLE_KEY_REFERENCE_TEXT)) {
			hasSavedData = true;
			savedReferenceText = savedInstanceState.getString(BUNDLE_KEY_REFERENCE_TEXT);
			savedReferenceIsEditing = savedInstanceState.getBoolean(BUNDLE_KEY_REFERENCE_EDITING);
		}
		
		super.restoreSavedData(savedInstanceState);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus) {
			updateViewsWithSavedData();
		}
	}

	private void updateViewsWithSavedData() {
		if (hasSavedData) {
			selector.restoreReference(savedReferenceText, savedReferenceIsEditing);
		}
		resetSavedData();
	}
	
	private void resetSavedData() {
		hasSavedData = false;
		savedReferenceText = null;
		savedReferenceIsEditing = false;
	}

	@Override
	protected void populateAvailableDestinations() {
		transactees = getApplicationState().getTransactees();
	}

	@Override
	protected MoneyMovementResultOverlayRenderer<Payment> createResultOverlayRenderer() {
		return new PaymentResultOverlayRenderer(this);
	}

	@Override
	protected Payment createMoneyMovement(ProductDetails source, Transactee destination) {
		return new Payment(source, destination);
	}

	@Override
	protected void populateMoneyMovement() {
		getMoneyMovement().setReference(getDestinationSelector().getSelectedValue().getSelectedTransacteeReference());
	}

	@Override
	protected void configureNoDestinationsMessage() {
		if (noTransacteesAvailable()) {
			View container = findViewById(R.id.movemoney_no_destinations_message);
			container.setVisibility(VISIBLE);
			TextView textView = (TextView) findViewById(R.id.informationText);
			textView.setText(R.string.payment_no_transactees_message);
		}
	}

	private boolean noTransacteesAvailable() {
		return (transactees == null) || transactees.isEmpty();
	}

	@Override
	protected ValueSelector<Transactee> createDestinationSelector(ValueSelectionListener<Transactee> valueSelectionListener) {
		return new TransacteeSelector(transactees, this, valueSelectionListener);
	}

	@Override
	protected void configureMoneyMover() {
		setMoneyMover(new PaymentMaker(getSourceProduct(), this, getInProgressIndicator(), this, this));
	}

	@Override
	protected void trackAnalytic(Payment moneyMovement) {
		getApplicationState().getAnalyticsTracker().trackMakePayment(moneyMovement);
	}

	@Override
	protected void trackProcessCompleted(Payment moneyMovement) {
		getApplicationState().getAnalyticsTracker().trackMakePaymentCompletedSuccessfully(moneyMovement);
	}

	@Override
	public int getMessageId() {
		return getDateSelector().hasFutureDateBeenSelected() ? R.string.progress_schedulePayment : R.string.progress_makePayment;
	}

	@Override
	public int getBackgroundDrawable() {
		return R.drawable.button_payments_background_selector;
	}

	@Override
	public int getTextColors() {
		return R.color.button_payments_text_selector;
	}

	@Override
	protected void refreshInProgressIndicator() {
		super.refreshInProgressIndicator();
		getInProgressIndicator().updateContinueButtonConfiguration(this);
	}
}
