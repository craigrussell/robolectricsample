package com.tescobank.mobile.ui.movemoney;

import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.model.movemoney.MoneyMovement;

/**
 * Money movement listener
 * 
 * @param T the money movement type
 */
public interface MoneyMovementListener<T extends MoneyMovement<?, ?>> {

	/**
	 * Called when a money movement was successful
	 * 
	 * @param moneyMovement the money movement
	 */
	void moneyMovementSuccessful(T moneyMovement);

	/**
	 * Called when a money movement was declined
	 * 
	 * @param moneyMovement the money movement
	 * @param errorResponseWrapper the error indicating that the money movement was declined
	 */
	void moneyMovementDeclined(T moneyMovement, ErrorResponseWrapper errorResponseWrapper);
}
