package com.tescobank.mobile.ui.movemoney;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectorState.NotStarted;
import static java.util.Calendar.DATE;
import static java.util.Calendar.DAY_OF_YEAR;
import static java.util.Calendar.YEAR;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.ui.TescoActivity;

public class DateSelector extends ValueSelector<Date> implements OnClickListener, OnDateSetListener {

	private static final String TAG = DateSelector.class.getSimpleName();

	private static final int SECONDS_59 = 59;
	private static final int MINUTES_59 = 59;
	private static final int HOURS_23 = 23;
	private static final int MONTHS_24 = 24;

	private final DataFormatter formatter = new DataFormatter();
	private View selectorLayout;
	private TextView header;
	private DatePicker datePicker;
	private boolean dateSelected;
	private ButtonClickListener buttonClickListener;
	private boolean isImmediatePaymentsEnabled;
	private boolean isFuturePaymentsEnabled;

	private View dateEditView;

	private boolean datePickerShowing;

	public DateSelector(TescoActivity activity, com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectionListener<Date> valueSelectionListener, ButtonClickListener buttonClickListener) {
		super(activity, valueSelectionListener);

		this.buttonClickListener = buttonClickListener;
		this.isImmediatePaymentsEnabled = activity.getApplicationState().getFeatures().isImmediatePaymentsEnabled();
		this.isFuturePaymentsEnabled = activity.getApplicationState().getFeatures().isFuturePaymentsEnabled();

		setSelectedValue();
		configureDateEditView();
		configureNowButton();
		configureChooseButton();
	}

	@Override
	public boolean isValid() {
		return getSelectedValue() != null;
	}

	@Override
	protected void setInitialState() {
		selectorLayout = getActivity().findViewById(R.id.movemoney_date_selector_layout);
		header = TextView.class.cast(getActivity().findViewById(R.id.movemoney_date_selector_title));
		setState(NotStarted);
	}

	private void setSelectedValue() {
		if (!isFuturePaymentsEnabled) {
			setSelectedValue(new Date(getStartOfToday()), false);
		}
	}

	private void configureNowButton() {
		Button nowButton = Button.class.cast(getActivity().findViewById(R.id.movemoney_date_selector_now_button));
		if (isImmediatePaymentsEnabled) {
			nowButton.setOnClickListener(this);
		} else {
			nowButton.setVisibility(GONE);
		}
	}

	private void configureChooseButton() {
		Button standardChooseButton = Button.class.cast(getActivity().findViewById(R.id.movemoney_date_selector_choose_button));
		if (isImmediatePaymentsEnabled) {
			standardChooseButton.setOnClickListener(this);
		} else {
			Button soleChooseButton = Button.class.cast(getActivity().findViewById(R.id.movemoney_date_selector_choose_button_only));
			standardChooseButton.setVisibility(GONE);
			soleChooseButton.setVisibility(VISIBLE);
			soleChooseButton.setOnClickListener(this);
		}
	}
	
	private void configureDateEditView() {
		dateEditView = getActivity().findViewById(R.id.transactee_date_edit);
		if(isFuturePaymentsEnabled) {
			dateEditView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					chooseDate();	
				}
			});
		}	
	}

	@Override
	public void setState(com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectorState state) {
		super.setState(state);

		switch (state) {

		case NotStarted:
			header.setText(R.string.movemoney_date_selector_prompt);
			selectorLayout.setVisibility(View.GONE);
			break;

		case Active:
			header.setText(R.string.movemoney_date_selector_prompt);
			selectorLayout.setVisibility(View.VISIBLE);
			dateEditView.setVisibility(View.GONE);
			updateSelectedDate(false);
			updateFutureDateMessage(false);
			showButtons(true);
			break;

		case Complete:
			header.setText(R.string.movemoney_date_selector_info);
			selectorLayout.setVisibility(View.VISIBLE);
			showOrHideDateEditView();
			updateSelectedDate(true);
			updateFutureDateMessage(true);
			showButtons(false);
			break;
		}
	}

	private void showOrHideDateEditView() {
		if(!isFuturePaymentsEnabled) {
			dateEditView.setVisibility(View.GONE);
		} else {
			dateEditView.setVisibility(View.VISIBLE);
		}
		
	}

	private void showButtons(boolean show) {
		getActivity().findViewById(R.id.movemoney_date_selector_buttons).setVisibility(show ? View.VISIBLE : View.GONE);
	}

	private void updateSelectedDate(boolean show) {
		Date selectedDate = getSelectedValue();
		TextView selectedDateView = TextView.class.cast(getActivity().findViewById(R.id.movemoney_date_selector_selected_value));

		if (selectedDate == null || !show) {
			selectedDateView.setVisibility(View.GONE);
		} else {
			selectedDateView.setVisibility(View.VISIBLE);
			selectedDateView.setText(formatter.formatDateAsDayFullMonthAndYear(selectedDate));
		}
	}
	
	public void openPickerWithDate(Calendar date) {
		chooseDate();
		datePicker.updateDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DATE));
	}
	
	public Calendar getPickerDate() {
		Calendar pickerDate = null;
		if(datePicker != null) {
			pickerDate = new GregorianCalendar(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
		}
		return pickerDate;
	}

	private void updateFutureDateMessage(boolean showIfApplicable) {

		boolean show = false;
		if (showIfApplicable) {

			Date selectedDate = getSelectedValue();
			show = isFutureDate(selectedDate);
		}

		View futureDateImage = getActivity().findViewById(R.id.movemoney_date_selector_future_date_image);
		View futureDateMessage = getActivity().findViewById(R.id.movemoney_date_selector_future_date_message);

		futureDateImage.setVisibility(show ? VISIBLE : GONE);
		futureDateMessage.setVisibility(show ? VISIBLE : GONE);
	}

	private boolean isFutureDate(Date date) {

		if (date == null) {
			return false;
		}

		Calendar specifiedDateCelendar = Calendar.getInstance();
		specifiedDateCelendar.setTime(date);

		Calendar currentDateCalendar = Calendar.getInstance();

		int specifiedYear = specifiedDateCelendar.get(YEAR);
		int currentYear = currentDateCalendar.get(YEAR);

		if (specifiedYear == currentYear) {

			int specifiedDayOfYear = specifiedDateCelendar.get(DAY_OF_YEAR);
			int currentDayOfYear = currentDateCalendar.get(DAY_OF_YEAR);

			return specifiedDayOfYear > currentDayOfYear;

		} else {
			return specifiedYear > currentYear;
		}
	}

	public boolean hasFutureDateBeenSelected() {
		return isFutureDate(getSelectedValue());
	}

	@Override
	public void setSelectedValue(Date selectedValue) {
		super.setSelectedValue(selectedValue);
		updateSelectedDate(true);
		datePicker = null;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.movemoney_date_selector_now_button:
			chooseNow();
			break;

		default:
			chooseDate();
			break;
		}

		buttonClickListener.buttonClicked();
	}

	private void chooseNow() {
		setSelectedValue(new Date());
	}

	private void chooseDate() {
		Calendar cal = Calendar.getInstance();
		if (getSelectedValue() != null) {
			cal.setTime(getSelectedValue());
		} else if (!isImmediatePaymentsEnabled) {
			cal.add(Calendar.DATE, 1);
		}

		datePicker = new DatePicker(getActivity());
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			configureForJellybeanMR1OrBetter(cal, datePicker);
		} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			configureForHoneycombOrBetter(cal, datePicker);
		}
		
		setDatePickerShowing(true);

		// Don't use TescoAlertDialog as analytics are not required.
		AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).setView(datePicker).setPositiveButton(R.string.movemoney_date_selector_ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				setSelectedValue(new GregorianCalendar(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth()).getTime());
				setDatePickerShowing(false);
			}
		}).setNegativeButton(R.string.movemoney_date_selector_cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				setDatePickerShowing(false);
				
			}
		}).setOnCancelListener(new OnCancelListener() {			
			@Override
			public void onCancel(DialogInterface dialog) {
				setDatePickerShowing(false);			
			}
		}).create();
		
		alertDialog.setCanceledOnTouchOutside(true);
		alertDialog.show();
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	private void configureForJellybeanMR1OrBetter(Calendar selected, DatePicker datePicker) {
		datePicker.setSpinnersShown(false);
		datePicker.setCalendarViewShown(true);
		datePicker.getCalendarView().setShowWeekNumber(false);

		datePicker.setMinDate(getMinDate());

		// Sets the selected date.
		// Workaround for CalendarView bug relating to setMinDate():
		// https://code.google.com/p/android/issues/detail?id=42750
		// Set then reset the date on the calendar so that it properly
		// shows today's date. The choice of 24 months is arbitrary.
		selected.add(Calendar.MONTH, MONTHS_24);
		datePicker.getCalendarView().setDate(selected.getTimeInMillis(), false, true);
		selected.add(Calendar.MONTH, -MONTHS_24);
		datePicker.getCalendarView().setDate(selected.getTimeInMillis(), false, true);

		datePicker.setMaxDate(getEndOfOneYearFromToday());
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void configureForHoneycombOrBetter(Calendar selected, DatePicker datePicker) {
		datePicker.setCalendarViewShown(false);
		datePicker.updateDate(selected.get(Calendar.YEAR), selected.get(Calendar.MONTH) - 1, selected.get(Calendar.DAY_OF_MONTH));

		datePicker.setMinDate(getMinDate());
		datePicker.setMaxDate(getEndOfOneYearFromToday());
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onDateSet: " + dateSelected + " - " + year + " / " + monthOfYear + " / " + dayOfMonth);
		}
		if (dateSelected) {
			setSelectedValue(new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime());
		}
	}

	private long getMinDate() {
		return isImmediatePaymentsEnabled ? getStartOfToday() : getStartOfTomorrow();
	}

	private long getStartOfToday() {

		Calendar currentDate = Calendar.getInstance();

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(0);
		calendar.set(Calendar.YEAR, currentDate.get(Calendar.YEAR));
		calendar.set(Calendar.DAY_OF_YEAR, currentDate.get(Calendar.DAY_OF_YEAR));
		calendar.set(Calendar.HOUR_OF_DAY, 0);

		return calendar.getTimeInMillis();
	}

	private long getStartOfTomorrow() {

		Calendar calendar = Calendar.getInstance();

		calendar.setTimeInMillis(getStartOfToday());
		calendar.add(DATE, 1);

		return calendar.getTimeInMillis();
	}

	private long getEndOfOneYearFromToday() {

		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.YEAR, 1);

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(0);
		calendar.set(Calendar.YEAR, currentDate.get(Calendar.YEAR));
		calendar.set(Calendar.DAY_OF_YEAR, currentDate.get(Calendar.DAY_OF_YEAR));
		calendar.set(Calendar.HOUR_OF_DAY, HOURS_23);
		calendar.set(Calendar.MINUTE, MINUTES_59);
		calendar.set(Calendar.SECOND, SECONDS_59);

		return calendar.getTimeInMillis();
	}

	public boolean isDatePickerShowing() {
		return datePickerShowing;
	}

	public void setDatePickerShowing(boolean datePickerShowing) {
		this.datePickerShowing = datePickerShowing;
	}
}
