package com.tescobank.mobile.ui.movemoney;

import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_MOVE_MONEY_SUCCESS;
import static com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectorState.Active;
import static com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectorState.Complete;
import static com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectorState.NotStarted;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.PopupWindow.OnDismissListener;

import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.model.movemoney.MoneyMovement;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.errors.ErrorHandler;
import com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectionListener;
import com.tescobank.mobile.ui.prodheader.ProductHeaderViewFactory;

/**
 * Abstract base class for money movement activities
 * 
 * @param T the money movement type
 * @param D the destination object type
 */
public abstract class MoneyMovementActivity<T extends MoneyMovement<ProductDetails, D>, D extends Serializable> extends TescoActivity implements ErrorHandler, MoneyMovementListener<T> {

	public static final String SOURCE_PRODUCT_EXTRA = "sourceProduct";

	private static final String BUNDLE_KEY_SELECTED_DESTINATION = "BUNDLE_KEY_SELECTED_DESTINATION";
	private static final String BUNDLE_KEY_SELECTED_AMOUNT = "BUNDLE_KEY_SELECTED_AMOUNT";
	private static final String BUNDLE_KEY_SELECTED_DATE = "BUNDLE_KEY_SELECTED_DATE";
	private static final String BUNDLE_KEY_PICKER_DATE = "BUNDLE_KEY_PICKER_DATE";

	private ProductDetails sourceProduct;

	private ValueSelector<D> destinationSelector;

	private AmountSelector amountSelector;

	private DateSelector dateSelector;

	private MoneyMover<T> moneyMover;

	private T moneyMovement;

	private InProgressIndicator inProgressIndicator;

	private MoneyMovementResultOverlayRenderer<T> resultOverlayRenderer;

	private boolean isFuturePaymentsEnabled;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		setContentView(R.layout.activity_movemoney);
		findViewById(R.id.button_progress_layout).setVisibility(View.GONE);

		sourceProduct = (ProductDetails) getIntent().getExtras().getSerializable(SOURCE_PRODUCT_EXTRA);
		resultOverlayRenderer = createResultOverlayRenderer();
		isFuturePaymentsEnabled = getApplicationState().getFeatures().isFuturePaymentsEnabled();

		populateAvailableDestinations();
		configureHeader();
		configureNoDestinationsMessage();
		configureDestinationSelector();
		configureAmountSelector();
		configureDateSelector();
		configureInProgressIndicator();
		configureMoneyMover();
		restoreSavedData(savedInstanceState);
		enableLayoutAnimationsOnPostJellyBeanDevices();
	}
	
	private void enableLayoutAnimationsOnPostJellyBeanDevices() {
		ViewGroup viewGroup = ViewGroup.class.cast(findViewById(R.id.movemoney_root));
		enableLayoutAnimationsOnPostJellyBeanDevices(viewGroup);
	}

	@SuppressWarnings("unchecked")
	protected void restoreSavedData(Bundle savedInstanceState) {
		if (savedInstanceState == null) {
			return;
		}

		Serializable destination = savedInstanceState.getSerializable(BUNDLE_KEY_SELECTED_DESTINATION);
		if (destination != null) {
			destinationSelector.setSelectedValue((D) destination, true);
		}

		Serializable amount = savedInstanceState.getSerializable(BUNDLE_KEY_SELECTED_AMOUNT);
		if (amount != null) {
			amountSelector.setSelectedValue(BigDecimal.class.cast(amount), true);
		}

		Serializable date = savedInstanceState.getSerializable(BUNDLE_KEY_SELECTED_DATE);
		if (date != null) {
			dateSelector.setSelectedValue(Date.class.cast(date), true);
		}
		
		Serializable pickerDate = savedInstanceState.getSerializable(BUNDLE_KEY_PICKER_DATE);
		if (pickerDate != null) {
			dateSelector.openPickerWithDate(Calendar.class.cast(pickerDate));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		saveSelectedValue(outState, BUNDLE_KEY_SELECTED_DESTINATION, destinationSelector);
		saveSelectedValue(outState, BUNDLE_KEY_SELECTED_AMOUNT, amountSelector);
		saveSelectedValue(outState, BUNDLE_KEY_SELECTED_DATE, dateSelector);
		if(dateSelector.getPickerDate() != null && dateSelector.isDatePickerShowing()) {
			outState.putSerializable(BUNDLE_KEY_PICKER_DATE, dateSelector.getPickerDate());
		}
	}

	private void saveSelectedValue(Bundle bundle, String key, ValueSelector<?> selector) {
		if (selector.getState() == Complete) {
			bundle.putSerializable(key, (Serializable) selector.getSelectedValue());
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus) {
			enableLayoutAnimationsOnPostJellyBeanDevices();
		}
		if (hasFocus && amountSelector.getState() == Complete) {
			amountSelector.setSelectedValue(amountSelector.getSelectedValue(), true);
		}
	}

	protected abstract MoneyMovementResultOverlayRenderer<T> createResultOverlayRenderer();

	protected abstract void trackAnalytic(T moneyMovement);

	protected abstract void trackProcessCompleted(T moneyMovement);

	protected void populateAvailableDestinations() {
		// Do nothing
	}

	@Override
	protected void onPause() {
		super.onPause();
		cancelTasks();
	}

	@Override
	public void unlockScreenOrientation() {
		//do nothing, once the screen is locked we never want to unlock it
	};

	private void cancelTasks() {
		resultOverlayRenderer.cancelTasks();
	}

	@Override
	public void onBackPressed() {

		if (moneyMover.isMoneyMovementInProgress()) {
			return;
		}

		if (resultOverlayRenderer.dismissOverlay()) {
			return;
		}

		if (destinationSelector.handleBack()) {
			return;
		}

		handleBack();
		refreshInProgressIndicatorOnBack();
	}

	private void handleBack() {
		if ((destinationSelector.getState() == NotStarted) || (destinationSelector.getState() == Active)) {
			super.onBackPressed();
		} else if (amountSelector.getState() == Active) {
			destinationSelector.setState(Active);
			amountSelector.setState(NotStarted);
			hideRightColumn();
		} else if (dateSelector.getState() == Active) {
			amountSelector.setState(Active);
			dateSelector.setState(NotStarted);
		} else if (dateSelector.getState() == Complete) {
			if (isFuturePaymentsEnabled) {
				dateSelector.setState(Active);
			} else {
				amountSelector.setState(Active);
				dateSelector.setState(NotStarted);
			}
		}
	}

	private void refreshInProgressIndicatorOnBack() {
		if (!isFinishing()) {
			refreshInProgressIndicator();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (moneyMover.isMoneyMovementInProgress()) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onContinuePressed(View view) throws InstantiationException, IllegalAccessException {

		destinationSelector.buttonClicked();

		moneyMovement = createMoneyMovement(sourceProduct, destinationSelector.getSelectedValue());
		moneyMovement.setAmount(amountSelector.getSelectedValue());
		moneyMovement.setDate(dateSelector.getSelectedValue());

		populateMoneyMovement();

		trackAnalytic(moneyMovement);
		moneyMover.moveMoney(moneyMovement);
	}

	protected abstract T createMoneyMovement(ProductDetails source, D destination);

	protected void populateMoneyMovement() {
		// Do nothing
	}

	@Override
	public void handleError(ErrorResponseWrapper errorResponseWrapper) {
		handleErrorResponse(errorResponseWrapper);
	}

	@Override
	public void moneyMovementSuccessful(T moneyMovement) {
		trackProcessCompleted(moneyMovement);
		resultOverlayRenderer.renderMoneyMovementSuccessOverlay(moneyMovement, createSuccessOverlayDismissListener());
		inProgressIndicator.hideAll();
	}

	private OnDismissListener createSuccessOverlayDismissListener() {
		return new OnDismissListener() {

			@Override
			public void onDismiss() {
				List<ProductDetails> products = new ArrayList<ProductDetails>();
				products.add(moneyMovement.getSource());
				addDestination(products, moneyMovement.getDestination());
				flagDirtyProducts(products);
				finishWithResult(RESULT_MOVE_MONEY_SUCCESS);
			}
		};
	}

	private void flagDirtyProducts(List<ProductDetails> products) {
		for (ProductDetails productDetail : products) {
			productDetail.setDirty(true);
		}
		getApplicationState().updateProductDetails(products);
	}

	protected void addDestination(List<ProductDetails> products, D destination) {
		// Do nothing
	}

	@Override
	public void moneyMovementDeclined(T moneyMovement, ErrorResponseWrapper errorResponseWrapper) {
		getApplicationState().getAnalyticsTracker().trackMoveMoneyCompletedUnsuccessfully(moneyMovement);
		resultOverlayRenderer.renderMoneyMovementDeclinedOverlay(moneyMovement, errorResponseWrapper, null);
	}

	private void configureHeader() {
		ViewGroup headerContainer = (ViewGroup) findViewById(R.id.movemoney_header);
		ProductHeaderViewFactory productHeaderViewFactory = new ProductHeaderViewFactory(sourceProduct, this, headerContainer);
		productHeaderViewFactory.createHeaderView();
	}

	protected void configureNoDestinationsMessage() {
		// Do nothing
	}

	private void configureDestinationSelector() {

		ValueSelectionListener<D> valueSelectionListener = new ValueSelectionListener<D>() {

			@Override
			public void valueSelected(D selectedValue) {

				if (destinationSelector.getState() == Active) {
					handleDestinationSelected();

				} else {
					handleActivateDestination();
				}
				refreshInProgressIndicator();
			}
		};

		destinationSelector = createDestinationSelector(valueSelectionListener);

		findViewById(R.id.movemoney_product_selector_layout).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				handleActivateDestination();
			}
		});
	}

	protected abstract ValueSelector<D> createDestinationSelector(ValueSelectionListener<D> valueSelectionListener);

	private void handleActivateDestination() {
		hideRightColumn();
		destinationSelector.setState(Active);
		amountSelector.setState(NotStarted);
		dateSelector.setState(NotStarted);
		refreshInProgressIndicator();
	}

	protected void handleDestinationSelected() {
		showRightColumn();
		destinationSelector.setState(Complete);
		amountSelector.setState(amountSelector.isValid() ? Complete : Active);
		if (amountSelector.isComplete()) {
			dateSelector.setState(dateSelector.isValid() ? Complete : Active);
		}
	}

	private void configureAmountSelector() {

		ValueSelectionListener<BigDecimal> valueSelectionListener = new ValueSelectionListener<BigDecimal>() {
			@Override
			public void valueSelected(BigDecimal selectedValue) {
				amountSelector.setState(Complete);
				dateSelector.setState(dateSelector.isValid() ? Complete : Active);
				refreshInProgressIndicator();
			}
		};

		amountSelector = new AmountSelector(this, valueSelectionListener, destinationSelector);

		findViewById(R.id.movemoney_amount_selector_layout).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				amountSelector.setState(Active);
				dateSelector.setState(NotStarted);
				refreshInProgressIndicator();
			}
		});
	}

	private void configureDateSelector() {

		dateSelector = new DateSelector(this, new ValueSelectionListener<Date>() {

			@Override
			public void valueSelected(Date selectedValue) {

				if (selectedValue == null) {
					return;
				}

				dateSelector.setState(Complete);
				refreshInProgressIndicator();
			}
		}, destinationSelector);
	}

	protected void configureInProgressIndicator() {

		inProgressIndicator = new InProgressIndicator(this, true);
		refreshInProgressIndicator();
	}

	protected void refreshInProgressIndicator() {
		boolean show = isShowInProgressIndicator();
		findViewById(R.id.button_progress_layout).setVisibility(show ? View.VISIBLE : View.GONE);
		inProgressIndicator.setContinueEnabled(show);
	}

	private void showRightColumn() {
		View rightScrollView = findViewById(R.id.movemoney_right_column);
		if (rightScrollView != null) {
			rightScrollView.setVisibility(View.VISIBLE);
		}
	}

	private void hideRightColumn() {
		View rightScrollView = findViewById(R.id.movemoney_right_column);
		if (rightScrollView != null) {
			rightScrollView.setVisibility(View.GONE);
		}
	}

	private boolean isShowInProgressIndicator() {
		return isDestinationOk() && isAmountOk() && isDateOk();
	}

	private boolean isDestinationOk() {
		return destinationSelector.isComplete() && destinationSelector.isValid();
	}

	private boolean isAmountOk() {
		return amountSelector.isComplete() && amountSelector.isValid();
	}

	private boolean isDateOk() {
		return dateSelector.isComplete() && dateSelector.isValid();
	}

	protected abstract void configureMoneyMover();

	protected ProductDetails getSourceProduct() {
		return sourceProduct;
	}

	protected ValueSelector<D> getDestinationSelector() {
		return destinationSelector;
	}

	protected T getMoneyMovement() {
		return moneyMovement;
	}

	protected InProgressIndicator getInProgressIndicator() {
		return inProgressIndicator;
	}

	protected void setMoneyMover(MoneyMover<T> moneyMover) {
		this.moneyMover = moneyMover;
	}

	protected DateSelector getDateSelector() {
		return dateSelector;
	}
}
