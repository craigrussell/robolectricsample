package com.tescobank.mobile.ui.movemoney;

import static com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError.MoneyMovementDateAfterOneYear;
import static com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError.MoneyMovementDateBeforeToday;
import static com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError.MoneyMovementDateBeforeTomorrow;
import static com.tescobank.mobile.model.product.ProductType.typeForLabel;
import static java.util.Calendar.DATE;
import static java.util.Calendar.DAY_OF_YEAR;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.YEAR;

import java.util.Calendar;
import java.util.Date;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.api.payment.PayTransacteeRequest;
import com.tescobank.mobile.api.payment.PayTransacteeResponse;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.movemoney.MoneyMovement;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoAlertDialog;
import com.tescobank.mobile.ui.errors.ErrorHandler;
import com.tescobank.mobile.ui.movemoney.transfer.TransferMaker;

/**
 * Abstract base class for money movers
 * 
 * @param T
 *            the money movement type
 */
public abstract class MoneyMover<T extends MoneyMovement<ProductDetails, ?>> {

	private static final String TAG = TransferMaker.class.getSimpleName();

	private final ErrorHandler errorHandler;

	private final DataFormatter dataFormatter;

	private final ProductDetails sourceProduct;

	private final TescoActivity activity;

	private final InProgressIndicator inProgressIndicator;

	private final MoneyMovementListener<T> moneyMovementListener;

	private boolean moneyMovementInProgress;

	private boolean isImmediatePaymentsEnabled;

	/**
	 * Constructor
	 * 
	 * @param sourceProduct
	 *            the source product to move money from
	 * @param activity
	 *            the activity
	 * @param inProgressIndicator
	 *            the in-progress indicator
	 * @param moneyMovementListener
	 *            the money movement listener
	 * @param errorHandler
	 *            the error handler
	 */
	public MoneyMover(ProductDetails sourceProduct, TescoActivity activity, InProgressIndicator inProgressIndicator, MoneyMovementListener<T> moneyMovementListener, ErrorHandler errorHandler) {
		super();

		this.sourceProduct = sourceProduct;
		this.activity = activity;
		this.inProgressIndicator = inProgressIndicator;
		this.moneyMovementListener = moneyMovementListener;
		this.errorHandler = errorHandler;
		this.dataFormatter = new DataFormatter();
		this.isImmediatePaymentsEnabled = activity.getApplicationState().getFeatures().isImmediatePaymentsEnabled();
	}

	/**
	 * @return whether a money movement is in progress
	 */
	public boolean isMoneyMovementInProgress() {
		return moneyMovementInProgress;
	}

	/**
	 * Moves money
	 * 
	 * @param moneyMovement
	 *            the money movement
	 */
	public abstract void moveMoney(T moneyMovement);

	protected boolean isDateValid(T moneyMovement) {

		Calendar moneyMovementCalendar = Calendar.getInstance();
		moneyMovementCalendar.setTime(moneyMovement.getDate());
		Date moneyMovementDate = createDateOnlyDate(moneyMovementCalendar);

		Date nowDate = getNowDate();
		Date lowerThresholdDate = getLowerThresholdDate();
		Date upperThresholdDate = getUpperThresholdDate(nowDate);

		if (moneyMovementDate.before(lowerThresholdDate)) {
			InAppError error = isImmediatePaymentsEnabled ? MoneyMovementDateBeforeToday : MoneyMovementDateBeforeTomorrow;
			errorHandler.handleError(new ErrorResponseWrapperBuilder(getActivity()).buildErrorResponse(error));
			return false;
		}

		if (moneyMovementDate.after(upperThresholdDate)) {
			errorHandler.handleError(new ErrorResponseWrapperBuilder(getActivity()).buildErrorResponse(MoneyMovementDateAfterOneYear));
			return false;
		}

		return true;
	}

	private Date getNowDate() {
		Calendar nowCalendar = Calendar.getInstance();
		return createDateOnlyDate(nowCalendar);
	}

	private Date getLowerThresholdDate() {
		Calendar lowerThresholdCalendar = Calendar.getInstance();

		if (!isImmediatePaymentsEnabled) {
			lowerThresholdCalendar.add(DATE, 1);
		}

		return createDateOnlyDate(lowerThresholdCalendar);
	}

	private Date getUpperThresholdDate(Date nowDate) {
		Calendar upperThresholdCalendar = Calendar.getInstance();
		upperThresholdCalendar.setTime(nowDate);
		upperThresholdCalendar.add(YEAR, 1);
		return createDateOnlyDate(upperThresholdCalendar);
	}

	private Date createDateOnlyDate(Calendar calendarIn) {

		Calendar calendarOut = Calendar.getInstance();
		calendarOut.setTimeInMillis(0);
		calendarOut.set(YEAR, calendarIn.get(YEAR));
		calendarOut.set(DAY_OF_YEAR, calendarIn.get(DAY_OF_YEAR));
		calendarOut.set(HOUR_OF_DAY, 0);

		return calendarOut.getTime();
	}

	protected void checkSourceAndMoveMoney(T moneyMovement) {
		if (typeForLabel(sourceProduct.getProductType()).isISA()) {
			confirmOKWithISASource(moneyMovement);
		} else {
			executeMoveMoneyRequest(moneyMovement);
		}
	}

	private void confirmOKWithISASource(final T moneyMovement) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "confirmOKWithISASource");
		}
		String message = activity.getResources().getString(R.string.movemoney_confirm_message, dataFormatter.formatCurrency(moneyMovement.getAmount()));
		new TescoAlertDialog.Builder(activity).setTitle(R.string.movemoney_confirm_title).setMessage(message).setPositiveButton(R.string.movemoney_confirm_yes, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				executeMoveMoneyRequest(moneyMovement);
			}
		}).setNeutralButton(R.string.movemoney_confirm_no, null).create().show();
	}

	private void executeMoveMoneyRequest(T moneyMovement) {
		moneyMovementInProgress = true;
		inProgressIndicator.showInProgressIndicator();
		activity.getApplicationState().getRestRequestProcessor().processRequest(createRequest(moneyMovement));
	}

	private PayTransacteeRequest createRequest(T moneyMovement) {

		PayTransacteeRequest request = new MoverPayTransacteeRequest(moneyMovement);

		populateTransacteeCategory(request, moneyMovement);
		populateTransacteeId(request, moneyMovement);
		populateReference(request, moneyMovement);
		populateAccountDetails(request, moneyMovement);

		request.setAmount(moneyMovement.getAmount());
		request.setDate(moneyMovement.getDate());
		request.setFromProductId(moneyMovement.getSource().getProductId());

		return request;
	}

	protected abstract void populateTransacteeCategory(PayTransacteeRequest request, T moneyMovement);

	protected abstract void populateTransacteeId(PayTransacteeRequest request, T moneyMovement);

	protected abstract void populateAccountDetails(PayTransacteeRequest request, T moneyMovement);

	protected void populateReference(PayTransacteeRequest request, T moneyMovement) {
		// Do nothing
	}

	protected TescoActivity getActivity() {
		return activity;
	}

	protected DataFormatter getDataFormatter() {
		return dataFormatter;
	}

	private class MoverPayTransacteeRequest extends PayTransacteeRequest {

		private T moneyMovement;

		public MoverPayTransacteeRequest(T moneyMovement) {
			super();
			this.moneyMovement = moneyMovement;
		}

		@Override
		public void onResponse(PayTransacteeResponse response) {
			moneyMovementInProgress = false;
			inProgressIndicator.hideInProgressIndicator();
			moneyMovementListener.moneyMovementSuccessful(moneyMovement);
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponseWrapper) {
			moneyMovementInProgress = false;
			inProgressIndicator.hideInProgressIndicator();

			if (errorResponseWrapper.getErrorResponse().isDeclined()) {
				moneyMovementListener.moneyMovementDeclined(moneyMovement, errorResponseWrapper);
			} else {
				errorHandler.handleError(errorResponseWrapper);
			}
		}
	}
}
