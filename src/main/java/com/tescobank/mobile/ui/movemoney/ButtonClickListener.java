package com.tescobank.mobile.ui.movemoney;

/**
 * Button click listener
 */
public interface ButtonClickListener {
	
	/**
	 * Called when a button is clicked
	 */
	void buttonClicked();
}
