package com.tescobank.mobile.ui.movemoney.payment;

import java.util.regex.Pattern;

/**
 * Reference validator
 */
public class ReferenceValidator {
	
	/** The validation pattern */
	private static final Pattern PATTERN = Pattern.compile("[A-Za-z0-9 ]{0,16}");
	
	/** Empty string */
	private static final String EMPTY_STRING = "";
	
	/**
	 * Constructor
	 */
	public ReferenceValidator() {
		super();
	}
	
	/**
	 * Validates the specified reference
	 * 
	 * @param reference the reference
	 * 
	 * @return true if the reference is valid
	 */
	public boolean isValid(String reference) {
		return PATTERN.matcher((reference == null) ? EMPTY_STRING : reference).matches();
	}
}
