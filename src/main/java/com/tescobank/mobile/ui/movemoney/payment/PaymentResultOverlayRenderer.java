package com.tescobank.mobile.ui.movemoney.payment;

import android.app.Activity;

import com.tescobank.mobile.R;
import com.tescobank.mobile.model.movemoney.Payment;
import com.tescobank.mobile.ui.movemoney.MoneyMovementResultOverlayRenderer;

/**
 * Renders result overlays required by the payment activity
 */
public class PaymentResultOverlayRenderer extends MoneyMovementResultOverlayRenderer<Payment> {

	/**
	 * Constructor
	 * 
	 * @param activity
	 *            the activity
	 */
	public PaymentResultOverlayRenderer(Activity activity) {
		super(activity);
	}

	@Override
	protected String getMoveMoneyTypeForDisplay() {
		return getActivity().getString(R.string.movemoney_type_payment);
	}
	
}
