package com.tescobank.mobile.ui.movemoney.payment;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectorState.Active;
import static com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectorState.Complete;
import static com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectorState.NotStarted;

import java.util.ArrayList;
import java.util.List;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.payment.GetTransacteesForUserResponse.Transactee;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoEditText;
import com.tescobank.mobile.ui.movemoney.ValueSelector;

/**
 * Supports selecting a single transactee from a list of available transactees
 */
public class TransacteeSelector extends ValueSelector<Transactee> {
	
	public static final String TAG_PREFIX_CONTAINER = "transactee";
	
	private List<Transactee> availableTransactees;
	
	private ViewGroup container;

	private TextView titleView;

	private List<View> transacteeViews;
	
	private ReferenceValidator referenceValidator;
	
	private boolean editingReference;
	
	private boolean ignoreReferenceChanges;
	
	private String previousReference;
	
	private boolean isEditPaymentReferenceEnabled;
	
	/**
	 * Constructor
	 * 
	 * @param availableTransactees the available transactees
	 * @param activity the activity
	 * @param valueSelectionListener the value selection listener
	 */
	public TransacteeSelector(List<Transactee> availableTransactees, TescoActivity activity, ValueSelectionListener<Transactee> valueSelectionListener) {
		super(activity, valueSelectionListener);
		
		this.availableTransactees = availableTransactees;
		this.container = (ViewGroup) activity.findViewById(R.id.movemoney_product_selection_available_container);
		this.titleView = (TextView) activity.findViewById(R.id.movemoney_product_selector_title);
		this.transacteeViews = new ArrayList<View>();
		this.referenceValidator = new ReferenceValidator();
		this.isEditPaymentReferenceEnabled = activity.getApplicationState().getFeatures().isEditPaymentReferenceEnabled();
		
		configureTransactees();
		configureInitialState();
	}
	
	@Override
	public void buttonClicked() {
		makeReferenceDisabled();
	}
	
	@Override
	public final void setState(ValueSelectorState state) {
		super.setState(state);

		if (container == null) {
			return;
		}
		
		getActivity().hideKeyboard();

		switch (state) {

		case NotStarted:
			hide();
			showTitleForNoTransacteeSelected();
			showAllAvailableTransactees();
			hideAllReferences();
			revertAllReferences();
			
		case Active:
			showTitleForNoTransacteeSelected();
			showAllAvailableTransactees();
			hideAllReferences();
			revertAllReferences();
			break;

		case Complete:
			showTitleForTransacteeSelected();
			showSelectedTransacteeOnly();
			showReferenceForSelectedTransactee();
			break;
		}
	}

	
	@Override
	protected void setInitialState() {
		setState(Active);
	}

	@Override
	public boolean isValid() {
		return getSelectedValue() != null;
	}
	
	@Override
	public boolean handleBack() {
		if (editingReference) {
			revertReference();
			return true;
		} else {
			return false;
		}
	}
	
	private int getSelectedIndex() {
		return availableTransactees.indexOf(getSelectedValue());
	}
	
	private View getSelectedView() {
		View view = null;
		int index = getSelectedIndex();
		if(index >= 0) {
			view = getActivity().findViewById(android.R.id.content).findViewWithTag(TAG_PREFIX_CONTAINER + index);
		}
		return view;
	}
	
	private void configureTransactees() {

		TescoActivity activity = getActivity();
		LayoutInflater layoutInflater = activity.getLayoutInflater();
		DataFormatter dataFormatter = new DataFormatter();
		int marginBottom = (int) activity.getResources().getDimension(R.dimen.movemoney_destination_bottom_margin);
		
		for (int i = 0; i < availableTransactees.size(); i++) {
			configureTransactee(availableTransactees.get(i), i, layoutInflater, dataFormatter, marginBottom);
		}
	}
	
	private void configureTransactee(Transactee transactee, int index, LayoutInflater layoutInflater, DataFormatter dataFormatter, int marginBottom) {

		View transacteeView = layoutInflater.inflate(R.layout.include_movemoney_payment_transactee_selector, null);
		transacteeViews.add(transacteeView);

		transacteeView.setTag(TAG_PREFIX_CONTAINER + index);
		transacteeView.setContentDescription(createContentDescription(transactee, index, dataFormatter));
		attachOnClickListenerToTransacteeView(transacteeView);

		TextView name = (TextView) transacteeView.findViewById(R.id.transactee_name);
		name.setText(getName(transactee));
		
		TextView number = (TextView) transacteeView.findViewById(R.id.transactee_number);
		number.setText(dataFormatter.formatSortCode(transactee.getBankAccount().getSortCode()) + "    " + transactee.getBankAccount().getAccountNumber());
		
		TescoEditText referenceInput = (TescoEditText) transacteeView.findViewById(R.id.transactee_reference_input);
		attachTextChangedListenerToReferenceInput(referenceInput, transactee);
		attachOnFocusChangeListenerToReferenceInput(referenceInput, transacteeView);
		attachOnEditorActionListenerToReferenceInput(referenceInput, transacteeView);
		
		View referenceEdit = transacteeView.findViewById(R.id.transactee_reference_edit);
		if (isEditPaymentReferenceEnabled) {
			attachOnClickListenerToReferenceEdit(referenceEdit, transacteeView);
		} else {
			transacteeView.findViewById(R.id.transactee_reference_edit).setVisibility(GONE);
		}

		LayoutParams layoutParams = new LayoutParams(MATCH_PARENT, WRAP_CONTENT);
		layoutParams.setMargins(0, 0, 0, marginBottom);
		container.addView(transacteeView, layoutParams);
	}
	
	private void attachOnClickListenerToTransacteeView(View transacteeView) {
		transacteeView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				if (getState() == Complete) {
					if (editingReference) {
						makeReferenceDisabled(view);
					} else {
						getActivity().findViewById(R.id.movemoney_product_selector_layout).performClick();
					}
				} else {
					String tag = (String) view.getTag();
					int index = Integer.valueOf(tag.substring(TAG_PREFIX_CONTAINER.length()));
					setSelectedValue(availableTransactees.get(index));
				}
			}
		});
	}
	
	private void attachTextChangedListenerToReferenceInput(TescoEditText referenceInput, Transactee transactee) {
		referenceInput.addTextChangedListener(new ReferenceTextWatcher(referenceInput, transactee));
	}
	
	private void attachOnFocusChangeListenerToReferenceInput(TescoEditText referenceInput, final View transacteeView) {
		referenceInput.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				
				if (ignoreReferenceChanges) {
					return;
					
				}
				if (!hasFocus && (getState() == Complete)) {
					makeReferenceDisabled(transacteeView);
				}
			}
		});
	}
	
	private void attachOnEditorActionListenerToReferenceInput(TescoEditText referenceInput, final View transacteeView) {
		referenceInput.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
					makeReferenceDisabled(transacteeView);
				}
				return false;
			}
		});
	}
	
	private void attachOnClickListenerToReferenceEdit(View referenceEdit, final View transacteeView) {
		referenceEdit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				makeReferenceEditable(transacteeView);
			}
		});
	}
	
	private void hide() {
		View outerContainer = getActivity().findViewById(R.id.movemoney_product_selector_layout);
		if (outerContainer != null) {
			outerContainer.setVisibility(GONE);
		}
	}
	
	private void showTitleForNoTransacteeSelected() {
		titleView.setText(R.string.movemoney_destination_selector_prompt);
	}
	
	private void showTitleForTransacteeSelected() {
		titleView.setText(R.string.movemoney_destination_selector_selected);
	}
	
	private void showAllAvailableTransactees() {
		for (View view : transacteeViews) {
			view.setVisibility(VISIBLE);
		}
	}
	
	private void showSelectedTransacteeOnly() {

		Transactee selectedTransactee = getSelectedValue();
		for (int i = 0; i < availableTransactees.size(); i++) {
			
			if (!availableTransactees.get(i).equals(selectedTransactee)) {
				transacteeViews.get(i).setVisibility(GONE);
			}
		}
	}
	
	private void showReferenceForSelectedTransactee() {
		
		Transactee selectedTransactee = getSelectedValue();
		for (int i = 0; i < availableTransactees.size(); i++) {
			
			if (availableTransactees.get(i).equals(selectedTransactee)) {
				
				View transacteeView = transacteeViews.get(i);
				
				transacteeView.findViewById(R.id.transactee_reference_title).setVisibility(VISIBLE);
				transacteeView.findViewById(R.id.transactee_reference_line).setVisibility(VISIBLE);
				
				TescoEditText referenceEdit = (TescoEditText) transacteeView.findViewById(R.id.transactee_reference_input);
				referenceEdit.setVisibility(VISIBLE);
				referenceEdit.setEnabled(false);
				referenceEdit.setText(selectedTransactee.getTransacteeReference());
				
				transacteeView.findViewById(R.id.transactee_reference_input).setVisibility(VISIBLE);
				
				showReferenceEditIfSupported(transacteeView);
				
				break;
			}
		}
		
		getActivity().hideKeyboard();
		
		editingReference = false;
	}
	
	private void hideAllReferences() {
		
		for (View transacteeView : transacteeViews) {
			transacteeView.findViewById(R.id.transactee_reference_title).setVisibility(GONE);
			transacteeView.findViewById(R.id.transactee_reference_input).setVisibility(GONE);
			transacteeView.findViewById(R.id.transactee_reference_line).setVisibility(GONE);
			hideReferenceEditIfSupported(transacteeView);
		}
		
		getActivity().hideKeyboard();
		
		editingReference = false;
	}

	private EditText getReferenceInput() {
		if(getSelectedView() != null) {
			return (EditText) getSelectedView().findViewById(R.id.transactee_reference_input);
		}
		return null;
	}
	
	public String getReferenceText() {	
		EditText referenceInput = getReferenceInput();
		return referenceInput.getText().toString();
	}

	public boolean isReferenceEditing() {	
		EditText referenceInput = getReferenceInput();
		return referenceInput.isEnabled();
	}
	
	public void restoreReference(String text, boolean isEditing) {
		EditText referenceInput = getReferenceInput();
		if(referenceInput == null) {
			return;
		}
		referenceInput.setText(text);
		if (isEditing) {
			makeReferenceEditable(getSelectedView());
		} else {
			makeReferenceDisabled(getSelectedView());
		}
	}
	
	private void makeReferenceEditable(View transacteeView) {
		
		ignoreReferenceChanges = true;
		
		TescoEditText referenceInput = (TescoEditText) transacteeView.findViewById(R.id.transactee_reference_input);
		referenceInput.setVisibility(VISIBLE);
		referenceInput.setFocusable(true);
		referenceInput.setFocusableInTouchMode(true);
		referenceInput.setEnabled(true);
		referenceInput.requestFocus();
		referenceInput.setSelection(referenceInput.getText().length());
		hideReferenceEditIfSupported(transacteeView);	
		getActivity().showKeyboard();
		
		editingReference = true;
		ignoreReferenceChanges = false;
		previousReference = getSelectedValue().getTransacteeReference();
	}
	
	public void makeReferenceDisabled() {
		
		Transactee selectedTransactee = getSelectedValue();
		
		for (int i = 0; i < availableTransactees.size(); i++) {
			if (availableTransactees.get(i).equals(selectedTransactee)) {
				
				View transacteeView = transacteeViews.get(i);
				View referenceInput = transacteeView.findViewById(R.id.transactee_reference_input);
				if (referenceInput.getVisibility() == VISIBLE) {
					makeReferenceDisabled(transacteeView);
				}
				break;
			}
		}
	}
	
	private void makeReferenceDisabled(View transacteeView) {
		
		TescoEditText referenceInput = (TescoEditText) transacteeView.findViewById(R.id.transactee_reference_input);
		referenceInput.setSelected(false);
		referenceInput.setFocusable(false);
		referenceInput.setFocusableInTouchMode(false);
		referenceInput.setEnabled(false);
		showReferenceEditIfSupported(transacteeView);
		getActivity().hideKeyboard();
		editingReference = false;
	}
	
	private void revertReference() {
		
		ignoreReferenceChanges = true;
		
		Transactee selectedTransactee = getSelectedValue();
		selectedTransactee.setSelectedTransacteeReference(previousReference);
		
		for (int i = 0; i < availableTransactees.size(); i++) {
			
			if (availableTransactees.get(i).equals(selectedTransactee)) {
				
				View transacteeView = transacteeViews.get(i);

				TescoEditText referenceInput = (TescoEditText) transacteeView.findViewById(R.id.transactee_reference_input);

				referenceInput.setText(previousReference);
				referenceInput.setSelected(false);
				referenceInput.setEnabled(false);
				
				showReferenceEditIfSupported(transacteeView);
				
				break;
			}
		}
		
		getActivity().hideKeyboard();
		
		editingReference = false;
		ignoreReferenceChanges = false;
	}
	
	private void revertAllReferences() {
		for (Transactee availableTransactee : availableTransactees) {
			availableTransactee.setSelectedTransacteeReference(availableTransactee.getTransacteeReference());
		}
	}
	
	private void showReferenceEditIfSupported(View transacteeView) {
		if (isEditPaymentReferenceEnabled) {
			transacteeView.findViewById(R.id.transactee_reference_edit).setVisibility(VISIBLE);
		}
	}
	
	private void hideReferenceEditIfSupported(View transacteeView) {
		if (isEditPaymentReferenceEnabled) {
			transacteeView.findViewById(R.id.transactee_reference_edit).setVisibility(GONE);
		}
	}
	
	private String createContentDescription(Transactee transactee, int index, DataFormatter dataFormatter) {
		int position = index + 1;
		int count = availableTransactees.size();
		String name = getName(transactee);
		String sortCode = dataFormatter.formatSortCode(transactee.getBankAccount().getSortCode());
		String number = transactee.getBankAccount().getAccountNumber();
		
		return getActivity().getResources().getString(R.string.payment_transactee_selector_transactee_description, position, count, name, sortCode, number);
	}
	
	private String getName(Transactee transactee) {
		String nickName = transactee.getTransacteeNickName();
		return ((nickName != null) && !nickName.isEmpty()) ? nickName : transactee.getTransacteeName();
	}
	
	private void configureInitialState() {
		if (availableTransactees.isEmpty()) {
			setState(NotStarted);
		}
	}
	
	private class ReferenceTextWatcher implements TextWatcher {
		
		private TescoEditText referenceInput;
		private Transactee transactee;
		private String previousText;
		
		public ReferenceTextWatcher(TescoEditText referenceInput, Transactee transactee) {
			super();
			
			this.referenceInput = referenceInput;
			this.transactee = transactee;
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			
			if (ignoreReferenceChanges) {
				return;
			}
			
			String newText = referenceInput.getText().toString();
			if (referenceValidator.isValid(newText)) {
				
				transactee.setSelectedTransacteeReference(newText);
				
			} else {
				
				ignoreReferenceChanges = true;
				referenceInput.setText(previousText);
				ignoreReferenceChanges = false;
				
				referenceInput.setSelection(referenceInput.getText().toString().length());
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			previousText = referenceInput.getText().toString();
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
	}
}
