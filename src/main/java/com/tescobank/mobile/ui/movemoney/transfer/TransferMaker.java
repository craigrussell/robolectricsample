package com.tescobank.mobile.ui.movemoney.transfer;

import static com.tescobank.mobile.api.payment.PayTransacteeRequest.TransacteeCategory.INTERNAL;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.payment.PayTransacteeRequest;
import com.tescobank.mobile.model.movemoney.Transfer;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoAlertDialog;
import com.tescobank.mobile.ui.errors.ErrorHandler;
import com.tescobank.mobile.ui.movemoney.MoneyMovementListener;
import com.tescobank.mobile.ui.movemoney.MoneyMover;

/**
 * Makes a transfer
 */
public class TransferMaker extends MoneyMover<Transfer> {

	private static final String TAG = TransferMaker.class.getSimpleName();

	/**
	 * Constructor
	 * 
	 * @param sourceProduct
	 *            the source product to move money from
	 * @param activity
	 *            the activity
	 * @param inProgressIndicator
	 *            the in-progress indicator
	 * @param moneyMovementListener
	 *            the money movement listener
	 * @param errorHandler
	 *            the error handler
	 */
	public TransferMaker(ProductDetails sourceProduct, TescoActivity activity, InProgressIndicator inProgressIndicator, MoneyMovementListener<Transfer> moneyMovementListener, ErrorHandler errorHandler) {
		super(sourceProduct, activity, inProgressIndicator, moneyMovementListener, errorHandler);
	}

	@Override
	public void moveMoney(Transfer moneyMovement) {
		if (!isDateValid(moneyMovement)) {
			return;
		}

		if (moneyMovement.isISADestination()) {
			checkISADestination(moneyMovement);
		} else {
			checkSourceAndMoveMoney(moneyMovement);
		}
	}

	private void checkISADestination(Transfer moneyMovement) {
		boolean nextTaxYear = moneyMovement.isNextTaxYear();
		if (moneyMovement.isBreachingLimits(nextTaxYear, getActivity().getApplicationState().getNextTaxYearSubscriptionLimit(moneyMovement.getDestination()))) {
			showBreachingLimit();
		} else if (nextTaxYear) {
			confirmOKWithNextTaxYear(moneyMovement);
		} else {
			checkSourceAndMoveMoney(moneyMovement);
		}
	}

	private void confirmOKWithNextTaxYear(final Transfer moneyMovement) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "confirmOKWithNextTaxYear");
		}

		new TescoAlertDialog.Builder(getActivity()).setTitle(R.string.movemoney_confirm_title).setMessage(R.string.transfer_date_selector_confirm_isa_date_message).setPositiveButton(R.string.movemoney_confirm_yes, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				checkSourceAndMoveMoney(moneyMovement);
			}
		}).setNeutralButton(R.string.movemoney_confirm_no, null).create().show();
	}

	private void showBreachingLimit() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "showBreachingLimit");
		}
		new TescoAlertDialog.Builder(getActivity()).setTitle(R.string.transfer_breaching_alert_title).setMessage(R.string.transfer_breaching_alert_message).setPositiveButton(android.R.string.ok, null).create().show();
	}

	@Override
	protected void populateTransacteeCategory(PayTransacteeRequest request, Transfer transfer) {
		request.setTransacteeCategory(INTERNAL);
	}

	@Override
	protected void populateTransacteeId(PayTransacteeRequest request, Transfer moneyMovement) {
		request.setTransacteeId(moneyMovement.getDestination().getProductId());
	}

	@Override
	protected void populateAccountDetails(PayTransacteeRequest request, Transfer moneyMovement) {
		String accountDetails = getDataFormatter().sortCodeAndAccoutNumberJoiner(moneyMovement.getDestination().getSortCode(), moneyMovement.getDestination().getAccountNumber());
		request.setAccountDetails(accountDetails);
	}

}
