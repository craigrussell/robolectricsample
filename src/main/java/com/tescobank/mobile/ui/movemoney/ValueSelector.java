package com.tescobank.mobile.ui.movemoney;

import static com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectorState.Complete;

import com.tescobank.mobile.ui.TescoActivity;

/**
 * Base class for value selectors
 * 
 * @param <T> the value type
 */
public abstract class ValueSelector<T> implements ButtonClickListener {
	
	/** The value selection listener */
	private ValueSelectionListener<T> valueSelectionListener;
	
	/** The current state */
	private ValueSelectorState state;
	
	/** The selected value */
	private T selectedValue;
	
	/** The activity. */
	private TescoActivity activity;

	/**
	 * Constructor
	 * 
	 * @param activty the activity  
	 * @param valueSelectionListener the value selection listener
	 */
	public ValueSelector(TescoActivity activity, ValueSelectionListener<T> valueSelectionListener) {
		super();
		
		this.activity = activity;
		this.valueSelectionListener = valueSelectionListener;
		
		setInitialState();
	}
	
	@Override
	public void buttonClicked() {
		// Do nothing
	}
	
	/**
	 * @return the current state
	 */
	public ValueSelectorState getState() {
		return state;
	}
	
	/**
	 * @param state the current state
	 */
	public void setState(ValueSelectorState state) {
		this.state = state;
	}
	
	/**
	 * @return the selected value
	 */
	public T getSelectedValue() {
		return selectedValue;
	}
	
	/**
	 * @return true if the selector contains a valid value selection
	 */
	public abstract boolean isValid();
	
	public boolean isComplete() {
		return state == Complete;
	}
	
	/**
	 * @return whether the value selector handled and consumed the 'back'
	 */
	public boolean handleBack() {
		return false;
	}
	
	/**
	 * Sets the initial state of the selector by calling <setState()
	 */
	protected abstract void setInitialState();
	
	/**
	 * @param selectedValue the selected value
	 */
	public void setSelectedValue(T selectedValue) {
		setSelectedValue(selectedValue, true);
	}
	
	/**
	 * @param selectedValue the selected value
	 * @param notify whether the value selection listener should be notified
	 */
	protected void setSelectedValue(T selectedValue, boolean notify) {
		this.selectedValue = selectedValue;
		if (notify) {
			valueSelectionListener.valueSelected(selectedValue);
		}
	}
	
	/**
	 * @return the activity
	 */
	public TescoActivity getActivity() {
		return activity;
	}
	
	/**
	 * Supported value selector states
	 */
	public static enum ValueSelectorState {
		NotStarted,
		Active,
		Complete
	}
	
	/**
	 * A value selection listener
	 * 
	 * @param <T> the value type
	 */
	public interface ValueSelectionListener<T> {
		
		/**
		 * @param selectedValue the selected value
		 */
		void valueSelected(T selectedValue);
	}
}
