package com.tescobank.mobile.ui.movemoney;

import static com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectorState.NotStarted;

import java.math.BigDecimal;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.ui.AmountFilter;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoEditText;

public class AmountSelector extends ValueSelector<BigDecimal> {

	private static final double MAX_AMOUNT = 999999.99;

	private final DataFormatter formatter = new DataFormatter();

	private TescoEditText otherAmountField;
	private TextView titleField;
	private View inputContainer;
	private TextView selectedValue;
	private View amountSelectorView;
	private View amountEditView;
	private Button doneButton;
	private ButtonClickListener buttonClickListener;

	private final OnClickListener valueButtonClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			String value = String.class.cast(v.getTag());
			BigDecimal current = getSelectedValue();
			BigDecimal newValue = current.add(new BigDecimal(value));
			if (newValue.doubleValue() <= MAX_AMOUNT) {
				otherAmountField.setText(newValue.toString());
				doneButton.setEnabled(true);
			}
			buttonClickListener.buttonClicked();
		}
	};
	
	private final OnClickListener pnpValueButtonClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			String value = String.class.cast(v.getTag());
			BigDecimal newValue = new BigDecimal(value);
			otherAmountField.setText(newValue.toString());
			doneButton.setEnabled(true);
			buttonClickListener.buttonClicked();
		}
	};
	

	private final OnClickListener doneButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			done();
			buttonClickListener.buttonClicked();
		}
	};

	public AmountSelector(TescoActivity activty, ValueSelectionListener<BigDecimal> listener, ButtonClickListener buttonClickListener) {
		super(activty, listener);
		this.amountSelectorView = activty.findViewById(R.id.movemoney_amount_selector_layout);
		this.buttonClickListener = buttonClickListener;

		amountSelectorView.findViewById(R.id.movemoney_amount_selector_five_pounds).setOnClickListener(valueButtonClickListener);
		amountSelectorView.findViewById(R.id.movemoney_amount_selector_ten_pounds).setOnClickListener(valueButtonClickListener);
		amountSelectorView.findViewById(R.id.movemoney_amount_selector_twenty_pounds).setOnClickListener(valueButtonClickListener);
		amountSelectorView.findViewById(R.id.movemoney_amount_selector_fifty_pounds).setOnClickListener(valueButtonClickListener);
		amountSelectorView.findViewById(R.id.movemoney_amount_selector_hundred_pounds).setOnClickListener(valueButtonClickListener);
		amountSelectorView.findViewById(R.id.movemoney_amount_selector_pnp).setOnClickListener(pnpValueButtonClickListener);

		amountSelectorView.findViewById(R.id.movemoney_amount_selector_done).setOnClickListener(doneButtonClickListener);

		otherAmountField = TescoEditText.class.cast(amountSelectorView.findViewById(R.id.movemoney_amount_selector_text_input));

		otherAmountField.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				handleTextChange();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
		});
		
		otherAmountField.setOnEditorActionListener(new OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
					done();
				}
				return false;
			}
		});
		
		otherAmountField.setFilters(new InputFilter[] { new AmountFilter() });

		titleField = TextView.class.cast(amountSelectorView.findViewById(R.id.movemoney_amount_selector_title));
		inputContainer = amountSelectorView.findViewById(R.id.movemoney_amount_selector_input_container);
		selectedValue = TextView.class.cast(amountSelectorView.findViewById(R.id.movemoney_amount_selector_selected_value));
		amountEditView = amountSelectorView.findViewById(R.id.transactee_amount_edit);

		doneButton = Button.class.cast(amountSelectorView.findViewById(R.id.movemoney_amount_selector_done));

		setInitialState();
	}
	
	private void handleTextChange() {
		
		try {
			if (otherAmountField.length() > 0) {		
				otherAmountField.setPrefix(getActivity().getResources().getString(R.string.movemoney_amount_selector_pound_symbol));
				otherAmountField.setTextSuffixColour(getActivity().getResources().getColor(R.color.blue_tesco));
				
				if(Double.valueOf(otherAmountField.getText().toString()) > 0.0) {
					doneButton.setEnabled(true);
					otherAmountField.setSelection(otherAmountField.getText().length());
				}
				
			} else {
				otherAmountField.setPrefix("");
				doneButton.setEnabled(false);
			}
		} catch (NumberFormatException e) {
			doneButton.setEnabled(false);
			otherAmountField.setText("");
		}
	}

	@Override
	public BigDecimal getSelectedValue() {
		if (!otherAmountField.getText().toString().trim().isEmpty()) {
			return new BigDecimal(otherAmountField.getText().toString().trim());
		}
		return BigDecimal.ZERO;
	}

	@Override
	protected final void setInitialState() {
		setState(NotStarted);
	}

	@Override
	public final void setState(com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectorState state) {
		super.setState(state);

		if (amountSelectorView == null) {
			return;
		}

		getActivity().hideKeyboard();

		switch (state) {

		case NotStarted:
			amountSelectorView.setVisibility(View.GONE);
			titleField.setText(R.string.movemoney_amount_selector_prompt);
			break;

		case Active:
			amountSelectorView.setVisibility(View.VISIBLE);
			selectedValue.setVisibility(View.GONE);
			amountEditView.setVisibility(View.GONE);
			inputContainer.setVisibility(View.VISIBLE);
			titleField.setText(R.string.movemoney_amount_selector_prompt);
			break;

		case Complete:
			amountSelectorView.setVisibility(View.VISIBLE);
			selectedValue.setVisibility(View.VISIBLE);
			amountEditView.setVisibility(View.VISIBLE);
			inputContainer.setVisibility(View.GONE);
			amountSelectorView.findViewById(R.id.transactee_amount_edit).setVisibility(View.VISIBLE);
			titleField.setText(R.string.movemoney_amount_selector_info);
			selectedValue.setText(formatter.formatCurrency(getSelectedValue()));
			break;
		}
	}

	@Override
	public boolean isValid() {
		return !otherAmountField.getText().toString().trim().isEmpty();
	}

	private void done() {
		BigDecimal value = getSelectedValue();
		selectedValue.setText(formatter.formatCurrency(value));

		if (value.doubleValue() > 0.0) {
			setSelectedValue(value);
		}
	}
}
