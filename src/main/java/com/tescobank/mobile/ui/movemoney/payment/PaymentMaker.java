package com.tescobank.mobile.ui.movemoney.payment;

import com.tescobank.mobile.api.payment.PayTransacteeRequest;
import com.tescobank.mobile.api.payment.PayTransacteeRequest.TransacteeCategory;
import com.tescobank.mobile.model.movemoney.Payment;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.errors.ErrorHandler;
import com.tescobank.mobile.ui.movemoney.MoneyMovementListener;
import com.tescobank.mobile.ui.movemoney.MoneyMover;

/**
 * Makes a payment
 */
public class PaymentMaker extends MoneyMover<Payment> {

	/**
	 * Constructor
	 * 
	 * @param sourceProduct
	 *            the source product to move money from
	 * @param activity
	 *            the activity
	 * @param inProgressIndicator
	 *            the in-progress indicator
	 * @param moneyMovementListener
	 *            the money movement listener
	 * @param errorHandler
	 *            the error handler
	 */
	public PaymentMaker(ProductDetails sourceProduct, TescoActivity activity, InProgressIndicator inProgressIndicator, MoneyMovementListener<Payment> moneyMovementListener, ErrorHandler errorHandler) {
		super(sourceProduct, activity, inProgressIndicator, moneyMovementListener, errorHandler);
	}

	@Override
	public void moveMoney(Payment moneyMovement) {
		if (!isDateValid(moneyMovement)) {
			return;
		}

		checkSourceAndMoveMoney(moneyMovement);
	}

	@Override
	protected void populateTransacteeCategory(PayTransacteeRequest request, Payment payment) {
		request.setTransacteeCategory(TransacteeCategory.valueOf(payment.getDestination().getTransacteeCategory()));
	}

	@Override
	protected void populateTransacteeId(PayTransacteeRequest request, Payment moneyMovement) {
		request.setTransacteeId(moneyMovement.getDestination().getTransacteeId());
	}

	@Override
	protected void populateReference(PayTransacteeRequest request, Payment moneyMovement) {
		request.setReference(moneyMovement.getReference());
	}

	@Override
	protected void populateAccountDetails(PayTransacteeRequest request, Payment moneyMovement) {
		String accountDetails = getDataFormatter().sortCodeAndAccoutNumberJoiner(moneyMovement.getDestination().getBankAccount().getSortCode(), moneyMovement.getDestination().getBankAccount().getAccountNumber());
		request.setAccountDetails(accountDetails);
	}

}
