package com.tescobank.mobile.ui.movemoney.transfer;

import java.util.List;

import android.view.View;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.movemoney.Transfer;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.InProgressIndicator.ContinueButtonConfiguration;
import com.tescobank.mobile.ui.PayNoPayProductDisplay;
import com.tescobank.mobile.ui.movemoney.MoneyMovementActivity;
import com.tescobank.mobile.ui.movemoney.MoneyMovementResultOverlayRenderer;
import com.tescobank.mobile.ui.movemoney.ValueSelector;
import com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectionListener;

/**
 * Activity for making a transfer
 */
public class TransferActivity extends MoneyMovementActivity<Transfer, ProductDetails> implements ContinueButtonConfiguration {
	
	private PayNoPayProductDisplay pnpDisplay;
	private ProductDetails destination;
	
	@Override
	protected MoneyMovementResultOverlayRenderer<Transfer> createResultOverlayRenderer() {
		return new TransferResultOverlayRenderer(this);
	}

	@Override
	protected Transfer createMoneyMovement(ProductDetails source, ProductDetails destination) {
		return new Transfer(source, destination);
	}

	@Override
	protected void addDestination(List<ProductDetails> products, ProductDetails destination) {		
		products.add(destination);
	}

	@Override
	protected ValueSelector<ProductDetails> createDestinationSelector(ValueSelectionListener<ProductDetails> valueSelectionListener) {
		return new DestinationProductSelector(getSourceProduct(), getApplicationState().getProductDetails(), this, valueSelectionListener);
	}

	@Override
	protected void configureMoneyMover() {
		setMoneyMover(new TransferMaker(getSourceProduct(), this, getInProgressIndicator(), this, this));
	}

	@Override
	protected void trackAnalytic(Transfer moneyMovement) {
		getApplicationState().getAnalyticsTracker().trackMakeTransfer(moneyMovement,isPayNoPayTransfer());
	}
	
	@Override	
	protected void handleDestinationSelected() {
		
		super.handleDestinationSelected();
		pnpDisplay = new PayNoPayProductDisplay(getDestinationSelector().getSelectedValue());
		destination = getDestinationSelector().getSelectedValue();
		if(pnpDisplay.shouldDisplay()) {
			displayPayNoPayButton();
		} else {
			hidePayNoPayButton();
		}
	}
	
	private void displayPayNoPayButton() {
		DataFormatter formatter = new DataFormatter();
		String pnpText = getString(R.string.movemoney_amount_pnp) + " " + formatter.formatCurrency(destination.getAmountDue());
		TextView pnpView = (TextView) findViewById(R.id.movemoney_amount_selector_pnp);
		pnpView.setText(pnpText);
		pnpView.setVisibility(View.VISIBLE);	
		pnpView.setTag(destination.getAmountDue().toPlainString());
	}
	
	private void hidePayNoPayButton() {
		TextView pnpView = (TextView) findViewById(R.id.movemoney_amount_selector_pnp);
		pnpView.setVisibility(View.GONE);	
	}
	
	private boolean isPayNoPayTransfer() {
		return pnpDisplay.shouldDisplay() && destination.getAmountDue().doubleValue() == getMoneyMovement().getAmount().doubleValue();
	}

	@Override
	protected void trackProcessCompleted(Transfer moneyMovement) {
		getApplicationState().getAnalyticsTracker().trackMakeTransferCompletedSuccessfully(moneyMovement);
	}

	@Override
	public int getMessageId() {
		return getDateSelector().hasFutureDateBeenSelected() ? R.string.progress_scheduleTransfer : R.string.progress_makeTransfer;
	}

	@Override
	public int getBackgroundDrawable() {
		return R.drawable.button_payments_background_selector;
	}
	
	@Override
	public int getTextColors() {
		return R.color.button_payments_text_selector;
	}

	@Override
	protected void refreshInProgressIndicator() {
		super.refreshInProgressIndicator();
		getInProgressIndicator().updateContinueButtonConfiguration(this);
	}
}
