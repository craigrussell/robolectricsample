package com.tescobank.mobile.ui.movemoney.transfer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tescobank.mobile.model.product.ProductDetails;

/**
 * For transfers, mapping from source products to lists of destination products
 */
public class TransferableProducts {
	
	/** Mapping from source product to ordered list of one or more destination products */
	private Map<ProductDetails, List<ProductDetails>> transferProducts;
	
	/**
	 * Constructor
	 */
	public TransferableProducts() {
		super();
		
		transferProducts = new HashMap<ProductDetails, List<ProductDetails>>();
	}
	
	/**
	 * @param products the ordered list of products to set, potentially both source and/or destination products
	 */
	public void setProducts(List<ProductDetails> products) {
		
		if (null == products || products.isEmpty()) {
			return;
		}
		
		List<ProductDetails> allDestinationProducts = getAllDestinationProducts(products);
		if (allDestinationProducts.isEmpty()) {
			return;
		}
		
		populateTransferProducts(products, allDestinationProducts);
	}
	
	/**
	 * @param sourceProduct the source product
	 * 
	 * @return true if you can transfer from the specified source product
	 */
	public boolean canTransferFrom(ProductDetails sourceProduct) {
		return !getDestinationProducts(sourceProduct).isEmpty();
	}
	
	/**
	 * @param sourceProduct the source product
	 * 
	 * @return the ordered list of one or more destination products for the specified source product, of null if there are none
	 */
	public List<ProductDetails> getDestinationProducts(ProductDetails sourceProduct) {
		List<ProductDetails> products = transferProducts.get(sourceProduct);
		return (products == null) ? new ArrayList<ProductDetails>() : products;
	}
	
	private List<ProductDetails> getAllDestinationProducts(List<ProductDetails> products) {
		
		List<ProductDetails> allDestinationProducts = new ArrayList<ProductDetails>();
		
		for (ProductDetails product : products) {
			if (product.getCanBeDestination()) {
				allDestinationProducts.add(product);
			}
		}
		
		return allDestinationProducts;
	}
	
	private void populateTransferProducts(List<ProductDetails> products, List<ProductDetails> allDestinationProducts) {
		
		for (ProductDetails product : products) {
			if (product.getCanBeSource()) {
				
				List<ProductDetails> destinationProductsForProduct = new ArrayList<ProductDetails>(allDestinationProducts);
				destinationProductsForProduct.remove(product);
				
				if (!destinationProductsForProduct.isEmpty()) {
					transferProducts.put(product, destinationProductsForProduct);
				}
			}
		}
	}
}
