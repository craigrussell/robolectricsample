package com.tescobank.mobile.ui.movemoney.transfer;

import static android.view.View.VISIBLE;
import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.model.movemoney.Transfer;
import com.tescobank.mobile.ui.movemoney.MoneyMovementResultOverlayRenderer;

/**
 * Renders result overlays required by the transfer activity
 */
public class TransferResultOverlayRenderer extends MoneyMovementResultOverlayRenderer<Transfer> {

	/**
	 * Constructor
	 * 
	 * @param activity
	 *            the activity
	 */
	public TransferResultOverlayRenderer(Activity activity) {
		super(activity);
	}

	@Override
	protected void renderMoneyMovementSuccessFooter(View overlay, Transfer moneyMovement) {
		if (moneyMovement.isISADestination() && moneyMovement.isNextTaxYear()) {
			TextView resultFooter = (TextView) overlay.findViewById(R.id.result_footer);
			resultFooter.setText(R.string.transfer_result_success_isa_next_year);
			resultFooter.setVisibility(VISIBLE);
		}
	}

	@Override
	protected String getMoveMoneyTypeForDisplay() {
		return getActivity().getString(R.string.movemoney_type_transfer);
	}

}
