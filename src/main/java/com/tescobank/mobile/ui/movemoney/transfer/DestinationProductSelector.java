package com.tescobank.mobile.ui.movemoney.transfer;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectorState.Active;
import static com.tescobank.mobile.ui.movemoney.ValueSelector.ValueSelectorState.Complete;

import java.util.ArrayList;
import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.movemoney.ValueSelector;

/**
 * Supports selecting a single destination product from a list of available destination products
 */
public class DestinationProductSelector extends ValueSelector<ProductDetails> implements OnClickListener {

	public static final String TAG_PREFIX_CONTAINER = "destinationProduct";

	private List<ProductDetails> availableProducts;

	private ViewGroup container;

	private TextView titleView;

	private List<View> productViews;

	/**
	 * Constructor
	 * 
	 * @param sourceProduct
	 *            the source product
	 * @param allProducts
	 *            all products
	 * @param activity
	 *            the activity
	 * @param valueSelectionListener
	 *            the value selection listener
	 */
	public DestinationProductSelector(ProductDetails sourceProduct, List<ProductDetails> allProducts, TescoActivity activity, ValueSelectionListener<ProductDetails> valueSelectionListener) {
		super(activity, valueSelectionListener);

		TransferableProducts transferableProducts = new TransferableProducts();
		transferableProducts.setProducts(allProducts);

		this.availableProducts = transferableProducts.getDestinationProducts(sourceProduct);
		this.container = (ViewGroup) activity.findViewById(R.id.movemoney_product_selection_available_container);
		this.titleView = (TextView) activity.findViewById(R.id.movemoney_product_selector_title);
		this.productViews = new ArrayList<View>();

		configureDestinationProducts();
	}

	@Override
	public void onClick(View view) {
		if (getState() == Complete) {
			getActivity().findViewById(R.id.movemoney_product_selector_layout).performClick();
		} else {
			String tag = (String) view.getTag();
			int index = Integer.valueOf(tag.substring(TAG_PREFIX_CONTAINER.length()));
			setSelectedValue(availableProducts.get(index));
		}
	}

	@Override
	public void setState(ValueSelectorState state) {
		super.setState(state);

		if (container == null) {
			return;
		}

		switch (state) {

		case NotStarted:
		case Active:
			showTitleForNoProductSelected();
			showAllAvailableProducts();
			break;

		case Complete:
			showTitleForProductSelected();
			showSelectedProductOnly();
			break;
		}
	}

	@Override
	public boolean isValid() {
		return getSelectedValue() != null;
	}

	@Override
	protected void setInitialState() {
		setState(Active);
	}

	private void configureDestinationProducts() {

		TescoActivity activity = getActivity();
		LayoutInflater layoutInflater = activity.getLayoutInflater();
		DataFormatter dataFormatter = new DataFormatter();

		for (int i = 0; i < availableProducts.size(); i++) {
			configureDestinationProduct(availableProducts.get(i), i, layoutInflater, dataFormatter);
		}
	}

	private void configureDestinationProduct(ProductDetails product, int index, LayoutInflater layoutInflater, DataFormatter dataFormatter) {

		View productView = layoutInflater.inflate(R.layout.include_movemoney_transfer_product_selector, null);
		productViews.add(productView);

		productView.setTag(TAG_PREFIX_CONTAINER + index);
		productView.setOnClickListener(this);
		productView.setContentDescription(createContentDescription(product, index, dataFormatter));

		TextView name = (TextView) productView.findViewById(R.id.product_name);
		TextView number = (TextView) productView.findViewById(R.id.product_number);
		TextView availableBalanace = (TextView) productView.findViewById(R.id.product_available_balance);

		name.setText(product.getProductName());
		number.setText(dataFormatter.formatSortCode(product.getSortCode()) + "    " + product.getAccountNumber());
		availableBalanace.setText(dataFormatter.formatCurrency(product.getAvailableBalance().abs()));

		LayoutParams layoutParams = new LayoutParams(MATCH_PARENT, WRAP_CONTENT);
		container.addView(productView, layoutParams);
	}

	private void showTitleForNoProductSelected() {
		titleView.setText(R.string.movemoney_destination_selector_prompt);
	}

	private void showTitleForProductSelected() {
		titleView.setText(R.string.movemoney_destination_selector_selected);
	}

	private void showAllAvailableProducts() {
		for (View view : productViews) {
			view.setVisibility(VISIBLE);
		}
	}

	private void showSelectedProductOnly() {

		ProductDetails selectedProduct = getSelectedValue();
		for (int i = 0; i < availableProducts.size(); i++) {

			if (!availableProducts.get(i).equals(selectedProduct)) {
				productViews.get(i).setVisibility(GONE);
			}
		}
	}

	private String createContentDescription(ProductDetails product, int index, DataFormatter dataFormatter) {
		int position = index + 1;
		int count = availableProducts.size();
		String name = product.getProductName();
		String sortCode = dataFormatter.formatSortCode(product.getSortCode());
		String number = product.getAccountNumber();
		String availableBlanace = dataFormatter.formatCurrency(product.getAvailableBalance().abs());
		
		return getActivity().getResources().getString(R.string.transfer_product_selector_product_description, position, count, name, sortCode, number, availableBlanace);
	}
}
