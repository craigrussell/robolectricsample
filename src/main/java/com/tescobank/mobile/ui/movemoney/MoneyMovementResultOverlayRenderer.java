package com.tescobank.mobile.ui.movemoney;

import static android.view.View.VISIBLE;
import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.tescobank.mobile.R;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapper.ErrorResponse;
import com.tescobank.mobile.api.payment.RetrievePaymentMethodsResponse.PaymentMethod;
import com.tescobank.mobile.model.movemoney.MoneyMovement;
import com.tescobank.mobile.ui.overlay.ResultOverlayRenderer;

/**
 * Base class for money movement overlay renderers
 * 
 * @param T
 *            the money movement type
 */
public abstract class MoneyMovementResultOverlayRenderer<T extends MoneyMovement<?, ?>> extends ResultOverlayRenderer {

	/**
	 * Constructor
	 * 
	 * @param activity
	 *            the activity
	 */
	public MoneyMovementResultOverlayRenderer(Activity activity) {
		super(activity);
	}

	/**
	 * Renders a money movement success overlay
	 * 
	 * @param moneyMovement
	 *            the money movement
	 * @param onDismissListener
	 *            the listener called when the overlay is dismissed
	 */
	public void renderMoneyMovementSuccessOverlay(T moneyMovement, OnDismissListener onDismissListener) {

		View overlay = getActivity().getLayoutInflater().inflate(R.layout.movemoney_result_overlay, null);

		TextView resultMessageStart = (TextView)overlay.findViewById(R.id.result_message_start);
		String messageStart = getActivity().getString(R.string.movemoney_result_success_message_start, getMoveMoneyTypeForDisplay().toLowerCase(getActivity().getResources().getConfiguration().locale));
		resultMessageStart.setText(messageStart);
		
		TextView resultAmount = (TextView) overlay.findViewById(R.id.result_amount);
		resultAmount.setText(getDataFormatter().formatCurrency(moneyMovement.getAmount()));

		TextView resultMessageEnd = (TextView) overlay.findViewById(R.id.result_message_end);

		if (moneyMovement.isMoneyMovementDateToday()) {
			resultMessageEnd.setText(R.string.movemoney_result_success_message_end_immediate);
		} else {
			resultMessageEnd.setText(getActivity().getResources().getString(R.string.movemoney_result_success_message_end_future, getDataFormatter().formatDateToFullDate(moneyMovement.getDate())));
		}

		renderMoneyMovementSuccessFooter(overlay, moneyMovement);
		renderOverlay(overlay, false, onDismissListener);
	}

	protected void renderMoneyMovementSuccessFooter(View overlay, T moneyMovement) {
		// Do nothing
	}

	/**
	 * Renders a money movement declined overlay
	 * 
	 * @param moneyMovement
	 *            the money movement
	 * @param errorResponseWrapper
	 *            the error indicating that the money movement was declined
	 */
	public void renderMoneyMovementDeclinedOverlay(T moneyMovement, ErrorResponseWrapper errorResponseWrapper, OnDismissListener onDismissListener) {

		View overlay = getActivity().getLayoutInflater().inflate(R.layout.movemoney_result_overlay, null);

		renderIcon(overlay);
		renderTitle(errorResponseWrapper, overlay);
		renderMessageStart(moneyMovement, overlay);
		renderAmount(moneyMovement, overlay);
		renderMessageEnd(errorResponseWrapper, overlay);
		renderFooter(moneyMovement, errorResponseWrapper, overlay);

		renderOverlay(overlay, false, onDismissListener);
	}

	private void renderIcon(View overlay) {
		ImageView resultIcon = (ImageView) overlay.findViewById(R.id.result_icon);
		resultIcon.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_fail));
	}

	private void renderFooter(T moneyMovement, ErrorResponseWrapper errorResponseWrapper, View overlay) {
		ErrorResponse errorResponse = errorResponseWrapper.getErrorResponse();
		int resultFooterText;
		if (errorResponse.isErrorPaymentTransferGenericDeclined()) {
			resultFooterText = R.string.movemoney_result_decline_message_footer_paymenttransfer;
		} else if (errorResponse.isErrorChannelLimitsBreached()) {
			resultFooterText = R.string.movemoney_result_decline_message_footer_channel_limits_breached;
		} else if (errorResponse.isErrorInsufficientFunds()) {
			resultFooterText = R.string.movemoney_result_decline_message_footer_insufficient_funds;
		} else if (moneyMovement.getSource() instanceof PaymentMethod) {
			resultFooterText = R.string.movemoney_result_decline_message_footer_debit_card_declined;
		} else {
			resultFooterText = R.string.movemoney_result_decline_message_footer_generic_declined;
		}
		TextView resultFooter = (TextView) overlay.findViewById(R.id.result_footer);
		resultFooter.setText(resultFooterText);
		resultFooter.setVisibility(VISIBLE);
	}

	private void renderMessageEnd(ErrorResponseWrapper errorResponseWrapper, View overlay) {
		TextView resultMessageEnd = (TextView) overlay.findViewById(R.id.result_message_end);
		if (errorResponseWrapper.getErrorResponse().isErrorPaymentTransferGenericDeclined()) {
			resultMessageEnd.setText(R.string.movemoney_result_decline_paymenttransfer_message_end);
		} else {
			resultMessageEnd.setText(R.string.movemoney_result_decline_message_end);
		}
	}

	private void renderAmount(T moneyMovement, View overlay) {
		TextView resultAmount = (TextView) overlay.findViewById(R.id.result_amount);
		resultAmount.setText(getDataFormatter().formatCurrency(moneyMovement.getAmount()));
	}

	private void renderMessageStart(T moneyMovement, View overlay) {
		TextView resultMessageStart = (TextView) overlay.findViewById(R.id.result_message_start);

		if (moneyMovement.getSource() instanceof PaymentMethod) {
			resultMessageStart.setText(R.string.movemoney_result_decline_debit_card_message_start);
		} else {
			String type = getMoveMoneyTypeForDisplay().toLowerCase(getActivity().getResources().getConfiguration().locale);
			String message = getActivity().getString(R.string.movemoney_result_decline_message_start, type);
			resultMessageStart.setText(message);
		}
	}

	private void renderTitle(ErrorResponseWrapper errorResponseWrapper, View overlay) {
		TextView resultTitle = (TextView) overlay.findViewById(R.id.result_title);
		if (errorResponseWrapper.getErrorResponse().isErrorPaymentTransferGenericDeclined()) {
			resultTitle.setText(R.string.movemoney_result_decline_title_paymenttransfer);
		} else {
			resultTitle.setText(R.string.movemoney_result_decline_title_generic);
		}
	}
	
	protected abstract String getMoveMoneyTypeForDisplay();
	
}
