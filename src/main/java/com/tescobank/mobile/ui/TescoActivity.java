package com.tescobank.mobile.ui;

import static com.tescobank.mobile.application.TimeoutManager.SAVED_TIME_KEY;
import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_BACK_FROM_RETRY_PRESSED;
import static com.tescobank.mobile.ui.ActivityResultCode.RESULT_RETRY_PRESSED;
import static com.tescobank.mobile.ui.Transitions.applyDefaultTransition;
import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.audit.AuditRequest.OperationName;
import com.tescobank.mobile.api.audit.AuditRequest.Reason;
import com.tescobank.mobile.api.audit.DefaultAuditRequest;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder.InAppError;
import com.tescobank.mobile.application.ActivityFinishSpec;
import com.tescobank.mobile.application.ActivityStartSpec;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.application.TimeoutManager;
import com.tescobank.mobile.flow.Flow;
import com.tescobank.mobile.network.NetworkEvent;
import com.tescobank.mobile.network.NetworkStatusChecker;
import com.tescobank.mobile.properties.TescoApplicationPropertiesReader.ApplicationPropertyKeys;
import com.tescobank.mobile.ui.auth.PreSplashActivity;
import com.tescobank.mobile.ui.errors.DefaultErrorResponseHandler;
import com.tescobank.mobile.ui.errors.ErrorResponseHandler;
import com.tescobank.mobile.ui.settings.SettingsDrawerHelper;
import com.tescobank.mobile.ui.tutorial.TooltipManager;

/**
 * Abstract activity providing defaults such as orientation
 */
public abstract class TescoActivity extends SherlockFragmentActivity {

	private static final String TAG = TescoActivity.class.getSimpleName();
	private TimeoutManager timeoutManager;
	private Flow flow;
	
	private static final boolean DEBUG_LIFECYCLE = BuildConfig.DEBUG;
	public static final int MILLIS_PER_SECOND = 1000;
	private static final int NETWORK_POLL_TIME = 1000;
	public static final String PASSWORD_EXTRA = "d2123dxmx!mdmwmdm!";
	
	private BroadcastReceiver networkConnectionLostReceiver;
	private BroadcastReceiver networkConnectionRegainedReceiver;
	private ErrorResponseWrapperBuilder errorBuilder;
	private InputMethodManager imm;

	/** Multiple things can ask for the UI to be blocked / unblocked so maintain a count rather than a boolean */
	private int userInteractionBlockedCount = 0;

	/** Can be injected for testing. A default one is set in the constructor. */
	private ErrorResponseHandler errorResponseHandler;

	private SettingsDrawerHelper drawer;

	private TooltipManager tooltipManager;
	
	public TescoActivity() {
		this.errorResponseHandler = new DefaultErrorResponseHandler(this);
	}

	/**
	 * Can be used to inject a custom error response handler. Must not be null.
	 * 
	 */
	public void setErrorResponseHandler(ErrorResponseHandler errorResponseHandler) {
		if (null == errorResponseHandler) {
			throw new IllegalArgumentException("errorResponseHandler must not be null");
		}
		this.errorResponseHandler = errorResponseHandler;
	}

	/**
	 * Call this when the user interacts with the applications. This is called by default from the {@link #onTouchEvent(MotionEvent)} method, so if you override that, remember to call this.
	 */
	protected final void onUserActivity() {		
		timeoutManager.onUserInteraction();
	}

	@Override
	protected void onCreate(Bundle savedState) {
		if (DEBUG_LIFECYCLE) {
			Log.d(TAG, "lifecycle: onCreate(" + savedState + ") " + this);
		}

		super.onCreate(savedState);
		restoreSavedState(savedState);
		configureTimeoutManager(savedState);
		configureFlow();
		configureActionBar();
		
		if (!isTestDeployment() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getWindow().setFlags(LayoutParams.FLAG_SECURE, LayoutParams.FLAG_SECURE);
		}

		applyScreenOrientation();
		errorBuilder = new ErrorResponseWrapperBuilder(this);
		drawer = new SettingsDrawerHelper(this);
		imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	}

	private void configureActionBar() {
		if(flow !=null && flow.getActionBarConfig() != null) {
			flow.getActionBarConfig().apply(this, getSupportActionBar());
		}
	}
	
	private void configureTimeoutManager(Bundle savedState) {
		long inactivityTimeout = getApplicationState().getSettings().getApplicationTimeout() * MILLIS_PER_SECOND;
		long backgroundTimeout = getApplicationState().getSettings().getLeavingApplicationTimeout() * MILLIS_PER_SECOND;
		timeoutManager = getApplicationState().createTimeoutManager(this, inactivityTimeout, backgroundTimeout);	
		notifyTimeoutManagerIfRestored(savedState);
	}
	
	private void notifyTimeoutManagerIfRestored(Bundle savedState) {
		if(savedState != null && savedState.containsKey(SAVED_TIME_KEY)) {
			timeoutManager.onActivityRestoredFromSavedState(savedState.getLong(SAVED_TIME_KEY));
		}
	}
	
	public void setTimeoutManager(TimeoutManager timeoutManager) {
		this.timeoutManager = timeoutManager;
	}

	public boolean portraitOnly() {
		return getResources().getBoolean(R.bool.portrait_only);
	}
	

	/**
	 * Sets the screen to correct orientation for the device. Portrait on mobile and native orientation on tablet. Override to change or prevent this behaviour.
	 */
	protected void applyScreenOrientation() {
		if (portraitOnly()) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
		}
	}

	/**
	 * Locks screen so that it cannot be rotated away from its current position.
	 */
	public void lockScreenOrientation() {
		int simpleOrientation = getResources().getConfiguration().orientation;
		int rotation = getWindowManager().getDefaultDisplay().getRotation();
		DetailedScreenOrientation screenOrientation = new DetailedScreenOrientation(simpleOrientation, rotation);
		setRequestedOrientation(screenOrientation.getFullOrientation());
	}

	public void unlockScreenOrientation() {
		applyScreenOrientation();
	}

	@Override
	protected void onStart() {
		if (DEBUG_LIFECYCLE) {
			Log.d(TAG, "lifecycle: onStart() " + this);
		}
		super.onStart();
	}

	@Override
	protected void onResume() {
		if (DEBUG_LIFECYCLE) {
			Log.d(TAG, "lifecycle: onResume() " + this);
		}
		timeoutManager.onActivityResumed();
		getApplicationState().getLocationProvider().startTracking();
		getApplicationState().getAnalyticsTracker().startTracking(this);
		registerNetworkReceivers();
		super.onResume();
		applyTransitionAnimation();
	}

	@Override
	protected void onPause() {
		if (DEBUG_LIFECYCLE) {
			Log.d(TAG, "lifecycle: onPause() " + this);
		}

		if (null != tooltipManager) {
			tooltipManager.hide();
			tooltipManager = null;
		}

		getApplicationState().getAnalyticsTracker().stopTracking();
		getApplicationState().getLocationProvider().stopTracking();
		errorResponseHandler.hidePassiveError();
		unregisterNetworkReceivers();
		timeoutManager.onActivityPaused();
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (DEBUG_LIFECYCLE) {
			Log.d(TAG, "lifecycle: onStop() " + this);
		}
	}

	@Override
	protected void onDestroy() {
		if (DEBUG_LIFECYCLE) {
			Log.d(TAG, "lifecycle: onDestroy() " + this);
		}
		getApplicationState().getRestRequestProcessor().clearLastRequestTypeForRequesterType(getClass());
		timeoutManager.onActivityDestroyed();
		super.onDestroy();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onOptionsItemSelected:" + item);
		}

		if (drawer.isAvailable() && item.getItemId() == android.R.id.home) {
			drawer.toggleDrawer();
			return true;
		}
				
		if(flow != null && item.getItemId() == android.R.id.home) {
			hideKeyboard();
			flow.onPolicyCancelled(this);
			return true;
		}
		
		if(item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void startActivity(Intent intent) {
		timeoutManager.startActivity(new ActivityStartSpec(intent, null));
	}

	@Override
	public void startActivity(Intent intent, Bundle options) {
		timeoutManager.startActivity(new ActivityStartSpec(intent, options));
	}

	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
		timeoutManager.startActivity(new ActivityStartSpec(intent, requestCode, null));
	}

	public void startActivityNow(ActivityStartSpec intentSpec) {
		if (DEBUG_LIFECYCLE) {
			Log.d(TAG, "lifecycle: onStartingActivityNow() " + this);
		}
		 		
		// As all startActivity methods eventually evaluate to super.startActivityForResult(intent, requestCode, options) 
		// we would ideally just call this. However as robolectic 2.2 cannot cope we are forced to use 
		// the following if statement instead.
		if (intentSpec.getOptions() != null) {
			super.startActivity(intentSpec.getIntent(), intentSpec.getOptions());
		} else {
			super.startActivityForResult(intentSpec.getIntent(), intentSpec.getRequestCode());
		}
	}

	/**
	 * Closes the drawer
	 */
	public void closeDrawer() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Closing Drawer");
		}

		drawer.closeDrawer();
	}

	/**
	 * @return whether user interaction is blocked
	 */
	public boolean isUserInteractionBlocked() {
		return userInteractionBlockedCount > 0;
	}

	/**
	 * @param uiBlocked whether user interaction should be blocked
	 */
	public void setUserInteractionBlocked(boolean uiBlocked) {
		if (uiBlocked) {
			userInteractionBlockedCount++;
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Handling request to block UI; block count = " + userInteractionBlockedCount);
			}
		} else {
			userInteractionBlockedCount--;
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Handling request to un-block UI; block count = " + userInteractionBlockedCount);
			}
		}

		if (isUserInteractionBlocked()) {
			hideKeyboard();
		}
	}

	protected void registerNetworkReceivers() {
		// Check current network status
		checkNetworkStatus();

		// Register for future changes
		networkConnectionLostReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				onNetworkConnectionLost();
			}
		};
		registerReceiver(networkConnectionLostReceiver, new IntentFilter(NetworkEvent.CONNECTION_LOST));

		networkConnectionRegainedReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "Received connection regained broadcast");
				}
				onNetworkConnectionRegained();
			}
		};
		registerReceiver(networkConnectionRegainedReceiver, new IntentFilter(NetworkEvent.CONNECTION_REGAINED));
	}

	protected void checkNetworkStatus() {
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				try {
					NetworkStatusChecker checker = new NetworkStatusChecker();
					if (!checker.isAvailable(TescoActivity.this)) {
						onNetworkConnectionLost();
					}
				} catch (Exception e) {
					// Ignore
				}
			}
		}, NETWORK_POLL_TIME);
	}

	protected void unregisterNetworkReceivers() {
		this.unregisterReceiver(networkConnectionLostReceiver);
		this.unregisterReceiver(networkConnectionRegainedReceiver);
	}

	protected void onNetworkConnectionLost() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Network connection down");
		}
		if (getApplicationState().isNetworkConnectionLostMessageDisplayed()) {
			return;
		}
		getApplicationState().setNetworkConnectionLostMessageDisplayed(true);
		errorResponseHandler.hidePassiveError();
		handleErrorResponse(errorBuilder.buildErrorResponse(InAppError.InternetConnectionLost));
	}

	protected void onNetworkConnectionRegained() {
		getApplicationState().setNetworkConnectionLostMessageDisplayed(false);
		errorResponseHandler.hidePassiveError();
		handleErrorResponse(errorBuilder.buildErrorResponse(InAppError.InternetConnectionRegained));
	}

	/**
	 * Utility method that can be used in layout xml to popup the keyboard via an onClick.
	 * 
	 * @param view
	 *            required for the method signature
	 */
	public final void popupKeyboard(View view) {
		showKeyboard();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		onUserActivity();
		if (!isUserInteractionBlocked()) {
			return super.dispatchTouchEvent(event);
		}
		return isUserInteractionBlocked();
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "dispatchKeyEvent: " + event);
		}
		if (!isUserInteractionBlocked() || event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
			return super.dispatchKeyEvent(event);
		}
		return isUserInteractionBlocked();
	}

	/**
	 * @return the application state
	 */
	public ApplicationState getApplicationState() {
		return (ApplicationState) getApplication();
	}

	/**
	 * Handles the given error
	 * 
	 * @param errorResponse
	 */
	public final void handleErrorResponse(ErrorResponseWrapper errorResponse) {
		if (errorResponseHandler.handleErrorResponse(errorResponse)) {
			onRetryForPassiveError();
		}
	}
	/**
	 * @return whether pressing back on a retry error should be interpreted as a retry
	 */
	public boolean backFromRetryErrorShouldRetry() {
		return true;
	}
	
	/**
	 * Called when the user clicks the retry button when a retry error has been displayed.
	 */
	protected void onRetryPressed() {
	}
	
	@Override
	public void onBackPressed() {
		
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Handling Back Pressed");
		}
		
		if(flow != null) {
			flow.onPolicyCancelled(this);
		} else {
			super.onBackPressed();
		}
	}
	
	/**
	 * Called when the user clicks back when a retry error has been displayed.
	 */
	protected void onBackFromRetryPressed() {
		finishWithResult(RESULT_BACK_FROM_RETRY_PRESSED);
	}
	
	/**
	 * Called when a passive error is displayed.
	 */
	protected void onRetryForPassiveError() {
	}

	@Override
	public void finish() {
		timeoutManager.finishActivity(new ActivityFinishSpec());
	}

	public void finishWithResult(int resultCode) {
		timeoutManager.finishActivity(new ActivityFinishSpec(resultCode, null));
	}

	public void finishWithResult(int resultCode, Intent data) {
		if (DEBUG_LIFECYCLE) {
			Log.d(TAG, "lifecycle: finishWithResult(" + resultCode + ", " + data + ") " + this);
		}
		timeoutManager.finishActivity(new ActivityFinishSpec(resultCode, data));
	}

	public void finishActivityNow(ActivityFinishSpec finishSpec) {

		if (DEBUG_LIFECYCLE) {
			Log.d(TAG, "lifecycle: onFinishActivityNow() " + this);
		}

		if (finishSpec.hasResult()) {
			setResult(finishSpec.getResultCode(), finishSpec.getData());
		}

		super.finish();
	}

	public void showKeyboard() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Show Keyboard");
		}

		if (!isUserInteractionBlocked()) {
			imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 1);
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		}
	}

	public void hideKeyboard() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Hide Keyboard");
		}
		imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getWindowToken(), 0);
	}

	public void audit(OperationName operationName, Reason reason) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Auditing: OperationName = " + operationName + ", Reason = " + reason);
		}
		DefaultAuditRequest request = new DefaultAuditRequest();
		request.setTbID(getApplicationState().getTbId());
		request.setDeviceID(getApplicationState().getDeviceId());
		request.setOperationName(operationName);
		request.setReason(reason);
		getApplicationState().getRestRequestProcessor().processRequest(request);
	}

	protected ErrorResponseWrapperBuilder getErrorBuilder() {
		return errorBuilder;
	}
	
	protected SettingsDrawerHelper getDrawer() {
		return drawer;
	}

	public TooltipManager getTooltipManager() {
		if (null == tooltipManager) {
			tooltipManager = new TooltipManager(this, null);
		}
		return tooltipManager;
	}

	/**
	 * Overrides the pending transition with the new default. You can either override this or call {@link #overridePendingTransition(int, int)} directly.
	 */
	protected void applyTransitionAnimation() {

		if(getFlow() != null) {
			getFlow().applyAnimation(this);
		} else {
			applyDefaultTransition(this);
		}	
	}
	
	/**
	 * Switch animateLayoutChangeson on for the viewGroup if it is a JellyBean or higher device.
	 * Useful in cases where layout animations cause rendering issues e.g failing to display the final layout
	 * on low end devices.
	 */
	@SuppressLint("NewApi")
	public void enableLayoutAnimationsOnPostJellyBeanDevices(ViewGroup viewGroup) {
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			viewGroup.setLayoutTransition(new LayoutTransition());
		}
	}

	public boolean isTestDeployment() {
		return Boolean.parseBoolean(getApplicationState().getApplicationPropertyValueForKey(ApplicationPropertyKeys.TEST_DEPLOYMENT));
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		saveFlow(outState, flow);
		saveBackgroundTime(outState);
	}
	
	private void saveBackgroundTime(Bundle outState) {
		if(timeoutManager.isAppInBackground()) {
			outState.putLong(SAVED_TIME_KEY, System.currentTimeMillis());
		}
	}

	private void saveFlow(Bundle outState, Flow flow) {
		outState.putParcelable(Flow.class.getName(), flow);
	}
	
	private void restoreSavedState(Bundle savedState) {
		if(savedState == null) {
			return;
		}
		restoreFlow(savedState);
	}

	private void restoreFlow(Bundle savedBundle) {
		flow = Flow.class.cast(savedBundle.getParcelable(Flow.class.getName()));
	}
	
	public Flow getFlow() {
		return flow;
	}
	
	private void configureFlow() {
		if(flow == null) {
			flow = getFlowExtra();
		}
	}
	
	private Flow getFlowExtra() {
		if(getIntent() != null && getIntent().getExtras() != null) {
			return Flow.class.cast(getIntent().getExtras().get(Flow.class.getName()));
		}
		return null;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (resultCode == RESULT_RETRY_PRESSED) {
			onRetryPressed();
			return;
		}
		
		if (resultCode == RESULT_BACK_FROM_RETRY_PRESSED) {
			onBackFromRetryPressed();
			return;
		}
	
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	public void logOut() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Logging out");
		}
		Intent preSplashActivity = new Intent(this, PreSplashActivity.class);
		preSplashActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(preSplashActivity);
		ActivityCompat.finishAffinity(this);
	}
}
