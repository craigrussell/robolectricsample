package com.tescobank.mobile.ui.balancepeek.inactivity;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.util.Log;

public class TimeoutCounter implements Counter {
	private ScheduledExecutorService executor;

	private long timeoutDelay;
	private boolean isRunning;

	private InactivtyCounterCallback callback;

	public static TimeoutCounter create(InactivtyCounterCallback callback, long timeoutDelay) {
		return new TimeoutCounter(callback, timeoutDelay);
	}

	TimeoutCounter(final InactivtyCounterCallback callback, long timeoutDelay) {
		this.timeoutDelay = timeoutDelay;
		this.callback = callback;
	}

	private Runnable getTask() {
		return new Runnable() {
			@Override
			public void run() {
				if (!executor.isShutdown()) {
					callback.onTimeoutReached();
				}
			}
		};
	}

	@Override
	public void resetCounter() {
		if (executor != null && isRunning) {
			cancelRunningTask();
			submitTimerTaskToExecutor();
		}
	}

	@Override
	public void start() {
		cancelRunningTask();
		submitTimerTaskToExecutor();
	}

	@Override
	public void stop() {
		if (executor != null) {
			executor.shutdownNow();
		}
		isRunning = false;
	}

	@Override
	public boolean isRunning() {
		return isRunning;
	}

	@Override
	public long getInactivityTimeoutDuration() {
		return timeoutDelay;
	}

	private void cancelRunningTask() {
		if (executor != null) {
			executor.shutdownNow();
		}
	}

	private void submitTimerTaskToExecutor() {
		executor = getExecutor();
		Runnable task = getTask();
		executor.schedule(task, timeoutDelay, TimeUnit.MILLISECONDS);
		isRunning = true;
	}

	private ScheduledExecutorService getExecutor() {
		if (executor == null || executor.isShutdown()) {
			Log.i("", "Getting new Executor");
			executor = Executors.newScheduledThreadPool(1);
		}
		return executor;
	}

}
