package com.tescobank.mobile.ui.balancepeek;

import java.util.List;

import android.app.Activity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewPropertyAnimator;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.balancepeek.DragDirectionCalculator.DragDirection;
import com.tescobank.mobile.ui.balancepeek.inactivity.Counter;
import com.tescobank.mobile.ui.balancepeek.inactivity.InactivtyCounterCallback;
import com.tescobank.mobile.ui.balancepeek.inactivity.TimeoutCounterFactory;

public class BalancePeekSlidingPanelTouchListener implements OnTouchListener {

	private static final String TAG = BalancePeekSlidingPanelTouchListener.class.getSimpleName();

	private static final int FLICK_DISTANCE_THRESHOLD = 50;
	private static final long FLICK_TIME_THRESHOLD = 300;

	public static long ANIMATION_DURATION_NONE = 0;
	public static long ANIMATION_DURATION_DEFAULT = 300;

	private SlidingPanelPositioner panelPositioner;

	public long animationDuration;
	private float yDelta;
	private final float origin;
	private final float originOffset;

	private SlidingPanelUIWrapper slidingUIWrapper;
	private Counter inactivityTimeoutCounter;

	public View draggingIndicator;

	private long timeWhenSliderLastChangedDirection;
	private int lastRecordedY;
	private DirectionCalculator directionCalculator;

	private TescoActivity activity;
	private boolean viewAnalyticTracked;

	public static BalancePeekSlidingPanelTouchListener create(TescoActivity activity, SlidingPanelUIWrapper slidingUIWrapper, SlidingPanelPositioner panelPositioner, float origin, float originOffset) {
		BalancePeekSlidingPanelTouchListener listener = new BalancePeekSlidingPanelTouchListener(activity, slidingUIWrapper, origin, originOffset);
		listener.panelPositioner = panelPositioner;
		listener.directionCalculator = new DragDirectionCalculator();
		return listener;
	}

	BalancePeekSlidingPanelTouchListener(final TescoActivity activity, SlidingPanelUIWrapper slidingUIWrapper, float origin, float originOffset) {
		this.activity = activity;
		this.slidingUIWrapper = slidingUIWrapper;
		this.origin = origin;
		this.originOffset = originOffset;
		animationDuration = ANIMATION_DURATION_DEFAULT;
		viewAnalyticTracked = false;
		configureInactivityTimeout(activity, 0l);
	}

	public void configureInactivityTimeout(final Activity activity, final long inactivityTimeout) {
		InactivtyCounterCallback callback = getInactivityCallback(activity);
		inactivityTimeoutCounter = TimeoutCounterFactory.getCounter(callback, inactivityTimeout);
	}

	private InactivtyCounterCallback getInactivityCallback(final Activity activity) {
		return new InactivtyCounterCallback() {
			@Override
			public void onTimeoutReached() {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (BuildConfig.DEBUG) {
							Log.i(TAG, "Closing panels due to inactivity");
						}
						closePanels(animationDuration);
					}
				});
			}
		};
	}

	public void userInteractionOccurred() {
		resetInactivityClock();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		final int y = (int) event.getRawY();

		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN:
			hideSoftKeyboardIfShowing();
			handleTouchDownEvent(y);
			return true;
		case MotionEvent.ACTION_UP:
			handleTouchUpEvent(y);
			return true;
		case MotionEvent.ACTION_MOVE:
			handleTouchMoveEvent(y);
			return true;
		}
		return false;
	}

	private void hideSoftKeyboardIfShowing() {
		 activity.hideKeyboard();
	}

	private void resetInactivityClock() {
		if (inactivityTimeoutCounter != null) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Resetting balance peek inactivity counter due to interaction");
			}
			inactivityTimeoutCounter.resetCounter();
		}
	}

	public void handleTouchDownEvent(int y) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "User has touched down on slider at " + y);
			Log.d(TAG, "Stopping inactivity timer whilst user touching the sliding panel");
		}

		inactivityTimeoutCounter.stop();

		lastRecordedY = y;
		timeWhenSliderLastChangedDirection = System.currentTimeMillis();

		changeStateDraggingIndicator(true);

		yDelta = y - slidingUIWrapper.getPanelBottom().getY();

		if (BuildConfig.DEBUG) {
			Log.v(TAG, String.format("Delta calculated to be %f {%d - %f}", yDelta, y, slidingUIWrapper.getPanelBottom().getY()));
		}
	}

	private void changeStateDraggingIndicator(boolean userIsTouchingPanel) {
		if (draggingIndicator == null) {
			return;
		}
		draggingIndicator.setSelected(userIsTouchingPanel);
	}

	public void handleTouchUpEvent(int y) {
		inactivityTimeoutCounter.start();

		if (BuildConfig.DEBUG) {
			Log.i(TAG, "User has released their touch from the slider");
		}
		changeStateDraggingIndicator(false);

		if (flickGestureOccurred(y)) {
			if (BuildConfig.DEBUG) {
				Log.i(TAG, "Flick gesture detected");
			}

			DragDirection direction = directionCalculator.calculateDirection(lastRecordedY, y);
			handleFlickGesture(direction);
			return;
		}

		boolean shouldOpenPanels = panelPositioner.shouldOpenPanels();

		if (shouldOpenPanels) {
			if (BuildConfig.DEBUG) {
				Log.i(TAG, "Panel released nearer to open position, fully opening");
			}
			openPanels(animationDuration);
		}
		else {
			if (BuildConfig.DEBUG) {
				Log.i(TAG, "Panel released nearer to close position, fully closing");
			}
			closePanels(animationDuration);
		}
	}

	private void handleFlickGesture(DragDirection direction) {
		if(direction == DragDirection.DOWN) {
			openPanels(animationDuration);
		}
		else if(direction == DragDirection.UP) {
			closePanels(animationDuration);
		}
	}

	private boolean flickGestureOccurred(int y) {
		long timeNow = System.currentTimeMillis();
		int distanceDifference = Math.abs(y - lastRecordedY);
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Calculated absolute y difference of " + distanceDifference);
		}

		long timeDifference = timeNow - timeWhenSliderLastChangedDirection;

		if (distanceDifference >= FLICK_DISTANCE_THRESHOLD && timeDifference <= FLICK_TIME_THRESHOLD) {
			return true;
		}
		return false;
	}

	public void handleTouchMoveEvent(int y) {
		DragDirection direction = directionCalculator.calculateDirection(lastRecordedY, y);
		if(direction == DragDirection.NO_DIRECTION) {
			lastRecordedY = y;
			timeWhenSliderLastChangedDirection = System.currentTimeMillis();
		}

		float newPosition = y - yDelta;
		float offset = newPosition - slidingUIWrapper.getPanelBottom().getY();

		if (BuildConfig.DEBUG) {
			Log.v(TAG, String.format("Drag location is %d, which is a change of %f from previous, resulting in a new position of %f", y, yDelta, newPosition));
		}

		float proposedPanelBTop = slidingUIWrapper.getPanelBottom().getY() + offset;

		if (panelPositioner.userDraggedSliderAboveAccountList(newPosition)) {
			closePanels(ANIMATION_DURATION_NONE);
		} else if (panelPositioner.userDraggedSliderBelowAccountList(proposedPanelBTop)) {
			openPanels(ANIMATION_DURATION_NONE);
		}
		else {
			float movementFactor = panelPositioner.calculateMovementFactor();
			moveViewByAmount(slidingUIWrapper.getPanelBottom(), offset, 0);
			moveViewByAmount(slidingUIWrapper.getPanelTop(), -(offset * movementFactor), 0);
		}
		slidingUIWrapper.getRootView().invalidate();
	}

	public void openPanels(long duration) {
		openPanelA(duration);
		openPanelB(duration);

		if (inactivityTimeoutCounter != null) {
			inactivityTimeoutCounter.resetCounter();
		}
		trackAnalytic();
	}

	private void trackAnalytic() {
		if(!viewAnalyticTracked) {
			activity.getApplicationState().getAnalyticsTracker().trackViewBalancePeek(getBalancePeekEnabledCount());
			viewAnalyticTracked = true;
		}
	}

	private int getBalancePeekEnabledCount() {
		List<ProductDetails> productDetails = activity.getApplicationState().getProductDetails();
		int count = productDetails != null ? productDetails.size() : 0;
		return count;
	}

	public void closePanels(long duration) {
		closePanelA(duration);
		closePanelB(duration);

		if (inactivityTimeoutCounter != null) {
			inactivityTimeoutCounter.stop();
		}
	}

	public boolean panelsAreOpen() {
		if (slidingUIWrapper.getPanelBottom().getY() + originOffset <= origin) {
			return false;
		}
		return true;
	}

	private void openPanelA(long duration) {
		float panelABottom = slidingUIWrapper.getPanelTop().getY() + slidingUIWrapper.getPanelTop().getHeight();
		float desiredPanelABottom = slidingUIWrapper.getPanelConcealed().getY() + originOffset * 2;
		float panelATranslationDistance = panelABottom - desiredPanelABottom;
		moveViewByAmount(slidingUIWrapper.getPanelTop(), -panelATranslationDistance, duration);
	}

	private void closePanelA(long duration) {
		float panelABottom = slidingUIWrapper.getPanelTop().getY() + slidingUIWrapper.getPanelTop().getHeight();
		float desiredPanelABottom = origin + originOffset;
		float panelATranslationDistance = desiredPanelABottom - panelABottom;
		moveViewByAmount(slidingUIWrapper.getPanelTop(), panelATranslationDistance, duration);
	}

	private void openPanelB(long duration) {
		float desiredNewPanelBTop = slidingUIWrapper.getPanelConcealed().getY() + slidingUIWrapper.getPanelConcealed().getHeight() - originOffset * 2;
		float panelBTranslationDistance = Math.abs(desiredNewPanelBTop - slidingUIWrapper.getPanelBottom().getY());
		moveViewByAmount(slidingUIWrapper.getPanelBottom(), panelBTranslationDistance, duration);
	}

	private void closePanelB(long duration) {
		float desiredNewPanelBTop = origin - originOffset;
		float panelBTranslationDistance = Math.abs(desiredNewPanelBTop - slidingUIWrapper.getPanelBottom().getY());
		moveViewByAmount(slidingUIWrapper.getPanelBottom(), -panelBTranslationDistance, duration);
	}

	private void moveViewByAmount(View view, float yOffset, long animationDuration) {
		ViewPropertyAnimator animator = view.animate();
		animator.setDuration(animationDuration);
		animator.translationYBy(yOffset);
		animator.start();
	}

	public float getYDelta() {
		return yDelta;
	}

	public void setAnimationDuration(long animationDuration) {
		this.animationDuration = animationDuration;
	}

	public void setPanelPositioner(SlidingPanelPositioner panelPositioner) {
		this.panelPositioner = panelPositioner;
	}

	public void setDraggingIndicator(View draggingIndicatorView) {
		this.draggingIndicator = draggingIndicatorView;
	}

	public void setDirectionCalculator(DirectionCalculator directionCalculator) {
		this.directionCalculator = directionCalculator;
	}

	public void setInactivityTimeoutCounter(Counter inactivityTimeoutCounter) {
		this.inactivityTimeoutCounter = inactivityTimeoutCounter;
	}

}
