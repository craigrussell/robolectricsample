package com.tescobank.mobile.ui.balancepeek;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.Switch;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.api.Display;
import com.tescobank.mobile.api.balancepeek.ChangeBalancePeekEnabledAccountsRequest;
import com.tescobank.mobile.api.balancepeek.ChangeBalancePeekEnabledAccountsRequestBody;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.success.SuccessResponseWrapper;
import com.tescobank.mobile.encode.HMACSHA256Encoder;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.InProgressIndicator;
import com.tescobank.mobile.ui.InProgressIndicator.ContinueButtonConfiguration;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.TescoAlertDialog;
import com.tescobank.mobile.ui.errors.ErrorHandler;
import com.tescobank.mobile.ui.overlay.SuccessResultOverlayRenderer;

public class BalancePeekSetUpActivity extends TescoActivity implements ContinueButtonConfiguration, ErrorHandler {

	private static final String TAG = BalancePeekSetUpActivity.class.getSimpleName();
	private static final String BUNDLE_KEY_CONFIRMATION_DIALOG_SHOWING = "BUNDLE_KEY_CONFIRMATION_DIALOG";

	private InProgressIndicator inProgressIndicator;
	private LinearLayout balancePeekAccountList;
	private Switch balancePeekSwitch;
	private View translucentBalancePeekView;
	private boolean balancePeekEnabled;
	private SuccessResultOverlayRenderer successOverlay;
	private List<ProductDetails> products;
	private AlertDialog confirmationDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		ActionBarConfiguration.setToStdUp(this, getSupportActionBar());
		setContentView(R.layout.activity_balance_peek_setup);
		
		translucentBalancePeekView = findViewById(R.id.balance_peek_translucent_view);
		balancePeekSwitch = Switch.class.cast(findViewById(R.id.balance_peek_switch));
		
		configureProductDataForBalancePeek();
		configureBalancePeekSwitch();
		configureBalancePeekAccountList();
		configureInProgressIndicator();
		restoreSavedState(savedInstanceState);
		trackViewAnalytic();
	}
	
	private void trackViewAnalytic() {
		getApplicationState().getAnalyticsTracker().trackViewBalancePeekSetup();
	}
	
	private void trackPreferencesSubmittedAnalytic() {
		if(balancePeekEnabled) {
			getApplicationState().getAnalyticsTracker().trackBalancePeekEnabledSubmitted(getBalancePeekEnabledProductNames());	
		} else {
			getApplicationState().getAnalyticsTracker().trackBalancePeekDisabledSubmitted();
		}
	}
	
	private void trackPreferenceSubmissionCompletedAnalytic(boolean success) {
		getApplicationState().getAnalyticsTracker().trackBalancePeekSubmitPreferencesProcessCompleted(success);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean(BUNDLE_KEY_CONFIRMATION_DIALOG_SHOWING, isConfirmationDialogShowing());
	}
	
	private void restoreSavedState(Bundle savedInstanceState) {
		if (savedInstanceState == null) {
			return;
		}
			
		if(savedInstanceState.getBoolean(BUNDLE_KEY_CONFIRMATION_DIALOG_SHOWING)) {
			confirmFormSubmission();
		}
	}

	@Override
	public void onBackPressed() {
		dismissSuccessOverlay();
		super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		dismissSuccessOverlay();
		dismissConfirmationDialog();
		super.onDestroy();
	}
	
	private void configureProductDataForBalancePeek() {
		products = getApplicationState().getProductDetails();
	}

	private void configureBalancePeekSwitch() {
		balancePeekEnabled = getApplicationState().getPersister().isBalancePeekEnabled();
		balancePeekSwitch.setChecked(balancePeekEnabled);
		translucentBalancePeekView.setVisibility(balancePeekEnabled ? View.GONE : View.VISIBLE);
		balancePeekSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				onBalancePeekSwitchStatusChanged(isChecked);
			}
		});
	}

	private void onBalancePeekSwitchStatusChanged(boolean isChecked) {
		balancePeekEnabled = isChecked;
		translucentBalancePeekView.setVisibility(isChecked ? View.GONE : View.VISIBLE);
		enableBalancePeekAccountCheckBoxes(isChecked ? true : false);
		refreshInProgressIndicator();
	}

	private void configureBalancePeekAccountList() {
		boolean hasBalancePeekEnabled = getApplicationState().getPersister().isBalancePeekEnabled();
		boolean isNonInteractiveProductsEnabled = getApplicationState().getFeatures().isNonInteractiveProductsEnabled();
		balancePeekAccountList = LinearLayout.class.cast(findViewById(R.id.balance_peek_setup_account_list));
		BalancePeekListRowBuilder builder = new BalancePeekListRowBuilder(getResources(), getLayoutInflater(), products, R.layout.balance_peek_setup_row, isNonInteractiveProductsEnabled, hasBalancePeekEnabled);
		for (int i = 0; i < builder.getCount(); i++) {
			View view = builder.createView(i);
			balancePeekAccountList.addView(view);
		}

		enableBalancePeekAccountCheckBoxes(balancePeekEnabled);
		configureBalancePeekCheckboxStatusFromSharedPreferences();
	}

	private void enableBalancePeekAccountCheckBoxes(boolean enabled) {
		for (int i = 0; i < products.size(); i++) {
			balancePeekAccountList.getChildAt(i).findViewById(R.id.balancePeek_checkbox).setEnabled(enabled);
		}
	}

	private void configureBalancePeekCheckboxStatusFromSharedPreferences() {
		Set<String> productIdentifiers = getApplicationState().getPersister().getBalancePeekProductIdentifiers();
		
		for (int i = 0; i < products.size(); i++) {
			CheckBox balancePeekAccountCheckbox = CheckBox.class.cast(balancePeekAccountList.getChildAt(i).findViewById(R.id.balancePeek_checkbox));
			if(productIdentifiers != null && productIdentifiers.contains(HMACSHA256Encoder.encode(products.get(i).getProductId(), getApplicationState().getPersister().getCssoId()))) {
				balancePeekAccountCheckbox.setChecked(true);
			} else {
				balancePeekAccountCheckbox.setChecked(false);
			}		
		}
	}
	
	
	
	public void doBalancePeekAccountSelected(View view) {
		if(!balancePeekEnabled) {
			return;
		}
		
		CheckBox checkBox = CheckBox.class.cast(view.findViewById(R.id.balancePeek_checkbox));
		if(checkBox.isChecked()) {
			checkBox.setChecked(false);
		} else {
			checkBox.setChecked(true);
		}
				
		refreshInProgressIndicator();
	}
	
	public void doBalancePeekAccountSelectedCheckBoxClicked(View view) {
		if(!balancePeekEnabled) {
			return;
		}
		
		refreshInProgressIndicator();
	}

	public void onContinuePressed(View v) {
		if(balancePeekEnabled) {
			confirmFormSubmission();
			return;
		}
		submitRequest();
	}

	private void confirmFormSubmission() {		
		TescoAlertDialog.Builder builder = new TescoAlertDialog.Builder(this);
		builder.setTitle(R.string.confirm_balance_peek_setup_title);
		builder.setMessage(R.string.confirm_balance_peek_setup_message);
		builder.setNegativeButton(R.string.dialog_confirm_cancel, null);
		builder.setPositiveButton(R.string.dialog_confirm_ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				submitRequest();
			}
		});
		confirmationDialog = builder.show();
	}
	
	private void submitRequest() {
		trackPreferencesSubmittedAnalytic();
		inProgressIndicator.showInProgressIndicator();
		ChangeBalancePeekEnabledAccountsRequestBody body = buildRequestBody();
		ChangeBalancePeekEnabledAccountsRequest request = buildRequest(body);
		getApplicationState().getRestRequestProcessor().processRequest(request);
	}

	private ChangeBalancePeekEnabledAccountsRequestBody buildRequestBody() {
		ChangeBalancePeekEnabledAccountsRequestBody requestBody = new ChangeBalancePeekEnabledAccountsRequestBody();
		buildBalancePeekListOfSelectedProductIds(requestBody);
		return requestBody;
	}

	private ChangeBalancePeekEnabledAccountsRequest buildRequest(ChangeBalancePeekEnabledAccountsRequestBody requestBody) {
		return new ChangeBalancePeekRegisterAccountsRequest(requestBody);
	}

	private void buildBalancePeekListOfSelectedProductIds(ChangeBalancePeekEnabledAccountsRequestBody requestBody) {
		if(!balancePeekEnabled) {
			return;
		}
		
		for (int i = 0; i < products.size(); i++) {
			if (isBalancePeekEnabledForProductAtIndex(i)) {
				requestBody.addTokenId(products.get(i).getProductId());
			}
		}
	}

	private void onError(ErrorResponseWrapper errorResponse) {
		trackPreferenceSubmissionCompletedAnalytic(false);
		inProgressIndicator.hideInProgressIndicator();
		handleErrorResponse(errorResponse);
	}

	private void onSuccess(SuccessResponseWrapper successResponseWrapper) {
		trackPreferenceSubmissionCompletedAnalytic(true);
		inProgressIndicator.hideAll();
		removeProductIdentifersFromSharedPreferences();
		saveBalancePeekSharedPreferences();
		displaySuccessOverlay(successResponseWrapper.getSuccessResponse().getDisplay());
	}
	
	private void saveBalancePeekSharedPreferences() {
		getApplicationState().enableBalancePeek(balancePeekEnabled);		
		Set<String> productIdentifers = new HashSet<String>();
		for (ProductDetails product : getBalancePeekEnabledProducts()) {
			productIdentifers.add(HMACSHA256Encoder.encode(product.getProductId(), getApplicationState().getPersister().getCssoId()));
		}
		getApplicationState().getPersister().persistBalancePeekProductIdentifiers(productIdentifers);
	}
	
	private List<String> getBalancePeekEnabledProductNames() {
		List<String> productDetails = new ArrayList<String>();
		for (ProductDetails product : getBalancePeekEnabledProducts()) {
			productDetails.add(product.getProductName());
		}
		return productDetails;
	}
	
	private List<ProductDetails> getBalancePeekEnabledProducts() {
		List<ProductDetails> enabledProducts = new ArrayList<ProductDetails>();
		for (int i = 0; i < products.size(); i++) {
			if(isBalancePeekEnabledForProductAtIndex(i)) {
				enabledProducts.add(products.get(i));
			}	
		}
		return enabledProducts;
	}

	private boolean isBalancePeekEnabledForProductAtIndex(int index) {
		return CheckBox.class.cast(balancePeekAccountList.getChildAt(index).findViewById(R.id.balancePeek_checkbox)).isChecked();
	}

	private void removeProductIdentifersFromSharedPreferences() {
		getApplicationState().getPersister().removeBalancePeekProductIdentifiers();
	}

	private void displaySuccessOverlay(Display display) {
		successOverlay = new SuccessResultOverlayRenderer(this, display);
		successOverlay.renderOverlay(new OnDismissListener() {
			@Override
			public void onDismiss() {
				onDismissSuccessOverlay();
			}
		});
	}

	private void onDismissSuccessOverlay() {
		getFlow().onPolicyComplete(this);
	}

	private void dismissSuccessOverlay() {
		if (successOverlay != null) {
			successOverlay.dismissOverlay();
		}
	}
	
	private void dismissConfirmationDialog() {
		if(isConfirmationDialogShowing()) {
			confirmationDialog.dismiss();
		}
	}

	private boolean isConfirmationDialogShowing() {
		return confirmationDialog != null && confirmationDialog.isShowing();
	}

	private void configureInProgressIndicator() {
		inProgressIndicator = new InProgressIndicator(this, true);
		inProgressIndicator.updateContinueButtonConfiguration(this);
		inProgressIndicator.setContinueEnabled(false);
	}

	private void refreshInProgressIndicator() {

		inProgressIndicator.setContinueEnabled(true);
	}

	@Override
	public void handleError(ErrorResponseWrapper errorResponseWrapper) {
		handleErrorResponse(errorResponseWrapper);
	}

	@Override
	public int getMessageId() {
		return R.string.balance_peek_save_button_text;
	}

	@Override
	public int getBackgroundDrawable() {
		return R.drawable.button_default_background_selector;
	}

	@Override
	public int getTextColors() {
		return R.color.button_default_text_selector;
	}

	private class ChangeBalancePeekRegisterAccountsRequest extends ChangeBalancePeekEnabledAccountsRequest {
		public ChangeBalancePeekRegisterAccountsRequest(ChangeBalancePeekEnabledAccountsRequestBody bodyObject) {
			super(bodyObject);
		}

		@Override
		public void onResponse(SuccessResponseWrapper response) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Successfully received response ChangeBalancePeekRegisterAccountsRequest");
			}
			onSuccess(response);
		}

		@Override
		public void onErrorResponse(ErrorResponseWrapper errorResponse) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Failed receiving response ChangeBalancePeekRegisterAccountsRequest: " + errorResponse.getErrorResponse().toString());
			}
			onError(errorResponse);
		}
	}
}
