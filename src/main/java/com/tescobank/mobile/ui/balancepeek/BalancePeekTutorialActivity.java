package com.tescobank.mobile.ui.balancepeek;

import android.os.Bundle;
import android.view.View;

import com.tescobank.mobile.R;
import com.tescobank.mobile.flow.Flow;
import com.tescobank.mobile.ui.TescoActivity;

public class BalancePeekTutorialActivity extends TescoActivity {
	
	private static final String ANALYTIC_TUTORIAL_BALANCE_PEEK = "Tutorial-BalancePeek";

	@Override
	protected void onCreate(Bundle savedState) {
		super.onCreate(savedState);
		setContentView(R.layout.activity_balance_peek_tutorial);
		trackAnalytic();
	}
	
	private void trackAnalytic() {
		getApplicationState().getAnalyticsTracker().trackViewTutorialScreen(ANALYTIC_TUTORIAL_BALANCE_PEEK);
	}
	
	public void onGetStartedPressed(View view) {
		Flow flow = BalancePeekFlowFactory.createBalancePeekSetUpFlow();
		flow.begin(this);
		finish();
	}
	
	public void onNoThanksPressed(View view) {
		finish();
	}
}
