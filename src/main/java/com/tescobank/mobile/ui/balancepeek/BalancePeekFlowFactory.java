package com.tescobank.mobile.ui.balancepeek;

import static com.tescobank.mobile.flow.Flow.StackBehaviour.KEEP_ALIVE;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;

import com.tescobank.mobile.flow.Flow;
import com.tescobank.mobile.flow.Policy;
import com.tescobank.mobile.flow.TescoFlow;
import com.tescobank.mobile.flow.TescoPolicy;
import com.tescobank.mobile.flow.TescoPolicyInventory;
import com.tescobank.mobile.ui.ActionBarConfiguration;
import com.tescobank.mobile.ui.ActionBarConfiguration.Style;
import com.tescobank.mobile.ui.ActionBarConfiguration.Type;

public class BalancePeekFlowFactory {
	public static Flow createBalancePeekSetUpFlow() {
		ActionBarConfiguration config = new ActionBarConfiguration(Style.AUTHENTICATED, Type.UP);
		List<Policy> policies = new ArrayList<Policy>();
		policies.add(new TescoPolicy.Builder(BalancePeekSetUpActivity.class)
			.setActionBarConfig(config)
			.create());
		TescoFlow flow = new TescoFlow(new Bundle(), new TescoPolicyInventory(policies));
		flow.setInitialStackBehaviour(KEEP_ALIVE);
		return flow;
	}
}
