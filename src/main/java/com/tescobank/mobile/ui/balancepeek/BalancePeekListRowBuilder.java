package com.tescobank.mobile.ui.balancepeek;

import java.util.List;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tescobank.mobile.R;
import com.tescobank.mobile.model.product.ProductDetails;

public class BalancePeekListRowBuilder {
	private static final int MARGIN = 4;
	private List<ProductDetails> products;
	private LayoutInflater inflater;
	private Resources resources;
	private ProductDetails product;
	private int layoutToBeInflated;
	private boolean isNonInteractiveProductsEnabled;
	private boolean isBalancePeekEnabled;
	private int position;
	
	public BalancePeekListRowBuilder(Resources resources, LayoutInflater inflater, List<ProductDetails> products, int layoutToBeInflated, boolean isNonInteractiveProductsEnabled, boolean isBalancePeekEnabled) {
		this.resources = resources;
		this.inflater = inflater;
		this.products = products;
		this.layoutToBeInflated = layoutToBeInflated;
		this.isNonInteractiveProductsEnabled = isNonInteractiveProductsEnabled;
		this.isBalancePeekEnabled = isBalancePeekEnabled;
	}

	public int getCount() {
		return hasProducts() ? products.size() : 1;
	}

	public View createView(int position) {	
		this.position = position;
		
		if(hasProducts()) {
			product = products.get(position);
		}

		return createRowView();
	}

	public boolean hasProducts() {
		return products != null && !products.isEmpty();
	}
	
	private View createRowView() {
		View content = inflateProductListRow(inflater, layoutToBeInflated);
		BalancePeekListRowContentPopulator balancePeekListRowContentPopulator = new BalancePeekListRowContentPopulator(content, (ImageView)content.findViewById(R.id.balancePeek_image), product, resources, isNonInteractiveProductsEnabled, isBalancePeekEnabled);
		balancePeekListRowContentPopulator.createRow();
		
		if(isListRow()) {
			return adjustListRowMargins(content);
		}
		return content;
	}
	
	private boolean isListRow() {
		if(layoutToBeInflated == R.layout.balance_peek_list_row) {
			return true;
		}
		return false;
	}

	private View adjustListRowMargins(View content) {
		if(!hasProducts() || position == 0) {
			removeTopAndBottomMargins(content);
			return content;
		}	
		return content;
	}
	
	private void removeTopAndBottomMargins(View content) {
		FrameLayout frame = FrameLayout.class.cast(content.findViewById(R.id.product_content));
		LinearLayout row = LinearLayout.class.cast(frame.findViewById(R.id.balancePeek_list_row_container));
		FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)row.getLayoutParams();
		int margin = (int) (MARGIN * resources.getDisplayMetrics().density);
		params.setMargins(margin,0,margin,0); 
		row.setLayoutParams(params);
	}

	private View inflateProductListRow(LayoutInflater inflater, int layoutToBeInflated) {
		return inflater.inflate(layoutToBeInflated, null);
	}
}
