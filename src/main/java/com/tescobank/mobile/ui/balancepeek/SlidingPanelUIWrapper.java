package com.tescobank.mobile.ui.balancepeek;

import android.view.View;
import android.view.ViewGroup;

public class SlidingPanelUIWrapper {

	private final ViewGroup panelTop;
	private final ViewGroup panelBottom;
	private final ViewGroup panelConcealed;
	private final View rootView;

	public SlidingPanelUIWrapper(ViewGroup panelTop, ViewGroup panelBottom, ViewGroup panelConcealed, View rootView) {
		super();
		this.panelTop = panelTop;
		this.panelBottom = panelBottom;
		this.panelConcealed = panelConcealed;
		this.rootView = rootView;

		verifyViewsAreValid(panelTop, panelBottom, panelConcealed, rootView);
	}

	private void verifyViewsAreValid(ViewGroup panelTop, ViewGroup panelBottom, ViewGroup accountLayoutContainer, View rootLayout) {
		verifyNotNull(panelTop);
		verifyNotNull(panelBottom);
		verifyNotNull(accountLayoutContainer);
		verifyNotNull(rootLayout);
	}

	private void verifyNotNull(View view) {
		if (view == null) {
			throw new IllegalArgumentException("All view paramters are required; View cannot be null");
		}
	}

	public ViewGroup getPanelTop() {
		return panelTop;
	}

	public ViewGroup getPanelBottom() {
		return panelBottom;
	}

	public ViewGroup getPanelConcealed() {
		return panelConcealed;
	}

	public View getRootView() {
		return rootView;
	}
}
