package com.tescobank.mobile.ui.balancepeek;

import static com.tescobank.mobile.model.product.ProductType.typeForLabel;

import java.util.Locale;

import android.content.res.Resources;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.tescobank.mobile.R;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.model.product.ProductType;
import com.tescobank.mobile.ui.TescoTextView;
import com.tescobank.mobile.ui.carousel.ProductDisplayer;

public class BalancePeekListRowContentPopulator {

	private final static String UNAVAILABLE_OUTAGE_STATUS ="unavailable";
	private ProductType productType;
	private Resources resources;
	private ProductDetails product;
	private View content;
	private ImageView imageView;
	private boolean isBalancePeekEnabled;
	private boolean isNonInteractiveProductsEnabled;

	public BalancePeekListRowContentPopulator(View content, ImageView imageView, ProductDetails product, Resources resources, boolean isNonInteractiveProductsEnabled, boolean isBalancePeekEnabled) {
		this.imageView = imageView;
		this.product = product;
		this.resources = resources;
		this.content = content;
		this.isNonInteractiveProductsEnabled = isNonInteractiveProductsEnabled;
		this.isBalancePeekEnabled = isBalancePeekEnabled;
	}
	
	public void createRow() {
		if (product!=null) {
			updateContentForProduct();
			return;
		}
		updateContentForMessage();
	}

	private void updateContentForProduct() {
		content.setBackgroundColor(resources.getColor(R.color.grey_list_background));
		this.productType = typeForLabel(product.getProductType());
		updateProductRowColour(content);
		updateMainImages(imageView);
		updateContentForProduct(content);
	}

	private void updateProductRowColour(View content) {
		View productRowColour = content.findViewById(R.id.product_colour);
		if (ProductDisplayer.isPcaProduct(productType)) {
			productRowColour.setBackgroundColor(resources.getColor(R.color.pca_product));
		} else if (ProductDisplayer.isCCProduct(productType)) {
			productRowColour.setBackgroundColor(resources.getColor(R.color.cc_product));
		} else if (ProductDisplayer.isLoanProduct(productType)) {
			productRowColour.setBackgroundColor(resources.getColor(R.color.loan_product));
		} else if (ProductDisplayer.isOtherProduct(productType)) {
			productRowColour.setBackgroundColor(resources.getColor(R.color.savings_product));
		}
	}

	private void updateMainImages(ImageView image) {
		if (ProductDisplayer.isPcaProduct(productType)) {
			image.setImageDrawable(resources.getDrawable(R.drawable.list_pca_img));
		} else if (ProductDisplayer.isCCProduct(productType)) {
			image.setImageDrawable(resources.getDrawable(R.drawable.list_cc_img));
		} else if (ProductDisplayer.isLoanProduct(productType)) {
			image.setImageDrawable(resources.getDrawable(R.drawable.list_loans_img));
		} else if (ProductDisplayer.isOtherProduct(productType)) {
			image.setImageDrawable(resources.getDrawable(R.drawable.list_savings_img));
		}
	}
	
	private void updateContentForProduct(View content) {
		if(contentContainsBalanceView()) {
			if(product.getOutageStatus().toUpperCase(Locale.UK).equals(UNAVAILABLE_OUTAGE_STATUS.toUpperCase(Locale.UK))) {
				updateContentForProductUnavailable();
				return;
			}
			updateContentForBalancePeekBalances(content);	
			return;
		} 
		
		updateContentForBalancePeekSetUp(content);
	}	
	
	private void updateContentForProductUnavailable() {
		BalancePeekDataSetter balancePeekDataSetter = new BalancePeekDataSetter(product, content);
		balancePeekDataSetter.setProductName();
		showProductContentAsProductAvailable(false);
		content.findViewById(R.id.balance_peek_translucent_row).setVisibility(View.VISIBLE);

	}

	private void showProductContentAsProductAvailable(boolean showProductData) {
		TescoTextView.class.cast(content.findViewById(R.id.balance_peek_product_unavailable_message)).setVisibility(showProductData ? View.GONE : View.VISIBLE);
		TescoTextView.class.cast(content.findViewById(R.id.balance_peek_product_id_number)).setVisibility(showProductData ? View.VISIBLE : View.GONE);
		TescoTextView.class.cast(content.findViewById(R.id.balance_peek_product_balance)).setVisibility(showProductData ? View.VISIBLE : View.GONE);
		TescoTextView.class.cast(content.findViewById(R.id.balance_peek_product_available)).setVisibility(showProductData ? View.VISIBLE : View.GONE);
		TescoTextView.class.cast(content.findViewById(R.id.balance_peek_product_available_label)).setVisibility(showProductData ? View.VISIBLE : View.GONE);
	}

	private boolean contentContainsBalanceView() {
		return (TescoTextView) content.findViewById(R.id.balance_peek_product_balance)  == null ? false :true;
	}
		
	private void updateContentForBalancePeekSetUp(View content) {
		BalancePeekDataSetter balancePeekDataSetter = new BalancePeekDataSetter(product, content);
		balancePeekDataSetter.setProductName();	
		
		if(ProductDisplayer.isCCProduct(productType)) {
			balancePeekDataSetter.setCreditCardNumber();
			return;
		}	
		
		balancePeekDataSetter.setSortCodeAndAccountNumber();
		return;	
	}

	private View updateContentForBalancePeekBalances(View content) {
		showProductContentAsProductAvailable(true);
		content.findViewById(R.id.balance_peek_translucent_row).setVisibility(View.GONE);
		BalancePeekDataSetter balancePeekDataSetter = new BalancePeekDataSetter(product, content);
		balancePeekDataSetter.setProductName();
		balancePeekDataSetter.setBalance();
		balancePeekDataSetter.setProductDisplayId();	
		
		if (ProductDisplayer.isLoanProduct(productType)) {
			configureBalanceDataForLoan(content, balancePeekDataSetter);
			return content;
		} 
		
		configureAvailableBalanceViews(content, true);
		
		if(ProductDisplayer.isCCProduct(productType)){
			configureBalanceDataForCreditCard(content, balancePeekDataSetter);
			return content;
		}
		
		TescoTextView.class.cast(content.findViewById(R.id.balance_peek_product_available_label)).setText(R.string.balance_peek_available_balance_text);
		balancePeekDataSetter.setAvailableBalance();
		return content;	
	}

	private void configureBalanceDataForLoan(View content, BalancePeekDataSetter balancePeekDataSetter) {
		configureAvailableBalanceViews(content, false);
		balancePeekDataSetter.setAmountRemainingOnLoan();
	}
	
	private void configureAvailableBalanceViews(View content, boolean makeVisible) {
		content.findViewById(R.id.balance_peek_product_available_label).setVisibility(makeVisible ? View.VISIBLE : View.GONE);
		content.findViewById(R.id.balance_peek_product_available).setVisibility(makeVisible ? View.VISIBLE : View.GONE);
	}

	private void configureBalanceDataForCreditCard(View content, BalancePeekDataSetter balancePeekDataSetter) {
		TescoTextView.class.cast(content.findViewById(R.id.balance_peek_product_available_label)).setText(R.string.balance_peek_available_credit_text);
		balancePeekDataSetter.setAvailableCredit();
	}
	
	private void updateContentForMessage() {
		hideAllProductViews();
		configureInfoRowMessageAndShow();	
	}
	
	private void hideAllProductViews() {
		content.findViewById(R.id.balance_peek_product_info).setVisibility(View.GONE);
		content.findViewById(R.id.balancePeek_product_img_middle_layout).setVisibility(View.GONE);
	}

	private void configureInfoRowMessageAndShow() {
		content.setBackgroundColor(resources.getColor(R.color.grey_list_background));
		content.findViewById(R.id.balance_peek_translucent_row).setVisibility(View.GONE);
		RelativeLayout messageContainer = RelativeLayout.class.cast(content.findViewById(R.id.balance_peek_info_message_container));
		TescoTextView message = TescoTextView.class.cast(content.findViewById(R.id.balance_peek_info_message_line));
		
		if(!isNonInteractiveProductsEnabled) {
			message.setText(R.string.balance_peek_unavailable_info_message_line_text);
		} else if(isBalancePeekEnabled) {
			message.setText(R.string.balance_peek_no_products_info_message_line_text);
		} else {
			message.setText(R.string.balance_peek_first_time_info_message_line_text);
		}
		messageContainer.setVisibility(View.VISIBLE);	
	}
}
