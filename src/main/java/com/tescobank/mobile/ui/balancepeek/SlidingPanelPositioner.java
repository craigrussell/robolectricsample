package com.tescobank.mobile.ui.balancepeek;

public interface SlidingPanelPositioner {
	public boolean shouldOpenPanels();

	public boolean userDraggedSliderAboveAccountList(float newPosition);
	public boolean userDraggedSliderBelowAccountList(float newPosition);
	public float calculateMovementFactor();
}
