package com.tescobank.mobile.ui.balancepeek;

import android.util.Log;
import android.view.ViewGroup;

import com.tescobank.mobile.BuildConfig;

public class BalancePeekPanelPositioner implements SlidingPanelPositioner {

	private static final String TAG = BalancePeekPanelPositioner.class.getSimpleName();

	private SlidingPanelUIWrapper panelUIWrapper;

	private float origin;
	private float originOffset;

	public BalancePeekPanelPositioner(SlidingPanelUIWrapper panelUIWrapper, float origin, float originOffset) {
		this.panelUIWrapper = panelUIWrapper;
		this.origin = origin;
		this.originOffset = originOffset;
	}

	@Override
	public boolean shouldOpenPanels() {
		if (panelUIWrapper.getPanelConcealed().getHeight() == 0) {
			return false;
		}

		float newPosition = panelUIWrapper.getPanelTop().getY() + panelUIWrapper.getPanelTop().getHeight();
		float heightOfAccountLayoutContainer = panelUIWrapper.getPanelConcealed().getHeight();

		float threshold = heightOfAccountLayoutContainer / 10;
		float difference = origin - newPosition;

		if (BuildConfig.DEBUG) {
			Log.i(TAG, String.format("Threshold is %f, New Y position is %f, distance is %f", threshold, newPosition, difference));
		}

		return difference > threshold;
	}

	@Override
	public boolean userDraggedSliderAboveAccountList(float newPosition) {

		float threshold = origin - originOffset;

		if(BuildConfig.DEBUG) {
			Log.v(TAG, String.format("new position = %f, threshold = %f", newPosition, threshold));
		}

		if (newPosition <= threshold) {
			return true;
		}
		return false;
	}

	@Override
	public boolean userDraggedSliderBelowAccountList(float newPosition) {
		float maxPosition = panelUIWrapper.getPanelConcealed().getY() + panelUIWrapper.getPanelConcealed().getHeight() - originOffset * 2;
		if (newPosition >= maxPosition) {
			return true;
		}
		return false;
	}

	@Override
	public float calculateMovementFactor() {
		ViewGroup panelConcealed = panelUIWrapper.getPanelConcealed();
		float panelADistanceFromOrigin = origin - panelConcealed.getY();
		float panelBDistanceFromOrigin = panelConcealed.getY() + panelConcealed.getHeight() - origin;

		if (panelADistanceFromOrigin == 0 || panelBDistanceFromOrigin == 0) {
			return 1;
		}

		float factor = panelADistanceFromOrigin / panelBDistanceFromOrigin;
		if (BuildConfig.DEBUG) {
			Log.i(TAG, "Calculated panel movement factor to be " + factor);
		}
		return factor;
	}
}
