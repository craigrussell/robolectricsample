package com.tescobank.mobile.ui.balancepeek.inactivity;

public class TimeoutCounterFactory {
	public static Counter getCounter(InactivtyCounterCallback callback, long timeoutTime) {
		if (timeoutTime == 0) {
			return getDisabledCounter();
		} else {
			return getEnabledCounter(callback, timeoutTime);
		}
	}

	private static Counter getEnabledCounter(InactivtyCounterCallback callback, long timeoutTime) {
		return new TimeoutCounter(callback, timeoutTime);
	}

	private static Counter getDisabledCounter() {
		return new Counter() {
			@Override
			public void stop() {
			}

			@Override
			public void start() {
			}

			@Override
			public void resetCounter() {
			}

			@Override
			public boolean isRunning() {
				return false;
			}

			@Override
			public long getInactivityTimeoutDuration() {
				return 0;
			}
		};
	}
}
