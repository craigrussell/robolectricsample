package com.tescobank.mobile.ui.balancepeek;

import static com.tescobank.mobile.model.product.ProductType.typeForLabel;
import static com.tescobank.mobile.ui.carousel.ProductDisplayer.isCCProduct;
import android.view.View;

import com.tescobank.mobile.R;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.ui.TescoTextView;

/**
 * Helper used to set product data on balance Peek UI components
 */
public class BalancePeekDataSetter {

	private ProductDetails product;

	private DataFormatter dataFormatter;
	
	private TescoTextView productId;
	
	private TescoTextView balance;
	
	private View content;

	public BalancePeekDataSetter(ProductDetails product, View content) {
		super();
		this.product = product;
		this.content = content;
		this.dataFormatter = new DataFormatter();
		this.productId = (TescoTextView) content.findViewById(R.id.balance_peek_product_id_number);
		this.balance = (TescoTextView) content.findViewById(R.id.balance_peek_product_balance);
	}

	public void setProductName() {
		TescoTextView productName = (TescoTextView) content.findViewById(R.id.balance_peek_product_name);
		productName.setText(product.getProductName());
	}

	public void setBalance() {
		balance.setText(dataFormatter.formatCurrency(product.getBalance()));
	}

	public void setProductDisplayId() {
        if(isCCProduct(typeForLabel(product.getProductType()))){
            productId.setText(DataFormatter.formatCardNumber(product.getDisplayProductId()));
        }else{
		    productId.setText(getFormattedAccountNumber());

        }
	}

    private String getFormattedAccountNumber(){
        return product.getDisplayProductId().substring(6);
    }
	
	public void setSortCodeAndAccountNumber() {
		productId.setText(dataFormatter.sortCodeAndAccoutNumberJoiner(product.getSortCode(), product.getAccountNumber()));
	}
	
	public void setCreditCardNumber() {
		productId.setText(DataFormatter.formatCardNumber(product.getCardNumber()));
	}
	
	public void setAmountRemainingOnLoan() {
		balance.setText(dataFormatter.formatCurrency(product.getAmountRemaining()));	
	}
	
	public void setAvailableBalance() {
		TescoTextView availableBalance = (TescoTextView) content.findViewById(R.id.balance_peek_product_available);
		availableBalance.setText(dataFormatter.formatCurrency(product.getAvailableBalance()));	
	}
	
	public void setAvailableCredit() {
		TescoTextView availableCredit = (TescoTextView) content.findViewById(R.id.balance_peek_product_available);
		availableCredit.setText(dataFormatter.formatCurrency(product.getAvailableCredit()));	
	}
}
