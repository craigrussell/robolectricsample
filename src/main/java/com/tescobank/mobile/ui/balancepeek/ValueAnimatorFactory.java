package com.tescobank.mobile.ui.balancepeek;

import android.animation.ValueAnimator;

public class ValueAnimatorFactory {
	public ValueAnimator getAnimator(float startValue, float endValue, long duration) {
		ValueAnimator animator = ValueAnimator.ofFloat(startValue, endValue);
		animator.setDuration(duration);
		return animator;
	}
}
