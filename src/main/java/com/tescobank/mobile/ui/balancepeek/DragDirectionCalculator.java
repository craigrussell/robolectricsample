package com.tescobank.mobile.ui.balancepeek;

public class DragDirectionCalculator implements DirectionCalculator {

	public enum DragDirection {
		NO_DIRECTION,
		UP,
		DOWN;
	}

	@Override
	public DragDirection calculateDirection(int oldPosition, int newPosition) {
		if (newPosition == oldPosition) {
			return DragDirection.NO_DIRECTION;
		}
		else if (newPosition > oldPosition) {
			return DragDirection.DOWN;
		}
		else {
			return DragDirection.UP;
		}

	}
}
