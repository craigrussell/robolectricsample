package com.tescobank.mobile.ui.balancepeek;

import java.util.ArrayList;
import java.util.List;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.View;

public class BalancePeekViewAnimator {

	public static final float VIEW_MAX_ALPHA = 1;
	public static final float VIEW_MIN_ALPHA = 0;
	public static final int DEFAULT_FADE_ANIMATION_DURATION = 250;

	private List<View> transitionViews;
	private List<View> afterTransitionViews;
	private List<View> preTransitionViews;

	private ValueAnimatorFactory animatorFactory;

	public BalancePeekViewAnimator() {
		transitionViews = new ArrayList<View>();
		afterTransitionViews = new ArrayList<View>();
		preTransitionViews = new ArrayList<View>();
		animatorFactory = new ValueAnimatorFactory();
	}

	public void addTransitionView(View view) {
		transitionViews.add(view);
	}

	public void addPreTransitionView(View view) {
		preTransitionViews.add(view);
	}

	public void addPostTransitionView(View view) {
		afterTransitionViews.add(view);
	}

	public void showComponents(long duration) {
		float startValue = VIEW_MIN_ALPHA;
		float endValue = VIEW_MAX_ALPHA;

		ValueAnimator animator = animatorFactory.getAnimator(startValue, endValue, duration);
		animateViews(animator, endValue);
	}

	public void hideComponents(long duration) {
		float startValue = VIEW_MAX_ALPHA;
		float endValue = VIEW_MIN_ALPHA;

		ValueAnimator animator = animatorFactory.getAnimator(startValue, endValue, duration);
		animateViews(animator, endValue);
	}

	private void animateViews(final ValueAnimator animator, final float endValue) {
		animator.addUpdateListener(new AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				Float newAlpha = (Float) animation.getAnimatedValue();
				handleTransitionViews(newAlpha);
			}
		});

		animator.addListener(new AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animation) {
			}

			@Override
			public void onAnimationRepeat(Animator animation) {
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				handlePostAnimationViews(endValue);
			}

			@Override
			public void onAnimationCancel(Animator animation) {
			}
		});

		handlePreAnimationViews(endValue);

		animator.start();
	}

	private void handlePreAnimationViews(float finalAlpha) {
		for (View view : preTransitionViews) {
			view.setAlpha(finalAlpha);
		}
	}

	private void handleTransitionViews(float finalAlpha) {
		for (View view : transitionViews) {
			view.setAlpha(finalAlpha);
		}
	}

	private void handlePostAnimationViews(Float finalAlpha) {
		for (View view : afterTransitionViews) {
			view.setAlpha(finalAlpha);
		}
	}

	public void setAnimatorFactory(ValueAnimatorFactory animatorFactory) {
		this.animatorFactory = animatorFactory;
	}
}
