package com.tescobank.mobile.ui.balancepeek;

import com.tescobank.mobile.ui.balancepeek.DragDirectionCalculator.DragDirection;

public interface DirectionCalculator {
	public DragDirection calculateDirection(int oldPosition, int newPosition);
}
