package com.tescobank.mobile.ui.balancepeek.inactivity;

public interface Counter {

	public void start();
	public void stop();
	public boolean isRunning();
	public void resetCounter();
	public long getInactivityTimeoutDuration();
}