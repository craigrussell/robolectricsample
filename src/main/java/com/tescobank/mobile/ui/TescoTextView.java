package com.tescobank.mobile.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class TescoTextView extends TextView implements TypefaceDuckType {

	public TescoTextView(Context context){
		super(context);
	}
	
	public TescoTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setTypeface(context, attrs);
	}

	public TescoTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setTypeface(context, attrs);
	}

	public final void setTypeface(Context context, AttributeSet attrs) {
		if (!isInEditMode()) {
			TypefaceAdapter.setTypeface(this, context, attrs);
		}
	}
}
