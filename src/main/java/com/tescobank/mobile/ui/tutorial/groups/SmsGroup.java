package com.tescobank.mobile.ui.tutorial.groups;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipGroup;
import com.tescobank.mobile.ui.tutorial.tooltips.SmsCopyPasteTooltip;

public class SmsGroup implements TooltipGroup {

	private final TescoActivity activity;

	public SmsGroup(TescoActivity activity) {
		this.activity = activity;
	}

	@Override
	public List<? extends Tooltip> getDelayedTooltips() {
		return Collections.emptyList();
	}

	@Override
	public List<? extends Tooltip> getMandatoryTooltips() {
		return Arrays.asList(new SmsCopyPasteTooltip(activity));
	}

}
