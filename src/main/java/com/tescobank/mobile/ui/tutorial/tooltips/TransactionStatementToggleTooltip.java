package com.tescobank.mobile.ui.tutorial.tooltips;

import android.content.Context;

import com.tescobank.mobile.R;
import static com.tescobank.mobile.ui.tutorial.TooltipId.TransactionStatementToggle;

public class TransactionStatementToggleTooltip extends ActionBarTooltip {

	private boolean canShow;

	public TransactionStatementToggleTooltip(Context context, int actionBarSlot, boolean transactionsPresent, boolean statementsEnabled) {
		super(context, actionBarSlot, TransactionStatementToggle.ordinal(), R.string.tooltip_title_default, R.string.tooltip_transaction_list_statement_toggle, R.drawable.ic_tutorial_statement_archive);
		this.canShow = transactionsPresent && statementsEnabled;
	}

	@Override
	public boolean canShow() {
		return canShow;
	}

}
