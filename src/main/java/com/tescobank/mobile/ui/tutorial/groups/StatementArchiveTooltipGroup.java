package com.tescobank.mobile.ui.tutorial.groups;

import java.util.Arrays;
import java.util.List;

import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipGroup;
import com.tescobank.mobile.ui.tutorial.tooltips.StatementTransactionToggleTooltip;

public class StatementArchiveTooltipGroup implements TooltipGroup {

	private List<? extends Tooltip> mandatory;
	private List<? extends Tooltip> delayed;

	public StatementArchiveTooltipGroup() {
		mandatory = Arrays.asList();
		delayed = Arrays.asList(new StatementTransactionToggleTooltip());
	}

	@Override
	public List<? extends Tooltip> getMandatoryTooltips() {
		return mandatory;
	}

	@Override
	public List<? extends Tooltip> getDelayedTooltips() {
		return delayed;
	}

}
