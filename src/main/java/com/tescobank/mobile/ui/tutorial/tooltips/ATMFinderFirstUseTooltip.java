package com.tescobank.mobile.ui.tutorial.tooltips;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class ATMFinderFirstUseTooltip extends ActionBarTooltip {

	private boolean canShow;

	public ATMFinderFirstUseTooltip(Context context, boolean isAtmAndGPS) {
		super(context, 2, TooltipId.ATMFinderFirstUse.ordinal(), R.string.tooltip_atm_finder_title, R.string.tooltip_atm_finder_first_use, R.drawable.ic_tutorial_atm_location);
		this.canShow = isAtmAndGPS;
	}

	@Override
	public boolean canShow() {
		return canShow;
	}
}
