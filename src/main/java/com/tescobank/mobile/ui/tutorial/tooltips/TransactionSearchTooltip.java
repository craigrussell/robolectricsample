package com.tescobank.mobile.ui.tutorial.tooltips;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class TransactionSearchTooltip extends ActionBarTooltip {

	private boolean canShow;

	public TransactionSearchTooltip(Context context, int actionBarSlot, boolean transactionsPresent) {
		super(context, actionBarSlot, TooltipId.TransactionSearch.ordinal(), R.string.tooltip_title_default, R.string.tooltip_transaction_list_search, R.drawable.ic_tutorial_transaction_search);
		canShow = transactionsPresent;
	}

	@Override
	public boolean canShow() {
		return canShow;
	}

}
