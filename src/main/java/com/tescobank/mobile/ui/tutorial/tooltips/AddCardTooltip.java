package com.tescobank.mobile.ui.tutorial.tooltips;

import static com.tescobank.mobile.ui.tutorial.TooltipLipDirection.SOUTH;
import android.util.DisplayMetrics;
import android.view.View;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class AddCardTooltip extends Tooltip {

	private boolean canShow;
	
	public AddCardTooltip(boolean addCardEnabled, boolean hasActiveCards) {
		super(TooltipId.AddACard.ordinal(), R.string.tooltip_add_card_title, R.string.tooltip_add_card_message, R.drawable.ic_tutorial_add_card, SOUTH);
		canShow = addCardEnabled && hasActiveCards;
	}

	protected int getTargetX(DisplayMetrics metrics) {
		int[] location = new int[2];
		View view = getActivity().findViewById(R.id.pay_creditcard_pay_card_title);
		view.getLocationInWindow(location);
		return location[0] + view.getWidth() / 2 - 120;
	}

	@Override
	protected int getTargetY(DisplayMetrics metrics) {
		int[] location = new int[2];
		View view = getActivity().findViewById(R.id.pay_creditcard_select_payment_pager);
		view.getLocationInWindow(location);
		return location[1] - (int)(60 * metrics.density);
	}

	@Override
	public boolean canShow() {
		return canShow;
	}
}