package com.tescobank.mobile.ui.tutorial;

/** We need to use ids that will survive across app versions (R.id won't, potentially neither will the class name). The only way to be sure is to define the constants explicitly and unit test them. */
public enum TooltipId {

	/** The order of these MUST be maintained. */
	/*  0 */ FlyoutMenuIntro, 
	/*  1 */ ATMFinderFirstUse, 
	/*  2 */ AuthorisedSettings, 
	/*  3 */ FlyoutMenuDrag, 
	/*  4 */ CalendarOnFlyoutMenu,
	/*  5 */ SavingsWheelFirstUse,
	/*  6 */ SavingsWheelChangeAccount,
	/*  7 */ TransactionDetail,
	/*  8 */ TransactionCalendarToggle,
	/*  9 */ TransactionSearch,
	/* 10 */ TransactionSort,
	/* 11 */ TransactionStatementToggle,
	/* 12 */ StatementTransactionToggle,
	/* 13 */ CalendarFirstTimeUse,
	/* 14 */ CalendarSwipe,
	/* 15 */ CalendarHeader,
	/* 16 */ CalendarTransactionsToggle,
	/* 17 */ SmsCopyPaste, 
	/* 18 */ CarouselList,
	/* 19 */ AddACard
}
