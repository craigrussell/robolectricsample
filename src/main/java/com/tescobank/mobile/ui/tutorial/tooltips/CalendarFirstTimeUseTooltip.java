package com.tescobank.mobile.ui.tutorial.tooltips;

import java.util.Calendar;

import android.util.DisplayMetrics;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipId;
import com.tescobank.mobile.ui.tutorial.TooltipLipDirection;

public class CalendarFirstTimeUseTooltip extends Tooltip {

	private final int dayOffset;

	public CalendarFirstTimeUseTooltip() {
		this(Calendar.getInstance());
	}

	CalendarFirstTimeUseTooltip(Calendar calendar) {
		super(TooltipId.CalendarFirstTimeUse.ordinal(), R.string.tooltip_calendar_title, R.string.tooltip_calendar_first_use, R.drawable.ic_tutorial_calendar_day, TooltipLipDirection.NORTH);
		Calendar cal = calendar;
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.get(Calendar.DAY_OF_WEEK);
		dayOffset = cal.get(Calendar.DAY_OF_WEEK) - Calendar.MONDAY;
	}

	@Override
	protected int getTargetX(DisplayMetrics metrics) {
		int pixelsPerDay = metrics.widthPixels / 7;
		return (pixelsPerDay * dayOffset) + (pixelsPerDay / 2);
	}

	@Override
	protected int getTargetY(DisplayMetrics metrics) {
		return (int) (235 * metrics.density);
	}

}
