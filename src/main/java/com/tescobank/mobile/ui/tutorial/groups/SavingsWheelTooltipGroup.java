package com.tescobank.mobile.ui.tutorial.groups;

import java.util.Arrays;
import java.util.List;

import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipGroup;
import com.tescobank.mobile.ui.tutorial.tooltips.SavingsWheelChangeAccountTooltip;
import com.tescobank.mobile.ui.tutorial.tooltips.SavingsWheelFirstUseTooltip;

public class SavingsWheelTooltipGroup implements TooltipGroup {
	
	private List<? extends Tooltip> mandatory;
	private List<? extends Tooltip> delayed;
	
	public SavingsWheelTooltipGroup(int numberOfLinkedAccounts) {
		mandatory = Arrays.asList(new SavingsWheelFirstUseTooltip(numberOfLinkedAccounts));
		delayed = Arrays.asList(new SavingsWheelChangeAccountTooltip(numberOfLinkedAccounts));
	}

	@Override
	public List<? extends Tooltip> getMandatoryTooltips() {
		return mandatory;
	}

	@Override
	public List<? extends Tooltip> getDelayedTooltips() {
		return delayed;
	}

}
