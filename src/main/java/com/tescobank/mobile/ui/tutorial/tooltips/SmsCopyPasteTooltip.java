package com.tescobank.mobile.ui.tutorial.tooltips;

import android.util.DisplayMetrics;
import android.view.View;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipId;
import com.tescobank.mobile.ui.tutorial.TooltipLipDirection;

public class SmsCopyPasteTooltip extends Tooltip {

	private int[] location = new int[2];
	private int height;
	private int width;

	public SmsCopyPasteTooltip(TescoActivity activity) {
		super(TooltipId.SmsCopyPaste.ordinal(), R.string.tooltip_smscopypaste_title, R.string.tooltip_smscopypaste_message, R.drawable.ic_tooltip_paste_sms, TooltipLipDirection.NORTH);
		
		View view = activity.findViewById(R.id.inputField1);
		height = view.getHeight();
		width = view.getWidth();
		view.getLocationOnScreen(location);
	}

	@Override
	protected int getTargetX(DisplayMetrics metrics) {
		return (int) (location[0] + (width / 2.0f));
	}

	@Override
	protected int getTargetY(DisplayMetrics metrics) {
		return (int) (location[1] + height);
	}

}
