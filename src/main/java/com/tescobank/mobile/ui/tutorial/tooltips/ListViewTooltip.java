package com.tescobank.mobile.ui.tutorial.tooltips;

import android.util.DisplayMetrics;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class ListViewTooltip extends Tooltip {

	public ListViewTooltip() {
		super(TooltipId.CarouselList.ordinal(), R.string.tooltip_carousel_list_title, R.string.tooltip_carousel_list, R.drawable.hint_ic_change_to_list);
	}

	@Override
	public int getTargetX(DisplayMetrics metrics) {
		return metrics.widthPixels - (int) (30 * metrics.density);
	}

	@Override
	public int getTargetY(DisplayMetrics metrics) {
		return (int) (70 * metrics.density);
	}

}
