package com.tescobank.mobile.ui.tutorial;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;
import com.tescobank.mobile.application.TescoApplication;
import com.tescobank.mobile.ui.TescoActivity;

public class TooltipManager {

	private static final String TAG = TooltipManager.class.getSimpleName();

	private static final String SESSION_COUNT_KEY = "tooltip.session.count";
	private static final String SESSION_KEY = "tooltip.shown.this.session";
	private static final String PREFS_NAME = "tutorials";

	private TescoActivity activity;
	private SharedPreferences prefs;
	private TooltipGroup group;
	private Tooltip currentTooltip;
	private View tooltipLayout;

	private Future<?> delayTask;

	private long showDelay;

	public TooltipManager(TescoActivity activity, TooltipGroup group) {
		this(500, activity, group);
	}

	/**
	 * Constructor for tests.
	 * 
	 * @param showDelay
	 */
	TooltipManager(long showDelay, TescoActivity activity, TooltipGroup group) {
		this.showDelay = showDelay;
		this.activity = activity;
		this.group = group;
	}

	public static void reset(TescoApplication application) {
		application.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit().clear().commit();
	}

	/**
	 * Show the current tutorial for the current activity. Should be called from {@link android.app.Activity#onResume}. Some tutorials are mandatory and will be displayed if they haven't been seen before, but then may not be displayed again. Some tutorials are passive and will only be displayed if
	 * the user hasn't interacted with a certain component recently.
	 */
	public void show() {
		if (null == group) {
			return;
		}

		refreshPrefs();
		if (next()) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "Showing tooltip: " + currentTooltip);
			}
			activity.getApplicationState().getAnalyticsTracker().trackViewTutorialScreen(currentTooltip.getClass().getSimpleName());
			resetSessionCount();
			delayedDisplayCurrentTooltip();
		}
	}

	private void delayedDisplayCurrentTooltip() {

		if (delayTask != null) {
			delayTask.cancel(true);
			delayTask = null;
		}

		delayTask = Executors.newSingleThreadScheduledExecutor().schedule(new Runnable() {
			@Override
			public void run() {
				displayCurrentToolipOnUIThread();
			}
		}, showDelay, TimeUnit.MILLISECONDS);

	}

	/** Hide, possibly showing this tooltip again later. */
	public void hide() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "hiding tooltip: " + currentTooltip);
		}

		if (null != delayTask) {
			delayTask.cancel(true);
			delayTask = null;
		}

		if (null != tooltipLayout) {
			getContentView().removeView(tooltipLayout);
			tooltipLayout = null;
			currentTooltip = null;
		}
	}

	/**
	 * Hide a tooltip forever, usually because it has been explicitly dismissed via the close button OR because the interaction it relates to has been completed.
	 * 
	 * @param tooltipId
	 *            the id of tooltip being dismissed (use {@link TooltipId#ordinal()})
	 */
	public synchronized void dismiss(int tooltipId) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "dismiss: " + tooltipId + ", current = " + (null == currentTooltip ? "null" : currentTooltip.getId()));

		}
		refreshPrefs();

		if (null != currentTooltip && tooltipId == currentTooltip.getId()) {
			hide();
		}

		prefs.edit().putBoolean(shownKey(tooltipId), true).commit();
	}

	/** Called to indicate a new session is currently starting and that the next 'delayed' tooltip is good to be shown. */
	public void newSession() {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "new session");
		}

		refreshPrefs();

		prefs.edit().remove(SESSION_KEY).commit();

		incSessionCount();
	}

	public void setGroup(TooltipGroup group) {
		this.group = group;
	}

	private void displayCurrentTooltip() {
		prefs.edit().putInt(SESSION_KEY, currentTooltip.getId()).commit();

		tooltipLayout = activity.getLayoutInflater().inflate(R.layout.tooltip, null);
		currentTooltip.configureDetails(tooltipLayout);
		getContentView().addView(tooltipLayout);
		currentTooltip.position(activity, tooltipLayout);
		activity.findViewById(R.id.tutorial_tooltip_close_button).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(currentTooltip != null) {
					dismiss(currentTooltip.getId());
				}
			}
		});

	}

	private void resetSessionCount() {
		if (group.getMandatoryTooltips().contains(currentTooltip) || !isDelayed()) {
			prefs.edit().remove(SESSION_COUNT_KEY).commit();
		} else {
			incSessionCount();
		}
	}

	private ViewGroup getContentView() {
		return ViewGroup.class.cast(activity.findViewById(android.R.id.content).getRootView());
	}

	private boolean next() {
		currentTooltip = null;

		for (Tooltip t : group.getMandatoryTooltips()) {			
			if (!hasShown(t)) {
				if (t.canShow()) {
					currentTooltip = t;
					return true;
				}
			}
		}

		Tooltip nextDelayed = null;
		for (Tooltip t : group.getDelayedTooltips()) {
			if (!hasShown(t) && t.canShow()) {
				nextDelayed = t;
				break;
			}
		}

		if (null != nextDelayed) {

			if (nextDelayed.getId() == prefs.getInt(SESSION_KEY, -1)) {
				// we've seen this tooltip this session but it hasn't been dismissed so show again
				currentTooltip = nextDelayed;
				return true;
			}

			if (!isDelayed()) {
				currentTooltip = nextDelayed;
				return true;
			}

		}

		return false;
	}

	private void refreshPrefs() {
		prefs = activity.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
	}

	private boolean hasShown(Tooltip t) {
		return prefs.getBoolean(shownKey(t.getId()), false);
	}

	private void incSessionCount() {
		int sessionCount = prefs.getInt(SESSION_COUNT_KEY, 0) + 1;
		prefs.edit().putInt(SESSION_COUNT_KEY, sessionCount).commit();
	}

	private String shownKey(int id) {
		return "tooltip." + id + ".shown";
	}

	private boolean isDelayed() {
		return prefs.getInt(SESSION_COUNT_KEY, 0) < 2;
	}

	private void displayCurrentToolipOnUIThread() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Preparing to display tooltip: " + currentTooltip.getId());
		}

		final Future<?> task = this.delayTask;
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (BuildConfig.DEBUG) {
					if (currentTooltip != null) {
						Log.d(TAG, "Running on UI thread, displaying tooltip: " + currentTooltip.getId());
					}
				}

				// only show if we're running in the same task
				if (task == delayTask) {
					displayCurrentTooltip();
				}
			}
		});
	}

}
