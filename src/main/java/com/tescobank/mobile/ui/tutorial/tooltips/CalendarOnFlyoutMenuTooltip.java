package com.tescobank.mobile.ui.tutorial.tooltips;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class CalendarOnFlyoutMenuTooltip extends FlyoutMenuTooltip {

	private boolean canShow;

	public CalendarOnFlyoutMenuTooltip(Context context, boolean isProduct) {
		super(context, TooltipId.CalendarOnFlyoutMenu.ordinal(), R.string.tooltip_title_default, R.string.tooltip_calendar_on_flyout_menu, R.drawable.ic_tutorial_calendar_day);
		this.canShow = isProduct;
	}

	@Override
	public boolean canShow() {
		return canShow;
	}

}
