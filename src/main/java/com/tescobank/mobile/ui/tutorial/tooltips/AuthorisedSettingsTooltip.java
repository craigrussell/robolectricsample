package com.tescobank.mobile.ui.tutorial.tooltips;

import android.util.DisplayMetrics;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class AuthorisedSettingsTooltip extends Tooltip {

	private boolean canShow;

	public AuthorisedSettingsTooltip(boolean isAuthenticated) {
		super(TooltipId.AuthorisedSettings.ordinal(), R.string.tooltip_title_default, R.string.tooltip_authorised_settings_text, R.drawable.ic_tutorial_settings);
		this.canShow = isAuthenticated;
	}

	@Override
	public int getTargetX(DisplayMetrics metrics) {
		return (int) (20 * metrics.density);
	}

	@Override
	public int getTargetY(DisplayMetrics metrics) {
		return (int) (70 * metrics.density);
	}

	@Override
	public boolean canShow() {
		return canShow;
	}

}
