package com.tescobank.mobile.ui.tutorial.tooltips;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class FlyoutMenuIntroTooltip extends FlyoutMenuTooltip {

	private boolean canShow;

	public FlyoutMenuIntroTooltip(Context context, boolean isProductWithStatements) {
		super(context, TooltipId.FlyoutMenuIntro.ordinal(), R.string.tooltip_flyout_menu_title, R.string.tooltip_flyout_menu_intro_text, R.drawable.ic_tutorial_flyout);
		this.canShow = isProductWithStatements;
	}

	@Override
	public boolean canShow() {
		return canShow;
	}

}
