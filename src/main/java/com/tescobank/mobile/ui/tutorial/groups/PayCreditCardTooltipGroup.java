package com.tescobank.mobile.ui.tutorial.groups;

import java.util.Arrays;
import java.util.List;

import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipGroup;
import com.tescobank.mobile.ui.tutorial.tooltips.AddCardTooltip;

public class PayCreditCardTooltipGroup implements TooltipGroup {

	private final List<? extends Tooltip> delayed;
	private final List<? extends Tooltip> mandatory;

	public PayCreditCardTooltipGroup(boolean managePaymentsEnabled, boolean hasActiveCards) {
		this.mandatory = Arrays.asList(new AddCardTooltip(managePaymentsEnabled, hasActiveCards));
		this.delayed = Arrays.asList();
	}

	@Override
	public List<? extends Tooltip> getDelayedTooltips() {
		return this.delayed;
	}

	@Override
	public List<? extends Tooltip> getMandatoryTooltips() {
		return mandatory;
	}
}
