package com.tescobank.mobile.ui.tutorial.tooltips;

import android.util.DisplayMetrics;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class CalendarSwipeTooltip extends Tooltip {

	public CalendarSwipeTooltip() {
		super(TooltipId.CalendarSwipe.ordinal(), R.string.tooltip_calendar_title, R.string.tooltip_calendar_swipe, R.drawable.ic_tutorial_swipe_down);
	}

	@Override
	protected int getTargetX(DisplayMetrics metrics) {
		int pixelsPerDay = metrics.widthPixels / 7;
		return (pixelsPerDay / 2);
	}

	@Override
	protected int getTargetY(DisplayMetrics metrics) {
		return (int) (235 * metrics.density);
	}

}
