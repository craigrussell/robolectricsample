package com.tescobank.mobile.ui.tutorial;

import java.util.List;

public interface TooltipGroup {

	List<? extends Tooltip> getMandatoryTooltips();

	List<? extends Tooltip> getDelayedTooltips();

}
