package com.tescobank.mobile.ui.tutorial.tooltips;

import android.content.Context;
import android.util.DisplayMetrics;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.fomenu.FlyoutMenu;
import com.tescobank.mobile.ui.tutorial.Tooltip;

public class FlyoutMenuTooltip extends Tooltip {

	private Context context;
	private int xPosition;
	private int yPosition;

	public FlyoutMenuTooltip(Context context, int id, int titleResId, int messageResId, int iconResId) {
		super(id, titleResId, messageResId, iconResId);
		this.context = context;
		xPosition = context.getResources().getDimensionPixelOffset(R.dimen.tooltip_flyout_menu_x_position);
		
		int height = context.getResources().getDimensionPixelOffset(R.dimen.flyout_menu_item0_padding_bottom);
		yPosition = height + context.getResources().getDimensionPixelOffset(R.dimen.tooltip_flyout_menu_y_position);
	}

	@Override
	public int getTargetX(DisplayMetrics metrics) {
		if (!FlyoutMenu.isConfiguredOnLeft(context)) {
			return (int) (metrics.widthPixels - xPosition);
		}
		return (int) xPosition;
	}

	@Override
	public int getTargetY(DisplayMetrics metrics) {
		return metrics.heightPixels - yPosition;
	}

}
