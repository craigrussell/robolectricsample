package com.tescobank.mobile.ui.tutorial.groups;

import java.util.Arrays;
import java.util.List;

import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipGroup;
import com.tescobank.mobile.ui.tutorial.tooltips.CalendarFirstTimeUseTooltip;
import com.tescobank.mobile.ui.tutorial.tooltips.CalendarHeaderTooltip;
import com.tescobank.mobile.ui.tutorial.tooltips.CalendarSwipeTooltip;
import com.tescobank.mobile.ui.tutorial.tooltips.CalendarTransactionsToggleTooltip;

public class CalendarTooltipGroup implements TooltipGroup {

	private final List<? extends Tooltip> delayed;
	private final List<? extends Tooltip> mandatory;

	public CalendarTooltipGroup() {
		this.mandatory = Arrays.asList(new CalendarFirstTimeUseTooltip());
		this.delayed = Arrays.asList(new CalendarSwipeTooltip(), new CalendarHeaderTooltip(), new CalendarTransactionsToggleTooltip());
	}

	@Override
	public List<? extends Tooltip> getDelayedTooltips() {
		return this.delayed;
	}

	@Override
	public List<? extends Tooltip> getMandatoryTooltips() {
		return mandatory;
	}

}
