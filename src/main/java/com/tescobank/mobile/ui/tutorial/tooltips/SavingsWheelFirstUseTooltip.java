package com.tescobank.mobile.ui.tutorial.tooltips;

import android.util.DisplayMetrics;
import android.view.View;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipId;
import com.tescobank.mobile.ui.tutorial.TooltipLipDirection;

public class SavingsWheelFirstUseTooltip extends Tooltip {

	private boolean canShow;

	public SavingsWheelFirstUseTooltip(int numberOfLinkedAccounts) {
		super(TooltipId.SavingsWheelFirstUse.ordinal(), R.string.tooltip_saving_now_title, R.string.tooltip_savings_wheel_first_use, R.drawable.ic_tutorial_savings_wheel, TooltipLipDirection.NORTH);
		canShow = numberOfLinkedAccounts > 0 ? true : false;
	}

	@Override
	protected int getTargetX(DisplayMetrics metrics) {
		int[] location = new int[2];
		View view = getActivity().findViewById(R.id.saving_save_image);
		view.getLocationInWindow(location);
		return location[0] + view.getWidth() / 2;
	}

	@Override
	protected int getTargetY(DisplayMetrics metrics) {
		int[] location = new int[2];
		View view = getActivity().findViewById(R.id.saving_save_image);
		view.getLocationInWindow(location);
		return (location[1] + view.getHeight() / 2) + 30;
	}

	@Override
	public boolean canShow() {
		return canShow;
	}

}
