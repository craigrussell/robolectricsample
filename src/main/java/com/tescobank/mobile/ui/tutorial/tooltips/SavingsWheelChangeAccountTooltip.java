package com.tescobank.mobile.ui.tutorial.tooltips;

import android.util.DisplayMetrics;
import android.view.View;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipId;
import com.tescobank.mobile.ui.tutorial.TooltipLipDirection;

public class SavingsWheelChangeAccountTooltip extends Tooltip {

	private boolean canShow;

	public SavingsWheelChangeAccountTooltip(int numberOfLinkedAccounts) {
		super(TooltipId.SavingsWheelChangeAccount.ordinal(), R.string.tooltip_saving_now_title, R.string.tooltip_savings_wheel_change_account, R.drawable.ic_tutorial_save_change_acc, TooltipLipDirection.NORTH);
		canShow = numberOfLinkedAccounts > 1 ? true : false;
	}

	@Override
	protected int getTargetX(DisplayMetrics metrics) {
		int[] location = new int[2];
		View view = getActivity().findViewById(R.id.savings_funding_account_container);
		view.getLocationOnScreen(location);
		return location[0] + view.getWidth() / 2; 
	}

	@Override
	protected int getTargetY(DisplayMetrics metrics) {
		int[] location = new int[2];
		getActivity().findViewById(R.id.savings_funding_account_container).getLocationOnScreen(location);
		return location[1] + 40; 
	}
	
	@Override
	public boolean canShow() {
		return canShow;
	}

}
