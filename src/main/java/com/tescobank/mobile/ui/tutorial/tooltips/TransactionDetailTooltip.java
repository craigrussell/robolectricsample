package com.tescobank.mobile.ui.tutorial.tooltips;

import android.content.Context;
import android.util.DisplayMetrics;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class TransactionDetailTooltip extends Tooltip {

	private boolean canShow;
	private int xPosition;
	private int yPosition;
	
	public TransactionDetailTooltip(Context context,boolean transactionsPresent) {
		super(TooltipId.TransactionDetail.ordinal(), R.string.tooltip_title_default, R.string.tooltip_transaction_list_transaction_detail, R.drawable.ic_tutorial_transaction_view);
		xPosition = context.getResources().getDimensionPixelOffset(R.dimen.tooltip_transaction_x_position);
		yPosition = context.getResources().getDimensionPixelOffset(R.dimen.tooltip_transaction_y_position);
		canShow = transactionsPresent;
	}

	@Override
	protected int getTargetX(DisplayMetrics metrics) {
		return xPosition;
	}

	@Override
	protected int getTargetY(DisplayMetrics metrics) {
		return yPosition;
	}

	@Override
	public boolean canShow() {
		return canShow;
	}

}
