package com.tescobank.mobile.ui.tutorial.tooltips;

import android.util.DisplayMetrics;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class CalendarHeaderTooltip extends Tooltip {

	public CalendarHeaderTooltip() {
		super(TooltipId.CalendarHeader.ordinal(), R.string.tooltip_title_default, R.string.tooltip_calendar_header, R.drawable.ic_tutorial_in_out_difference);
	}

	@Override
	protected int getTargetX(DisplayMetrics metrics) {
		return metrics.widthPixels / 2;
	}

	@Override
	protected int getTargetY(DisplayMetrics metrics) {
		return (int) (150 * metrics.density);
	}

}
