package com.tescobank.mobile.ui.tutorial.groups;

import java.util.Arrays;
import java.util.List;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipGroup;
import com.tescobank.mobile.ui.tutorial.tooltips.TransactionCalendarToggleTooltip;
import com.tescobank.mobile.ui.tutorial.tooltips.TransactionDetailTooltip;
import com.tescobank.mobile.ui.tutorial.tooltips.TransactionSearchTooltip;
import com.tescobank.mobile.ui.tutorial.tooltips.TransactionSortTooltip;
import com.tescobank.mobile.ui.tutorial.tooltips.TransactionStatementToggleTooltip;

public class TransactionsListTooltipGroup implements TooltipGroup {

	private List<? extends Tooltip> mandatory;
	private List<? extends Tooltip> delayed;
	
	public TransactionsListTooltipGroup(Context context, boolean transactionsPresent, boolean hasCalendar, boolean statementsEnabled) {
		
		TransactionDetailTooltip detail = new TransactionDetailTooltip(context,transactionsPresent);
		TransactionCalendarToggleTooltip calendar = new TransactionCalendarToggleTooltip(context, 4, transactionsPresent, hasCalendar);
		TransactionSearchTooltip search = new TransactionSearchTooltip(context,getSearchActionBarSlot(hasCalendar, statementsEnabled),transactionsPresent);
		TransactionSortTooltip sort = new TransactionSortTooltip(context,getSortActionBarSlot(hasCalendar, statementsEnabled), transactionsPresent);
		TransactionStatementToggleTooltip statement = new TransactionStatementToggleTooltip(context,getStatmentActionBarSlot(hasCalendar), transactionsPresent, statementsEnabled);
		
		if(!isTablet(context) && hasCalendar && statementsEnabled) {
			calendar.setActionInOverflowMode(true);
			search.setActionInOverflowMode(true);
			sort.setActionInOverflowMode(true);
			statement.setActionInOverflowMode(true);
		}
		
		mandatory = Arrays.asList();
		delayed = Arrays.asList(detail, calendar, search, sort, statement);
	}
	
	private boolean isTablet(Context context) {
		return context.getResources().getBoolean(R.bool.tablet_size);
	}
	
	private int getSearchActionBarSlot(boolean hasCalendar, boolean statementsEnabled) {
		if(hasCalendar && statementsEnabled) {
			return 1;
		}
		return (hasCalendar || statementsEnabled) ? 2 : 3;
	}

	private int getSortActionBarSlot(boolean hasCalendar, boolean statementsEnabled) {
		if(hasCalendar && statementsEnabled) {
			return 2;
		}
		return (hasCalendar || statementsEnabled) ? 3 : 4;
	}
	
	private int getStatmentActionBarSlot(boolean hasCalendar) {
		return (hasCalendar) ? 3 : 4;
	}
	
	@Override
	public List<? extends Tooltip> getDelayedTooltips() {
		return delayed;
	}

	@Override
	public List<? extends Tooltip> getMandatoryTooltips() {
		return mandatory;
	}

}
