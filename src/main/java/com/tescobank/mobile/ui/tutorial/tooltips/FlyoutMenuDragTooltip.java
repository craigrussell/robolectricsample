package com.tescobank.mobile.ui.tutorial.tooltips;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class FlyoutMenuDragTooltip extends FlyoutMenuTooltip {

	private boolean canShow;

	public FlyoutMenuDragTooltip(Context context, boolean isProduct) {
		super(context, TooltipId.FlyoutMenuDrag.ordinal(), R.string.tooltip_title_default, R.string.tooltip_flyout_menu_drag_text, R.drawable.ic_tutorial_drag_flyout);
		this.canShow = isProduct;
	}

	@Override
	public boolean canShow() {
		return canShow;
	}

}
