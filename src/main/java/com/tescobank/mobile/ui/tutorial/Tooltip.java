package com.tescobank.mobile.ui.tutorial;

import android.app.Activity;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tescobank.mobile.R;

public abstract class Tooltip {

	private final int id;
	private final int messageResId;
	private final int iconResId;
	private final int titleResId;
	private final TooltipLipDirection forcedLipDirection;
	private Activity activity;

	/**
	 * With this constructor the Tooltip will work out where to place the lip (north/south) based on the target position
	 * 
	 * @param id use {@link TooltipId} enum to generate a unique id for this (must survive across app versions, recompiliation, refactoring, etc)
	 * @param titleResId the R.string.x of the title string
	 * @param messageResId the R.string.x of the message string
	 * @param iconResId the R.drawable.x of the icon to use or 0 to not use one
	 */
	public Tooltip(int id, int titleResId, int messageResId, int iconResId) {
		this(id, titleResId, messageResId, iconResId, null);
	}

	/**
	 * With this constructor the Tooltip will place the lip north/south depending on the parameter passed in (regardless of whether this will fit on the screen).
	 * 
	 * @param id use {@link TooltipId} enum to generate a unique id for this (must survive across app versions, recompiliation, refactoring, etc)
	 * @param titleResId the R.string.x of the title string
	 * @param messageResId the R.string.x of the message string
	 * @param iconResId the R.drawable.x of the icon to use or 0 to not use one
	 * @param forcedLipDirection the direction which lip should point
	 */
	public Tooltip(int id, int titleResId, int messageResId, int iconResId, TooltipLipDirection forcedLipDirection) {
		this.id = id;
		this.titleResId = titleResId;
		this.messageResId = messageResId;
		this.iconResId = iconResId;
		this.forcedLipDirection = forcedLipDirection;
	}

	public int getId() {
		return id;
	}

	/**
	 * The tooltip can show if it is next in the stack. Otherwise, the buck stops here.
	 * 
	 * @return true if it can show.
	 */
	public boolean canShow() {
		return true;
	}

	public int getMessageResId() {
		return messageResId;
	}

	public int getTitleResId() {
		return titleResId;
	}

	public int getIconResId() {
		return iconResId;
	}

	public void configureDetails(View tooltipLayout) {
		TextView.class.cast(tooltipLayout.findViewById(R.id.tutorial_tooltip_title)).setText(getTitleResId());
		TextView.class.cast(tooltipLayout.findViewById(R.id.tutorial_tooltip_message)).setText(getMessageResId());
		ImageView.class.cast(tooltipLayout.findViewById(R.id.tutorial_tooltip_icon)).setImageResource(getIconResId());
	}

	public Activity getActivity() {
		return activity;
	}

	void position(Activity activity, View tooltipLayout) {
		this.activity = activity;

		final DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
		final int targetX = getTargetX(metrics);
		final int targetY = getTargetY(metrics);
		final TooltipLipDirection lipDirection = forcedLipDirection != null ? forcedLipDirection : tooltipDirectionForTarget(metrics, targetY);
		final int tooltipWidth = activity.getResources().getDimensionPixelSize(R.dimen.tooltip_width);

		final View tutorialDetail = tooltipLayout.findViewById(R.id.tutorial_detail);

		int x = targetX - tooltipWidth / 2;
		int y = targetY;

		int offsetY = 0;
		View lip;

		switch (lipDirection) {

		case NORTH:
			lip = tooltipLayout.findViewById(R.id.tutorial_lip_north);
			showLip(lip);
			break;

		case SOUTH:
			lip = tooltipLayout.findViewById(R.id.tutorial_lip_south);
			showLip(lip);
			offsetY -= tutorialDetail.getHeight();
			break;
			
		default:
			throw new RuntimeException("lipDirection is null");
		}
		
		if (x < 0) {
			int offset = 0;
			while (x < 0) {
				x += 10;
				offset -= 10;
			}
			setTranslationX(lip, offset);
		}

		if (tooltipWidth + x > metrics.widthPixels) {
			int offset = 0;
			while (tooltipWidth + x > metrics.widthPixels) {
				x -= 10;
				offset += 10;
			}
			setTranslationX(lip, offset);
		}
		setTranslationX(tutorialDetail, x);
		setTranslationY(tutorialDetail, y + offsetY);
	}
	
	private void showLip(View lip) {
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			lip.setVisibility(View.VISIBLE);
		}
	}
	
	private void setTranslationY(View view, int offset) {
		view.setPadding(view.getPaddingLeft(), offset, view.getPaddingRight(), view.getPaddingBottom());
	}

	private void setTranslationX(View view, int offset) {
		view.setPadding(offset, view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
	}

	private TooltipLipDirection tooltipDirectionForTarget(DisplayMetrics metrics, int targetY) {
		return isTopHalfOfScreen(metrics, targetY) ? TooltipLipDirection.NORTH : TooltipLipDirection.SOUTH;
	}

	/** The position on screen from the left. */
	protected abstract int getTargetX(DisplayMetrics metrics);

	/** The position on screen from the top. */
	protected abstract int getTargetY(DisplayMetrics metrics);

	protected boolean isTopHalfOfScreen(DisplayMetrics metrics, int targetY) {
		return targetY < (metrics.heightPixels / 2);
	}

	protected boolean isLeftHalfOfScreen(DisplayMetrics metrics, int targetX) {
		return targetX < (metrics.widthPixels / 2);
	}

}
