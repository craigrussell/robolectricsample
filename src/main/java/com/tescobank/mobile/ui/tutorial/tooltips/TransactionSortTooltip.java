package com.tescobank.mobile.ui.tutorial.tooltips;

import android.content.Context;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class TransactionSortTooltip extends ActionBarTooltip {

	private boolean canShow;

	public TransactionSortTooltip(Context context, int actionBarSlot, boolean transactionsPresent) {
		super(context, actionBarSlot, TooltipId.TransactionSort.ordinal(), R.string.tooltip_title_default, R.string.tooltip_transaction_list_sort, R.drawable.ic_tutorial_transaction_sort);
		canShow = transactionsPresent;
	}

	@Override
	public boolean canShow() {
		return canShow;
	}
}
