package com.tescobank.mobile.ui.tutorial.tooltips;

import android.util.DisplayMetrics;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class StatementTransactionToggleTooltip extends Tooltip {

	public StatementTransactionToggleTooltip() {
		super(TooltipId.StatementTransactionToggle.ordinal(), R.string.tooltip_title_default, R.string.tooltip_statement_archive_transaction_list_toggle, R.drawable.ic_tutorial_transactions_view);
	}

	@Override
	public int getTargetX(DisplayMetrics metrics) {
		return metrics.widthPixels - (int) (32 * metrics.density);
	}

	@Override
	public int getTargetY(DisplayMetrics metrics) {
		return (int) (70 * metrics.density);
	}

}
