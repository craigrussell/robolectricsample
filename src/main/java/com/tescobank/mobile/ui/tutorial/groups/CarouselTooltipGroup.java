package com.tescobank.mobile.ui.tutorial.groups;

import java.util.Arrays;
import java.util.List;

import android.content.Context;

import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipGroup;
import com.tescobank.mobile.ui.tutorial.tooltips.ATMFinderFirstUseTooltip;
import com.tescobank.mobile.ui.tutorial.tooltips.AuthorisedSettingsTooltip;
import com.tescobank.mobile.ui.tutorial.tooltips.CalendarOnFlyoutMenuTooltip;
import com.tescobank.mobile.ui.tutorial.tooltips.ListViewTooltip;
import com.tescobank.mobile.ui.tutorial.tooltips.FlyoutMenuDragTooltip;
import com.tescobank.mobile.ui.tutorial.tooltips.FlyoutMenuIntroTooltip;

public class CarouselTooltipGroup implements TooltipGroup {

	private List<? extends Tooltip> mandatory;
	private List<? extends Tooltip> delayed;

	public CarouselTooltipGroup(Context context, boolean isAuthenticated, boolean atmFinder, boolean hasAtms, boolean hasCalendar) {
		mandatory = Arrays.asList(new FlyoutMenuIntroTooltip(context, !atmFinder), new ATMFinderFirstUseTooltip(context, atmFinder && hasAtms));
		delayed = Arrays.asList(new AuthorisedSettingsTooltip(isAuthenticated), new FlyoutMenuDragTooltip(context, !atmFinder), new CalendarOnFlyoutMenuTooltip(context, !atmFinder && hasCalendar), new ListViewTooltip());
	}

	@Override
	public List<? extends Tooltip> getDelayedTooltips() {
		return delayed;
	}

	@Override
	public List<? extends Tooltip> getMandatoryTooltips() {
		return mandatory;
	}

}
