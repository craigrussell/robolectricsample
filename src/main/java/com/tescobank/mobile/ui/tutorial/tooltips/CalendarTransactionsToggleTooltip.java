package com.tescobank.mobile.ui.tutorial.tooltips;

import android.util.DisplayMetrics;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.Tooltip;
import com.tescobank.mobile.ui.tutorial.TooltipId;

public class CalendarTransactionsToggleTooltip extends Tooltip {

	public CalendarTransactionsToggleTooltip() {
		super(TooltipId.CalendarTransactionsToggle.ordinal(), R.string.tooltip_calendar_title, R.string.tooltip_calendar_transactions_toggle, R.drawable.ic_tutorial_transactions_view);
	}

	@Override
	public int getTargetX(DisplayMetrics metrics) {
		return metrics.widthPixels - (int) (32 * metrics.density);
	}

	@Override
	public int getTargetY(DisplayMetrics metrics) {
		return (int) (70 * metrics.density);
	}


}
