package com.tescobank.mobile.ui.tutorial.tooltips;

import static com.tescobank.mobile.ui.tutorial.TooltipId.TransactionCalendarToggle;
import android.content.Context;

import com.tescobank.mobile.R;

public class TransactionCalendarToggleTooltip extends ActionBarTooltip {

	private boolean canShow;

	public TransactionCalendarToggleTooltip(Context context, int actionBarSlot, boolean transactionsPresent, boolean hasCalendar) {
		super(context, actionBarSlot, TransactionCalendarToggle.ordinal(), R.string.tooltip_title_default, R.string.tooltip_transaction_list_calendar_toggle, R.drawable.ic_tutorial_calendar_view);
		canShow = transactionsPresent && hasCalendar;
	}

	@Override
	public boolean canShow() {
		return canShow;
	}

}
