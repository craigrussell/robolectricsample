package com.tescobank.mobile.ui.tutorial.tooltips;

import android.content.Context;
import android.util.DisplayMetrics;

import com.tescobank.mobile.R;
import com.tescobank.mobile.ui.tutorial.Tooltip;

public class ActionBarTooltip extends Tooltip {
	
	private Context context;
	private int actionBarSlot;
	private boolean inOverflow;
	
	public ActionBarTooltip(Context context, int actionBarSlot, int id, int titleResId, int messageResId, int iconResId) {
		super(id, titleResId, messageResId, iconResId);
		this.context = context;
		this.actionBarSlot = actionBarSlot;
	}

	@Override
	protected int getTargetX(DisplayMetrics metrics) {
		return metrics.widthPixels - getXPositionOffset(context,actionBarSlot);
	}

	@Override
	protected int getTargetY(DisplayMetrics metrics) {
		return context.getResources().getDimensionPixelOffset(R.dimen.tooltip_actionbar_y_position);
	}
	
	public void setActionInOverflowMode(boolean inOverflow) {
		this.inOverflow = inOverflow;
	}
		
	private int getXPositionOffset(Context context, int actionBarSlot) {
		switch (actionBarSlot) {
		case 1:
			return inOverflow ? context.getResources().getDimensionPixelOffset(R.dimen.tooltip_actionbar_slot1_x_overflow_offset) : context.getResources().getDimensionPixelOffset(R.dimen.tooltip_actionbar_slot1_x_offset);
		case 2:
			return inOverflow ? context.getResources().getDimensionPixelOffset(R.dimen.tooltip_actionbar_slot2_x_overflow_offset) : context.getResources().getDimensionPixelOffset(R.dimen.tooltip_actionbar_slot2_x_offset);
		case 3:
			return inOverflow ? context.getResources().getDimensionPixelOffset(R.dimen.tooltip_actionbar_slot3_x_overflow_offset) : context.getResources().getDimensionPixelOffset(R.dimen.tooltip_actionbar_slot3_x_offset);
		case 4:
			return inOverflow ? context.getResources().getDimensionPixelOffset(R.dimen.tooltip_actionbar_slot4_x_overflow_offset) : context.getResources().getDimensionPixelOffset(R.dimen.tooltip_actionbar_slot4_x_offset);
		default:
			return 0;
		}
	}
}
