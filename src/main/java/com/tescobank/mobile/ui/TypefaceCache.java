package com.tescobank.mobile.ui;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;

/**
 * Workaround for font caching memory issue on pre Jelly Bean android Source
 * https://code.google.com/p/android/issues/detail?id=9904
 */
public final class TypefaceCache {

	private static final Map<String, Typeface> CACHE = new HashMap<String, Typeface>();
	
	private TypefaceCache() {
		super();
	}
 	
	public static Typeface get(Context c, String name) {
		return TypefaceCache.get(c.getAssets(),name);
	}
	
	public static Typeface get(AssetManager assets, String name) {
 
 		if (!CACHE.containsKey(name)) {
 			synchronized (CACHE) {
 				if (!CACHE.containsKey(name)) {
 					Typeface t = Typeface.createFromAsset(
							assets,
							String.format("fonts/%s.ttf", name));
					CACHE.put(name, t);
				}
			}
		}
		return CACHE.get(name);
	}
}