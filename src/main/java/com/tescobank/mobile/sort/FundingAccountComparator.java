package com.tescobank.mobile.sort;

import java.util.Comparator;

import com.tescobank.mobile.model.product.FundingAccount;

/**
 * Comparator for funding accounts
 */
public class FundingAccountComparator implements Comparator<FundingAccount> {
	
	private static final String EMPTY_STRING = "";
	
	public FundingAccountComparator() {
		super();
	}
	
	@Override
	public int compare(FundingAccount account1, FundingAccount account2) {
		
		int result = getSortCode(account1).compareTo(getSortCode(account2));
		if (result != 0) {
			return result;
		}
		
		return getAccountNumber(account1).compareTo(getAccountNumber(account2));
	}
	
	private String getSortCode(FundingAccount account) {
		if (account == null) {
			return EMPTY_STRING;
		}
		
		if (account.getSortCode() == null) {
			return EMPTY_STRING;
		}
		
		return account.getSortCode();
	}
	
	private String getAccountNumber(FundingAccount account) {
		if (account == null) {
			return EMPTY_STRING;
		}
		
		if (account.getAccountNumber() == null) {
			return EMPTY_STRING;
		}
		
		return account.getAccountNumber();
	}
}
