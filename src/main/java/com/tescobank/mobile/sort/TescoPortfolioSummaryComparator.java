package com.tescobank.mobile.sort;

import static com.tescobank.mobile.model.product.ProductType.typeForLabel;

import java.util.Comparator;

import com.tescobank.mobile.model.product.Product;

public class TescoPortfolioSummaryComparator implements Comparator<Product> {
	
	private static final int OFFSET = 4;

	@Override
	public int compare(Product first, Product second) {
		
		int returnCode = typeForLabel(first.getProductType()).ordinal() - typeForLabel(second.getProductType()).ordinal();
		if (returnCode != 0) {
			return returnCode;
		}
		
		int firstLength = first.getDisplayProductId().length();
		int secondLength = second.getDisplayProductId().length();
		
		if (isSavingsOrPcaOrLoan(first)) {
			return Integer.parseInt(first.getDisplayProductId().substring(firstLength-OFFSET)) - Integer.parseInt(second.getDisplayProductId().substring(secondLength-OFFSET));
		} else if (first.isCreditCard()) {
			return Integer.parseInt(first.getDisplayProductId().substring(firstLength-OFFSET)) - Integer.parseInt(second.getDisplayProductId().substring(secondLength-OFFSET));
		} else {
			return returnCode;
		}
	}
	
	private boolean isSavingsOrPcaOrLoan(Product product) {
		return product.isSavings() || product.isPersonalCurrentAccount() || product.isLoan();
	}
}
