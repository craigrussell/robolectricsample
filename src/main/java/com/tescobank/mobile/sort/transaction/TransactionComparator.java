package com.tescobank.mobile.sort.transaction;

import java.util.Comparator;

import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;

/**
 * Abstract base class for transaction comparators
 */
public abstract class TransactionComparator implements Comparator<Transaction> {
	
	private static final Integer ZERO = Integer.valueOf(0);
	
	public TransactionComparator() {
		super();
	}
	
	/**
	 * Compares the specified transactions by in / out
	 * 
	 * @param transaction1 the first transaction
	 * @param transaction2 the second transaction
	 * 
	 * @return the result
	 */
	protected int compareByInOut(Transaction transaction1, Transaction transaction2) {
		
		boolean isCredit1 = getIsCredit(transaction1);
		boolean isCredit2 = getIsCredit(transaction2);
		
		if (isCredit1 && !isCredit2) {
			return -1;
		} else if (!isCredit1 && isCredit2) {
			return 1;
		} else {
			return 0;
		}
	}
	
	private boolean getIsCredit(Transaction transaction) {
		
		if (transaction == null) {
			return false;
		}
		
		return transaction.isCredit();
	}
	
	/**
	 * Compares the specified transactions by in / out
	 * 
	 * @param transaction1 the first transaction
	 * @param transaction2 the second transaction
	 * 
	 * @return the result
	 */
	protected int compareByOriginalOrder(Transaction transaction1, Transaction transaction2) {
		return getOccurredIndex(transaction1).compareTo(getOccurredIndex(transaction2));
	}
	
	private Integer getOccurredIndex(Transaction transaction) {
		
		if (transaction == null) {
			return ZERO;
		}
		
		Integer occurredIndex = transaction.getOccurredIndex();
		if (occurredIndex == null) {
			return ZERO;
		}
		
		return occurredIndex;
	}
}
