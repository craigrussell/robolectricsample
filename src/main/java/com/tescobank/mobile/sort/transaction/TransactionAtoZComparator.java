package com.tescobank.mobile.sort.transaction;

import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;

/**
 * Comparator for transactions comparing them by A-to-Z values (transaction description)
 */
public class TransactionAtoZComparator extends TransactionComparator {
	
	private static final String EMPTY_STRING = "";
	
	public TransactionAtoZComparator() {
		super();
	}
	
	@Override
	public int compare(Transaction transaction1, Transaction transaction2) {
		
		String description1 = getDescription(transaction1);
		String description2 = getDescription(transaction2);
		
		// Compare by description
		int descriptionCompare = description1.compareToIgnoreCase(description2);
		if (descriptionCompare != 0) {
			return descriptionCompare;
		}
		
		// Finally compare by original order
		return compareByOriginalOrder(transaction1, transaction2);
	}
	
	private String getDescription(Transaction transaction) {
		
		if (transaction == null) {
			return EMPTY_STRING;
		}
		
		String description = transaction.getCondensedDescription();
		if (description == null) {
			return EMPTY_STRING;
		}
		
		return description;
	}
}
