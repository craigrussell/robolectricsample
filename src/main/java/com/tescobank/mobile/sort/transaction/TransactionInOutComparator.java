package com.tescobank.mobile.sort.transaction;

import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;

/**
 * Comparator for transactions comparing them by transaction direction (in / out)
 */
public class TransactionInOutComparator extends TransactionComparator {
	
	public TransactionInOutComparator() {
		super();
	}
	
	@Override
	public int compare(Transaction transaction1, Transaction transaction2) {
		
		// Compare by in / out
		int inOutCompare = compareByInOut(transaction1, transaction2);
		if (inOutCompare != 0) {
			return inOutCompare;
		}
		
		// Finally compare by original order
		return compareByOriginalOrder(transaction1, transaction2);
	}
}
