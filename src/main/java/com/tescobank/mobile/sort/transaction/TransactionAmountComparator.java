package com.tescobank.mobile.sort.transaction;

import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;

import com.tescobank.mobile.api.product.RetrievePagedTransactionsResponse.Transaction;

/**
 * Comparator for transactions comparing them by transaction amount
 */
public class TransactionAmountComparator extends TransactionComparator {
	
	public TransactionAmountComparator() {
		super();
	}
	
	@Override
	public int compare(Transaction transaction1, Transaction transaction2) {
		
		BigDecimal amount1 = getAmount(transaction1);
		BigDecimal amount2 = getAmount(transaction2);
		
		// Compare by amount, descending
		int amountCompare = amount2.compareTo(amount1);
		if (amountCompare != 0) {
			return amountCompare;
		}
		
		// Then compare by in / out
		int inOutCompare = compareByInOut(transaction1, transaction2);
		if (inOutCompare != 0) {
			return inOutCompare;
		}
		
		// Finally compare by original order
		return compareByOriginalOrder(transaction1, transaction2);
	}
	
	private BigDecimal getAmount(Transaction transaction) {
		
		if (transaction == null) {
			return ZERO;
		}
		
		BigDecimal amount = transaction.getAmount();
		if (amount == null) {
			return ZERO;
		}
		
		return amount;
	}
}
