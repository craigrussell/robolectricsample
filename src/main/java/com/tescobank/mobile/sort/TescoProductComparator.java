package com.tescobank.mobile.sort;

import static com.tescobank.mobile.model.product.ProductType.typeForLabel;

import java.util.Comparator;

import com.tescobank.mobile.model.product.Product;

public class TescoProductComparator implements Comparator<Product> {
	
	private static final int CREDIT_CARD_OFFSET = 12;
	private static final int ACCOUNT_OFFSET = 6;

	@Override
	public int compare(Product first, Product second) {
		
		int returnCode = typeForLabel(first.getProductType()).ordinal() - typeForLabel(second.getProductType()).ordinal();
		if (returnCode != 0) {
			return returnCode;
		}
		
		if (isSavingsOrPcaOrLoan(first)) {
			return Integer.parseInt(first.getProductId().substring(ACCOUNT_OFFSET)) - Integer.parseInt(second.getProductId().substring(ACCOUNT_OFFSET));
		} else if (first.isCreditCard()) {
			return Integer.parseInt(first.getProductId().substring(CREDIT_CARD_OFFSET)) - Integer.parseInt(second.getProductId().substring(CREDIT_CARD_OFFSET));
		} else {
			return returnCode;
		}
	}
	
	private boolean isSavingsOrPcaOrLoan(Product product) {
		return product.isSavings() || product.isPersonalCurrentAccount() || product.isLoan();
	}
}
