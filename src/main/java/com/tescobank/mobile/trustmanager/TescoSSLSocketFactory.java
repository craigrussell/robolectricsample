package com.tescobank.mobile.trustmanager;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import android.util.Log;

public class TescoSSLSocketFactory {
	
    private static final String TAG = TescoSSLSocketFactory.class.getName();
    private final static String SSL_CONTEXT ="TLS";
    private SSLSocketFactory factory;

	
	public SSLSocketFactory getTescoSSLSocketFactory() {
        try {
            if (factory == null) {
                SSLContext sslContext = SSLContext.getInstance(SSL_CONTEXT);
                sslContext.init(null, new TrustManager[]{new TescoTrustManager(getDefaultX509TM())}, null);
                factory = sslContext.getSocketFactory();
            }
        } catch (Exception e) {
            Log.d(TAG, "Failed to create Socket Factory from KeyStore", e);
        }
        return factory;
    }

    private X509TrustManager getDefaultX509TM() throws NoSuchAlgorithmException, KeyStoreException {
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init((KeyStore) null);
        X509TrustManager x509Tm = null;
        for (TrustManager tm : tmf.getTrustManagers()) {
            if (tm instanceof X509TrustManager) {
                x509Tm = (X509TrustManager) tm;
                break;
            }
        }
        return x509Tm;
    }
}
