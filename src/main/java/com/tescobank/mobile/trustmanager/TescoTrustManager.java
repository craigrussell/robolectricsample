package com.tescobank.mobile.trustmanager;

import android.util.Log;
import com.tescobank.mobile.BuildConfig;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

public class TescoTrustManager implements X509TrustManager {

    private X509TrustManager defaultTrustManager;
    private static final String TAG = TescoTrustManager.class.getName();

    public TescoTrustManager(final X509TrustManager defaultTrustManager) {
        super();
        if (defaultTrustManager == null) {
            throw new IllegalArgumentException("Trust manager must not be null");
        }
        this.defaultTrustManager = defaultTrustManager;
    }

    public void checkClientTrusted(X509Certificate[] certificates, String authType) throws CertificateException {
        defaultTrustManager.checkClientTrusted(certificates, authType);
    }

    public void checkServerTrusted(X509Certificate[] certificates, String authType) throws CertificateException {
        if (BuildConfig.DEBUG) {
            for (int c = 0; c < certificates.length; c++) {
                X509Certificate cert = certificates[c];
                Log.v(TAG, " Server Cert "+c+" Subject DN: " + cert.getSubjectDN());
                Log.v(TAG, " Server Cert "+c+" Issuer DN: " + cert.getIssuerDN());
            }
        }
        defaultTrustManager.checkServerTrusted(getOrderedCertificates(certificates), authType);
    }

    public X509Certificate[] getAcceptedIssuers() {
        return this.defaultTrustManager.getAcceptedIssuers();
    }

    private X509Certificate[] getOrderedCertificates(X509Certificate[] certificates) {
        ArrayList<X509Certificate> orderedCertificates = new ArrayList<X509Certificate>();
        X509Certificate current = (certificates.length>0) ? certificates[0] : null;
        while(current!=null){
            orderedCertificates.add(current);
            current = getNextCertificateInChain(current,certificates);
        }
        return orderedCertificates.toArray(new X509Certificate[0]);
    }

    private X509Certificate getNextCertificateInChain(X509Certificate current, X509Certificate[] certificates) {
        for(X509Certificate cert : certificates){
            if(!isRootCertificate(current) && current.getIssuerDN().equals(cert.getSubjectDN())){
                return cert;
            }
        }
        return null;
    }

    private boolean isRootCertificate(X509Certificate current){
        return current.getIssuerDN().equals(current.getSubjectDN());
    }
}
