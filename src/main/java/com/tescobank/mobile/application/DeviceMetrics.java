package com.tescobank.mobile.application;

import java.io.Serializable;

public interface DeviceMetrics extends Serializable {
	
	String getDeviceId();
	
	void setDeviceId(String deviceId);
	
	String getDeviceModel();

	void setDeviceModel(String deviceModel);
	
	String getDeviceOsVersion();

	void setDeviceOsName(String deviceOsName);
	
	String getDeviceOsName();
	
	void setDeviceOsVersion(String deviceOsVersion);

	String getClientAppName();
	
	void setClientAppName(String clientAppName);
	
	String getClientAppVersion();

	void setClientAppVersion(String clientAppVersion);

	String getLanguage();
	
	void setLanguage(String language);

	String getTimezone();

	void setTimezone(String timezone);
	
	Integer getFullHeight();
	
	void setFullHeight(Integer fullHeight);

	Integer getAvlHeight();
	
	void setAvlHeight(Integer avlHeight);

	Integer getFullWidth();
	
	void setFullWidth(Integer fullWidth);
	
	Integer getAvlWidth();

	void setAvlWidth(Integer avlWidth);

	String getMac();	

	void setMac(String mac);
	
	String getExternalIP();
	
	void setExternalIP(String externalIP);
		
	String getInternalIP();
	
	void setInternalIP(String internalIP);

	Boolean getDeviceIsJailbroken();
	
	void setDeviceIsJailbroken(Boolean deviceIsJailbroken);
}