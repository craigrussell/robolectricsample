package com.tescobank.mobile.application;



public class TescoDeviceMetrics implements DeviceMetrics {

	private static final long serialVersionUID = -7571656988123038122L;

	private String deviceId;
	
	private String deviceModel;
	
	private String deviceOsName;
	
	private String deviceOsVersion;
	
	private Boolean deviceIsJailbroken;
	
	private String clientAppVersion;
	
	private String clientAppName;
	
	private String language;
	
	private String timezone;
	
	private Integer fullHeight;
	
	private Integer fullWidth;

	private Integer avlHeight;
		
	private Integer avlWidth;

	private String mac;
	
	private String externalIP;
	
	private String internalIP;
	
	public TescoDeviceMetrics() {
	}
	
	public TescoDeviceMetrics(String deviceId, DeviceInfo info) {
		setDeviceId(deviceId);
		if(info != null) {
			setDeviceModel(info.getDeviceModel());
			setDeviceOsVersion(info.getDeviceOsVersion());
			setDeviceOsName(info.getDeviceOsName());
			setDeviceIsJailbroken(info.isRooted());
			setClientAppVersion(info.getVersionName());
			setClientAppName(info.getAppName());
			setLanguage(info.getLanguage());
			setTimezone(info.getTimeZone());
			setFullHeight(info.getScreenDimensions().getHeight());
			setFullWidth(info.getScreenDimensions().getWidth());
			setAvlHeight(info.getScreenDimensions().getHeight());
			setAvlWidth(info.getScreenDimensions().getWidth());
			setMac(info.getMacAddress());
			setExternalIP(info.getIPAddress());
			setInternalIP(info.getIPAddress());
		}
	}

	@Override
	public String getDeviceId() {
		return deviceId;
	}
	
	@Override
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public String getDeviceModel() {
		return deviceModel;
	}

	@Override
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	@Override
	public String getDeviceOsName() {
		return deviceOsName;
	}

	@Override
	public void setDeviceOsName(String deviceOsName) {
		this.deviceOsName = deviceOsName;
	}

	@Override
	public String getDeviceOsVersion() {
		return deviceOsVersion;
	}
	
	@Override
	public void setDeviceOsVersion(String deviceOsVersion) {
		this.deviceOsVersion = deviceOsVersion;
	}

	@Override
	public Boolean getDeviceIsJailbroken() {
		return deviceIsJailbroken;
	}
	
	@Override
	public void setDeviceIsJailbroken(Boolean deviceIsJailbroken) {
		this.deviceIsJailbroken = deviceIsJailbroken;
	}
	
	@Override
	public String getClientAppVersion() {
		return clientAppVersion;
	}
	
	@Override
	public void setClientAppVersion(String clientAppVersion) {
		this.clientAppVersion = clientAppVersion;
	}
	
	@Override
	public String getClientAppName() {
		return clientAppName;
	}

	@Override
	public void setClientAppName(String clientAppName) {
		this.clientAppName = clientAppName;
	}
	
	@Override
	public String getLanguage() {
		return language;
	}

	@Override
	public void setLanguage(String language) {
		this.language = language;
	}

	@Override
	public String getTimezone() {
		return timezone;
	}

	@Override
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	@Override
	public Integer getFullHeight() {
		return fullHeight;
	}

	@Override
	public void setFullHeight(Integer fullHeight) {
		this.fullHeight = fullHeight;
	}

	@Override
	public Integer getFullWidth() {
		return fullWidth;
	}

	@Override
	public void setFullWidth(Integer fullWidth) {
		this.fullWidth = fullWidth;
	}

	@Override
	public Integer getAvlHeight() {
		return avlHeight;
	}

	@Override
	public void setAvlHeight(Integer avlHeight) {
		this.avlHeight = avlHeight;
	}

	@Override
	public Integer getAvlWidth() {
		return avlWidth;
	}

	@Override
	public void setAvlWidth(Integer avlWidth) {
		this.avlWidth = avlWidth;
	}

	@Override
	public String getMac() {
		return mac;
	}

	@Override
	public void setMac(String mac) {
		this.mac = mac;
	}

	@Override
	public String getExternalIP() {
		return externalIP;
	}

	@Override
	public void setExternalIP(String externalIP) {
		this.externalIP = externalIP;
	}

	@Override
	public String getInternalIP() {
		return internalIP;
	}

	@Override
	public void setInternalIP(String internalIP) {
		this.internalIP = internalIP;
	}
}
