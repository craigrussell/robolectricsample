package com.tescobank.mobile.application;

import android.location.Location;

/**
 * Provides location tracking functionality. Implementations MUST:
 * <ul>
 * <li>send a local broadcast with the action set to {@link TescoLocationProvider#LOCATION_STATUS_BROADCAST_EVENT} when the status changes. The intent MUST contain a {@link Serializable} extra with a key equal to {@link Status#getClass().getSimpleName()} containing the new {@link Status}.</li>
 * <li>track the availability of location services.</li>
 * <li>respect the {@link Persister.UseLocationPreference} value.</li>
 * </ul>
 */
public interface TescoLocationProvider {

	String LOCATION_STATUS_BROADCAST_EVENT = TescoLocationProvider.class.getName();

	/**
	 * Get the current known location, or null. May also return null if the user has disabled location tracking either globally or via app preferences, etc.
	 * 
	 * @return the current known location or null
	 */
	Location getLocation();

	Status getLocationStatus();

	void startTracking();

	void update();

	void stopTracking();

	boolean isUserEnabled();

	enum Status {
		/** Location is ok to use. */
		OK,

		/** Location is currently being determined. */
		IN_PROGRESS,

		/** User has disabled the location. */
		USER_DISABLED,

		/** The location is disabled at the system level. */
		SYSTEM_DISABLED;

	}
}
