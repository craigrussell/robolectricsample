package com.tescobank.mobile.application;

import android.content.Intent;

public class ActivityFinishSpec {

	private boolean hasResult;
	private int resultCode;
	private Intent data;
	
	public ActivityFinishSpec() {}
	
	public ActivityFinishSpec(int resultCode, Intent data) {
		this.hasResult = true;
		this.resultCode = resultCode;
		this.data = data;
	}

	public Intent getData() {
		return data;
	}

	public int getResultCode() {
		return resultCode;
	}

	public boolean hasResult() {
		return hasResult;
	}	
}
