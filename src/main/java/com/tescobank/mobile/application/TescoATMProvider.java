package com.tescobank.mobile.application;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.maps.model.LatLng;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.storefinder.RetrieveStoreLocationsRequest;
import com.tescobank.mobile.api.storefinder.RetrieveStoreLocationsResponse;
import com.tescobank.mobile.api.storefinder.Store;
import com.tescobank.mobile.ui.atm.ATMProvider;

public class TescoATMProvider implements ATMProvider {

	private static final double LOCATION_ROUNDER = 10000.0;
	private static final int STORE_COUNT = 20;

	private Context context;

	private LatLng cachedLatLng;
	private List<Store> cachedStores;
	private ApplicationState appState;

	public TescoATMProvider(Context context, ApplicationState appState) {
		this.context = context;
		this.appState = appState;
	}

	@Override
	public void retrieveCachableATMsForLatLng(LatLng latLng) {
		if (cachedLatLng != null && cachedLatLng.equals(latLng)) {
			sendSuccessBroadcast(cachedLatLng, cachedStores);
		} else {
			makeRequest(latLng, true);
		}
	}

	@Override
	public void retrieveATMsForLatLng(LatLng latLng) {
		makeRequest(latLng, false);
	}

	private void makeRequest(final LatLng latLng, final boolean shouldCache) {

		RetrieveStoreLocationsRequest request = new RetrieveStoreLocationsRequest() {
			@Override
			public void onResponse(RetrieveStoreLocationsResponse response) {
				if (response.getStores().size() > 0) {
					if (shouldCache) {
						cachedLatLng = latLng;
						cachedStores = response.getStores();
					}
					sendSuccessBroadcast(latLng, response.getStores());
				} else {
					sendErrorBroadcast(null);
				}
			}

			@Override
			public void onErrorResponse(ErrorResponseWrapper errorResponse) {
				sendErrorBroadcast(errorResponse);
			}
		};
		request.setFilter("ATM");
		double roundedLatitude = Math.round(latLng.latitude * LOCATION_ROUNDER) / LOCATION_ROUNDER;
		double roundedLongitude = Math.round(latLng.longitude * LOCATION_ROUNDER) / LOCATION_ROUNDER;
		request.setLatitude(roundedLatitude);
		request.setLongitude(roundedLongitude);
		request.setQuantity(STORE_COUNT);
		appState.getRestRequestProcessor().processRequest(request);
	}

	private void sendSuccessBroadcast(LatLng latLng, List<Store> stores) {
		Intent intent = new Intent(ATM_RETRIEVAL_BROADCAST_EVENT);
		intent.putExtra(ATM_RETRIEVAL_STATUS_EXTRA, ATMRetrievalStatus.SUCCESS);
		intent.putExtra(Location.class.getName(), latLng);
		intent.putExtra(Store.class.getName(), new ArrayList<Store>(stores));
		LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
	}

	private void sendErrorBroadcast(ErrorResponseWrapper errorResponseWrapper) {
		Intent intent = new Intent(ATM_RETRIEVAL_BROADCAST_EVENT);
		intent.putExtra(ATM_RETRIEVAL_STATUS_EXTRA, ATMRetrievalStatus.FAIL);
		intent.putExtra(ErrorResponseWrapper.class.getName(), errorResponseWrapper);
		LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
	}
}
