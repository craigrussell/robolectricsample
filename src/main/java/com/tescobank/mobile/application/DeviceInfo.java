package com.tescobank.mobile.application;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.R;

/**
 * Accessor for device-specific information
 */
public class DeviceInfo {
	
	private static final String TAG = DeviceInfo.class.getSimpleName();
	
	private Context context;
	private boolean isRooted;

	/**
	 * Constructor
	 * 
	 * @param context the context
	 */
	public DeviceInfo(Context context) {
		super();
		this.context = context;
	}
	
	/**
	 * @return the device model
	 */
	public String getDeviceModel() {
		return Build.MODEL;
	}
	
	/**
	 * @return the device OS name
	 */
	public String getDeviceOsName() {
		return "Android";
	}
	
	/**
	 * @return the device OS version
	 */
	public String getDeviceOsVersion() {
		return android.os.Build.VERSION.RELEASE;
	}
	
	/**
	 * @return the application name
	 */
	public String getAppName() {
		return context.getString(R.string.app_name);
	}
	
	/**
	 * @return the version name
	 */
	public String getVersionName() {
		try {
			return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, e.getMessage());
			}
		}
		return null;
	}
	
	/**
	 * @return whether the device is rooted
	 */
	public boolean isRooted() {
		return Boolean.TRUE.equals(isRooted);
	}
	
	/**
	 * @param isRooted whether the device is rooted
	 */
	public void setRooted(boolean isRooted) {
		this.isRooted = isRooted;
	}
	
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return Locale.getDefault().getDisplayLanguage();
	}
	
	/**
	 * @return the time zone
	 */
	public String getTimeZone() {
		return TimeZone.getDefault().getDisplayName();
	}
	
	/**
	 * @return the device's MAC address
	 */
	public String getMacAddress() {
		WifiManager wifiMan = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInf = wifiMan.getConnectionInfo();
		return wifiInf.getMacAddress();
	}


	public String getIPAddress() {

		Enumeration<NetworkInterface> networks = tryGettingNetworkInterfaces();
		if(networks == null) {
			return null;
		}
		
		while (networks.hasMoreElements()) {
			NetworkInterface current = networks.nextElement();
			String ipAddress = findAnIPAddressInInterfaceList(current.getInterfaceAddresses());
			if (ipAddress != null) {
				return ipAddress;
			}
		}
		return null;
	}

	private Enumeration<NetworkInterface> tryGettingNetworkInterfaces() {
		try {
			return NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e) {}
		return null;
	}
	
	private String findAnIPAddressInInterfaceList(List<InterfaceAddress> addresses) {
		for (InterfaceAddress address : addresses) {
			InetAddress current = address.getAddress();
			if (current.isSiteLocalAddress()) {
				return current.getHostAddress();
			}
		}
		return null;
	}
	
	/**
	 * @return the screen dimensions
	 */
	@SuppressWarnings("deprecation")
	public Dimensions getScreenDimensions() {
		
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		
		// Using deprecated getWidth() and getHeight() as getSize() is only available from api 13
		return new Dimensions(display.getWidth(),display.getHeight());
	}
	
	/**
	 * Screen dimensions
	 */
	public static class Dimensions {
		
		private int width;
		private int height;

		public Dimensions(int width, int height) {
			this.width = width;
			this.height = height;
		}
		
		public int getWidth() {
			return width;
		}
		
		public int getHeight() {
			return height;
		}
	}
}
