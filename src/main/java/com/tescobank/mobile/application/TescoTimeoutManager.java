package com.tescobank.mobile.application;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.ui.TescoActivity;

public class TescoTimeoutManager implements TimeoutManager {

	private static final String TAG = TescoTimeoutManager.class.getSimpleName();

	private final long inactivityTimeout;
	private final long backgroundTimeout;

	private long lastUserInteractionTime;
	private long lastBackgroundEventTime;
	private boolean inBackground;
	private boolean startedNewActivity;

	private TescoActivity activity;
	private ActivityStartSpec deferredStartActivity;
	private ActivityFinishSpec deferredFinishActivity;

	private ScheduledFuture<?> timeoutMonitor;

	public TescoTimeoutManager(TescoActivity activity, long inactivityTimeout, long backgroundTimeout) {
		this.activity = activity;
		this.inactivityTimeout = inactivityTimeout;
		this.backgroundTimeout = backgroundTimeout;
	}

	@Override
	public void onActivityResumed() {
		inBackground = false;
		startedNewActivity = false;
		onUserInteraction();
		startTimeoutMonitor();
		executeDeferredStart();
		executeDeferredFinish();
	}

	private void onNewInternalActivityStarted() {
		startedNewActivity = true;
		stopTimeoutMonitor();
	}

	@Override
	public void onActivityPaused() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Activity paused " + activity);
		}
		onActivityMovedToBackground();
	}
	
	@Override
	public void onActivityRestoredFromSavedState(long savedTime) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Activity restored by system after " + (System.currentTimeMillis() - savedTime) + "ms elapsed " + activity);
		}
		if (backgroundTimeOutBreachedWhileSaved(savedTime)) {
			activity.logOut();
		}
	}
	
	private boolean backgroundTimeOutBreachedWhileSaved(long savedTime) {
		return (System.currentTimeMillis() - savedTime) > backgroundTimeout;
	}

	@Override
	public void onActivityDestroyed() {
		stopTimeoutMonitor();
	}

	private void onActivityMovedToBackground() {
		lastBackgroundEventTime = System.currentTimeMillis();
		inBackground = true;
	}

	@Override
	public void onUserInteraction() {
		lastUserInteractionTime = System.currentTimeMillis();
	}

	private void startTimeoutMonitor() {
		stopTimeoutMonitor();
		timeoutMonitor = createInactivityMonitor();
	}

	private void stopTimeoutMonitor() {
		if (timeoutMonitor != null) {
			timeoutMonitor.cancel(true);
			timeoutMonitor = null;
		}
	}

	@Override
	public boolean inactivityTimeOutReached() {
		return (System.currentTimeMillis() - lastUserInteractionTime) > inactivityTimeout;
	}
	
	@Override
	public boolean backgroundTimeOutReached() {
		return inBackground && ((System.currentTimeMillis() - lastBackgroundEventTime) > backgroundTimeout);
	}
	
	@Override
	public boolean isAppInBackground() {
		return inBackground && !startedNewActivity;
	}

	private ScheduledFuture<?> createInactivityMonitor() {
		return Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				if (inactivityTimeOutReached() || backgroundTimeOutReached()) {
					closeApplicationFromMainThread();
				}
			}
		}, 0, 1, TimeUnit.SECONDS);
	}

	private void closeApplicationFromMainThread() {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				closeApplication();
			}
		});
	}

	private void closeApplication() {
		if (BuildConfig.DEBUG) {
			if (inBackground) {
				Log.d(TAG, "App backgrounded for " + (System.currentTimeMillis() - lastBackgroundEventTime) + "ms. Shutting down. " + activity);
			} else {
				Log.d(TAG, "App inactive for " + (System.currentTimeMillis() - lastUserInteractionTime) + "ms. Shutting down. " + activity);
			}
		}
		stopTimeoutMonitor();
		activity.hideKeyboard();
		ActivityCompat.finishAffinity(activity);
	}

	@Override
	public void startActivity(ActivityStartSpec activitySpec) {
		if (inBackground) {
			startActivityLater(activitySpec);
		} else {
			startActivityNow(activitySpec);
		}
	}

	private void startActivityLater(ActivityStartSpec activitySpec) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "In background, deferring start activity " + activitySpec.getIntent().toString());
		}
		deferredStartActivity = activitySpec;
	}
	
	private void startActivityNow(ActivityStartSpec activitySpec) {
		if(isActivityWithinApp(activitySpec.getIntent())) {
			onNewInternalActivityStarted();
		}
		activity.startActivityNow(activitySpec);
	}

	@Override
	public void finishActivity(ActivityFinishSpec activitySpec) {
		if (inBackground) {
			finishActivityLater(activitySpec);
		} else {
			activity.finishActivityNow(activitySpec);
		}	
	}

	private void finishActivityLater(ActivityFinishSpec activitySpec) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "In background, deferring finish activity " + activity.getClass().getName());
		}
		deferredFinishActivity = activitySpec;
	}

	private void executeDeferredStart() {
		if (deferredStartActivity != null) {
			activity.startActivityNow(deferredStartActivity);
			deferredStartActivity = null;
		}
	}
	
	private void executeDeferredFinish() {
		if(deferredFinishActivity != null) {
			activity.finishActivityNow(deferredFinishActivity);
			deferredFinishActivity = null;
		}
	}
	
	private boolean isActivityWithinApp(Intent intent) {
		String packageName = intent.getComponent() != null ? intent.getComponent().getPackageName() : null;
		return activity.getPackageName().equals(packageName);
	}
}
