package com.tescobank.mobile.application;

public interface TimeoutManager {

	String SAVED_TIME_KEY = "SAVED_TIME_KEY";

	void onActivityResumed();

	void onActivityPaused();

	void onActivityRestoredFromSavedState(long savedTime);

	void onActivityDestroyed();

	void onUserInteraction();
	
	void startActivity(ActivityStartSpec activityStartSpec);

	void finishActivity(ActivityFinishSpec activityFinishSpec);

	boolean inactivityTimeOutReached();

	boolean backgroundTimeOutReached();

	boolean isAppInBackground();
}
