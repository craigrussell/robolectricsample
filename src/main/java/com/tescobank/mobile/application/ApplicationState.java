package com.tescobank.mobile.application;

import java.math.BigDecimal;
import java.util.List;

import android.os.Bundle;

import com.tescobank.mobile.analytics.TescoAnalyticsTracker;
import com.tescobank.mobile.api.configuration.FeaturesResponse;
import com.tescobank.mobile.api.configuration.SettingsResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.headers.RequestHeadersProvider;
import com.tescobank.mobile.api.payment.GetTransacteesForUserResponse.Transactee;
import com.tescobank.mobile.api.payment.RetrieveFundingAccountsResponse.Account;
import com.tescobank.mobile.inauth.InAuthController;
import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.persist.Persister;
import com.tescobank.mobile.persist.Persister.UseLocationPreference;
import com.tescobank.mobile.properties.TescoApplicationPropertiesReader.ApplicationPropertyKeys;
import com.tescobank.mobile.rest.processor.RestRequestProcessor;
import com.tescobank.mobile.security.SecurityProvider;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.atm.ATMProvider;
import com.tescobank.mobile.ui.regpay.RegularPaymentsAdapter;
import com.tescobank.mobile.ui.transaction.TransactionsListAdapter;

public interface ApplicationState {

	void resetApplication();

	void resetForNewSession();

	void persistSensitiveData();

	RestRequestProcessor<ErrorResponseWrapper> getRestRequestProcessor();

	SecurityProvider getSecurityProvider();

	RequestHeadersProvider getRequestHeadersProvider();

	TescoAnalyticsTracker getAnalyticsTracker();
	
	TimeoutManager createTimeoutManager(TescoActivity activity, long inactivityTimeout, long backgroundTimeout);
	
	String getDeviceId();

	void setDeviceId(String deviceId);

	String getCssoId();

	void setCssoId(String cssoId);

	String getTbId();

	String getHashedTbId();

	void setTbId(String tbId);

	boolean isPassword();

	void setIsPassword(boolean isPassword);

	UseLocationPreference getUseLocationPreference();

	void setUseLocationPreference(UseLocationPreference preference);

	void notifyLocationChanged();
	
	SettingsResponse getSettings();

	void setSettings(SettingsResponse settings);

	FeaturesResponse getFeatures();

	void setFeatures(FeaturesResponse features);
	
	TescoLocationProvider getLocationProvider();
	
	ATMProvider getATMProvider();

	/**
	 * Note: This method is potentially expensive. Occasional calls should be
	 * fine, but it's not designed to be called in a loop, etc.
	 * 
	 * @return true if the user is registered.
	 */
	boolean isRegistered();

	/**
	 * @return the products
	 */
	List<Product> getProducts();

	/**
	 * @param products
	 *            the products to set
	 */
	void setProducts(List<Product> products);

	/**
	 * @return the productDetails
	 */
	List<ProductDetails> getProductDetails();

	void updateProductDetails(List<ProductDetails> newProductDetails);

	List<ProductDetails> getDirtyProductDetails();

	/**
	 * @return the funding accounts
	 */
	Account[] getFundingAccounts();

	/**
	 * @param fundingAccounts
	 *            the funding accounts to set
	 */
	void setFundingAccounts(Account[] fundingAccounts);

	/**
	 * @param productDetails
	 *            the productDetails to set
	 */
	void setProductDetails(List<ProductDetails> productDetails);

	/**
	 * @return the transactees
	 */
	List<Transactee> getTransactees();

	/**
	 * @param transactees
	 *            the transactees to set
	 */
	void setTransactees(List<Transactee> transactees);

	/**
	 * @return whether the 'network connection lost' message has already been
	 *         displayed
	 */
	boolean isNetworkConnectionLostMessageDisplayed();

	/**
	 * @param networkConnectionLostMessageDisplayed
	 *            whether the 'network connection lost' message has already been
	 *            displayed
	 */
	void setNetworkConnectionLostMessageDisplayed(boolean networkConnectionLostMessageDisplayed);

	void setTransactionSortOrderForProduct(TransactionsListAdapter.SortOrder sort, ProductDetails product);

	void setRegularPaymentSortOrderForProduct(RegularPaymentsAdapter.SortOrder sort, ProductDetails product);

	TransactionsListAdapter.SortOrder getTransactionSortOrderForProduct(ProductDetails product);

	RegularPaymentsAdapter.SortOrder getRegularPaymentSortOrderForProduct(ProductDetails productDetails);
	
	void incrementSubsequentLoginCount();
	
	void enableBalancePeek(boolean enabled);
	
	boolean shouldDisplayBalancePeekTutorial();
	
	void hasSeenBalancePeekTutorial();
		
	DeviceInfo getDeviceInfo();

	Persister getPersister();
	
	InAuthController getInAuthController();

	BigDecimal getNextTaxYearSubscriptionLimit(ProductDetails product);
		
	String getApplicationPropertyValueForKey(ApplicationPropertyKeys propertyValue);

	void saveData(Bundle outState);

	void restoreData(Bundle savedState);
}
