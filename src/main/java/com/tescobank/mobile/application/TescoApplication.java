package com.tescobank.mobile.application;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.analytics.AnalyticsEventFactory;
import com.tescobank.mobile.analytics.AnalyticsTracker;
import com.tescobank.mobile.analytics.AnalyticsTrackerFactory;
import com.tescobank.mobile.analytics.TescoAnalyticsTracker;
import com.tescobank.mobile.api.configuration.FeaturesResponse;
import com.tescobank.mobile.api.configuration.SettingsResponse;
import com.tescobank.mobile.api.error.ErrorResponseWrapper;
import com.tescobank.mobile.api.error.ErrorResponseWrapperBuilder;
import com.tescobank.mobile.api.headers.RequestHeadersProvider;
import com.tescobank.mobile.api.headers.RequestHeadersProvider.Credential;
import com.tescobank.mobile.api.headers.ResponseHeadersHandler;
import com.tescobank.mobile.api.headers.TescoLocation;
import com.tescobank.mobile.api.payment.GetTransacteesForUserResponse.Transactee;
import com.tescobank.mobile.api.payment.RetrieveFundingAccountsResponse.Account;
import com.tescobank.mobile.encode.HMACSHA256Encoder;
import com.tescobank.mobile.encrypt.CouldNotGenerateSecretKeyException;
import com.tescobank.mobile.encrypt.Encrypter;
import com.tescobank.mobile.encrypt.EncrypterFactory;
import com.tescobank.mobile.encrypt.SecretKeyFactory;
import com.tescobank.mobile.inauth.InAuthController;
import com.tescobank.mobile.inauth.MobileInAuthController;
import com.tescobank.mobile.json.JsonParserFactory;
import com.tescobank.mobile.model.product.Product;
import com.tescobank.mobile.model.product.ProductDetails;
import com.tescobank.mobile.model.product.ProductType;
import com.tescobank.mobile.network.NetworkStatusChangeReceiver;
import com.tescobank.mobile.persist.Persister;
import com.tescobank.mobile.persist.Persister.UseLocationPreference;
import com.tescobank.mobile.persist.PersisterFactory;
import com.tescobank.mobile.properties.TescoApplicationPropertiesReader;
import com.tescobank.mobile.properties.TescoApplicationPropertiesReader.ApplicationPropertyKeys;
import com.tescobank.mobile.rest.processor.RestConfig;
import com.tescobank.mobile.rest.processor.RestRequestProcessor;
import com.tescobank.mobile.rest.processor.RestRequestProcessorFactory;
import com.tescobank.mobile.rest.processor.volley.VolleyRestRequestProcessorFactory;
import com.tescobank.mobile.security.SecurityException;
import com.tescobank.mobile.security.SecurityProvider;
import com.tescobank.mobile.security.SecurityProviderFactory;
import com.tescobank.mobile.security.SecurityProviderFactory.DefaultSecurityProviderFactory;
import com.tescobank.mobile.ui.TescoActivity;
import com.tescobank.mobile.ui.UncaughtErrorActivity;
import com.tescobank.mobile.ui.atm.ATMProvider;
import com.tescobank.mobile.ui.fomenu.FlyoutMenu;
import com.tescobank.mobile.ui.regpay.RegularPaymentsAdapter;
import com.tescobank.mobile.ui.transaction.TransactionsListAdapter;
import com.tescobank.mobile.ui.transaction.TransactionsListAdapter.SortOrder;
import com.tescobank.mobile.ui.tutorial.TooltipManager;

/**
 * Stores required application state.
 */
public class TescoApplication extends Application implements ApplicationState {

	private static final String TAG = TescoApplication.class.getSimpleName();

	private static final String STORE = "store";
	private static final String APPLICATION_PROPERTIES_FILE = "properties/application.properties";
	private static final String DEVICE_ID_BUNDLE_KEY = "DEVICE_ID_BUNDLE_KEY";
	private static final String TBID_BUNDLE_KEY = "TBIID_BUNDLE_KEY";
	private static final String CSSO_ID_BUNDLE_KEY = "CSSO_ID_BUNDLE_KEY";

	private NetworkStatusChangeReceiver networkStatusChangeReceiver;
	private boolean networkConnectionLostMessageDisplayed;
	private RequestHeadersProvider requestHeadersProvider;
	private RestRequestProcessor<ErrorResponseWrapper> restProcessor;
	private TescoAnalyticsTracker analyticsTracker;

	private Persister persister;
	private SettingsResponse settings;
	private FeaturesResponse features;
	private String deviceId;
	private String cssoId;
	private String tbId;
	private boolean isPassword;

	private List<Product> products;
	private List<ProductDetails> productDetails;
	private Account[] fundingAccounts;
	private List<Transactee> transactees;
	private TescoLocationProvider locationProvider;
	private ATMProvider atmProvider;

	private RestRequestProcessorFactory restRequestProcessorFactory;
	private SecurityProviderFactory securityProviderFactory;

	private DeviceInfo deviceInfo;
	
	private Encrypter encrypter;

	private TescoApplicationPropertiesReader tescoApplicationPropertiesReader;

	private InAuthController inAuthInitialiser;

	public TescoApplication() {
		super();

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Constructing Application State");
		}

		// The default request processor factory
		this.restRequestProcessorFactory = new VolleyRestRequestProcessorFactory<Object>();
		this.securityProviderFactory = new DefaultSecurityProviderFactory();
		installCustomUncaughtExceptionHandler();
	}

	public static void onResourceVerificationFailed() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Resource Verification Failed");
		}
		new RuntimeException("App properties in an unknonw state");
	}

	public static void onResourceVerificationSucceeded() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Resource Verification Succeeded");
		}
	}

	/**
	 * Sets the rest request processor factory to use. If null, defaults to Volley. Reason for making the factory an instance is that the factory is called in the {@link #prepareRequestProcessor()} method.
	 * 
	 * @param restRequestProcessFactory
	 */
	public void setRestRequestProcessorFactory(RestRequestProcessorFactory restRequestProcessFactory) {
		this.restRequestProcessorFactory = restRequestProcessFactory;
		if (null == this.restRequestProcessorFactory) {
			this.restRequestProcessorFactory = new VolleyRestRequestProcessorFactory<Object>();
		}
	}

	public RestRequestProcessorFactory getRestRequestProcessorFactory() {
		return restRequestProcessorFactory;
	}

	@Override
	public void onCreate() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Creating Application State");
		}
		super.onCreate();
		createTescoApplicationPropertiesReader();

		locationProvider = new TescoLocationTracker(this, this);

		createAnalyticsFactory();
		createPersister();

		settings = new SettingsResponse();
		features = new FeaturesResponse();
		deviceInfo = new DeviceInfo(this);

		createInAuthInitialiser();

		prepareRequestHeadersProvider();
		loadState();
		registerNetworkStatusChangeReceiver();
		atmProvider = new TescoATMProvider(this, this);
	}

	private void createAnalyticsFactory() {		
		AnalyticsTracker tracker = AnalyticsTrackerFactory.createAnalyticsTracker(getLocationProvider());
		analyticsTracker = new TescoAnalyticsTracker(tracker, new AnalyticsEventFactory());
	}

	private void installCustomUncaughtExceptionHandler() {

		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread thread, Throwable ex) {
				if (BuildConfig.DEBUG) {
					Log.e(TAG, "An uncaught exception has occured!", ex);
				}
				launchErrorActivity();
				Process.killProcess(Process.myPid());
			}

			private void launchErrorActivity() {
				Intent errorIntent = new Intent(TescoApplication.this, UncaughtErrorActivity.class);
				errorIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(errorIntent);
			}
		});
	}

	private void prepareRequestHeadersProvider() {
		requestHeadersProvider = new RequestHeadersProvider(deviceInfo);
	}

	@Override
	public void onTerminate() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Terminating Application State");
		}

		unregisterNetworkStatusChangeReceiver();
		super.onTerminate();
	}

	@Override
	public synchronized void resetApplication() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Resetting Application State");
		}

		TooltipManager.reset(this);
		FlyoutMenu.reset(this);
		settings = new SettingsResponse();
		features = new FeaturesResponse();

		prepareRequestHeadersProvider();
		prepareRequestProcessor();

		setDeviceId(null);
		setCssoId(null);
		setTbId(null);
		setIsPassword(true);

		// reset customer's products and associated details
		setProducts(null);
		setProductDetails(null);
		setFundingAccounts(null);
		setTransactees(null);

		resetSecurityProviderAccounts();
		resetPersisterAndSecretKey();

		locationProvider.stopTracking();
		locationProvider.startTracking();
	}

	private void resetSecurityProviderAccounts() {
		try {
			getSecurityProvider().deleteAllAccounts();
		} catch (SecurityException e) {
			if (BuildConfig.DEBUG) {
				Log.e(TAG, "Error deleting Arcot Account", e);
			}
		}
	}

	private void resetPersisterAndSecretKey() {
		persister.clear();
		SecretKeyFactory.clearSecretKey(this, STORE);
		createPersister();
	}

	private void createPersister() {
		try {
			encrypter = EncrypterFactory.createEncrypter(SecretKeyFactory.createSecretKey(getApplicationContext(), STORE));
			persister = PersisterFactory.createPersister(this, encrypter);
		} catch (CouldNotGenerateSecretKeyException e) {
			// TODO Need to go to error page!
			Log.e(TAG, "Failed to create SecretKey", e);
		}
	}

	private void createTescoApplicationPropertiesReader() {

		try {
			tescoApplicationPropertiesReader = new TescoApplicationPropertiesReader(this, APPLICATION_PROPERTIES_FILE);
		} catch (Exception e) {
			Log.e(TAG, "Failed to load application properties", e);
		}
	}

	@Override
	public synchronized void resetForNewSession() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Resetting For New Session");
		}

		requestHeadersProvider.resetAuthorization();
		setProducts(null);
		setProductDetails(null);
		setFundingAccounts(null);
		setTransactees(null);
	}

	@Override
	public void persistSensitiveData() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Persisting Sensitive Data");
		}

		persister.persistSensitiveData(deviceId, cssoId, tbId, isPassword);
	}

	@Override
	public RestRequestProcessor<ErrorResponseWrapper> getRestRequestProcessor() {

		if (null == restProcessor) {
			synchronized (restRequestProcessorFactory) {
				if (null == restProcessor) {
					prepareRequestProcessor();
				}
			}
		}

		return restProcessor;
	}

	@Override
	public final SecurityProvider getSecurityProvider() {
		String provUrl = getApplicationPropertyValueForKey(ApplicationPropertyKeys.ARCOT_PROV_URL);
		return securityProviderFactory.getSecurityProvider(this, persister, provUrl);
	}

	public final void setSecurityProviderFactory(SecurityProviderFactory securityProviderFactory) {
		if (BuildConfig.DEBUG) {
			this.securityProviderFactory = securityProviderFactory;
		}
	}

	@Override
	public RequestHeadersProvider getRequestHeadersProvider() {
		return requestHeadersProvider;
	}

	@Override
	public TescoAnalyticsTracker getAnalyticsTracker() {
		return analyticsTracker;
	}

	@Override
	public TimeoutManager createTimeoutManager(TescoActivity activity, long inactivityTimeout, long backgroundTimeout) {
		return new TescoTimeoutManager(activity, inactivityTimeout, backgroundTimeout);
	}

	public void setAnalyticsTracker(TescoAnalyticsTracker analyticsTracker) {
		if (BuildConfig.DEBUG) {
			this.analyticsTracker = analyticsTracker;
		}
	}

	@Override
	public String getDeviceId() {
		return deviceId;
	}

	@Override
	public synchronized void setDeviceId(String deviceId) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Setting Device ID: " + deviceId);
		}
		this.deviceId = deviceId;

		requestHeadersProvider.setDeviceId(deviceId);
	}

	@Override
	public String getCssoId() {
		return cssoId;
	}

	@Override
	public void setCssoId(String cssoId) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Setting CSSO ID: " + cssoId);
		}
		this.cssoId = cssoId;
	}

	@Override
	public String getTbId() {
		return tbId;
	}

	@Override
	public String getHashedTbId() {
		String hashedTbId = null;
		if (tbId != null && cssoId != null) {
			hashedTbId = HMACSHA256Encoder.encode(tbId, cssoId);
		}
		return hashedTbId;
	}

	@Override
	public void setTbId(String tbId) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Setting TB ID: " + tbId);
		}
		this.tbId = tbId;
		analyticsTracker.setHashedTbId(getHashedTbId());
	}

	@Override
	public boolean isPassword() {
		return isPassword;
	}

	@Override
	public void setIsPassword(boolean isPassword) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Setting isPassword: " + isPassword);
		}
		this.isPassword = isPassword;
		persister.persistIsPassword(isPassword);

		requestHeadersProvider.setCredential(isPassword ? Credential.PASSWORD : Credential.PASSCODE);
	}

	@Override
	public SettingsResponse getSettings() {
		return settings;
	}

	@Override
	public void setSettings(SettingsResponse settings) {
		this.settings = (settings == null) ? new SettingsResponse() : settings;
		analyticsTracker.setEnabledTrackingGroups(settings.getAnalyticsGroups());
	}

	@Override
	public FeaturesResponse getFeatures() {
		return features;
	}

	@Override
	public void setFeatures(FeaturesResponse features) {
		this.features = (features == null) ? new FeaturesResponse() : features;
	}

	@Override
	public boolean isRegistered() {
		return persister.getCssoId() != null && persister.getDeviceId() != null && persister.getTbId() != null;
	}

	@Override
	public List<Product> getProducts() {
		return products;
	}

	@Override
	public void setProducts(List<Product> products) {
		this.products = products;
	}

	@Override
	public List<ProductDetails> getProductDetails() {
		return productDetails;
	}

	@Override
	public void setProductDetails(List<ProductDetails> productDetails) {
		this.productDetails = productDetails;
	}

	@Override
	public void updateProductDetails(List<ProductDetails> newProductDetails) {
		for (ProductDetails newProductDetail : newProductDetails) {
			int index = productDetails.indexOf(newProductDetail);
			productDetails.add(index, newProductDetail);
			productDetails.remove(index + 1);
		}
	}

	public List<ProductDetails> getDirtyProductDetails() {
		List<ProductDetails> staleProducts = new ArrayList<ProductDetails>();
		for (ProductDetails product : productDetails) {
			if (product.isDirty()) {
				staleProducts.add(product);
			}
		}
		return staleProducts;
	}

	@Override
	public Account[] getFundingAccounts() {
		return fundingAccounts;
	}

	@Override
	public void setFundingAccounts(Account[] fundingAccounts) {
		this.fundingAccounts = (fundingAccounts == null) ? null : Arrays.copyOf(fundingAccounts, fundingAccounts.length);
	}

	public List<Transactee> getTransactees() {
		return transactees;
	}

	public void setTransactees(List<Transactee> transactees) {
		this.transactees = transactees;
	}

	@Override
	public UseLocationPreference getUseLocationPreference() {
		return persister.getUseLocationPreference();
	}

	@Override
	public void setUseLocationPreference(UseLocationPreference preference) {
		persister.persistUseLocation(preference);
		getLocationProvider().update();
	}

	@Override
	public TescoLocationProvider getLocationProvider() {
		return locationProvider;
	}

	@Override
	public void notifyLocationChanged() {
		Location location = locationProvider.getLocation();
		requestHeadersProvider.setLocation(new TescoLocation(location.getLatitude(), location.getLongitude()));
	}

	@Override
	public ATMProvider getATMProvider() {
		return atmProvider;
	}

	@Override
	public void setTransactionSortOrderForProduct(SortOrder sort, ProductDetails product) {
		persister.persistTransactionSortOrderForProductId(sort.persistenceValue(), product.getProductId());
	}

	@Override
	public TransactionsListAdapter.SortOrder getTransactionSortOrderForProduct(ProductDetails product) {
		String sortOrder = persister.getTransactionSortOrderForProductId(product.getProductId());
		if (null == sortOrder) {
			return null;
		}

		return TransactionsListAdapter.SortOrder.fromPersistenceValue(sortOrder);
	}

	@Override
	public void setRegularPaymentSortOrderForProduct(com.tescobank.mobile.ui.regpay.RegularPaymentsAdapter.SortOrder sort, ProductDetails product) {
		persister.persistRegularPaymentSortOrderForProductId(sort.persistenceValue(), product.getProductId());
	}
	
	@Override
	public void incrementSubsequentLoginCount() {
		int newCount = persister.getSubsequentLoginCount() + 1;
		persister.persistSubsequentLoginCount(newCount);
	}

	@Override
	public void enableBalancePeek(boolean enabled) {
		getPersister().persistIsBalancePeekEnabled(enabled);
		if(enabled) {
			getPersister().persistBalancePeekHasBeenEnabled();
		}
	}
	
	@Override
	public boolean shouldDisplayBalancePeekTutorial() {
		return features.isNonInteractiveProductsEnabled() && !persister.hasSeenBalancePeekTutorial() && !persister.hasBalancePeekEverBeenEnabled() && persister.getSubsequentLoginCount() > 0;
	}
	
	@Override
	public void hasSeenBalancePeekTutorial() {
		persister.persistHasSeenBalancePeekTutorial();
	}
	
	@Override
	public RegularPaymentsAdapter.SortOrder getRegularPaymentSortOrderForProduct(ProductDetails productDetails) {
		String sortOrder = persister.getRegularPaymentSortOrderForProductId(productDetails.getProductId());
		if (null == sortOrder) {
			return null;
		}

		return RegularPaymentsAdapter.SortOrder.fromPersistenceValue(sortOrder);
	}

	private void loadState() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Loading State");
		}

		setDeviceId(persister.getDeviceId());
		setCssoId(persister.getCssoId());
		setTbId(persister.getTbId());
		setIsPassword(persister.getIsPassword());
	}

	private synchronized void prepareRequestProcessor() {

		RestConfig<ErrorResponseWrapper> config = new RestConfig<ErrorResponseWrapper>();
		config.setContext(this);
		config.setConnectionTimeOut(Integer.valueOf(getApplicationPropertyValueForKey(ApplicationPropertyKeys.API_CONECTION_TIMEOUT_MS)));
		config.setJsonParser(JsonParserFactory.createJsonParser());
		config.setDefaultBaseUrl(getApplicationPropertyValueForKey(ApplicationPropertyKeys.BASE_API_URL));
		config.setRequestHeadersProvider(getRequestHeadersProvider());
		config.setResponseHeadersHandler(new ResponseHeadersHandler(getRequestHeadersProvider()));
		config.setErrorResponseBuilder(new ErrorResponseWrapperBuilder(this));

		restProcessor = getRestRequestProcessorFactory().createRestRequestProcessor(config);
	}

	private void registerNetworkStatusChangeReceiver() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Registering Network Status Change Receiver");
		}

		networkStatusChangeReceiver = new NetworkStatusChangeReceiver(this);
		registerReceiver(networkStatusChangeReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
		networkConnectionLostMessageDisplayed = false;
	}

	private void unregisterNetworkStatusChangeReceiver() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "Un-Registering Network Status Change Receiver");
		}

		unregisterReceiver(networkStatusChangeReceiver);
		networkConnectionLostMessageDisplayed = false;
	}

	@Override
	public boolean isNetworkConnectionLostMessageDisplayed() {
		return networkConnectionLostMessageDisplayed;
	}

	@Override
	public void setNetworkConnectionLostMessageDisplayed(boolean networkConnectionLostMessageDisplayed) {
		this.networkConnectionLostMessageDisplayed = networkConnectionLostMessageDisplayed;
	}

	@Override
	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	@Override
	public Persister getPersister() {
		return persister;
	}
	
	public void createInAuthInitialiser() {
		inAuthInitialiser = new MobileInAuthController(this);
	}

	@Override
	public InAuthController getInAuthController() {
		return inAuthInitialiser;
	}

	@Override
	public BigDecimal getNextTaxYearSubscriptionLimit(ProductDetails product) {
		if (ProductType.JISA_PRODUCT.getLabel().equals(product.getProductType())) {
			return getSettings().getNextTaxYearJuniorSubscriptionLimit();
		}
		return getSettings().getNextTaxYearSubscriptionLimit();
	}

	@Override
	public String getApplicationPropertyValueForKey(ApplicationPropertyKeys propertyKey) {
		return tescoApplicationPropertiesReader.getApplicationPropertiesValue(propertyKey);
	}

	@Override
	public void saveData(Bundle outState) {
		saveObject(outState, DEVICE_ID_BUNDLE_KEY, deviceId);
		saveObject(outState, TBID_BUNDLE_KEY, tbId);
		saveObject(outState, CSSO_ID_BUNDLE_KEY, cssoId);
		saveObject(outState, SettingsResponse.class.getName(), settings);
		saveObject(outState, FeaturesResponse.class.getName(), features);
		saveObject(outState, RequestHeadersProvider.class.getName(), requestHeadersProvider);
	}

	@Override
	public void restoreData(Bundle savedState) {
		restoreObjectIfSaved(savedState, DEVICE_ID_BUNDLE_KEY, deviceId);
		restoreObjectIfSaved(savedState, TBID_BUNDLE_KEY, tbId);
		restoreObjectIfSaved(savedState, CSSO_ID_BUNDLE_KEY, cssoId);
		restoreObjectIfSaved(savedState, SettingsResponse.class.getName(), settings);
		restoreObjectIfSaved(savedState, FeaturesResponse.class.getName(), features);
		restoreObjectIfSaved(savedState, RequestHeadersProvider.class.getName(), requestHeadersProvider);
		requestHeadersProvider.setDeviceId(deviceId);
		prepareRequestProcessor();
	}

	private void restoreObjectIfSaved(Bundle savedState, String key, Object item) {
		if (savedState.containsKey(key)) {
			item = savedState.get(key);
		}
	}

	private void saveObject(Bundle outState, String key, Serializable item) {
		if (item != null) {
			outState.putSerializable(key, item);
		}
	}
}
