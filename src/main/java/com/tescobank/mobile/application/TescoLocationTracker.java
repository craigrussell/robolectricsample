package com.tescobank.mobile.application;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.persist.Persister.UseLocationPreference;
import com.tescobank.mobile.properties.TescoApplicationPropertiesReader.ApplicationPropertyKeys;

public class TescoLocationTracker implements LocationListener, TescoLocationProvider {

	private static final String TAG = TescoLocationTracker.class.getSimpleName();

	private final Context context;
	private final float locationUpdateMinDistance;
	private final int locationUpdateMinTime;

	private Location lastLocation;
	private ApplicationState appState;
	private String provider;
	private Status status;

	private LocationManager mgr;

	private ScheduledFuture<?> timeoutSchedule;

	public TescoLocationTracker(ApplicationState appState, Context context) {
		this.context = context;
		this.appState = appState;

		locationUpdateMinTime = Integer.parseInt(appState.getApplicationPropertyValueForKey(ApplicationPropertyKeys.LOCATION_UPDATE_MIN_TIME_MILLIS));
		locationUpdateMinDistance = Float.parseFloat(appState.getApplicationPropertyValueForKey(ApplicationPropertyKeys.LOCATION_UPDATE_DISTANCE_METERS_MILLIS));
		mgr = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
	}

	@Override
	public void startTracking() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "startTracking");
		}

		if (appState.getUseLocationPreference() == UseLocationPreference.YES) {

			provider = mgr.getBestProvider(new Criteria(), true);

			if (null != provider && !LocationManager.PASSIVE_PROVIDER.equals(provider)) {
				requestLocationUpdates();
			} else {
				updateStatus(Status.SYSTEM_DISABLED);
			}
		} else {
			updateStatus(Status.USER_DISABLED);
		}
	}

	@Override
	public void stopTracking() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "stopTracking");
		}
		mgr.removeUpdates(this);
	}

	public Location getLocation() {
		return lastLocation;
	}

	@Override
	public Status getLocationStatus() {
		return status;
	}

	@Override
	public boolean isUserEnabled() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "UseLocationPreference: " + appState.getUseLocationPreference());
		}
		return appState.getUseLocationPreference() == UseLocationPreference.YES;
	}

	// location listener methods

	@Override
	public void onLocationChanged(Location location) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onLocationChanged: " + location);
		}

		if (null != timeoutSchedule) {
			timeoutSchedule.cancel(false);
			timeoutSchedule = null;
		}

		lastLocation = location;
		appState.notifyLocationChanged();
		updateStatus(Status.OK);
	}

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	// private methods:

	private void requestLocationUpdates() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "requestLocationUpdates, " + locationUpdateMinTime + ", " + locationUpdateMinDistance);
		}

		Location location = mgr.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
		location = bestLocation(location, mgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
		location = bestLocation(location, mgr.getLastKnownLocation(LocationManager.GPS_PROVIDER));

		lastLocation = location;
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "current best known location: " + lastLocation);
		}

		if (null == lastLocation) {
			updateStatus(Status.IN_PROGRESS);
		} else {
			updateStatus(Status.OK);
		}

		mgr.requestLocationUpdates(provider, locationUpdateMinTime, locationUpdateMinDistance, this);
	}

	private Runnable createInProgressTimeout() {
		return new Runnable() {
			@Override
			public void run() {
				if (Status.IN_PROGRESS == status) {
					updateStatus(Status.SYSTEM_DISABLED);
					timeoutSchedule = null;
				}
			}
		};
	}

	@Override
	public void update() {
		stopTracking();
		startTracking();
	}

	private void updateStatus(Status newStatus) {
		if (statusChanged(newStatus) || newStatus == Status.OK) {
			setNewStatus(newStatus);
			scheduleInProgressTimeout();
		}
	}
	
	private boolean statusChanged(Status newStatus) {
		return (newStatus != null) && !newStatus.equals(status);
	}
	
	private void setNewStatus(Status newStatus) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "setting new status: " + newStatus);
		}
		status = newStatus;
		Intent intent = new Intent(LOCATION_STATUS_BROADCAST_EVENT);
		intent.putExtra(Status.class.getSimpleName(), newStatus);
		LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
	}
	
	private void scheduleInProgressTimeout() {
		if (status == Status.IN_PROGRESS) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "scheduling in progress timeout");
			}
			timeoutSchedule = Executors.newSingleThreadScheduledExecutor().schedule(createInProgressTimeout(), 1, TimeUnit.MINUTES);
		}
	}

	protected Location bestLocation(Location locationA, Location locationB) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "comparing: " + locationA + " vs " + locationB);
		}

		if (locationA == null) {
			return locationB;
		}

		if (locationB == null) {
			return locationA;
		}

		// Check whether the new location fix is newer or older
		if (isTimeDifferenceSignificant(locationA, locationB)) {
			return getMostRecentLocation(locationA, locationB);
		}

		// Check whether the new location fix is more or less accurate
		return getMostAccurateLocation(locationA, locationB);
	}
	
	private boolean isTimeDifferenceSignificant(Location locationA, Location locationB) {
		return Math.abs(locationA.getTime() - locationB.getTime()) > locationUpdateMinTime;
	}
	
	private Location getMostRecentLocation(Location locationA, Location locationB) {
		if (locationA.getTime() > locationB.getTime()) {
			return locationA;
		} else {
			return locationB;
		}
	}
	
	private Location getMostAccurateLocation(Location locationA, Location locationB) {
		if (locationA.getAccuracy() > locationB.getAccuracy()) {
			return locationA;
		} else {
			return locationB;
		}
	}
}
