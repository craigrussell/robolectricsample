package com.tescobank.mobile.application;

import android.content.Intent;
import android.os.Bundle;

public class ActivityStartSpec {
	
	private Intent intent;
	private int requestCode;
	private Bundle options;	
	
	public ActivityStartSpec(Intent intent, int requestCode, Bundle options) {
		this.intent = intent;
		this.requestCode = requestCode;
		this.options = options;
	}
	
	public ActivityStartSpec(Intent intent, Bundle options) {
		this(intent, -1, options);
	}

	public Intent getIntent() {
		return intent;
	}

	public int getRequestCode() {
		return requestCode;
	}

	public Bundle getOptions() {
		return options;
	}
}