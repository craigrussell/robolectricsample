package com.tescobank.mobile.json;

/**
 * JSON parse exception
 */
public class JsonParseException extends Exception {
	
	/** Serial version ID */
	private static final long serialVersionUID = 6486207402193012916L;

	/**
	 * Constructor
	 * 
	 * @param cause the cause
	 */
	public JsonParseException(Throwable cause) {
		super(cause);
	}
}
