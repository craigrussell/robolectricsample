package com.tescobank.mobile.json;

import com.fasterxml.jackson.databind.ObjectMapper;

class JacksonJsonParser implements JsonParser {

	private ObjectMapper mapper = new ObjectMapper();

	@Override
	public <T> T parseObject(String json, Class<T> classOfT) throws JsonParseException {
		
		if (isEmpty(json)) {
			return null;
		}
		
		return performParse(json, classOfT);
	}
	
	private boolean isEmpty(String json) {
		return (json == null) || json.trim().isEmpty();
	}
	
	private <T> T performParse(String json, Class<T> classOfT) throws JsonParseException {
		try {
			return mapper.readValue(json, classOfT);
		} catch (Exception e) {
			throw new JsonParseException(e);
		}
	}
}
