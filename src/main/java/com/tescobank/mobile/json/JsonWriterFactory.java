package com.tescobank.mobile.json;

public class JsonWriterFactory {

	public static JsonWriter createJsonWriter() {
		return new JacksonJsonWriter();
	}

}
