package com.tescobank.mobile.json;

/**
 * This is a runtime exception because creating JSON from a java object should be both "easy" and well tested, so exceptions while writing should not occur.
 */
public class JsonWriteException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public JsonWriteException(Throwable cause) {
		super(cause);
	}

}
