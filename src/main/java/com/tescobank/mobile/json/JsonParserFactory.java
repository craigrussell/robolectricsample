package com.tescobank.mobile.json;


/**
 * Factory for JSON parsers
 */
public final class JsonParserFactory {
	
	/**
	 * Private constructor
	 */
	private JsonParserFactory() {
		super();
	}
	
	/**
	 * Creates and returns a JSON parser
	 * 
	 * @return a JSON parser
	 */
	public static JsonParser createJsonParser() {
		return new JacksonJsonParser();
	}
}
