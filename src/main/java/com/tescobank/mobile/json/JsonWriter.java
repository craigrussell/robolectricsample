package com.tescobank.mobile.json;

public interface JsonWriter {

	String writeValueAsString(Object o) throws JsonWriteException;

}
