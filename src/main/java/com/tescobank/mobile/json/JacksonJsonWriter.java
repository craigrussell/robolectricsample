package com.tescobank.mobile.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

class JacksonJsonWriter implements JsonWriter {

	private static final ObjectMapper MAPPER = new ObjectMapper();

	@Override
	public String writeValueAsString(Object o) throws JsonWriteException {
		try {
			return MAPPER.writeValueAsString(o);
		} catch (JsonProcessingException e) {
			throw new JsonWriteException(e);
		}
	}

}
