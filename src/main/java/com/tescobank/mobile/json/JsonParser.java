package com.tescobank.mobile.json;

/**
 * JSON parser
 */
public interface JsonParser {
	
	/**
	 * Deserializes the specified JSON into an object of the specified class
	 * 
	 * @param json the JSON
	 * @param classOfT the class of object to deserialize into
	 * 
	 * @return the deserialized object
	 * 
	 * @throws JsonParseException thrown if the JSON cannot be parsed
	 */
	<T> T parseObject(String json, Class<T> classOfT) throws JsonParseException;
}
