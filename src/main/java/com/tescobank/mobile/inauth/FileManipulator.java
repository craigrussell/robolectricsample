package com.tescobank.mobile.inauth;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;

public interface FileManipulator {
	boolean fileExists(String filename);
	void deleteFile(String fileName) throws IOException;
	void createFileFromApkAsset(String sourceAssetPath, String destinationFilename);
	void close(Closeable closeable);
	void flush(OutputStream outputStream);
}
