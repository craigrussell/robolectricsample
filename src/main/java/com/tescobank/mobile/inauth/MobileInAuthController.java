package com.tescobank.mobile.inauth;

import android.util.Log;

import com.inauth.mme.InAuthManager.InitCompletionListener;
import com.inauth.utilities.InAuthConstants;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.application.TescoApplication;
import com.tescobank.mobile.properties.TescoApplicationPropertiesReader.ApplicationPropertyKeys;

public class MobileInAuthController implements InAuthController {

	private static final String TAG = MobileInAuthController.class.getSimpleName();
	private InAuth inAuth;
	private TescoApplication application;
	private RootedDetector rootedDetector;
	private MalwareDetector malwareDetector;
	
	public MobileInAuthController(TescoApplication application) {
		this.application = application;
		this.inAuth = new MobileInAuth(application);
		this.rootedDetector = new  MobileRootedDetector(application);
		this.malwareDetector = new  MobileMalwareDetector(application);
	}
	
	@Override
	public InAuth getInAuth() {
		return inAuth;
	}
	
	public RootedDetector getRootedDetector() {
		return rootedDetector;
	}
	
	public MalwareDetector getMalwareDetector() {
		return malwareDetector;
	}
	
	@Override
	public boolean isOfflineInitialised() {
		return inAuth.isInitializedOffline();
	}
	
	@Override
	public boolean isOnlineInitialised() {
		return inAuth.isInitialized();
	}
	
	@Override
	public void initialiseOffline(InAuthOfflineInitialisationListener inAuthOfflineInitialisationListener) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "InAuth: Performing offline initialisation");
		}
		inAuth.initOffline(new OfflineInitCompletionListener(inAuthOfflineInitialisationListener));
	}

	public void initialiseOnline(final InAuthOnlineInitialisationListener inAuthOnlineInitialisationListener) {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "InAuth: Performing online initialisation");
		}
		inAuth.init(application.getApplicationPropertyValueForKey(ApplicationPropertyKeys.INAUTH_URL), application.getApplicationPropertyValueForKey(ApplicationPropertyKeys.INAUTH_ACCOUNT_ID), new OnlineInitCompletionListener(inAuthOnlineInitialisationListener));
	}
	

	private class OfflineInitCompletionListener implements InitCompletionListener {

		private static final String STATUS_SUCCESS = InAuthConstants.INITIALIZED_OFFLINE;

		private InAuthOfflineInitialisationListener inAuthOfflineInitialisationListener;

		public OfflineInitCompletionListener(InAuthOfflineInitialisationListener inAuthOfflineInitialisationListener) {
			super();

			this.inAuthOfflineInitialisationListener = inAuthOfflineInitialisationListener;
		}

		@Override
		public void onInitComplete(final String status) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "InAuth: initOffline Status: " + status);
			}
			handleResponse(status);
		}

		private void handleResponse(String status) {
			if (STATUS_SUCCESS.equals(status) && inAuth.isInitializedOffline()) {
				handleSuccess();
			} else {
				handleFailure();
			}
		}

		private void handleSuccess() {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "InAuth: Offline initialisation succeeded");
			}
			inAuthOfflineInitialisationListener.inAuthOfflineInitialised();
		}

		private void handleFailure() {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "InAuth: Offline initialisation failed");
			}
			inAuthOfflineInitialisationListener.inAuthOfflineFailed();
		}
	}
	

	private class OnlineInitCompletionListener implements InitCompletionListener {

		private static final String STATUS_SUCCESS = InAuthConstants.SUCCESS;

		private InAuthOnlineInitialisationListener inAuthOnlineInitialisationListener;

		public OnlineInitCompletionListener(InAuthOnlineInitialisationListener inAuthOnlineInitialisationListener) {
			super();

			this.inAuthOnlineInitialisationListener = inAuthOnlineInitialisationListener;
		}

		@Override
		public void onInitComplete(final String status) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "InAuth: Init Online Status: " + status);
			}
			handleResponse(status);
		}

		private void handleResponse(String status) {
			if (STATUS_SUCCESS.equals(status)) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "InAuth: Online initialisation succeeded");
				}
				inAuthOnlineInitialisationListener.inAuthOnlineInitialised();	
			} 
		}
	}
}
