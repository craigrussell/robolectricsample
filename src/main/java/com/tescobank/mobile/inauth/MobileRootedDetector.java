package com.tescobank.mobile.inauth;


import android.os.AsyncTask;
import android.util.Log;

import com.inauth.root.utilities.RootConstants;
import com.inauth.root.utilities.RootLog;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.application.TescoApplication;
import com.tescobank.mobile.format.DataFormatter;
import com.tescobank.mobile.network.NetworkStatusChecker;
import com.tescobank.mobile.properties.TescoApplicationPropertiesReader.ApplicationPropertyKeys;

public class MobileRootedDetector implements RootedDetector {

	private static final String TAG = MobileRootedDetector.class.getSimpleName();
	public static final String CONFIG_LAST_UPDATED_KEY = "nfo2hdo2ndkj2ndeo2h3do";
	private static final String SIG_FILE_NAME = "rootfile";
	private static final String SIG_FILE_ASSET_PATH = "inauth/" + SIG_FILE_NAME;
	private static final int MILLISECONDS_IN_HOUR = 3600000;
	private FileManipulator fileManipulator;
	private NetworkStatusChecker networkStatusChecker;
	private TescoApplication application;

	public MobileRootedDetector(TescoApplication application) {
		super();
		this.application = application;
		this.fileManipulator = new MobileFileManipulator(application);
		this.networkStatusChecker = new NetworkStatusChecker();
		defaultToLastKnownRooted();
	}

	private void defaultToLastKnownRooted() {
		application.getDeviceInfo().setRooted(application.getPersister().getLastKnownRooted());
	}

	@Override
	public void detectRooted(RootedDetectionListener rootedDetectionListener, boolean backgroundThreadTask) {	
		if(backgroundThreadTask) {
			detectRootedBackgroundTask();
			
		} else {
			new DetectRootedTask(rootedDetectionListener).execute((Void) null);
		}
	}

	private void detectRootedBackgroundTask() {
		if(BuildConfig.DEBUG) {
			Log.d(TAG, "InAuth: Online now so doing background thread Root Detected call");
		}
		
		if(canUpdateConfig() && shouldInvokeRootProcessing()) {
			ensureSigFileExists();
			detectRooted();	
			return;
		}		
		
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "InAuth: Detecting rooted not required as run in the last 24 hours!");
		}
	}

	@Override
	public void updateRootSigFile() {
		if (canUpdateConfig() && shouldInvokeRootProcessing()) {
			persistConfigLastUpdated();
			processUpdateConfig();
		}
	}

	private void processUpdateConfig() {

		String result = application.getInAuthController().getInAuth().updateRootSigfile(getSigFilePath());
				
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "InAuth: Update config result for Root: " + result);
		}
	}

	private void ensureSigFileExists() {
		if (!fileManipulator.fileExists(SIG_FILE_NAME)) {
			fileManipulator.createFileFromApkAsset(SIG_FILE_ASSET_PATH, SIG_FILE_NAME);
		}
	}

	private boolean detectRooted() {	
		long startTime = System.currentTimeMillis();
		RootLog rootLog = application.getInAuthController().getInAuth().getRootDetectionLog(getSigFilePath(), true);
		boolean result = RootConstants.DEVICE_ROOTED.equals(rootLog.getRootStatus());
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "InAuth: Root detection took " + (System.currentTimeMillis() - startTime) + " ms");
			Log.d(TAG, "InAuth: Root detected: " + result);
		}
		return result;
	}

	private String getSigFilePath() {
		return application.getFilesDir().getAbsolutePath() + "/" + SIG_FILE_NAME;
	}

	private void notifyRootedDetectionListener(RootedDetectionListener rootedDetectionListener, boolean rooted) {
		application.getPersister().persistLastKnownRooted(rooted);
		application.getDeviceInfo().setRooted(rooted);
		rootedDetectionListener.rootedCompleted();
	}

	private boolean canUpdateConfig() {
		return networkStatusChecker.isAvailable(application) && application.getInAuthController().getInAuth().isInitialized();
	}

	private boolean shouldInvokeRootProcessing() {
		int configUpdateIntervalMillis = Integer.parseInt(application.getApplicationPropertyValueForKey(ApplicationPropertyKeys.INAUTH_FILE_DOWNLOAD_DELAY)) * MILLISECONDS_IN_HOUR;
		Long millisSinceConfigLastUpdated = getMillisSinceConfigLastUpdated();
		return (millisSinceConfigLastUpdated == null) ? true : millisSinceConfigLastUpdated > configUpdateIntervalMillis;
	}

	private Long getMillisSinceConfigLastUpdated() {
		Long configLastUpdated = getConfigLastUpdated();
		return (configLastUpdated == null) ? null : System.currentTimeMillis() - configLastUpdated;
	}

	private Long getConfigLastUpdated() {
		Long lastUpdated = null;
		String configLastUpdated = application.getPersister().getInAuthSigfileUpdatedAt();
		if(configLastUpdated != null) {
			try {
				lastUpdated = Long.valueOf(configLastUpdated);
			} catch (NumberFormatException e) {
				DataFormatter dataFormatter = new DataFormatter(); 
				lastUpdated = dataFormatter.parseDate(configLastUpdated).getTime();
			}
		}
		return lastUpdated;
	}

	private void persistConfigLastUpdated() {
		application.getPersister().persistInAuthSigfileUpdatedAt(String.valueOf(System.currentTimeMillis()));
	}

	private class DetectRootedTask extends AsyncTask<Void, Void, Boolean> {
		private RootedDetectionListener rootedDetectionListener;

		public DetectRootedTask(RootedDetectionListener rootedDetectionListener) {
			super();
			this.rootedDetectionListener = rootedDetectionListener;
		}

		@Override
		protected Boolean doInBackground(Void... params) {

			if(shouldInvokeRootProcessing()) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "InAuth: Detecting rooted");
				}
				ensureSigFileExists();
				return detectRooted();
			} 
			
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "InAuth: Detecting rooted not required as run in the last 24 hours!");
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			notifyRootedDetectionListener(rootedDetectionListener, result);
		}
	}
}
