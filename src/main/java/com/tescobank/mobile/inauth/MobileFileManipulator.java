package com.tescobank.mobile.inauth;

import static android.content.Context.MODE_PRIVATE;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;

public class MobileFileManipulator implements FileManipulator {
	private static final int STREAM_COPY_BUFFER_SIZE = 1024;
	private Context context;
	
	public MobileFileManipulator(Context context) {
		super();			
		this.context = context;
	}

	public boolean fileExists(String filename) {
		
		String[] fileList = context.fileList();
		if (fileList == null) {
			return false;
		}
				
		for (String nextFilename : fileList) {
			if (filename.equals(nextFilename)) {
				return true;
			}
		}
				
		return false;
	}
	
			
	public void deleteFile(String filename) throws IOException {
				
		if (!fileExists(filename)) {
			return;
		}
				
		if (!context.deleteFile(filename)) {
			throw new IOException();
		}
	}
			
	public void createFileFromApkAsset(String sourceAssetPath, String destinationFilename) {
				
		InputStream inputStream = null;
		try {
				
			inputStream = context.getAssets().open(sourceAssetPath);
			createFileFromInputStream(inputStream, destinationFilename);
					
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			close(inputStream);
		}
	}
			
	private void createFileFromInputStream(InputStream sourceInputStream, String destinationFilename) throws IOException {
				
		OutputStream outputStream = null;
		try {
					
			deleteFile(destinationFilename);
			outputStream = context.openFileOutput(destinationFilename, MODE_PRIVATE);
			copy(sourceInputStream, outputStream);
					
		} finally {
			close(sourceInputStream);
			flush(outputStream);
			close(outputStream);
		}
	}
			
	private void copy(InputStream inputStream, OutputStream outputStream) throws IOException {
		byte[] buffer = new byte[STREAM_COPY_BUFFER_SIZE];  
		int length;
		while ((length = inputStream.read(buffer)) > 0) {
		    outputStream.write(buffer, 0, length);
		}
	}
			
	public void flush(OutputStream outputStream){
		if (outputStream == null) {
			return;
		}
		try {
			outputStream.flush();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
		 	
	public void close(Closeable closeable) {
		if (closeable == null) {
			return;
		}
		try {
			closeable.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
