package com.tescobank.mobile.inauth;

import android.app.Application;

import com.inauth.mme.InAuthManager;
import com.inauth.mme.InAuthManager.InitCompletionListener;
import com.inauth.mw.utilities.MalwareLog;
import com.inauth.root.utilities.RootLog;


/**
 * Default {@link InAuth} implementation. Singleton because it tracks the offline initialisation
 */
public class MobileInAuth implements InAuth {
	private Application application;
	private InAuthManager inAuthManager;

	public MobileInAuth(Application application) {
		super();
		this.application = application;

		inAuthManager = InAuthManager.getInstance();
	}

	@Override
	public void init(String serverURL, String accountGUID, InitCompletionListener listener) {
		inAuthManager.init(application, serverURL, accountGUID, listener);
	}

	@Override
	public void initOffline(final InitCompletionListener listener) {

		inAuthManager.initOffline(application, new InitCompletionListener() {
			@Override
			public void onInitComplete(String status) {
				listener.onInitComplete(status);
			}
		});
	}

	@Override
	public boolean isInitialized() {
		return inAuthManager.isInitialized();
	}

	@Override
	public boolean isInitializedOffline() {
		return inAuthManager.isInitializedOffline();
	}

	@Override
	public RootLog getRootDetectionLog(String sigFilePath, boolean setDeepRootEnabled) {
		return inAuthManager.getRootDetectionLog(sigFilePath, setDeepRootEnabled);
	}

	@Override
	public MalwareLog getMalwareDetectionLog(String sigFilePath) {
		return inAuthManager.getMalwareDetectionLog(sigFilePath);
	}

	@Override
	public String updateRootSigfile(String filePath) {
		return inAuthManager.updateRootSigfile(filePath);
	}

	@Override
	public String updateMalwareSigfile(String filePath) {
		return inAuthManager.updateMalwareSigfile(filePath);
	}
}
