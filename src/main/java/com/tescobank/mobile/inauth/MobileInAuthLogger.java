package com.tescobank.mobile.inauth;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.inauth.mme.InAuthManager;
import com.tescobank.mobile.BuildConfig;
import com.tescobank.mobile.application.ApplicationState;
import com.tescobank.mobile.persist.Persister;

public class MobileInAuthLogger {

	private final static String TAG = MobileInAuthLogger.class.getSimpleName();

	public static void sendGPSLog(final ApplicationState applicationState) {
		if(!applicationState.getInAuthController().isOnlineInitialised()) {
			return;
		}
		
		if (applicationState.getUseLocationPreference() == Persister.UseLocationPreference.YES) {
			
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				@Override
				public void run() {
					InAuthManager.getInstance().sendGPSLog();	
				}
			});
		}
	}

	public static void sendInAuthLogs(final ApplicationState applicationState) {
		if(!applicationState.getInAuthController().isOnlineInitialised()) {
			return;
		}
		
		long startTime = System.currentTimeMillis();

		InAuthManager.getInstance().sendAppActivityLog();
		InAuthManager.getInstance().sendAppInstallLog();
		InAuthManager.getInstance().sendAppDataUsageLog();
		
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "InAuth: sending all other logs (APP_ACTIVITY,APP_INSTALL,APP_DATA) took " + (System.currentTimeMillis() - startTime) + " ms");
		}
	}
	
	public static void sendInAuthLogInLogs(final ApplicationState applicationState) {
		if(!applicationState.getInAuthController().isOnlineInitialised()) {
			return;
		}
		long startTime = System.currentTimeMillis();

		InAuthManager.getInstance().sendTelephonyInfoLog();
		InAuthManager.getInstance().sendDeviceInfoLog();
		InAuthManager.getInstance().sendBatteryLog();
		InAuthManager.getInstance().sendWifiConnectionLog();
		
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "InAuth: sending logon logs (TELE_INFO,DEVICE,BATTERY,WIFI_CONN) took " + (System.currentTimeMillis() - startTime) + " ms");
		}
	}
}
