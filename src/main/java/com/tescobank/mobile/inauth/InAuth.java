package com.tescobank.mobile.inauth;

import com.inauth.mme.InAuthManager.InitCompletionListener;
import com.inauth.mw.utilities.MalwareLog;
import com.inauth.root.utilities.RootLog;

public interface InAuth {	
	void init(String serverURL, String accountGUID, InitCompletionListener listener);
	void initOffline(InitCompletionListener listener);
	boolean isInitialized();
	boolean isInitializedOffline();
	RootLog getRootDetectionLog(String sigFilePath, boolean setDeepRootEnabled);
	MalwareLog getMalwareDetectionLog(String sigFilePath);
	String updateRootSigfile(String filePath);
	String updateMalwareSigfile(String filePath);
}
