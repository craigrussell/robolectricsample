package com.tescobank.mobile.inauth;

public interface InAuthController {
	
	boolean isOfflineInitialised();

	boolean isOnlineInitialised();

	InAuth getInAuth();

	RootedDetector getRootedDetector();

	MalwareDetector getMalwareDetector();

	void initialiseOffline(InAuthOfflineInitialisationListener inAuthOfflineInitialisationListener);

	void initialiseOnline(InAuthOnlineInitialisationListener inAuthOnlineInitialisationListener);

	interface InAuthOfflineInitialisationListener {
		
		void inAuthOfflineInitialised();

		void inAuthOfflineFailed();
	}

	interface InAuthOnlineInitialisationListener {
		void inAuthOnlineInitialised();
	}
}
