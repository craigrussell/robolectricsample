package com.tescobank.mobile.inauth;

public interface RootedDetector {
	void detectRooted(RootedDetectionListener rootedDetectionListener, boolean backgroundThreadTask);
	void updateRootSigFile();	

	interface RootedDetectionListener {		
		void rootedCompleted();
	}
	
}
