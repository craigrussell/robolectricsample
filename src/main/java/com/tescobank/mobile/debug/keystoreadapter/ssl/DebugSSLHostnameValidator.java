package com.tescobank.mobile.debug.keystoreadapter.ssl;

import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public final class DebugSSLHostnameValidator {

	private static final TrustingHostnameVerifier TRUST_HOST_VERIFIER = new TrustingHostnameVerifier();
	private static final String SSL_CONTEXT = "TLS";
	private static SSLSocketFactory factory;
	
	private DebugSSLHostnameValidator() {
		super();
	}

	public static URLConnection ignoreHostCertsForDebug(URLConnection conn) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {

		if (conn instanceof HttpsURLConnection) {
			HttpsURLConnection httpsConnection = (HttpsURLConnection) conn;
			SSLSocketFactory theFactory = createAndIntitialiseSSLFactory(httpsConnection);
			httpsConnection.setSSLSocketFactory(theFactory);
			httpsConnection.setHostnameVerifier(TRUST_HOST_VERIFIER);
		}
		
		return conn;
	}

	static synchronized SSLSocketFactory createAndIntitialiseSSLFactory(HttpsURLConnection httpsConnection) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

		if (factory == null) {
			SSLContext ctx = SSLContext.getInstance(SSL_CONTEXT);
			ctx.init(null, new TrustManager[] { new AlwaysTrustManager() }, null);
			factory = ctx.getSocketFactory();
		}
		return factory;
	}

	private static final class TrustingHostnameVerifier implements HostnameVerifier {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	}

	private static class AlwaysTrustManager implements X509TrustManager {
		public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
		}

		public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
		}

		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}
	}

}
